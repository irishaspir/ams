package com.zodiac.ams.charter.tests.ft.ppv;

import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.handlers.PPVHandler;
import com.dob.test.charter.handlers.PPVNotificationListener;
import com.dob.test.charter.iface.HEHandlerType;
import com.dob.test.charter.iface.IHEDataConsumer;
import com.zodiac.ams.charter.emuls.stb.ft.STBEmulatorFT;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.ppv.*;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.bd.DBHelper;
import com.zodiac.ams.common.http.FTHttpUtils;
import com.zodiac.ams.common.ssh.SSHHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.DescriptionType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.tests.FeatureList.PPV;


public class PPVTest extends FuncTest{
    private static final String TEMP = "tmp";
    
    private static final String PPV_DATA_DIR = "dataPPV";
    private static final String PPV_NEGATIVE_TEST = "negative.txt" ;
    private static final String PPV_NEGATIVE_DESCR  = "negative_description.txt"; 
    private static final String PPV_MOTO_DATA = "MotoPPVMetadata";
    private static final String PPV_CISCO_DATA = "CiscoPPVMetadata";
    
    
    private final static long MAX_TIMEOUT_MS = 2 * 60 * 1000; //2min
    private final static int MAX_ATTEMPT = 10;
    
    private final static String PPV_SOURCE_ID ="111";
    private final static String PPV_EID = "1234567";
    private final static String PPV_PURCHASE = "2016-11-22 13:44:55";
    private final static String PPV_START = "2017-01-02 03:04:00";
    
    private final static String PPV_MOTO_SOURCE_ID = "222";
    private final static String PPV_MOTO_EID = "1234569";
    private final static String PPV_MOTO_PURCHASE = "2016-12-13 17:18:16";
    private final static String PPV_MOTO_START = "2017-05-06 07:08:00";
    
    final static String NOW = "${now}";
    final static int MAX_NUM_OF_EVENTS = 3;
    final static Logger LOG = LoggerFactory.getLogger(PPVTest.class);
    
    private static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat PUBLISH_DATE_FORMAT =
            new SimpleDateFormat("yyyyMMdd");
    private final Pattern PUBLISH_PATTERN = Pattern.compile(".*(ppv\\-(\\d{8})00\\.zdb\\.\\-?\\d+\\.\\-?\\d+)$");
        
    //TO DO: read from file ???
    private final static int[] MOTO_SOURCE_IDS = {13503, 15000};
    private final static int[] CISCO_SOURCE_IDS = {1801, 1802, 1803, 1804, 1805, 1806, 1807, 1834, 2835, 1794, 16277, 15079, 1836};
    
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        PUBLISH_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        prepareNegativeTest();
    }
        
    private String deviceId;
    private String ppvMotoDataName;
    private String ppvCiscoDataName;
    
    private static class FileInfo {
        private File file;
        private Date date;
        
        private FileInfo(File file, Date date){
            this.file = file;
            this.date = date;
        }
    }
    
    private SSHHelper helper;
    private CarouselDNCS carousel;
    private String remoteDir;
    
    
    private List<FileInfo> waitForPublisher() throws Exception {
        List<FileInfo> result = new ArrayList<>();
        
        int old = 1;
        
        long start = System.currentTimeMillis();
        int attempt = 1;
        while (true){
            
            if (System.currentTimeMillis() - start > MAX_TIMEOUT_MS)
                break;
            
            if (attempt > MAX_ATTEMPT)
                break;
            
            int count = helper.getLineCounts(remoteDir);
            
            if (count != old) {  //something is changed
                old = count;
                attempt = 0;
            }
            else 
                Thread.sleep(1_000);
            attempt++;
        }
        
        String output = helper.getExecOutput("cd " + remoteDir + "; ls -l");
        String [] tokens = output.split("\\n");
        for (String token: tokens){
            FileInfo fi = getRemoteFile(helper, token);
            if (null != fi)
                result.add(fi);
        }
        return result;
    }
    
    
    private FileInfo getRemoteFile(SSHHelper helper,String line) throws Exception{
        Matcher m = PUBLISH_PATTERN.matcher(line);
        if (!m.matches())
            return null;
        String remote = remoteDir + "/" + m.group(1);
        String local = TEMP + "/" + m.group(1);
        Date dt = PUBLISH_DATE_FORMAT.parse(m.group(2));
        helper.getFile(local, remote);
        return new FileInfo(new File(local), dt); 
    }
    
    
    @Override
    protected void generateServerXML(){
        
        CarouselHostsDNCS hostsDNCS = new CarouselHostsDNCS(properties, new CarouselType[] {CarouselType.PPV, CarouselType.DNCS}, 
            false/*isZipped*/, false/*aLocEnabled*/, false/*dalinc*/);
        
        CarouselHost host = hostsDNCS.get(0);
        helper = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());;
        remoteDir = helper.getHomeDir()+"/"+host.getLocation();
            
        carousel = new CarouselDNCS(host, helper, host.getLocation(), host.isZipped());
        //carousel.createRemoteCarouselDir();
        
        ServerXMLHelper helper=new ServerXMLHelper(properties, new HashMap<>(), hostsDNCS);
        //ServerXMLHelper helper=new ServerXMLHelper(properties);
        String uri = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_PPV_CONTEXT;
        helper.addPPVMotorolaService(uri);
        helper.addPPVCiscoService(uri);
        helper.save("server.xml");
    }

    @Override
    protected void init() {
        super.init();
        
        deviceId = properties.getDeviceId();
        ppvMotoDataName = FTConfig.getFileFromJar(PPV_DATA_DIR, PPV_MOTO_DATA);
        ppvCiscoDataName = FTConfig.getFileFromJar(PPV_DATA_DIR, PPV_CISCO_DATA);
        
        new File(TEMP).mkdirs();
        
        DBHelper.disconnect();
        DBHelper.getConnection(properties.getAMSDbConn(),properties.getAMSDbUser(),properties.getAMSDbPwd());
        
        prepareDB();
    }
    
    private void prepareLinupsSvc(){
        DBHelper.update("delete from LINEUPS_SVC");
        final String suffix = ", 2, 3, 4, 5,";
        
        for (int id: MOTO_SOURCE_IDS)
            DBHelper.update("insert into LINEUPS_SVC (ID, PARAMETERS, SERVICEID) VALUES(" + id + " , '"+id+suffix+"' , " + id + ")"); 
        
        for (int id: CISCO_SOURCE_IDS)
            DBHelper.update("insert into LINEUPS_SVC (ID, PARAMETERS, SERVICEID) VALUES(" + id + " , '"+id+suffix+"' , " + id + ")"); 
    }
    
    private void clearPPVData(){
        DBHelper.update("delete from PPV_DATA");
        DBHelper.update("delete from PPV_DATA_MOTO");
    }

    private void preparePPVData(){
        clearPPVData();
        
        String [] start1 = getWithoutSecs(PPV_START).split(" "); 
        String [] start2 = getWithoutSecs(PPV_MOTO_START).split(" ");
        DBHelper.update("insert into  PPV_DATA (ID,SOURCE_ID,DATE_VAL,TIME_VAL,EID) VALUES(?, ?, ?, ?, ?)",
            1, Integer.parseInt(PPV_SOURCE_ID), start1[0], start1[1], Integer.parseInt(PPV_EID));
        DBHelper.update("insert into  PPV_DATA_MOTO (ID,SOURCE_ID,DATE_VAL,TIME_VAL,EID) VALUES(?, ?, ?, ?, ?)",
            1, Integer.parseInt(PPV_MOTO_SOURCE_ID), start2[0], start2[1], Integer.parseInt(PPV_MOTO_EID));
    }   
    
    private void setVendorId(int vendorId){
        DBHelper.update("delete from STB_METADATA where MAC=?", Long.parseLong(deviceId, 16));
        DBHelper.update("insert into STB_METADATA (MAC, MAC_STR, REGISTR_TIME_STAMP, VENDOR_ID) VALUES(?,?,?,?)", 
            Long.parseLong(deviceId, 16) ,deviceId, System.currentTimeMillis()/1000,vendorId);
    }
    
    private void prepareDB(){
        preparePPVData();
        prepareLinupsSvc();
    }
    
    
    private List<PPVMetaData> getPPVMetaData (PPVTable table) {
        String sql ="select SOURCE_ID,SID,DATE_VAL,TIME_VAL,DURATION,FLAGS,EID,COSTPENNIES,REVIEW,PURCHASE,CANCEL,ADVERT,TITLE" +
            " FROM " + table;
        return PPVMetaData.getMetaData(DBHelper.select(sql));
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.01 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST is valid")
    @Test(priority=1)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", "1234567", NOW,
            "PurchaseTime", "1234568", NOW}
    )
    public void test_7_1_01() throws Exception {
        Assert.assertTrue(waitForAMS(AMS_TIMEOUT),"AMS is not active");
        testImpl();
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.51 [PPV] PPV EVENT NOTIFICATION REQUEST with several events for one EID: select the latest command")
    @Test(priority=2)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={"PurchaseTime", "1234567", "2017-01-01 21:00:00"},
        
        command2 = PPVCommand.DELETE,
        parameters2={/*"PurchaseTime"*/"DeleteTime", "1234567", "2017-01-01 21:00:01"}
    )
    public void test_7_1_51() throws Exception {
        testImpl();
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.52 [PPV] PPV EVENT NOTIFICATION REQUEST with several EIDs for one command PURCHASE")
    @Test(priority=3)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={"PurchaseTime", "1234567", NOW},
        
        command2 = PPVCommand.PURCHASE,
        parameters2={"PurchaseTime", "1234568", NOW}
    )
    public void test_7_1_52() throws Exception{
        testImpl();
    } 
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.53 [PPV] PPV EVENT NOTIFICATION REQUEST with several EIDs for one command MODIFY")
    @Test(priority=4)
    @IPPVEventNotification(
        command1 = PPVCommand.MODIFY,
        parameters1={
            "PurchaseTime", "413488", "2017-04-07 01:00:00",
            "PurchaseTime", "413489", "2017-04-07 02:00:00"
        }
    ) 
    public void test_7_1_53() throws Exception{
        testImpl();
    } 
    
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.54 [PPV] PPV EVENT NOTIFICATION REQUEST with several EIDs for one command DELETE")
    @Test(priority=5)
    @IPPVEventNotification(
        command1 = PPVCommand.DELETE,
        parameters1={
            "DeleteTime", "413488", "2017-04-07 01:00:00",
            "DeleteTime", "413489", "2017-04-07 02:00:00"
        }
    )        
    public void test_7_1_54() throws Exception{
        testImpl();
    }       
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.55 [PPV] PPV EVENT NOTIFICATION REQUEST with all three commands")
    @Test(priority=6)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", "413488", "2017-04-07 01:00:00",
            "PurchaseTime", "413489", "2017-04-07 02:00:00"
        },
        
        command2 = PPVCommand.MODIFY,
        parameters2={
            "PurchaseTime", "413490", "2017-04-07 03:00:00",
            "PurchaseTime", "413491", "2017-04-07 04:00:00"
        },
        
        command3 = PPVCommand.DELETE,
        parameters3={
            "DeleteTime", "413899", "2017-04-07 05:00:00",
            "DeleteTime", "413900", "2017-04-07 06:00:00"
        }
    )
    public void test_7_1_55() throws Exception{
        testImpl();
    }       
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.56 [PPV] PPV EVENT NOTIFICATION REQUEST. AMS->STB request with PURCHASE command")
    @Test(priority=7)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", "413490", "2017-04-07 01:00:00"
        }
    )
    public void test_7_1_56() throws Exception{
        testImpl();
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.57  [PPV] PPV EVENT NOTIFICATION REQUEST. AMS->STB request with MODIFY command")
    @Test(priority=8)
    @IPPVEventNotification(
        command1 = PPVCommand.MODIFY,
        parameters1={
            "PurchaseTime", "413490", "2017-04-07 01:00:00"
        }
    )
    public void test_7_1_57() throws Exception{
        testImpl();
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.58  [PPV] PPV EVENT NOTIFICATION REQUEST. AMS->STB request with DELETE command")
    @Test(priority=9)
    @IPPVEventNotification(
        command1 = PPVCommand.DELETE,
        parameters1={
            "DeleteTime", "413490", "2017-04-07 01:00:00"
        }
    )
    public void test_7_1_58() throws Exception{
        testImpl();
    }
        
    private Date getRoundDate(Date date) throws Exception{
        return PUBLISH_DATE_FORMAT.parse(PUBLISH_DATE_FORMAT.format(date));
    }
    
    private Date addDate(Date date, int field, int value){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(field, calendar.get(field) + value);
        return calendar.getTime();
    }
    
    private void negativeUpdateTest(String uri, int expected) throws Exception {
        FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
        showAMSResponse(uri, response);
        Assert.assertTrue(expected == response.getStatus(),"AMS response must be "+expected+",but found "+response.getStatus());
    }
    
    private static class HEDataSentConsumer implements IHEDataConsumer {
        private final Semaphore semaphore;
        private List<PPVMetaData> metadata;
        String rawData;
        
        HEDataSentConsumer(Semaphore semaphore){
            this.semaphore = semaphore;
        }
        
        public List<PPVMetaData> getData(){
            try {
                if (semaphore.tryAcquire(MAX_TIMEOUT_MS, TimeUnit.MILLISECONDS))
                    return metadata;
            }
            catch (InterruptedException ex){      
            }
            return null;
        } 

        @Override
        public void dataReceived(URI uri, byte[] data) {
            rawData = new String(data);
            metadata = PPVMetaData.getMetaData(rawData);
            semaphore.release();
        }
    }
    
    private List<FileInfo> publisherTestImpl(PPVType type, PPVTable table, String dataName, Date start, Date stop) throws Exception{
        return publisherTestImpl(type, table, dataName, start, stop, stop);
    }
    
    private List<FileInfo> publisherTestImpl(PPVType type, PPVTable table, String dataName, Date start, Date stopHE, Date stopCompare) throws Exception{
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20_000);
        carousel.createRemoteCarouselDir();
        LighweightCHEemulator he = null;
        
        Semaphore semaphore = new Semaphore(0);
        HEDataSentConsumer  consumer = new HEDataSentConsumer(semaphore);
        
        List<FileInfo> result = null;
        
        try {
            
            he = createCharterHEEmulator(null, HEHandlerType.PPV);
            PPVHandler handler = (PPVHandler) he.getHandler(PPVHandler.class);
            handler.registerDataSentConsumer(consumer);
            
            handler.setStartDate(start);
            handler.setStopDate(stopHE);
            
            handler.setPPVFileName(dataName);
            startCharterHEEmulator(he);
            
            String uri = properties.getPPVUpdateUri()+"?ctype="+type;
            FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
            showAMSResponse(uri, response);
            Assert.assertTrue(200 == response.getStatus(),"AMS response must be 200, but found " + response.getStatus());
            
            result = waitForPublisher();
            
            
            //verify the equivalence data sent form HE and data written to DB 
            List<PPVMetaData> heData = consumer.getData();
            showHEResponse(consumer.rawData);
            List<PPVMetaData> dbData = getPPVMetaData(table);
            List<List<PPVMetaData>> verified = verifyPPVMetadata(heData, dbData, start, stopCompare);
            
            //verify the equivalence data written to DB and written to zdb file
            for (FileInfo fi: result)
                verfiyPPVZDBMetadata(verified.get(1), fi);
            
        }
        finally {
//            Thread.sleep(60 * 60 * 1_000);
            if (null != he)
                he.stop();
        }
        return result;
    }
    
    //10
    //7.2.01 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST is valid (DAC)
    
    //11
    //7.2.02 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST is valid (DNCS)

    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.2.10 [PPV] PPV METADATA NOTIFICATION REQUEST. CVS-file processing. Generated 7 zdb-files, 1 zdb-file contains information")
    @Test(priority=10)
    public void test_7_2_10() throws Exception{
        Date start = getRoundDate(new Date());
        Date stop = addDate(start, Calendar.DAY_OF_YEAR, 1);
        List<FileInfo> fis = publisherTestImpl(PPVType.DAC, PPVTable.PPV_DATA_MOTO, ppvMotoDataName, start, stop);
        Assert.assertTrue(7 == fis.size(), "Must be 7 files, but found " + fis.size());
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.2.11 [PPV] PPV METADATA NOTIFICATION REQUEST. CVS-file processing. Generated 7 zdb-files, 7 zdb-files contains information")
    @Test(priority=11)
    public void test_7_2_11() throws Exception{
        Date start = getRoundDate(new Date());
        Date stop = addDate(start, Calendar.DAY_OF_YEAR, 7);
        List<FileInfo> fis = publisherTestImpl(PPVType.DAC, PPVTable.PPV_DATA_MOTO, ppvMotoDataName, start, stop);
        Assert.assertTrue(7 == fis.size(), "Must be 7 files, but found " + fis.size());
    }
    
    
    
    //14
    //7.2.12 [PPV] PPV METADATA NOTIFICATION REQUEST. CVS-file processing. Generated 2 zdb-files
    
    //15
    //7.3.01	 [PPV][Validation] If entering data is correct: filled ZBD file with appropriate information.
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.4.21 [PPV+Publisher] Publishing files. For MOTO/PACE")
    @Test(priority=16)
    public void test_7_4_21() throws Exception{
        Date start = getRoundDate(new Date());
        Date stop = addDate(start, Calendar.DAY_OF_YEAR, 1);
        List<FileInfo> fis = publisherTestImpl(PPVType.DAC, PPVTable.PPV_DATA_MOTO, ppvMotoDataName, start, stop);
        Assert.assertTrue(7 == fis.size(), "Must be 7 files, but found " + fis.size());
    }
    
            
    @Features(PPV)
    @Stories("Positive")
    @Description("7.4.22 [PPV+Publisher] Publishing files. For Cisco")
    @Test(priority=17)
    public void test_7_4_22() throws Exception{
        Date start = getRoundDate(new Date());
        Date stopHE = addDate(start, Calendar.DAY_OF_YEAR, 7);
        Date stopCompare = addDate(start, Calendar.DAY_OF_YEAR, 2);
        List<FileInfo> fis = publisherTestImpl(PPVType.DNCS, PPVTable.PPV_DATA, ppvCiscoDataName, start, stopHE, stopCompare);
        Assert.assertTrue(2 == fis.size(), "Must be 2 files, but found " + fis.size());
    }
            
                
    @Features(PPV)
    @Stories("Negative")
    @Description("7.1.02 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST is NOT valid")
    @Test(priority=18)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", "1234567", "2017-03-01 11:11:11"
        }
    )
    public void test_7_1_02() throws Exception{
        PPVEventNotification amsNotification = getNotificationPPVEvent();
        testNegativeImpl(properties.getPPVNotificationUri()+"2", amsNotification.toString(), 404);
    }
    
    //43 negative ppv validation tests with the same behavior 
    //dataPPV/negative.txt - ITestInfo
    //dataPPV/negative_description.txt - description
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest1() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest2() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest3() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest4() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest5() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest6() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest7() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest8() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest9() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest10() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest11() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest12() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest13() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest14() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest15() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest16() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest17() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest18() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest19() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest20() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest21() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest22() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest23() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest24() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest25() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest26() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest27() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest28() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest29() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest30() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest31() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest32() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest33() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest34() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest35() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest36() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest37() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest38() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest39() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest40() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest41() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest42() throws Exception {testNegativeImpl();};
    @Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    public void ppvValidationTest43() throws Exception {testNegativeImpl();};
    //7_1_41,42,43 are removed

    //@Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    //public void ppvValidationTest44() throws Exception {testNegativeImpl();};
    //@Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    //public void ppvValidationTest45() throws Exception {testNegativeImpl();};
    //@Test(priority=20) @Features(PPV) @Stories("Negative") @Description("") 
    //public void ppvValidationTest46() throws Exception {testNegativeImpl();};    



    @Features(PPV)
    @Stories("Negative")
    @Description("7.1.41 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent EID for command PURCHASE")
    @Test(priority=40)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={"PurchaseTime", "8888888", NOW}
    )
    public void test_7_1_41() throws Exception {
        testImpl(getNotificationPPVEvent(), 0 /*STB status*/, 200 /*AMS expected status*/, false /*no parse stb output*/, null/*find nothing in stb*/);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.1.42 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent EID for command MODIFY")
    @Test(priority=40)
    @IPPVEventNotification(
        command1 = PPVCommand.MODIFY,
        parameters1={"PurchaseTime", "8888888", NOW}
    )
    public void test_7_1_42() throws Exception {
        testImpl(getNotificationPPVEvent(), 0 /*STB status*/, 200 /*AMS expected status*/, false /*no parse stb output*/, null/*find nothing in stb*/);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.1.43 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent EID for command DELETE")
    @Test(priority=40)
    @IPPVEventNotification(
        command1 = PPVCommand.DELETE,
        parameters1={"DeleteTime", "8888888", NOW}
    )
    public void test_7_1_43() throws Exception {
        testImpl(getNotificationPPVEvent(), 0 /*STB status*/, 200 /*AMS expected status*/, false /*no parse stb output*/, null/*find nothing in stb*/);
    }
            
    
    
    private static final String NEED_MORE_INFO = "PPVN-010 Lineup, ppv or both files is out of date.\n";
    @Features(PPV)
    @Stories("Negative")
    @Description("7.1.49 [PPV] PPV EVENT NOTIFICATION REQUEST. Metadata for an EID are not found on STB. Processing STB response = 1")
    @Test(priority=50)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={"PurchaseTime", "1234567", NOW}
    )
    public void test_7_1_49() throws Exception {
        verifyResponseMessage(testImpl(getNotificationPPVEvent(), 1 /*STB status*/, 500 /*AMS expected status*/, false /*no parse stb output*/, 
            null/*find nothing in stb*/), NEED_MORE_INFO);
    }
    
    private static final String STB_UNKNOWN_ERROR = "PPVN-011 STB cannot process request.\n";
    @Features(PPV)
    @Stories("Negative")
    @Description("7.1.50 [PPV] PPV EVENT NOTIFICATION REQUEST. STB can't processing request. Processing STB response = 6")
    @Test(priority=51)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={"PurchaseTime", "1234567", NOW}
    )
    public void test_7_1_50() throws Exception {
        verifyResponseMessage(testImpl(getNotificationPPVEvent(), 6 /*STB status*/, 500 /*AMS expected status*/, false /*no parse stb output*/,
            null /*find nothing in stb*/), STB_UNKNOWN_ERROR);
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.59 [PPV] PPV EVENT NOTIFICATION REQUEST. AMS->STB request with several EIDs for one command")
    @Test(priority=59)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", "413488", "2017-04-07 01:00:00",
            "PurchaseTime", "413489", "2017-04-07 02:00:00",
            "PurchaseTime", "413490", "2017-04-07 02:00:00"}
    )
    public void test_7_1_59() throws Exception {
        testImpl();
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.60 [PPV] PPV EVENT NOTIFICATION REQUEST. AMS->STB request with all three commands")
    @Test(priority=60)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", "413488", "2017-04-07 01:00:00",
            "PurchaseTime", "413489", "2017-04-07 02:00:00",
            "PurchaseTime", "413490", "2017-04-07 02:00:00"},
        
        command2 = PPVCommand.MODIFY,
        parameters2={
            "PurchaseTime", "413491", "2017-04-07 03:00:00",
            "PurchaseTime", "413492", "2017-04-07 04:00:00"},
        
        command3 =PPVCommand.DELETE,
        parameters3={
            "DeleteTime", "413899", "2017-04-07 05:00:00",
            "DeleteTime", "413900", "2017-04-07 06:00:00"}
    )
    public void test_7_1_60() throws Exception {
        testImpl();
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.61 [PPV] PPV EVENT NOTIFICATION REQUEST. AMS get corresponding information from DB. Use PPV_DATA_MOTO for Motorola or Pace STB")
    @Test(priority=61)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", PPV_MOTO_EID, PPV_MOTO_PURCHASE}
    )
    public void test_7_1_61() throws Exception {
        setVendorId(30);
        preparePPVData();
        testImpl(getNotificationPPVEvent(), 0 /*stb response*/, 200 /*ams response*/, true /*verify stb*/,
            new HashMap(){{put("StartTime",PPV_MOTO_START); put("ServiceId",PPV_MOTO_SOURCE_ID);}});
    }
    
    @Features(PPV)
    @Stories("Positive")
    @Description("7.1.62 [PPV] PPV EVENT NOTIFICATION REQUEST. AMS get corresponding information from DB. Use PPV_DATA for other STB (not Motorola or Pace)")
    @Test(priority=62)
    @IPPVEventNotification(
        command1 = PPVCommand.PURCHASE,
        parameters1={
            "PurchaseTime", PPV_EID, PPV_PURCHASE}
    )
    public void test_7_1_62() throws Exception {
        setVendorId(10);
        preparePPVData();
        testImpl(getNotificationPPVEvent(), 0 /*stb response*/, 200 /*ams response*/, true /*verify stb*/,
            new HashMap(){{put("StartTime",PPV_START); put("ServiceId",PPV_SOURCE_ID);}});
    }
    
    private String getWithoutSecs(String date){
        return date.substring(0, date.length()-3); //remove the last :00
    }
    
            
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.2.03 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST cannot be properly parsed")
    @Test(priority=70)
    public void test_7_2_03() throws Exception{
        negativeUpdateTest(properties.getPPVUpdateUri()+"?type=DNCS", 400);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.2.04 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST has parameter with empty value")
    @Test(priority=71)
    public void test_7_2_04() throws Exception{
        negativeUpdateTest(properties.getPPVUpdateUri()+"?ctype=", 400);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.2.05 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST has parameter with nonexistent value")
    @Test(priority=72)
    public void test_7_2_05() throws Exception{
        negativeUpdateTest(properties.getPPVUpdateUri()+"?ctype=TEST", 400);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.2.06 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST with numbers in value of parameter \"ctype\"")
    @Test(priority=73)
    public void test_7_2_06() throws Exception{
        negativeUpdateTest(properties.getPPVUpdateUri()+"?ctype=12345", 400);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.2.07 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST with special characters in value of parameter \"ctype\"")
    @Test(priority=74)
    public void test_7_2_07() throws Exception{
        negativeUpdateTest(properties.getPPVUpdateUri() + "?ctype=" + URLEncoder.encode("!@#$%^","UTF-8"), 400);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.2.08 [PPV][Validation] PPV METADATA NOTIFICATION REQUEST without value of parameter \"ctype\"")
    @Test(priority=75)
    public void test_7_2_08() throws Exception{
        negativeUpdateTest(properties.getPPVUpdateUri()+"?", 400);
    }
    
    
    private void ppvValidationTestImpl(String dataName) throws Exception{
        ppvValidationTestImpl(dataName, 7, 7);
    }
    
    private void ppvValidationTestImpl(String dataName, Integer daysToAddHE, Integer daysToAddCompare) throws Exception{
        String name = FTConfig.getFileFromJar(PPV_DATA_DIR, dataName);
        Date start = getRoundDate(new Date());
        Date stopHE = null;
        if (null != daysToAddHE)
            stopHE = addDate(start, Calendar.DAY_OF_YEAR, daysToAddHE);
        Date stopCompare = null;
        if (null != daysToAddCompare)
            stopCompare = addDate(start, Calendar.DAY_OF_YEAR, daysToAddCompare);
        List<FileInfo> fis = publisherTestImpl(PPVType.DAC, PPVTable.PPV_DATA_MOTO, name, start, stopHE, stopCompare);
        Assert.assertTrue(7 == fis.size(), "Must be 7 files, but found " + fis.size());
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.02 [PPV][Validation] If entering SOURCEID data is empty or invalid: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=82)
    public void test_7_3_02() throws Exception{
        ppvValidationTestImpl("moto_bad_2");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.03 [PPV][Validation] If entering SOURCEID data is not more then zero: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=83)
    public void test_7_3_03() throws Exception{
        ppvValidationTestImpl("moto_bad_3");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.04 [PPV][Validation] If entering DATE data is empty or invalid: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=84)
    public void test_7_3_04() throws Exception{
        ppvValidationTestImpl("moto_bad_4");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.05 [PPV][Validation] If entering DATE data is not within the range: skip the record, do not log the error, and continue processing the input feed.")
    @Test(priority=85)
    public void test_7_3_05() throws Exception{
        ppvValidationTestImpl(PPV_MOTO_DATA, null, 7);
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.06 [PPV][Validation] If entering TIME data is empty or invalid: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=86)
    public void test_7_3_06() throws Exception{
        ppvValidationTestImpl("moto_bad_6");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.07 [PPV][Validation] If entering LENGTH data is empty or invalid: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=87)
    public void test_7_3_07() throws Exception{
        ppvValidationTestImpl("moto_bad_7");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.08 [PPV][Validation] If entering LENGTH data is not more than zero: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=88)
    public void test_7_3_08() throws Exception{
        ppvValidationTestImpl("moto_bad_8");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.09 [PPV][Validation] If entering EID data is empty or invalid: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=89)
    public void test_7_3_09() throws Exception{
        ppvValidationTestImpl("moto_bad_9");
    }
    
    @Features(PPV)
    @Stories("Negative")
    @Description("7.3.10 [PPV][Validation] If entering EID data is not more than zero: skip the record, log the error, and continue processing the input feed.")
    @Test(priority=90)
    public void test_7_3_10() throws Exception{
        ppvValidationTestImpl("moto_bad_10");
    }
    
    @Features(PPV)
    @Stories("AMS log")
    @Description("PPV AMS log")
    @Test(priority=100)
    public void getPPVTestAMSLog() throws Exception {
        getServerXml();
        stopAndgetAMSLog(true);
    } 
    
    void testNegativeImpl() throws Exception{
        TestInfo test = getNotificationPPVEventJSON();
        testNegativeImpl(test.json, test.expected);
    }
    
    void testNegativeImpl(String notification, int expected) throws Exception{
        testNegativeImpl(properties.getPPVNotificationUri(), notification, expected);
    }
    
    void testNegativeImpl(String uri, String notification, int expected) throws Exception {
        FTHttpUtils.HttpResult response=FTHttpUtils.doPost(uri, notification);
        showAMSNotification(notification);
        showAMSResponse(uri, response);
        Assert.assertTrue(expected == response.getStatus(),"AMS response must be "+expected+", but found "+response.getStatus());
    }
    
    void testImpl() throws Exception {
        testImpl(getNotificationPPVEvent());
        
    }
    
    //DEBUG ONNLY testImpl(getNotificationPPVEventJSON());
    void testImpl(String notification) throws Exception{
        STBExecutorThread stb = null;
        try{
            stb = new STBExecutorThread(0);
            stb.start();
        
            String uri = properties.getPPVNotificationUri();
            FTHttpUtils.HttpResult response=FTHttpUtils.doPost(uri, notification);
            stb.join(MAX_TIMEOUT_MS);
        
            showAMSNotification(notification);
            showAMSResponse(uri, response);
        }
        finally {
            if (null != stb)
                stb.stopEmulator();
        }
    }
    
    void verifyResponseMessage(FTHttpUtils.HttpResult response, String expected){
        String result = (String)response.getEntity();
        Assert.assertTrue(expected.equalsIgnoreCase(result), "Found: \"" + result+"\". Expected: \""+expected+"\".");
    }
    
    FTHttpUtils.HttpResult testImpl(PPVEventNotification notification) throws Exception{
        return testImpl(notification, 0, 200, true, null);
    }
    
    FTHttpUtils.HttpResult testImpl(PPVEventNotification notification, int stbStatus, int expected, boolean verifyStb,
        Map<String,String> toFindInStb) throws Exception{
        STBExecutorThread stb = null;
        FTHttpUtils.HttpResult response =  null;
        
        try{
            stb = new STBExecutorThread(stbStatus);
            stb.start();
        
            String uri = properties.getPPVNotificationUri();
            response=FTHttpUtils.doPost(uri, notification.toString());
            stb.join(MAX_TIMEOUT_MS);
        
            PPVEventNotification stbNotification = verifyStb ? new PPVEventNotification(stb.getData()) : null;
        
            showAMSNotification(notification);
            if (verifyStb)
                showSTBNotification(stbNotification);
            showAMSResponse(uri, response);
        
            Assert.assertTrue(expected == response.getStatus(),"AMS response must be " + expected+", but found " + response.getStatus());
            
            if (verifyStb){
                Assert.assertTrue(PPVEventNotification.equal(notification, stbNotification, false/*descending*/),
                    "AMS and STB response must be the same");
                if (null != toFindInStb){
                    Iterator<String> it =toFindInStb.keySet().iterator();
                    while (it.hasNext()){
                        String key = it.next();
                        String value = toFindInStb.get(key);
                        Assert.assertTrue(stbNotification.contains(key,value),"The pair \""+key+"\"=\""+value+"\" must present in stb request");
                    }
                }
            }
        }
        finally {
            if (null != stb)
                stb.stopEmulator();
        }
        return response;
    }
    
    
    @Attachment(value = "AMS Response", type = "text/plain")
    protected String showAMSResponse(String uri, FTHttpUtils.HttpResult response){
        return uri+"\nResponse:"+response.getStatus()+"\n\n"+response.getEntity();
    }
    
    @Attachment(value = "AMS Event Notification", type = "text/plain")
    protected String showAMSNotification(PPVEventNotification event) {
        return event.toString();
    }
    
    @Attachment(value = "AMS Event Notification", type = "text/plain")
    protected String showAMSNotification(String notification) {
        return notification;
    }
    
    @Attachment(value = "STB Event Notification", type = "text/plain")
    protected String showSTBNotification(PPVEventNotification event) {
        return event.toString();
    }
    
    @Attachment(value = "HE Response", type = "text/csv")
    protected String showHEResponse(String response) {
        return response;
    }
    
    @Attachment(value = "ZDB file", type = "text/csv")
    protected String showZDB(String name, List<PPVMetaDataZDB> zdb){
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" :\n\n").
            append("service_id|showing_time|duration|purchase|preview|title|eid");
        for (PPVMetaDataZDB z: zdb)
            sb.append("\n |").append(z);
        return sb.toString();
    }
    
    static class TestInfo{
        String json;
        int expected;
    }
    
    
    private PPVEventNotification getNotificationPPVEvent(){
        PPVEventNotification event = null;
        try{
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            for (StackTraceElement element : elements) {
                String name = element.getMethodName();
                Method method;
                try{
                    method = PPVTest.class.getDeclaredMethod(name);
                }
                catch(NoSuchMethodException ex){
                    continue;
                }
                method.setAccessible(true);
                Annotation annotation = method.getAnnotation(IPPVEventNotification.class);
                if (annotation instanceof IPPVEventNotification) {
                    IPPVEventNotification ien = (IPPVEventNotification) annotation;

                    PPVEvents ppvEvents = null;

                    for (int i = 1; i <= MAX_NUM_OF_EVENTS; i++) {
                        Method commandM = IPPVEventNotification.class.getMethod("command" + i);
                        Method paramsM = IPPVEventNotification.class.getMethod("parameters" + i);

                        PPVCommand ppvCommand = (PPVCommand) commandM.invoke(ien);
                        String[] params = (String[]) paramsM.invoke(ien);

                        if (PPVCommand.UNKNOWN == ppvCommand) {
                            break;
                        }

                        PPVParameters ppvParams = null;
                        int index = 0;
                        while (index < params.length) {
                            if (null == ppvParams) {
                                ppvParams = new PPVParameters();
                            }
                            PPVParameterType type = PPVParameterType.valueOf(params[index++]);
                            String eid = params[index++];
                            String tm = params[index++];
                            if (NOW.equals(tm)) {
                                tm = PPVUtils.getDate();
                            }
                            PPVParameter ppvParam = new PPVParameter(type, eid, tm);
                            ppvParams.addParameter(ppvParam);
                        }

                        PPVEvent ppvEvent = new PPVEvent(ppvCommand, ppvParams);
                        if (null == ppvEvents) {
                            ppvEvents = new PPVEvents();
                        }
                        ppvEvents.addEvent(ppvEvent);

                    }
                    event = new PPVEventNotification(deviceId, ppvEvents);
                    return event;
                }
            }
        }
        catch(Exception ex){
            LOG.error("getNotificationPPVEvent error: {}",ex.getMessage());
        }
        return event;
    }
    
    
    class STBExecutorThread extends Thread{
        private byte[] data;
        STBEmulatorFT emulator;
        private int status = 0;
        
        STBExecutorThread(int status){
            this.status = status;
        }
        
        
        @Override
        public void run(){
            try {
                emulator =new STBEmulatorFT(HandlerType.PPVNotifications);
                
                PPVNotificationListener listener = (PPVNotificationListener)emulator.getMessageHandler();
                listener.setPredifinedStatus(status);
            
                emulator.start();
                data = emulator.getData();
            }
            catch(Exception ex){
                LOG.error("STBExecutionThread exception: {}",ex.getMessage());
            }
        }
        
        byte[] getData(){
            return data;
        }
        
        void stopEmulator(){
            if (null != emulator)
                emulator.stop();
        }
    }
    
    
    static private List<String> readDescription(File file){
        List<String> result = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(file))){
            String line;
            while (null != (line = in.readLine()))
                if (!line.startsWith("#") && !line.trim().equals(""))
                    result.add(line);
        }
        catch(Exception ex){
            LOG.error("readDescription exception: {}", ex.getMessage());
        }
        return result;
    }
    
    static private List<TestInfo> readTest(File file){
        List<TestInfo> result = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(file))){
            String line;
            while (null != (line = in.readLine())){
                if (!line.startsWith("#") && !line.trim().equals("")){
                    String[] tokens = line.split("\\|");
                    TestInfo info = new TestInfo();
                    info.expected = Integer.parseInt(tokens[0]);
                    info.json = tokens[1];
                    result.add(info);
                }
            }
        }
        catch(Exception ex){
            LOG.error("readTestInfo exception: {}", ex.getMessage());
        }
        return result;
        
    }

    
    static private void replaceTestAnnotation(final Class clazz, final String methodName, final String descr){
        try{
            Method method = clazz.getDeclaredMethod(methodName);
            method.setAccessible(true);
            Description description = method.getAnnotation(Description.class);
            if (null != description){
                Description newDescription = new NewDescription(description, descr);
                Class<?> executableClass = Class.forName("java.lang.reflect.Executable");
                Field field = executableClass.getDeclaredField("declaredAnnotations");
                field.setAccessible(true);
                Map<Class<? extends Annotation>, Annotation> annotations = (Map<Class<? extends Annotation>, Annotation>) field.get(method);
                annotations.put(Description.class, newDescription);
            }
        }
        catch(Exception ex){
            LOG.error("replaceDescription exception: {}", ex.getMessage());
        } 
    }
    
    
    private static class NewDescription implements Description{
        private final String value;
        private final DescriptionType type;
        private final Class<? extends Annotation> annotationType;

        NewDescription(Description description, String value){
            this.value = value;
            this.type = description.type();
            this.annotationType = description.annotationType();
        }
        
        @Override
        public String value() {
            return value;
        }

        @Override
        public DescriptionType type() {
            return type;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return annotationType;
        }
    }
    
    
    
    static private void prepareNegativeTest(){
                
        File fileD = new File(FTConfig.getFileFromJar(PPV_DATA_DIR, PPV_NEGATIVE_DESCR));
        File fileT = new File(FTConfig.getFileFromJar(PPV_DATA_DIR, PPV_NEGATIVE_TEST));
        
        negativeDescriptions = readDescription(fileD);
        negativeTests = readTest(fileT);
        
        Assert.assertTrue(negativeDescriptions.size() == negativeTests.size(), 
            "Tests and tests descriptions must have the same size, but " + negativeDescriptions.size() + "!=" + negativeTests.size());
        
        Assert.assertTrue(NEGATIVE_TEST_COUNT == negativeDescriptions.size(), 
            "Must be " + NEGATIVE_TEST_COUNT + " negative tests, but found " + negativeDescriptions.size());
        
        Iterator<String> itD = negativeDescriptions.iterator();
        for (int i=1; i<=NEGATIVE_TEST_COUNT; i++)
            replaceTestAnnotation(PPVTest.class, TEST_NAME_PREFIX+i, itD.next());
    }
    
    static private final int NEGATIVE_TEST_COUNT = 43; //46;
    static private final String TEST_NAME_PREFIX = "ppvValidationTest";
    static private final String EMPTY_STRING_MARKER = ".";
    static private List<TestInfo> negativeTests;
    static private List<String> negativeDescriptions;
    
    
    private TestInfo getNotificationPPVEventJSON(){
        try{
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            for (StackTraceElement element : elements) {
                String name = element.getMethodName();
                if (!name.startsWith(TEST_NAME_PREFIX))
                    continue;
                Method method;
                try{
                    method = PPVTest.class.getDeclaredMethod(name);
                }
                catch(NoSuchMethodException ex){
                    continue;
                }
                method.setAccessible(true);
                int index = Integer.parseInt(name.substring(TEST_NAME_PREFIX.length()))-1;
                TestInfo test = negativeTests.get(index);
                test.json = test.json.replaceAll("\\$\\{MAC\\}", properties.getDeviceId());
                if (EMPTY_STRING_MARKER.equals(test.json))
                    test.json = "";
                return test;
            }
        }
        catch(Exception ex){
            LOG.error("getNotificationPPVEventJSON error: {}",ex.getMessage());
        }
        return null;
    }
    
    //dbMetaData must be sorted before calling this method
    private void verfiyPPVZDBMetadata(List<PPVMetaData> dbMetaData, FileInfo info){
        List<PPVMetaDataZDB> zdb  = PPVMetaDataZDB.getMetaDataZDB(info.file.getAbsolutePath());
        showZDB(info.file.getName(), zdb);
        Collections.sort(zdb, new PPVMetaDataZDBComparator());
        
        Date start = info.date;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
        Date stop = calendar.getTime();
        
        List<PPVMetaData> filtered = PPVCompareUtils.filterByDate(dbMetaData, start, stop);
        
        Assert.assertTrue(filtered.size() == zdb.size(), "PPV metadata must have the same size in DB and " + info.file.getName() + 
            " file, but " + filtered.size() +" != " + zdb.size());
        
        Iterator<PPVMetaData> it1 = filtered.iterator();
        Iterator<PPVMetaDataZDB> it2 = zdb.iterator();
        while (it1.hasNext()){
            PPVMetaData data1 = it1.next();
            PPVMetaDataZDB data2 = it2.next();
            Assert.assertTrue(PPVCompareUtils.equal(data1, data2), "PPV metadata in DB and " + info.file.getName() + "file must be the same, but found: " + 
                data1 +" and "+data2);
        }
    }
    
    
    private List<List<PPVMetaData>>  verifyPPVMetadata(List<PPVMetaData> m1, List<PPVMetaData> m2, Date from, Date to){
        
        List<PPVMetaData> tmp1 = new ArrayList<>();
        for (PPVMetaData m: m1){
            if (m.isValid(from, to))
                tmp1.add(m);
        }
        
        List<PPVMetaData> tmp2 = new ArrayList<>();
        for (PPVMetaData m: m2){
            if (m.isValid(from, to))
                tmp2.add(m);
        }
        
        Assert.assertTrue(tmp1.size() == tmp2.size(), "PPV Metadata must have the same size, but " + tmp1.size() + " != " + tmp2.size());
        
        PPVMetaDataComparator comparator = new PPVMetaDataComparator();
        Collections.sort(tmp1, comparator);
        Collections.sort(tmp2, comparator);
        
        Iterator<PPVMetaData> it1 = tmp1.iterator();
        Iterator<PPVMetaData> it2 = tmp2.iterator();
        
        while (it1.hasNext()){
            PPVMetaData d1 = it1.next();
            PPVMetaData d2 = it2.next();
            boolean ok = d1.equals(d2);
            if (!ok)
                Assert.assertTrue(false, "PPV Metadata must be the same, but found " + d1 + " and " + d2);
        }
        
        List<List<PPVMetaData>> result = new ArrayList<>();
        result.add(tmp1);
        result.add(tmp2);
        return result;
    }
}