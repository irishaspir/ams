package com.zodiac.ams.charter.tests.activityControl;

import com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils;
import com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils;
import com.zodiac.ams.charter.http.helpers.activityControl.ActivityControlJsonDataUtils;
import com.zodiac.ams.charter.tests.Steps;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class StepsForActivityControl extends Steps{

    protected ActivityControlJsonDataUtils activityControlJsonDataUtils = new ActivityControlJsonDataUtils();

    @BeforeMethod
    public void getStartTestTime(){
        startTestTime = getCurrentTimeTest();
    }

    @BeforeClass
    public void fillDB(){
        clean();
     //   STBMetadataUtils.insertToSTBMetadata(Long.parseLong(MAC_NUMBER_1), DEVICE_ID_NUMBER_1);
     //   STBLastActivityUtils.insertToSTBLastActivity(MAC_NUMBER_1, 1495112237883L);
    }

    @AfterClass
    public void cleanDB(){
        clean();
    }

    @Step
    private void clean(){
        new STBMetadataUtils().deleteAll();
        new STBLastActivityUtils().deleteAll();
    }

    @Step
    public List<String> readAMSLog() {
        return parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT);
    }
}
