package com.zodiac.ams.charter.tests.ft.talkingguide;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.FeatureList.TALKING_GUIDE;

public class TalkingGuideTestBadConfig extends TalkingGuideTestImpl {
    
    private static final String BAD_CONF_NAME = "bad_tts.cfg";
    private static final String SOUND_NAME = "audio.wav";
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties,~ServerXMLHelper.TALKING_GUIDE_MASK);
        String remote = executor.getHomeDir()+"/"+BAD_CONF_NAME;
        String local = getName(BAD_CONF_NAME);
        executor.putFile(local, remote);
        createRemoteCacheDir();
        helper.addTalkingGuideService(properties.getLocalHost(),properties.getTTSPort(),remote,getRemoteCacheDir());
        helper.save("server.xml");
    }  
    
    
    @Features(TALKING_GUIDE)
    @Stories("Positive")
    @Description("Send request to AMS to convert text with macroses. Configuration file is corrupted")
    @Test(priority=1)  
    public void simpleTextWithMacrosCorruptedConf() throws Exception{
        positiveTestImpl("{tts1} {tts2} {tts3} "+System.currentTimeMillis(),getName(SOUND_NAME),getName(BAD_CONF_NAME));
    }
    
    @Features(TALKING_GUIDE)
    @Stories("AMS logs")
    @Description("TalkingGuideTestBadConfig AMS log")
    @Test(priority=2)
    public void getTalkingGuideTestBadConfigAMSLog() throws Exception {
        stopAndgetAMSLog(true);
    }
}
