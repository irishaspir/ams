package com.zodiac.ams.charter.tests.settings.che;


import com.zodiac.ams.charter.emuls.che.settings.SettingsSampleBuilder;
import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getAllKeysFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.responseJson;
import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostSettingsAllList;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_CHE;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendSetSettingsFromCHEWithTimeoutTest extends StepsForSettings {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE+SET_SETTINGS_FROM_CHE)
    @Description("1.4.11 Send POST setSettings from CHE to AMS, AMS send settings to STB," +
            " settings are valid, STB is unavailable, settings haven't saved in DB, receive error 500 ")
    @TestCaseId("12345")
    @Test()
    public void sendSetSettingsWithTimeoutReceiveError500() {
        
        String mac=MAC_NUMBER_2;
        new SettingsSampleBuilder(getDeviceIdByMac(mac), "./dataCHE", "/sample1.xml").messageBuild();
        new GetSettingsBeforeAfter().setDefaultValue(options, mac);
        ArrayList<SettingsOption> options = delete55And59And60Keys(allSettings());
        runSTBWithTimeoutOfResponse();
        runCHESettings();
        List<String> changedKeys = changeKeyValue(options, mac);
        startTestTime = getCurrentTimeTest();
        String response = sendPostSettingsAllList(getDeviceIdByMac(mac));

        createAttachment(responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, ERROR_500);
        assertJsonEquals(responseJson, jsonDataUtils.error500TimeoutBox(getDeviceIdByMac(mac), SettingsPostHelper.getNum(responseJson)));
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPattern(options), INCORRECT_SETTINGS);
        assertEquals(getAllKeysFromSettingsSTBDataV2(mac), changedKeys, wrongValueInDB);
    }

    @AfterMethod
    public void stop() {
        stopSTB();
        stopCHESettings();
    }
}
