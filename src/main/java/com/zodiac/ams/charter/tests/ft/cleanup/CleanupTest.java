package com.zodiac.ams.charter.tests.ft.cleanup;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.CassandraHelper;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.cleanup.CleanupRequest;
import com.zodiac.ams.charter.services.ft.cleanup.CleanupResponse;
import com.zodiac.ams.charter.services.ft.defent.dsg.ConnectionMode;
import com.zodiac.ams.charter.services.ft.defent.dsg.DSGMessage30;
import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.charter.tests.ft.defent.DefEntDSGTest;
import com.zodiac.ams.common.bd.DBHelper;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.io.ByteArrayInputStream;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.tests.FeatureList.CLEANUP;

public class CleanupTest extends FuncTest{
    
    private static final String FAIL_STATUS = "FAIL";
    private static final String OK_STATUS = "OK";
    private static final String WRONG_DEVICE_ID = "wrong device id format: %s";
    private static final String HTTP_PARAMS = "req=CleanupStbData";
    
    private String deviceId;
    private Session cassandra;
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.addCleanupService();
        
        //DSG cache testing 
        String uriDent = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_DENT_NEW_CONTEXT;
        String uriSettings = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_ERROR_REPORTING_CONTEXT;
        helper.addDSGService(uriDent, uriSettings);
        
        helper.save("server.xml");
    }
    
     @Override
    protected void init() {
        super.init();
        
        deviceId = properties.getDeviceId();
          
        DBHelper.disconnect();
        DBHelper.getConnection(properties.getAMSDbConn(),properties.getAMSDbUser(),properties.getAMSDbPwd());
        
        cassandra = CassandraHelper.openSession();
    }
    
    private static enum Tables{
        ALL, SETTINGS_STB, STB_METADATA, APP_CHAN, DVR_HISTORY, CHARTER_SUBS, MAC_IP, DVR_STB, DVR_RECORDINGS, DVR_UNSYNC,
        DVR_MIGR, DVR_MIGR_CHE, DVR_MIGR_STB;
    }
    
    private static final String SETTINGS_STB = "SELECT COUNT(*) FROM SETTINGS_STB WHERE ID IN";
    private static final String SETTINGS_STBDATA_V2 = "SELECT COUNT(*) FROM SETTINGS_STBDATA_V2 WHERE STB_ID IN";
    private static final String STB_LAST_ACTIVITY = "SELECT COUNT(*) FROM STB_LAST_ACTIVITY WHERE MAC IN";
    private static final String STB_METADATA = "SELECT COUNT(*) FROM STB_METADATA WHERE MAC IN";
    private static final String APP_CHAN = "SELECT COUNT(*) FROM APP_CHAN_AUTH_SERVICES  WHERE MAC IN";
    private static final String DVR_HISTORY = "SELECT COUNT(*) FROM DVR_HISTORY WHERE STB_ID IN";
    private static final String CHARTER_SUBS = "SELECT COUNT(*) FROM CHARTER_SUBSCRIPTION WHERE MAC IN";
    private static final String MAC_IP = "SELECT COUNT(*) FROM MAC_IP WHERE MAC IN";
    private static final String DVR_STB = "SELECT COUNT(*) FROM DVR_STB WHERE MAC IN";
    private static final String DVR_RECORDINGS = "SELECT COUNT(*) FROM DVR_RECORDINGS WHERE MAC IN";
    private static final String DVR_UNSYNC = "SELECT COUNT(*) FROM DVR_UNSYNC_STB WHERE MAC IN";
    private static final String DVR_MIGR = "SELECT COUNT(*) FROM DVR_MIGR_CHE_MTD_REQ WHERE STBID IN";
    private static final String DVR_MIGR_CHE = "SELECT COUNT(*) FROM DVR_MIGR_FOR_CHE WHERE STBID IN";
    private static final String DVR_MIGR_STB = "SELECT COUNT(*) FROM DVR_MIGR_FOR_STB WHERE STBID IN";
    
    
    private <T> String getString(T [] ids){
        StringBuilder sb=new StringBuilder();
        sb.append("(");
        boolean first = true;
        for (T id: ids){
            sb.append(first ? "" : " , ");
            if (id instanceof String)
                sb.append("'");
            sb.append(id);
            if (id instanceof String)
                sb.append("'");
            first = false;
        }
        sb.append(")");
        return sb.toString();
    }
    
    
    //select .... where FIELD IN  -> select ... where FIELD IN (id1, id2, id3)
    private <T> String getSql(String prefix, T[] ids){
        StringBuilder sb = new StringBuilder(prefix);
        sb.append(" ");
        sb.append(getString(ids));
        return sb.toString();
    }
    
    private String getId(long id){
        return String.format("%012X", id); 
    }
    
    private String[] getIds(Long [] ids){
        String[] idsS = new String[ids.length];
        for (int i=0; i<idsS.length; i++)
            idsS[i] = getId(ids[i]); 
        return idsS;
    }
    
    private void cleanDB(Tables tables, String id){
        cleanDB(tables, new Long[] {getDeviceId(id)});
    }
     
    private void cleanDB(Tables tables, long id){
        cleanDB(tables, new Long[] {id});
    }
    
    private void cleanDB(Tables tables, Long [] ids){
        if (null == tables)
            return;
        
        String [] idsS = getIds(ids);
        
        switch (tables){
            case ALL:
            case SETTINGS_STB:
                DBHelper.update(getSql("DELETE FROM SETTINGS_STBDATA_V2 WHERE STB_ID IN", ids));
                DBHelper.update(getSql("DELETE FROM SETTINGS_STB WHERE ID IN", ids));
                DBHelper.update(getSql("DELETE FROM STB_LAST_ACTIVITY WHERE MAC IN", ids));
                if (Tables.SETTINGS_STB == tables)
                    break;
            case STB_METADATA:
                DBHelper.update(getSql("DELETE FROM STB_METADATA WHERE MAC IN" , ids));
                if (Tables.STB_METADATA == tables)
                    break;
            case APP_CHAN:
                DBHelper.update(getSql("DELETE FROM APP_CHAN_AUTH_SERVICES WHERE MAC IN", ids));
                if (Tables.APP_CHAN == tables)
                    break;
            case DVR_HISTORY:
                DBHelper.update(getSql("DELETE FROM DVR_HISTORY WHERE STB_ID IN", ids));
                if (Tables.DVR_HISTORY == tables)
                    break;
            case CHARTER_SUBS:
                DBHelper.update(getSql("DELETE FROM CHARTER_SUBSCRIPTION WHERE MAC IN", idsS));
                if (Tables.CHARTER_SUBS == tables)
                    break;
            case MAC_IP:
                DBHelper.update(getSql("DELETE FROM MAC_IP WHERE MAC IN", ids));
                if (Tables.MAC_IP == tables)
                    break;
            case DVR_STB:
                DBHelper.update(getSql("DELETE FROM DVR_STB WHERE MAC IN", ids));
                if (Tables.DVR_STB == tables)
                    break;
            case DVR_RECORDINGS:
                DBHelper.update(getSql("DELETE FROM DVR_RECORDINGS WHERE MAC IN", ids));
                if (Tables.DVR_RECORDINGS == tables)
                    break;
            case DVR_UNSYNC:
                DBHelper.update(getSql("DELETE FROM DVR_UNSYNC_STB WHERE MAC IN", ids));
                if (Tables.DVR_UNSYNC == tables)
                    break;
            case DVR_MIGR:
                DBHelper.update(getSql("DELETE FROM DVR_MIGR_CHE_MTD_REQ WHERE STBID IN", ids));
                if (Tables.DVR_MIGR == tables)
                    break;
            case DVR_MIGR_CHE:
                DBHelper.update(getSql("DELETE FROM DVR_MIGR_FOR_CHE WHERE STBID IN", ids));
                if (Tables.DVR_MIGR_CHE == tables)
                    break;
            case DVR_MIGR_STB:
                DBHelper.update(getSql("DELETE FROM DVR_MIGR_FOR_STB WHERE STBID IN", ids));
                if (Tables.DVR_MIGR_STB == tables)
                    break;
        }
    }
    
    
    private void prepareDB(Tables tables, Long [] ids){
        if (null == tables)
            return;
        
        String [] idsS = getIds(ids);
        
        cleanDB(tables, ids);
        
        switch (tables){
            case ALL:
            case SETTINGS_STB:
                for (long id: ids){
                    DBHelper.update(String.format("INSERT INTO SETTINGS_STBDATA_V2(STB_ID) VALUES(%d)", id));
                    DBHelper.update(String.format("INSERT INTO SETTINGS_STB(ID,STBID) VALUES(%d,'%s')", id, getId(id)));
                    DBHelper.update(String.format("INSERT INTO STB_LAST_ACTIVITY(MAC,LAST_ACT_TIME_STAMP) VALUES(%d,%d)", id, System.currentTimeMillis()));
                }
                if (Tables.SETTINGS_STB == tables)
                    break;
            case STB_METADATA:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO STB_METADATA(MAC,MAC_STR,REGISTR_TIME_STAMP) VALUES(%d,'%s',%d)", 
                        id, getId(id), System.currentTimeMillis()));
                if (Tables.STB_METADATA == tables)
                    break;
            case APP_CHAN:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO APP_CHAN_AUTH_SERVICES(MAC,MAC_STR) VALUES(%d,'%s')", id, getId(id)));
                if (Tables.APP_CHAN == tables)
                    break;
            case DVR_HISTORY:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_HISTORY(STB_ID,RECORDING_ID) VALUES(%d,%d)", id, id));
                if (Tables.DVR_HISTORY == tables)
                    break;
            case CHARTER_SUBS:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO CHARTER_SUBSCRIPTION(MAC) VALUES('%s')", getId(id)));
                if (Tables.CHARTER_SUBS == tables)
                    break;
            case MAC_IP:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO MAC_IP(MAC) VALUES(%d)", id));
                if (Tables.MAC_IP == tables)
                    break;
            case DVR_STB:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_STB(MAC,MAC_STR) VALUES(%d,'%s')", id, getId(id)));
                if (Tables.DVR_STB == tables)
                    break;
            case DVR_RECORDINGS:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_RECORDINGS(MAC,RECORDING_ID) VALUES(%d,%d)", id, id));
                if (Tables.DVR_RECORDINGS == tables)
                    break;
            case DVR_UNSYNC:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_UNSYNC_STB(MAC) VALUES(%d)", id));
                if (Tables.DVR_UNSYNC == tables)
                    break;
            case DVR_MIGR:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_MIGR_CHE_MTD_REQ(STBID) VALUES(%d)", id));
                if (Tables.DVR_MIGR == tables)
                    break;
            case DVR_MIGR_CHE:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_MIGR_FOR_CHE(UUID,STBID) VALUES('%s',%d)", getId(id), id));
                if (Tables.DVR_MIGR_CHE == tables)
                    break;
            case DVR_MIGR_STB:
                for (long id: ids)
                    DBHelper.update(String.format("INSERT INTO DVR_MIGR_FOR_STB(STBID) VALUES(%d)", id));
                if (Tables.DVR_MIGR_STB == tables)
                    break;
        }
    }
    
    private <T> void verifyDB(String prefix, T [] ids, long expected){
        String sql = getSql(prefix, ids); 
        long count = ((Number)DBHelper.selectSingle(sql)).longValue();
        Assert.assertTrue(count == expected, "SQL query: " + sql + " Actual: " + count + " Expected:" + expected);
    }
    
    
    private void verifyDB(Tables tables, Long[] ids, long expected){
        if (null == tables)
            return;
        
        String [] idsS = getIds(ids);
        switch(tables){
            case ALL:
            case SETTINGS_STB:
                verifyDB(SETTINGS_STBDATA_V2, ids, expected);
                verifyDB(SETTINGS_STB, ids, expected);
                verifyDB(STB_LAST_ACTIVITY, ids, expected);
                if (Tables.SETTINGS_STB == tables)
                    break;
            case STB_METADATA:
                verifyDB(STB_METADATA, ids, expected);
                if (Tables.STB_METADATA == tables)
                    break;
            case APP_CHAN:
                verifyDB(APP_CHAN, ids, expected);
                if (Tables.APP_CHAN == tables)
                    break;
            case DVR_HISTORY:
                verifyDB(DVR_HISTORY, ids, expected);
                if (Tables.DVR_HISTORY == tables)
                    break;
            case CHARTER_SUBS:
                verifyDB(CHARTER_SUBS, idsS, expected);
                if (Tables.CHARTER_SUBS == tables)
                    break;
            case MAC_IP:
                verifyDB(MAC_IP, ids, expected);
                if (Tables.MAC_IP == tables)
                    break;
            case DVR_STB:
                verifyDB(DVR_STB, ids, expected);
                if (Tables.DVR_STB == tables)
                    break;
            case DVR_RECORDINGS:
                verifyDB(DVR_RECORDINGS, ids, expected);
                if (Tables.DVR_RECORDINGS == tables)
                    break;
            case DVR_UNSYNC:
                verifyDB(DVR_UNSYNC, ids, expected);
                if (Tables.DVR_UNSYNC == tables)
                    break;
            case DVR_MIGR:
                verifyDB(DVR_MIGR, ids, expected);
                if (Tables.DVR_MIGR == tables)
                    break;
            case DVR_MIGR_CHE:
                verifyDB(DVR_MIGR_CHE, ids, expected);
                if (Tables.DVR_MIGR_CHE == tables)
                    break;
            case DVR_MIGR_STB:
                verifyDB(DVR_MIGR_STB, ids, expected);
                if (Tables.DVR_MIGR_STB == tables)
                    break;
        }    
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.01. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from Settings Service Table") 
    @Test(priority=1) 
    public void test_15_1_01() throws Exception {
        positiveImpl(Tables.SETTINGS_STB);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.02. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from STB Metadata Table") 
    @Test(priority=1) 
    public void test_15_1_02() throws Exception {
        positiveImpl(Tables.STB_METADATA);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.03. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from Application Channels Authorization") 
    @Test(priority=1) 
    public void test_15_1_03() throws Exception {
        positiveImpl(Tables.APP_CHAN);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.04. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR History Table") 
    @Test(priority=1) 
    public void test_15_1_04() throws Exception {
        positiveImpl(Tables.DVR_HISTORY);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.05. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from Subscription Table") 
    @Test(priority=1) 
    public void test_15_1_05() throws Exception {
        positiveImpl(Tables.CHARTER_SUBS);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.06. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from MAC-IP Table") 
    @Test(priority=1) 
    public void test_15_1_06() throws Exception {
        positiveImpl(Tables.MAC_IP);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.07. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR STB") 
    @Test(priority=1) 
    public void test_15_1_07() throws Exception {
        positiveImpl(Tables.DVR_STB);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.08. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR Recordings") 
    @Test(priority=1) 
    public void test_15_1_08() throws Exception {
        positiveImpl(Tables.DVR_RECORDINGS);
    }
    
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.09. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR Unsync STB") 
    @Test(priority=1) 
    public void test_15_1_09() throws Exception {
        positiveImpl(Tables.DVR_UNSYNC);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.10. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR_MIGR_CHE_MTD_REQ table") 
    @Test(priority=1) 
    public void test_15_1_10() throws Exception {
        positiveImpl(Tables.DVR_MIGR);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.11. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR_MIGR_FOR_CHE table") 
    @Test(priority=1) 
    public void test_15_1_11() throws Exception {
        positiveImpl(Tables.DVR_MIGR_CHE);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.12. [Cleanup] STB CLEANUP REQUEST removes the data associated with the STB from DVR_MIGR_FOR_STB table") 
    @Test(priority=1) 
    public void test_15_1_12() throws Exception {
        positiveImpl(Tables.DVR_MIGR_STB);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.13. [Cleanup] STB CLEANUP REQUEST removes cached STB Metadata") 
    @Test(priority=1) 
    public void test_15_1_13() throws Exception {
        updateMacIp();
        Long[] ids = new Long[] {getDeviceId(deviceId)};
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVersion(0);
        msg.setFieldsCount(1);
        msg.setConnectionMode(ConnectionMode.Aloha);
        testDSGImpl(deviceId, msg);
        verifyDB(Tables.STB_METADATA, ids, 1);
        cleanDB(Tables.STB_METADATA, ids);
        testDSGImpl(deviceId, msg);
        verifyDB(Tables.STB_METADATA, ids, 0);
        positiveImpl(null, ids);
        updateMacIp();
        testDSGImpl(deviceId, msg);
        verifyDB(Tables.STB_METADATA, ids, 1);
    }
    
    private int getCassandraCount(String id, long l){
        PreparedStatement st = cassandra.prepare("select deviceid,recordingid from recordings where deviceid=? and recordingid=?");
        ResultSet rs = cassandra.execute(st.bind(id, l));
        return rs.all().size();
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.14. [Cleanup] STB CLEANUP REQUEST sends appropriate request to Headend to remove DVR data stored in Cassandra database (via Shared Library).") 
    @Test(priority=0) 
    public void test_15_1_14() throws Exception {
        updateMacIp();
        Long[] ids = new Long[] {getDeviceId(deviceId)};
        
        long l = 123L;
        PreparedStatement st = cassandra.prepare("insert into recordings (deviceid, recordingid) values(?,?)");
        cassandra.execute(st.bind(deviceId, l));
        int count = getCassandraCount(deviceId, l);
        Assert.assertTrue(count>0, "Cassandra: expected size>0, actual=" + count);
       
        positiveImpl(null, ids);
        
        count = getCassandraCount(deviceId, l);
        Assert.assertTrue(0 == count, "Cassandra: expected size=0, actual=" + count);
    }
    
    
    
        
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.15. [Cleanup] STB CLEANUP REQUEST with response status ’OK’") 
    @Test(priority=1) 
    public void test_15_1_15() throws Exception {
        positiveImpl(null);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.1.16. [Cleanup] STB CLEANUP REQUEST with response status ’Failed’") 
    @Test(priority=1) 
    public void test_15_1_16() throws Exception {
        updateMacIp();
        String id = "123";
        CleanupRequest request = new CleanupRequest();
        request.addDevice(id);
        CleanupResponse response = new CleanupResponse();
        response.addDevice(id, FAIL_STATUS, String.format(WRONG_DEVICE_ID, id));
        testImpl(request.toString(), 200, response.toJSON());
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.17. [Cleanup] STB CLEANUP REQUEST with several different STB") 
    @Test(priority=1) 
    public void test_15_1_17() throws Exception {
        long deviceIdL = getDeviceId(deviceId);
        Long[] ids = new Long[] {deviceIdL, deviceIdL + 10, deviceIdL + 20, deviceIdL + 30, deviceIdL + 40, deviceIdL + 50};
        positiveImpl(Tables.ALL, ids);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.1.18. [Cleanup] STB CLEANUP REQUEST with several identical STB") 
    @Test(priority=1) 
    public void test_15_1_18() throws Exception {
        positiveImpl(Tables.ALL, getDeviceId(deviceId), 5);
    }
    
    
    
    
    //
    
    @Features(CLEANUP) 
    @Stories("Cleanup,positive") 
    @Description("15.2.01. [Cleanup][Validation] STB CLEANUP REQUEST is valid") 
    @Test(priority=1) 
    public void test_15_2_01() throws Exception {
        positiveImpl(null);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.02. [Cleanup][Validation] STB CLEANUP REQUEST whith mistake in path to Cleanup service") 
    @Test(priority=2) 
    public void test_15_2_02() throws Exception {
        updateMacIp();
        CleanupRequest request = new CleanupRequest();
        request.addDevice(deviceId);
        testImpl(properties.getCleanupUri() + "MISTAKE" + "?" +HTTP_PARAMS, request.toString(), 404);
    }
    
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.03. [Cleanup][Validation] STB CLEANUP REQUEST cannot be properly parsed") 
    @Test(priority=2) 
    public void test_15_2_03() throws Exception {
        testImpl("unparseable body", 500);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.04. [Cleanup][Validation] STB CLEANUP REQUEST  with empty value of parameter \"req\"") 
    @Test(priority=2) 
    public void test_15_2_04() throws Exception {
        updateMacIp();
        CleanupRequest request = new CleanupRequest();
        request.addDevice(deviceId);
        testImpl(properties.getCleanupUri() + "?req=", request.toString(), 400);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.05. [Cleanup][Validation] STB CLEANUP REQUEST with numbers in value of parameter \"req\"") 
    @Test(priority=2) 
    public void test_15_2_05() throws Exception {
        updateMacIp();
        CleanupRequest request = new CleanupRequest();
        request.addDevice(deviceId);
        testImpl(properties.getCleanupUri() + "?req=12345", request.toString(), 400);
    }

    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.06. [Cleanup][Validation] STB CLEANUP REQUEST with special characters in value of parameter \"req\"") 
    @Test(priority=2) 
    public void test_15_2_06() throws Exception {
        updateMacIp();
        CleanupRequest request = new CleanupRequest();
        request.addDevice(deviceId);
        testImpl(properties.getCleanupUri() + "?req=%21%40%23%24%25%5E", request.toString(), 400);
    }    
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.07. [Cleanup][Validation] STB CLEANUP REQUEST without parameter \"req\"") 
    @Test(priority=2) 
    public void test_15_2_07() throws Exception {
        updateMacIp();
        CleanupRequest request = new CleanupRequest();
        request.addDevice(deviceId);
        testImpl(properties.getCleanupUri(), request.toString(), 400);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.08. [Cleanup][Validation] STB CLEANUP REQUEST with empty body") 
    @Test(priority=2) 
    public void test_15_2_08() throws Exception {
        testImpl("", 400);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.09. [Cleanup][Validation] STB CLEANUP REQUEST with bad format of body, one quote was deleted") 
    @Test(priority=2) 
    public void test_15_2_09() throws Exception {
        updateMacIp();
        CleanupRequest request = new CleanupRequest();
        request.addDevice(deviceId);
        String body = request.toString();
        Pattern pattern = Pattern.compile("([^\"]*\"[^\"]*)(\")(.*)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.matches())
            body = matcher.replaceFirst(matcher.group(1) + " " + matcher.group(3));
        testImpl(body, 500);
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.15. [Cleanup][Validation] STB CLEANUP REQUEST with empty value of deviceId") 
    @Test(priority=2) 
    public void test_15_2_15() throws Exception {
        updateMacIp();
        String id = "";
        CleanupRequest request = new CleanupRequest();
        request.addDevice(id);
        CleanupResponse response = new CleanupResponse();
        response.addDevice(id, FAIL_STATUS, String.format(WRONG_DEVICE_ID, id));
        testImpl(request.toString(), 200, response.toJSON());
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.16. [Cleanup][Validation] STB CLEANUP REQUEST with error in value of deviceId") 
    @Test(priority=2) 
    public void test_15_2_16() throws Exception {
        updateMacIp();
        String id = "ABCD";
        CleanupRequest request = new CleanupRequest();
        request.addDevice(id);
        CleanupResponse response = new CleanupResponse();
        response.addDevice(id, FAIL_STATUS, String.format(WRONG_DEVICE_ID, id));
        testImpl(request.toString(), 200, response.toJSON());
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.17. [Cleanup][Validation] STB CLEANUP REQUEST with special characters in value of deviceId") 
    @Test(priority=2) 
    public void test_15_2_17() throws Exception {
        updateMacIp();
        String id = "!@#$%^";
        CleanupRequest request = new CleanupRequest();
        request.addDevice(id);
        CleanupResponse response = new CleanupResponse();
        response.addDevice(id, FAIL_STATUS, String.format(WRONG_DEVICE_ID, id));
        testImpl(request.toString(), 200, response.toJSON());
    }
    
    @Features(CLEANUP) 
    @Stories("Cleanup,negative") 
    @Description("15.2.18. [Cleanup][Validation] STB CLEANUP REQUEST with nonexistent deviceId") 
    @Test(priority=2) 
    public void test_15_2_18() throws Exception {
        updateMacIp();
        String id = "888888888888";
        CleanupRequest request = new CleanupRequest();
        request.addDevice(id);
        CleanupResponse response = new CleanupResponse();
        response.addDevice(id, OK_STATUS/*FAIL_STATUS*/, null);
        testImpl(request.toString(), 200, response.toJSON());
    }
    
    private void positiveImpl(Tables tables, long id, int count) throws Exception {
        updateMacIp();
        Long [] ids = new Long[]{id};
        prepareDB(tables,ids);
        verifyDB(tables, ids, ids.length);
        CleanupRequest request = new CleanupRequest();
        CleanupResponse response = new CleanupResponse();
        for (int i=0; i<count; i++){
            request.addDevice(getId(id));
            response.addDevice(getId(id), OK_STATUS, null);
        }
        testImpl(request.toString(), 200, response.toJSON());
        verifyDB(tables, ids, 0);
    }
    
    private void positiveImpl(Tables tables, Long[]  ids) throws Exception {
        updateMacIp();
        prepareDB(tables,ids);
        verifyDB(tables, ids, ids.length);
        CleanupRequest request = new CleanupRequest();
        CleanupResponse response = new CleanupResponse();
        for (long id: ids){
            request.addDevice(getId(id));
            response.addDevice(getId(id), OK_STATUS, null);
        }
        testImpl(request.toString(), 200, response.toJSON());
        verifyDB(tables, ids, 0);
    }
    
    
    private void positiveImpl(Tables tables) throws Exception{
        positiveImpl(tables, new Long[] {getDeviceId(deviceId)});
    }
            
            
    private void testImpl(String request, int expectedStatus) throws Exception{
        testImpl(properties.getCleanupUri() + "?" + HTTP_PARAMS, request, expectedStatus, null);
    }
    
    private void testImpl(String request, int expectedStatus, JSONObject expectedBody) throws Exception{
        testImpl(properties.getCleanupUri() + "?" + HTTP_PARAMS, request, expectedStatus, expectedBody);
    }
    
    private void testImpl(String uri, String request, int expectedStatus) throws Exception{
        testImpl(uri, request, expectedStatus, null);
    }
    
    private void testImpl(String uri, String request, int expectedStatus, JSONObject expectedBody) throws Exception{
        FTHttpUtils.HttpResult response = FTHttpUtils.doPost(uri, request);
        getCleanupRequest(uri, request, response, expectedBody);
        int actual = response.getStatus();
        Assert.assertTrue(expectedStatus == actual, "Expected status: " + expectedStatus + " Actual: " + actual);
        if (null != expectedBody){
            try {
                JSONAssert.assertEquals((String)response.getEntity(), expectedBody, true);
            }
            catch(AssertionError ex){
                Assert.assertTrue(false, "Expected and actual response body must be the same");
            }
        }
    }
    
    
    @Attachment(value = "Cleanup request", type = "text/plain")
    private String getCleanupRequest(String uri, String request,FTHttpUtils.HttpResult response, JSONObject expected){
        StringBuilder sb = new StringBuilder();
        sb.append("REQUEST: ").append(uri).append("\n\n").append(request).append("\n\n").
                append("RESPONSE: ").append(response.getStatus());
        if (null != expected)
            sb.append("\n\nExpected:\n\n").append(expected).append("\n\nActual:");
        sb.append("\n\n").append(response.getEntity());
        return sb.toString();
    }
    
    
    @Features(CLEANUP) 
    @Stories("Cleanup AMS log") 
    @Description("Cleanup AMS log") 
    @Test(priority=100) 
    public void getCleanupAmsLog() throws Exception {
        getServerXml();
        stopAndgetAMSLog(true);
    }
    
    private void testDSGImpl(String deviceId, DSGMessage30 msg) throws Exception{   
       
        Semaphore semaphoreSettings = new Semaphore(0);
        DataConsumer consumerSettings = new DataConsumer(semaphoreSettings);
        
        Semaphore semaphore = new Semaphore(0);
        DataConsumer  consumer =  new DataConsumer(semaphore);
        
        DefEntDSGTest.prepareSTBMetadata(deviceId, msg.getVendor().getCode(), false /*insert record into STB_METADATA*/);
        
        LighweightCHEemulator he = null;
        CharterStbEmulator stb = null;
        
        try {
            
            he = createCharterHEEmulator(consumer, HEHandlerType.DENT_NEW);
            he.registerDataConsumer(consumerSettings, HEHandlerType.ERROR_REPORTING);
            startCharterHEEmulator(he);
            
            stb = createCharterSTBEmulator();
            startCharterSTBEmulator(stb);
        
            ZodiacMessage message = new ZodiacMessage();
            message.setBodyBytes();
            message.setMessageId(String.valueOf(0));
            int version = msg.getVersion();
            message.setSender("dsg" + "/" + version); 
            message.setAddressee("dsg" + "/" + version);
            message.setMac(Long.parseLong(properties.getDeviceId(), 16));
            
            byte [] data = msg.getMessage();
            message.setData(data);
            
            //message.setFlag(ZodiacMessage.FL_PERSISTENT);
            message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
            message.setFlag(ZodiacMessage.FL_SENDER_NOT_ADRESSEE);
            //message.setFlag(ZodiacMessage.FL_COMPRESSED);
        
            ZodiacMessage response = null;
            response = (ZodiacMessage)stb.getRUDPTransport().send(message);
                
            DOBCountingInputStream in = new DOBCountingInputStream(new ByteArrayInputStream(response.getData()));
            int error = DOB7bitUtils.decodeUInt(in)[0];
            
            //Assert.assertTrue(0 ==  error, "Expected error = 0, but actual = " + error);
            
            //wait for consumers
            consumer.getData();
            consumerSettings.getData();
        }
        finally {
            if (null != he)
                he.stop();
            if (null != stb)
                stb.stop();
        }
    }
    
    
    private long getDeviceId(String id){
        return Long.parseLong(id, 16);
    }
    
    private void updateMacIp(){
        String ipS = properties.getLocalHost();
        int ip = FTUtils.ipToInt(ipS);
        cleanDB(Tables.MAC_IP, deviceId);
        DBHelper.update(String.format("INSERT INTO MAC_IP(MAC,IP,MAC_STR,IP_STR) VALUES(%d,%d,'%s','%s')",
            getDeviceId(deviceId), ip, deviceId, ipS));
    }
}
