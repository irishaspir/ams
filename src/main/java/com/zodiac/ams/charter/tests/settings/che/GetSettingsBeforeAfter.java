package com.zodiac.ams.charter.tests.settings.che;


import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.*;

import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;
import static com.zodiac.ams.common.helpers.DateTimeHelper.*;


public class GetSettingsBeforeAfter extends StepsForSettings {

    @BeforeClass
    protected void runEmulators() {
        setDefaultValue(allSettings(), MAC_NUMBER_1);
    }

    @BeforeMethod
    public void parsAMSLog() {
        startTestTime = getCurrentTimeTest();
    }



}
