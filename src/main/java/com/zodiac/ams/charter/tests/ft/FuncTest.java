package com.zodiac.ams.charter.tests.ft;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.handlers.CharterRecentlyWatchedListener;
import com.dob.test.charter.iface.HEHandlerType;
import com.dob.test.charter.iface.IHEDataConsumer;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.CharterSTBEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.reminder.ReminderHelper;
import com.zodiac.ams.charter.services.ft.rwatched.RChannel;
import com.zodiac.ams.charter.services.ft.rwatched.RSTBChannels;
import com.zodiac.ams.common.http.FTHttpUtils;
import com.zodiac.ams.common.ssh.SSHHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author alexander.filippov
 */
public class FuncTest {
    protected final static long AMS_TIMEOUT = 5 * 60 * 1000;
    protected final static Logger logger = LoggerFactory.getLogger(FuncTest.class);

    protected FTConfig properties;
    protected SSHHelper executor;
    protected static Set<String> files = new HashSet<>();
    protected long timeDiff;

    @BeforeSuite
    public void beforeSuite() {
        logger.info("Before suite ...");
    }
    
    public boolean needRestart(){
        return true;
    }

    protected void init() {
        properties = FTConfig.getInstance();
        executor = properties.getSSHExecutor();
        String tz = properties.getAMSTimeZone();
        RChannel.setTimeZone(tz);
        RSTBChannels.setTimeZone(tz);
    }

    protected void generateServerXML() {
        ServerXMLHelper helper = new ServerXMLHelper(properties);
        helper.save("server.xml");
    }
    
    protected long getTimeDiff(){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss Z");
            Date remote = sdf.parse(executor.getExecOutput("date '+%d/%m/%y %H:%M:%S %z'"));
            Date local = new Date();
            return remote.getTime() - local.getTime();            
        }
        catch(Exception ex){
            logger.error("Unable to get time difference between local and remote hosts: {}", ex.getMessage());
            return 0;
        }   
    }
    
    
    public static void restartAMS(){
        FTConfig properties = FTConfig.getInstance();
        SSHHelper executor =properties.getSSHExecutor();
        String home = executor.getHomeDir();
        
        Assert.assertTrue(executor.exec(properties.getAMSStopCommand()), "Unable to execute stop command");
        
        String config = properties.getAMSConfig();
        if (null != config && !config.trim().equals("")) {
            Assert.assertTrue(executor.exec("sudo rm " + config), "Unable to remove the old server.xml configuration file");
            Assert.assertTrue(null != home, "Unable to get home directory");
            Assert.assertTrue(executor.putFile("server.xml", home + "/server.xml"), "Unable to copy server.xml on remote server");
            Assert.assertTrue(executor.exec("cd ; sudo cp server.xml " + properties.getAMSConfig()), "Unable to copy server.xml to Tomcat");
        }
        
        String context = properties.getAMSContext();
        if (null != context && !context.trim().equals("")) {
            Assert.assertTrue(executor.exec("sudo rm " + context), "Unable to remove the old context.xml configuration file");
            Assert.assertTrue(null != home, "Unable to get home directory");
            String fname = FTConfig.getFileFromJar("etc", "context.xml");
            Assert.assertTrue(executor.putFile(fname, home + "/context.xml"), "Unable to copy context.xml on remote server");
            Assert.assertTrue(executor.exec("cd ; sudo cp context.xml " + context), "Unable to copy context.xml to Tomcat");
        }
        
        String remove = properties.getAMSRemoveCommand();
        if (null != remove && !remove.trim().equals("")) {
            Assert.assertTrue(executor.exec(remove), "Unable to execute remove command");
            Assert.assertTrue(null != home, "Unable to get home directory");
            Assert.assertTrue(executor.putFile("ams.war", home + "/ams.war"), "Unable to copy ams.war on remote server");
            Assert.assertTrue(executor.exec("cd ; sudo cp ams.war " + properties.getAMSDir()), "Unable to copy ams.war to Tomcat");
        }
        
        Assert.assertTrue(executor.exec(properties.getAMSClearLogsCommand()), "Unable to execute clear command");
        Assert.assertTrue(executor.exec(properties.getAMSStartCommand()), "Unable to execute start command");
    }

    @BeforeClass
    public void beforeClass() {
        logger.info("Before class ...");
        
        init();
        
        timeDiff = getTimeDiff();
        
        if (needRestart()){
            logger.info("Start AMS ...");
            generateServerXML();
            restartAMS();
        }
    }


    @AfterClass
    public void afterClass() {
        logger.info("After class ...");
    }


    @Step("Create Charter HE Emulator")
    protected LighweightCHEemulator createCharterHEEmulator() {
        return createCharterHEEmulator(null, HEHandlerType.RWATCHED);
    }

    @Step("Create Charter HE Emulator")
    protected LighweightCHEemulator createCharterHEEmulator(IHEDataConsumer consumer, HEHandlerType type) {
        logger.info("Create Charter HE Emulator ...");
        CHEmulatorHelper helper = new CHEmulatorHelper();
        helper.setPort(properties.getEmulatorPort());
        LighweightCHEemulator emulator = helper.createEmulator();
        Assert.assertTrue(null != emulator, "Unable to create Charter HE Emulator");
        if (null != consumer)
            emulator.registerDataConsumer(consumer, type);
        return emulator;
    }

    @Step("Start Charter HE Emulator")
    protected void startCharterHEEmulator(LighweightCHEemulator emulator) {
        emulator.start();
    }

    @Step("Create Charter STB Emulator")
    protected CharterStbEmulator createCharterSTBRWatchedEmulator(String name) {
        logger.info("Create Charter STB Emulator ...");
        CharterSTBEmulatorHelper helper = new CharterSTBEmulatorHelper();
        helper.setRudpAmsHost(properties.getAMSHost());
        helper.setRudpLocalHost(properties.getLocalHost());
        CharterStbEmulator emulator = helper.createRWatchedEmulator(name);
        CharterRecentlyWatchedListener.setTimeZone(properties.getAMSTimeZone());
        Assert.assertTrue(null != emulator, "Unable to create charter stb emulator");
        return emulator;
    }

    @Step("Create Charter STB Emulator")
    protected CharterStbEmulator createCharterSTBEmulator(int error) {
        logger.info("Create Charter STB Emulator ...");
        CharterSTBEmulatorHelper helper = new CharterSTBEmulatorHelper();
        helper.setRudpAmsHost(properties.getAMSHost());
        helper.setRudpLocalHost(properties.getLocalHost());
        CharterStbEmulator emulator = (0 == error) ? helper.createEmulator() : helper.createEmulator(error);
        Assert.assertTrue(null != emulator, "Unable to create charter stb emulator");
        return emulator;
    }

    @Step("Create Charter STB Emulator")
    protected CharterStbEmulator createCharterSTBEmulator() {
        return createCharterSTBEmulator(null);
    }
    
    @Step("Create Charter STB Emulator")
    protected CharterStbEmulator createCharterSTBEmulator(Map<String,String> params) {
        logger.info("Create Charter STB Emulator ...");
        CharterSTBEmulatorHelper helper = new CharterSTBEmulatorHelper();
        helper.setRudpAmsHost(properties.getAMSHost());
        helper.setRudpLocalHost(properties.getLocalHost());
        CharterStbEmulator emulator = helper.createEmulator(params);
        Assert.assertTrue(null != emulator, "Unable to create charter stb emulator");
        return emulator;
    }
    
    @Step("Create Charter STB Emulator")        
    protected CharterStbEmulator createCharterSTB_DVR_Emulator(String dvrTestCase){
        logger.info("Create Charter STB Emulator for DVR ...");
        CharterSTBEmulatorHelper helper = new CharterSTBEmulatorHelper();
        helper.setRudpAmsHost(properties.getAMSHost());
        helper.setRudpLocalHost(properties.getLocalHost());
        helper.setDvrTestCase(dvrTestCase);
        CharterStbEmulator emulator = helper.createEmulator();
        Assert.assertTrue(null != emulator, "Unable to create charter stb emulator for DVR");
        return emulator;
    }

    @Step("Start Charter STB Emulator")
    protected void startCharterSTBEmulator(CharterStbEmulator emulator) {
        emulator.start();
    }

    @Attachment(value = "HTTP POST status", type = "text/plain")
    protected String getHTTPPostStatus(String uri, ReminderHelper request, FTHttpUtils.HttpResult response) {
        return "URI: " + uri + "\nREQUEST:\n" + request + "\nRESPONSE: " + response.getStatus() + "\n" + response.getEntity();
    }


    @Attachment(value = "HTTP POST status", type = "text/plain")
    protected String getHTTPPostStatus(String uri, FTHttpUtils.HttpResult response) {
        return "URI: " + uri + "\nRESPONSE: " + response.getStatus() + "\n" + response.getEntity();
    }

    @Attachment(value = "HTTP GET status", type = "text/plain")
    protected String getHTTPGetStatus(String uri, FTHttpUtils.HttpResult response) {
        return "URI: " + uri + "\nRESPONSE: " + response.getStatus() + "\n" + response.getEntity();
    }

    @Attachment(value = "HTTP GET status", type = "text/plain")
    protected String getHTTPGetStatus(String uri, FTHttpUtils.HttpResult response, String expected) {
        return "URI: " + uri + "\nRESPONSE: " + response.getStatus() + "\n" + response.getEntity() +
                ((null == expected) ? "" : "\nEXPECTED RESPONSE: " + expected);

    }

    @Attachment(value = "AMS log", type = "text/plain")
    protected String stopAndgetAMSLog(boolean stop) {
        if (stop)
            executor.exec(properties.getAMSStopCommand());
        String result = getRemoteFile("ams.log", properties.getAMSLog());
        if (stop)
            getJaCoCoDataFile();
        return result;
    }
    
    @Attachment(value = "server.xml", type = "text/xml")
    protected String getServerXml() throws IOException{
        return new String(Files.readAllBytes(new File("server.xml").toPath()));
    }

    protected void getJaCoCoDataFile(){
        String name = properties.getJaCoCoDataFile();
        if (null != name && !name.trim().equals(""))
            executor.getFile("jacoco.exec", name);
    }

    protected String getLocalFile(String local) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(local))) {
            String line;
            while (null != (line = in.readLine())) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        return sb.toString();
    }

    protected String getRemoteFile(SSHHelper executor, String local, String remote) {
        executor.getFile(local, remote);
        return getLocalFile(local);
    }

    protected String getRemoteFile(String local, String remote) {
        return getRemoteFile(executor, local, remote);
    }

    protected boolean waitForAMS(long delay) throws Exception {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < delay) {
            FTHttpUtils.HttpResult result = FTHttpUtils.doGet(properties.getPublisherUri());
            if (200 == result.getStatus())
                return true;
            Thread.sleep(1_000);
        }
        return false;
    }
}
