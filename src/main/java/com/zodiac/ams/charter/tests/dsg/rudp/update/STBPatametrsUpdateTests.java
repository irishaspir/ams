package com.zodiac.ams.charter.tests.dsg.rudp.update;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_2_STRING;

/**
 * Created by SpiridonovaIM on 15.06.2017.
 */
public class STBPatametrsUpdateTests extends BeforeAfter {


    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_2_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 2 AND <vendor> IS specified" +
            "migrationStart is absent")
    @Test(priority = 1)
    public void sendVersion3RequestId2IsNull() {
        String mac = MAC_NUMBER_4;
        int content = 0;
        content |= 0x01;
        content |= 0x02;
        content |= 0x04;
        content |= 0x8;
        content |= 0x10;
        content |= 0x20;

        message = stbParametersUpdateRequestBuilder.protocolVersion3(true, mac, 0x01, 2);
        List<String> stbResponse = stbParametersUpdateRequestBuilder.parseDsgResponse(stbParametersUpdateRequestBuilder.dsgRequest(message));
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
//        assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
//        assertJson(mac);
//        assertLog(log, mac,connectionMode, "", standardSTBRunning);
    }


}
