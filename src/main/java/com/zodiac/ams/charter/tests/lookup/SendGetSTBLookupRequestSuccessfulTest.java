package com.zodiac.ams.charter.tests.lookup;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.lookup.LookupGetHelper.responseJson;
import static com.zodiac.ams.charter.http.helpers.lookup.LookupGetHelper.sendGetStbStatusRequestMac;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendGetSTBLookupRequestSuccessfulTest extends StepsForLookup {

    @BeforeMethod
    public void beforeMethod(){
        runCHELookupWithCode200();
        getTestTime();
    }

    @AfterMethod
    public void afterMethod(){
        stopCHELookup();
    }

    @Features(FeatureList.STB_LOOKUP)
    @Stories(StoriesList.POSITIVE + StoriesList.STB_LOOKUP_POSITIVE)
    @Description("14.1.1 Send GET STB LOOKUP REQUEST")
    @TestCaseId("167190")
    @Test()
    public void sendSuccessfulGetStbLookupRequest() {
        assertEquals(sendGetStbStatusRequestMac(DEVICE_ID_NUMBER_1),SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, lookupJsonDataUtils.createJsonResponse());
    }

}
