package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.csv.file.entry.enumtype.ScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TunerUse;
import com.zodiac.ams.charter.csv.file.entry.enumtype.YesNo;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import static com.zodiac.ams.charter.tests.dc.StepsForDc.HUB_ID;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_15;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_5;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import static com.zodiac.ams.charter.tests.dc.TestUtils.getTunerUse;
import static com.zodiac.ams.charter.rudp.dc.message.LocalMessageUtils.UINT16;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import java.util.Random;

public class DcEvent15Test extends StepsForDcEvent {
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] All values are not empty")
    public void sendMessage1() throws Exception {
        sendPositiveMssageImpl(15 /*0-15*/, TunerUse.AVN, ScreenMode.FULL, YesNo.YES, YesNo.YES, YesNo.YES);
    }
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] TunerUse value is empty")
    public void sendMessage2() throws Exception {
        sendPositiveMssageImpl(2, TunerUse.UNKNOWN, ScreenMode.SCALED, YesNo.YES, YesNo.YES, YesNo.YES);
    }
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] ScreenMode value is empty")
    public void sendMessage3() throws Exception {
        sendPositiveMssageImpl(3 , TunerUse.EAS, ScreenMode.UNKNWON, YesNo.YES, YesNo.YES, YesNo.YES);
    }
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] TsbRecording value is empty")
    public void sendMessage4() throws Exception {
        sendPositiveMssageImpl(4 , TunerUse.IB, ScreenMode.FULL, YesNo.NO, YesNo.YES, YesNo.YES);
    }
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] DvrRecording value is empty")
    public void sendMessage5() throws Exception {
        sendPositiveMssageImpl(5 , TunerUse.LINEAR, ScreenMode.SCALED, YesNo.YES, YesNo.NO, YesNo.YES);
    }
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] DlnaOutput value is empty")
    public void sendMessage6() throws Exception {
        sendPositiveMssageImpl(6 , TunerUse.PPV, ScreenMode.FULL, YesNo.YES, YesNo.YES, YesNo.NO);
    }
    
    @Test(priority=1)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories("[Positive tests] Event15")
    @Description("[DC][Event 15] All values are empty")
    public void sendMessage7() throws Exception {
        sendPositiveMssageImpl(0 , TunerUse.UNKNOWN, ScreenMode.UNKNWON, YesNo.NO, YesNo.NO, YesNo.NO);
    }
    
    
    private void sendPositiveMssageImpl(int tunerId, TunerUse tunerUse, ScreenMode screenMode, 
        YesNo tsbRecording, YesNo dvrRecording, YesNo dlnaOutput) throws Exception {
        
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 0;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 1;
        final int eventId = EVENT_ID_15;
        
        final int tunerUseInt = getTunerUse(tunerId , tunerUse, screenMode, tsbRecording, dvrRecording, dlnaOutput);
        
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = new Random().nextInt(200);
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;
        
        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder(MESSAGE_TYPE_5)
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)
                
                //type 5
                .setStringsNumber(2)
                .addString("String1")
                .addString("String2")

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(UINT16+tunerUseInt) //must be encoded as uint16

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        
        
           CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                   
                .addParameter(tunerId)
                .addParameter(tunerUse)
                .addParameter(screenMode)
                .addParameter(tsbRecording)
                .addParameter(dvrRecording)
                .addParameter(dlnaOutput)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTunerUseCsvFile(csvFileAssertionInput);
    }
}
