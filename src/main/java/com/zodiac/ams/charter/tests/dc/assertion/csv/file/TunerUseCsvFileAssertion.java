package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.TunerUseCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.ScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TunerUse;
import com.zodiac.ams.charter.csv.file.entry.enumtype.YesNo;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEventStartTimeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMacEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSequenceNumberEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertStbSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTuneIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTunerUseEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertScreenMode;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTsbRecording;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertDvrRecording;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertDlnaOutput;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputTimeDeltaSum;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.toLocalDateTime;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_2;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_3;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;
import org.apache.commons.collections.CollectionUtils;

public class TunerUseCsvFileAssertion extends AbstractCsvFileAssertion<TunerUseCsvFileEntry>{
    
    public TunerUseCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    @Override
    public void assertFile(List<TunerUseCsvFileEntry> csvFileEntries) {
        
        
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        int eventsCount = calculateInputEventsCount(input);

        assertEntriesCountEquals(eventsCount, csvFileEntries.size());

        ListIterator<TunerUseCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputMessageType = input.getMessageType();

        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();

            int inputFirstEventNumber = sessionData.getFirstEventNumber();

            int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

            int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

            Integer previousInputParameterDestinationChannelNumber = null;

            int sequenceNumberCounter = 1;

            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                TunerUseCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();

                List<Object> inputParameters = eventData.getParameters();
                
                Integer inputTuneId = (Integer) inputParameters.get(0);
                TunerUse inputTunerUse = (TunerUse) inputParameters.get(1);
                ScreenMode inputScreenMode = (ScreenMode) inputParameters.get(2);
                YesNo inputTsbRecording = (YesNo) inputParameters.get(3);
                YesNo inputDvrRecording = (YesNo) inputParameters.get(4);
                YesNo inputDlnaOutput = (YesNo) inputParameters.get(5);
                
                int inputTimestampPartMsec = eventData.getTimestampPartMsec();

                String macId = csvFileEntry.getMacId();
                Long mac = DOBUtil.string2mac(macId);
                String stbSessionId = csvFileEntry.getStbSessionId();
                Integer sequenceNumber = csvFileEntry.getSequenceNumber();
                LocalDateTime eventStartTime = csvFileEntry.getEventStartTime();
                
                Integer tuneId = csvFileEntry.getTunerId();
                TunerUse tunerUse = csvFileEntry.getTunerUse();
                ScreenMode screenMode = csvFileEntry.getScreenMode();
                YesNo tsbRecording = csvFileEntry.getTsbRecording();
                YesNo dvrRecording = csvFileEntry.getDvrRecording();
                YesNo dlnaOutput = csvFileEntry.getDlnaOutput();
                
                // mac assertion
                assertMacEquals(mac, inputMac);
                // end of mac assertion

                // stbSessionId assertion
                // "Z" + "_" + <STB MAC> + "_" + <Session start timestamp>
                String inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
                assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
                // end of stbSessionId assertion

                // eventStartTime assertion
                // if 'messageType' < 3 -> <Session start timestamp> + sum of respective <Time delta>s, in yyyy-MM-DDThh:mm:ss format
                // if 'messageType' >= 3 -> <Session start timestamp> + sum of respective <Time delta>s + <timestampPartMsec>msec, in yyyy-MM-DDThh:mm:ss.xxx format
                long inputEventStartTimeMsec;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
                } else {
                    inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                            + inputTimestampPartMsec;
                }
                LocalDateTime inputEventStartTime = toLocalDateTime(inputEventStartTimeMsec);
                assertEventStartTimeEquals(eventStartTime, inputEventStartTime);
                // end of eventStartTime assertion



                // sequenceNumber assertion
                // if 'messageType' < 3 -> empty
                // if 'messageType' >= 3 -> <First event number> + sequence number of the event in the session, starting at 0
                Integer inputSequenceNumber = null;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputSequenceNumber = null;
                } else {
                    inputSequenceNumber = inputFirstEventNumber + (sequenceNumberCounter - 1);
                }
                assertSequenceNumberEquals(sequenceNumber, inputSequenceNumber);
                // end of sequenceNumber assertion
                
                
                assertTuneIdEquals(tuneId, inputTuneId);
                assertTunerUseEquals(tunerUse, inputTunerUse);
                assertScreenMode(screenMode, inputScreenMode);
                assertTsbRecording(tsbRecording, inputTsbRecording);
                assertDvrRecording(dvrRecording, inputDvrRecording);
                assertDlnaOutput(dlnaOutput, inputDlnaOutput);

                sequenceNumberCounter++;
            }
        }
    }
    
}
