package com.zodiac.ams.charter.tests.dc.assertion;

import com.zodiac.ams.charter.csv.file.entry.enumtype.AppScreen;
import com.zodiac.ams.charter.csv.file.entry.enumtype.AppType;
import com.zodiac.ams.charter.csv.file.entry.enumtype.ScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionActivity;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventType;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TunerUse;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TuningEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.YesNo;

import java.time.LocalDateTime;

import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.compareSafelyAgainst0;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * The assertion utilities.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public class AssertionUtils {
    /*
     * The private constructor.
     */
    private AssertionUtils() {
        // do nothing here
    }

    /**
     * The method to assert the CSV file entry and input additionalInfo.
     *
     * @param additionalInfo      the CSV file entry additionalInfo
     * @param inputAdditionalInfo the input additionalInfo
     */
    public static void assertAdditionalInfoEquals(String additionalInfo, String inputAdditionalInfo) {
        final String message = "The CSV file entry and input additionalInfo are not equal";
        assertEquals(inputAdditionalInfo, additionalInfo, message);
    }

    /**
     * The method to assert the CSV file entry and input appExitTime.
     *
     * @param appExitTime      the CSV file entry appExitTime
     * @param inputAppExitTime the input appExitTime
     */
    public static void assertAppExitTimeEquals(LocalDateTime appExitTime, LocalDateTime inputAppExitTime) {
        final String message = "The CSV file entry and input appExitTime are not equal";
        assertEquals(inputAppExitTime, appExitTime, message);
    }

    /**
     * The method to assert the CSV file entry and input appScreen.
     *
     * @param appScreen      the CSV file entry appScreen
     * @param inputAppScreen the input appScreen
     */
    public static void assertAppScreenEquals(AppScreen appScreen, AppScreen inputAppScreen) {
        final String message = "The CSV file entry and input appScreen are not equal";
        assertEquals(inputAppScreen, appScreen, message);
    }

    /**
     * The method to assert the CSV file entry and input appSessionId.
     *
     * @param appSessionId      the CSV file entry appSessionId
     * @param inputAppSessionId the input appSessionId
     */
    public static void assertAppSessionIdEquals(String appSessionId, String inputAppSessionId) {
        final String message = "The CSV file entry and input appSessionId are not equal";
        assertEquals(inputAppSessionId, appSessionId, message);
    }

    /**
     * The method to assert the CSV file entry and input appType.
     *
     * @param appType      the CSV file entry appType
     * @param inputAppType the input appType
     */
    public static void assertAppTypeEquals(AppType appType, AppType inputAppType) {
        final String message = "The CSV file entry and input appType are not equal";
        assertEquals(inputAppType, appType, message);
    }

    /**
     * The method to assert the CSV file entry and input avnClientSessionId.
     *
     * @param avnClientSessionId      the CSV file entry avnClientSessionId
     * @param inputAvnClientSessionId the input avnClientSessionId
     */
    public static void assertAvnClientSessionIdEquals(Integer avnClientSessionId, Integer inputAvnClientSessionId) {
        final String message = "The CSV file entry and input avnClientSessionId are not equal";
        assertEquals(inputAvnClientSessionId, avnClientSessionId, message);
    }

    /**
     * The method to assert the CSV file entry and input channelMapId.
     *
     * @param channelMapId      the CSV file entry channelMapId
     * @param inputChannelMapId the input channelMapId
     */
    public static void assertChannelMapIdEquals(String channelMapId, String inputChannelMapId) {
        final String message = "The CSV file entry and input channelMapId are not equal";
        assertEquals(inputChannelMapId, channelMapId, message);
    }

    /**
     * The method to assert the CSV file entry and input destinationTmsId.
     *
     * @param destinationTmsId      the CSV file entry destinationTmsId
     * @param inputDestinationTmsId the input destinationTmsId
     */
    public static void assertDestinationTmsIdEquals(Integer destinationTmsId, Integer inputDestinationTmsId) {
        final String message = "The CSV file entry and input destinationTmsId are not equal";
        assertEquals(inputDestinationTmsId, destinationTmsId, message);
    }

    /**
     * The method to assert the CSV file entry and input eventStartTime.
     *
     * @param eventStartTime      the CSV file entry eventStartTime
     * @param inputEventStartTime the input eventStartTime
     */
    public static void assertEventStartTimeEquals(LocalDateTime eventStartTime, LocalDateTime inputEventStartTime) {
        final String message = "The CSV file entry and input eventStartTime are not equal";
        assertEquals(inputEventStartTime, eventStartTime, message);
    }

    /**
     * The method to assert the CSV file entry and input eventTime.
     *
     * @param eventTime      the CSV file entry eventTime
     * @param inputEventTime the input eventTime
     */
    public static void assertEventTimeEquals(LocalDateTime eventTime, LocalDateTime inputEventTime) {
        final String message = "The CSV file entry and input eventTime are not equal";
        assertEquals(inputEventTime, eventTime, message);
    }

    /**
     * The method to assert the CSV file entry and input externalHddCapacity.
     *
     * @param externalHddCapacity      the CSV file entry externalHddCapacity
     * @param inputExternalHddCapacity the input externalHddCapacity
     */
    public static void assertExternalHddCapacityEquals(Integer externalHddCapacity, Integer inputExternalHddCapacity) {
        final String message = "The CSV file entry and input externalHddCapacity are not equal";
        if (compareSafelyAgainst0(externalHddCapacity)) {
            assertNull(inputExternalHddCapacity, message);
        } else {
            assertEquals(inputExternalHddCapacity, externalHddCapacity, message);
        }
    }

    /**
     * The method to assert the CSV file entry and input externalHddFreeSpacePercent.
     *
     * @param externalHddFreeSpacePercent      the CSV file entry externalHddFreeSpacePercent
     * @param inputExternalHddFreeSpacePercent the input externalHddFreeSpacePercent
     */
    public static void assertExternalHddFreeSpacePercentEquals(Integer externalHddFreeSpacePercent, Integer inputExternalHddFreeSpacePercent) {
        final String message = "The CSV file entry and input externalHddFreeSpacePercent are not equal";
        if (compareSafelyAgainst0(externalHddFreeSpacePercent)) {
            assertNull(inputExternalHddFreeSpacePercent, message);
        } else {
            assertEquals(inputExternalHddFreeSpacePercent, externalHddFreeSpacePercent, message);
        }
    }

    /**
     * The method to assert the CSV file entry and input internalHddCapacity.
     *
     * @param internalHddCapacity      the CSV file entry internalHddCapacity
     * @param inputInternalHddCapacity the input internalHddCapacity
     */
    public static void assertInternalHddCapacityEquals(Integer internalHddCapacity, Integer inputInternalHddCapacity) {
        final String message = "The CSV file entry and input internalHddCapacity are not equal";
        if (compareSafelyAgainst0(internalHddCapacity)) {
            assertNull(inputInternalHddCapacity, message);
        } else {
            assertEquals(inputInternalHddCapacity, internalHddCapacity, message);
        }
    }

    /**
     * The method to assert the CSV file entry and input internalHddFreeSpacePercent.
     *
     * @param internalHddFreeSpacePercent      the CSV file entry internalHddFreeSpacePercent
     * @param inputInternalHddFreeSpacePercent the input internalHddFreeSpacePercent
     */
    public static void assertInternalHddFreeSpacePercentEquals(Integer internalHddFreeSpacePercent, Integer inputInternalHddFreeSpacePercent) {
        final String message = "The CSV file entry and input internalHddFreeSpacePercent are not equal";
        if (compareSafelyAgainst0(internalHddFreeSpacePercent)) {
            assertNull(inputInternalHddFreeSpacePercent, message);
        } else {
            assertEquals(inputInternalHddFreeSpacePercent, internalHddFreeSpacePercent, message);
        }
    }

    /**
     * The method to assert the CSV file entry and input hubId.
     *
     * @param hubId      the CSV file entry hubId
     * @param inputHubId the input hubId
     */
    public static void assertHubIdEquals(Integer hubId, Integer inputHubId) {
        final String message = "The CSV file entry and input hubId are not equal";
        assertEquals(inputHubId, hubId, message);
    }

    /**
     * The method to assert the CSV file entry and input mac.
     *
     * @param mac      the CSV file entry mac
     * @param inputMac the input mac
     */
    public static void assertMacEquals(Long mac, Long inputMac) {
        final String message = "The CSV file entry and input mac are not equal";
        assertEquals(inputMac, mac, message);
    }

    /**
     * The method to assert the CSV file entry and input maxRebootsPerDay.
     *
     * @param maxRebootsPerDay      the CSV file entry maxRebootsPerDay
     * @param inputMaxRebootsPerDay the input maxRebootsPerDay
     */
    public static void assertMaxRebootsPerDayEquals(Integer maxRebootsPerDay, Integer inputMaxRebootsPerDay) {
        final String message = "The CSV file entry and input maxRebootsPerDay are not equal";
        assertEquals(inputMaxRebootsPerDay, maxRebootsPerDay, message);
    }

    /**
     * The method to assert the CSV file entry and input menuOptionSelected.
     *
     * @param menuOptionSelected      the CSV file entry menuOptionSelected
     * @param inputMenuOptionSelected the input menuOptionSelected
     */
    public static void assertMenuOptionSelectedEquals(String menuOptionSelected, String inputMenuOptionSelected) {
        final String message = "The CSV file entry and input menuOptionSelected are not equal";
        assertEquals(inputMenuOptionSelected, menuOptionSelected, message);
    }

    /**
     * The method to assert the CSV file entry and input nodeId.
     *
     * @param nodeId      the CSV file entry nodeId
     * @param inputNodeId the input nodeId
     */
    public static void assertNodeIdEquals(Integer nodeId, Integer inputNodeId) {
        final String message = "The CSV file entry and input nodeId are not equal";
        assertEquals(inputNodeId, nodeId, message);
    }

    /**
     * The method to assert the CSV file entry and input remoteKeys.
     *
     * @param remoteKeys      the CSV file entry remoteKeys
     * @param inputRemoteKeys the input remoteKeys
     */
    public static void assertRemoteKeysEquals(String remoteKeys, String inputRemoteKeys) {
        final String message = "The CSV file entry and input remoteKeys are not equal";
        assertEquals(inputRemoteKeys, remoteKeys, message);
    }

    /**
     * The method to assert the CSV file entry and input sequenceNumber.
     *
     * @param sequenceNumber      the CSV file entry sequenceNumber
     * @param inputSequenceNumber the input sequenceNumber
     */
    public static void assertSequenceNumberEquals(Integer sequenceNumber, Integer inputSequenceNumber) {
        final String message = "The CSV file entry and input sequenceNumber are not equal";
        assertEquals(inputSequenceNumber, sequenceNumber, message);
    }

    /**
     * The method to assert the CSV file entry and input sessionActivity.
     *
     * @param sessionActivity      the CSV file entry sessionActivity
     * @param inputSessionActivity the input sessionActivity
     */
    public static void assertSessionActivityEquals(SessionActivity sessionActivity, SessionActivity inputSessionActivity) {
        final String message = "The CSV file entry and input sessionActivity are not equal";
        assertEquals(inputSessionActivity, sessionActivity, message);
    }

    /**
     * The method to assert the CSV file entry and input reason.
     *
     * @param reason      the CSV file entry reason
     * @param inputReason the input reason
     */
    public static void assertSessionEventReasonEquals(SessionEventReason reason, SessionEventReason inputReason) {
        final String message = "The CSV file entry and input reason are not equal";
        assertEquals(inputReason, reason, message);
    }

    /**
     * The method to assert the CSV file entry and input signalLevel.
     *
     * @param signalLevel      the CSV file entry signalLevel
     * @param inputSignalLevel the input signalLevel
     */
    public static void assertSignalLevelEquals(Integer signalLevel, Integer inputSignalLevel) {
        final String message = "The CSV file entry and input signalLevel are not equal";
        assertEquals(inputSignalLevel, signalLevel, message);
    }

    /**
     * The method to assert the CSV file entry and input sourceTmsId.
     *
     * @param sourceTmsId      the CSV file entry sourceTmsId
     * @param inputSourceTmsId the input sourceTmsId
     */
    public static void assertSourceTmsIdEquals(Integer sourceTmsId, Integer inputSourceTmsId) {
        final String message = "The CSV file entry and input sourceTmsId are not equal";
        assertEquals(inputSourceTmsId, sourceTmsId, message);
    }

    /**
     * The method to assert the CSV file entry and input stbSessionId.
     *
     * @param stbSessionId      the CSV file entry stbSessionId
     * @param inputStbSessionId the input stbSessionId
     */
    public static void assertStbSessionIdEquals(String stbSessionId, String inputStbSessionId) {
        final String message = "The CSV file entry and input stbSessionId are not equal";
        assertEquals(inputStbSessionId, stbSessionId, message);
    }

    /**
     * The method to assert the CSV file entry and input tmsId.
     *
     * @param tmsId      the CSV file entry tmsId
     * @param inputTmsId the input tmsId
     */
    public static void assertTmsIdEquals(Integer tmsId, Integer inputTmsId) {
        final String message = "The CSV file entry and input internalHddCapacity are not equal";
        if (compareSafelyAgainst0(tmsId)) {
            assertNull(inputTmsId, message);
        } else {
            assertEquals(inputTmsId, tmsId, message);
        }
    }

    /**
     * The method to assert the CSV file entry and input reason.
     *
     * @param reason      the CSV file entry reason
     * @param inputReason the input reason
     */
    public static void assertTsbEventReasonEquals(TsbEventReason reason, TsbEventReason inputReason) {
        final String message = "The CSV file entry and input reason are not equal";
        assertEquals(inputReason, reason, message);
    }

    /**
     * The method to assert the CSV file entry and input screenMode.
     *
     * @param screenMode      the CSV file entry screenMode
     * @param inputScreenMode the input screenMode
     */
    public static void assertTsbEventScreenModeEquals(TsbEventScreenMode screenMode, TsbEventScreenMode inputScreenMode) {
        final String message = "The CSV file entry and input screenMode are not equal";
        assertEquals(inputScreenMode, screenMode, message);
    }

    /**
     * The method to assert the CSV file entry and input eventType.
     *
     * @param eventType      the CSV file entry eventType
     * @param inputEventType the input eventType
     */
    public static void assertTsbEventTypeEquals(TsbEventType eventType, TsbEventType inputEventType) {
        final String message = "The CSV file entry and input eventType are not equal";
        assertEquals(inputEventType, eventType, message);
    }

    /**
     * The method to assert the CSV file entry and input reason.
     *
     * @param reason      the CSV file entry reason
     * @param inputReason the input reason
     */
    public static void assertTuningEventReasonEquals(TuningEventReason reason, TuningEventReason inputReason) {
        final String message = "The CSV file entry and input reason are not equal";
        assertEquals(inputReason, reason, message);
    }

    /**
     * The method to assert CSV file entries count.
     * <p>
     * Important! We assume that the count of entities should be equal to the count of events in sessions.
     *
     * @param eventsCount  the events count
     * @param entriesCount the CSV file entries count
     */
    public static void assertEntriesCountEquals(int eventsCount, int entriesCount) {
        assertEquals(entriesCount, eventsCount, "The count of entities is not equal to the count of events in" +
                " sessions");
    }

    /**
     * The method to assert CSV file entries count of 1.
     *
     * @param entriesCount the CSV file entries count
     */
    public static void assertEntriesCountOf1Equals(int entriesCount) {
        assertEntriesCountOfXEqualsInternal(entriesCount, 1);
    }

    private static void assertEntriesCountOfXEqualsInternal(int entriesCount, int expectedCount) {
        assertEquals(entriesCount, expectedCount, "The count of entities is not equal to " + expectedCount);
    }
    
    public static void assertTuneIdEquals(Integer tuneId, Integer inputTuneId){
        assertEquals(tuneId, inputTuneId, "TuneId values are not equal");
    }
    
    public static void  assertTunerUseEquals(TunerUse tunerUse, TunerUse inputTunerUse){
        assertEquals(tunerUse, inputTunerUse, "TunerUse values are not equal");
    }
    
    public static void assertScreenMode(ScreenMode screenMode, ScreenMode inputScreenMode){
        assertEquals(screenMode, inputScreenMode, "ScreenMode values are not equal");
    }
    
    public static void assertTsbRecording(YesNo tsbRecording, YesNo inputTsbRecording){
        assertEquals(tsbRecording, inputTsbRecording, "TsbRecording values are not equal");
    }
    
    public static void assertDvrRecording(YesNo dvrRecording, YesNo inputDvrRecording){
        assertEquals(dvrRecording, inputDvrRecording, "DvrRecording values are not equal");
    }
    
    public static  void assertDlnaOutput(YesNo dlnaOutput, YesNo inputDlnaOutput){
        assertEquals(dlnaOutput, inputDlnaOutput, "DlnaOutput values are not equal");
    }
}
