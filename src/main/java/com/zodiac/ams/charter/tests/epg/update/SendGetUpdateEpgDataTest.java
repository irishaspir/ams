package com.zodiac.ams.charter.tests.epg.update;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static org.testng.Assert.assertEquals;

import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;

public class SendGetUpdateEpgDataTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_UPDATE_EPG_DATA)
    @Description("2.2.2 Send Get update EPG cheDataReceived, receive valid cheDataReceived ")
    @TestCaseId("21212")
    @Test()
    public void sendGetUpdateEpgDataReceiveData() {
        createAttachment(epgSampleBuilder.createCsv("3"));
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
    }
}
