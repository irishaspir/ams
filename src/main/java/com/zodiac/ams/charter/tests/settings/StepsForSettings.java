package com.zodiac.ams.charter.tests.settings;


import com.zodiac.ams.charter.bd.ams.tables.SettingsSyncV2Utils;
import com.zodiac.ams.charter.emuls.che.settings.SettingsCHEEmulator;
import com.zodiac.ams.charter.emuls.stb.settings.SettingsSTBEmulator;
import com.zodiac.ams.charter.http.helpers.settings.json.JsonDataUtils;
import com.zodiac.ams.charter.rudp.settings.SettingsRudpHelper;
import com.zodiac.ams.charter.rudp.settings.message.SettingsSTBResponseBuilder;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.*;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.*;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;

public class StepsForSettings extends Steps {

    protected SettingsRudpHelper settingsRudpHelper;
    protected SettingsSTBEmulator settingsSTBEmulator;
    protected SettingsCHEEmulator cheEmulatorSettings;
    protected JsonDataUtils jsonDataUtils = new JsonDataUtils();
    protected SettingsSTBResponseBuilder settingsSTBResponse = new SettingsSTBResponseBuilder();
    protected ArrayList<SettingsOption> options = allSettings();

    @Step
    public void cleanBDForSettings() {
        Logger.info("Delete MAC_NUMBER_3 and MAC_NUMBER_4 from SettingsSTBDataV2,SettingsSTB" +
                " and clean SettingsSyncV2");
        deleteKeysFromSettingsSTBDataV2ByMAC(MAC_NUMBER_3);
        deleteKeysFromSettingsSTBDataV2ByMAC(MAC_NUMBER_4);
        deleteRowFromSettingsSTB(MAC_NUMBER_3);
        deleteRowFromSettingsSTB(MAC_NUMBER_4);
        new SettingsSyncV2Utils().deleteAll();
    }

    @Step
    protected List<String> changeKeyValue(ArrayList<SettingsOption> options, String mac) {
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        List<String> allKeysFromDb = getAllKeysFromSettingsSTBDataV2(mac);

        allKeysFromDb.remove(0);
        allKeysFromDb.remove(0);
        for (int i = 0; i < options.size(); i++) {
            keys.add(i, options.get(i).getColumnName());
            String key = allKeysFromDb.get(options.get(i).getColumnNumber());
            switch (options.get(i).getColumnNumber()) {
                case 27:
                case 37:
                case 41:
                case 53:
                case 54:
                    if (key.equals("0|")) values.add(i, "'" + nextElement(options.get(i)) + "'");
                    break;
                default:
                    if (key.equals(options.get(i).getDefaultValue()))
                        values.add(i, "'" + nextElement(options.get(i)) + "'");
            }
        }
        setKeysInSettingsSTBDataV2(mac, keys, values);
        Logger.info("Keys have been set as not default");
        return getAllKeysFromSettingsSTBDataV2(mac);

    }


    @Step
    protected List<String> allKeys(ArrayList<SettingsOption> options) {
        List<String> list = new ArrayList<>();
        for (SettingsOption one : options) {
            switch (one.getColumnNumber()) {
                case 27:
                case 37:
                case 53:
                case 54: {
                    list.add("0|");
                    break;
                }
                case 55:
                    list.add("0|");
                    break;
                case 31: {
                    if (one.getDefaultValue().length() == 0) {
                        list.add("0");
                    } else {
                        list.add(one.getDefaultValue());
                    }

                    break;
                }
                default:
                    list.add(one.getDefaultValue());
            }
        }
        Logger.info(list.toString());
        return list;
    }


    @Step
    protected List<String> allKeys(ArrayList<SettingsOption> options, int newValue) {
        List<String> list = new ArrayList<>();
        for (SettingsOption one : options) {
            switch (one.getColumnNumber()) {
                case 25:
                    list.add(String.valueOf(newValue));
                    break;
                case 27:
                case 37:
                case 53:
                case 54: {
                    list.add("0|");
                    break;
                }
                case 55:
                    list.add("0|");
                    break;
                case 31: {
                    if (one.getDefaultValue().length() == 0) {
                        list.add("0");
                    } else {
                        list.add(one.getDefaultValue());
                    }

                    break;
                }
                default:
                    list.add(one.getDefaultValue());
            }
        }
        Logger.info(list.toString());
        return list;
    }

    @Step
    protected List<String> allKeysWithNull62Key(ArrayList<SettingsOption> options) {
        List<String> list = new ArrayList<>();
        for (SettingsOption one : options) {
            switch (one.getColumnNumber()) {
                case 27:
                case 37:
                case 53:
                case 54:
                case 55:
                    list.add("0|");
                    Logger.info(String.valueOf(one.getColumnNumber()) + " - " + String.valueOf("0|"));
                    break;
                case 31:
                    if (one.getDefaultValue().length() == 0) {
                        list.add("0");
                        Logger.info(String.valueOf(one.getColumnNumber()) + " - " + String.valueOf("0"));
                    } else {
                        list.add(one.getDefaultValue());
                        Logger.info(String.valueOf(one.getColumnNumber()) + " - " + String.valueOf(one.getDefaultValue()));
                    }
                    break;
                case 58:
                    list.add(one.getDefaultValue() + "|");
                    Logger.info(String.valueOf(one.getColumnNumber()) + " - " + String.valueOf(one.getDefaultValue()));
                    break;
                case 62:
                    list.add("null");
                    Logger.info(String.valueOf(one.getColumnNumber()) + " - " + String.valueOf("null"));
                    break;
                default:
                    list.add(one.getDefaultValue());
                    Logger.info(String.valueOf(one.getColumnNumber()) + " - " + String.valueOf(one.getDefaultValue()));
            }
        }
        Logger.info(list.toString());
        return list;
    }

    @Step
    private String nextElement(SettingsOption option) {
        String next = null;
        int lenth = option.getRange().length;
        if (lenth == 0)
            next = option.getDefaultValue();
        if (lenth == 1)
            next = option.getRange()[0];
        if (lenth > 1) {
            for (String one : option.getRange()) {
                if (!one.equals(option.getDefaultValue())) {
                    next = one;
                    break;
                }
            }
        }
        return next;
    }

    @Step
    protected ArrayList<String> nullResponse() {
        return null;
    }

    @Step
    protected String migrateFlagEqual1() {
        return "1";
    }

    @Step
    protected void runSTBWithTimeoutOfResponse() {
        settingsSTBEmulator = new SettingsSTBEmulator(config.getTIMEOUT());
        settingsSTBEmulator.registerConsumer();
        settingsSTBEmulator.start();
        Logger.info("STB Emulator's run with timeout " + config.getTIMEOUT());
        settingsRudpHelper = new SettingsRudpHelper(settingsSTBEmulator);
    }

    @Step
    protected void runCHESettings() {
        cheEmulatorSettings = new SettingsCHEEmulator();
        cheEmulatorSettings.registerConsumer();
        cheEmulatorSettings.start();
        Logger.info("CHE Emulator's run");
    }


    @Step
    protected void runSTB() {
        settingsSTBEmulator = new SettingsSTBEmulator();
        settingsSTBEmulator.registerConsumer();
        settingsSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        settingsRudpHelper = new SettingsRudpHelper(settingsSTBEmulator);
    }

    @Step
    protected void stopSTB() {
        settingsSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
    }

    @Step
    protected void stopCHESettings() {
        cheEmulatorSettings.stop();
        Logger.info("CHE Emulator's stopped");
    }


    @Step
    public void setDefaultValue(ArrayList<SettingsOption> options, String mac) {
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        for (int i = 0; i < options.size(); i++) {
            keys.add(i, options.get(i).getColumnName());
            values.add(i, "'" + options.get(i).getDefaultValue() + "'");
        }
        setKeysInSettingsSTBDataV2(mac, keys, values);
        Logger.info("Keys have been set as default");
    }

    @Step
    public static ArrayList<SettingsOption> delete55And59And60Keys(ArrayList<SettingsOption> options) {
        Iterator itr = options.iterator();
        while (itr.hasNext()) {
            Object object = itr.next();
            switch (((SettingsOption) object).getColumnNumber()) {
                case 55:
                case 59:
                case 60:
                    Logger.info("" + ((SettingsOption) object).getColumnNumber());
                    itr.remove();
                    break;
            }
        }
        return options;
    }

    @Step
    public ArrayList<SettingsOption> delete62Key(ArrayList<SettingsOption> options) {
        Iterator itr = options.iterator();
        while (itr.hasNext()) {
            Object object = itr.next();
            switch (((SettingsOption) object).getColumnNumber()) {
                case 62:
                    Logger.info("Delete " + ((SettingsOption) object).getColumnNumber());
                    itr.remove();
                    break;
            }
        }
        return options;
    }

    @Step
    protected void setMigrationFlagOne(String mac) {
        setMigrationFlagOneInSettingSTB(mac);
    }

    @Step
    protected void setMigrationFlagNull(String mac) {
        setMigrationFlagNullInSettingsSTB(mac);
    }


}
