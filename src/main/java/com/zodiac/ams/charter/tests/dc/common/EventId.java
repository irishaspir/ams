package com.zodiac.ams.charter.tests.dc.common;

/**
 * The event id constants.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public interface EventId {
    Integer EVENT_ID_ZERO = 0;
    Integer EVENT_ID_ONE = 1;
    Integer EVENT_ID_TWO = 2;
    Integer EVENT_ID_THREE = 3;
    Integer EVENT_ID_FOUR = 4;
    Integer EVENT_ID_FIVE = 5;
    Integer EVENT_ID_SIX = 6;
    Integer EVENT_ID_ELEVEN = 11;
    Integer EVENT_ID_TWELVE = 12;
    Integer EVENT_ID_13 = 13;
    Integer EVENT_ID_14 = 14;
    Integer EVENT_ID_15 = 15;
}
