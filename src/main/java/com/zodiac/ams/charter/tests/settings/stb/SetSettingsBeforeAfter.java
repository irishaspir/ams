package com.zodiac.ams.charter.tests.settings.stb;


import com.zodiac.ams.charter.emuls.che.settings.SettingsSampleBuilder;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;


/**
 * Created by SpiridonovaIM on 01.02.2017.
 */
public class SetSettingsBeforeAfter extends StepsForSettings {

    @BeforeMethod
    public void checkDataInBD() {
        new SettingsSampleBuilder(DEVICE_ID_NUMBER_2, "./dataCHE", "/sample1.xml").messageBuild();
        Logger.info("setDefaultValue");
        setDefaultValue(options, MAC_NUMBER_2);
        Logger.info("setMigrationFlagOne");
        setMigrationFlagOne(MAC_NUMBER_2);
        runSTB();
        runCHESettings();
        startTestTime = getCurrentTimeTest();
    }

    @AfterMethod
    public void stop() {
        stopSTB();
        stopCHESettings();
    }
}
