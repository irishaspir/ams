package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.rudp.cd.message.CDRequestBuider;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.*;
import static com.zodiac.ams.charter.tests.StoriesList.DVR_RECORDING_PLAYBACK_START;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

/**
 * Created by SpiridonovaIM on 31.07.2017.
 */
public class DVRRecordingPlaybackStartReceiveErrorTests extends AssertLog{

    protected CDRequestBuider cdRequestBuider = new CDRequestBuider();
    protected List<CDProtocolOption> message;


    @BeforeMethod
    public void checkDataInBD() {
        startTestTime = getCurrentTimeTest();
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.2  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackReceiveError(int recordingId, int offset) {
        runSTB(DVR_PLAYBACK_START_REQUEST_ID, ERROR_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
        assertErrorLog(log, deviceId, DVR_PLAYBACK_START_REQUEST_ID, ERROR_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.3  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackReceiveIncorrectParams(int recordingId, int offset) {
        runSTB(DVR_PLAYBACK_START_REQUEST_ID, INCORRECT_PARAMETER_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
        assertErrorLog(log, deviceId, DVR_PLAYBACK_START_REQUEST_ID, INCORRECT_PARAMETER_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.4  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackReceiveSTNInStandbyMode(int recordingId, int offset) {
        runSTB(DVR_PLAYBACK_START_REQUEST_ID, STB_IN_STANDBY_MODE_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
        assertErrorLog(log, deviceId, DVR_PLAYBACK_START_REQUEST_ID, STB_IN_STANDBY_MODE_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.5  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackReceiveTargetRecordingNotExist(int recordingId, int offset) {
        runSTB(DVR_PLAYBACK_START_REQUEST_ID, TARGET_NOT_EXIST_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
        assertErrorLog(log, deviceId, DVR_PLAYBACK_START_REQUEST_ID, TARGET_NOT_EXIST_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.6  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackReceiveParameterValueIsOutOfValidRange(int recordingId, int offset) {
        runSTB(DVR_PLAYBACK_START_REQUEST_ID, OUT_OF_RANGE_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
        assertErrorLog(log, deviceId, DVR_PLAYBACK_START_REQUEST_ID, OUT_OF_RANGE_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.7  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackReceiveDVRIsNotReady(int recordingId, int offset) {
        runSTB(DVR_PLAYBACK_START_REQUEST_ID, DVR_NOT_READY_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
        assertErrorLog(log, deviceId, DVR_PLAYBACK_START_REQUEST_ID, DVR_NOT_READY_CD_ID, PARAMS);
    }


    @AfterMethod
    public void stop() {
        stopSTB();
    }

}
