package com.zodiac.ams.charter.tests.settings.che;

import com.zodiac.ams.charter.emuls.che.settings.SettingsSampleBuilder;
import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.services.settings.SettingsOptions;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.DataSettingsHelper;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.io.File;
import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostRangeSettingsValue;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.*;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_CHE_CHECK_RANGE;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

/**
 * Created by SpiridonovaIM on 19.06.2017.
 */
public class SendSettingsCheckRangeTest extends StepsForSettings {


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Default Audio Range ")
    @TestCaseId("169596")
    @Test(dataProvider = "DefaultAudioForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveDefaultAudio(String newValue) {
        test(DEFAULT_AUDIO, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check DVS Range ")
    @TestCaseId("169596")
    @Test(dataProvider = "DVSForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveDVS(String newValue) {
        test(DVS, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Stereo Audio Range ")
    @TestCaseId("169596")
    @Test(dataProvider = "StereoAudioForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveStereoAudio(String newValue) {
        test(STEREO_AUDIO, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Video Output Format Range ")
    @TestCaseId("169596")
    @Test(dataProvider = "VideoOutputFormatForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveVideoOutputFormat(String newValue) {
        test(VIDEO_OUTPUT_FORMAT, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check SD on HD Aspect Ratio Range ")
    @TestCaseId("169596")
    @Test(dataProvider = "SDOnHDAspectRatioForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveSDOnHDAspectRatio(String newValue) {
        test(SD_ON_HD_ASPECT_RATIO, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check HD on HD Aspect Ratio Range ")
    @TestCaseId("169596")
    @Test(dataProvider = "HDOnHDAspectRatioForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveHDOnHDAspectRatio(String newValue) {
        test(HD_ON_SD_ASPECT_RATIO, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Channel Bar Position ")
    @TestCaseId("169596")
    @Test(dataProvider = "ChannelBarPositionForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveChannelBarPosition(String newValue) {
        test(CHANNEL_BAR_POSITION, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check HD Auto Tune ")
    @TestCaseId("169596")
    @Test(dataProvider = "HDAutoTuneForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveHDAutoTune(String newValue) {
        test(HD_AUTO_TUNE, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Mini Guide Display Position ")
    @TestCaseId("169596")
    @Test(dataProvider = "MiniGuideDisplayPositionForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveMiniGuideDisplayPosition(String newValue) {
        test(MINI_GUIDE_DISPLAY_POSITION, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Enable Close Captioning")
    @TestCaseId("169596")
    @Test(dataProvider = "EnableCloseCaptioningForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveEnableCloseCaptioning(String newValue) {
        test(ENABLE_CLOSE_CAPTION, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Caption Background Color")
    @TestCaseId("169596")
    @Test(dataProvider = "CCCaptionBackgroundColorForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCCaptionBackgroundColor(String newValue) {
        test(CC_CAPTION_BACKGROUND_COLOR, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Text Color")
    @TestCaseId("169596")
    @Test(dataProvider = "CCTextColorForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCTextColor(String newValue) {
        test(CC_TEXT_COLOR, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Text Size")
    @TestCaseId("169596")
    @Test(dataProvider = "CCTextSizeForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCTextSize(String newValue) {
        test(CC_TEXT_SIZE, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Fonts")
    @TestCaseId("169596")
    @Test(dataProvider = "CCFontsForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCFonts(String newValue) {
        test(CC_FONTS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Background Opacity ")
    @TestCaseId("169596")
    @Test(dataProvider = "CCBackgroundOpacityForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCBackgroundOpacity(String newValue) {
        test(CC_BACKGROUND_OPACITY, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Character Edge Attributes ")
    @TestCaseId("169596")
    @Test(dataProvider = "CCCharacterEdgeAttributesForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCCharacterEdgeAttributes(String newValue) {
        test(CC_CHARACTER_EDGE_ATTRIBUTES, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Window Color")
    @TestCaseId("169596")
    @Test(dataProvider = "CCWindowColorForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCWindowColor(String newValue) {
        test(CC_WINDOW_COLOR, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Window Opacity")
    @TestCaseId("169596")
    @Test(dataProvider = "CCWindowOpacityForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCWindowOpacity(String newValue) {
        test(CC_WINDOW_OPACITY, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Language")
    @TestCaseId("169596")
    @Test(dataProvider = "CCLanguageForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCLanguage(String newValue) {
        test(CC_LANGUAGE, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check CC Caption Simplification")
    @TestCaseId("169596")
    @Test(dataProvider = "CCCaptionSimplificationForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCCCaptionSimplification(String newValue) {
        test(CC_CAPTION_SIMPLIFICATION, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Channel Filters")
    @TestCaseId("169596")
    @Test(dataProvider = "ChannelFiltersForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveChannelFilters(String newValue) {
        test(CHANNEL_FILTERS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Channel Sorting")
    @TestCaseId("169596")
    @Test(dataProvider = "ChannelSortingForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveChannelSorting(String newValue) {
        test(CHANNEL_SORTING, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check AC Outlet")
    @TestCaseId("169596")
    @Test(dataProvider = "ACOutletForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveACOutlet(String newValue) {
        test(AC_OUTLET, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Auto Power Off")
    @TestCaseId("169596")
    @Test(dataProvider = "AutoPowerOffForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveAutoPowerOff(String newValue) {
        test(AUTO_POWER_OFF, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Front Panel Display")
    @TestCaseId("169596")
    @Test(dataProvider = "FrontPanelDisplayForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveFrontPanelDisplay(String newValue) {
        test(FRONT_PANEL_DISPLAY_BRIGHTNESS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Tune Away Warning")
    @TestCaseId("169596")
    @Test(dataProvider = "TuneAwayWarningForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveTuneAwayWarning(String newValue) {
        test(TUNE_AWAY_WARNING, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Enable Disable Parental Controls ")
    @TestCaseId("169596")
    @Test(dataProvider = "EnableDisableParentalControlsForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveEnableDisableParentalControls(String newValue) {
        test(ENABLE_DISABLE_PARENTAL_CONTROLS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Rating Locks")
    @TestCaseId("169596")
    @Test(dataProvider = "RatingLocksForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveRatingLocks(String newValue) {
        test(RATING_LOCKS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Content Locks")
    @TestCaseId("169596")
    @Test(dataProvider = "ContentLocksForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveContentLocks(String newValue) {
        test(CONTENT_LOCKS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Hide Adult Content")
    @TestCaseId("169596")
    @Test(dataProvider = "HideAdultContentForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveHideAdultContent(String newValue) {
        test(HIDE_ADULT_TITLES, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Caller ID Notification")
    @TestCaseId("169596")
    @Test(dataProvider = "CallerIDNotificationForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCallerIDNotification(String newValue) {
        test(CALLER_ID_NOTIFICATION, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Caller ID Display")
    @TestCaseId("169596")
    @Test(dataProvider = "CallerIDDisplayForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveCallerIDDisplay(String newValue) {
        test(CALLER_ID_DISPLAY, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Purchase PIN Active")
    @TestCaseId("169596")
    @Test(dataProvider = "PurchasePINActiveForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceivePurchasePINActive(String newValue) {
        test(PURCHASE_PIN_ACTIVATE, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Warning On Tune To Recording")
    @TestCaseId("169596")
    @Test(dataProvider = "WarningOnTuneToRecordingForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveWarningOnTuneToRecording(String newValue) {
        test(WARNING_ON_TUNE_TO_RECORDING, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Record Duplicates")
    @TestCaseId("169596")
    @Test(dataProvider = "RecordDuplicatesForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveRecordDuplicates(String newValue) {
        test(RECORD_DUPLICATES, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Sleep Timer")
    @TestCaseId("169596")
    @Test(dataProvider = "SleepTimerForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveSleepTimer(String newValue) {
        test(SLEEP_TIMER, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Front Panel Display Brightness")
    @TestCaseId("169596")
    @Test(dataProvider = "FrontPanelDisplayBrightnessForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveFrontPanelDisplayBrightness(String newValue) {
        test(FRONT_PANEL_DISPLAY_BRIGHTNESS, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Time Display")
    @TestCaseId("169596")
    @Test(dataProvider = "TimeDisplayForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveTimeDisplay(String newValue) {
        test(TIME_DISPLAY, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Guide Presentation")
    @TestCaseId("169596")
    @Test(dataProvider = "GuidePresentationForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveGuidePresentation(String newValue) {
        test(GUIDE_PRESENTATION, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Guide Narration")
    @TestCaseId("169596")
    @Test(dataProvider = "GuideNarrationForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveGuideNarration(String newValue) {
        test(GUIDE_NARRATION, newValue);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Energy Savings Mode")
    @TestCaseId("169596")
    @Test(dataProvider = "EnergySavingsModeForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveEnergySavingsMode(String newValue) {
        test(ENERGY_SAVING_MODE, newValue);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Turn On/Off Reminders")
    @TestCaseId("169596")
    @Test(dataProvider = "TurnOn/OffRemindersForJson", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveTurnOnOffRemindersForJson(String newValue) {
        test(TURN_ON_OFF_REMINDER, newValue);
    }

//    @Features(FeatureList.SETTINGS)
//    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE_CHECK_RANGE)
//    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, check Tune To Recording Warning")
//    @TestCaseId("169596")
//    @Test(dataProvider = "TuneToRecordingWarningForJson", dataProviderClass = DataSettingsHelper.class)
//    public void sendSettingsReceiveTuneToRecordingWarningForJson( String newValue) {
//        test(TUNE_TO_RECORDING_WARNING, newValue);
//    }

    @Step
    private void test(SettingsOptions setting, String newValue) {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(setting)
                .build();
        String response = sendPostRangeSettingsValue(getDeviceIdByMac(mac), options, newValue);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);

        switch (setting.getOption().getName()) {
            case "Caller ID Notification":
            case "Turn On/Off Reminders":
                if (!newValue.equals("Off"))
                    checkMessageToSTB(options, newValue);
                break;
            default: {
                checkMessageToSTB(options, newValue);
            }
        }
    }

    @Step
    private void checkMessageToSTB(ArrayList<SettingsOption> options, String newValue) {
        Logger.info("STB message");
        ArrayList<String> patList = settingsSTBResponse.stbSetSettingsRequestPattern(options, newValue);
        Logger.info("Pattern!!! " + patList.toString());
        ArrayList<String> emList = settingsRudpHelper.getSettingsStbResponse();
        Logger.info("Emulator!!! " + emList.toString());

        assertEquals(emList, patList, INCORRECT_SETTINGS);
    }


    @BeforeMethod
    public void allSettingsWithout59And60Keys() {
        delete55And59And60Keys(options);
        startTestTime = getCurrentTimeTest();
    }

    @BeforeClass
    protected void runEmulators() {
        new SettingsSampleBuilder(DEVICE_ID_NUMBER_2, "./dataCHE", "/sample1.xml").messageBuild();
        setDefaultValue(options, MAC_NUMBER_2);
        runCHESettings();
        runSTB();
    }

    @AfterClass
    protected void stopEmulators() {
        stopCHESettings();
        stopSTB();
        new File("./dataCHE/sample1.xml").delete();
    }

}