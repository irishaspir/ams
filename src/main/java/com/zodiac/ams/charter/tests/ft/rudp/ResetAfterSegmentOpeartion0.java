
package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.CLIENT_SESSION_TIMEOUT;
import com.zodiac.ams.charter.services.ft.rudp.packet.ResetErrorResponsePacket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnyResult;

public class ResetAfterSegmentOpeartion0 extends ResetAfterSegmentOperation{
    
     private CountDownLatch countDown = new CountDownLatch(1);
    
    @Override
    public boolean isSuccessAfterSegment(int sessionId,ChannelFuture future, int segment, int index, int total)  throws Exception {
        if (sessionId != firstSessionId){
            countDown.await(CLIENT_SESSION_TIMEOUT, TimeUnit.MINUTES);
            return getExpectedAnyResult(sessionId,future);
        }
        
        if (index <= total/2){
            boolean ok = getExpectedSuccessResult(sessionId,future);
            if (index == total/2 && ok){
                ResetErrorResponsePacket pkg = new ResetErrorResponsePacket();
                pkg.setFileId(0);
                pkg.setRemoteAddress(sender.getRemoteAddress());
                ChannelFuture cf = sender.sendResponse(pkg);
                if (!cf.await(CLIENT_SESSION_TIMEOUT, TimeUnit.MINUTES))
                    setError(sessionId,"Unable to send ERROR Reset packet. Request is not completed.");
                else 
                if (!cf.isSuccess())
                    setError(sessionId,"Unable to send ERROR Reset packet. Error is detected.");  
                countDown.countDown();
            }
            return ok;
        }
        return getExpectedFailureResult(sessionId,future);
    }
    
}
