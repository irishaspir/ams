package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSTFactory;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsST;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.Test;

public class PublisherTestTSZip extends PublisherTestBase {
    
    private static final List<String> excludedTests = new ArrayList<String>(){{
        add("deleteFileFromCarouselNoRecord");
        add("addModifyExistingNoRecord");
        add("updateExistingNoRecord");
    }};
    
    public PublisherTestTSZip(){
        FTAllureUtils.copyAnnotations(PublisherTestBase.class , PublisherTestTSZip.class);
        FTAllureUtils.replaceStory(PublisherTestTSZip.class, this);
        FTAllureUtils.replaceDescription(PublisherTestTSZip.class, this);
        FTAllureUtils.exclude(PublisherTestTSZip.class, this);
        
        this.isZipped = true;
        this.aLocEnabled = false;
        this.dalinc = false;
        this.carouselType = CarouselType.ST;
    }
    
    @Override
    public String getStory(){
        return "TS Broadcaster Publisher (zipped=true)";
    }
    
    @Override
    public boolean isExcluded(String test) {
        return excludedTests.contains(test);
    }
    
    
    private void initExecutors(){
        CarouselHostsST hosts = new CarouselHostsST(properties, isZipped, aLocEnabled, dalinc);
        for (int i=0;i<hosts.size();i++){
            CarouselHost host = hosts.get(i);
            ICarousel carousel = CarouselSTFactory.getCarouselST(host,host.isZipped(),aLocEnabled);
            carousel.createRemoteCarouselDir();
            if (0 == i)
                carouselsIn.add(carousel);
            carouselsOut.add(carousel);
        }
    }
    
    @Override
    protected void generateServerXML(){
        Map<String,String> params = new HashMap<>();
        params.put("useZippedZdtsIni", "true");        
        ServerXMLHelper helper=new ServerXMLHelper(properties, params);
        helper.save("server.xml");
        initExecutors();
    }
   
    @Test
    @Override
    public void addNewFileOnCarousel() throws Exception{
        super.addNewFileOnCarousel();
    }
    
    @Test
    @Override
    public void addNewFileOnCarouselNoFlags() throws Exception{
        super.addNewFileOnCarouselNoFlags();
    }
    
    @Test
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        super.addExistingFileOnCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarousel() throws Exception{
        super.deleteFileFromCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        super.deleteFileFromCarouselInvalidCarousel();    
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidName() throws Exception {
        super.deleteFileFromCarouselInvalidName();
    } 
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        super.deleteFileFromCarouselNoFileNoRecord();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFile() throws Exception {
        super.deleteFileFromCarouselNoFile(); 
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoRecord() throws Exception {
        super.deleteFileFromCarouselNoRecord();
    }
     
    @Test
    @Override
    public void updateExisting() throws Exception {
        super.updateExisting();
    }
    
    @Test
    @Override
    public void updateExistingInvalidCarousel() throws Exception {
        super.updateExistingInvalidCarousel();
    }
    
    @Test
    @Override
    public void updateExistingInvalidName() throws Exception {
        super.updateExistingInvalidName();
    }
      
    @Test
    @Override
    public void updateNoExisting() throws Exception {
        super.updateNoExisting();
    }
    
    @Test
    @Override
    public void addModifyExisting() throws Exception {
        super.addModifyExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExisting() throws Exception {
        super.addModifyNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExistingNoFlags() throws Exception{
        super.addModifyNoExistingNoFlags();
    }
        
    @Test
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        super.addModifyNoVerIncExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncNoExisting() throws Exception {
        super.addModifyNoVerIncNoExisting();
    }
    
    @Test
    @Override
    public void clearOld() throws Exception{
        super.clearOld();
    }
    
    @Test
    @Override
    public void addModifyExistingNoFile() throws Exception {
        super.addModifyExistingNoFile();
    }
    
    @Test
    @Override
    public void updateExistingNoFile() throws Exception {
        super.updateExistingNoFile();
    }
    
    @Test
    @Override
    public void addModifyExistingNoRecord() throws Exception {
        super.addModifyExistingNoRecord();
    }
    
    @Test
    @Override
    public void updateExistingNoRecord() throws Exception {
        super.updateExistingNoRecord();
    }
    
    @Test
    @Override
    public void incVersionExisting() throws Exception {
        super.incVersionExisting();
    }
    
    @Test
    @Override
    public void incVersionNoExisting() throws Exception {
        super.incVersionNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        super.addModifyNoVerIncIncVersionExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        super.addModifyNoVerIncIncVersionNoExisting();
    }
    
    @Test
    @Override
    public void unknownAction() throws Exception{
        super.unknownAction();
    }
    
    @Test
    @Override
    public void unknownServer() throws Exception {
        super.unknownServer();
    }
    
    @Test
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
   
}
