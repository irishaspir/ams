package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSFTP;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsHTTP;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;


public class PublisherTest extends PublisherTestBase {
    
    private static final Map<String,String> tarantulaIds = new HashMap<String,String>(){{
        put("addNewFileOnCarousel", "8.1.6");
        put("addNewFileOnCarouselNoFlags", "8.1.37");
        put("addExistingFileOnCarousel", "8.2.47");
        put("deleteFileFromCarousel", "8.1.26");
        put("deleteFileFromCarouselInvalidName", "8.1.46");
        put("deleteFileFromCarouselNoFile", "8.2.71");
        put("deleteFileFromCarouselNoRecord", "8.2.51");
        put("updateExisting", "8.1.21");
        put("addModifyExisting", "8.2.31");
        put("addModifyNoExisting", "8.1.16");
        put("addModifyNoExistingNoFlags", "8.1.41");
        put("addModifyNoVerIncExisting", "8.2.67");
        put("addModifyNoVerIncNoExisting", "8.2.35");
        put("clearOld", "8.1.33");
        put("updateExistingNoFile", "8.2.9");
        put("incVersionExisting", "8.1.30");
        put("incVersionNoExisting", "8.2.58");
        put("unknownAction", "8.1.18");
        put("unknownServer", "8.1.2");
    }};
    
    public PublisherTest(){
        FTAllureUtils.copyAnnotations(PublisherTestBase.class, PublisherTest.class);
        FTAllureUtils.replaceStory(PublisherTest.class, this);
        FTAllureUtils.replaceDescription(PublisherTest.class, this);
        FTAllureUtils.exclude(PublisherTest.class, this);
        
        this.isZipped = false;
        this.aLocEnabled = false;
        this.dalinc = false;
        this.carouselType = CarouselType.HTTP;
    }
    
    @Override
    public String getStory(){
        return "HTTP Publisher (zipped = false)";
    }
    
    @Override
    public String getDescription(String test, String oldDescription){
        String id = tarantulaIds.get(test);
        return (null == id) ? oldDescription : id + " " + oldDescription;
    }
    
    @Override
    public boolean isExcluded(String test) {
        return false;
    }
      
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.save("server.xml");
        initExecutors();
        
    }
    
    void initExecutors(){
        CarouselHostsHTTP hosts = new CarouselHostsHTTP(properties,isZipped,aLocEnabled,dalinc);
        for (int i=0;i<hosts.size();i++){
            CarouselHost host = hosts.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());;
            ICarousel carousel = new CarouselSFTP(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            if (0 == i)
                carouselsIn.add(carousel);
            carouselsOut.add(carousel);
        }
    }
    
    
    @Test
    @Override
    public void addNewFileOnCarousel() throws Exception{
        super.addNewFileOnCarousel();
    }
    
    @Test
    @Override
    public void addNewFileOnCarouselNoFlags() throws Exception{
        super.addNewFileOnCarouselNoFlags();
    }
    
    @Test
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        super.addExistingFileOnCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarousel() throws Exception{
        super.deleteFileFromCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        super.deleteFileFromCarouselInvalidCarousel();    
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidName() throws Exception {
        super.deleteFileFromCarouselInvalidName();
    } 
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        super.deleteFileFromCarouselNoFileNoRecord();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFile() throws Exception {
        super.deleteFileFromCarouselNoFile(); 
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoRecord() throws Exception {
        super.deleteFileFromCarouselNoRecord();
    }
     
    @Test
    @Override
    public void updateExisting() throws Exception {
        super.updateExisting();
    }
    
    @Test
    @Override
    public void updateExistingInvalidCarousel() throws Exception {
        super.updateExistingInvalidCarousel();
    }
    
    @Test
    @Override
    public void updateExistingInvalidName() throws Exception {
        super.updateExistingInvalidName();
    }
      
    @Test
    @Override
    public void updateNoExisting() throws Exception {
        super.updateNoExisting();
    }
    
    @Test
    @Override
    public void addModifyExisting() throws Exception {
        super.addModifyExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExisting() throws Exception {
        super.addModifyNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExistingNoFlags() throws Exception{
        super.addModifyNoExistingNoFlags();
    }
        
    @Test
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        super.addModifyNoVerIncExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncNoExisting() throws Exception {
        super.addModifyNoVerIncNoExisting();
    }
    
    @Test
    @Override
    public void clearOld() throws Exception{
        super.clearOld();
    }
    
    @Test
    @Override
    public void addModifyExistingNoFile() throws Exception {
        super.addModifyExistingNoFile();
    }
    
    @Test
    @Override
    public void updateExistingNoFile() throws Exception {
        super.updateExistingNoFile();
    }
    
    @Test
    @Override
    public void addModifyExistingNoRecord() throws Exception {
        super.addModifyExistingNoRecord();
    }
    
    @Test
    @Override
    public void updateExistingNoRecord() throws Exception {
        super.updateExistingNoRecord();
    }
    
    @Test
    @Override
    public void incVersionExisting() throws Exception {
        super.incVersionExisting();
    }
    
    @Test
    @Override
    public void incVersionNoExisting() throws Exception {
        super.incVersionNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        super.addModifyNoVerIncIncVersionExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        super.addModifyNoVerIncIncVersionNoExisting();
    }
    
    @Test
    @Override
    public void unknownAction() throws Exception{
        super.unknownAction();
    }
    
    @Test
    @Override
    public void unknownServer() throws Exception {
        super.unknownServer();
    }
    
    @Test
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
    
}
