package com.zodiac.ams.charter.tests.ft.dvr;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.handlers.UniversalListener;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.dvr.*;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.WeekDays;
import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

import static com.zodiac.ams.charter.tests.FeatureList.DVR;

public class DVRRecordingTest extends DVRTest{
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.1 [DVR][Record] Single: Space is needed for new recordings: HD: Without Duplicates")
    @Test(priority=2)
    public void test_6_1_1_1() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.2 [DVR][Record] Single: Space is needed for new recordings : HD: With Duplicates")
    @Test(priority=2)
    public void test_6_1_1_2() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.3 [DVR][Record] Single: Space is needed for new recordings : SD: Without Duplicates")
    @Test(priority=2)
    public void test_6_1_1_3() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.4 [DVR][Record] Single: Space is needed for new recordings : SD: With Duplicates")
    @Test(priority=2)
    public void test_6_1_1_4() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.5 [DVR][Record] Single: User delete : HD: Without Duplicates")
    @Test(priority=2)
    public void test_6_1_1_5() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.6 [DVR][Record] Single: User delete : HD: With Duplicates")
    @Test(priority=2)
    public void test_6_1_1_6() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.7 [DVR][Record] Single: User delete : SD: Without Duplicates")
    @Test(priority=2)
    public void test_6_1_1_7() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.8 [DVR][Record] Single: User delete : SD: With Duplicates")
    @Test(priority=2)
    public void test_6_1_1_8() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.9 [DVR][Record] Series: Space is needed for new recordings: HD: All episodes: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_9() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.10 [DVR][Record] Series: Space is needed for new recordings: HD: All episodes: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_10() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.11 [DVR][Record] Series: Space is needed for new recordings: HD: New episodes only: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_11() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.12 [DVR][Record] Series: Space is needed for new recordings: HD: New episodes only: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_12() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.13 [DVR][Record] Series: Space is needed for new recordings: SD: All episodes: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_13() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.14 [DVR][Record] Series: Space is needed for new recordings: SD: All episodes: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_14() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.15 [DVR][Record] Series: Space is needed for new recordings: SD: New episodes only: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_15() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.16 [DVR][Record] Series: Space is needed for new recordings: SD: New episodes only: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_16() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.17 [DVR][Record] Series: User delete: HD: All episodes: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_17() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.18 [DVR][Record] Series: User delete: HD: All episodes: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_18() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.19 [DVR][Record] Series: User delete: HD: New episodes only: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_19() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.20 [DVR][Record] Series: User delete: HD: New episodes only: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_20() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.21 [DVR][Record] Series: User delete: SD: All episodes: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_21() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.22 [DVR][Record] Series: User delete: SD: All episodes: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_22() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.23 [DVR][Record] Series: User delete: SD: New episodes only: Without duplicates")
    @Test(priority=2)
    public void test_6_1_1_23() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.24 [DVR][Record] Series: User delete: SD: New episodes only: With duplicates")
    @Test(priority=2)
    public void test_6_1_1_24() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.25 [DVR][Record] Manual: Space is needed for new recordings: HD: Sunday")
    @Test(priority=2)
    public void test_6_1_1_25() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.26 [DVR][Record] Manual: Space is needed for new recordings: HD: Saturday")
    @Test(priority=2)
    public void test_6_1_1_26() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.27 [DVR][Record] Manual: Space is needed for new recordings: HD: Wednesday")
    @Test(priority=2)
    public void test_6_1_1_27() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.28 [DVR][Record] Manual: Space is needed for new recordings: HD: Monday, Tuesday, Thursday")
    @Test(priority=2)
    public void test_6_1_1_28() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.29 [DVR][Record] Manual: Space is needed for new recordings: HD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday")
    @Test(priority=2)
    public void test_6_1_1_29() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.30 [DVR][Record] Manual: Space is needed for new recordings: SD: Sunday")
    @Test(priority=2)
    public void test_6_1_1_30() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.31 [DVR][Record] Manual: Space is needed for new recordings: HD: Saturday")
    @Test(priority=2)
    public void test_6_1_1_31() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.32 [DVR][Record] Manual: Space is needed for new recordings: SD: Wednesday")
    @Test(priority=2)
    public void test_6_1_1_32() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.33 [DVR][Record] Manual: Space is needed for new recordings: SD: Monday, Tuesday, Thursday")
    @Test(priority=2)
    public void test_6_1_1_33() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.34 [DVR][Record] Manual: Space is needed for new recordings: SD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday")
    @Test(priority=2)
    public void test_6_1_1_34() throws Exception {
        recordingTest();
    }

    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.35 [DVR][Record] Manual: User delete: HD: Sunday")
    @Test(priority=2)
    public void test_6_1_1_35() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.36 [DVR][Record] Manual: User delete: HD: Saturday")
    @Test(priority=2)
    public void test_6_1_1_36() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.37 [DVR][Record] Manual: User delete: HD: Wednesday")
    @Test(priority=2)
    public void test_6_1_1_37() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.38 [DVR][Record] Manual: User delete: HD: Monday, Tuesday, Thursday")
    @Test(priority=2)
    public void test_6_1_1_38() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.39 [DVR][Record] Manual: User delete: HD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday")
    @Test(priority=2)
    public void test_6_1_1_39() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.40 [DVR][Record] Manual: User delete: SD: Sunday")
    @Test(priority=2)
    public void test_6_1_1_40() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.41 [DVR][Record] Manual: User delete: HD: Saturday")
    @Test(priority=2)
    public void test_6_1_1_41() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.42 [DVR][Record] Manual: User delete: SD: Wednesday")
    @Test(priority=2)
    public void test_6_1_1_42() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.43 [DVR][Record] Manual: User delete: SD: Monday, Tuesday, Thursday")
    @Test(priority=2)
    public void test_6_1_1_43() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.1.44 [DVR][Record] Manual: User delete: SD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday")
    @Test(priority=2)
    public void test_6_1_1_44() throws Exception {
        recordingTest();
    }
            
    
    private void recordingTestError(Error error) throws Exception{
        recordingTestError(null, error.getCodeSTB(), new Error[] {error});
    }
    
    private void recordingTestError(Type type, int errorCode, Error error) throws Exception{
        recordingTestError(type, errorCode, new Error[] {error});
    }
    
    /**
     * 
     * @param errorCode STB error code
     * @param statusCode STB status codes
     * @param statusCodeHE AMS-HE status codes
     * @throws Exception 
     */
    private void recordingTestError(Type type, int errorCode, Error [] errors) throws Exception{
        RequestRecording request = new RequestRecording(deviceId, Method.SCHEDULE);
        
        for (Error error: errors){
            RecordingParam param = (null == type) ? RecordingParam.getRandom() : RecordingParam.getRandom(type);
            request.addParam(param);
        }
        
        ResponseRecording response = ResponseRecording.getRandom(request.getRepeat(), errorCode, errors);
        stepsRecordingTest(request, response, 0 == errorCode ? null : errorCode, errors);
    }
    
    
    /**
     * int values: ServiceId, ChannelNumber, StartTime, Duration, StartOffset, EndOffset
     * @throws Exception 
     */
    private void recordingNegativeIntCommonFieldsTest() throws Exception{
        List<Consumer<RecordingParam>> consumersNegative = Arrays.asList(
            t -> t.setServiceId("test"), t -> t.setServiceId(""), t -> t.setServiceId(null),
            t -> t.setChannelNumber("test"), t -> t.setChannelNumber(""), t -> t.setChannelNumber(null),
            t -> t.setStartTime("test"), t -> t.setStartTime(""), t -> t.setStartTime(null),
            t -> t.setDuration("test"), t -> t.setDuration(""), t -> t.setDuration(null),
            t -> t.setStartOffset("test"), t -> t.setStartOffset(""),
            t -> t.setEndOffset("test"), t -> t.setEndOffset(""));
        
        List<Consumer<RecordingParam>> consumersPositive = Arrays.asList(
            t -> t.setStartOffset(null),
            t -> t.setEndOffset(null)
        );
        recordingNegativeTestImpl(consumersNegative, consumersPositive);
    }
    
    private void recordingNegativeTestImpl(List<Consumer<RecordingParam>> consumersNegative, 
            List<Consumer<RecordingParam>> consumersPositive) throws Exception{
        
        StringBuilder error = new StringBuilder();
        int index = 0;
        
        RequestRecording request  = RecordingDescriptionParser.getRequestRecording(deviceId, FTAllureUtils.getDescription());
        
        if (null != consumersNegative){
            for (Consumer<RecordingParam> consumer: consumersNegative){
                index++;
                RequestRecording rr = new RequestRecording(request);
                consumer.accept(rr.getParam(0));
                try {
                    stepsRecordingNegativeTest(rr, 400);
                }
                catch(AssertionError err){
                    error.append(index).append(". ").append(err.getMessage()).append(" ");
                }
            }
        }
        
        if (null != consumersPositive){
            for (Consumer<RecordingParam> consumer: consumersPositive){
                index++;
                RequestRecording rr = new RequestRecording(request);
                consumer.accept(rr.getParam(0));
                ResponseRecording resp = ResponseRecording.getRandom(rr.getRepeat()); 
                try {
                    stepsRecordingTest(rr, resp);
                }
                catch(AssertionError err){
                    error.append(index).append(". ").append(err.getMessage()).append(" ");
                }
            }
        }
        
        if (0 != error.length())
            Assert.assertTrue(false, error.toString());
    }
    
    
    private void recordingPositiveSTBTest(Consumer<ResponseRecording> consumer) throws Exception{
        recordingNegativeSTBTest(null, consumer, null);
    }

    private void recordingNegativeSTBTest(RequestRecording request, Consumer<ResponseRecording> consumer) throws Exception{
        recordingNegativeSTBTest(request, consumer, Error.INAVLID_PARAMETER /*Error.UNKNOWN_ERROR*/);
    }
    
    private void recordingNegativeSTBTest(Consumer<ResponseRecording> consumer) throws Exception{
        recordingNegativeSTBTest(null, consumer, Error.INAVLID_PARAMETER /*Error.UNKNOWN_ERROR*/);
    }
    
    private void recordingNegativeSTBTest(RequestRecording request, Consumer<ResponseRecording> consumer, Error error) throws Exception{
        if (null == request){
            request = new RequestRecording(deviceId, Method.SCHEDULE);
            RecordingParam param = RecordingParam.getRandom();
            request.addParam(param);
        }
        
        ResponseRecording response = ResponseRecording.getRandom(request.getRepeat());
        consumer.accept(response);
        stepsRecordingTest(request, response, null, (null == error) ? null : new Error[] {error});
    }
    
    private void recordingNegativeTest() throws Exception{
        recordingNegativeTest(400);
    }
    
    private void recordingNegativeTest(int expectedStatus) throws Exception{
        RequestRecording request  = RecordingDescriptionParser.getRequestRecording(deviceId, FTAllureUtils.getDescription());
        stepsRecordingNegativeTest(request, expectedStatus);
    }
    
    private void recordingTest() throws Exception{
        RequestRecording request  = RecordingDescriptionParser.getRequestRecording(deviceId, FTAllureUtils.getDescription());
        ResponseRecording response = ResponseRecording.getRandom(request.getRepeat()); 
        stepsRecordingTest(request, response);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.1 [DVR][Record] Error code '1 - ERROR, no operation has been accomplished'")
    @Test(priority=3)
    public void test_6_1_2_1() throws Exception {
        recordingTestError(Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.2 [DVR][Record] Error code '7 - DVR is not authorized on the STB'")
    @Test(priority=3)
    public void test_6_1_2_2() throws Exception {
        recordingTestError(Error.Error7);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.3 [DVR][Record] Error code '10 - DVR is not ready (still initializing)'")
    @Test(priority=3)
    public void test_6_1_2_3() throws Exception {
        recordingTestError(Error.Error10);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.4 [DVR][Record] Error code '13 - Invalid parameter was specified'")
    @Test(priority=3)
    public void test_6_1_2_4() throws Exception {
        recordingTestError(Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.5 [DVR][Record] Error code '14 - No programs were scheduled with the request'")
    @Test(priority=3)
    public void test_6_1_2_5() throws Exception {
        recordingTestError(Error.Error14);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.6 [DVR][Record] Error code '16 - Unknown error '")
    @Test(priority=3)
    public void test_6_1_2_6() throws Exception {
        recordingTestError(Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.2.7 [DVR][Record] Error code '17 - STB is busy '")
    @Test(priority=3)
    public void test_6_1_2_7() throws Exception {
        recordingTestError(Error.Error17);
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.1 [DVR][Record] Single: Status '1 - ERROR, no operation has been accomplished'")
    @Test(priority=4)
    public void test_6_1_3_1_1() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.10 [DVR][Record] Single: Status '16 - Unknown error'")
    @Test(priority=4)
    public void test_6_1_3_1_10() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.11 [DVR][Record] Single: Status '18 - No EPG available for the program'")
    @Test(priority=4)
    public void test_6_1_3_1_11() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error18);
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.12 [DVR][Record] Single: Random status (successful and unsuccessful)")
    @Test(priority=4)
    public void test_6_1_3_1_12() throws Exception {
        Error[] errors = Error.getRecordingErrors();
        recordingTestError(Type.Single, 0, RandomUtils.getRandomObject(errors, errors.length, false));
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.13 [DVR][Record] Single: All statuses")
    @Test(priority=4)
    public void test_6_1_3_1_13() throws Exception {
        recordingTestError(Type.Single, 0, Error.getRecordingErrors());
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.2 [DVR][Record] Single: Status '3 - No target program exists'")
    @Test(priority=4)
    public void test_6_1_3_1_2() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error3);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.3 [DVR][Record] Single: Status '4 - There is a schedule conflict'")
    @Test(priority=4)
    public void test_6_1_3_1_3() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error4);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.4 [DVR][Record] Single: Status '8 - The DVR is full'")
    @Test(priority=4)
    public void test_6_1_3_1_4() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error8);
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.5 [DVR][Record] Single: Status '11 - Requested program is already scheduled'")
    @Test(priority=4)
    public void test_6_1_3_1_5() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error11);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.1.6 [DVR][Record] Single: Status '12 - PPV program being scheduled for recording has not been purchased'")
    @Test(priority=4)
    public void test_6_1_3_1_6() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error12);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.7 [DVR][Record] Series: Status '13 - Invalid parameter was specified'")
    @Test(priority=4)
    public void test_6_1_3_1_7() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.8 [DVR][Record] Series: Status '14 - No programs were scheduled with the request'")
    @Test(priority=4)
    public void test_6_1_3_1_8() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error14);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.9 [DVR][Record] Series: Status '15 - No subscription to the channel'")
    @Test(priority=4)
    public void test_6_1_3_1_9() throws Exception {
        recordingTestError(Type.Single, 0, Error.Error15);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.1 [DVR][Record] Series: Status '1 - ERROR, no operation has been accomplished'")
    @Test(priority=4)
    public void test_6_1_3_2_1() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.10 [DVR][Record] Series: Status '16 - Unknown error'")
    @Test(priority=4)
    public void test_6_1_3_2_10() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.11 [DVR][Record] Series: Status '18 - No EPG available for the program'")
    @Test(priority=4)
    public void test_6_1_3_2_11() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error18);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.12 [DVR][Record] Series: Random status (successful and unsuccessful)")
    @Test(priority=4)
    public void test_6_1_3_2_12() throws Exception {
        Error[] errors = Error.getRecordingErrors();
        recordingTestError(Type.Series, 0, RandomUtils.getRandomObject(errors, errors.length, false));
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.13 [DVR][Record] Series: All statuses")
    @Test(priority=4)
    public void test_6_1_3_2_13() throws Exception {
        recordingTestError(Type.Series, 0, Error.getRecordingErrors());
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.2 [DVR][Record] Series: Status '3 - No target program exists'")
    @Test(priority=4)
    public void test_6_1_3_2_2() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error3);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.3 [DVR][Record] Series: Status '4 - There is a schedule conflict'")
    @Test(priority=4)
    public void test_6_1_3_2_3() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error4);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.4 [DVR][Record] Series: Status '8 - The DVR is full'")
    @Test(priority=4)
    public void test_6_1_3_2_4() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error8);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.5 [DVR][Record] Series: Status '11 - Requested program is already scheduled'")
    @Test(priority=4)
    public void test_6_1_3_2_5() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error11);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.6 [DVR][Record] Series: Status '12 - PPV program being scheduled for recording has not been purchased'")
    @Test(priority=4)
    public void test_6_1_3_2_6() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error12);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.7 [DVR][Record] Series: Status '13 - Invalid parameter was specified'")
    @Test(priority=4)
    public void test_6_1_3_2_7() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.8 [DVR][Record] Series: Status '14 - No programs were scheduled with the request'")
    @Test(priority=4)
    public void test_6_1_3_2_8() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error14);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.2.9 [DVR][Record] Series: Status '15 - No subscription to the channel'")
    @Test(priority=4)
    public void test_6_1_3_2_9() throws Exception {
        recordingTestError(Type.Series, 0, Error.Error15);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.1 [DVR][Record] Manual: Status '1 - ERROR, no operation has been accomplished'")
    @Test(priority=4)
    public void test_6_1_3_3_1() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.10 [DVR][Record] Manual: Status '16 - Unknown error'")
    @Test(priority=4)
    public void test_6_1_3_3_10() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.11 [DVR][Record] Manual: Status '18 - No EPG available for the program'")
    @Test(priority=4)
    public void test_6_1_3_3_11() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error18);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.12 [DVR][Record] Manual: Random status (successful and unsuccessful)")
    @Test(priority=4)
    public void test_6_1_3_3_12() throws Exception {
        Error[] errors = Error.getRecordingErrors();
        recordingTestError(Type.Manual, 0, RandomUtils.getRandomObject(errors, errors.length, false));
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.13 [DVR][Record] Manual: All statuses")
    @Test(priority=4)
    public void test_6_1_3_3_13() throws Exception {
        recordingTestError(Type.Manual, 0, Error.getRecordingErrors());
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.2 [DVR][Record] Manual: Status '3 - No target program exists'")
    @Test(priority=4)
    public void test_6_1_3_3_2() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error3);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.3 [DVR][Record] Manual: Status '4 - There is a schedule conflict'")
    @Test(priority=4)
    public void test_6_1_3_3_3() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error4);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.4 [DVR][Record] Manual: Status '8 - The DVR is full'")
    @Test(priority=4)
    public void test_6_1_3_3_4() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error8);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.5 [DVR][Record] Manual: Status '11 - Requested program is already scheduled'")
    @Test(priority=4)
    public void test_6_1_3_3_5() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error11);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.6 [DVR][Record] Series: Status '12 - PPV program being scheduled for recording has not been purchased'")
    @Test(priority=4)
    public void test_6_1_3_3_6() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error12);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.7 [DVR][Record] Manual: Status '13 - Invalid parameter was specified'")
    @Test(priority=4)
    public void test_6_1_3_3_7() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.8 [DVR][Record] Manual: Status '14 - No programs were scheduled with the request'")
    @Test(priority=4)
    public void test_6_1_3_3_8() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error14);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.3.9 [DVR][Record] Manual: Status '15 - No subscription to the channel'")
    @Test(priority=4)
    public void test_6_1_3_3_9() throws Exception {
        recordingTestError(Type.Manual, 0, Error.Error15);
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.3.4.1 [DVR][Record] Random Type: Random Statuses")
    @Test(priority=4)
    public void test_6_1_3_4_1() throws Exception {
        Error[] errors = Error.getRecordingErrors();
        recordingTestError(null, 0, RandomUtils.getRandomObject(errors, errors.length, false));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.1 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] serviceId is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_1() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.10 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] duration is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_10() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.11 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] duration is empty")
    @Test(priority=5)
    public void test_6_1_4_1_11() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.12 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] duration is missing")
    @Test(priority=5)
    public void test_6_1_4_1_12() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.13 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] startOffset is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_13() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.14 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] startOffset is empty")
    @Test(priority=5)
    public void test_6_1_4_1_14() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.4.1.15 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] startOffset is missing")
    @Test(priority=5)
    public void test_6_1_4_1_15() throws Exception {
         recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.16 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] endOffset is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_16() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.17 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] endOffset is empty")
    @Test(priority=5)
    public void test_6_1_4_1_17() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.4.1.18 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] endOffset is missing")
    @Test(priority=5)
    public void test_6_1_4_1_18() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.19 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] serviceId is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_19() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.2 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] serviceId is empty")
    @Test(priority=5)
    public void test_6_1_4_1_2() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.20 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] serviceId is empty")
    @Test(priority=5)
    public void test_6_1_4_1_20() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.21 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] serviceId is missing")
    @Test(priority=5)
    public void test_6_1_4_1_21() throws Exception {
        recordingNegativeTest();
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.22 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] channelNumber is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_22() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.23 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] channelNumber is empty")
    @Test(priority=5)
    public void test_6_1_4_1_23() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.24 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] channelNumber is missing")
    @Test(priority=5)
    public void test_6_1_4_1_24() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.25 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] startTime is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_25() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.26 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] startTime is empty")
    @Test(priority=5)
    public void test_6_1_4_1_26() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.27 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] startTime is missing")
    @Test(priority=5)
    public void test_6_1_4_1_27() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.28 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] duration is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_28() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.29 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] duration is empty")
    @Test(priority=5)
    public void test_6_1_4_1_29() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.3 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] serviceId is missing")
    @Test(priority=5)
    public void test_6_1_4_1_3() throws Exception {
        recordingNegativeTest();
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.30 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] duration is missing")
    @Test(priority=5)
    public void test_6_1_4_1_30() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.31 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] startOffset is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_31() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.32 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] startOffset is empty")
    @Test(priority=5)
    public void test_6_1_4_1_32() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.4.1.33 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] startOffset is missing")
    @Test(priority=5)
    public void test_6_1_4_1_33() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.34 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] endOffset is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_34() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.35 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] endOffset is empty")
    @Test(priority=5)
    public void test_6_1_4_1_35() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.4.1.36 [DVR][Record][Single: Space is needed for new recordings : HD: With Duplicates] endOffset is missing")
    @Test(priority=5)
    public void test_6_1_4_1_36() throws Exception {
        recordingTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.37 [DVR][Record][Single: Space is needed for new recordings : SD: Without Duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_1_37() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.38 [DVR][Record][Single: Space is needed for new recordings : SD: With Duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_1_38() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.39 [DVR][Record][Single: User delete : HD: Without Duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_1_39() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.4 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] channelNumber is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_4() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.40 [DVR][Record][Single: User delete: HD: With Duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_1_40() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.41 [DVR][Record][Single: User delete: SD: Without Duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_1_41() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.42 [DVR][Record][Single: User delete: SD: With Duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_1_42() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.43 [DVR][Record] Single: Check saveDays, recordingFormat, recordDuplicates")
    @Test(priority=5)
    public void test_6_1_4_1_43() throws Exception {
        recordingNegativeTestImpl(Arrays.asList( 
            t -> t.setSaveDays("test"), t -> t.setSaveDays(""), t -> t.setSaveDays(null),
            t -> t.setRecordingFormat("test"), t -> t.setRecordingFormat(""), t -> t.setRecordingFormat(null),
            t -> t.setRecordDuplicates("test"), t-> t.setRecordDuplicates("")),
        Arrays.asList(t -> t.setRecordDuplicates(null)));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.5 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] channelNumber is empty")
    @Test(priority=5)
    public void test_6_1_4_1_5() throws Exception {
        recordingNegativeTest();
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.6 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] channelNumber is missing")
    @Test(priority=5)
    public void test_6_1_4_1_6() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.7 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] startTime is invalid")
    @Test(priority=5)
    public void test_6_1_4_1_7() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.8 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] startTime is empty")
    @Test(priority=5)
    public void test_6_1_4_1_8() throws Exception {
        recordingNegativeTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.1.9 [DVR][Record][Single: Space is needed for new recordings : HD: Without Duplicates] startTime is missing")
    @Test(priority=5)
    public void test_6_1_4_1_9() throws Exception {
        recordingNegativeTest();
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.10 [DVR][Record][Series: User delete: HD: All episodes: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_10() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.11 [DVR][Record][Series: User delete: HD: New episodes only: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_11() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }

    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.12 [DVR][Record][Series: User delete: HD: New episodes only: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_12() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }

    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.13 [DVR][Record][Series: User delete: SD: All episodes: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_13() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.14 [DVR][Record][Series: User delete: SD: All episodes: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_14() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.15 [DVR][Record][Series: User delete: SD: New episodes only: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_15() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.16 [DVR][Record][Series: User delete: SD: New episodes only: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_16() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.17 [DVR][Record] Series: Check saveDays, recordingFormat, episodesDefinition, recordDuplicate")
    @Test(priority=5)
    public void test_6_1_4_2_17() throws Exception {
        recordingNegativeTestImpl(Arrays.asList( 
            t -> t.setSaveDays("test"), t -> t.setSaveDays(""), t -> t.setSaveDays(null),
            t -> t.setRecordingFormat("test"), t -> t.setRecordingFormat(""), t -> t.setRecordingFormat(null),
            t -> t.setEpisodesDefinition("test"), t->t.setEpisodesDefinition(""),
            t -> t.setRecordDuplicates("test"), t-> t.setRecordDuplicates("")),
        Arrays.asList(
            t -> t.setEpisodesDefinition(null),
            t -> t.setRecordDuplicates(null)));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.2 [DVR][Record][Series: Space is needed for new recordings: HD: All episodes: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_2() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.3 [DVR][Record][Series: Space is needed for new recordings: HD: New episodes only: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_3() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.4 [DVR][Record][Series: Space is needed for new recordings: HD: New episodes only: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_4() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.5 [DVR][Record][Series: Space is needed for new recordings: SD: All episodes: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_5() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.6 [DVR][Record][Series: Space is needed for new recordings: SD: All episodes: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_6() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.7 [DVR][Record][Series: Space is needed for new recordings: SD: New episodes only: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_7() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.8 [DVR][Record][Series: Space is needed for new recordings: SD: New episodes only: With duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_8() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.2.9 [DVR][Record][Series: User delete: HD: All episodes: Without duplicates] negative cases")
    @Test(priority=5)
    public void test_6_1_4_2_9() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.1 [DVR][Record][Manual: Space is needed for new recordings: HD: Sunday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_1() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.10 [DVR][Record][Manual: Space is needed for new recordings: SD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_10() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.11 [DVR][Record][Manual: User delete: HD: Sunday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_11() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.12 [DVR][Record][Manual: User delete: HD: Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_12() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.13 [DVR][Record][Manual: User delete: HD: Wednesday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_13() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.14 [DVR][Record][Manual: User delete: HD: Monday, Tuesday, Thursday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_14() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.15 [DVR][Record][Manual: User delete: HD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_15() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.16 [DVR][Record][Manual: User delete: SD: Sunday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_16() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.17 [DVR][Record][Manual: User delete: SD: Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_17() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.18 [DVR][Record][Manual: User delete: SD: Wednesday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_18() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.19 [DVR][Record][Manual: User delete: SD: Monday, Tuesday, Thursday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_19() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.2 [DVR][Record][Manual: Space is needed for new recordings: HD: Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_2() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.20 [DVR][Record][Manual: User delete: SD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_20() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.21 [DVR][Record] Manual: Check saveDays, recordingFormat, repeat")
    @Test(priority=5)
    public void test_6_1_4_3_21() throws Exception {
        recordingNegativeTestImpl(Arrays.asList( 
            t -> t.setSaveDays("test"), t -> t.setSaveDays(""), t -> t.setSaveDays(null),
            t -> t.setRecordingFormat("test"), t -> t.setRecordingFormat(""), t -> t.setRecordingFormat(null),
            t -> t.setRepeat("test"), t -> t.setRepeat("")),
        Arrays.asList(t -> t.setRepeat(null)));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.3 [DVR][Record][Manual: Space is needed for new recordings: HD: Wednesday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_3() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.4 [DVR][Record][Manual: Space is needed for new recordings: HD: Monday, Tuesday, Thursday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_4() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.5 [DVR][Record][Manual: Space is needed for new recordings: HD: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_5() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.6 [DVR][Record][Manual: Space is needed for new recordings: SD: Sunday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_6() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.7 [DVR][Record][Manual: Space is needed for new recordings: SD: Saturday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_7() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.8 [DVR][Record][Manual: Space is needed for new recordings: SD: Wednesday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_8() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.4.3.9 [DVR][Record][Manual: Space is needed for new recordings: SD: Monday, Tuesday, Thursday] negative cases")
    @Test(priority=5)
    public void test_6_1_4_3_9() throws Exception {
        recordingNegativeIntCommonFieldsTest();
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.1 [DVR][Record] First 'Count' is inadmissible")
    @Test(priority=6)
    public void test_6_1_5_1() throws Exception {
        recordingNegativeSTBTest(t -> t.setSize(1000));
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.10 [DVR][Record] 'STB scheduled checksum' is '0'")
    @Test(priority=6)
    public void test_6_1_5_10() throws Exception {
        recordingPositiveSTBTest(t -> t.setCrc(new Long(0)));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.11 [DVR][Record] 'STB scheduled checksum' is missing")
    @Test(priority=6)
    public void test_6_1_5_11() throws Exception {
        recordingNegativeSTBTest(t -> t.setCrc(null));
    }
    
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.12 [DVR][Record] 'STBSpaceStatus' is '0'")
    @Test(priority=6)
    public void test_6_1_5_12() throws Exception {
        recordingPositiveSTBTest(t -> t.setSpaceStatus(SpaceStatus.get0()));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.13 [DVR][Record] 'STBSpaceStatus' is missing")
    @Test(priority=6)
    public void test_6_1_5_13() throws Exception {
        recordingNegativeSTBTest(t -> t.setSpaceStatus(null));
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.14 [DVR][Record] 'PriorityVector' is '0'")
    @Test(priority=6)
    public void test_6_1_5_14() throws Exception {
        recordingPositiveSTBTest(t -> t.setPriority(new ArrayList<>()));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.15 [DVR][Record] 'PriorityVector' is missing")
    @Test(priority=6)
    public void test_6_1_5_15() throws Exception {
        recordingNegativeSTBTest(t -> t.setPriority(null));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.16 [DVR][Record] Second 'Count' is inadmissible")
    @Test(priority=6)
    public void test_6_1_5_16() throws Exception {
        recordingNegativeSTBTest(t -> t.setPrioritySize(1000));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.17 [DVR][Record] Second 'Count' is missing")
    @Test(priority=6)
    public void test_6_1_5_17() throws Exception {
        recordingNegativeSTBTest(t -> t.setPrioritySize(-1)); //-1 == missing
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.18 [DVR][Record] Second  'RecordingID' is inadmissible")
    @Test(priority=6)
    public void test_6_1_5_18() throws Exception {
        ArrayList<Integer> pr = new ArrayList<>();
        pr.add(-1);
        recordingPositiveSTBTest(t -> t.setPriority(pr));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.19 [DVR][Record] Second  'RecordingID' is missing")
    @Test(priority=6)
    public void test_6_1_5_19() throws Exception {
        ArrayList<Integer> pr = new ArrayList<>();
        recordingNegativeSTBTest(t -> {t.setPrioritySize(1); t.setPriority(pr);});
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.2 [DVR][Record] First 'Count' is missing")
    @Test(priority=6)
    public void test_6_1_5_2() throws Exception {
        recordingNegativeSTBTest(t -> t.setSize(-1)); //-1 == missing
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.20 [DVR][Record] Second  'RecordingID' is '0'")
    @Test(priority=6)
    public void test_6_1_5_20() throws Exception {
        ArrayList<Integer> pr = new ArrayList<>();
        pr.add(0);
        recordingPositiveSTBTest(t -> t.setPriority(pr));
    }
    
    @Features(DVR)
    //@Stories("Request recording. Negative")
    @Stories("Request recording. Positive")
    @Description("6.1.5.3 [DVR][Record] First  'RecordingID' is inadmissible")
    @Test(priority=6)
    public void test_6_1_5_3() throws Exception {
        DvrDbHelper.clearRecording(deviceId);
        DISABLE_CLEAR_RECORDING = true;
        try {
            DvrDbHelper.addRecording(deviceId, 1969);
            recordingPositiveSTBTest(t -> t.getParam(0).setRecordingId(1969));
            //recordingNegativeSTBTest(t -> t.getParam(0).setRecordingId(1969));
        }
        finally{
            DISABLE_CLEAR_RECORDING = false;    
        }
    }

    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.4 [DVR][Record] First 'RecordingID' is missing")
    @Test(priority=6)
    public void test_6_1_5_4() throws Exception {
        recordingNegativeSTBTest(t -> t.getParam(0).setRecordingId(null)); 
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.5 [DVR][Record] First  'RecordingID' is '0'")
    @Test(priority=6)
    public void test_6_1_5_5() throws Exception {
        recordingPositiveSTBTest(t -> t.getParam(0).setRecordingId(0));
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.6 [DVR][Record] 'Status' is nonexistent")
    @Test(priority=6)
    public void test_6_1_5_6() throws Exception {
        recordingNegativeSTBTest(t -> t.getParam(0).setStatusCode(1000)); 
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.7 [DVR][Record] 'Status' is missing")
    @Test(priority=6)
    public void test_6_1_5_7() throws Exception {
        recordingNegativeSTBTest(t -> t.getParam(0).setStatusCode(null)); 
    }
    
    @Features(DVR)
    @Stories("Request recording. Positive")
    @Description("6.1.5.8 [DVR][Record] 'UnitingParameter' is '0'")
    @Test(priority=1)
    public void test_6_1_5_8() throws Exception {
        RequestRecording request = new RequestRecording(deviceId, Method.SCHEDULE);
        RecordingParam param = RecordingParam.getRandom(Type.Manual);
        param.setRepeat(WeekDays.getRandomWeekDays());
        request.addParam(param);
        recordingNegativeSTBTest(request, t -> t.getParam(0).setUnitingParameter(0), Error.Error0); 
    }
    
    @Features(DVR)
    @Stories("Request recording. Negative")
    @Description("6.1.5.9 [DVR][Record] 'UnitingParameter' is missing'")
    @Test(priority=6)
    public void test_6_1_5_9() throws Exception {
        RequestRecording request = new RequestRecording(deviceId, Method.SCHEDULE);
        RecordingParam param = RecordingParam.getRandom(Type.Manual);
        param.setRepeat(WeekDays.getRandomWeekDays());
        request.addParam(param);
        recordingNegativeSTBTest(request, t -> t.getParam(0).setUnitingParameter(null)); 
    }
    
//    @Features(DVR)
//    @Stories("AMS log")
//    @Description("DVR Recording AMS log")
//    @Test(priority=100)
//    public void getDvrAmsLog() throws Exception {
//        getServerXml();
//        stopAndgetAMSLog(true);
//    } 
    
    /**
     * 
     * @param uri
     * @param request HE-AMS request
     * @param response AMS-HE response
     * @param expectedSTB expected STB response
     * @param actualSTB actual STB response
     * @param errorCode STB error code
     * @param statusCodeHE STB status codes
     * @throws Exception 
     */
    private void showAndVerifyRecording(String uri, 
        RequestRecording request, FTHttpUtils.HttpResult response, ResponseRecording expectedSTB, byte[] actualSTB,
        Integer errorCode, Error [] errors) throws Exception{
        
        RequestRecording he = new RequestRecording(request); 
        RequestRecording heTmp = new RequestRecording(request); //uniting parameter may be changed by stb
        ResponseRecording stb = new ResponseRecording(expectedSTB);
        ResponseRecording amsHE = new ResponseRecording(expectedSTB);
        
        List<RecordingParam> dbs = new ArrayList<>();
        
        List<RecordingParam> csActual = new ArrayList<>();
        List<RecordingParam> csExpected = new ArrayList<>();
        
        int size = he.getSize();
        
        for (int i=0; i < size; i++){
            RecordingParam he1 = he.getParam(i);
            ResponseRecordingParam stb1 = stb.getParam(i);
            ResponseRecordingParam stb2 = amsHE.getParam(i);
            
            Integer uniting = stb1.getUnitingParameter();
            if (null != uniting &&  0 != uniting)
                heTmp.getParam(i).setUnitingParameter(stb1.getUnitingParameter());
            
            if (null ==  errorCode){
                he1.setRecordingId(stb1.getRecordingId());
                heTmp.getParam(i).setRecordingId(stb1.getRecordingId());
            }
            
            stb1.setServiceId(he1.getServiceId());
            stb1.setStartTime(he1.getStartTime());
            
            stb2.setServiceId(he1.getServiceId());
            stb2.setStartTime(he1.getStartTime());
            if (null != errors && 0 != errors[i].getCodeHE()){
                stb2.setRecordingId(null);
                stb2.setStatusCode(errors[i].getCodeHE());
            }
            
            dbs.add(DvrDbHelper.getDVRRecordingParam(FTConfig.getMac(deviceId), stb1.getRecordingId()));
            
            Integer id = stb.getParam(i).getRecordingId();
            RecordingParam act = DvrDbHelper.getCassandraRecordingParam(deviceId, (null == id) ? null : new Long(id));
            csActual.add(act);
            RecordingParam exp = RecordingParam.getTruncated(he1, act);
            csExpected.add(exp);
        }
        
        //show
        showHERequestToAMS(uri, request.toString()/*request*/, amsHE/*stb*/.toString()/*expected response*/, response/*actual response*/);
        showAMSRequestToSTB(he.getData()/*expected*/, actualSTB/*actual*/);
        if (null == errorCode){
            for (int i=0; i< size; i++){
                showDBRecording(null != errors && 0 != errors[i].getCodeHE() ?  null : heTmp.getParam(i) /*expected*/, dbs.get(i) /*actual*/);
                showCassandraRecording(null != errors && 0 != errors[i].getCodeHE() ?  null : csExpected.get(i) /*expected*/, csActual.get(i)/*actual*/);
            }
        }
        
        //verify
        Assert.assertTrue(200 == response.getStatus(),"Expected status: 200 Actual: "+ response.getStatus());
        if (null == errors)
            verifyJSON(amsHE.toJSON(), new JSONObject((String)response.getEntity()),"Expected and actual AMS response must be the same");
        else
            verifyJSON(amsHE.toJSON(new HashSet<String>(){{ add("startTime"); /*??? strange, but not a bug ???*/}}), new JSONObject((String)response.getEntity()),
                "Expected and actual AMS response must be the same", false);
        Assert.assertTrue(Arrays.equals(he.getData(), actualSTB), "Expected and actual request from AMS to STB must be the same");
        
        if (null == errorCode){
            for (int i=0; i<size; i++){
                if (null == errors || 0 == errors[i].getCodeHE()){
                    verifyJSON(heTmp.getParam(i).toJSON(), dbs.get(i).toJSON(), "Expected and actual information in DB DVR_RECORDING table must be the same");
                    verifyJSON(csExpected.get(i).toJSON(), csActual.get(i).toJSON(), "Expected and actual information in CASSANDRA RECORDINGS table must be the same");
                }
                else {
                    Assert.assertTrue(null == dbs.get(i), "Actual information in DB DVR_RECORDING table must be null");
                    Assert.assertTrue(null == csActual.get(i), "Actual information in CASSANDRA RECORDINGS table must be null");
                }
            }
        }
    }
    
    private void stepsRecordingNegativeTest(IDVRObject heRequest, int expectedStatus) throws Exception{
        String uri = properties.getDVRUri();
        FTHttpUtils.HttpResult amsResponse = FTHttpUtils.doPost(uri, heRequest.toString());
        showHERequestToAMS(uri, heRequest.toString(), null /*expected*/, amsResponse /*actual*/);
        Assert.assertTrue(expectedStatus == amsResponse.getStatus(), "AMS Response: Expected=" + expectedStatus + " Actual=" + amsResponse.getStatus());
    }
    
    private void stepsRecordingTest(IDVRObject heRequest, IDVRObject stbResponse) throws Exception{
        stepsRecordingTest(heRequest, stbResponse, null, null);
    }
    
    private static boolean DISABLE_CLEAR_RECORDING = false;
    
    private void stepsRecordingTest(IDVRObject heRequest, IDVRObject stbResponse, Integer errorCode, Error [] errors) throws Exception{
        CharterStbEmulator stb = null;
        DataConsumer consumer =  null;
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20*1_000);
        
        if (!DISABLE_CLEAR_RECORDING)
            DvrDbHelper.clearRecording(deviceId);
        
        try {
            Semaphore semaphore = new Semaphore(0);
            consumer = new DataConsumer(semaphore);
            stb = createCharterSTBEmulator(STB_PROPS);
            stb.registerConsumer(consumer, HandlerType.Universal);
            UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
            listener.setResponse(stbResponse.getData());            
            startCharterSTBEmulator(stb);
            
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult amsResponse = FTHttpUtils.doPost(uri, heRequest.toString());
//            Thread.sleep(60*60*1_000);
            
            
            byte[] actual = consumer.getData();
            if (null == errorCode || 0 == errorCode)
                showAndVerifyRecording(uri, (RequestRecording) heRequest, amsResponse, (ResponseRecording) stbResponse, actual, errorCode, errors);
            else {
                showHERequestToAMS(uri, heRequest.toString(), null /*expected*/, amsResponse /*actual*/);
                Assert.assertTrue(errorCode == amsResponse.getStatus(), "AMS Response: Expected=" + errorCode + " Actual=" + amsResponse.getStatus());
            }
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
}
