package com.zodiac.ams.charter.tests.ft.rudp;

import com.zodiac.ams.common.http.FTHttpUtils;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckStartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSenderSession;
import ru.yandex.qatools.allure.annotations.Attachment;

public class Publisher {
    
    @Attachment(value = "Error code", type ="text/plain")
    static String showErrorCode(int fileId,ErrorCodeType expected){
        return "Expected error: "+expected+"\n"+getFileStatistics(fileId);
    }
    
    @Attachment(value = "File Transfer Information", type ="text/plain")
    static String showFileTransferStatictics(int fileId){
        return getFileStatistics(fileId,true);
    }
    
    @Attachment(value = "File Transfer Information", type ="text/plain")
    static String showFileTransferStatictics(int fileId,boolean showData){
        return getFileStatistics(fileId,showData);
    }
    
    @Attachment(value = "Segment Size", type ="text/plain")
    static String showSegmentSize(int fileId,int expected) throws Exception{
        return "Expected segment size: "+expected+"\n"+getFileStatistics(fileId);
    }
    
     
    @Attachment(value = "Session Statistic", type ="text/plain")
    static synchronized String showSessionsStatistics(SessionsStatistic stat) throws Exception{
        return stat.toString();
    }
    
    
    @Attachment(value = "Test Statistic", type ="text/plain")
    static String showTestStatistics(int total,int failed) throws Exception{
        return "Total file transfers: "+total+" Failed: "+failed;
    }
    
    
    @Attachment(value = "Test Statistic", type ="text/plain")
    static String showHTTPStatus(String uri,byte [] request,FTHttpUtils.HttpResult response) throws Exception {
        if (200 == response.getStatus())
            return "URI: "+uri+"\nREQUEST:\n"+FTUtils.toHexString(request)+
                "\n\nRESPONSE: "+response.getStatus()+"\n"+response;
        else
            return "URI: "+uri+"\nRESPONSE: "+response.getStatus()+"\n"+response;
    }
    
    static String getFileStatistics(int fileId){
        return getFileStatistics(fileId,false);
    }
    
    static String getFileStatistics(int fileId,boolean showData) {
        StringBuilder sb = new StringBuilder();
        try{
            sb.append("File ID=").append(fileId).append("\n");
            RUDPTestingSession ts = getTestingSession(fileId);
            IRUDPSenderSession ss = (null == ts) ? null : ts.getRUDPSenderSession();
            RUDPTestingPackage tp = (null == ts) ? null : ts.getRUDPTestingPackage();
            AckStartPacket ap = (null == ts) ? null : ts.getAckStartPacket();
            if (null != ts){
                byte [] localData = (null == tp) ? null : tp.getData();
                if (null != localData)
                    sb.append("Local data: ").append(localData.length).append(" bytes\n");
                byte [] remoteData = ts.getRemoteData();
                if (null != remoteData)
                    sb.append("Remote data: ").append(remoteData.length).append(" bytes\n");
                if (null != ap)
                    sb.append("Ack Start packet: ").append(ap).append("\n");
                String msg =  ts.getMsg();
                if (null != msg && !msg.trim().equals(""))
                    sb.append("\n").append(msg).append("\n");
                msg = ts.getErrorMsg();
                if (null != msg && !msg.trim().equals(""))
                    sb.append("Error Message: ").append(msg).append("\n");
                ErrorPacket ep =  (null == ss ) ? null : ss.getErrorPacket();
                if (null != ep)
                    sb.append("Error packet: ").append(ep).append("\n");
                sb.append("Success: ").append(ts.isSuccess()).append("\n");
                if (showData){
                    sb.append("\n");
                    if (null != localData)
                        sb.append("LOCAL [").append(FTUtils.getIntFromBytes(localData)).append("]:\n").append(FTUtils.toHexString(localData)).append("\n\n");
                    if (null != remoteData)
                        sb.append("REMOTE [").append(FTUtils.getIntFromBytes(remoteData)).append("]:\n").append(FTUtils.toHexString(remoteData)).append("\n\n");
                }
            }
        }
        catch(Exception ex){
            return ex.getMessage();
        }
        return sb.toString();
    }
}
