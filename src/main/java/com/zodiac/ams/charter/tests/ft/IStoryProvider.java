package com.zodiac.ams.charter.tests.ft;

public interface IStoryProvider {
    String getStory(); 
}
