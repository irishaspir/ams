package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class InvalidResponseVerifier1 extends InvalidResponseVerifier0{
    
    protected final static String RESULT = "Error";

    @Override
    public boolean verify(FTHttpUtils.HttpStringResult response) {
        super.verify(response);
        try {
            JSONObject obj = new JSONObject(response.getEntity());
            JSONArray arr = obj.getJSONArray("resultList");
            int length = arr.length();
            for (int i=0;i<length;i++){
                obj = arr.getJSONObject(i);
                String str = obj.getString("result");
                if (!RESULT.equalsIgnoreCase(str.trim()))
                    Assert.assertTrue(false,"Expected result: \""+RESULT+"\" Found: \""+str+"\"");
            } 
        }
        catch(Exception ex){
             Assert.assertTrue(false,"Exception: "+ex.getMessage());
        }
        return true;
    }
}
