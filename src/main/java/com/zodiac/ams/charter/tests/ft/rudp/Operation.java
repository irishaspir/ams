package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.CLIENT_SESSION_TIMEOUT;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.jboss.netty.channel.ChannelFuture;
import org.testng.Assert;

class Operation implements IOperation{
    RUDPTestingSendManager sender;
    List<Integer> sessionsIds = new ArrayList<>();
    Integer firstSessionId;
    
    Operation(){
    }
    
    @Override
    public void setRUDPTestingSendManager(RUDPTestingSendManager sender){
        this.sender = sender;
    }
    
    @Override
    public synchronized void addSessionId(Integer id){
        sessionsIds.add(id);
        if (null == firstSessionId)
            firstSessionId = new Integer(id);
    }
    
    @Override
    public void publish() throws Exception{
        for (int id: sessionsIds)
            Publisher.showFileTransferStatictics(id);
    }
    
    @Override
    public boolean isSuccess(){
        for (int id: sessionsIds){
            RUDPTestingSession ts = getTestingSession(id);
            Assert.assertTrue(null != ts,"RUDP Testing Session id="+id+" must not be null");
            String msg = (null == ts.getErrorMsg() || ts.getErrorMsg().trim().equals("")) ? "Success flag is not set" : ts.getErrorMsg();
            Assert.assertTrue(ts.isSuccess(),ts.getErrorMsg());
            RUDPTestingPackage pk=ts.getRUDPTestingPackage();
            Assert.assertTrue(null != pk,"RUDPTestingPackage must not be null");
            Assert.assertTrue(Arrays.equals(pk.getData(),ts.getRemoteData()),"Local and remote messages must be the same");
        }
        return true;
    } 
    
    static void setError(int sessionId,String msg){
        setError(getTestingSession(sessionId),msg);
    }
    
    static  void setError(RUDPTestingSession ts,String msg){
        if (null != ts){
            ts.setSuccess(false);
            ts.setErrorMsg(msg);
        }
    }
    
    
    static boolean getExpectedAnyResult(int sessionId,ChannelFuture future){
        return getExpectedResultImpl(ExpectedResult.ANY,sessionId,future,null);
    }
    
    static boolean getExpectedFailureResult(int sessionId,ChannelFuture future){
        return getExpectedFailureResult(sessionId,future,null);
    }
    
    static boolean getExpectedFailureResult(int sessionId,ChannelFuture future,ErrorCodeType expectedError){
        return getExpectedResultImpl(ExpectedResult.BAD, sessionId, future, expectedError);
    }
    
    static boolean getExpectedSuccessResult(int sessionId,ChannelFuture future){
        return getExpectedResultImpl(ExpectedResult.GOOD,sessionId,future,null);
    }
    
    private static boolean getExpectedResultImpl(ExpectedResult expected,int sessionId,ChannelFuture future,ErrorCodeType expectedError){
        RUDPTestingSession ts = null;
        try {
            ts =  getTestingSession(sessionId);
            boolean ok = future.await(CLIENT_SESSION_TIMEOUT, TimeUnit.MINUTES);
            if (!ok){
                setError(ts,"Unable to send packet. Request is not completed");
                return false;
            }
            switch(expected) {
                case BAD:
                    if (future.isSuccess()){
                        setError(ts,"Channel Error must be detected.");
                        //return false;
                    }
                    if (null != expectedError){
                        ErrorCodeType error=getErrorCodeType(sessionId);
                        if (!expectedError.equals(error)) {
                            setError(ts,"Error type is "+error+". Must be "+expectedError);
                            //return false;
                        }
                    }
                    break;
                case GOOD:
                    if (!future.isSuccess()){
                        setError(ts,"Unable to send packet. Channel Error is must not be detected.");
                        return false;
                    }
                    break;
            }
        }
        catch(Exception ex){
            setError(ts,ex.getMessage());
            return false;
        }
        return true;
    }
    
    private static enum ExpectedResult{
        ANY,BAD,GOOD;
    }
    
    static private boolean getExpectedSessionResultImpl(ExpectedResult expected,int sessionId)  throws Exception{
        RUDPTestingSession ts = null;
        try {
            ts = getTestingSession(sessionId);
            if (ExpectedResult.ANY.equals(expected) ||
                    (ExpectedResult.BAD.equals(expected) && !ts.isSuccess()) ||
                    (ExpectedResult.GOOD.equals(expected) && ts.isSuccess())){
                boolean ok=ts.getSemaphore().tryAcquire(1,CLIENT_SESSION_TIMEOUT, TimeUnit.MINUTES);
                if (!ok)
                    ts.setErrorMsg("Response form Charter HE emulator must be received");
                ts.setSuccess(ok);
            }
        }
        catch(Exception ex){
            setError(ts,ex.getMessage());
            return false;
        }  
        return true;
    }
    
    
    static boolean getExpectedAnySessionResult(int sessionId)  throws Exception{
        return getExpectedSessionResultImpl(ExpectedResult.ANY,sessionId);
    }
    
    static boolean getExpectedSuccessSessionResult(int sessionId)  throws Exception{
        return getExpectedSessionResultImpl(ExpectedResult.GOOD,sessionId);
    }
    
    static boolean getExpectedFailureSessionResult(int sessionId) throws Exception{
        return getExpectedSessionResultImpl(ExpectedResult.BAD,sessionId);
    }
    
    static ErrorCodeType getErrorCodeType(int sessionId){
        RUDPTestingSession ts = getTestingSession(sessionId);
        Assert.assertTrue(null != ts,"RUDP Testing Session id="+sessionId+" must not be null");
        ErrorCodeType error =  null;
        ErrorPacket packet=ts.getRUDPSenderSession().getErrorPacket();
        if (null != packet)
            error = packet.getErrorCode();
        return error;
    }
    

     

    @Override
    public boolean isSuccessAfterStart(int sessionId,ChannelFuture future)  throws Exception {
        return getExpectedSuccessResult(sessionId,future);
    }
    
    @Override
    public boolean isSuccessAfterData(int sessionId,ChannelFuture future)  throws Exception {
        return getExpectedSuccessResult(sessionId,future);
    }

    @Override
    public boolean isSuccessAfterSegment(int sessionId,ChannelFuture future, int segment, int index, int total)  throws Exception {
        return getExpectedSuccessResult(sessionId,future);
    }
    
    @Override
    public boolean isSuccessAfterSession(int sessionId)  throws Exception{
        return getExpectedSuccessSessionResult(sessionId);
    }
   
}
