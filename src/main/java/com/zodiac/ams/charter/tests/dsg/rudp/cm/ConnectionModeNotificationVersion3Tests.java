package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.standardSTBRunning;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_3_STRING;
import static com.zodiac.ams.charter.tomcat.logger.dsg.DsgLogKeyWords.ERROR_VERSION3_PROTOCOL;
import static com.zodiac.ams.common.helpers.Utils.intToIp;
import static com.zodiac.ams.common.helpers.Utils.ipToInt;

public class ConnectionModeNotificationVersion3Tests extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "hubId is absent")
    @Test(priority = 1)
    public void sendVersion3RequestId0HubIdIsAbsent() {
        message = version3Request.createMessage(mac);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertJson(mac, connectionMode, getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, standardSTBRunning);
        //   assertDB(messageTypesMap, mac);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "hubId is changed")
    @Test(dataProvider = "id", dataProviderClass = DSGDataProvider.class, priority = 1)
    public void sendVersion3RequestId0HubIdIs(int hubId) {
        Logger.info("!!!!!!!ConnectionMode: " + connectionMode);
        message = version3Request.createMessage(mac, (Object) hubId);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertJson(mac, connectionMode, (int) getBoxModel(), vendor, hubId);//boxmodel?????
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, (Object) hubId);
        //  assertDB(messageTypesMap, hubId, mac);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "ServiceGroupId is changed")
    @Test(dataProvider = "id", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion3RequestId0ServiceGroupIdIs(int serviceGroupId) {
        message = version3Request.createMessage(mac, hubId, serviceGroupId);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        //   assertJson(mac,connectionMode, getBoxModel(), hubId, serviceGroupId, "${json-unit.ignore}"); //boxmodel?????
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, hubId, serviceGroupId);
        //  assertDB(messageTypesMap, hubId, mac);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "loadBalancerIpAddress is changed")
    @Test(dataProvider = "ipAddress", dataProviderClass = DSGDataProvider.class, priority = 3)
    public void sendVersion3RequestId0LoadBalancerIpAddressIs(String loadBalancerIpAddressString) {
        loadBalancerIpAddress = ipToInt(loadBalancerIpAddressString);
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        //    assertJson(mac, connectionMode,getBoxModel(), hubId, serviceGroupId, intToIp(loadBalancerIpAddress)); //boxmodel?????
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, hubId, serviceGroupId, intToIp(loadBalancerIpAddress));
        //  assertDB(messageTypesMap, hubId, mac);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "SwVersion is changed")
    @Test(dataProvider = "id", dataProviderClass = DSGDataProvider.class, priority = 4)
    public void sendVersion3RequestId0SwVersion(int swVersion) {
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress, swVersion);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        //     assertJson(mac,connectionMode, getBoxModel(), hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion); //boxmodel?????
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion);
        //    assertDB(messageTypesMap, hubId, mac);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "SdvServiceGroupId is changed")
    @Test(dataProvider = "id", dataProviderClass = DSGDataProvider.class, priority = 5)
    public void sendVersion3RequestId0SdvServiceGroupIdIs(int sdvServiceGroupId) {
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress, swVersion, sdvServiceGroupId);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        List<String> log = readAMSLog();
        createAttachment(log);
        // assertJson(mac,connectionMode, getBoxModel(), hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId); //boxmodel?????
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId);
        //  assertDB(messageTypesMap, hubId, mac);
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "mcMacAddress is 0")
    @Test(priority = 6, dataProvider = "cmMacAddress", dataProviderClass = DSGDataProvider.class)
    public void sendVersion3RequestId0CmMavAddressIs(String cmMacAddress) {
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress, swVersion, sdvServiceGroupId, cmMacAddress);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        //  assertJson(mac,connectionMode, getBoxModel(), hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress); //boxmodel?????
        assertLog(log, mac, connectionMode, ERROR_VERSION3_PROTOCOL, hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress);
        //    assertDB(messageTypesMap, hubId, mac);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "serialNumber is 888")
    @Test(priority = 7, dataProvider = "serialNumber", dataProviderClass = DSGDataProvider.class)
    public void sendVersion3RequestId0SerialNumberIs(String serialNumber) {
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);

        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        //   assertJson(mac, connectionMode,getBoxModel(), hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress, serialNumber); //boxmodel?????
        //   assertDB(messageTypesMap, hubId, mac);

        assertLog(log, mac, connectionMode, "", hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress, serialNumber);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "cmIp is 888")
    @Test(priority = 8, dataProvider = "ipAddress", dataProviderClass = DSGDataProvider.class)
    public void sendVersion3RequestId0CmIpIs(String cmIpString) {
        cmIp = ipToInt(cmIpString);
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, cmIp);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
        // assertJson(mac,connectionMode, getBoxModel(), hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, intToIp(cmIp));
        //  assertDB(messageTypesMap, hubId, mac);
        assertLog(log, mac, connectionMode, "", hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, intToIp(cmIp));
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 3 " +
            "swVersionTmp is ")
    @Test(priority = 8, dataProvider = "ipAddress", dataProviderClass = DSGDataProvider.class)
    public void sendVersion3RequestId0SwVersionTmpIs(String cmIpString) {
        cmIp = ipToInt(cmIpString);
        message = version3Request.createMessage(mac, hubId, serviceGroupId, loadBalancerIpAddress, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, cmIp, swVersionTemp);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertJson(mac, connectionMode, getBoxModel(), vendor, hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, intToIp(cmIp));
        //  assertDB(messageTypesMap, hubId, mac);
        assertLog(log, mac, connectionMode, "", hubId, serviceGroupId, intToIp(loadBalancerIpAddress), swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, intToIp(cmIp));
    }
}
