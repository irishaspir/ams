package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC sessions number.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DcSessionsNumberTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSIONS_NUMBER_NEGATIVE + "1 - Check without Session #")
    @Description("4.04.1 [DC][Session #] Check without Session #" +
            "<Node ID><Hub ID><0><>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithoutSessionNr() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                // SessionsNumber must be null, this is the test purpose!
                .setSessionsNumber(null)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession();

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSIONS_NUMBER_NEGATIVE + "2 - Check with incorrect Session #")
    @Description("4.04.2 [DC][Session #] Check with incorrect Session #" +
            "<Node ID><Hub ID><0><            >" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionNr1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                // SessionsNumber must be an spaces in string, this is the test purpose!
                .setSessionsNumber("            ")

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession();

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSIONS_NUMBER_NEGATIVE + "2 - Check with incorrect Session #")
    @Description("4.04.2 [DC][Session #] Check with incorrect Session #" +
            "<Node ID><Hub ID><0><ertwtt>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionNr2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                // SessionsNumber must be a garbage string , this is the test purpose!
                .setSessionsNumber("ertwtt")

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession();

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSIONS_NUMBER_NEGATIVE + "2 - Check with incorrect Session #")
    @Description("4.04.2 [DC][Session #] Check with incorrect Session #" +
            "<Node ID><Hub ID><0><$#%5435>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionNr3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                // SessionsNumber must be a garbage string , this is the test purpose!
                .setSessionsNumber("$#%5435")

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession();

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
