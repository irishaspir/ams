package com.zodiac.ams.charter.tests.rollback.rollbackToRoviFirmware;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.rollback.StepsForRollback;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.rollback.RollbackPostHelper.responseJson;
import static com.zodiac.ams.charter.http.helpers.rollback.RollbackPostHelper.sendRollbackToRoviFirmware;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendRollbackToRoviCommandSTBWithStatusErrorTest extends StepsForRollback {

    @BeforeMethod
    public void beforeMethod(){
        runSTBRollbackWithErrorStatus();
        getTestTime();
    }

    @AfterMethod
    public void afterMethod(){
        stopSTBRollback();
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.POSITIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.8 Send \"Rollback To Rovi Firmware\" command when STB returns FAILED status ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWhenSTBReturnStatusError() {
        assertEquals(sendRollbackToRoviFirmware(DEVICE_ID_NUMBER_1), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getSuccessfulResponseWithStatusFailed(DEVICE_ID_NUMBER_1));
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), rollbackSTBRequestPattern.rollbackToRoviFirmwareRequest(),
                ROLLBACK_REQUEST_IS_INCORRECT);
        assertTrue( rollbackLogParser.compareLogAMSWithPatternStatusError(rollbackLogParser.getLogAMSForValid(log), DEVICE_ID_NUMBER_1));


    }
}
