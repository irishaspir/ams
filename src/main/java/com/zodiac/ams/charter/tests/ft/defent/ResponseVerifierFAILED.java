package com.zodiac.ams.charter.tests.ft.defent;

import java.util.HashMap;

public class ResponseVerifierFAILED extends ResponseVerifier{
    public ResponseVerifierFAILED(String macAddress) {
        super(macAddress, new HashMap<String,String>(){
            {put("status","FAILED"); put("errorMessage",null);}
        });
    }
}
