package com.zodiac.ams.charter.tests.ft.talkingguide;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.talkingguide.TGUtils;
import com.zodiac.ams.charter.services.ft.talkingguide.TalkingGuideItem;
import com.zodiac.ams.charter.services.ft.talkingguide.TalkingGuideMockServer;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static com.zodiac.ams.charter.tests.FeatureList.TALKING_GUIDE;

public class TalkingGuideTest extends TalkingGuideTestImpl{
    
            
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties,~ServerXMLHelper.TALKING_GUIDE_MASK);
        String remote = executor.getHomeDir()+"/tts.cfg";
        String local = getName("tts.cfg");
        executor.putFile(local, remote);
        createRemoteCacheDir();
        helper.addTalkingGuideService(properties.getLocalHost(),properties.getTTSPort(),remote,getRemoteCacheDir());
        helper.save("server.xml");
    }  
    
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send request to AMS with text to convert.TTS engine is unavailable.")
    @Test(priority=1)  
    public void ttsNotAvailable() throws Exception{
        String uri=getURI("QWERTY");
        FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(503 == response.getStatus(),"Invalid status");
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send malformed request to AMS (coding parameter is missing)")
    @Test(priority=2)
    public void codingIsMissing() throws Exception{
        String uri=properties.getTalkingGuideUri()+"?body=QWERTY&mac="+properties.getDeviceId();
        FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(400 == response.getStatus(),"Invalid status");
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send malformed request to AMS (coding parameter is invalid)")
    @Test(priority=3)
    public void codingIsInvalid() throws Exception{
        String uri=properties.getTalkingGuideUri()+"?coding=XXX&body=QWERTY&mac="+properties.getDeviceId();
        FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(400 == response.getStatus(),"Invalid status");
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send malformed request to AMS (body parameter is missing)")
    @Test(priority=4)
    public void textIsMissing() throws Exception{
        String uri=properties.getTalkingGuideUri()+"?coding="+DEFAULT_CODING+"&mac="+properties.getDeviceId();
        FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(400 == response.getStatus(),"Invalid status");
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send malformed request to AMS (body parameter is empty)")
    @Test(priority=5)
    public void textIsEmpty() throws Exception{
        String uri=properties.getTalkingGuideUri()+"?coding="+DEFAULT_CODING+"&body=&mac="+properties.getDeviceId();
        FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(400 == response.getStatus(),"Invalid status");
    }
    
    
    //--------------------------------------------------------------------------
    /*
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send malformed request to AMS (mac parameter is missing)")
    @Test(enabled=false)
    public void macIsMissing() throws Exception{
        String uri=properties.getTalkingGuideUri()+"?coding="+DEFAULT_CODING+"&body=QWERTYY";
        Utils.Result response=Utils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(400 == response.getId(),"Invalid status");
    }
    */
    
    
    /*
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send request to AMS with coding parameter contained unsupported value.")
    @Test(enabled=false)  
    public void unsupportedCodingParameter() throws Exception{
        String uri=getURI("mp3","QWERTY");
        Utils.Result response=Utils.doGet(uri);
        getHTTPGetStatus(uri,response);
        Assert.assertTrue(415 == response.getId(),"Invalid status");  
    }
    */
    //--------------------------------------------------------------------------
    
    
    
    @Features(TALKING_GUIDE)
    @Stories("Positive")
    @Description("Send request to AMS to convert text with macroses.")
    @Test(priority=8)  
    public void simpleTextWithMacros() throws Exception{
        positiveTestImpl("{tts1} {tts2} {tts3} "+System.currentTimeMillis(),getName("audio.wav"),getName("tts.cfg"));
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Positive")
    @Description("Send request to AMS to convert simple text containing macros, which is not defined in the configuration file")
    @Test(priority=9)  
    public void simpleTextWithUndefMacros() throws Exception{
        positiveTestImpl("{undef1} {undef2} {undef3} "+System.currentTimeMillis(),getName("audio.wav"),getName("tts.cfg"));
    }
    
    
    @Features(TALKING_GUIDE)
    @Stories("Positive")
    @Description("Send request to AMS to convert simple text without macroses.")
    @Test(priority=10)  
    public void simpleTextWithoutMacros() throws Exception{
        positiveTestImpl("QWERTYUIOPASDFGHJKLZXCVBNM qwertyuiopasdfghjklzxcvbnm"+System.currentTimeMillis(),getName("audio.wav"),null);
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("Send request to AMS to convert text.Charter Proxy emulator sends part of the file and reset the connection.")
    @Test(priority=11)
    public void interactionError() throws Exception {
        int lines1 = executor.getLineCounts(getRemoteCacheDir());
        negativeTestImpl("{tts1} {tts2} {tts3} "+System.currentTimeMillis(),getName("audio.wav"),getName("tts.cfg"),500);
        int lines2 = executor.getLineCounts(getRemoteCacheDir());
        Assert.assertTrue(lines1 == lines2, "Invalid cache size");
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Positive")
    @Description("1)Send request to AMS to convert text. 2)Send the same request with the same text. 3)AMS takes response form the cache without request to the emulator")
    @Test(priority=12)
    public void cacheTest() throws Exception {
        String text = "CACHE TEST {tts1} {tts2} {tts3} {undefined} "+System.currentTimeMillis();
        String sound = getName("audio4.wav");
        String config = getName("tts.cfg");
        String uri = getURI(TGUtils.parseHTTPRequestString(text));
        
        TalkingGuideMockServer server =  null;
        
        try{
            server=new TalkingGuideMockServer(Integer.parseInt(properties.getTTSPort()));
            server.start();
        
            //Send the first request
            positiveTestImpl(server, text, sound, config);
            
            //Send the second request 
            TalkingGuideItem item = new TalkingGuideItem(sound);
            GetExecutor thread=new GetExecutor(uri);
            thread.start();
            
            boolean b=server.addTalkingGuideItem(item, MAX_TIMEOUT_NEGATIVE, TimeUnit.MINUTES);
            Assert.assertTrue(!b,"Unexpected request to Talking Guide mock server");
            thread.join(MAX_TIMEOUT_POSITIVE*60*1000);
            FTHttpUtils.HttpResult response = thread.getResponse();
        
            getSTBtoAMSRequest(uri,response);
            Assert.assertTrue(200 == thread.getResponse().getStatus(),"Invalid response");
            Assert.assertTrue(Arrays.equals(item.getSound(), (byte[])response.getEntity()), "The real and expected sounds are not equals");
            
        }
        finally {
            if (null != server)
                server.shutdown();
        }
    }
    
    @Features(TALKING_GUIDE)
    @Stories("Negative")
    @Description("1)Send request to AMS to convert text. 2)Proxy emulator sends part of the file and reset the connection 3)Send the same request with the same text. 4)AMS mustn't take response form the cache")
    @Test(priority=13)
    public void cacheTestNegative() throws Exception {
        String text = "CACHE TEST NEGATIVE {tts1} {tts2} {tts3} {undefined} "+System.currentTimeMillis();
        String sound = getName("audio4.wav");
        String config = getName("tts.cfg");
        
        TalkingGuideMockServer server =  null;
        
        try{
            server=new TalkingGuideMockServer(Integer.parseInt(properties.getTTSPort()));
            server.start();
        
            //Send the first request
            negativeTestImpl(server, text, sound, config,0);
            
            //Send the second request
            positiveTestImpl(server, text, sound, config);
        }
        finally {
            if (null != server)
                server.shutdown();
        }
    }
    
    @Features(TALKING_GUIDE)
    @Stories("AMS logs")
    @Description("TalkingGuideTest AMS log")
    @Test(priority=14)
    public void getTalkingGuideTestAMSLog() throws Exception {
        stopAndgetAMSLog(true);
    }
    
  
}
