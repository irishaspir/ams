package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.connectionMode;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.vendor;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.standardSTBRunning;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_2_STRING;

public class ConnectionModeNotificationVersion2MigrationStartTests extends DsgBeforeAfter {


    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_2_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 2 AND <vendor> IS specified " +
            "migrationStart is absent")
    @Test(priority = 0)
    public void sendVersion2RequestId0MigrationStartIsNull() {
        message = version2Request.createMessage(mac);
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
        assertJson(mac,connectionMode, getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, "", standardSTBRunning);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_2_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 2 AND <vendor> IS specified " +
            "migrationStart is StandardSTBRunning")
    @Test(priority = 1)
    public void sendVersion2RequestId0MigrationStartIsStandardSTBRunning() {
        message = version2Request.createMessage(mac, String.valueOf(standardSTBRunning));
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
     //   assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
        assertJson(mac,connectionMode, getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, "", standardSTBRunning);
    }

}
