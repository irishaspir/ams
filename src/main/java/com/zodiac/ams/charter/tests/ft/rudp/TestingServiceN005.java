package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.getSegments;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getRUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.IDataPacketProvider;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacketSerializer;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureSessionResult;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import com.zodiac.ams.charter.services.ft.rudp.packet.PacketType;


public class TestingServiceN005 extends TestingService {
    
     private static final ErrorCodeType expected = ErrorCodeType.CRC32;
    
    TestingServiceN005(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    class DataPacketProvider implements IDataPacketProvider{
        
        private final RUDPTestingSession ts;
        private final int badSegmentNumber;
        
        private DataPacketProvider(RUDPTestingSession ts, int badSegmentNumber){
            this.ts = ts;
            this.badSegmentNumber = badSegmentNumber;
        }

        @Override
        public byte[] get(int sessionId, int segmentNumber, DataPacket dataPacket) {
            try {
                if (segmentNumber == badSegmentNumber){
                    byte[] data = dataPacket.getData();
                    //data[0] = 0;
                    data[data.length-1] = 0;
                    return RUDPTestingUtils.generateDataPacket(PacketType.DATA.packetType, sessionId, segmentNumber,data);
                }
                else
                    return DataPacketSerializer.toBytes(dataPacket);
            }
            catch(Exception ex){
                if (null != ts){
                    ts.setErrorMsg(ex.getMessage());
                    ts.setSuccess(false);
                }
                return null;
            }
        }
    }
    
    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = getTestingSession(sessionId);
            RUDPTestingPackage pk = getRUDPTestingPackage(sessionId);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (!getExpectedSuccessResult(sessionId,future))
                return;
            
            int count = pk.getSegmentsCount();
            int [] segments = getSegments(count,order);
            int badNumber = segments.length - 1; //CRC here
            //int badNumber = 0;
            
            sender.setDataPacketProvider(sessionId, new DataPacketProvider(ts,badNumber));
            
            int index = 0;
            for (int segment: segments){
                future=sender.sendSegment(sessionId,segment);
                if (index == segments.length-1)
                    getExpectedFailureResult(sessionId,future,expected);
                else
                if (!getExpectedSuccessResult(sessionId,future))
                        return;
                index++;
            }
        
            getExpectedFailureSessionResult(sessionId);
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            sender.removeDataPacketProvider(sessionId);
            if (null != this.countDown)
                countDown.countDown();
        }
    }
}
