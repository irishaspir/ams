package com.zodiac.ams.charter.tests.callerId.subscription;

import com.zodiac.ams.charter.http.helpers.settings.json.JsonDataUtils;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.services.settings.SettingsOptions;
import com.zodiac.ams.charter.tests.callerId.StepsForCallerId;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.setCallerIdNotificationOneInSettingsSTBDataV2;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.setCallerIdNotificationZeroInSettingsSTBDataV2;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.CALLER_ID_NOTIFICATION;

public class StepsForCallerIdSubscription extends StepsForCallerId {

    protected JsonDataUtils jsonDataUtils = new JsonDataUtils();

    @Step
    protected void turnOnCallerIdNotification(){
        setCallerIdNotificationOneInSettingsSTBDataV2(MAC_NUMBER_1);
        setCallerIdNotificationOneInSettingsSTBDataV2(MAC_NUMBER_2);
        setCallerIdNotificationOneInSettingsSTBDataV2(MAC_NUMBER_3);
    }

    @Step
    protected void turnOffCallerIdNotification(){
        setCallerIdNotificationZeroInSettingsSTBDataV2(MAC_NUMBER_1);
        setCallerIdNotificationZeroInSettingsSTBDataV2(MAC_NUMBER_2);
        setCallerIdNotificationZeroInSettingsSTBDataV2(MAC_NUMBER_3);
    }

    @Step
    protected ArrayList<SettingsOption> setCallerIdNotificationOn(){
        SettingsOptions.CALLER_ID_NOTIFICATION.getOption().setValue("On");
        SettingsOptions.CALLER_ID_NOTIFICATION.getOption().setDefaultValue("1");
        return SettingsOption.create().addOptions(CALLER_ID_NOTIFICATION).build();
    }

    @Step
    protected ArrayList<SettingsOption> setCallerIdNotificationOff(){
        SettingsOptions.CALLER_ID_NOTIFICATION.getOption().setValue("Off");
        SettingsOptions.CALLER_ID_NOTIFICATION.getOption().setDefaultValue("0");
        return SettingsOption.create().addOptions(CALLER_ID_NOTIFICATION).build();
    }

    @Step
    protected void waitTimeout(){
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step
    protected void waitThreeSeconds(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
