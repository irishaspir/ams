package com.zodiac.ams.charter.tests.settings.che;


import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.DataSettingsHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSyncV2Utils.isSettingsSyncV2Empty;
import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.*;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.*;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_CHE;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;


public class SendSetSettingsFromCHETest extends SetSettingsBeforeAfter {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.1 Send POST setSettings from CHE to AMS, mac is null, receive 404 error" +
            "(POST HTTP://host:port/ams/settings)")
    @TestCaseId("169593")
    @Test()
    public void sendSetSettingsReceiveError404MacIsNull() {
        String response = sendPostInvalidDeviceId(UNKNOWN_DEVICE_ID);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, ERROR_404);
        assertJsonEquals(jsonDataUtils.error404MacNotFound(UNKNOWN_DEVICE_ID), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), nullResponse(), requestToSTBMustBeAbsent);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.2 Send POST setSettings from CHE to AMS, settings are null, receive 500 error " +
            "(POST HTTP://host:port/ams/settings)")
    @TestCaseId("169594")
    @Test()
    public void sendSetSettingsReceiveError500JSONIsNull() {
        String response = sendPostEmptySettings();
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, ERROR_500);
        assertJsonEquals(jsonDataUtils.error500Set25(), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), nullResponse(), requestToSTBMustBeAbsent);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.4 Send POST setSettings from CHE to AMS, settings are valid, STB is available, receive settings list, " +
            "settings values in DB equals settings values on the Box (POST HTTP://host:port/ams/settings) ")
    @TestCaseId("169587")
    @Test()
    public void sendSettingsReceiveSettingsListNullRequestToSTB() {
        String response = sendPostSettingsAllList(DEVICE_ID_NUMBER_2);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), nullResponse(), requestToSTBMustBeAbsent);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.7 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, receive all settings list, " +
            "AMS sends Settings to a Box (POST HTTP://host:port/ams/settings) " +
            "key 55, key 59 and key 60 are never send from CHE")
    @TestCaseId("169591")
    @Test()
    public void sendSettingsReceiveSuccessAMSSendSettingsToBox() {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = delete55And59And60Keys(allSettings());
        changeKeyValue(options, mac);
        String response = sendPostSettingsDeviseIdNames(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPattern(options), INCORRECT_SETTINGS);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.12 Send two POST setSettings from CHE to AMS, settings are valid, STB is available, receive all settings list, " +
            "AMS sends Settings to a Box " +
            "cause there was problems with lock in second response " +
            "key 55, key 59 and key 60 are never send from CHE")
    @TestCaseId("169598")
    @Test()
    public void sendTwoSettingsRequestsReceiveTwoSuccessResponse() {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = delete55And59And60Keys(allSettings());
        changeKeyValue(options, mac);
        String response = sendPostSettingsDeviseIdNames(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPattern(options), INCORRECT_SETTINGS);
        changeKeyValue(options, mac);
        String response1 = sendPostSettingsDeviseIdNames(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response1, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPattern(options), INCORRECT_SETTINGS);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.8 Send POST setSettings from CHE to AMS, settings are valid, STB is available, some names are invalid in settings list, " +
            "AMS cloudSettings timeout not expired")
    @TestCaseId("169595")
    @Test()
    public void sendSettingsReceiveInvalidNames() {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(DVS)
                .addOptions(CC_FONTS)
                .addOptions(VIDEO_OUTPUT_FORMAT)
                .addOptions(DEFAULT_AUDIO)
                .addOptions(FAVORITES)
                .addOptions(LIVE_CONTENT_BUFFERING)
                .addOptions(RATING_LOCKS)
                .build();
        changeKeyValue(options, mac);
        String response = sendPostInvalidSettingsName(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.responseWithInvalidSettingsName(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPatternEachSecondName(options), INCORRECT_SETTINGS);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.8 Send POST setSettings from CHE to AMS, settings are valid, STB is available, name is invalid, " +
            "AMS cloudSettings timeout not expired")
    @TestCaseId("169595")
    @Test()
    public void sendSettingsReceiveInvalidName() {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(DVS)
                .build();
        String response = sendPostInvalidSettingsName(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.responseWithInvalidSettingsName(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), nullResponse(), INCORRECT_SETTINGS);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available,some values are invalid in settings list, " +
            "AMS cloudSettings timeout not expired")
    @TestCaseId("169596")
    @Test()
    public void sendSettingsReceiveIncorrectValues() {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(DVS)
                .addOptions(CC_FONTS)
                .addOptions(VIDEO_OUTPUT_FORMAT)
                .addOptions(DEFAULT_AUDIO)
                .addOptions(FAVORITES)
                .addOptions(LIVE_CONTENT_BUFFERING)
                .addOptions(RATING_LOCKS)
                .build();
        changeKeyValue(options, mac);
        String response = sendPostInvalidSettingsValue(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.responseWithInvalidSettingsValue(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPatternEachSecondName(options), INCORRECT_SETTINGS);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.10 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, " +
            "AMS cloudSettings timeout not expired, temporary table is empty")
    @TestCaseId("169592")
    @Test()
    public void sendSettingsReceiveSuccessSettingsAreInDBSettingsAreNotTemporaryTables() {
        String mac = MAC_NUMBER_2;
        changeKeyValue(options, mac);
        String response = sendPostSettingsDeviseIdNames(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        assertEquals(isSettingsSyncV2Empty(), true);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_CHE)
    @Description("1.4.9 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, value is invalid, " +
            "AMS cloudSettings timeout not expired")
    @TestCaseId("169596")
    @Test(dataProvider = "settingsListWithout55And59And60Keys", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveIncorrectValue( String name,SettingsOption setting) {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOption(setting)
                .build();
        String response = sendPostInvalidSettingsValue(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.responseWithInvalidSettingsValue(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), nullResponse(), INCORRECT_SETTINGS);
    }

}
