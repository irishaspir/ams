package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.AMS_SESSION_TIMEOUT;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import org.jboss.netty.channel.ChannelFuture;


public class TimeoutAfterSegmentOperation extends ErrorOperation{
    
    TimeoutAfterSegmentOperation(){
        super(ErrorCodeType.FILEID);
    }
    
    @Override
    public boolean isSuccessAfterStart(int sessionId,ChannelFuture future) throws Exception{
        return getExpectedSuccessResult(sessionId,future);
    }
    
    @Override
    public boolean isSuccessAfterData(int sessionId,ChannelFuture future)  throws Exception {
        return true;
    }

    /**
     * 
     * @param sessionId
     * @param future
     * @param segment
     * @param total >2
     * @return
     * @throws Exception 
     */
    @Override
    public boolean isSuccessAfterSegment(int sessionId,ChannelFuture future, int segment, int index, int total)  throws Exception {
        if (index <= total/2){
            boolean ok = getExpectedSuccessResult(sessionId,future);
            if (index == total/2 && ok)
                Thread.sleep(AMS_SESSION_TIMEOUT+15_0000);
            return ok;
        }
        return getExpectedFailureResult(sessionId,future);
    }
    
}
