package com.zodiac.ams.charter.tests.ft.dvr;

import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.FeatureList.DVR;


public class DVRLogTest extends DVRTest {
    
    @Override
    public boolean needRestart(){
        return false;
    }
    
    @Features(DVR)
    @Stories("AMS log")
    @Description("DVR AMS log")
    @Test(priority=100)
    public void getDvrAmsLog() throws Exception {
        getServerXml();
        stopAndgetAMSLog(true);
    }
}
