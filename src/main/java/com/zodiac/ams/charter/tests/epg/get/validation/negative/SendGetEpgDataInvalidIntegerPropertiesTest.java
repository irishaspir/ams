package com.zodiac.ams.charter.tests.epg.get.validation.negative;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataInvalidIntegerPropertiesTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.42 Send Get EPG data, receive file with invalid SEASON, AMS write error ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithInvalidSeason() {
        String csv = epgCsvHelper.createInvalidCsvInvalidSeason();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidSeason(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidSeason());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.24 Send Get EPG data, receive file with invalid EPISODE, AMS write error ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithInvalidEpisode() {
        String csv = epgCsvHelper.createInvalidCsvInvalidEpisode();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidEpisode(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidEpisode());
    }

//    @Features(FeatureList.GET_EPG_DATA)
//    @Stories(StoriesList.VALIDATION_NEGATIVE)
//    @Description("2.1.28 Send Get EPG data, recezive file with invalid HOMETEAMID, AMS write error ")
//    @Test(enabled = false) //not implemented validation rules
//    public void sendGetEpgDataReceiveFileWithInvalidHometeamid() {
//        String csv = epgCsvHelper.createInvalidCsvInvalidHometeamid();
//        createAttachment(csv);
//
//        Assert.assertEquals(sendUpdateEpgDataRequest(),  success200);
//        Assert.assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), cheEmulatorEpg.getURIList());
//
//        log = readAMSLog();
//        createAttachment(log);
//
//        List<String> logEpg = epgLog.getLogAMSForValidData(log);
//        List<String> zdb = epgLog.getPublishedZdbFiles(logEpg);
//        copyZDB(zdb);
//
//        Assert.assertTrue(epgLog.compareLogAMSWithPatternInvalidHometeamid(epgLog.getLogAMSForInvalidFiled(log), epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()));
//        Assert.assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()));
//        Assert.assertEquals(dbUtils.getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidHometeamid());
//    }
//
//    @Features(FeatureList.GET_EPG_DATA)
//    @Stories(StoriesList.VALIDATION_NEGATIVE)
//    @Description("2.1.13 Send Get EPG data, receive file with invalid AWAYTEAMID, AMS write error ")
//    @Test(enabled = false) // not implemented validation rules
//    public void sendGetEpgDataReceiveFileWithInvalidAwayteamid() {
//        String csv = epgCsvHelper.createInvalidCsvInvalidAwayteamid();
//        createAttachment(csv);
//
//        Assert.assertEquals(sendUpdateEpgDataRequest(),  success200);
//        Assert.assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), cheEmulatorEpg.getURIList());
//
//        log = readAMSLog();
//        createAttachment(log);
//
//        List<String> logEpg = epgLog.getLogAMSForValidData(log);
//        List<String> zdb = epgLog.getPublishedZdbFiles(logEpg);
//        copyZDB(zdb);
//
//        Assert.assertTrue(epgLog.compareLogAMSWithPatternInvalidAwayteamid(epgLog.getLogAMSForInvalidFiled(log), epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()));
//        Assert.assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()));
//        Assert.assertEquals(dbUtils.getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidAwayteamid());
//    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.49 Send Get EPG data, receive file with invalid YEAR, AMS write error ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithInvalidYear() {
        String csv = epgCsvHelper.createInvalidCsvInvalidYear();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidYear(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidYear());
    }
}
