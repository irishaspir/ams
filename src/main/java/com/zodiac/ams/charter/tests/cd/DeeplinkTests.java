package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.DEEPLINK_REQUEST_ID;
import static com.zodiac.ams.charter.tests.StoriesList.DEEPLINK;

/**
 * Created by SpiridonovaIM on 20.07.2017.
 */
public class DeeplinkTests extends CDBeforAfter {

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DEEPLINK)
    @Description("10.5.5 Message from device to AMS where <request id>=5, command isn't specified  " +
            "unknown mac, receive ERROR ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(priority = 1)
    public void sendKeyPressReceiveError() {
        String deviceId = DEVICE_ID_NUMBER_1;
        cdRudpHelper.sendRequest(deviceId, DEEPLINK_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        byte[] data = cdstbEmulator.getData();
        assertLog(log, deviceId);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DEEPLINK)
    @Description("10.5.6 Message from device to AMS where <request id>=5, command is specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "deeplinkNegative", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendKeyPressKeyIs(String command) {
        String mac = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createDeeplinkMessage(command);
        cdRudpHelper.sendRequest(mac, message, DEEPLINK_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertDeeplink(DEEPLINK_REQUEST_ID, command);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DEEPLINK)
    @Description("10.5.1 Message from device to AMS where <request id>=5, command is specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "deeplinkPositive", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendKeyPressKeyIs1(String command) {
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createDeeplinkMessage(command);
        cdRudpHelper.sendRequest(deviceId, message, DEEPLINK_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertDeeplink(DEEPLINK_REQUEST_ID, command);

    }
}
