package com.zodiac.ams.charter.tests.ft;

import com.dob.test.charter.iface.IHEDataConsumer;
import com.dob.test.transport.iface.IDataConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class DataConsumer implements IHEDataConsumer, IDataConsumer {
    
    private final static Logger LOG = LoggerFactory.getLogger(DataConsumer.class);
    private final static long MAX_TIMEOUT_MS = 2 * 60 * 1000; //2min
    
    private final Semaphore semaphore;
    private byte [] data;
    private URI uri;
        
    public DataConsumer(Semaphore semaphore){
        this.semaphore = semaphore;
    }

    @Override
    public void dataReceived(URI uri, byte[] data) {
        this.uri = uri;
        this.data = data;
        semaphore.release();
    }
    
    @Override
    public void dataReceived(byte[] data) {
        this.data = data;
        semaphore.release();
    }
    
    
    
    public byte [] getData(){
        try {
            if (semaphore.tryAcquire(MAX_TIMEOUT_MS, TimeUnit.MILLISECONDS))
                return data;
        }
        catch(InterruptedException ex){
            LOG.error("getData exception: {}", ex.getMessage());
        }
        return null;
    }
    
    /**
     * Call only after getData !!!
     * @return 
     */
    public URI getURI(){
        return uri;
    }
}
