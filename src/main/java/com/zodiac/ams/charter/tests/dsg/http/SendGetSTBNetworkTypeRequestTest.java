package com.zodiac.ams.charter.tests.dsg.http;

import com.zodiac.ams.charter.helpers.activityControl.DataForActivityControl;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.json.JSONObject;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.setLastActivityTime;
import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.setSTBModeInSTBMetadata;
import static com.zodiac.ams.charter.http.helpers.dsg.DsgGetHelper.*;
import static com.zodiac.ams.charter.http.listeners.dsg.DsgGetParams.DIGITS;
import static com.zodiac.ams.charter.http.listeners.dsg.DsgGetParams.SPECIAL_SYMBOLS;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;

public class SendGetSTBNetworkTypeRequestTest extends StepsForDsg {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.1 Send valid GET STB NETWORK TYPE REQUEST")
    @TestCaseId("171556")
    @Test (dataProvider = "networkType", dataProviderClass = DataForActivityControl.class)
    public void sendValidGetSTBNetworkTypeRequest(int stbMode, String networkType) {
        setSTBModeInSTBMetadata(stbMode, MAC_NUMBER_1);
        setLastActivityTime(MAC_NUMBER_1);
        assertEquals(sendGetStbNetworkTypeRequestDeviceId(DEVICE_ID_NUMBER_1), SUCCESS_200);
        createAttachment(readAMSLog());
        JSONObject response = responseJson;
        createAttachment(response);
        assertJsonEquals(dsgJsonDataUtils.createStbNetworkTypeResponseJson(networkType), response);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.2 Send GET STB NETWORK TYPE REQUEST with mistake in 'req' parameter name (delete 'q')")
    @TestCaseId("171557")
    @Test()
    public void sendGetSTBNetworkTypeRequestInvalidReqParameter() {
        assertEquals(sendGetStbNetworkTypeRequestToUrlRequestWithMistakeInReq(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.3 Send STB NETWORK TYPE REQUEST with empty value of 'req' parameter")
    @TestCaseId("171558")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithEmptyReq() {
        assertEquals(sendGetStbNetworkTypeRequestReq(EMPTY), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.4 Send STB NETWORK TYPE REQUEST with digits in 'req' parameter")
    @TestCaseId("171559")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithDigitsInReq() {
        assertEquals(sendGetStbNetworkTypeRequestReq(DIGITS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.5 Send STB NETWORK TYPE REQUEST with special symbols in 'req' parameter")
    @TestCaseId("171560")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithSpecialSymbolsInReq() {
        assertEquals(sendGetStbNetworkTypeRequestReq(SPECIAL_SYMBOLS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.6 Send STB NETWORK TYPE REQUEST without 'req' parameter")
    @TestCaseId("171561")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithoutReqParameter() {
        assertEquals(sendGetStbNetworkTypeRequestWithoutReq(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.7 Send STB NETWORK TYPE REQUEST without 'deviceId' parameter")
    @TestCaseId("171562")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithoutDeviceIdParameter() {
        assertEquals(sendGetStbNetworkTypeRequestWithoutDeviceId(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.8 Send STB NETWORK TYPE REQUEST with empty value of 'deviceId' parameter")
    @TestCaseId("171563")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithEmptyDeviceId() {
        assertEquals(sendGetStbNetworkTypeRequestDeviceId(EMPTY), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.9 Send STB NETWORK TYPE REQUEST with digits in 'deviceId' parameter")
    @TestCaseId("171564")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithDigitsInDeviceId() {
        assertEquals(sendGetStbNetworkTypeRequestDeviceId(DIGITS), ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.10 Send STB NETWORK TYPE REQUEST with special symbols in 'deviceId' parameter")
    @TestCaseId("171565")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithSpecialSymbolsInDeviceId() {
        assertEquals(sendGetStbNetworkTypeRequestDeviceId(SPECIAL_SYMBOLS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_NETWORK_TYPE_REQUEST)
    @Description("5.1.11 Send STB NETWORK TYPE REQUEST with nonexistent STB MAC in 'deviceId' parameter")
    @TestCaseId("171566")
    @Test()
    public void sendGetStbSTBNetworkTypeRequestWithNonexistentSTBMac() {
        assertEquals(sendGetStbNetworkTypeRequestDeviceId(UNKNOWN_DEVICE_ID), ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

}
