package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.services.ft.publisher.PublishTask;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;

public interface ITasksUpdater {
    void update(ICarousel carousel, PublishTask task);    
    CarouselType getCarouselType(CarouselType type);
}
