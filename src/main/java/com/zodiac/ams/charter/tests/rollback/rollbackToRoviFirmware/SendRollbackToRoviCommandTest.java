package com.zodiac.ams.charter.tests.rollback.rollbackToRoviFirmware;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.rollback.RollbackPostHelper.*;
import static com.zodiac.ams.charter.rudp.rollback.message.RollbackSTBRequestPattern.nullRequest;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendRollbackToRoviCommandTest extends StepsForRollbackSTBOn{
    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.POSITIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.1 Send \"Rollback To Rovi Firmware\" command, STB is available ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWhenSTBReturnStatusSuccess() {
        assertEquals(sendRollbackToRoviFirmware(DEVICE_ID_NUMBER_1), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getSuccessfulResponse(DEVICE_ID_NUMBER_1));
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), rollbackSTBRequestPattern.rollbackToRoviFirmwareRequest(),
                ROLLBACK_REQUEST_IS_INCORRECT);
        assertTrue(rollbackLogParser.compareLogAMSWithPatternStatusSuccess(rollbackLogParser.getLogAMSForValid(log),
                DEVICE_ID_NUMBER_1), PATTERN_ROLLBACK_LOG);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.3 Send \"Rollback To Rovi Firmware\" command with mistake in 'req' parameter value ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithMistakeInReqValue() {
        assertEquals(sendRollbackToRoviFirmwareWithMistakeInReqValue(DEVICE_ID_NUMBER_1),ERROR_400);
        createAttachment(readAMSLog());
        assertEquals(responseIsAbsent, NULL_RESPONSE, ROLLBACK_RESPONSE_IS_INCORRECT);
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.4 Send \"Rollback To Rovi Firmware\" command with non existent STB MAC")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithUnknownMac() {
        assertEquals(sendRollbackToRoviFirmware(UNKNOWN_DEVICE_ID),SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getResponseIfMacIsUnknown(UNKNOWN_DEVICE_ID));
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.6 Send \"Rollback To Rovi Firmware\" command with mistake in path to Rollback service ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithInvalidServiceName() {
        assertEquals(sendRollbackToRoviFirmwareWithInvalidServiceName(DEVICE_ID_NUMBER_1),ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.POSITIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.7 Send \"Rollback To Rovi Firmware\" command for several MACs ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandForSeveralMacs() {
        assertEquals(sendRollbackToRoviFirmware(DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getSuccessfulResponse( DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2));
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), rollbackSTBRequestPattern.rollbackToRoviFirmwareRequest(),
                ROLLBACK_REQUEST_IS_INCORRECT);
        assertTrue(rollbackLogParser.compareLogAMSWithPatternStatusSuccess(rollbackLogParser.getLogAMSForValid(log),
                DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2), PATTERN_ROLLBACK_LOG);
    }
}
