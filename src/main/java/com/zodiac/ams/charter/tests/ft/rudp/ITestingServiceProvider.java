package com.zodiac.ams.charter.tests.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import java.util.concurrent.CountDownLatch;

public interface ITestingServiceProvider {
    TestingService getTestingService(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown);
}
