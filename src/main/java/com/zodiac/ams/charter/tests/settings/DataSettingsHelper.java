package com.zodiac.ams.charter.tests.settings;

import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.services.settings.SettingsOptions;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsKeysUtils.getDefaultSettingsValues;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsKeysUtils.getSupportedSettingsValues;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.*;
import static com.zodiac.ams.charter.tests.settings.StepsForSettings.delete55And59And60Keys;


public class DataSettingsHelper {

    @DataProvider(name = "allSettingsList")
    public static Object[][] getSetting() {
        ArrayList<SettingsOption> options = allSettings();
        Object[][] objects = new Object[options.size()][2];
        for (int i = 0; i < options.size(); i++) {
            objects[i][1] = options.get(i);
            objects[i][0] = options.get(i).getName();
        }
        return objects;
    }

    @DataProvider(name = "settingsListWithout55And59And60Keys")
    public static Object[][] setSetting() {
        ArrayList<SettingsOption> options = delete55And59And60Keys(allSettings());
        Object[][] objects = new Object[options.size()][2];
        for (int i = 0; i < options.size(); i++) {
            objects[i][1] = options.get(i);
            objects[i][0] = options.get(i).getName();
        }
        return objects;
    }

    @DataProvider(name = "DefaultAudio")
    public static Object[][] defaultAudioRange() {
        return getRange(DEFAULT_AUDIO);
    }

    @DataProvider(name = "ccLanguageRange")
    public static Object[][] languageRange() {
        return getRange(CC_LANGUAGE);
    }


    private static Object[][] getRange(SettingsOptions option) {
        String[] range = option.getOption().getRange();
        Object[][] objects = new Object[range.length][1];
        for (int i = 0; i < range.length; i++) {
            objects[i][0] = Integer.parseInt(range[i]);
        }
        return objects;
    }


    @DataProvider(name = "settingsLRange")
    public static Object[][] setRangeSetting() {
        ArrayList<SettingsOption> options = delete55And59And60Keys(allSettings());
        Object[][] objects = new Object[options.size() * 3][2];
        final int[] i = {0};
        options.forEach(value -> {
                    String str = getSupportedSettingsValues(value.getName());
                    if (str != null) {
                        if (str.contains("=")) {
                            String[] supValues = str.split("\\|");
                            for (int j = 0; j < supValues.length; j++) {
                                String[] sValue = supValues[j].split("=");
                                objects[i[0] + j][0] = value;
                                objects[i[0] + j][1] = sValue[1];
                            }
                            i[0] = i[0] + supValues.length;
                        }
                    }
                }
        );
        return objects;
    }


    @DataProvider(name = "DefaultAudioForJson")
    public static Object[][] defaultAudioRangeForJson() {
        return getRangForJson(DEFAULT_AUDIO);
    }

    @DataProvider(name = "DVSForJson")
    public static Object[][] dvsRangeForJson() {
        return getRangForJson(DVS);
    }

    @DataProvider(name = "StereoAudioForJson")
    public static Object[][] stereoAudioRangeForJson() {
        return getRangForJson(STEREO_AUDIO);
    }

    @DataProvider(name = "VideoOutputFormatForJson")
    public static Object[][] videoOutputFormatRangeForJson() {
        return getRangForJson(VIDEO_OUTPUT_FORMAT);
    }

    @DataProvider(name = "SDOnHDAspectRatioForJson")
    public static Object[][] sdOnHDAspectRatioRangeForJson() {
        return getRangForJson(SD_ON_HD_ASPECT_RATIO);
    }

    @DataProvider(name = "HDOnHDAspectRatioForJson")
    public static Object[][] hdOnHDAspectRatioRangeForJson() {
        return getRangForJson(HD_ON_SD_ASPECT_RATIO);
    }

    @DataProvider(name = "ChannelBarPositionForJson")
    public static Object[][] channelBarPositionForJson() {
        return getRangForJson(CHANNEL_BAR_POSITION);
    }

    @DataProvider(name = "MiniGuideDisplayPositionForJson")
    public static Object[][] miniGuideDisplayPositionForJson() {
        return getRangForJson(MINI_GUIDE_DISPLAY_POSITION);
    }

    @DataProvider(name = "HDAutoTuneForJson")
    public static Object[][] hdAutoTuneForJson() {
        return getRangForJson(HD_AUTO_TUNE);
    }

    @DataProvider(name = "EnableCloseCaptioningForJson")
    public static Object[][] enableCloseCaptioningForJson() {
        return getRangForJson(ENABLE_CLOSE_CAPTION);
    }

    @DataProvider(name = "CCCaptionBackgroundColorForJson")
    public static Object[][] ccCaptionBackgroundColorForJson() {
        return getRangForJson(CC_CAPTION_BACKGROUND_COLOR);
    }

    @DataProvider(name = "CCTextColorForJson")
    public static Object[][] ccTextColorForJson() {
        return getRangForJson(CC_TEXT_COLOR);
    }

    @DataProvider(name = "CCTextSizeForJson")
    public static Object[][] ccTextSizeForJson() {
        return getRangForJson(CC_TEXT_SIZE);
    }

    @DataProvider(name = "CCFontsForJson")
    public static Object[][] ccFontsForJson() {
        return getRangForJson(CC_FONTS);
    }

    @DataProvider(name = "CCBackgroundOpacityForJson")
    public static Object[][] ccBackgroundOpacityForJson() {
        return getRangForJson(CC_BACKGROUND_OPACITY);
    }

    @DataProvider(name = "CCCharacterEdgeAttributesForJson")
    public static Object[][] ccCharacterEdgeAttributesForJson() {
        return getRangForJson(CC_CHARACTER_EDGE_ATTRIBUTES);
    }

    @DataProvider(name = "CCWindowColorForJson")
    public static Object[][] ccWindowColorForJson() {
        return getRangForJson(CC_WINDOW_COLOR);
    }

    @DataProvider(name = "CCWindowOpacityForJson")
    public static Object[][] ccWindowOpacityForJson() {
        return getRangForJson(CC_WINDOW_OPACITY);
    }

    @DataProvider(name = "CCLanguageForJson")
    public static Object[][] ccLanguageForJson() {
        return getRangForJson(CC_LANGUAGE);
    }

    @DataProvider(name = "CCCaptionSimplificationForJson")
    public static Object[][] ccCaptionSimplificationForJson() {
        return getRangForJson(CC_CAPTION_SIMPLIFICATION);
    }

    @DataProvider(name = "ChannelFiltersForJson")
    public static Object[][] channelFiltersForJson() {
        return getRangForJson(CHANNEL_FILTERS);
    }

    @DataProvider(name = "ChannelSortingForJson")
    public static Object[][] channelSortingForJson() {
        return getRangForJson(CHANNEL_SORTING);
    }

    @DataProvider(name = "ACOutletForJson")
    public static Object[][] acOutletForJson() {
        return getRangForJson(AC_OUTLET);
    }

    @DataProvider(name = "AutoPowerOffForJson")
    public static Object[][] autoPowerOffForJson() {
        return getRangForJson(AUTO_POWER_OFF);
    }

    @DataProvider(name = "FrontPanelDisplayForJson")
    public static Object[][] frontPanelDisplayForJson() {
        return getRangForJson(FRONT_PANEL_DISPLAY_BRIGHTNESS);
    }

    @DataProvider(name = "TuneAwayWarningForJson")
    public static Object[][] tuneAwayWarningForJson() {
        return getRangForJson(TUNE_AWAY_WARNING);
    }

    @DataProvider(name = "EnableDisableParentalControlsForJson")
    public static Object[][] enableDisableParentalControlsForJson() {
        return getRangForJson(ENABLE_DISABLE_PARENTAL_CONTROLS);
    }

    @DataProvider(name = "RatingLocksForJson")
    public static Object[][] ratingLocksForJson() {
        return getRangForJson(RATING_LOCKS);
    }

    @DataProvider(name = "ContentLocksForJson")
    public static Object[][] contentLocksForJson() {
        return getRangForJson(CONTENT_LOCKS);
    }

    @DataProvider(name = "HideAdultContentForJson")
    public static Object[][] hideAdultContentForJson() {
        return getRangForJson(HIDE_ADULT_TITLES);
    }

    @DataProvider(name = "CallerIDNotificationForJson")
    public static Object[][] callerIDNotificationForJson() {
        return getRangForJson(CALLER_ID_NOTIFICATION);
    }

    @DataProvider(name = "CallerIDDisplayForJson")
    public static Object[][] callerIDDisplayForJson() {
        return getRangForJson(CALLER_ID_DISPLAY);
    }

    @DataProvider(name = "PurchasePINActiveForJson")
    public static Object[][] purchasePINActiveForJson() {
        return getRangForJson(PURCHASE_PIN_ACTIVATE);
    }

    @DataProvider(name = "WarningOnTuneToRecordingForJson")
    public static Object[][] warningOnTuneToRecordingForJson() {
        return getRangForJson(PURCHASE_PIN_ACTIVATE);
    }

    @DataProvider(name = "RecordDuplicatesForJson")
    public static Object[][] recordDuplicatesForJson() {
        return getRangForJson(RECORD_DUPLICATES);
    }

    @DataProvider(name = "SleepTimerForJson")
    public static Object[][] sleepTimerForJson() {
        return getRangForJson(SLEEP_TIMER);
    }

    @DataProvider(name = "FrontPanelDisplayBrightnessForJson")
    public static Object[][] frontPanelDisplayBrightnessForJson() {
        return getRangForJson(FRONT_PANEL_DISPLAY_BRIGHTNESS);
    }

    @DataProvider(name = "TimeDisplayForJson")
    public static Object[][] timeDisplayForJson() {
        return getRangForJson(TIME_DISPLAY);
    }

    @DataProvider(name = "OnDemandLockForJson")
    public static Object[][] onDemandLockForJson() {
        return getRangForJson(ON_DEMAND_LOCK);
    }

    @DataProvider(name = "MessageIndicatorForJson")
    public static Object[][] messageIndicatorForJson() {
        return getRangForJson(MESSAGE_INDICATOR);
    }

    @DataProvider(name = "GuidePresentationForJson")
    public static Object[][] guidePresentationForJson() {
        return getRangForJson(GUIDE_PRESENTATION);
    }

    @DataProvider(name = "GuideNarrationForJson")
    public static Object[][] guideNarrationForJson() {
        return getRangForJson(GUIDE_NARRATION);
    }

    @DataProvider(name = "EnergySavingsModeForJson")
    public static Object[][] energySavingsModeForJson() {
        return getRangForJson(ENERGY_SAVING_MODE);
    }

    @DataProvider(name = "TurnOn/OffRemindersForJson")
    public static Object[][] turnOnOffRemindersForJson() {
        return getRangForJson(TURN_ON_OFF_REMINDER);
    }

//    @DataProvider(name = "TuneToRecordingWarningForJson")
//    public static Object[][] tuneToRecordingWarningForJson() {
//        return getRangForJson(TUNE_TO_RECORDING_WARNING);
//    }

    private static Object[][] getRangForJson(SettingsOptions option) {
        String str = getSupportedSettingsValues(option.getOption().getName());
        String defaultValue = getDefaultSettingsValues(option.getOption().getName());
        Logger.info("Default value: " + defaultValue);
        Object[][] objects = new Object[option.getOption().getRange().length][1];
        if (str != null) {
            if (str.contains("=")) {
                String[] supValues = str.split("\\|");
                if (supValues[0].contains(defaultValue)) {
                    for (int j = supValues.length - 1; j >= 0; j--) {
                        String[] sValue = supValues[j].split("=");
                        objects[supValues.length - 1 - j][0] = sValue[1];
                    }
                } else {
                    for (int j = 0; j < supValues.length; j++) {
                        String[] sValue = supValues[j].split("=");
                        objects[j][0] = sValue[1];

                    }
                }
            }
        }
        return objects;
    }

}
