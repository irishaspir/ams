package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSFTP;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsHTTP;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;

public class PublisherTestAZip extends PublisherTestBase {
    private static final String ROOT_INI_1_2 = "z_dts.ini.root.55_zip";
    private static final String ROOT_INI_3 = "z_dts.ini.root.http.zip.3.0";
    private CarouselHostsHTTP hosts;
    
    private String getRootIniName(){
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch(version){
            case VERSION_1:
            case VERSION_2:
                return ROOT_INI_1_2;
            default:
                return ROOT_INI_3;
        }
    } 
            
    public PublisherTestAZip(){
        FTAllureUtils.copyAnnotations(PublisherTestBase.class, PublisherTestAZip.class);
        FTAllureUtils.replaceStory(PublisherTestAZip.class, this);
        FTAllureUtils.replaceDescription(PublisherTestAZip.class, this);
        FTAllureUtils.exclude(PublisherTestAZip.class, this);
        
        this.isZipped = true;
        this.aLocEnabled = true;
        this.dalinc = false;
        this.carouselType = CarouselType.HTTP;
    }
    
     @Override
    public String getStory(){
        return "HTTP Publisher Alternative location (zipped=true)";
    }
    
    @Override
    public boolean isExcluded(String test) {
        return false;
    }
    
    void initExecutors(){
    
        hosts = new CarouselHostsHTTP(properties, isZipped, aLocEnabled, dalinc);
        
        for (int i=0;i<hosts.size();i++){
            CarouselHost host = hosts.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());
            ICarousel carousel;
            if (host.isRootNode()){
                carousel = new CarouselSFTP(host, executor, host.getLocation(), getRootIniName(), host.isZipped());
                if (null == root){
                    root = carousel;
                    carouselsIn.add(root);
                }
            }
            else
                carousel = new CarouselSFTP(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
    }
    
    
    @Override
    protected void generateServerXML(){
        initExecutors();
        Map<String,String> params = new HashMap<>();
        params.put("useAlternativeLocations", "true");
        params.put("useZippedZdtsIni", "true");        
        ServerXMLHelper helper=new ServerXMLHelper(properties, params, hosts);
        helper.save("server.xml");
        
    }
    
    @Test
    @Override
    public void addNewFileOnCarousel() throws Exception{
        super.addNewFileOnCarousel();
    }
    
    @Test
    @Override
    public void addNewFileOnCarouselNoFlags() throws Exception{
        super.addNewFileOnCarouselNoFlags();
    }
    
    @Test
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        super.addExistingFileOnCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarousel() throws Exception{
        super.deleteFileFromCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        super.deleteFileFromCarouselInvalidCarousel();    
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidName() throws Exception {
        super.deleteFileFromCarouselInvalidName();
    } 
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        super.deleteFileFromCarouselNoFileNoRecord();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFile() throws Exception {
        super.deleteFileFromCarouselNoFile(); 
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoRecord() throws Exception {
        super.deleteFileFromCarouselNoRecord();
    }
     
    @Test
    @Override
    public void updateExisting() throws Exception {
        super.updateExisting();
    }
    
    @Test
    @Override
    public void updateExistingInvalidCarousel() throws Exception {
        super.updateExistingInvalidCarousel();
    }
    
    @Test
    @Override
    public void updateExistingInvalidName() throws Exception {
        super.updateExistingInvalidName();
    }
      
    @Test
    @Override
    public void updateNoExisting() throws Exception {
        super.updateNoExisting();
    }
    
    @Test
    @Override
    public void addModifyExisting() throws Exception {
        super.addModifyExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExisting() throws Exception {
        super.addModifyNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExistingNoFlags() throws Exception{
        super.addModifyNoExistingNoFlags();
    }
        
    @Test
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        super.addModifyNoVerIncExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncNoExisting() throws Exception {
        super.addModifyNoVerIncNoExisting();
    }
    
    @Test
    @Override
    public void clearOld() throws Exception{
        super.clearOld();
    }
    
    @Test
    @Override
    public void addModifyExistingNoFile() throws Exception {
        super.addModifyExistingNoFile();
    }
    
    @Test
    @Override
    public void updateExistingNoFile() throws Exception {
        super.updateExistingNoFile();
    }
    
    @Test
    @Override
    public void addModifyExistingNoRecord() throws Exception {
        super.addModifyExistingNoRecord();
    }
    
    @Test
    @Override
    public void updateExistingNoRecord() throws Exception {
        super.updateExistingNoRecord();
    }
    
    @Test
    @Override
    public void incVersionExisting() throws Exception {
        super.incVersionExisting();
    }
    
    @Test
    @Override
    public void incVersionNoExisting() throws Exception {
        super.incVersionNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        super.addModifyNoVerIncIncVersionExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        super.addModifyNoVerIncIncVersionNoExisting();
    }
    
    @Test
    @Override
    public void unknownAction() throws Exception{
        super.unknownAction();
    }
    
    @Test
    @Override
    public void unknownServer() throws Exception {
        super.unknownServer();
    }
    
    @Test
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
}
