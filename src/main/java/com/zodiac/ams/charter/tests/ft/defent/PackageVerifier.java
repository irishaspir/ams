package com.zodiac.ams.charter.tests.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.zodiac.ams.charter.services.ft.defent.DefEntMessage;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import org.testng.Assert;

class PackageVerifier implements DENTVerifier {
    @Override
    public void verify(DefEntMessage msg, byte[] response) throws Exception{
        int[] packages1 = msg.getPackages();
        int[] packages2 = null;
        if (null != packages1) {
            Arrays.sort(packages1);
        }

        try (DOBCountingInputStream in = new DOBCountingInputStream(new ByteArrayInputStream(response));) {
            int error = DOB7bitUtils.decodeUInt(in)[0];
            Assert.assertTrue(0 == error, "Error must be 0, but found " + error);
            int count = DOB7bitUtils.decodeUInt(in)[0];
            packages2 = new int[count];
            for (int i = 0; i < count; i++) {
                packages2[i] = DOB7bitUtils.decodeUInt(in)[0];
            }
        }
        if (null != packages2) {
            Arrays.sort(packages2);
        }

        String exp1 = (null == packages1) ? "null" : Arrays.toString(packages1);
        String exp2 = (null == packages2) ? "null" : Arrays.toString(packages2);

        Assert.assertTrue(Arrays.equals(packages1, packages2), "Packages must be the same, but " + exp1 + " != " + exp2);
    }
}
