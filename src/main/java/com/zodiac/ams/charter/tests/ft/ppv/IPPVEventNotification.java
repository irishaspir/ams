package com.zodiac.ams.charter.tests.ft.ppv;

import com.zodiac.ams.charter.services.ft.ppv.PPVCommand;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface IPPVEventNotification {
    PPVCommand command1() default PPVCommand.UNKNOWN;
    String[] parameters1() default "";
    
    PPVCommand command2() default PPVCommand.UNKNOWN;
    String[] parameters2() default "";
    
    PPVCommand command3() default PPVCommand.UNKNOWN;
    String[] parameters3() default "";
}