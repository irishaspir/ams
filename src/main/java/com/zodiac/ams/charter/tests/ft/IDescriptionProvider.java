package com.zodiac.ams.charter.tests.ft;

public interface IDescriptionProvider {
    public String getDescription(String test, String oldDescription);
}
