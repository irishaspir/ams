package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC max reboot mer day.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DcMaxRebootPerDayTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_MAX_REBOOT_PER_DAY_NEGATIVE + "1 - Check without Max Reboot Per Day")
    @Description("4.03.1 [DC][Max Reboot Per Day] Check without Max Reboot Per Day" +
            "<Node ID><Hub ID><><2>" +
            "[<Session start timestamp><1><1><4>" +
            "{<Time delta><54><0><10057|2|1>}]")
    public void sendMessageWithoutMaxRebootPerDay() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                // MaxRebootsPerDay must be null, this is the test purpose!
                .setMaxRebootsPerDay(null)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(4)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(2)
                .addParameter(1)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(54)
                .endEvent(4)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_MAX_REBOOT_PER_DAY_NEGATIVE + "2 - Check with incorrect Max Reboot Per Day")
    @Description("4.03.2 [DC][Max Reboot Per Day] Check with incorrect Max Reboot Per Day" +
            "<Node ID><Hub ID>< ><2>" +
            "[<Session start timestamp><1><1><4>" +
            "{<Time delta><54><0><10057|2|1>}]")
    public void sendMessageWithIncorrectMaxRebootPerDay1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                // MaxRebootsPerDay must be an spaces in string, this is the test purpose!
                .setMaxRebootsPerDay(" ")
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(4)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(2)
                .addParameter(1)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(54)
                .endEvent(4)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_MAX_REBOOT_PER_DAY_NEGATIVE + "2 - Check with incorrect Max Reboot Per Day")
    @Description("4.03.2 [DC][Max Reboot Per Day] Check with incorrect Max Reboot Per Day" +
            "<Node ID><Hub ID><qwerqr><2>" +
            "[<Session start timestamp><1><1><4>" +
            "{<Time delta><54><0><10057|2|1>}]")
    public void sendMessageWithIncorrectMaxRebootPerDay2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                // MaxRebootsPerDay must be a garbage string , this is the test purpose!
                .setMaxRebootsPerDay("qwerqr")
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(4)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(2)
                .addParameter(1)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(54)
                .endEvent(4)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_MAX_REBOOT_PER_DAY_NEGATIVE + "2 - Check with incorrect Max Reboot Per Day")
    @Description("4.03.2 [DC][Max Reboot Per Day] Check with incorrect Max Reboot Per Day" +
            "<Node ID><Hub ID><#$%%%%><2>" +
            "[<Session start timestamp><1><1><4>" +
            "{<Time delta><54><0><10057|2|1>}]")
    public void sendMessageWithIncorrectMaxRebootPerDay3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                // MaxRebootsPerDay must be a garbage string , this is the test purpose!
                .setMaxRebootsPerDay("#$%%%%")
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(4)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(2)
                .addParameter(1)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(54)
                .endEvent(4)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
