package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.tomcat.logger.cd.CdLogParser;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.*;
import static com.zodiac.ams.charter.tomcat.logger.cd.CDLogKeyWords.*;
import static org.testng.Assert.assertTrue;

/**
 * Created by SpiridonovaIM on 26.07.2017.
 */
public class AssertLog extends AssertResponses {

    private CdLogParser logParser = new CdLogParser();

    @Step
    public void assertErrorLog(List<String> log, String deviceId, int message, int error, int params) {
        assertContent(log, cdRudpHelper.getMessageId(), deviceId);
        assertSTBError(log, gerErrorString(error));
        assertResponseContent(log, cdRudpHelper.getMessageId(), deviceId, gerMessageString(message), gerErrorString(error), gerParamsString(params));
    }

    @Step
    public void assertLog(List<String> log, String deviceId) {
        assertSocketClose(log);
        assertContent(log, cdRudpHelper.getMessageId(), deviceId);
    }

    @Step
    private void assertContent(List<String> log, int messageId, String deviceId) {
        assertTrue(logParser.isContentInLog(log, messageId, deviceId), ERROR + String.format(CONTENT_PATTERN, messageId, deviceId) + NOT_IN_LOG);
    }

    @Step
    private void assertSocketClose(List<String> log) {
        assertTrue(logParser.isSocketClose(log), ERROR + SOCKET_CLOSE + NOT_IN_LOG);
    }

    @Step
    private void assertSTBError(List<String> log, String error) {
        assertTrue(logParser.isSTBError(log, error), ERROR + STB_ERROR + NOT_IN_LOG);
    }

    @Step
    private void assertResponseContent(List<String> log, int messageId, String deviceId, String message, String code, String params) {
        assertTrue(logParser.isResponseContentInLog(log, messageId, deviceId, message, code, params),
                ERROR + String.format(CD_RESPONSE_PATTERN, messageId, deviceId, message, code, params) + NOT_IN_LOG);
    }

    private String gerErrorString(int error) {
        switch (error) {
            case 0:
                return OK_CD;
            case 1:
                return ERROR_CD;
            case 2:
                return INCORRECT_PARAMETER_CD;
            case 14:
                return STB_IN_STANDBY_MODE_CD;
            case 15:
                return TARGET_NOT_EXIST_CD;
            case 16:
                return OUT_OF_RANGE_CD;
            case 17:
                return DVR_NOT_READY_CD;
        }
        return null;
    }

    private String gerParamsString(int param) {
        switch (param) {
            case 22:
                return "";
        }
        return null;
    }

    private String gerMessageString(int id) {
        switch (id) {
            case 5:
                return DEEPLINK;
            case 6:
                return DVR_RECORDING_PLAYBACK_START;
            case 7:
                return DVR_RECORDING_TRICKMODE;
        }
        return null;
    }
}
