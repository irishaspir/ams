package com.zodiac.ams.charter.tests.ft.rudp;

import com.dob.ams.util.DOB7bitUtils;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getSegmentSize;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.PacketType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

class RUDPTestingUtils {
    
    static final long AMS_SESSION_TIMEOUT =  1 * 60 * 1000; //AMS session RUDP timeout ms
    static final long CLIENT_SESSION_TIMEOUT = 2; 
    static final long CLIENT_TEST_TIMEOUT = 15; 
    
    static final int MAX_NUMBER_OF_SEGMENTS = 7;
    static final int MIN_NUMBER_OF_SEGMENTS = 3;
    static final int MAX_NUMBER_OF_SEGMENTS_STRESS = 2;
    
    
    static final int MAX_NUMBER_OF_THREADS = 5;
    static final int MAX_NUMBER_OF_CLIENTS = 3;
    static final int MAX_NUMBER_OF_SESSIONS = 1;
    static final int MAX_NUMBER_OF_ITERATIONS =5;
    static final long CLIENT_TESTS_TIMEOUT_STRESS = 60;
    
    
    static int getStressSize(){
        return getSize(1,MAX_NUMBER_OF_SEGMENTS_STRESS,false);
    }
    
    static int getSize(){
        return getSize(MIN_NUMBER_OF_SEGMENTS,MAX_NUMBER_OF_SEGMENTS,false);
    }
   
    static int getSize(boolean roundOnly){
        return getSize(MIN_NUMBER_OF_SEGMENTS,MAX_NUMBER_OF_SEGMENTS,roundOnly);
    }
    
    static int getSize(int minNumber,int maxNumber,boolean roundOnly){
        int number = new Random().nextInt((maxNumber - minNumber) + 1) + minNumber;
        int size =number*getSegmentSize();
        if (!roundOnly)
            size+=new Random().nextInt(2)*new Random().nextInt(getSegmentSize());
        return size;
        
    }
    
    static byte[] getRandomBytes(int maxSize){
        return getRandomBytes(1,maxSize);
    }
    static byte[] getRandomBytes(int minSize,int maxSize){
        byte [] result = new byte[new Random().nextInt((maxSize-minSize)+1) + minSize];
        Random random = new Random();
        result[0] = 0;
        for (int i=1;i<result.length;i++)
            result[i] =(byte)(random.nextInt(128) & 0xFF);
        return result;
    }
    
    
    static int[] getSegments(int size,ORDER order){
        
        class RandomComparator implements Comparator{
                @Override
                public int compare(Object o1, Object o2) {
                    return new Random().nextInt(3)-1;
                }
        }
        class ReverseComparator implements Comparator<Integer> {
                public int compare(Integer o1, Integer o2) {
                    return o2.compareTo(o1);
                }
        
        }
        
        Integer[] result = new Integer[size];
        for(int i=0;i<result.length;i++)
            result[i] = i;
        
        
        switch (order) {
            case EMPTY:
                return null;
            case DESCENDING:
                Arrays.sort(result,new ReverseComparator() /*Comparator.reverseOrder()*/);
                break;
            case DESCENDING_LAST_IS_LAST:
                Arrays.sort(result,0,result.length-1,new ReverseComparator() /*Comparator.reverseOrder()*/);
                break;
            case ASCENDING_LAST_IS_FIRST:
                {
                    Integer last = result[result.length - 1]; 
                    result[result.length - 1] = result[0];
                    result[0] = last;
                    Arrays.sort(result,1,result.length);
                }
                break;
            case RANDOM:
                Arrays.sort(result,new RandomComparator());
                break;
            case RANDOM_LAST_IS_LAST:
                Arrays.sort(result,0,result.length-1,new RandomComparator());
                break;
            case RANDOM_LAST_IS_FIRST:
                {
                    Integer last = result[result.length - 1]; 
                    result[result.length - 1] = result[0];
                    result[0] = last;
                    Arrays.sort(result,1,result.length,new RandomComparator());
                }
                break;
        }
         
        int[] ints = new int[result.length];
        for(int i=0;i<result.length;i++)
            ints[i] = result[i];
        return  ints;
        
    }
    
    
    static byte[] generateDataPacket(byte type,int sessionId,int segmentNumber, byte[] data) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        baos.write(type);
        byte[] fileIdBytes = DOB7bitUtils.encodeUInt(sessionId);
        baos.write(fileIdBytes);
        byte[] segmentNumberBytes = DOB7bitUtils.encodeUInt(segmentNumber);
        baos.write(segmentNumberBytes);
        baos.write(data);
        baos.close();
        return baos.toByteArray();
    }
    
}
