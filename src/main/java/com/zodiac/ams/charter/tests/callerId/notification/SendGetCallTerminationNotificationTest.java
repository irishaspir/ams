package com.zodiac.ams.charter.tests.callerId.notification;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.callerId.CallerIdGetHelper.*;
import static com.zodiac.ams.charter.http.helpers.callerId.CallerIdStringDataUtils.*;
import static com.zodiac.ams.charter.rudp.callerId.message.CallerIdSTBRequestPattern.nullRequest;
import static com.zodiac.ams.charter.services.callerId.CallerIdOptions.*;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetCallTerminationNotificationTest extends StepsForCallerIdNotification {
    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.1 Send Call Termination notification request to 1 STB")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationTo1STB() {
        assertEquals(sendGetIncomingCallNotificationRequestTo1STB(), SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestTo1STB(),  SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(VALID_CALL_ID),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.3 Send Call Termination notification request to STB if the CallID does not match")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationToSTBCallIDDoesNotMatch()  {
        assertEquals(sendGetIncomingCallNotificationRequestCallID(VALID_CALL_ID),SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        createAttachment(readAMSLog());
        assertEquals(sendGetCallTerminationNotificationRequestCallID(VALID_SECOND_CALL_ID),  SUCCESS_200);
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.8 Send Call Termination notification request to STB with unknown ID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithUnknownID()  {
        assertEquals(sendGetCallTerminationNotificationRequestID(UNKNOWN_DEVICE_ID), ERROR_203);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
        assertEquals(error203UnknownMAC(UNKNOWN_DEVICE_ID), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.9 Send Call Termination notification request with only letters (except A, F) in ID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithOnlyLettersInID() {
        assertEquals(sendGetCallTerminationNotificationRequestID(INVALID_ID_ONLY_LETTERS), ERROR_203);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
        assertEquals(error203InvalidMAC(INVALID_ID_ONLY_LETTERS), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.10 Send Call Termination notification request with special symbols in ID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithSpecialSymbolsInID() {
        assertEquals(sendGetCallTerminationNotificationRequestID(INVALID_ID_SPECIAL_SYMBOLS), ERROR_203);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest());
        assertEquals(error203InvalidMAC(INVALID_ID_SPECIAL_SYMBOLS), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.11 Send Call Termination notification request with incorrect order of params")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithIncorrectOrderOfParams()  {
        assertEquals(sendGetIncomingCallNotificationRequestIncorrectOrderOfParams(), SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestIncorrectOrderOfParams(), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(VALID_CALL_ID),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.12 Send Call Termination notification request with digits in CallId parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithDigitsInCallId()  {
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_ONLY_DIGITS), SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestCallID(CALL_ID_ONLY_DIGITS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(CALL_ID_ONLY_DIGITS),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.14 Send Call Termination notification request with letters in CallID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithLettersInCallId()  {
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_ONLY_LETTERS), SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestCallID(CALL_ID_ONLY_LETTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(CALL_ID_ONLY_LETTERS),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.15 Send Call Termination notification request with lot of characters in CallID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithLotOfCharactersInCallId()  {
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_MANY_CHARACTERS), SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestCallID(CALL_ID_MANY_CHARACTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(CALL_ID_MANY_CHARACTERS),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.16 Send Call Termination notification request with lot of characters in ID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithLotOfCharactersInID()  {
        assertEquals(sendGetCallTerminationNotificationRequestID(ID_MANY_CHARACTERS), ERROR_203);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
        assertEquals(error203InvalidMAC(ID_MANY_CHARACTERS), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.17a Send Call Termination notification request with mistake in CallID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithMistakeInCallIDParameter()  {
        assertEquals(sendGetCallTerminationNotificationRequestWithMistakeCallIDParameter(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, NULL));
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.17b Send Call Termination notification request with mistake in ID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithMistakeInIDParameter()  {
        assertEquals(sendGetCallTerminationNotificationRequestWithMistakeIDParameter(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log), NULL, VALID_CALL_ID));
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.18 Send Call Termination notification request with special symbols in CallID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithSpecialSymbolsInCallIDParameter()  {
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_SPECIAL_SYMBOLS), SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestCallID(CALL_ID_SPECIAL_SYMBOLS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(CALL_ID_SPECIAL_SYMBOLS),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.19 Send Call Termination notification request with & symbol in the end URI")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithAmpersandInTheEndURI()  {
        assertEquals(sendGetIncomingCallNotificationRequestWithAmpersandInTheEnd(),SUCCESS_200);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestWithAmpersandInTheEnd(), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(VALID_CALL_ID),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.20 Send Call Termination notification request with valid and unknown ID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithValidAndUnknownID()  {
        assertEquals(sendGetIncomingCallNotificationRequestToSeveralSTBs(DEVICE_ID_NUMBER_1, UNKNOWN_DEVICE_ID), ERROR_203);
        callerIdRudpHelper.getCallerIdStbRequest();
        assertEquals(sendGetCallTerminationNotificationRequestToSeveralSTBs(DEVICE_ID_NUMBER_1, UNKNOWN_DEVICE_ID), ERROR_203);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(VALID_CALL_ID),
                CALLER_ID_REQUEST_IS_INCORRECT);
        assertEquals(error203UnknownMAC(UNKNOWN_DEVICE_ID), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.21a Send Call Termination notification request without symbol '=' after ID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithoutEqualsAfterID()  {
        assertEquals(sendGetCallTerminationNotificationRequestWithoutEqualsAfterID(),ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log),NULL, VALID_CALL_ID),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.21b Send Call Termination notification request without symbol '=' after CallID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithoutEqualsAfterCallID()  {
        assertEquals(sendGetCallTerminationNotificationRequestWithoutEqualsAfterCallID(),ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log),DEVICE_ID_NUMBER_1, NULL),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.22 Send Call Termination notification request without CallID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithoutCallID()  {
        assertEquals(sendGetCallTerminationNotificationRequestWithoutCallID(),ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log),DEVICE_ID_NUMBER_1, NULL),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.23 Send Call Termination notification request without CallId data")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithoutCallIdData()  {
        assertEquals(sendGetCallTerminationNotificationRequestCallID(EMPTY),ERROR_500);
        createAttachment(readAMSLog());
        assertTrue( callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log),DEVICE_ID_NUMBER_1, EMPTY),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.24 Send Call Termination notification request without ID data")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithoutIDData()  {
        assertEquals(sendGetCallTerminationNotificationRequestID(EMPTY),ERROR_500);
        createAttachment(readAMSLog());
        assertTrue( callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log),EMPTY, VALID_CALL_ID),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.2.25 Send Call Termination notification request without ID")
    @TestCaseId("12345")
    @Test()
    public void sendCallTerminationNotificationWithoutID()  {
        assertEquals(sendGetCallTerminationNotificationRequestWithoutID(),ERROR_500);
        createAttachment(readAMSLog());
        assertTrue( callerIDLog.compareLogAMSWithPatternTermination(callerIDLog.getLogAMSForValid(log),NULL, VALID_CALL_ID),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }
}



