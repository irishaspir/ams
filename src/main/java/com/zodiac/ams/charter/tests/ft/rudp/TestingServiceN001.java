package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.CLIENT_SESSION_TIMEOUT;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import com.zodiac.ams.charter.services.ft.rudp.IStartPacketProvider;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnyResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureResult;


public class TestingServiceN001 extends TestingService{
    
    private static final ErrorCodeType expected = ErrorCodeType.FILEID;
    
    TestingServiceN001(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    
    class StartPacketProvider implements IStartPacketProvider{
        private final int id;
        
        private StartPacketProvider(int id){
            this.id = id;
        }
       
        @Override
        public StartPacket get(StartPacket packet) {
            packet.setFileId(id);
            return packet;
        }        
    }
    
    class StartThread extends Thread {
        final int sessionId;
        private ChannelFuture future;
        
        private StartThread(int sessionId){
            this.sessionId = sessionId;
        }
  
        @Override
        public void run(){
            future = sender.sendStart(sessionId);
        }
    }
    
    
    @Override
    public void run() {
        RUDPTestingSession ts1 =  null;
        RUDPTestingSession ts2 =  null;
        int newSessionId = 0;
        try{
            
            ts1 = getTestingSession(sessionId);
            newSessionId = sender.createSenderSession(ts1.getRUDPTestingPackage().toBytes().length);
            if (null != operation)
                operation.addSessionId(newSessionId);
            ts2 = getTestingSession(newSessionId);
            
            StartPacketProvider provider= new StartPacketProvider(sessionId);
            sender.setStartPacketProvider(newSessionId,provider);
            
            StartThread th1 = new StartThread(sessionId);
            StartThread th2 = new StartThread(newSessionId);
            th1.start();
            th2.start();
            th1.join(CLIENT_SESSION_TIMEOUT*60*1000);
            th2.join(CLIENT_SESSION_TIMEOUT*60*1000);
            
            if (!getExpectedFailureResult(sessionId,th1.future,expected))
                return;
            if (!getExpectedAnyResult(newSessionId,th2.future))
                return;
            
            ts2.setSuccess(true);
        }
        catch(Exception ex){
            if (null != ts1){
                ts1.setSuccess(false);
                ts1.setErrorMsg(ex.getMessage());
            }
            if (null != ts2){
                ts2.setSuccess(false);
                ts2.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            sender.removeStartPacketProvider(newSessionId);
            if (null != this.countDown)
                countDown.countDown();
        }
    }
    
}
