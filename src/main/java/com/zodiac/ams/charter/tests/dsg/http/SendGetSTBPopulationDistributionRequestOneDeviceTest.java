package com.zodiac.ams.charter.tests.dsg.http;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.json.JSONObject;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.setSTBModeInSTBMetadata;
import static com.zodiac.ams.charter.http.helpers.dsg.DsgGetHelper.*;
import static com.zodiac.ams.charter.http.listeners.dsg.DsgGetParams.DIGITS;
import static com.zodiac.ams.charter.http.listeners.dsg.DsgGetParams.SPECIAL_SYMBOLS;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.*;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;

public class SendGetSTBPopulationDistributionRequestOneDeviceTest extends StepsForDsg {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.3 Send valid GET STB POPULATION DISTRIBUTION REQUEST if DOCSIS device is presented")
    @TestCaseId("171556")
    @Test
    public void sendValidGetStbPopulationDistributionRequestWithDocsisDeviceCount() {
        setSTBModeInSTBMetadata(DOCSIS, MAC_NUMBER_1);
        assertEquals(sendGetStbPopulationDistributionRequest(), SUCCESS_200);
        JSONObject response = responseJson;
        assertJsonEquals(response, dsgJsonDataUtils.createStbPopulationDistributionResponseJson(1, 0, 0));
        createAttachment(response);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.4 Send valid GET STB POPULATION DISTRIBUTION REQUEST if ALOHA device is presented")
    @TestCaseId("171556")
    @Test
    public void sendValidGetStbPopulationDistributionRequestWithAlohaDeviceCount() {
        setSTBModeInSTBMetadata(ALOHA, MAC_NUMBER_1);
        assertEquals(sendGetStbPopulationDistributionRequest(), SUCCESS_200);
        JSONObject response = responseJson;
        assertJsonEquals(response, dsgJsonDataUtils.createStbPopulationDistributionResponseJson(0,0,1));
        createAttachment(response);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.5 Send valid GET STB POPULATION DISTRIBUTION REQUEST id DAVIC device is presented")
    @TestCaseId("171556")
    @Test
    public void sendValidGetStbPopulationDistributionRequestWithDavicDeviceCount() {
        setSTBModeInSTBMetadata(DAVIC, MAC_NUMBER_1);
        assertEquals(sendGetStbPopulationDistributionRequest(), SUCCESS_200);
        JSONObject response = responseJson;
        assertJsonEquals(response, dsgJsonDataUtils.createStbPopulationDistributionResponseJson(0,1,0));
        createAttachment(response);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.6 Send GET STB POPULATION DISTRIBUTION REQUEST with mistake in 'req' parameter name (delete 'q')")
    @TestCaseId("171557")
    @Test()
    public void sendGetStbPopulationDistributionRequestInvalidReqParameter() {
        assertEquals(sendGetStbPopulationDistributionRequestToUrlWithMistakeInReq(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.7 Send STB POPULATION DISTRIBUTION REQUEST with empty value of 'req' parameter")
    @TestCaseId("171558")
    @Test()
    public void sendGetStbPopulationDistributionRequestWithEmptyReq() {
        assertEquals(sendGetStbPopulationDistributionRequestReq(EMPTY), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.8 Send STB POPULATION DISTRIBUTION REQUEST with digits in 'req' parameter")
    @TestCaseId("171559")
    @Test()
    public void sendGetStbPopulationDistributionRequestWithDigitsInReq() {
        assertEquals(sendGetStbPopulationDistributionRequestReq(DIGITS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.9 Send STB POPULATION DISTRIBUTION REQUEST with special symbols in 'req' parameter")
    @TestCaseId("171560")
    @Test()
    public void sendGetStbPopulationDistributionRequestWithSpecialSymbolsInReq() {
        assertEquals(sendGetStbPopulationDistributionRequestReq(SPECIAL_SYMBOLS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

}
