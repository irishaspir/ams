package com.zodiac.ams.charter.tests.settings.che;


import com.zodiac.ams.charter.http.helpers.settings.SettingsGetHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.DataSettingsHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.SettingsGetHelper.*;
import static com.zodiac.ams.charter.tests.StoriesList.*;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;


public class SendGetSettingsFromCHETest extends GetSettingsBeforeAfter {


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + GET_SETTINGS_FROM_CHE)
    @Description("1.3.1 Send getSettings from CHE to AMS, MAC is invalid, receive 404 error. " +
            "(GET HTTP://host:port/ams/settings?deviceid=UNKNOWN_MAC)")
    @TestCaseId("169586")
    @Test(priority = 2)
    public void getSettingsReceiveError404() {
        String response = sendGetSettingsIncorrectDeviceId(UNKNOWN_DEVICE_ID);
        createAttachment(SettingsGetHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, ERROR_404);
        assertJsonEquals(jsonDataUtils.error404NotSpecified(), SettingsGetHelper.responseJson);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + GET_SETTINGS_FROM_CHE)
    @Description("1.3.2 Send getSettings from CHE to AMS, MAC is valid, settings are valid, STB is available, receive settings list. " +
            "(GET HTTP://host:port/ams/settings?deviceid=VALID_MAC)")
    @TestCaseId("169585")
    @Test(priority = 3)
    public void getSettingsDeviceIdReceiveSuccess() {
        String response = sendGetSettingsDeviceId(DEVICE_ID_NUMBER_1);
        createAttachment(SettingsGetHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.getSettingsResponse(), SettingsGetHelper.responseJson);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + GET_SETTINGS_FROM_CHE)
    @Description("1.3.3 Send getSettings from CHE to AMS, MAC is valid, settings are valid, STB is available, receive setting. " +
            "(GET HTTP://host:port/ams/settings?deviceid=VALID_MAC&name=SETTING)")
    @TestCaseId("169584")
    @Test(dataProvider = "allSettingsList", dataProviderClass = DataSettingsHelper.class, priority = 5)
    public void getSettingsDeviceIdNameReceiveSuccess(String name, SettingsOption setting) {
        ArrayList<SettingsOption> option = SettingsOption.create()
                .addOption(setting)
                .build();
        String response = sendGetSettingsDeviceIdName(DEVICE_ID_NUMBER_1, option);
        assertEquals(response, SUCCESS_200);
        createAttachment(SettingsGetHelper.responseJson);
        assertJsonEquals(jsonDataUtils.getSettingsDeviceIdNameResponse(option), SettingsGetHelper.responseJson);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + GET_SETTINGS_FROM_CHE)
    @Description("1.3.4 Send getSettings from CHE to AMS, MAC is valid, setting name is invalid, STB is available, receive error. " +
            "(GET HTTP://host:port/ams/settings?deviceid=VALID_MAC&name=INCORRECT_SETTING_NAME)")
    @TestCaseId("171889")
    @Test()
    public void getSettingsDeviceIdIncorrectSettingNameReceiveError() {
        String response = sendGetSettingsDeviceIdName(DEVICE_ID_NUMBER_1, "INCORRECT_SETTING_NAME");
        assertEquals(response, ERROR_500);
        createAttachment(SettingsGetHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertJsonEquals(jsonDataUtils.error500UnknownSettingsName("INCORRECT_SETTING_NAME"), SettingsGetHelper.responseJson);
    }
}
