package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselCDN;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselDNCS;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSFTP;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSTFactory;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsCDN;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsHTTP;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsST;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.Test;

public class PublisherTestAllZip extends PublisherTestBase {
    
    private static final Map<String,String> tarantulaIds = new HashMap<String,String>(){{
        put("addModifyExisting", "8.2.53");
    }};
     
    private static final List<String> excludedTests = new ArrayList<String>(){{
        add("deleteFileFromCarouselNoRecord");
        add("addModifyExistingNoRecord");
        add("updateExistingNoRecord");
    }};
    
    private CarouselHostsST hostsST;
    private CarouselHostsHTTP hostsHTTP;
    private CarouselHostsDNCS hostsDNCS;
    private CarouselHostsCDN hostsCDN;
    
    public PublisherTestAllZip(){
        FTAllureUtils.copyAnnotations(PublisherTestBase.class , PublisherTestAllZip.class);
        FTAllureUtils.replaceStory(PublisherTestAllZip.class, this);
        FTAllureUtils.replaceDescription(PublisherTestAllZip.class, this);
        FTAllureUtils.exclude(PublisherTestAllZip.class, this);
        
        this.isZipped = true;
        this.aLocEnabled = true;
        this.dalinc = false;
        this.carouselType = CarouselType.BATCH_1;
    }
    
    @Override
    public String getStory(){
        return "All types of the alternative locations (zipped=true)";
    }
    
    @Override
    public String getDescription(String test, String oldDescription){
        String id = tarantulaIds.get(test);
        return (null == id) ? oldDescription : id + " " + oldDescription;
    }
    
    @Override
    public boolean isExcluded(String test) {
        return excludedTests.contains(test);
    }
    
    
    void initExecutors(){
        hostsST = new CarouselHostsST(properties, new CarouselType[] {carouselType, CarouselType.ST}, isZipped, aLocEnabled, dalinc);
        hostsHTTP = new CarouselHostsHTTP(properties, new CarouselType[] {carouselType, CarouselType.HTTP}, isZipped, aLocEnabled, dalinc);
        hostsDNCS = new CarouselHostsDNCS(properties, new CarouselType[] {carouselType, CarouselType.DNCS}, isZipped, aLocEnabled, dalinc);
        hostsCDN = new CarouselHostsCDN(properties, new CarouselType[] {carouselType, CarouselType.CDN}, isZipped, aLocEnabled, dalinc);
       
        //only HTTP may be root
        for (int i=0;i<hostsDNCS.size();i++){
            CarouselHost host = hostsDNCS.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());;
            ICarousel carousel = new CarouselDNCS(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsST.size();i++){
            CarouselHost host = hostsST.get(i);
            ICarousel carousel = CarouselSTFactory.getCarouselST(host,host.isZipped(), aLocEnabled);
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsCDN.size();i++){
            CarouselHost host = hostsCDN.get(i);
            ICarousel carousel = new CarouselCDN(host, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsHTTP.size();i++){
            CarouselHost host = hostsHTTP.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());
            ICarousel carousel;
            if (host.isRootNode()){
                carousel = new CarouselSFTP(host, executor, host.getLocation(), getBatchRootIniName(), host.isZipped());
                if (null == root){
                    root = carousel;
                    carouselsIn.add(root);
                }
            }
            else
                carousel = new CarouselSFTP(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
    }
    
    
    @Override
    protected void generateServerXML(){
        initExecutors();
        Map<String,String> params = new HashMap<>();
        params.put("useAlternativeLocations", "true");
        params.put("useZippedZdtsIni", "true");  
        ServerXMLHelper helper=new ServerXMLHelper(properties, params, hostsHTTP, hostsST, hostsDNCS, hostsCDN);
        helper.save("server.xml");
        
    }
    
    @Test
    @Override
    public void addNewFileOnCarousel() throws Exception{
        super.addNewFileOnCarousel();
    }
    
    @Test
    @Override
    public void addNewFileOnCarouselNoFlags() throws Exception{
        super.addNewFileOnCarouselNoFlags();
    }
    
    @Test
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        super.addExistingFileOnCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarousel() throws Exception{
        super.deleteFileFromCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        super.deleteFileFromCarouselInvalidCarousel();    
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidName() throws Exception {
        super.deleteFileFromCarouselInvalidName();
    } 
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        super.deleteFileFromCarouselNoFileNoRecord();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFile() throws Exception {
        super.deleteFileFromCarouselNoFile(); 
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoRecord() throws Exception {
        super.deleteFileFromCarouselNoRecord();
    }
     
    @Test
    @Override
    public void updateExisting() throws Exception {
        super.updateExisting();
    }
    
    @Test
    @Override
    public void updateExistingInvalidCarousel() throws Exception {
        super.updateExistingInvalidCarousel();
    }
    
    @Test
    @Override
    public void updateExistingInvalidName() throws Exception {
        super.updateExistingInvalidName();
    }
      
    @Test
    @Override
    public void updateNoExisting() throws Exception {
        super.updateNoExisting();
    }
    
    @Test
    @Override
    public void addModifyExisting() throws Exception {
        super.addModifyExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExisting() throws Exception {
        super.addModifyNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExistingNoFlags() throws Exception{
        super.addModifyNoExistingNoFlags();
    }
        
    @Test
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        super.addModifyNoVerIncExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncNoExisting() throws Exception {
        super.addModifyNoVerIncNoExisting();
    }
    
    @Test
    @Override
    public void clearOld() throws Exception{
        super.clearOld();
    }
    
    @Test
    @Override
    public void addModifyExistingNoFile() throws Exception {
        super.addModifyExistingNoFile();
    }
    
    @Test
    @Override
    public void updateExistingNoFile() throws Exception {
        super.updateExistingNoFile();
    }
    
    @Test
    @Override
    public void addModifyExistingNoRecord() throws Exception {
        super.addModifyExistingNoRecord();
    }
    
    @Test
    @Override
    public void updateExistingNoRecord() throws Exception {
        super.updateExistingNoRecord();
    }
    
    @Test
    @Override
    public void incVersionExisting() throws Exception {
        super.incVersionExisting();
    }
    
    @Test
    @Override
    public void incVersionNoExisting() throws Exception {
        super.incVersionNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        super.addModifyNoVerIncIncVersionExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        super.addModifyNoVerIncIncVersionNoExisting();
    }
    
    @Test
    @Override
    public void unknownAction() throws Exception{
        super.unknownAction();
    }
    
    @Test
    @Override
    public void unknownServer() throws Exception {
        super.unknownServer();
    }
    
    @Test
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
}
