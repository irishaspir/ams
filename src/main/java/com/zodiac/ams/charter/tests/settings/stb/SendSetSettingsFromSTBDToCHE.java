package com.zodiac.ams.charter.tests.settings.stb;


import com.zodiac.ams.charter.emuls.che.settings.SettingsSampleBuilder;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getAllKeysWithout65KeyFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getKeysFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.http.helpers.settings.UrlHelper.setUriPattern;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.*;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_STB;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendSetSettingsFromSTBDToCHE extends StepsForSettings {

    @BeforeMethod
    public void checkDataInBD() {
        cleanBDForSettings();
        setMigrationFlagOne(MAC_NUMBER_3);
        options = delete62Key(options);
        new SettingsSampleBuilder(DEVICE_ID_NUMBER_3, "./dataCHE", "/sample1.xml").messageBuild();
        runSTB();
        runCHESettings();
        startTestTime = getCurrentTimeTest();
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE+SET_SETTINGS_FROM_STB)
    @Description("1.3.2 Send setSettings from STB to AMS, AMS send setSettings to CHE settings don't exist in BD AMS, settings exist in BD HE, migration flag = 1," +
            " receive error code = 0, settings are in DB   ")
    @TestCaseId("12345")
    @Test()
    public void sendSetSettingsReceiveErrorCode0SettingsInDBAMS() {
        // ON_DEMAND_PIN and PARENTAL_CONTROL_PIN have the same value
        String mac = MAC_NUMBER_3;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(CHANNEL_SORTING)
                .addOptions(CC_TEXT_COLOR)
                .addOptions(CHANNEL_BAR_DURATION)
                .addOptions(PARENTAL_CONTROL_PIN)
                .build();
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), "URI is incorrect");
        assertJsonEquals(cheEmulatorSettings.checkData(), jsonDataUtils.amsSetSettingsRequestWithODP(options, getDeviceIdByMac(mac)));
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getKeysFromSettingsSTBDataV2(options, mac), allKeys(options), wrongValueInDB);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE+SET_SETTINGS_FROM_STB)
    @Description("1.3.7 Send setSettings from STB to AMS, AMS send setSettings to CHE, settings don't exist in BD AMS, settings exist in BD HE, migration flag = 1 ")
    @Test()
    public void sendSetAllSettingsReceiveErrorCode0SettingsInDBAMS() {
        String mac = MAC_NUMBER_3;
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), "URI is incorrect");
        assertJsonEquals(cheEmulatorSettings.checkData(), jsonDataUtils.amsSetSettingsRequestWithODP(options, getDeviceIdByMac(mac)));
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getAllKeysWithout65KeyFromSettingsSTBDataV2(mac), allKeys(allSettings()), wrongValueInDB);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE+SET_SETTINGS_FROM_STB)
    @Description("1.3.2 Send setSettings from STB to AMS, AMS send setSettings to CHE, settings don't exist in BD AMS, settings exist in BD HE, migration flag = 1 ")
    @Test()
    public void sendSetSettingsReceiveDeltaWithOnDemandPinIsEmpty() {
        String mac = MAC_NUMBER_3;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(CALLER_ID_NOTIFICATION)
                .addOptions(CC_TEXT_COLOR)
                .addOptions(CHANNEL_BAR_DURATION)
                .build();
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), "URI is incorrect");
        assertJsonEquals(cheEmulatorSettings.checkData(), jsonDataUtils.amsSetSettingsRequestWithODPIsEmpty(options, getDeviceIdByMac(mac)));
        createAttachment(cheEmulatorSettings.getJson());// research KEY62 and than delete
        assertEquals(getKeysFromSettingsSTBDataV2(options, mac), allKeysWithNull62Key(options), wrongValueInDB);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE+SET_SETTINGS_FROM_STB)
    @Description("1.3.2 Send setSettings from STB to AMS, AMS send setSettings to CHE, settings don't exist in BD AMS, settings exist in BD HE,  migration flag = 0 ")
    @Test()
    public void sendSetSettingsMigrationFlagIsNullReceiveErrorCode0SettingsInDBAMS() {
        String mac = MAC_NUMBER_3;
        setMigrationFlagNull(mac);
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), "URI is incorrect");
        assertJsonEquals(cheEmulatorSettings.checkData(), jsonDataUtils.amsSetSettingsRequestWithODP(options, getDeviceIdByMac(mac)));
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getAllKeysWithout65KeyFromSettingsSTBDataV2(mac), allKeys(allSettings()), wrongValueInDB);
    }

    @AfterMethod
    public void stop() {
        stopSTB();
        stopCHESettings();
    }
}
