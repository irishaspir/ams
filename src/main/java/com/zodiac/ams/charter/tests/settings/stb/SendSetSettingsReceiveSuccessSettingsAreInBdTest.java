package com.zodiac.ams.charter.tests.settings.stb;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.deleteKeysFromSettingsSTBDataV2ByMAC;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getAllKeysWithout65And62KeysFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.http.helpers.settings.UrlHelper.setUriPattern;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_STB;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendSetSettingsReceiveSuccessSettingsAreInBdTest extends StepsForSettings {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_STB)
    @Description("1.3.7 Send setSettings from STB to AMS, AMS send setSettings to CHE, settings don't exist in BD AMS,  " +
            "settings don't exist in BD CHE, settingsRudpHelper migration flag = 0")
    @TestCaseId("12345")
    @Test()
    public void sendSetSettingsReceiveSuccessSettingsAreInBd() {

        String mac = MAC_NUMBER_4;
        setMigrationFlagNull(mac);
        delete62Key(options);
        deleteKeysFromSettingsSTBDataV2ByMAC(mac);
        runSTB();
        runCHESettings();
        startTestTime = getCurrentTimeTest();

        ArrayList<String> list = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(list, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), urlIsIncorrect);

        assertEquals(getAllKeysWithout65And62KeysFromSettingsSTBDataV2(mac), allKeys(options), wrongValueInDB);
        assertJsonEquals(jsonDataUtils.amsSetSettingsRequestWithODP(options, getDeviceIdByMac(mac)), cheEmulatorSettings.checkData());
        createAttachment(cheEmulatorSettings.getJson());
    }

    @AfterMethod
    public void stop() {
        stopSTB();
        stopCHESettings();
    }
}
