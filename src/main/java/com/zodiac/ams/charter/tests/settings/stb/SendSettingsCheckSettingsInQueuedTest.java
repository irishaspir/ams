package com.zodiac.ams.charter.tests.settings.stb;

import com.zodiac.ams.charter.bd.ams.tables.SettingsSyncV2Utils;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getAllKeysWithout65KeyFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_STB;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static org.testng.Assert.assertEquals;

public class SendSettingsCheckSettingsInQueuedTest extends StepsForSettings {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE+SET_SETTINGS_FROM_STB)
    @Description("1.3.9 Send settings from STB to AMS, AMS send setSettings to CHE, CHE is unavailable, repeat sending, CHE is unavailable, check resend ")
    @TestCaseId("12345")
    @Test()
    public void sendSettingsCheckSettingsInQueued() {
        
        String mac=MAC_NUMBER_2;
        setMigrationFlagOne(mac);
        setDefaultValue(options, mac);

        runSTB();
        startTestTime = getCurrentTimeTest();

        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);

        assertEquals(getAllKeysWithout65KeyFromSettingsSTBDataV2(mac), allKeys(options), wrongValueInDB);
        assertEquals(new SettingsSyncV2Utils().isMacInSettingsSyncV2(mac), true, macIsNotInBD);

    }

    @AfterMethod
    public void stop() {
        stopSTB();
    }
}
