package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.ActivityCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.AppScreen;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionActivity;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.TestUtils.isEmptyObject;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAppScreenEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAppSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAvnClientSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountOf1Equals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEventStartTimeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMenuOptionSelectedEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertRemoteKeysEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSequenceNumberEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSessionActivityEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSessionEventReasonEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertStbSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTmsIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputTimeDeltaSum;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.toLocalDateTime;
import static com.zodiac.ams.charter.tests.dc.common.EventId.*;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_2;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_3;

/**
 * The activity CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class ActivityCsvFileAssertion extends AbstractCsvFileAssertion<ActivityCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public ActivityCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<ActivityCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        boolean multiLineFile = isMultiLineFile();

        int eventsCount = calculateInputEventsCount(input);

        // TODO fix later
        if (multiLineFile) {
            assertEntriesCountEquals(eventsCount + 1, csvFileEntries.size());
        } else {
            assertEntriesCountOf1Equals(csvFileEntries.size());
        }

        ListIterator<ActivityCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        //String inputMacId = DOBUtil.mac2string(inputMac);

        int inputMessageType = input.getMessageType();

        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();

            int sequenceNumberCounter = 1;

            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                ActivityCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();

                if (multiLineFile) {
                    if (sequenceNumberCounter == eventsCount + 1) {
                        assertLastLine(inputMac, inputMessageType, sessionData, eventData, csvFileEntry);
                    } else {
                        assertRegularLine(inputMac, inputMessageType, sessionData, eventData, csvFileEntry
                                , sequenceNumberCounter);
                    }
                } else {
                    assertLastLine(inputMac, inputMessageType, sessionData, eventData, csvFileEntry);
                    return;
                }

                sequenceNumberCounter++;
            }
        }
    }

    private void assertLastLine(Long inputMac, int inputMessageType
            , CsvFileAssertionInput.SessionData sessionData
            , CsvFileAssertionInput.EventData eventData
            , ActivityCsvFileEntry csvFileEntry) {
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputFirstEventNumber = sessionData.getFirstEventNumber();

        int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

        int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

        int inputEventId = eventData.getEventId();

        List<Object> inputParameters = eventData.getParameters();
        Integer inputParameterDestinationChannelNumber = (Integer) inputParameters.get(0);

        int inputTimestampPartMsec = eventData.getTimestampPartMsec();

        String appSessionId = csvFileEntry.getAppSessionId();
        SessionActivity sessionActivity = csvFileEntry.getSessionActivity();
        String remoteKeys = csvFileEntry.getRemoteKeys();
        AppScreen appScreen = csvFileEntry.getAppScreen();
        String menuOptionSelected = csvFileEntry.getMenuOptionSelected();
        Integer tmsId = csvFileEntry.getTmsId();
        LocalDateTime eventStartTime = csvFileEntry.getEventStartTime();
        SessionEventReason reason = csvFileEntry.getReason();
        String stbSessionId = csvFileEntry.getStbSessionId();
//        Integer sequenceNumber = csvFileEntry.getSequenceNumber();
//        Integer avnClientSessionId = csvFileEntry.getAvnClientSessionId();
//        String additionalInfo = csvFileEntry.getAdditionalInfo();

        boolean doNothingFlag;

        // appSessionId assertion
        // if AMS-generated HB -> "Z" + "_" + <STB MAC>
        // if 'Event ID' == 11, 'Event ID' == 12 -> "Z" + "_" + <STB MAC> + "_" + <Session start timestamp>
        // if 'Event ID' == 2, 'Event ID' == 5, 'Event ID' == 13, or 'Event ID' == 14:
        //      if current event 'sessionId' == 0 -> "Z" + "_" + <STB MAC> + "_" + <Session start timestamp>
        //      otherwise -> "Z" + "_" + <STB MAC> + "_" + <current event 'sessionId'>
        String inputAppSessionId;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputAppSessionId = "Z" + "_" + inputMacId;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_13
                || inputEventId == EVENT_ID_14) {
            // TODO fix later
            if (isEmptyObject(stbSessionId)) {
                inputAppSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
            } else {
                inputAppSessionId = "Z" + "_" + inputMacId + "_" + stbSessionId;
            }
        } else if (inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE) {
            inputAppSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        assertAppSessionIdEquals(appSessionId, inputAppSessionId);
        // end of appSessionId assertion

        // sessionActivity assertion
        // if AMS-generated HB -> HB
        // if 'Event ID' = 2, 'Event ID' = 5, 'Event ID' == 11, 'Event ID' == 12, 'Event ID' == 13, or 'Event ID' == 14:
        //      current event 'sessionActivityId' is mapped to: com.zodiac.ams.charter.csv.file.entry.enumtype.SessionActivity
        doNothingFlag = false;
        SessionActivity inputSessionActivity = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_ELEVEN
                || inputEventId == EVENT_ID_TWELVE || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        if (!doNothingFlag) {
            assertSessionActivityEquals(sessionActivity, inputSessionActivity);
        }
        // end of sessionActivity assertion

        // remoteKeys assertion
        // Empty TBD
        // TODO fix later
        String inputRemoteKeys = null;
        assertRemoteKeysEquals(remoteKeys, inputRemoteKeys);
        // end of remoteKeys assertion

        // appScreen assertion
        // if AMS-generated HB -> empty
        // if 'Event ID' == 11 or 'Event ID' == 12 -> Empty
        // if 'Event ID' == 2, 'Event ID' == 5, 'Event ID' == 13, or 'Event ID' == 14:
        //      current event 'screen' value is mapped to: com.zodiac.ams.charter.csv.file.entry.enumtype.AppScreen
        doNothingFlag = false;
        AppScreen inputAppScreen = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            assertAppScreenEquals(appScreen, null);
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_13
                || inputEventId == EVENT_ID_14) {
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else if (inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE) {
            inputAppScreen = null;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        if (!doNothingFlag) {
            assertAppScreenEquals(appScreen, inputAppScreen);
        }
        // end of appScreen assertion

        // menuOptionSelected assertion
        // Empty
        // TODO fix later
        String inputMenuOptionSelected = null;
        assertMenuOptionSelectedEquals(menuOptionSelected, inputMenuOptionSelected);
        // end of menuOptionSelected assertion

        // tmsId assertion
        // if AMS-generated HB -> empty
        // if 'messageType' < 2 -> empty
        // otherwise:
        //      if 'Event ID' == 5, 'Event ID' == 11, 'Event ID' == 12, 'Event ID' == 13 or 'Event ID' == 14 -> empty
        //      if 'Event ID' == 2:
        //          if current event 'sessionActivityId' != 2 -> empty
        //          otherwise:
        //          if current event 'channel' == 0 -> empty
        //          otherwise - > current event 'channel' value
        Integer inputTmsId = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputTmsId = null;
        } else if (inputMessageType < MESSAGE_TYPE_2) {
            inputTmsId = null;
        } else {
            if (inputEventId == EVENT_ID_TWO) {
                if (sessionActivity != SessionActivity.SESSION_ACTIVITY_2) {
                    inputTmsId = null;
                } else {
                    if (isEmptyObject(inputParameterDestinationChannelNumber)) {
                        inputTmsId = null;
                    } else {
                        inputTmsId = inputParameterDestinationChannelNumber;
                    }
                }
            } else if (inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE
                    || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
                inputTmsId = null;
            } else {// WTF?
                throw new IllegalStateException("Unsupported input eventId of " + inputEventId);
            }
        }
        assertTmsIdEquals(tmsId, inputTmsId);
        // end of tmsId assertion

        // eventStartTime assertion
        // if AMS-generated HB -> time when the AM message was received by AMS
        // if 'messageType' <= 2 and <timestampPartMsec> == 0 -> <Session start timestamp> + sum of respective <Time delta>s, in yyyy-MM-DDThh:mm:ss format
        // otherwise -> <Session start timestamp> + sum of respective <Time delta>s + <timestampPartMsec>msec, in yyyy-MM-DDThh:mm:ss.xxx format
        doNothingFlag = false;
        long inputEventStartTimeMsec = 0;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else if (inputMessageType <= MESSAGE_TYPE_2 && inputSessionStartTimestamp == 0) {
            inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
        } else {
            inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                    + inputTimestampPartMsec;
        }
        if (!doNothingFlag) {
            LocalDateTime inputEventStartTime = toLocalDateTime(inputEventStartTimeMsec);
            assertEventStartTimeEquals(eventStartTime, inputEventStartTime);
        }
        // end of eventStartTime assertion

        // reason assertion
        // if AMS-generated HB -> empty
        // if 'Event ID' == 11 -> empty
        // if 'Event ID' == 2, 'Event ID' == 5, 'Event ID' == 12, 'Event ID' == 13 or 'Event ID' == 14:
        //      if current event 'reason' == 0 -> Empty
        //      otherwise -> current event 'reason' value mapped to text code according to Section 3 'Reason' field description
        doNothingFlag = false;
        SessionEventReason inputReason = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputReason = null;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_TWELVE
                || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
            if (reason == null) {
                inputReason = null;
            } else {
                // TODO fix later
                // do nothing here
                doNothingFlag = true;
            }
        } else if (inputEventId == EVENT_ID_ELEVEN) {
            // TODO fix later
            inputReason = null;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        if (!doNothingFlag) {
            assertSessionEventReasonEquals(reason, inputReason);
        }
        // end of reason assertion

        // stbSessionId assertion
        // if AMS-generated HB -> the same as <App Session ID> for AMS-generated HB
        // otherwise -> the same as <STB Session ID> field in TUNING.CSV
        String inputStbSessionId = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputStbSessionId = inputAppSessionId;
        } else {
            inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
        }
        assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
        // end of stbSessionId assertion
    }

    private void assertRegularLine(Long inputMac, int inputMessageType
            , CsvFileAssertionInput.SessionData sessionData
            , CsvFileAssertionInput.EventData eventData
            , ActivityCsvFileEntry csvFileEntry
            , int sequenceNumberCounter) {
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputFirstEventNumber = sessionData.getFirstEventNumber();

        int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

        int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

        int inputEventId = eventData.getEventId();

        List<Object> inputParameters = eventData.getParameters();

        Integer inputParameterApp = null;
        Integer inputParameterChannel = null;
        Integer inputParameterClientSessionId = null;
        Integer inputParameterDestinationChannelNumber = null;
        Integer inputParameterReason = null;
        Integer inputParameterScreen = null;
        Integer inputParameterScreenMode = null;
        Integer inputParameterSessionActivityId = null;
        Integer inputParameterSessionId = null;
        Integer inputParameterSignalLevel = null;
        Integer inputParameterType = null;

        Integer inputParameterInternalHddCapacity = null;
        Integer inputParameterExternalHddCapacity = null;
        Integer inputParameterInternalHddFreeSpacePercent = null;
        Integer inputParameterExternalHddFreeSpacePercent = null;


        if(inputEventId == EVENT_ID_ZERO) {
            inputParameterDestinationChannelNumber = (Integer) inputParameters.get(0);
            inputParameterReason = (Integer) inputParameters.get(1);
            inputParameterSignalLevel = (Integer) inputParameters.get(2);
        } else if(inputEventId == EVENT_ID_ONE) {
            inputParameterApp  = (Integer) inputParameters.get(0);
            inputParameterSessionId = (Integer) inputParameters.get(1);
            inputParameterReason = (Integer) inputParameters.get(2);
        } else if(inputEventId == EVENT_ID_TWO) {
            inputParameterSessionId = (Integer) inputParameters.get(0);
            inputParameterSessionActivityId = (Integer) inputParameters.get(1);
            inputParameterScreen = (Integer) inputParameters.get(2);
            inputParameterReason = (Integer) inputParameters.get(3);
            inputParameterChannel = (Integer) inputParameters.get(4);
        } else if(inputEventId == EVENT_ID_THREE) {
            inputParameterChannel = (Integer) inputParameters.get(0);
            inputParameterType = (Integer) inputParameters.get(1);
            inputParameterReason = (Integer) inputParameters.get(2);
            inputParameterScreenMode = (Integer) inputParameters.get(3);
        } else  if(inputEventId == EVENT_ID_FOUR) {
            inputParameterApp  = (Integer) inputParameters.get(0);
            inputParameterSessionId = (Integer) inputParameters.get(1);
            inputParameterReason = (Integer) inputParameters.get(2);
            inputParameterClientSessionId = (Integer) inputParameters.get(3);
        } else if (inputEventId == EVENT_ID_FIVE) {
            inputParameterSessionId = (Integer) inputParameters.get(0);
            inputParameterSessionActivityId = (Integer) inputParameters.get(1);
            inputParameterScreen = (Integer) inputParameters.get(2);
            inputParameterReason = (Integer) inputParameters.get(3);
            inputParameterClientSessionId = (Integer) inputParameters.get(4);
        } else if (inputEventId == EVENT_ID_SIX) {
            inputParameterInternalHddCapacity = (Integer) inputParameters.get(0);
            inputParameterExternalHddCapacity = (Integer) inputParameters.get(1);
            inputParameterInternalHddFreeSpacePercent = (Integer) inputParameters.get(2);
            inputParameterExternalHddFreeSpacePercent = (Integer) inputParameters.get(3);
        } else if (inputEventId == EVENT_ID_ELEVEN) {
            inputParameterSessionActivityId = (Integer) inputParameters.get(0);
        } else if (inputEventId == EVENT_ID_TWELVE) {
            inputParameterSessionActivityId = (Integer) inputParameters.get(0);
            inputParameterReason = (Integer) inputParameters.get(1);
        }

        int inputTimestampPartMsec = eventData.getTimestampPartMsec();

        String appSessionId = csvFileEntry.getAppSessionId();
        SessionActivity sessionActivity = csvFileEntry.getSessionActivity();
        String remoteKeys = csvFileEntry.getRemoteKeys();
        AppScreen appScreen = csvFileEntry.getAppScreen();
        String menuOptionSelected = csvFileEntry.getMenuOptionSelected();
        Integer tmsId = csvFileEntry.getTmsId();
        LocalDateTime eventStartTime = csvFileEntry.getEventStartTime();
        SessionEventReason reason = csvFileEntry.getReason();
        String stbSessionId = csvFileEntry.getStbSessionId();
        Integer sequenceNumber = csvFileEntry.getSequenceNumber();
        Integer avnClientSessionId = csvFileEntry.getAvnClientSessionId();
        String additionalInfo = csvFileEntry.getAdditionalInfo();

        boolean doNothingFlag;

        // appSessionId assertion
        // if AMS-generated HB -> "Z" + "_" + <STB MAC>
        // if 'Event ID' == 11, 'Event ID' == 12 -> "Z" + "_" + <STB MAC> + "_" + <Session start timestamp>
        // if 'Event ID' == 2, 'Event ID' == 5, 'Event ID' == 13, or 'Event ID' == 14:
        //      if current event 'sessionId' == 0 -> "Z" + "_" + <STB MAC> + "_" + <Session start timestamp>
        //      otherwise -> "Z" + "_" + <STB MAC> + "_" + <current event 'sessionId'>
        String inputAppSessionId;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputAppSessionId = "Z" + "_" + inputMacId;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_13
                || inputEventId == EVENT_ID_14) {
            // TODO fix later
            if (isEmptyObject(inputParameterSessionId)) {
                inputAppSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
            } else {
                inputAppSessionId = "Z" + "_" + inputMacId + "_" + inputParameterSessionId;
            }
        } else if (inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE) {
            inputAppSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        assertAppSessionIdEquals(appSessionId, inputAppSessionId);
        // end of appSessionId assertion

        // sessionActivity assertion
        // if AMS-generated HB -> HB
        // if 'Event ID' = 2, 'Event ID' = 5, 'Event ID' == 11, 'Event ID' == 12, 'Event ID' == 13, or 'Event ID' == 14:
        //      current event 'sessionActivityId' is mapped to: com.zodiac.ams.charter.csv.file.entry.enumtype.SessionActivity
        doNothingFlag = false;
        SessionActivity inputSessionActivity = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_ELEVEN
                || inputEventId == EVENT_ID_TWELVE || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        if (!doNothingFlag) {
            assertSessionActivityEquals(sessionActivity, inputSessionActivity);
        }
        // end of sessionActivity assertion

        // remoteKeys assertion
        // Empty TBD
        // TODO fix later
        String inputRemoteKeys = null;
        assertRemoteKeysEquals(remoteKeys, inputRemoteKeys);
        // end of remoteKeys assertion

        // appScreen assertion
        // if AMS-generated HB -> empty
        // if 'Event ID' == 11 or 'Event ID' == 12 -> Empty
        // if 'Event ID' == 2, 'Event ID' == 5, 'Event ID' == 13, or 'Event ID' == 14:
        //      current event 'screen' value is mapped to: com.zodiac.ams.charter.csv.file.entry.enumtype.AppScreen
        doNothingFlag = false;
        AppScreen inputAppScreen = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            assertAppScreenEquals(appScreen, null);
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_13
                || inputEventId == EVENT_ID_14) {
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else if (inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE) {
            inputAppScreen = null;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        if (!doNothingFlag) {
            assertAppScreenEquals(appScreen, inputAppScreen);
        }
        // end of appScreen assertion

        // menuOptionSelected assertion
        // Empty
        // TODO fix later
        String inputMenuOptionSelected = null;
        assertMenuOptionSelectedEquals(menuOptionSelected, inputMenuOptionSelected);
        // end of menuOptionSelected assertion

        // tmsId assertion
        // if AMS-generated HB -> empty
        // if 'messageType' < 2 -> empty
        // otherwise:
        //      if 'Event ID' == 5, 'Event ID' == 11, 'Event ID' == 12, 'Event ID' == 13 or 'Event ID' == 14 -> empty
        //      if 'Event ID' == 2:
        //          if current event 'sessionActivityId' != 2 -> empty
        //          otherwise:
        //          if current event 'channel' == 0 -> empty
        //          otherwise - > current event 'channel' value
        Integer inputTmsId = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputTmsId = null;
        } else if (inputMessageType < MESSAGE_TYPE_2) {
            inputTmsId = null;
        } else {
            if (inputEventId == EVENT_ID_TWO) {
                if (sessionActivity != SessionActivity.SESSION_ACTIVITY_2) {
                    inputTmsId = null;
                } else {
                    if (isEmptyObject(inputParameterChannel)) {
                        inputTmsId = null;
                    } else {
                        inputTmsId = inputParameterChannel;
                    }
                }
            } else if (inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE
                    || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
                inputTmsId = null;
            } else {// WTF?
                throw new IllegalStateException("Unsupported input eventId of " + inputEventId);
            }
        }
        assertTmsIdEquals(tmsId, inputTmsId);
        // end of tmsId assertion

        // eventStartTime assertion
        // if AMS-generated HB -> time when the AM message was received by AMS
        // if 'messageType' <= 2 and <timestampPartMsec> == 0 -> <Session start timestamp> + sum of respective <Time delta>s, in yyyy-MM-DDThh:mm:ss format
        // otherwise -> <Session start timestamp> + sum of respective <Time delta>s + <timestampPartMsec>msec, in yyyy-MM-DDThh:mm:ss.xxx format
        doNothingFlag = false;
        long inputEventStartTimeMsec = 0;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        } else if (inputMessageType <= MESSAGE_TYPE_2 && inputSessionStartTimestamp == 0) {
            inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
        } else {
            inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                    + inputTimestampPartMsec;
        }
        if (!doNothingFlag) {
            LocalDateTime inputEventStartTime = toLocalDateTime(inputEventStartTimeMsec);
            assertEventStartTimeEquals(eventStartTime, inputEventStartTime);
        }
        // end of eventStartTime assertion

        // reason assertion
        // if AMS-generated HB -> empty
        // if 'Event ID' == 11 -> empty
        // if 'Event ID' == 2, 'Event ID' == 5, 'Event ID' == 12, 'Event ID' == 13 or 'Event ID' == 14:
        //      if current event 'reason' == 0 -> Empty
        //      otherwise -> current event 'reason' value mapped to text code according to Section 3 'Reason' field description
        doNothingFlag = false;
        SessionEventReason inputReason = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputReason = null;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_TWELVE
                || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
            if (reason == null) {
                inputReason = null;
            } else {
                // TODO fix later
                // do nothing here
                doNothingFlag = true;
            }
        } else if (inputEventId == EVENT_ID_ELEVEN) {
            // TODO fix later
            inputReason = null;
        } else {// WTF?
            throw new IllegalStateException("Unsupported sessionActivity of " + sessionActivity
                    + " or input eventId of " + inputEventId);
        }
        if (!doNothingFlag) {
            assertSessionEventReasonEquals(reason, inputReason);
        }
        // end of reason assertion

        // stbSessionId assertion
        // if AMS-generated HB -> the same as <App Session ID> for AMS-generated HB
        // otherwise -> the same as <STB Session ID> field in TUNING.CSV
        String inputStbSessionId = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputStbSessionId = inputAppSessionId;
        } else {
            inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
        }
        assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
        // end of stbSessionId assertion

        // sequenceNumber assertion
        // if AMS-generated HB -> empty
        // otherwise -> the same as <Sequence Number> field in TUNING.CSV
        doNothingFlag = false;
        Integer inputSequenceNumber = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
        } else {
            if (inputMessageType < MESSAGE_TYPE_3) {
                inputSequenceNumber = null;
            } else {
                // TODO fix later
                inputSequenceNumber = inputFirstEventNumber + (sequenceNumberCounter - 1);
            }
            // TODO fix later
            // do nothing here
            doNothingFlag = true;
        }
        if (!doNothingFlag) {
            assertSequenceNumberEquals(sequenceNumber, inputSequenceNumber);
        }
        // end of sequenceNumber assertion

        // avnClientSessionId assertion
        // if AMS-generated HB -> empty
        // if 'Event ID' == 2, 'Event ID' == 11, 'Event ID' == 12, 'Event ID' == 13 or 'Event ID' == 14 -> empty
        // if 'Event ID' == 5:
        //      if current event 'clientSessionId' == 0 -> empty
        //      otherwise -> current event 'clientSessionId' value
        Integer inputAvnClientSessionId = null;
        if (sessionActivity == SessionActivity.SESSION_ACTIVITY_3) {// WTF?
            // TODO fix later
            inputAvnClientSessionId = null;
        } else if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_ELEVEN || inputEventId == EVENT_ID_TWELVE
                || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
            inputAvnClientSessionId = null;
        } else if (inputEventId == EVENT_ID_FIVE) {
            // TODO fix later
            if (isEmptyObject(inputParameterClientSessionId)) {
                inputAvnClientSessionId = null;
            } else {
                inputAvnClientSessionId = inputParameterClientSessionId;
            }
        }
        assertAvnClientSessionIdEquals(avnClientSessionId, inputAvnClientSessionId);
        // end of avnClientSessionId assertion
// TODO fix later
//        // additionalInfo assertion
//        // if 'Event ID' == 14:
//        //      lookup element from <string array> with index = 'additionalInfo'
//        //      if not found -> empty
//        // otherwise -> empty
//        String inputAdditionalInfo = null;
//        assertAdditionalInfoEquals(additionalInfo, inputAdditionalInfo);
//        // end of additionalInfo assertion
    }

    private boolean isMultiLineFile() {
        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();
            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                int inputEventId = eventData.getEventId();

                if (inputEventId == EVENT_ID_TWO || inputEventId == EVENT_ID_FIVE || inputEventId == EVENT_ID_ELEVEN
                        || inputEventId == EVENT_ID_TWELVE || inputEventId == EVENT_ID_13 || inputEventId == EVENT_ID_14) {
                    return true;
                }
            }
        }
        return false;
    }
}
