package com.zodiac.ams.charter.tests.activityControl;

import com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils;
import com.zodiac.ams.charter.helpers.activityControl.DataForActivityControl;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.json.JSONObject;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.setSTBModeInSTBMetadata;
import static com.zodiac.ams.charter.http.helpers.activityControl.ActivityControlGetHelper.*;
import static com.zodiac.ams.charter.http.listeners.activityControl.ActivityControlGetParams.DIGITS;
import static com.zodiac.ams.charter.http.listeners.activityControl.ActivityControlGetParams.SPECIAL_SYMBOLS;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;

public class SendGetSTBStatusRequestTest extends StepsForActivityControl {

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.01, 13.1.13, 13.1.14, 13.1.15, 13.1.16, 13.1.17, 13.1.20 Send valid GET STB STATUS REQUEST")
    @TestCaseId("171556, 171568, 171569, 171570, 171571, 171572, 171573")
    @Test (dataProvider = "networkType", dataProviderClass = DataForActivityControl.class)
    public void sendValidGetStbStatusRequest(int stbMode, String networkType) {
        setSTBModeInSTBMetadata(stbMode, MAC_NUMBER_1);
        STBLastActivityUtils.insertToSTBLastActivity(MAC_NUMBER_1, 1495112237883L);
        assertEquals(sendGetStbStatusRequestDeviceId(DEVICE_ID_NUMBER_1), SUCCESS_200);
        JSONObject response = responseJson;
        assertJsonEquals(response, activityControlJsonDataUtils.createResponseJsonWithNetworkType(networkType));
        createAttachment(response);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.02 Send GET STB STATUS REQUEST with mistake in 'req' parameter name (delete 'q')")
    @TestCaseId("171557")
    @Test()
    public void sendGetStbStatusRequestInvalidReqParameter() {
        assertEquals(sendGetStbStatusRequestWithMistakeInReq(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.03 Send GET STB STATUS REQUEST with empty value of 'req' parameter")
    @TestCaseId("171558")
    @Test()
    public void sendGetStbStatusRequestWithEmptyReq() {
        assertEquals(sendGetStbStatusRequestReq(EMPTY), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.04 Send GET STB STATUS REQUEST with digits in 'req' parameter")
    @TestCaseId("171559")
    @Test()
    public void sendGetStbStatusRequestWithDigitsInReq() {
        assertEquals(sendGetStbStatusRequestReq(DIGITS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.05 Send GET STB STATUS REQUEST with special symbols in 'req' parameter")
    @TestCaseId("171560")
    @Test()
    public void sendGetStbStatusRequestWithSpecialSymbolsInReq() {
        assertEquals(sendGetStbStatusRequestReq(SPECIAL_SYMBOLS), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.06 Send GET STB STATUS REQUEST without 'req' parameter")
    @TestCaseId("171561")
    @Test()
    public void sendGetStbStatusRequestWithoutReqParameter() {
        assertEquals(sendGetStbStatusRequestWithoutReq(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.07 Send GET STB STATUS REQUEST without 'deviceId' parameter")
    @TestCaseId("171562")
    @Test()
    public void sendGetStbStatusRequestWithoutDeviceIdParameter() {
        assertEquals(sendGetStbStatusRequestWithoutDeviceId(), ERROR_400);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.08 Send GET STB STATUS REQUEST with empty value of 'deviceId' parameter")
    @TestCaseId("171563")
    @Test()
    public void sendGetStbStatusRequestWithEmptyDeviceId() {
        assertEquals(sendGetStbStatusRequestDeviceId(EMPTY), ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.09 Send GET STB STATUS REQUEST with digits in 'deviceId' parameter")
    @TestCaseId("171564")
    @Test()
    public void sendGetStbStatusRequestWithDigitsInDeviceId() {
        assertEquals(sendGetStbStatusRequestDeviceId(DIGITS), ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.10 Send GET STB STATUS REQUEST with special symbols in 'deviceId' parameter")
    @TestCaseId("171565")
    @Test()
    public void sendGetStbStatusRequestWithSpecialSymbolsInDeviceId() {
        assertEquals(sendGetStbStatusRequestDeviceId(SPECIAL_SYMBOLS), ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

    @Features(FeatureList.ACTIVITY_CONTROL)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_STB_STATUS_REQUEST)
    @Description("13.1.11 Send GET STB STATUS REQUEST with nonexistent STB MAC in 'deviceId' parameter")
    @TestCaseId("171566")
    @Test()
    public void sendGetStbStatusRequestWithNonexistentSTBMac() {
        assertEquals(sendGetStbStatusRequestDeviceId(UNKNOWN_DEVICE_ID), ERROR_404);
        createAttachment(readAMSLog());
        createAttachment(responseString);
    }

}
