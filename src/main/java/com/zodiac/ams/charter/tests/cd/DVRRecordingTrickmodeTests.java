package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.DVR_TRICKMODE_REQUEST_ID;
import static com.zodiac.ams.charter.tests.StoriesList.DVR_RECORDING_TRICKMODE;

/**
 * Created by SpiridonovaIM on 20.07.2017.
 */
public class DVRRecordingTrickmodeTests extends CDBeforAfter {

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DVR_RECORDING_TRICKMODE)
    @Description("10.7.4  Message from device to AMS where <request id>=7, mode and speed and interval aren't specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(priority = 1)
    public void sendTrickModeReceiveError() {
        String deviceId = DEVICE_ID_NUMBER_1;
        cdRudpHelper.sendRequest(deviceId, DVR_TRICKMODE_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        byte[] data = cdstbEmulator.getData();
        assertLog(log, deviceId);

    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DVR_RECORDING_TRICKMODE)
    @Description("10.7.5  Message from device to AMS where <request id>=7,  speed and interval aren't specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "mode", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendTrickModeKeyOffsetIsNullReceiveError(int mode) {
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createTrickModeMessage(mode);
        cdRudpHelper.sendRequest(deviceId, message, DVR_TRICKMODE_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertLog(log, deviceId);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DVR_RECORDING_TRICKMODE)
    @Description("10.7.5  Message from device to AMS where <request id>=7,  interval isn't specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "modeAndSpeed", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendTrickModeRecordingIdOffsetIs(int mode, int speed) {
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createTrickModeMessage(mode, speed);
        cdRudpHelper.sendRequest(deviceId, message, DVR_TRICKMODE_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertLog(log, deviceId);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_TRICKMODE)
    @Description("10.7.5  Message from device to AMS where <request id>=7, mode and speed and interval are specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "modeAndSpeedAndInterval", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendTrickModeRecordingIdOffsetAndIntervalIs(int mode, int speed, int interval) {
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createTrickModeMessage(mode, speed, interval);
        cdRudpHelper.sendRequest(deviceId, message, DVR_TRICKMODE_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertTrickMode(DVR_TRICKMODE_REQUEST_ID, mode, speed, interval);
    }


}
