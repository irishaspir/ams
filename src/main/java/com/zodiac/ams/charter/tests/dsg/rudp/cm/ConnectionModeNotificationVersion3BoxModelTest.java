package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils.isMacInSTBLastActivity;
import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;

import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_3_STRING;
import static org.testng.Assert.assertEquals;

/**
 * Created by SpiridonovaIM on 26.06.2017.
 */
public class ConnectionModeNotificationVersion3BoxModelTest extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.3 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> ISN'T specified, " +
            "unknown mac, receive ERROR_VERSION3_PROTOCOL CODE = 0 ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test()
    public void sendVersion3RequestId0ConnectionModeUnknownMac() {
        String mac = unknownMac();
        message = version3Request.createMessage(mac, connectionMode);
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertEquals(true, isMacInMacIp(mac), UNKNOWN + MAC_IS_NOT_IN_MAC_IP);
        assertEquals(true, isMacInSTBLastActivity(mac), CHANGE_TIME_STAMP + AND + LAST_ACTIVITY_STAMP + NOT_EQUAL);
        assertLog(log, Long.parseLong(mac), msgType, connectionMode, boxModel, vendor);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
     //   assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
        assertJson(mac, connectionMode,getBoxModel());
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> ISN'T specified,  " +
            "box model changes")
    @TestCaseId("123")
    @Issue("CHRAMS-270/:view/")
    @Test(dataProvider = "box models", dataProviderClass = DSGDataProvider.class, priority = 1)
    public void sendVersion3RequestId0BoxModelChanges(int boxModel, int vendor) {
        connectionMode = changeConnectionMode();
        message = version3Request.createMessage(mac, connectionMode, String.valueOf(boxModel));
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode, boxModel);
     //   assertDB(getModelId(UNKNOWN), vendor, boxModel, mac);
        assertJson(mac, connectionMode,boxModel, vendor);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified,  " +
            "box model unknown")
    @TestCaseId("123")
    @Issue("CHRAMS-270/:view/")
    @Test(priority = 1)
    public void sendVersion3RequestId0BoxModelUnknown() {
        connectionMode = changeConnectionMode();
        int boxModel = 8975;
        message = version3Request.createMessage(mac, connectionMode, String.valueOf(boxModel));
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode, boxModel);
     //   assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), boxModel, mac);
        assertJson(mac,connectionMode, boxModel);
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified" +
            "romId and VendorId changes")
    @Test(dataProvider = "romId and VendorId", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion3RequestId0VendorIsSpecifiedRomIdChange(int romId, int vendor) {
        connectionMode = changeConnectionMode();
        message = version3Request.createMessage(mac, connectionMode, romId, vendor);
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode, romId, vendor);
    //    assertDB(romId, vendor, getModelId(UNKNOWN), mac);
        assertJson(mac,connectionMode, getBoxModel(romId), (Object) vendor);
    }


}
