package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.rudp.dsg.message.cm.StbMessageTypeMapBuilder;
import com.zodiac.ams.charter.services.stb.BoxModelOptions;
import com.zodiac.ams.charter.services.stb.BoxModels;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.DataProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBModelsUtils.getDataFromStbModels;
import static com.zodiac.ams.charter.services.dsg.MessageTypesMapList.*;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.migrationDataWillBeSentSoon;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.standardSTBRunning;
import static com.zodiac.ams.charter.store.TestKeyWordStore.*;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.PATH_TO_DSG_DATA;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.ROM_ID_TXT;


public class DSGDataProvider {

    @DataProvider(name = "connectionMode")
    public static Object[][] getConnectionMode() {
        return new Object[][]{
                {DOCSIS, ALOHA},
                {DOCSIS, DAVIC},
                {ALOHA, DAVIC},
                {ALOHA, DOCSIS},
                {DAVIC, DOCSIS},
                {DAVIC, ALOHA},
        };
    }

    @DataProvider(name = "migrationStart")
    public static Object[][] getMigrationStart() {
        return new Object[][]{
                {standardSTBRunning},
                {migrationDataWillBeSentSoon},
        };
    }

    @DataProvider(name = "messageTypesMapPPV")
    public static Object[][] getMessageTypesMapPPV() {
        String[] range = PPV.getOption().getRange();
        Object[][] objects = new Object[range.length][1];
        for (int i = 0; i < range.length; i++) {
            objects[i][0] = Integer.parseInt(range[i]);
        }
        return objects;
    }

    @DataProvider(name = "messageTypesMap")
    public static Object[][] getMessageTypesMap1() {
        List<String[]> opts = allMessageTypesMapOptionsWithRange();
        Object[][] objects = new Object[opts.size()][1];
        int i = 0;
        long y = 0;
        for (String[] opt : opts) {
            if (opt[0].equals(PPV.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setPPVMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(SETTINGS.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setSettingsMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(CALLER_ID.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setCallerIdMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(COMPANION_DEVICES.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setCompanionDevicesMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(DVR.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setDvrMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(DVR_MIGRATION.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setDvrMigrationMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(MOTOROLA_PACKAGES.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setMotorolaPackagesMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(EPG_MOTOROLA.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setEpgForMotorola2kMessageType((byte) Integer.parseInt(opt[1])).build();
            } else if (opt[0].equals(INTERACTIVE_CHANEL_AUTHORIZATION.getOption().getName())) {
                y = new StbMessageTypeMapBuilder().setInteractiveChannelAuthorizationMessageType((byte) Integer.parseInt(opt[1])).build();
            }
            objects[i][0] = y;
            i++;
        }
        return objects;
    }

    @DataProvider(name = "box models")
    public static Object[][] getBoxModel() {
        List<BoxModelOptions> boxModelsList = new BoxModels(getDataFromStbModels()).getBoxModelsList();
        Object[][] objects = new Object[5][2];
        for (int i = 0; i < 5; i++) {
            objects[i][0] = boxModelsList.get(i + 10).getModelId();
            objects[i][1]=  boxModelsList.get(i + 10).getVendorId();
        }
        return objects;
    }

//    @DataProvider(name = "romId and VendorId")
//    public static Object[][] getRomeId() {
//        List<BoxModelOptions> boxModelsList = readRomIdAndVendorsFromFile().getBoxModelsList();
//        Object[][] objects = new Object[boxModelsList.size()][1];
//        for (int i = 0; i < boxModelsList.size(); i++) {
//            objects[i][0] = boxModelsList.get(i);
//        }
//        return objects;
//    }

    @DataProvider(name = "romId and VendorId")
    public static Object[][] getRomeId() {
        List<BoxModelOptions> boxModelsList = readRomIdAndVendorsFromFile().getBoxModelsList();
        Object[][] objects = new Object[boxModelsList.size()][2];
        for (int i = 0; i < boxModelsList.size(); i++) {
            objects[i][0] = boxModelsList.get(i).getModelId();
            objects[i][1] = boxModelsList.get(i).getVendorId();
        }
        return objects;
    }

    public static BoxModels readRomIdAndVendorsFromFile() {
        Logger.info("Read " + PATH_TO_DSG_DATA + ROM_ID_TXT);

        List<List<String>> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(PATH_TO_DSG_DATA + ROM_ID_TXT))) {
            String[] sb;
            String line;

            while ((line = br.readLine()) != null) {
                sb = line.split(" - ");
                List<String> model = new ArrayList<>();
                for (int i = 0; i < sb.length; i++) model.add(sb[i]);
                list.add(model);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BoxModels(list);
    }

    @DataProvider(name = "id")
    public static Object[][] getHubId() {
        return new Object[][]{
                {0},
                {5},
                {2},
                {10},
        };
    }


    @DataProvider(name = "ipAddress")
    public static Object[][] getLoadBalancerIpAddress() {
        return new Object[][]{
                {"0.0.0.0"},
                {"192.168.25.131"},
                {new Config().getAmsIp()},
        };
    }


    @DataProvider(name = "serialNumber")
    public static Object[][] getSerialNumber() {
        return new Object[][]{
                {"1234"},
                {"888"},
                {"738"},
        };
    }

    @DataProvider(name = "cmMacAddress")
    public static Object[][] getCmMacAddress() {
        return new Object[][]{
                {DEVICE_ID_NUMBER_1},
                {DEVICE_ID_NUMBER_2},
                {DEVICE_ID_NUMBER_3},
                {DEVICE_ID_NUMBER_4},
        };
    }


}
