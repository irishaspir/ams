package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.rudp.cd.message.CDRequestBuider;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

/**
 * Created by SpiridonovaIM on 19.07.2017.
 */
public class CDBeforAfter extends AssertLog {

    protected CDRequestBuider cdRequestBuider= new CDRequestBuider();
    protected List<CDProtocolOption> message;

    @BeforeClass
    public void startEmulatorsForCD() {
        runSTB();
    }

    @AfterClass
    public void stopEmulatorsForCD() {
        stopSTB();
    }

    @BeforeMethod
    public void checkDataInBD() {
        startTestTime = getCurrentTimeTest();
    }


}
