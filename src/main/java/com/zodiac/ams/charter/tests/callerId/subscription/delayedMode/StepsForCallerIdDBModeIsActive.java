package com.zodiac.ams.charter.tests.callerId.subscription.delayedMode;

import com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils;
import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.callerId.StepsForCallerId;
import com.zodiac.ams.charter.tests.callerId.subscription.StepsForCallerIdSubscription;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostSettingsCallerIdNotification;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

public class StepsForCallerIdDBModeIsActive extends StepsForCallerIdSubscription{

    @BeforeMethod
    public void beforeMethod() {
        new CharterSubscriptionUtils().deleteAll();
        runSTBCallerId();
    }

    @AfterMethod
    public void afterMethod() {
      runCHECallerId();
      waitTimeout();
      stopSTBCallerId();
      stopCHECallerId();
    }

    @Step
    protected void activateDBModeForUnsubscription(String id){
        turnOnCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOff();
        sa.assertEquals(sendPostSettingsCallerIdNotification(id, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }

    @Step
    protected void activateDBModeForSubscription(String id){
        turnOffCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOn();
        sa.assertEquals(sendPostSettingsCallerIdNotification(id, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }
}
