package com.zodiac.ams.charter.tests.rollback.rollbackToRoviFirmware;

import com.zodiac.ams.charter.tests.rollback.StepsForRollback;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class StepsForRollbackSTBOn extends StepsForRollback {

    @BeforeMethod
    protected void beforeMethod() {
        runSTBRollback();
        getTestTime();
    }

    @AfterMethod
    protected void afterMethod() {
        stopSTBRollback();
    }

}
