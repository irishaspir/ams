package com.zodiac.ams.charter.tests.callerId.subscription.delayedMode;

import com.zodiac.ams.charter.emuls.stb.settings.SettingsSTBEmulator;
import com.zodiac.ams.charter.rudp.settings.SettingsRudpHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils.getAllFromSubscriptions;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.setMigrationFlagNullInSettingsSTB;
import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostSettingsCallerIdNotification;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendPostSubscriptionRequestInDelayedModeDBModeIsActiveTest extends StepsForCallerIdDBModeIsActive {

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.5a Attempt of STB subscription when DB mode is active, CHE is unavailable")
    @TestCaseId("12345")
    @Test(priority = 1)
    public void attemptOfSTBSubscriptionWhenDBModeActiveCHEIsUnavailable() {
        activateDBModeForSubscription(DEVICE_ID_NUMBER_1);
        getTestTime();
        turnOffCallerIdNotification();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, setCallerIdNotificationOn()), SUCCESS_200);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternDBModeIsActive(callerIDLog.getLogAMSForValidDBModeIsActive(log)));
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterSubscriptions(DEVICE_ID_NUMBER_1));
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.5b Attempt of STB unsubscription when DB mode is active, CHE is unavailable")
    @TestCaseId("12345")
    @Test(priority = 2)
    public void attemptOfSTBUnsubscriptionWhenDBModeActiveCHEIsUnavailable() {
        activateDBModeForUnsubscription(DEVICE_ID_NUMBER_1);
        getTestTime();
        turnOnCallerIdNotification();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, setCallerIdNotificationOff()), SUCCESS_200);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternDBModeIsActive(callerIDLog.getLogAMSForValidDBModeIsActive(log)));
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterUnsubscriptions(DEVICE_ID_NUMBER_1));
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.8a Attempt of STB subscription when DB mode is active, CHE is available")
    @TestCaseId("12345")
    @Test(priority = 3)
    public void attemptOfSTBSubscriptionWhenDBModeActiveCHEIsAvailable() {
        activateDBModeForSubscription(DEVICE_ID_NUMBER_1);
        runCHECallerId();
        getTestTime();
        turnOffCallerIdNotification();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, setCallerIdNotificationOn()), SUCCESS_200);
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternDBModeIsActive(callerIDLog.getLogAMSForValidDBModeIsActive(log)));
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterSubscriptions(DEVICE_ID_NUMBER_1));
     }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.8b Attempt of STB unsubscription when DB mode is active, CHE is available")
    @TestCaseId("12345")
    @Test(priority = 4)
    public void attemptOfSTBUnsubscriptionWhenDBModeActiveCHEIsAvailable() {
        activateDBModeForUnsubscription(DEVICE_ID_NUMBER_2);
        runCHECallerId();
        getTestTime();
        turnOnCallerIdNotification();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_2, setCallerIdNotificationOff()), SUCCESS_200);
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternDBModeIsActive(callerIDLog.getLogAMSForValidDBModeIsActive(log)));
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterUnsubscriptions(DEVICE_ID_NUMBER_2));
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.9a Subscription in DB mode after the first boot")
    @TestCaseId("12345")
    @Test(priority = 5)
    public void subscriptionInDBModeAfterFirstBoot() {
        activateDBModeForSubscription(DEVICE_ID_NUMBER_2);
        getTestTime();
        stopSTBCallerId();
        SettingsSTBEmulator settingsSTBEmulator = new SettingsSTBEmulator();
        settingsSTBEmulator.registerConsumer();
        settingsSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");

        SettingsRudpHelper settingsRudpHelper = new SettingsRudpHelper(settingsSTBEmulator);
        setMigrationFlagNullInSettingsSTB(MAC_NUMBER_2);
        settingsRudpHelper.sendSetSettings(MAC_NUMBER_2, setCallerIdNotificationOn());
        settingsSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternDBModeIsActive(callerIDLog.getLogAMSForValidDBModeIsActive(log)));
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterSubscriptions(DEVICE_ID_NUMBER_2));
    }

}


