package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.SessionCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.AppType;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.TestUtils.isEmptyObject;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAppExitTimeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAppSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAppTypeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertAvnClientSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMacEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSequenceNumberEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSessionEventReasonEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertStbSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputTimeDeltaSum;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.toLocalDateTime;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_FOUR;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ONE;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_3;

/**
 * The session CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class SessionCsvFileAssertion extends AbstractCsvFileAssertion<SessionCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public SessionCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<SessionCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        int eventsCount = calculateInputEventsCount(input);

        assertEntriesCountEquals(eventsCount, csvFileEntries.size());

        ListIterator<SessionCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputMessageType = input.getMessageType();

        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();

            int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

            int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

            int sequenceNumberCounter = 1;

            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                SessionCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();

                int inputEventId = eventData.getEventId();

                List<Object> inputParameters = eventData.getParameters();
                Integer inputParameterAppType = (Integer) inputParameters.get(0);
                Integer inputParameterSessionId = (Integer) inputParameters.get(1);
                Integer inputParameterReason = (Integer) inputParameters.get(2);
                Integer inputParameterClientSessionId = null;
                if (inputEventId == EVENT_ID_FOUR) {
                    inputParameterClientSessionId = (Integer) inputParameters.get(3);
                }

                int inputTimestampPartMsec = eventData.getTimestampPartMsec();

                String macId = csvFileEntry.getMacId();
                Long mac = DOBUtil.string2mac(macId);
                AppType appType = csvFileEntry.getAppType();
                String appSessionId = csvFileEntry.getAppSessionId();
                LocalDateTime appExitTime = csvFileEntry.getAppExitTime();
                SessionEventReason reason = csvFileEntry.getReason();
                String stbSessionId = csvFileEntry.getStbSessionId();
                Integer sequenceNumber = csvFileEntry.getSequenceNumber();
                Integer avnClientSessionId = csvFileEntry.getAvnClientSessionId();

                // mac assertion
                // MAC address from Zodiac Transport header
                assertMacEquals(mac, inputMac);
                // end of mac assertion

                // appType assertion
                assertAppTypeEquals(appType, AppType.of(inputParameterAppType));
                // end of appType assertion

                // appSessionId assertion
                // "Z" + "_" + <STB MAC> + "_" + <current event 'sessionId'>
                String inputAppSessionId = "Z" + "_" + inputMacId + "_" + inputParameterSessionId;
                assertAppSessionIdEquals(appSessionId, inputAppSessionId);
                // end of appSessionId assertion

                // appExitTime assertion
                // if 'messageType' < 3 -> <Session start timestamp> + sum of respective <Time delta>s, in yyyy-MM-DDThh:mm:ss format
                // if 'messageType' >= 3 -> <Session start timestamp> + sum of respective <Time delta>s + <timestampPartMsec>msec, in yyyy-MM-DDThh:mm:ss.xxx format
                long inputAppExitTimeMsec;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputAppExitTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
                } else {
                    inputAppExitTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                            + inputTimestampPartMsec;
                }
                LocalDateTime inputAppExitTime = toLocalDateTime(inputAppExitTimeMsec);
                assertAppExitTimeEquals(appExitTime, inputAppExitTime);
                // end of appExitTime assertion

                // reason assertion
                assertSessionEventReasonEquals(reason, SessionEventReason.of(inputParameterReason));
                // end of reason assertion

                // stbSessionId assertion
                // the same as <STB Session ID> field in TUNING.CSV
                String inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
                assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
                // end of stbSessionId assertion

                // sequenceNumber assertion
                // the same as <Sequence Number> field in TUNING.CSV
                Integer inputSequenceNumber = sequenceNumberCounter;
                assertSequenceNumberEquals(sequenceNumber, inputSequenceNumber);
                // end of sequenceNumber assertion

                // avnClientSessionId assertion
                // if 'event ID' == 1 -> empty
                // if 'event ID' == 4:
                //      if current event 'clientSessionId' == 0 -> empty
                //      otherwise -> current event 'clientSessionId' value
                Integer inputAvnClientSessionId;
                if (inputEventId == EVENT_ID_ONE) {
                    inputAvnClientSessionId = null;
                } else if (inputEventId == EVENT_ID_FOUR) {
                    if (isEmptyObject(inputParameterClientSessionId)) {
                        inputAvnClientSessionId = null;
                    } else {
                        inputAvnClientSessionId = inputParameterClientSessionId;
                    }
                } else {// WTF?
                    throw new IllegalStateException("Unsupported input eventId of " + inputEventId);
                }
                assertAvnClientSessionIdEquals(avnClientSessionId, inputAvnClientSessionId);
                // end of avnClientSessionId assertion

                sequenceNumberCounter++;
            }
        }
    }
}
