package com.zodiac.ams.charter.tests.ft.talkingguide;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.FeatureList.TALKING_GUIDE;

public class TalkingGuideTestNoConfig extends TalkingGuideTestImpl{
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties,~ServerXMLHelper.TALKING_GUIDE_MASK);
        String remote = executor.getHomeDir()+"/config_file_doesnt_exist.cfg";
        createRemoteCacheDir();
        helper.addTalkingGuideService(properties.getLocalHost(),properties.getTTSPort(),remote,getRemoteCacheDir());
        helper.save("server.xml");
    }  
    
    
    @Features(TALKING_GUIDE)
    @Stories("Positive")
    @Description("Send request to AMS to convert text with macroses. Configuration file doesn't exist")
    @Test(priority=1)  
    public void simpleTextWithMacrosNoConf() throws Exception{
        positiveTestImpl("{tts1} {tts2} {tts3} "+System.currentTimeMillis(),getName("audio.wav"),null);
    }
    
    @Features(TALKING_GUIDE)
    @Stories("AMS logs")
    @Description("TalkingGuideTestNoConfig AMS log")
    @Test(priority=2)
    public void getTalkingGuideTestNoConfigAMSLog() throws Exception {
        stopAndgetAMSLog(true);
    }
    
}
