package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.zodiac.ams.charter.csv.file.entry.DvrCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventOperationResult;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventType;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountOf1Equals;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;

/**
 * The DVR CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DvrCsvFileAssertion extends AbstractCsvFileAssertion<DvrCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public DvrCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<DvrCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        int eventsCount = calculateInputEventsCount(input);

        assertEntriesCountEquals(eventsCount, csvFileEntries.size());

        ListIterator<DvrCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

//        Long inputMac = input.getMac();
//        //String inputMacId = DOBUtil.mac2string(inputMac);
//
//        //int inputMessageType = input.getMessageType();

        DvrCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();
        String macId = csvFileEntry.getMacId();
        String stbSessionId = csvFileEntry.getStbSessionId();
        Integer sequenceNumber = csvFileEntry.getSequenceNumber();
        LocalDateTime eventTime = csvFileEntry.getEventTime();
        DvrEventType eventType = csvFileEntry.getEventType();
        DvrEventReason reason = csvFileEntry.getReason();
        DvrEventOperationResult operationResult = csvFileEntry.getOperationResult();
        String recordingId = csvFileEntry.getRecordingId();
        String tmsProgramId = csvFileEntry.getTmsProgramId();
        String tmsChannelId = csvFileEntry.getTmsChannelId();
        Integer channelNumber = csvFileEntry.getChannelNumber();
        LocalDateTime scheduledStartTime = csvFileEntry.getScheduledStartTime();
        LocalDateTime scheduledEndTime = csvFileEntry.getScheduledEndTime();
        LocalDateTime actualStartTime = csvFileEntry.getActualStartTime();
        LocalDateTime actualEndTime = csvFileEntry.getActualEndTime();
        Integer internalHddFreeSpacePercent = csvFileEntry.getInternalHddFreeSpacePercent();
        Integer externalHddFreeSpacePercent = csvFileEntry.getExternalHddFreeSpacePercent();
        Integer watchedVideoCurrentPosition = csvFileEntry.getWatchedVideoCurrentPosition();

        // TODO fix later (add assertions)
    }
}
