package com.zodiac.ams.charter.tests.lookup;

import com.zodiac.ams.charter.emuls.che.lookup.LookupCHEEmulator;
import com.zodiac.ams.charter.http.helpers.lookup.LookupJsonDataUtils;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class StepsForLookup extends Steps {

    protected LookupCHEEmulator lookupCHEEmulator;
    protected LookupJsonDataUtils lookupJsonDataUtils = new LookupJsonDataUtils();

    @Step
    public List<String> readAMSLog() {
        return parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT);
    }

    @Step
    protected void runCHELookupWithCode200() {
        lookupCHEEmulator = new LookupCHEEmulator("200");
        lookupCHEEmulator.registerConsumer();
        lookupCHEEmulator.start();
        Logger.info("CHE Emulator's run");
    }

    @Step
    protected void stopCHELookup() {
        lookupCHEEmulator.stop();
        Logger.info("CHE Emulator's stopped");
    }

    @Step
    protected void getTestTime(){
        startTestTime = getCurrentTimeTest();
    }

}
