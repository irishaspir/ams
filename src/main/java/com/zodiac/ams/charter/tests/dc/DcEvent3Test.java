package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_THREE;

/**
 * The tests suite for DC event 3.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcEvent3Test extends StepsForDcEvent {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_POSITIVE + "1 - Check positive reason")
    @Description("4.3.1 [DC][Event 3] Check positive reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139 | 1 | reason | 1>}], where reason is from 1 to 149 ")
    public void sendMessageWithPositiveReason() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_THREE;
        // parameters
        final int parameterChannel = 10139;
        final int parameterType = 1;
        final int parameterReason = randomParameterReasonForEvent3;
        final int parameterScreenMode = 1;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTsbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_POSITIVE + "2 - Check positive screenMode")
    @Description("4.3.2 [DC][Event 3] Check positive screenMode" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139 | 2 | 1 | screenMode>}], where screenMode is 1 or 2")
    public void sendMessageWithPositiveScreenMode() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_THREE;
        // parameters
        final int parameterChannel = 10139;
        final int parameterType = 2;
        final int parameterReason = 1;

        final int parameterScreenMode = randomParameterScreenMode;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTsbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_POSITIVE + "3 - Check positive type")
    @Description("4.3.3 [DC][Event 3] Check positive type" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139 | type | 1 | 1>}], where type is from 1 to 7")
    public void sendMessageWithPositiveScreenType() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_THREE;
        // parameters
        final int parameterChannel = 10139;
        // TODO fix later (uncomment random parameter)
        final int parameterType = /*randomParameterType*/4;
        final int parameterReason = 1;
        final int parameterScreenMode = 1;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTsbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_POSITIVE + "4 - Check TSB.csv where reason and screen mode =0")
    @Description("4.3.4 [DC][Event 3] Check TSB.csv where reason and screen mode =0" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139 | 1 | 0 | 0>}]")
    public void sendMessageCheckTSBcsvWhereReasonAndScreenmodeAreZero() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_THREE;
        // parameters
        final int parameterChannel = 10139;
        final int parameterType = 1;
        final int parameterReason = 0;
        final int parameterScreenMode = 0;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterChannel)
                .addParameter(parameterType)
                .addParameter(parameterReason)
                .addParameter(parameterScreenMode)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteTsbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "5 - Check negative channel")
    @Description("4.3.5 [DC][Event 3] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><3252345|3|3|1>}] ")
    public void sendMessageWithIncorrectChannel1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                // channel must be incorrect, this is the test purpose!
                .addParameter(3252345) // channel
                .addParameter(3) // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "5 - Check negative channel")
    @Description("4.3.5 [DC][Event 3] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><dfgsdfg|3|3|1>}]")
    public void sendMessageWithIncorrectChannel2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                // channel must be garbage, this is the test purpose!
                .addParameter("dfgsdfg") // channel
                .addParameter(3) // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "5 - Check negative channel")
    @Description("4.3.5 [DC][Event 3] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><#@$%@#$%|3|3|1>}] ")
    public void sendMessageWithIncorrectChannel3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                // channel must be garbage, this is the test purpose!
                .addParameter("#@$%@#$%") // channel
                .addParameter(3) // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "5 - Check negative channel")
    @Description("4.3.5 [DC][Event 3] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><        |3|3|1>}]")
    public void sendMessageWithIncorrectChannel4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                // channel must be garbage, this is the test purpose!
                .addParameter("        ") // channel
                .addParameter(3) // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "6 - Check negative reason")
    @Description("4.3.6 [DC][Event 3] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139 | 1 | 45645456 | 1>}]")
    public void sendMessageWithIncorrectReason1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(1) // type
                // reason must be garbage, this is the test purpose!
                .addParameter(45645456) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "6 - Check negative reason")
    @Description("4.3.6 [DC][Event 3] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|1|rewert|1>}]")
    public void sendMessageWithIncorrectReason2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(1) // type
                // reason must be garbage, this is the test purpose!
                .addParameter("rewert") // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "6 - Check negative reason")
    @Description("4.3.6 [DC][Event 3] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|1|#$%@#$%|1>}] ")
    public void sendMessageWithIncorrectReason3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(1) // type
                // reason must be garbage, this is the test purpose!
                .addParameter("#$%@#$%") // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "6 - Check negative reason")
    @Description("4.3.6 [DC][Event 3] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|1|         |1>}]")
    public void sendMessageWithIncorrectReason4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(1) // type
                // reason must be garbage, this is the test purpose!
                .addParameter("         ") // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "7 - Check negative screenMode")
    @Description("4.3.7 [DC][Event 3] Check negative screenMode" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139 | 4 | 3 | 345645>}] ")
    public void sendMessageWithIncorrectScreenMode1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(4) // type
                .addParameter(3) // reason
                // screenMode must be garbage, this is the test purpose!
                .addParameter(345645) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "7 - Check negative screenMode")
    @Description("4.3.7 [DC][Event 3] Check negative screenMode" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|4|3|dfgdfg>}] ")
    public void sendMessageWithIncorrectScreenMode2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(4) // type
                .addParameter(3) // reason
                // screenMode must be garbage, this is the test purpose!
                .addParameter("dfgdfg") // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "7 - Check negative screenMode")
    @Description("4.3.7 [DC][Event 3] Check negative screenMode" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|4|3|$%^#$%^>}] ")
    public void sendMessageWithIncorrectScreenMode3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(4) // type
                .addParameter(3) // reason
                // screenMode must be garbage, this is the test purpose!
                .addParameter("$%^#$%^") // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "7 - Check negative screenMode")
    @Description("4.3.7 [DC][Event 3] Check negative screenMode" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|4|3|        >}] ")
    public void sendMessageWithIncorrectScreenMode4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                .addParameter(4) // type
                .addParameter(3) // reason
                // screenMode must be garbage, this is the test purpose!
                .addParameter("        ") // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "8 - Check negative type")
    @Description("4.3.8 [DC][Event 3] Check negative type" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|765761|3|1>}]")
    public void sendMessageWithIncorrectType1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                // type must be garbage, this is the test purpose!
                .addParameter(765761) // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "8 - Check negative type")
    @Description("4.3.8 [DC][Event 3] Check negative type" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|tdytytt|3|1>}]")
    public void sendMessageWithIncorrectType2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                // type must be garbage, this is the test purpose!
                .addParameter("tdytytt") // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "8 - Check negative type")
    @Description("4.3.8 [DC][Event 3] Check negative type" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|&^%$#|3|1>}]")
    public void sendMessageWithIncorrectType3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                // type must be garbage, this is the test purpose!
                .addParameter("&^%$#") // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><channel | type | reason | screenMode>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_3_NEGATIVE + "8 - Check negative type")
    @Description("4.3.8 [DC][Event 3] Check negative type" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10139|         |3|1>}]")
    public void sendMessageWithIncorrectType4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_THREE)
                .addParameter(10139) // channel
                // type must be garbage, this is the test purpose!
                .addParameter("         ") // type
                .addParameter(3) // reason
                .addParameter(1) // screenMode
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
