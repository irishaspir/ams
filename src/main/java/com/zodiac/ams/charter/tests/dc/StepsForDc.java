package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.csv.file.entry.ActivityCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.DvrCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.HddCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.SessionCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.StbCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.TsbCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.TunerUseCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.TuningCsvFileEntry;
import com.zodiac.ams.charter.csv.file.parser.ActivityCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.DvrCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.HddCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.SessionCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.StbCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.TsbCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.TunerUseCsvFileParser;
import com.zodiac.ams.charter.csv.file.parser.TuningCsvFileParser;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.emuls.stb.dc.DcStbEmulator;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;

import com.zodiac.ams.charter.rudp.dc.LocalRudpHelper;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.rudp.dc.message.LocalResponseMessageParser;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.ActivityCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.DvrCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.HddCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.SessionCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.StbCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.TsbCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.TuningCsvFileAssertion;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.TunerUseCsvFileAssertion;
import com.zodiac.ams.common.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.zodiac.ams.charter.tests.dc.LocalFileUtils.*;
import static com.zodiac.ams.charter.tests.dc.TestUtils.delayInSeconds;
import static com.zodiac.ams.charter.tests.dc.TestUtils.waitForRemote;
import static com.zodiac.ams.charter.tests.dc.TestUtils.nextRandomInt;
import static com.zodiac.ams.charter.tests.dc.TestUtils.parseMacsString;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_4;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import com.zodiac.ams.common.ssh.SSHExecutor;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import static org.testng.Assert.assertTrue;

/**
 * The abstract implementation of steps for DC.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public abstract class StepsForDc extends Steps {
    private static final String MESSAGE_ID = "10012";
    private static final String SENDER = "am";
    private static final String ADDRESSEE = "am";

    protected static final int HUB_ID = 1;
    protected static final int NODE_ID = 1;
    protected static final int TIME_DELTA = 0;

    protected static final Integer PARAMETER_APP_MIN = 0;
    protected static final Integer PARAMETER_APP_MAX = 14;

    protected static final Integer PARAMETER_APP_MIN_FOR_EVENT4 = 0;
    protected static final Integer PARAMETER_APP_MAX_FOR_EVENT4 = 13;

    protected static final Integer PARAMETER_REASON_MIN_FOR_EVENT1 = 1;
    protected static final Integer PARAMETER_REASON_MAX_FOR_EVENT1 = 229;

    protected static final Integer PARAMETER_REASON_MIN_FOR_EVENT2 = 1;
    protected static final Integer PARAMETER_REASON_MAX_FOR_EVENT2 = 207;

    protected static final Integer PARAMETER_REASON_MIN_FOR_EVENT3 = 1;
    protected static final Integer PARAMETER_REASON_MAX_FOR_EVENT3 = 149;

    protected static final Integer PARAMETER_REASON_MIN_FOR_EVENT4 = 1;
    protected static final Integer PARAMETER_REASON_MAX_FOR_EVENT4 = 229;

    protected static final Integer PARAMETER_REASON_MIN_FOR_EVENT5 = 1;
    protected static final Integer PARAMETER_REASON_MAX_FOR_EVENT5 = 229;

    protected static final Integer PARAMETER_REASON_MIN_FOR_EVENT12 = 1;
    protected static final Integer PARAMETER_REASON_MAX_FOR_EVENT12 = 207;

    protected static final Integer PARAMETER_SCREEN_MODE_MIN = 1;
    protected static final Integer PARAMETER_SCREEN_MODE_MAX = 2;

    protected static final Integer PARAMETER_SESSION_ACTIVITY_ID_MIN_FOR_EVENT11 = 1;
    protected static final Integer PARAMETER_SESSION_ACTIVITY_ID_MAX_FOR_EVENT11 = 40;

    protected static final Integer PARAMETER_SESSION_ACTIVITY_ID_MIN_FOR_EVENT12 = 1;
    protected static final Integer PARAMETER_SESSION_ACTIVITY_ID_MAX_FOR_EVENT12 = 40;

    protected static final Integer PARAMETER_TYPE_MIN = 1;
    protected static final Integer PARAMETER_TYPE_MAX = 7;

    private static final String ASSERTION_MESSAGE_CORRECT_FORMAT_PREFIX = "The correct format assertion on" +
            " response message is failed for the following message: ";
    private static final String ASSERTION_MESSAGE_INCORRECT_FORMAT_PREFIX = "The incorrect format assertion on" +
            " response message is failed for the following message: ";

    private static final int DELAY_IN_SECONDS = 60;

    protected STBEmulator stbEmulator;
    protected LocalRudpHelper localRudpHelper;

    protected List<Long> macs;
    protected Long mac;

    private Map<InetAddress, List<Long>> macIpMap;

    protected String pathAmsAmFiles;

    protected Random random;

    protected int randomParameterApp;
    protected int randomParameterAppForEvent4;
    protected int randomParameterReasonForEvent1;
    protected int randomParameterReasonForEvent2;
    protected int randomParameterReasonForEvent3;
    protected int randomParameterReasonForEvent4;
    protected int randomParameterReasonForEvent5;
    protected int randomParameterReasonForEvent12;
    protected int randomParameterScreenMode;
    protected int randomParameterSessionActivityIdForEvent11;
    protected int randomParameterSessionActivityIdForEvent12;
    protected int randomParameterType;
    
    protected SSHHelper amsExecutor;
    protected SSHHelper sftpExecutor;
    
            
    protected boolean needAmsRestart(){
        return false;
    }
    
    @BeforeClass
    protected void startup() {
        
        FTConfig cfg = FTConfig.getInstance();
        String outDir = cfg.getDCOutDir();
        String host = cfg.getDCSftpHost();
        String login = cfg.getDCSftpLogin();
        String pwd = cfg.getDCSftpPwd();
        String remoteDir = cfg.getDCSftpRemoteDir();
        amsExecutor = cfg.getSSHExecutor();
        sftpExecutor = new SSHHelper(host, login, pwd);
        
        String template = "sudo rm -rf %s ; sudo mkdir -p %s ; sudo chmod -R a=rwx %s ;";
        String command = String.format(template, outDir, outDir + "/uploaded", outDir);
        assertTrue(amsExecutor.exec(command), "Unable to execute: " + command);
        
        command = String.format(template, remoteDir, remoteDir, remoteDir);
        assertTrue(sftpExecutor.exec(command), "Unable to execute: " + command);
        
        if (needAmsRestart()){
            ServerXMLHelper helper=new ServerXMLHelper(cfg);
            helper.addDCService(outDir, host, login, pwd, remoteDir);
            helper.save("server.xml");
        
            FuncTest.restartAMS();
        }
        startStbEmulator();

        localRudpHelper = new LocalRudpHelper(stbEmulator);

//        //String validMac = config.getValidMac();
//        String validMacSet = config.getValidMacSet();
//        if (StringUtils.isBlank(validMacSet)) {
//            throw new IllegalArgumentException("The macs string can't be null or empty/blank");
//        }
//        macs = parseMacsString(validMacSet);
//        mac = macs.get(0);
        String validMacNumber = config.getValidMacNumber1();
        if (StringUtils.isBlank(validMacNumber)) {
            throw new IllegalArgumentException("The macs string can't be null or empty/blank");
        }
        macs = parseMacsString(validMacNumber);
        mac = macs.get(0);

        // WTF?
        File macIpMappingFile = config.getMacIpMappingFile();
        if (macIpMappingFile != null) {
            macIpMap = readMacIpMappingFile(macIpMappingFile);
        }

        // TODO fix later
        //pathAmsAmFiles = config.getAmsAmFilesPath();
        //pathAmsAmFiles = "/srv/AMS/AMFiles/";

        pathAmsAmFiles = remoteDir;
        random = new Random();
    }




    @AfterClass
    protected void shutdown() {
        stopStbEmulator();
    }

    
    protected void forceFileClear(){
        long tm = System.currentTimeMillis();
        while (true) {
            sftpExecutor.exec("sudo rm " + pathAmsAmFiles + "/*");
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(LocalFileUtils.TEMP_DIR), "*.csv")) {
                for (Path entry: stream) 
                    Files.delete(entry);
            } 
            catch (DirectoryIteratorException|IOException ex) {
            }
            try{
                Thread.sleep(200);
            }
            catch(InterruptedException ex){
            }
            if (sftpExecutor.getLineCounts(pathAmsAmFiles) <= 1)
                return;
            if (System.currentTimeMillis() -tm > DELAY_IN_SECONDS * 1000)
                return ;        
        }
    }

    @BeforeMethod
    protected void startupMethod() {
        forceFileClear();
        startTestTime = getCurrentTimeTest();

        randomParameterApp = nextRandomInt(random, PARAMETER_APP_MIN, PARAMETER_APP_MAX);
        randomParameterAppForEvent4 = nextRandomInt(random, PARAMETER_APP_MIN_FOR_EVENT4, PARAMETER_APP_MAX_FOR_EVENT4);

        // need to exclude 5 and 12 regarding to documentation, may be can be done better ...
        if (randomParameterAppForEvent4 == 5 | randomParameterAppForEvent4 == 12) {
            randomParameterAppForEvent4++;
        }
        randomParameterReasonForEvent1 = nextRandomInt(random, PARAMETER_REASON_MIN_FOR_EVENT1, PARAMETER_REASON_MAX_FOR_EVENT1);
        randomParameterReasonForEvent2 = nextRandomInt(random, PARAMETER_REASON_MIN_FOR_EVENT2, PARAMETER_REASON_MAX_FOR_EVENT2);
        randomParameterReasonForEvent3 = nextRandomInt(random, PARAMETER_REASON_MIN_FOR_EVENT3, PARAMETER_REASON_MAX_FOR_EVENT3);
        randomParameterReasonForEvent4 = nextRandomInt(random, PARAMETER_REASON_MIN_FOR_EVENT4, PARAMETER_REASON_MAX_FOR_EVENT4);
        randomParameterReasonForEvent5 = nextRandomInt(random, PARAMETER_REASON_MIN_FOR_EVENT5, PARAMETER_REASON_MAX_FOR_EVENT5);
        randomParameterReasonForEvent12 = nextRandomInt(random, PARAMETER_REASON_MIN_FOR_EVENT12, PARAMETER_REASON_MAX_FOR_EVENT12);
        randomParameterScreenMode = nextRandomInt(random, PARAMETER_SCREEN_MODE_MIN, PARAMETER_SCREEN_MODE_MAX);
        randomParameterSessionActivityIdForEvent11 = nextRandomInt(random, PARAMETER_SESSION_ACTIVITY_ID_MIN_FOR_EVENT11, PARAMETER_SESSION_ACTIVITY_ID_MAX_FOR_EVENT11);
        randomParameterSessionActivityIdForEvent12 = nextRandomInt(random, PARAMETER_SESSION_ACTIVITY_ID_MIN_FOR_EVENT12, PARAMETER_SESSION_ACTIVITY_ID_MAX_FOR_EVENT12);
        randomParameterType = nextRandomInt(random, PARAMETER_TYPE_MIN, PARAMETER_TYPE_MAX);
    }

    @Step("The step to start STB emulator")
    protected void startStbEmulator() {
        stbEmulator = new DcStbEmulator();
        stbEmulator.registerConsumer();
        stbEmulator.start();

        Logger.info("STB Emulator started (without timeout)");
    }

    @Step("The step to stop STB emulator")
    protected void stopStbEmulator() {
        stbEmulator.stop();

        Logger.info("STB emulator stopped");
    }

    protected IZodiacMessage sendMessage(ZodiacMessage message) {
        return localRudpHelper.sendMessage(message);
    }

    protected IZodiacMessage sendMessageAndAttachLog(ZodiacMessage message) {
        IZodiacMessage responseMessage = sendMessage(message);
        if (FTConfig.getInstance().isDCDocker())
            createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        return responseMessage;
    }

    protected void assertResponseCorrectFormat(ZodiacMessage message, IZodiacMessage responseMessage) {
        LocalResponseMessageParser responseMessageParser = new LocalResponseMessageParser(responseMessage);
        assertTrue(responseMessageParser.isCorrectFormat(), ASSERTION_MESSAGE_CORRECT_FORMAT_PREFIX + message);
    }

    protected void assertResponseIncorrectFormat(ZodiacMessage message, IZodiacMessage responseMessage) {
        LocalResponseMessageParser responseMessageParser = new LocalResponseMessageParser(responseMessage);
        assertTrue(responseMessageParser.isIncorrectFormat(), ASSERTION_MESSAGE_INCORRECT_FORMAT_PREFIX + message);
    }

    protected void sendMessageAndAssertResponseCorrectFormat(ZodiacMessage message) {
        IZodiacMessage responseMessage = sendMessageAndAttachLog(message);
        assertResponseCorrectFormat(message, responseMessage);
    }

    protected void sendMessageAndAssertResponseIncorrectFormat(ZodiacMessage message) {
        IZodiacMessage responseMessage = sendMessageAndAttachLog(message);
        assertResponseIncorrectFormat(message, responseMessage);
    }

    /*
     * Methods to parse CSV files.
     */

    protected List<ActivityCsvFileEntry> parseActivityCsvFile(String filename) throws IOException {
        ActivityCsvFileParser csvFileParser = new ActivityCsvFileParser(filename);
        return csvFileParser.parse();
    }

    protected List<DvrCsvFileEntry> parseDvrCsvFile(String filename) throws IOException {
        DvrCsvFileParser csvFileParser = new DvrCsvFileParser(filename);
        return csvFileParser.parse();
    }

    protected List<HddCsvFileEntry> parseHddCsvFile(String filename) throws IOException {
        HddCsvFileParser csvFileParser = new HddCsvFileParser(filename);
        return csvFileParser.parse();
    }

    protected List<SessionCsvFileEntry> parseSessionCsvFile(String filename) throws IOException {
        SessionCsvFileParser csvFileParser = new SessionCsvFileParser(filename);
        return csvFileParser.parse();
    }

    protected List<StbCsvFileEntry> parseStbCsvFile(String filename) throws IOException {
        StbCsvFileParser csvFileParser = new StbCsvFileParser(filename);
        return csvFileParser.parse();
    }

    protected List<TsbCsvFileEntry> parseTsbCsvFile(String filename) throws IOException {
        TsbCsvFileParser csvFileParser = new TsbCsvFileParser(filename);
        return csvFileParser.parse();
    }

    protected List<TuningCsvFileEntry> parseTuningCsvFile(String filename) throws IOException {
        TuningCsvFileParser csvFileParser = new TuningCsvFileParser(filename);
        return csvFileParser.parse();
    }
    
    protected List<TunerUseCsvFileEntry> parseTunerUseCsvFile(String filename) throws IOException {
        TunerUseCsvFileParser csvFileParser = new TunerUseCsvFileParser(filename);
        return csvFileParser.parse();
    }

    /*
     * Methods to assert CSV files.
     */

    protected void assertActivityCsvFile(CsvFileAssertionInput input, List<ActivityCsvFileEntry> csvFileEntries) {
        ActivityCsvFileAssertion csvFileAssertion = new ActivityCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    protected void assertDvrCsvFile(CsvFileAssertionInput input, List<DvrCsvFileEntry> csvFileEntries) {
        DvrCsvFileAssertion csvFileAssertion = new DvrCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    protected void assertHddCsvFile(CsvFileAssertionInput input, List<HddCsvFileEntry> csvFileEntries) {
        HddCsvFileAssertion csvFileAssertion = new HddCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    protected void assertSessionCsvFile(CsvFileAssertionInput input, List<SessionCsvFileEntry> csvFileEntries) {
        SessionCsvFileAssertion csvFileAssertion = new SessionCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    protected void assertStbCsvFile(CsvFileAssertionInput input, List<StbCsvFileEntry> csvFileEntries) {
        StbCsvFileAssertion csvFileAssertion = new StbCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    protected void assertTsbCsvFile(CsvFileAssertionInput input, List<TsbCsvFileEntry> csvFileEntries) {
        TsbCsvFileAssertion csvFileAssertion = new TsbCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    protected void assertTuningCsvFile(CsvFileAssertionInput input, List<TuningCsvFileEntry> csvFileEntries) {
        TuningCsvFileAssertion csvFileAssertion = new TuningCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }
    
    protected void assertTunerUseCsvFile(CsvFileAssertionInput input, List<TunerUseCsvFileEntry> csvFileEntries) {
        TunerUseCsvFileAssertion csvFileAssertion = new TunerUseCsvFileAssertion(input);
        csvFileAssertion.assertFile(csvFileEntries);
    }

    /*
     * Methods to parse and assert CSV files.
     */

    protected void parseAndAssertActivityCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<ActivityCsvFileEntry> csvFileEntries = parseActivityCsvFile(filename);
        assertActivityCsvFile(input, csvFileEntries);
    }

    protected void parseAndAssertDvrCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<DvrCsvFileEntry> csvFileEntries = parseDvrCsvFile(filename);
        assertDvrCsvFile(input, csvFileEntries);
    }

    protected void parseAndAssertHddCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<HddCsvFileEntry> csvFileEntries = parseHddCsvFile(filename);
        assertHddCsvFile(input, csvFileEntries);
    }

    protected void parseAndAssertSessionCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<SessionCsvFileEntry> csvFileEntries = parseSessionCsvFile(filename);
        assertSessionCsvFile(input, csvFileEntries);
    }

    protected void parseAndAssertStbCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<StbCsvFileEntry> csvFileEntries = parseStbCsvFile(filename);
        assertStbCsvFile(input, csvFileEntries);
    }

    protected void parseAndAssertTsbCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<TsbCsvFileEntry> csvFileEntries = parseTsbCsvFile(filename);
        assertTsbCsvFile(input, csvFileEntries);
    }

    protected void parseAndAssertTuningCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<TuningCsvFileEntry> csvFileEntries = parseTuningCsvFile(filename);
        assertTuningCsvFile(input, csvFileEntries);
    }
    
    protected void parseAndAssertTunerUseCsvFile(CsvFileAssertionInput input, String filename) throws IOException {
        List<TunerUseCsvFileEntry> csvFileEntries = parseTunerUseCsvFile(filename);
        assertTunerUseCsvFile(input, csvFileEntries);
    }

    protected void parseAssertAndDeleteActivityCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteActivityHddStbCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String hddFilename = getHddFilename(pathAmsAmFiles);
        deleteFilenames.add(hddFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertHddCsvFile(input, hddFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteActivityHddSessionStbTsbTuningCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String hddFilename = getHddFilename(pathAmsAmFiles);
        deleteFilenames.add(hddFilename);
        String sessionFilename = getSessionFilename(pathAmsAmFiles);
        deleteFilenames.add(sessionFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);
        String tsbFilename = getTsbFilename(pathAmsAmFiles);
        deleteFilenames.add(tsbFilename);
        String tuningFilename = getTuningFilename(pathAmsAmFiles);
        deleteFilenames.add(tuningFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertHddCsvFile(input, hddFilename);
            parseAndAssertSessionCsvFile(input, sessionFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
            parseAndAssertTsbCsvFile(input, tsbFilename);
            parseAndAssertTuningCsvFile(input, tuningFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteActivitySessionStbCsvFile(CsvFileAssertionInput input) throws IOException {

        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String sessionFilename = getSessionFilename(pathAmsAmFiles);
        deleteFilenames.add(sessionFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertSessionCsvFile(input, sessionFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteActivityStbCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteActivityStbTsbCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);
        String tsbFilename = getTsbFilename(pathAmsAmFiles);
        deleteFilenames.add(tsbFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
            parseAndAssertTsbCsvFile(input, tsbFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteActivityStbTuningCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);
        String tuningFilename = getTuningFilename(pathAmsAmFiles);
        deleteFilenames.add(tuningFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
            parseAndAssertTuningCsvFile(input, tuningFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }
    
    protected void parseAssertAndDeleteActivityStbTunerUseCsvFile(CsvFileAssertionInput input) throws IOException {
        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String activityFilename = getActivityFilename(pathAmsAmFiles);
        deleteFilenames.add(activityFilename);
        String stbFilename = getStbFilename(pathAmsAmFiles);
        deleteFilenames.add(stbFilename);
        String tunerUseFilename = getTunerUseFilename(pathAmsAmFiles);
        deleteFilenames.add(tunerUseFilename);

        try {
            parseAndAssertActivityCsvFile(input, activityFilename);
            parseAndAssertStbCsvFile(input, stbFilename);
            parseAndAssertTunerUseCsvFile(input, tunerUseFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected void parseAssertAndDeleteTsbCsvFile(CsvFileAssertionInput input) throws IOException {

        waitForRemote(pathAmsAmFiles, DELAY_IN_SECONDS);
        //delayInSeconds(DELAY_IN_SECONDS);

        List<String> deleteFilenames = new ArrayList<>();

        String tsbFilename = getTsbFilename(pathAmsAmFiles);
        deleteFilenames.add(tsbFilename);

        try {
            parseAndAssertTsbCsvFile(input, tsbFilename);
        } finally {
            deleteFiles(deleteFilenames);
        }
    }

    protected LocalMessageBuilder createLocalMessageBuilder() {
        return createLocalMessageBuilder(MESSAGE_TYPE_4);
    }

    protected LocalMessageBuilder createLocalMessageBuilder(int messageType) {
        return new LocalMessageBuilder()
                .setMacs(macs)
                .setMac(mac)
                .setMessageType(messageType)
                .setMessageId(MESSAGE_ID)
                .setSender(SENDER)
                .setAddresse(ADDRESSEE);
    }

    protected CsvFileAssertionInput.Builder createCsvFileAssertionInputBuilder() {
        return createCsvFileAssertionInputBuilder(MESSAGE_TYPE_4);
    }

    protected CsvFileAssertionInput.Builder createCsvFileAssertionInputBuilder(int messageType) {
        return new CsvFileAssertionInput.Builder()
                .setMacs(macs)
                .setMac(mac)
                .setMessageType(messageType)
                .setMessageId(MESSAGE_ID)
                .setSender(SENDER)
                .setAddresse(ADDRESSEE);
    }
}
