package com.zodiac.ams.charter.tests.ft.rudp;

enum ORDER {
    EMPTY,
    ASCENDING,
    ASCENDING_LAST_IS_FIRST,
    DESCENDING,
    DESCENDING_LAST_IS_LAST,
    RANDOM,
    RANDOM_LAST_IS_LAST,
    RANDOM_LAST_IS_FIRST;
}
