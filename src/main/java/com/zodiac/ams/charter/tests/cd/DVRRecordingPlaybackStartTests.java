package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.DVR_PLAYBACK_START_REQUEST_ID;
import static com.zodiac.ams.charter.tests.StoriesList.DVR_RECORDING_PLAYBACK_START;

/**
 * Created by SpiridonovaIM on 20.07.2017.
 */
public class DVRRecordingPlaybackStartTests extends CDBeforAfter {

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.8  Message from device to AMS where <request id>=6, recordingId and playbackOffset aren't specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(priority = 1)
    public void sendRecordingPlaybackReceiveError() {
        String deviceId = DEVICE_ID_NUMBER_1;
        cdRudpHelper.sendRequest(deviceId, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        byte[] data = cdstbEmulator.getData();
        assertLog(log, deviceId);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.1  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified " )
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingIdAndPlaybackOffset", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackRecordingIdAndOffsetIs(int recordingId, int offset) {
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId, offset);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertPlayback(DVR_PLAYBACK_START_REQUEST_ID, recordingId, offset);
    }


    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + DVR_RECORDING_PLAYBACK_START)
    @Description("10.6.1  Message from device to AMS where <request id>=6, recordingId and playbackOffset are specified " )
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "recordingId", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendRecordingPlaybackRecordingIdIs(int recordingId) {
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createPlayBackMessage(recordingId);
        cdRudpHelper.sendRequest(deviceId, message, DVR_PLAYBACK_START_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertLog(log, deviceId);
    }

}
