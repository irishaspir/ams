package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.StbCsvFileEntry;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountOf1Equals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertHubIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMacEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMaxRebootsPerDayEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertNodeIdEquals;

/**
 * The STB CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class StbCsvFileAssertion extends AbstractCsvFileAssertion<StbCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public StbCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<StbCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }
        // TODO fix later
        assertEntriesCountOf1Equals(csvFileEntries.size());

        ListIterator<StbCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        //String inputMacId = DOBUtil.mac2string(inputMac);

        //int inputMessageType = input.getMessageType();

        int inputHubId = input.getHubId();
        int inputMaxRebootsPerDay = input.getMaxRebootsPerDay();
        int inputNodeId = input.getNodeId();

        StbCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();
        String macId = csvFileEntry.getMacId();
        Long mac = DOBUtil.string2mac(macId);
        Integer nodeId = csvFileEntry.getNodeId();
        Integer hubId = csvFileEntry.getHubId();
        String channelMapId = csvFileEntry.getChannelMapId();
        Integer maxRebootsPerDay = csvFileEntry.getMaxRebootsPerDay();

        // mac assertion
        // MAC address from Zodiac Transport header
        assertMacEquals(mac, inputMac);
        // end of mac assertion

        // nodeId assertion
        // <Node ID> from AM data
        assertNodeIdEquals(nodeId, inputNodeId);
        // end of nodeId assertion

        // hubId assertion
        // <Hub ID> from AM data
        assertHubIdEquals(hubId, inputHubId);
        // end of hubId assertion

// TODO fix later
//        // channelMapId assertion
//        // Lineup ID in HubID-to-LineupID mapping basing on <Hub ID> value
//        // if STB model was not found or  HubID-to-LineupID mapping  does not contain required data -> empty
//        String inputChannelMapId = null;
//        assertChannelMapIdEquals(channelMapId, inputChannelMapId);
//        // end of channelMapId assertion

        // maxRebootsPerDay assertion
        // <Max Reboots Per Day> from AM data
        // if not found -> empty
// TODO fix later (if not found)
        assertMaxRebootsPerDayEquals(maxRebootsPerDay, inputMaxRebootsPerDay);
        // end of maxRebootsPerDay assertion
    }
}
