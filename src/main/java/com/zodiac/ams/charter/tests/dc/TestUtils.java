package com.zodiac.ams.charter.tests.dc;

import java.util.ArrayList;
import java.util.List;
import com.zodiac.ams.charter.csv.file.entry.enumtype.ScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TunerUse;
import com.zodiac.ams.charter.csv.file.entry.enumtype.YesNo;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The test utilities.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public class TestUtils {

    private final static Logger LOG = LoggerFactory.getLogger(TestUtils.class);
    
    private static final int MAX_ITERATION = 5;
    private static SSHHelper executor = null;
    static {
        FTConfig cfg = FTConfig.getInstance();
        executor = new SSHHelper(cfg.getDCSftpHost(), cfg.getDCSftpLogin(), cfg.getDCSftpPwd());
    }


    /*
     * The private constructor.
     */
    private TestUtils() {
        // do nothing here
    }
    
    public static void waitForRemote(String remoteDir, long timeoutSec){
        int oldL = 0, newL = 0;
        
        long start = System.currentTimeMillis();
        int iteration = 0;
        
        while (true){
            iteration++;
            
            if (System.currentTimeMillis() - start > timeoutSec * 1_000){
                LOG.info("waitForRemote({},{}): Timeout is exceeded", remoteDir, timeoutSec);
                return;
            }
            
            newL = executor.getLineCounts(remoteDir);
            
            if (newL > 1 && oldL == newL ){
                if (iteration > MAX_ITERATION){
                    LOG.info("waitForRemote({},{}): Iterations limit is exceeded", remoteDir, timeoutSec);
                    return;
                }
            }
            else {
                iteration = 0;
                oldL = newL;
            }
            
            try{
                Thread.sleep(1_000);
            }
            catch(InterruptedException ex){
            }
        }
    }


    public static void delayInMillis(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("The millis can't be less than zero");
        }

        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // do nothing here, just ignore the exception
        }
    }

    public static void delayInSeconds(int seconds) {
        delayInMillis(seconds * 1000L);
    }

    public static void delayInMinutes(int minutes) {
        delayInSeconds(minutes * 60);
    }

    /**
     * The method to return the next random int.
     *
     * @param random the random
     * @param min    the range minimum
     * @param max    the range maximum
     * @return the next random int
     */
    public static int nextRandomInt(Random random, int min, int max) {
        return min + random.nextInt((max - min) + 1);
    }

    /**
     * The method to check if the object is null or empty.
     *
     * @param obj the object
     * @return {@code true} if the object is null or empty {@code false} otherwise
     */
    public static boolean isEmptyObject(Object obj) {
        if (obj != null) {
            if (obj instanceof Number) {
                // WTF?
                return ((Number) obj).longValue() == 0;
            } else if (obj instanceof String) {
                return ((String) obj).length() == 0;
            }

            throw new IllegalArgumentException("The type of the object (" + obj.getClass() + ") is not supported");
        }

        return true;
    }

    /**
     * The method to parse the macs string.
     *
     * @param macsString the macs string
     * @return the list of parsed macs (as long)
     */
    public static List<Long> parseMacsString(String macsString) {
        return parseMacsString(macsString, defaultMacsStringConverter);
    }

    /**
     * The method to parse the macs string.
     *
     * @param macsString the macs string
     * @param converter  the macs string converter
     * @return the list of parsed macs (as long)
     */
    public static List<Long> parseMacsString(String macsString, MacsStringConverter converter) {
        List<Long> macs = new ArrayList<>();
        String[] macsStringEntries = macsString.trim().split("\\s*,\\s*");
        for (String macsStringEntry : macsStringEntries) {
            if (macsStringEntry.indexOf('-') != -1) {
                macs.addAll(parseMacsStringEntryRange(macsStringEntry));
            } else {
                macs.add(Long.parseLong(macsStringEntry));
            }
        }
        return macs;
    }

    private static List<Long> parseMacsStringEntryRange(String macsStringRange) {
        String[] bounds = macsStringRange.split("\\s*-\\s*");
        if (bounds.length != 2) {
            throw new IllegalArgumentException("Invalid macs string range: " + macsStringRange);
        }

        List<Long> macs = new ArrayList<>();
        long leftBound = Long.parseLong(bounds[0]);
        long rightBound = Long.parseLong(bounds[1]);
        for (long mac = leftBound; mac <= rightBound; ++mac) {
            macs.add(mac);
        }
        return macs;
    }

    /*
     * The macs string converter.
     */
    private interface MacsStringConverter {
        Long convert(String macsString);
    }

    private static final MacsStringConverter defaultMacsStringConverter = new MacsStringConverter() {
        @Override
        public Long convert(String macsString) {
            return Long.valueOf(macsString, 16);
        }
    };
    
    
    // Tuner ID:  0..3 bits 
    // Tuner Use: 4..7 bits 
    // Screen Mode 8..9 bits 
    // TSB Recording 10 bit
    // DVR Recording 11 bit 
    // DLNA output 12 bit 
    public static short getTunerUse(int tunerId, TunerUse tunerUse, ScreenMode screenMode, YesNo tsb, YesNo dvr, YesNo dlna){
        return (short)( 0xFFFF &
            ((tunerId & 15)| 
            ((tunerUse.getIntValue() & 15) << 4) |
            ((screenMode.getIntValue() & 3) << 8) |
            ((tsb.getIntValue() & 1) << 10) |    
            ((dvr.getIntValue() & 1) << 11) |    
            ((dlna.getIntValue() & 1) << 12))
        );
    }
    
    /**
     * (is.readByte() & 255) << 8 | (is.readByte() & 255);
     */
}
