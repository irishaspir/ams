package com.zodiac.ams.charter.tests.rollback.rollbackToRoviFirmware;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.rollback.StepsForRollback;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.rollback.RollbackPostHelper.*;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendRollbackToRoviCommandSTBIsUnavailableTest extends StepsForRollback {

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.2 Send \"Rollback To Rovi Firmware\" command, STB is not available ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWhenSTBIsUnavailable() {
        getTestTime();
        assertEquals(sendRollbackToRoviFirmware(DEVICE_ID_NUMBER_1), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getResponseIfSTBIsUnavailable(DEVICE_ID_NUMBER_1));
    }
}
