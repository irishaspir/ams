package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.TuningCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TuningEventReason;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertDestinationTmsIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEventStartTimeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMacEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSequenceNumberEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSignalLevelEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSourceTmsIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertStbSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTuningEventReasonEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputTimeDeltaSum;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.toLocalDateTime;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_2;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_3;

/**
 * The tuning CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class TuningCsvFileAssertion extends AbstractCsvFileAssertion<TuningCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public TuningCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<TuningCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        int eventsCount = calculateInputEventsCount(input);

        assertEntriesCountEquals(eventsCount, csvFileEntries.size());

        ListIterator<TuningCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputMessageType = input.getMessageType();

        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();

            int inputFirstEventNumber = sessionData.getFirstEventNumber();

            int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

            int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

            Integer previousInputParameterDestinationChannelNumber = null;

            int sequenceNumberCounter = 1;

            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                TuningCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();

                List<Object> inputParameters = eventData.getParameters();
                Integer inputParameterDestinationChannelNumber = (Integer) inputParameters.get(0);
                Integer inputParameterReason = (Integer) inputParameters.get(1);
                Integer inputParameterSignalLevel = (Integer) inputParameters.get(2);

                int inputTimestampPartMsec = eventData.getTimestampPartMsec();

                String macId = csvFileEntry.getMacId();
                Long mac = DOBUtil.string2mac(macId);
                String stbSessionId = csvFileEntry.getStbSessionId();
                Integer sourceTmsId = csvFileEntry.getSourceTmsId();
                Integer destinationTmsId = csvFileEntry.getDestinationTmsId();
                LocalDateTime eventStartTime = csvFileEntry.getEventStartTime();
                TuningEventReason reason = csvFileEntry.getReason();
                Integer signalLevel = csvFileEntry.getSignalLevel();
                Integer sequenceNumber = csvFileEntry.getSequenceNumber();

                // mac assertion
                assertMacEquals(mac, inputMac);
                // end of mac assertion

                // stbSessionId assertion
                // "Z" + "_" + <STB MAC> + "_" + <Session start timestamp>
                String inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
                assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
                // end of stbSessionId assertion

                // sourceTmsId assertion
                // Previous event 'destinationChannelNumber'
                // if not available, <First channel number> is used
// TODO fix later (if not available)
                Integer inputSourceTmsId;
                if (previousInputParameterDestinationChannelNumber != null) {
                    inputSourceTmsId = previousInputParameterDestinationChannelNumber;
                } else {
                    inputSourceTmsId = inputFirstEventNumber;
                }
                assertSourceTmsIdEquals(sourceTmsId, inputSourceTmsId);
                // end of sourceTmsId assertion

                // destinationTmsId assertion
                // Current event 'destinationChannelNumber'
                Integer inputDestinationTmsId = inputParameterDestinationChannelNumber;
                assertDestinationTmsIdEquals(destinationTmsId, inputDestinationTmsId);
                // end of destinationTmsId assertion

                // eventStartTime assertion
                // if 'messageType' < 3 -> <Session start timestamp> + sum of respective <Time delta>s, in yyyy-MM-DDThh:mm:ss format
                // if 'messageType' >= 3 -> <Session start timestamp> + sum of respective <Time delta>s + <timestampPartMsec>msec, in yyyy-MM-DDThh:mm:ss.xxx format
                long inputEventStartTimeMsec;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
                } else {
                    inputEventStartTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                            + inputTimestampPartMsec;
                }
                LocalDateTime inputEventStartTime = toLocalDateTime(inputEventStartTimeMsec);
                assertEventStartTimeEquals(eventStartTime, inputEventStartTime);
                // end of eventStartTime assertion

                // reason assertion
                assertTuningEventReasonEquals(reason, TuningEventReason.of(inputParameterReason));
                // end of reason assertion

                // signalLevel assertion
                // if 'messageType' < 2 -> empty
                // if 'messageType' >= 2 -> Current event 'signalLevel'
                Integer inputSignalLevel = null;
                if (inputMessageType < MESSAGE_TYPE_2) {
                    inputSignalLevel = null;
                } else {
                    inputSignalLevel = inputParameterSignalLevel;
                }
                assertSignalLevelEquals(signalLevel, inputSignalLevel);
                // end of signalLevel assertion

                // sequenceNumber assertion
                // if 'messageType' < 3 -> empty
                // if 'messageType' >= 3 -> <First event number> + sequence number of the event in the session, starting at 0
                Integer inputSequenceNumber = null;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputSequenceNumber = null;
                } else {
                    inputSequenceNumber = inputFirstEventNumber + (sequenceNumberCounter - 1);
                }
                assertSequenceNumberEquals(sequenceNumber, inputSequenceNumber);
                // end of sequenceNumber assertion

                previousInputParameterDestinationChannelNumber = inputParameterDestinationChannelNumber;

                sequenceNumberCounter++;
            }
        }
    }
}
