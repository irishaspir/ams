package com.zodiac.ams.charter.tests.callerId.notification;

import org.apache.commons.lang.RandomStringUtils;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.services.callerId.CallerIdOptions.*;

public class StepsForCallerIdIncomingCall extends StepsForCallerIdNotification {

    protected String CALLER_NAME_MANY_CHARACTERS = RandomStringUtils.randomAlphabetic(256);
    protected String PHONE_NUMBER_MANY_CHARACTERS = RandomStringUtils.randomAlphabetic(256);

    @Step
    protected List<String> createParametersList(String callerName, String phoneNumber, String callID){
        parameters = new ArrayList<>();
        parameters.add(callerName);
        parameters.add(phoneNumber);
        parameters.add(callID);
        return parameters;
    }

    @Step
    protected List<String> createValidParametersList(){
       return createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, VALID_CALL_ID);
    }
}
