package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.PublishTask;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselCDN;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselDNCS;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSFTP;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSTFactory;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsCDN;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsHTTP;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsST;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import com.zodiac.ams.common.ssh.SSHHelper;

public class PublisherTestAllErr extends PublisherTestImpl {
    
    private CarouselHostsST hostsST;
    private CarouselHostsHTTP hostsHTTP;
    private CarouselHostsDNCS hostsDNCS;
    private CarouselHostsCDN hostsCDN;
    private ICarousel root;
    
    public PublisherTestAllErr() {
        this.isZipped = true;
        this.aLocEnabled = true;
        this.dalinc = false;
        this.carouselType = CarouselType.BATCH_2;
    }
    
    void initExecutors(){
        hostsST = new CarouselHostsST(properties, new CarouselType[] {carouselType, CarouselType.ST}, isZipped, aLocEnabled, dalinc);
        hostsHTTP = new CarouselHostsHTTP(properties, new CarouselType[] {carouselType, CarouselType.HTTP}, isZipped, aLocEnabled, dalinc);
        hostsDNCS = new CarouselHostsDNCS(properties, new CarouselType[] {carouselType, CarouselType.DNCS}, isZipped, aLocEnabled, dalinc);
        hostsCDN = new CarouselHostsCDN(properties, new CarouselType[] {carouselType, CarouselType.CDN}, isZipped, aLocEnabled, dalinc);
       
        //only HTTP may be root; CDN is invalid - must be the last in the list!!!!!
        for (int i=0;i<hostsDNCS.size();i++){
            CarouselHost host = hostsDNCS.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());;
            ICarousel carousel = new CarouselDNCS(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsST.size();i++){
            CarouselHost host = hostsST.get(i);
            ICarousel carousel = CarouselSTFactory.getCarouselST(host,host.isZipped(), aLocEnabled);
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsHTTP.size();i++){
            CarouselHost host = hostsHTTP.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());
            ICarousel carousel;
            if (host.isRootNode()){
                carousel = new CarouselSFTP(host, executor, host.getLocation(), getBatchRootIniName(), host.isZipped());
                if (null == root){
                    root = carousel;
                    carouselsIn.add(root);
                }
            }
            else
                carousel = new CarouselSFTP(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsCDN.size();i++){
            CarouselHost host = hostsCDN.get(i);
            ICarousel carousel = new CarouselCDN(host, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
    }
    
    
    @Override
    protected void generateServerXML(){
        initExecutors();
        Map<String,String> params = new HashMap<>();
        params.put("useAlternativeLocations", "true");
        params.put("useZippedZdtsIni", "true");  
        ServerXMLHelper helper=new ServerXMLHelper(properties, params, hostsHTTP, hostsST, hostsDNCS, hostsCDN);
        helper.save("server.xml");
        
    }
    
    @Features("Publisher")
    @Stories("All types. One of the locations is not available")
    @Description("Add new file on carousels. Expected result: file is added on available locations, root z_dts.ini is not changed")
    @Test(priority=1)
    @Override
    public void addNewFileOnCarousel() throws Exception{
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.ADD, ADD_NEW_FILE_ON_CAROUSEL, ADD_NEW_FILE_ON_CAROUSEL, 
            0, ExpectedResult.getFileFlags(), GoldenFile.SECOND, 200, null, new InvalidResponseVerifier0(), getExcludeMask(carouselsOut.size()-1));
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    
    @Features("Publisher")
    @Stories("All types. One of the locations is not available")
    @Description("Perform Update operation. Expected result: file is updated on available locations, root z_dts.ini is not changed")
    @Test(priority=2)
    @Override
    public void updateExisting() throws Exception{
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.UPDATE, UPDATE_EXISTING, UPDATE_EXISTING, 
            CREATE_BEFORE_FLAG, ExpectedResult.getFileIncFlags(), GoldenFile.SECOND, 200, null, new InvalidResponseVerifier0(), getExcludeMask(carouselsOut.size()-1));
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("All types. One of the locations is not available")
    @Description("Delete existing file. Expected result: file is deleted on available locations, root z_dts.ini is not changed")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarousel() throws Exception{
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.DELETE, DELETE_FILE_FROM_CAROUSEL, DELETE_FILE_FROM_CAROUSEL, 
            CREATE_BEFORE_FLAG, 0, GoldenFile.SECOND, 200, null, new InvalidResponseVerifier0(), getExcludeMask(carouselsOut.size()-1));
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("All types. One of the locations is not available")
    @Description("PublisherTest AMS log")
    @Test(priority=100)
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
    
}
