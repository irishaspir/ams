package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.KEY_PRESS_REQUEST_ID;
import static com.zodiac.ams.charter.tests.StoriesList.KEY_PRESS;

/**
 * Created by SpiridonovaIM on 20.07.2017.
 */
public class KeyPressTests extends CDBeforAfter {

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + KEY_PRESS)
    @Description("10.2.2 Message from device to AMS where <request id>=2, key isn't specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(priority = 1)
    public void sendKeyPressReceiveError() {
        String deviceId = DEVICE_ID_NUMBER_1;
        cdRudpHelper.sendRequest(deviceId, KEY_PRESS_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        byte[] data = cdstbEmulator.getData();
        assertLog(log, deviceId);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + KEY_PRESS)
    @Description("10.2.1 Message from device to AMS where <request id>=2, key is specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "pressKey", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendKeyPressKeyIs(Integer key) {
        String mac = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createKeyPressMessage(key);
        cdRudpHelper.sendRequest(mac, message, KEY_PRESS_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertKeyPress(KEY_PRESS_REQUEST_ID,key);
    }
}
