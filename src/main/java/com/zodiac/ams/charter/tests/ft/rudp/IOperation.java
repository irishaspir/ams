package com.zodiac.ams.charter.tests.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import org.jboss.netty.channel.ChannelFuture;

public interface IOperation {
    boolean isSuccessAfterSession(int sessionId) throws Exception;
    boolean isSuccessAfterStart(int sessionId,ChannelFuture future) throws Exception;
    boolean isSuccessAfterData(int sessionId,ChannelFuture future) throws Exception;
    boolean isSuccessAfterSegment(int sessionId,ChannelFuture future,int segment,int index,int total) throws Exception;
    boolean isSuccess() throws Exception;
    void publish() throws Exception;
    void setRUDPTestingSendManager(RUDPTestingSendManager sender);
    void addSessionId(Integer id);
}
