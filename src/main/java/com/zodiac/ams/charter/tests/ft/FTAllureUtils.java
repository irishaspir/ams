package com.zodiac.ams.charter.tests.ft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.DescriptionType;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

public class FTAllureUtils {
    
    static final Logger LOG = LoggerFactory.getLogger(FTAllureUtils.class);
    
    static public void copyAnnotations(Class from, Class to){
        try{
            Method[] methodsFrom = from.getDeclaredMethods();
            for (Method methodFrom: methodsFrom){
                methodFrom.setAccessible(true);
                Test testFrom = methodFrom.getAnnotation(Test.class);
                if (null != testFrom){
                    Class<?> executableClass = Class.forName("java.lang.reflect.Executable");
                    Field field = executableClass.getDeclaredField("declaredAnnotations");
                    field.setAccessible(true);
                    Map<Class<? extends Annotation>, Annotation> annotationsFrom = (Map<Class<? extends Annotation>, Annotation>) field.get(methodFrom);
                    try{
                        Method methodTo = to.getDeclaredMethod(methodFrom.getName());
                        methodTo.setAccessible(true);
                        if (null != methodTo.getAnnotation(Test.class)){
                            Map<Class<? extends Annotation>, Annotation> annotationsTo = (Map<Class<? extends Annotation>, Annotation>) field.get(methodTo);
                            annotationsTo.clear();
                            annotationsTo.putAll(annotationsFrom);
                        }
                    }
                    catch(NoSuchMethodException ex){   
                    }
                }
            }
        }
        catch(Exception ex){
            LOG.error("PublisherAllureUtils copyAnnotations exception: {}", ex.getMessage());
        }
        
    }
    /**
     * returns Test with invocationCount=0
     * @param test
     * @return 
     */
    private static Test getDisabledTest(Test test){
        return new Test(){
            @Override
            public String[] groups() {
                return test.groups();
            }

            @Override
            public boolean enabled() {
                return test.enabled();
            }

            @Override
            public String[] parameters() {
                return test.parameters();
            }

            @Override
            public String[] dependsOnGroups() {
                return test.dependsOnGroups();
            }

            @Override
            public String[] dependsOnMethods() {
                return test.dependsOnMethods();
            }

            @Override
            public long timeOut() {
                return test.timeOut();
            }

            @Override
            public long invocationTimeOut() {
                return test.invocationCount();
            }

            @Override
            public int invocationCount() {
                return 0;
            }

            @Override
            public int threadPoolSize() {
                return test.threadPoolSize();
            }

            @Override
            public int successPercentage() {
                return test.successPercentage();
            }

            @Override
            public String dataProvider() {
                return test.dataProvider();
            }

            @Override
            public Class<?> dataProviderClass() {
                return test.dataProviderClass();
            }

            @Override
            public boolean alwaysRun() {
                return test.alwaysRun();
            }

            @Override
            public String description() {
                return test.description();
            }

            @Override
            public Class[] expectedExceptions() {
                return test.expectedExceptions();
            }

            @Override
            public String expectedExceptionsMessageRegExp() {
                return test.expectedExceptionsMessageRegExp();
            }

            @Override
            public String suiteName() {
                return test.suiteName();
            }

            @Override
            public String testName() {
                return test.testName();
            }

            @Override
            public boolean sequential() {
                return test.sequential();
            }

            @Override
            public boolean singleThreaded() {
                return test.singleThreaded();
            }

            @Override
            public Class retryAnalyzer() {
                return test.retryAnalyzer();
            }

            @Override
            public boolean skipFailedInvocations() {
                return test.skipFailedInvocations();
            }

            @Override
            public boolean ignoreMissingDependencies() {
                return test.ignoreMissingDependencies();
            }

            @Override
            public int priority() {
                return test.priority();
            }

            @Override
            public Class<? extends Annotation> annotationType() {
                return test.annotationType();
            }
        };

    }
    
    static public void exclude(Class clazz, IExcludeProvider provider) {
        try{
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method: methods){
                method.setAccessible(true);
                Test test = method.getAnnotation(Test.class);
                if (null != test && provider.isExcluded(method.getName())){
                    Test disabledTest = getDisabledTest(test);
                    Class<?> executableClass = Class.forName("java.lang.reflect.Executable");
                    Field field = executableClass.getDeclaredField("declaredAnnotations");
                    field.setAccessible(true);
                    Map<Class<? extends Annotation>, Annotation> annotations = (Map<Class<? extends Annotation>, Annotation>) field.get(method);
                    annotations.put(Test.class, disabledTest);
                }
            }
        }
        catch(Exception ex){
            LOG.error("PublisherAllureUtils exclude exception: {}", ex.getMessage());
        } 
    }
    
    static public void replaceStory(Class clazz, IStoryProvider provider) {
        try{
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method: methods){
                method.setAccessible(true);
                Stories stories = method.getAnnotation(Stories.class);
                if (null != stories){
                    Stories newStory = new Stories(){
                        @Override
                        public String[] value() {
                            return new String[] {provider.getStory()};
                        }

                        @Override
                        public Class<? extends Annotation> annotationType() {
                            return stories.annotationType();
                        }
                    };
                    Class<?> executableClass = Class.forName("java.lang.reflect.Executable");
                    Field field = executableClass.getDeclaredField("declaredAnnotations");
                    field.setAccessible(true);
                    Map<Class<? extends Annotation>, Annotation> annotations = (Map<Class<? extends Annotation>, Annotation>) field.get(method);
                    annotations.put(Stories.class, newStory);
                }
            }
        }
        catch(Exception ex){
            LOG.error("PublisherAllureUtils replaceStory exception: {}", ex.getMessage());
        } 
    }
    
    /**
     * Returns the description only for method without parameters
     * @return the description 
     * @throws ClassNotFoundException 
     */
    static public String getDescription(){        
        StackTraceElement [] elements = Thread.currentThread().getStackTrace();
        
        for (StackTraceElement element: elements){
            String methodName = element.getMethodName();
            String className = element.getClassName();
            
            try {
                Class clazz = Class.forName(className);
                try {
                    Method method = clazz.getDeclaredMethod(methodName);
                    method.setAccessible(true);
                    Description description = method.getAnnotation(Description.class);
                    if (null != description)
                        return description.value();
                }
                catch(NoSuchMethodException ex){
                    LOG.error("FTAllureUtils: getDescription exception = {}", ex.getMessage());
                }
            }
            catch(ClassNotFoundException ex){
                LOG.error("FTAllureUtils: getDescription exception = {}", ex.getMessage());
                return null;
            }
        }
        
        return null;
    }
    static public void replaceDescription(Class clazz, IDescriptionProvider provider){
        try{
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method: methods){
                method.setAccessible(true);
                Description description = method.getAnnotation(Description.class);
                if (null != description){
                    Description newDescription = new Description(){
                        @Override
                        public String value() {
                            return provider.getDescription(method.getName(), description.value());
                        }

                        @Override
                        public Class<? extends Annotation> annotationType() {
                            return description.annotationType();
                        }

                        @Override
                        public DescriptionType type() {
                            return description.type();
                        }
                    };
                    Class<?> executableClass = Class.forName("java.lang.reflect.Executable");
                    Field field = executableClass.getDeclaredField("declaredAnnotations");
                    field.setAccessible(true);
                    Map<Class<? extends Annotation>, Annotation> annotations = (Map<Class<? extends Annotation>, Annotation>) field.get(method);
                    annotations.put(Description.class, newDescription);
                }
            }
        }
        catch(Exception ex){
            LOG.error("PublisherAllureUtils replaceDescription exception: {}", ex.getMessage());
        } 
    }
    
    /**
     * Returns method with given name without parameters in the youngest child class
     * @param name the name of the method
     * @return 
     */
    static public Method getYoungestMethod(String name){
        Method method = null;
        StackTraceElement [] elements = Thread.currentThread().getStackTrace();
        String className = null;
        for (StackTraceElement element: elements){
            if (name.equals(element.getMethodName()))
                className = element.getClassName();
            else
            if (null != className)
                break;
        }
        
        if (null != className){
            try{
                Class clazz = Class.forName(className);
                method = clazz.getDeclaredMethod(name);
                method.setAccessible(true);
            }
            catch(ClassNotFoundException|NoSuchMethodException ex){
                LOG.error("PublisherAllureUtils getYoungestMethod={} exception: {}", name, ex.getMessage());
            }
        }
        return method;
    }
    
}
