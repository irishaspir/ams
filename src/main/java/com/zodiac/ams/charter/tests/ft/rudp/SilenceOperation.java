package com.zodiac.ams.charter.tests.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SilenceOperation extends Operation{
    
    public int getTotalCount(){
        return sessionsIds.size();
    }
    
    public int getFailedCount(){
        return getFailed().size();
    }
    
    private boolean isFailed(int sessionId){
        RUDPTestingSession ts = getTestingSession(sessionId);
        if (null ==  ts || !ts.isSuccess())
            return true;
        
        RUDPTestingPackage pk=ts.getRUDPTestingPackage();
        if (null == pk || !Arrays.equals(pk.getData(),ts.getRemoteData()))
            return true;
        
        return false;
    }
    
    private List<Integer> getFailed(){
        List<Integer> result = new ArrayList<>();
        for (int id: sessionsIds){
            if (isFailed(id))
                result.add(id);
        }
        return result;
    }
    
    @Override
    public void publish() throws Exception{
        Publisher.showSessionsStatistics(new SessionsStatistic(getFailed(),getTotalCount()));
    }
    
    
    @Override
    public boolean isSuccess(){
        return 0 == getFailedCount();
    }
    
}
