package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.getSegments;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getRUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.IDataPacketProvider;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacketSerializer;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnyResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnySessionResult;
import com.zodiac.ams.charter.services.ft.rudp.packet.PacketType;


public class TestingServiceN010 extends TestingService {
    
    TestingServiceN010(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    class DataPacketProvider implements IDataPacketProvider{
        
        private final RUDPTestingSession ts;
        private final int badSegmentNumber;
        
        private DataPacketProvider(RUDPTestingSession ts, int badSegmentNumber){
            this.ts = ts;
            this.badSegmentNumber = badSegmentNumber;
        }

        @Override
        public byte[] get(int sessionId, int segmentNumber, DataPacket dataPacket) {
            try {
                if (segmentNumber == badSegmentNumber){
                    byte[] dataOld = dataPacket.getData();
                    int length = dataOld.length-1;
                    byte[] dataNew = new byte[length];
                    System.arraycopy(dataOld, 0, dataNew, 0, length);
                    return RUDPTestingUtils.generateDataPacket(PacketType.DATA.packetType, sessionId, segmentNumber,dataNew);
                }
                else
                    return DataPacketSerializer.toBytes(dataPacket);
            }
            catch(Exception ex){
                if (null != ts){
                    ts.setErrorMsg(ex.getMessage());
                    ts.setSuccess(false);
                }
                return null;
            }
        }
    }
    
    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = getTestingSession(sessionId);
            RUDPTestingPackage pk = getRUDPTestingPackage(sessionId);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (!getExpectedSuccessResult(sessionId,future))
                return;
            
            int count = pk.getSegmentsCount();
            int [] segments = getSegments(count,order);
            //int badNumber = segments.length - 1; //CRC here
            int badNumber = segments.length/2;
            
            sender.setDataPacketProvider(sessionId, new DataPacketProvider(ts,badNumber));
            
            for (int segment: segments){
                future=sender.sendSegment(sessionId,segment);
                if (!getExpectedAnyResult(sessionId,future))
                    return;
            }
        
            if (null == ts.getRUDPSenderSession().getErrorPacket()){
                ts.setSuccess(false);
                ts.setErrorMsg("Channel error must be detected.");
                getExpectedAnySessionResult(sessionId);
            }
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            sender.removeDataPacketProvider(sessionId);
            if (null != this.countDown)
                countDown.countDown();
        }
    }
}
