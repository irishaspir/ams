package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * The tests suite for DC event id.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DcEventIdTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_ID_NEGATIVE + "1 - Check without Event ID")
    @Description("4.011.1 [DC][Event ID] Check without Event ID" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><><10057|0|50>}]")
    public void sendMessageWithoutEventId() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                // EventId must be null, this is the test purpose!
                .setEventId(null)
                .addParameter(10057) //destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50) //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_ID_NEGATIVE + "2 - <Event ID> is unknown")
    @Description("4.011.2 [DC] <Event ID> is unknown" +
            "<Node ID><Hub ID><5><2>[<1484729890><1><1><1584>" +
            "{<Time delta><89><3><10139 | 1 | reason | 1>}], where reason is from 1 to 149")
    public void sendMessageWithUnknownEventId() throws Exception {
        // TODO fix later
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_ID_NEGATIVE + "3 - Check with incorrect Event ID")
    @Description("4.011.3 [DC][Event ID] Check with incorrect Event ID" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><ewer><10057|0|50>}]")
    public void sendMessageWithIncorrectEventId1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                // EventId must be garbage, this is the test purpose!
                .setEventId("ewer")
                .addParameter(10057) //destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50) //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_ID_NEGATIVE + "3 - Check with incorrect Event ID")
    @Description("4.011.3 [DC][Event ID] Check with incorrect Event ID" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286>< ><10057|0|50>}]")
    public void sendMessageWithIncorrectEventId2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                // EventId must be garbage, this is the test purpose!
                .setEventId(" ")
                .addParameter(10057) //destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50) //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_ID_NEGATIVE + "3 - Check with incorrect Event ID")
    @Description("4.011.3 [DC][Event ID] Check with incorrect Event ID" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><213><10057|0|50>}]")
    public void sendMessageWithIncorrectEventId3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                // EventId must be garbage, this is the test purpose!
                .setEventId("213")
                .addParameter(10057) //destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50) //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
