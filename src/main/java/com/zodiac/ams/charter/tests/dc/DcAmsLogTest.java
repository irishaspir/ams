package com.zodiac.ams.charter.tests.dc;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

public class DcAmsLogTest {
    
    private static final String STORIES = "DC AMS Log and Settings";
    private final SSHHelper executor = FTConfig.getInstance().getSSHExecutor();
    
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(STORIES)
    @Description(STORIES)
    public void getAmsLogAndSettings() throws Exception{
        getServerXml();
        getAmsLog();
    }
    
    @Attachment(value = "AMS log", type = "text/plain")
    private String getAmsLog() throws Exception{
        return getRemoteFile("ams.log", FTConfig.getInstance().getAMSLog());
    }
    
    @Attachment(value = "server.xml", type = "text/xml")
    private String getServerXml() throws IOException{
        return new String(Files.readAllBytes(new File("server.xml").toPath()));
    }

    private String getRemoteFile(String local, String remote) throws Exception {
        executor.getFile(local, remote);
        return new String(Files.readAllBytes(new File(local).toPath()));
    }
}
