package com.zodiac.ams.charter.tests.epg.get.positive;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByServiceId;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataPositiveTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.17, 2.1.79 Send Get EPG cheDataReceived, receive valid cheDataReceived, only one service_id ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataForTodayAmountRowsDurationReceiveData() {
        String csv = epgSampleBuilder.createCsv("24", "60");
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        createAttachment(logEpg);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        Logger.info("getPublishedZdbFiles");
        boolean x = epgLog.compareLogAMSWithPatternValid(logEpg, epgSampleBuilder.getCountRows(), epgSampleBuilder.getCountServices(), zdbDataUtisl.epgLastFolder());
        assertTrue(x, PATTERN_EPG_LOG);
        Logger.info("compareEpgVerifierLogWithPattern");
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        Logger.info("getStringRowByDate");
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.21 Send Get EPG cheDataReceived, receive valid cheDataReceived, two service_ids ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataForTodayAmountServiceIdAmountRowsDurationReceiveData() {
        String csv = epgSampleBuilder.createCsv(2, "24", "60");
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgSampleBuilder.getCountRows(), epgSampleBuilder.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows * 2), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByServiceId(epgSampleBuilder.getServiceIdList()), epgCsvHelper.expectedValidCsvWithSegments(amountRows, 2));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.78 Send Get EPG cheDataReceived, receive valid cheDataReceived, for next day ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataForTomorrowAmountRowsAmountDaysDurationData() {
        String csv = epgSampleBuilder.createCsv("24", "60", 1);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgSampleBuilder.getCountRows(), epgSampleBuilder.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.82 Send Get EPG cheDataReceived, receive valid cheDataReceived with breaks ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataForTodayReceiveDataWithBreaks() {
        String csv = epgSampleBuilder.createCsv("20", "60", "60", "4");
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgSampleBuilder.getCountRows(), epgSampleBuilder.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternWithGapEpgVerifierLog(20, 4), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments(20, 1));
    }

//    @Features(FeatureList.GET_EPG_DATA)
//    @Stories(StoriesList.POSITIVE)
//    @Description("2.1.81 Send Get EPG cheDataReceived, repeat action with the same meanings, receive valid cheDataReceived ")
//    @Test(enabled = false)
//    public void sendGetEpgDataForCurrentRepeatReceiveData() {
//        String csv = epgSampleBuilder.createCsv("24", "60");
//        createAttachment(csv);
//
//        String[] serviceIdList = epgSampleBuilder.getServiceIdList();
//        Assert.assertEquals(sendUpdateEpgDataRequest(),  success200);
//
//        Assert.assertEquals(sendUpdateEpgDataRequest(), "Result code 200 is success");
//        Assert.assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
//
//        String zdbDir = zdbDataUtisl.epgLastFolder();
//        sshHelper.getFiles(zdbDir);
//
//        log = readAMSLog();
//
//        Assert.assertTrue(epgLog.compareLogAMSWithPatternValid(epgLog.getLogAMSForValidData(log), epgSampleBuilder.getCountRows(), epgSampleBuilder.getCountServices(), zdbDir));
//        Assert.assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(24), runEPGVerifier()));
//        Assert.assertEquals(dbUtils.getStringRowByServiceId(serviceIdList), epgCsvHelper.expectedValidCsvWithSegmentsRepeatData());
//    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.75 Send Get EPG data, receive file with out optional columns, AMS must work correct ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithOutOptionalColumns() {
        String csv = epgCsvHelper.createInvalidCsvWithoutOptional();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternWithouOptional(epgLog.getLogAMSForMissedFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        // assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegmentsWithoutOptional());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.77 Send Get EPG cheDataReceived, receive file with not ordered by TIME cheDataReceived")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithNotOrderedByTime() {
        String csv = epgCsvHelper.createInvalidCsvNotOrderedByTime();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.createCsvWithSegmentsNotOrdered());
    }
}