package com.zodiac.ams.charter.tests.settings.che;

import com.zodiac.ams.charter.http.helpers.settings.SettingsGetHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.DataSettingsHelper;
import com.zodiac.ams.common.logging.Logger;
import org.json.JSONObject;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.SettingsDeleteHelper.sendDeleteSettingsDeviceId;
import static com.zodiac.ams.charter.tests.StoriesList.DELETE_SETTINGS_FROM_CHE;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

/**
 * Created by SpiridonovaIM on 21.08.2017.
 */
public class SendDeleteSettingsFromCHETest extends GetSettingsBeforeAfter {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + DELETE_SETTINGS_FROM_CHE)
    @Description("1.3.1 Send delete all Settings from CHE to AMS, MAC is unknown, receive error. " +
            "(DELETE HTTP://host:port/ams/settings?deviceid=UNKNOWN_MAC)")
    @TestCaseId("169586")
    @Test(priority = 2)
    public void deleteSettingsReceiveError() {
        String response = sendDeleteSettingsDeviceId(UNKNOWN_DEVICE_ID);
        createAttachment(SettingsGetHelper.responseJson);
        createAttachment(readAMSLog());
        assertEquals(response, ERROR_404);
        assertJsonEquals(jsonDataUtils.error404NotSpecified(), SettingsGetHelper.responseJson);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + DELETE_SETTINGS_FROM_CHE)
    @Description("1.3.1 Send delete all Settings from CHE to AMS, MAC is valid, receive success. " +
            "(DELETE HTTP://host:port/ams/settings?deviceid=MAC)")
    @TestCaseId("169586")
    @Test(priority = 2)
    public void deleteSettingsReceiveSuccess() {
        String response = sendDeleteSettingsDeviceId(DEVICE_ID_NUMBER_1);
        createAttachment(SettingsGetHelper.responseJson);
        createAttachment(readAMSLog());
        assertEquals(response, ERROR_404);
        assertJsonEquals(jsonDataUtils.error404NotSpecified(), SettingsGetHelper.responseJson);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + DELETE_SETTINGS_FROM_CHE)
    @Description("1.3.1 Send delete all Settings from CHE to AMS, MAC is valid, receive success. " +
            "(DELETE HTTP://host:port/ams/settings?deviceid=MAC)")
    @TestCaseId("169586")
    @Test(dataProvider = "allSettingsList", dataProviderClass = DataSettingsHelper.class, priority = 2)
    public void deleteSettingReceiveSuccess(SettingsOption option) {

        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOption(option)
                .build();
        JSONObject json = jsonDataUtils.deviceIdRequest(options, DEVICE_ID_NUMBER_1);
        Logger.info("Request json: " + json.toString());

        String response = sendDeleteSettingsDeviceId(DEVICE_ID_NUMBER_1);

        createAttachment(SettingsGetHelper.responseJson);
        createAttachment(readAMSLog());
        assertEquals(response, ERROR_404);
        assertJsonEquals(jsonDataUtils.error404NotSpecified(), SettingsGetHelper.responseJson);
    }

    



}
