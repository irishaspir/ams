package com.zodiac.ams.charter.tests.epg;

import com.zodiac.ams.charter.bd.ams.tables.LineupsSvcWbxUtils;
import com.zodiac.ams.charter.emuls.che.epg.EpgCHEEmulator;
import com.zodiac.ams.charter.emuls.che.epg.EpgSampleBuilder;
import com.zodiac.ams.charter.http.listeners.epg.EpgHttpDataUtils;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.charter.tomcat.command.TomcatHelper;
import com.zodiac.ams.charter.tomcat.logger.epg.EpgLogParser;
import com.zodiac.ams.charter.tomcat.zbd.ZDBDataUtisl;
import com.zodiac.ams.common.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.BeforeSuite;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.cleanEpgData;
import static com.zodiac.ams.charter.bd.ams.tables.LineupsSvcWbxUtils.delTmServiceIdInLineupSvcWbx;
import static com.zodiac.ams.charter.bd.ams.tables.LineupsSvcWbxUtils.insertTmsServiceIdLineupSvcWbx;

public class StepsForEpg extends Steps {

    protected EpgCHEEmulator epgCHEEmulator;
    protected EpgSampleBuilder epgSampleBuilder = new EpgSampleBuilder();
    protected EpgHttpDataUtils epgHttpDataUtils = new EpgHttpDataUtils();
    protected ZDBDataUtisl zdbDataUtisl = new ZDBDataUtisl();
    protected List<String> log = new ArrayList<>();
    protected EpgLogParser epgLog = new EpgLogParser();
    private int amountDays = 14;

    @Step
    protected void runCHEEpg() {
        epgCHEEmulator = new EpgCHEEmulator();
        epgCHEEmulator.registerConsumer();
        epgCHEEmulator.start();
        Logger.info("CHE Emulator's run");
    }

    @Step
    protected void stopCHEEpg() {
        epgCHEEmulator.stop();
        Logger.info("CHE Emulator's stopped");
    }

    @Step
    public List<String> readAMSLog() {
        Logger.info("Read AMS log");
        return readAMSLog(EPG_TEST_FINISHED);
    }

    @Step
    protected void deleteEPGData() {
        TomcatHelper.clearTemp(zdbDataUtisl.getTempFolder());
        try {
            if (!new File("charter.log").exists())
                throw new IOException();
            FileUtils.forceDelete(new File("charter.log"));
        } catch (IOException e) {
            Logger.info("File charter.log doesn't exist");
        }
        File[] listFile = new File(config.getEpgData()).listFiles();
        for (File f : listFile)
            if (f.getName().startsWith("ipg") || f.getName().startsWith("_epg"))
                f.delete();
        PrintWriter out = null;
        try {
            out = new PrintWriter("epg_verifier.log");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        out.write("");
        out.close();
    }

    @Step
    protected void cleanDB() {
        cleanEpgData(amountDays);
    }

    @BeforeSuite
    public void cleanAMSServer() {
        LineupsSvcWbxUtils dbUtils = new LineupsSvcWbxUtils();
        delTmServiceIdInLineupSvcWbx();
        insertTmsServiceIdLineupSvcWbx();
    }
}
