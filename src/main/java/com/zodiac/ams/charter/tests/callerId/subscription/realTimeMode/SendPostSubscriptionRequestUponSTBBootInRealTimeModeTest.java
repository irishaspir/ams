package com.zodiac.ams.charter.tests.callerId.subscription.realTimeMode;

import com.zodiac.ams.charter.emuls.stb.settings.SettingsSTBEmulator;
import com.zodiac.ams.charter.http.helpers.callerId.CallerIdStringDataUtils;
import com.zodiac.ams.charter.http.listeners.callerId.CallerIdHttpDataUtils;
import com.zodiac.ams.charter.rudp.settings.SettingsRudpHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.callerId.subscription.StepsForCallerIdSubscription;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils.getAllFromSubscriptions;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.setMigrationFlagNullInSettingsSTB;
import static org.testng.Assert.assertEquals;

public class SendPostSubscriptionRequestUponSTBBootInRealTimeModeTest extends StepsForCallerIdSubscription {

    private SettingsSTBEmulator settingsSTBEmulator;
    private SettingsRudpHelper settingsRudpHelper;
    private CallerIdHttpDataUtils callerIdHttpDataUtils = new CallerIdHttpDataUtils();
    private CallerIdStringDataUtils stringDataUtils = new CallerIdStringDataUtils();

    @BeforeMethod
    public void beforeMethod(){
        settingsSTBEmulator = new SettingsSTBEmulator();
        settingsSTBEmulator.registerConsumer();
        settingsSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        settingsRudpHelper = new SettingsRudpHelper(settingsSTBEmulator);
        runCHECallerId();
        getTestTime();
    }

    @AfterMethod
    public void afterMethod(){
        settingsSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
        stopCHECallerId();
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.REAL_TIME_SUBSCRIPTION)
    @Description("3.3.6 Sent POST request for STB subscription after first boot")
    @TestCaseId("12345")
    @Test()
    public void sendSubscriptionUponStbBoot() {
        setMigrationFlagNullInSettingsSTB(MAC_NUMBER_2);
        settingsRudpHelper.sendSetSettings(MAC_NUMBER_2, setCallerIdNotificationOn());
        createAttachment(readAMSLog());
        assertEquals(callerIdHttpDataUtils.getCallerIdSubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_2));
        assertEquals(getAllFromSubscriptions(),NO_DATA_IN_DB);
    }
}
