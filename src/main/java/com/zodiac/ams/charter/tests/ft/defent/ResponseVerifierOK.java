package com.zodiac.ams.charter.tests.ft.defent;

import java.util.HashMap;

public class ResponseVerifierOK extends ResponseVerifier{
    ResponseVerifierOK(String macAddress){
        super(macAddress, new HashMap<String,String>(){{put("status","OK");}});
    }
}
