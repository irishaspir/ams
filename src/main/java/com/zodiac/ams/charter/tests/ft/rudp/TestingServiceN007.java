package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.getSegments;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getSegmentSize;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getRUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnySessionResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnyResult;

public class TestingServiceN007 extends TestingService {
    
    TestingServiceN007(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = getTestingSession(sessionId);
            RUDPTestingPackage pk = getRUDPTestingPackage(sessionId);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (!getExpectedSuccessResult(sessionId,future))
                return;
            
            int size = sender.getSize(sessionId);
            sender.setSize(sessionId, size+1);//3*getSegmentSize());
            pk = getRUDPTestingPackage(sessionId);
            
            int count = pk.getSegmentsCount();
            int [] segments = getSegments(count,order);
            
            for (int segment: segments){
                future=sender.sendSegment(sessionId,segment);
                if (!getExpectedAnyResult(sessionId,future))
                    return;
            }
            
            if (null == ts.getRUDPSenderSession().getErrorPacket()){
                ts.setSuccess(false);
                ts.setErrorMsg("Channel error must be detected");
                getExpectedAnySessionResult(sessionId);
            }
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            if (null != this.countDown)
                countDown.countDown();
        }
    }
}
