package com.zodiac.ams.charter.tests.callerId.notification;

import com.zodiac.ams.charter.helpers.DataUtils;
import com.zodiac.ams.charter.tests.callerId.StepsForCallerId;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.List;

public class StepsForCallerIdNotification extends StepsForCallerId {

    protected String CALL_ID_MANY_CHARACTERS = RandomStringUtils.randomAlphabetic(256);
    protected String ID_MANY_CHARACTERS = RandomStringUtils.randomAlphabetic(256);
    protected List<String> parameters;

    @BeforeMethod
    public void beforeMethod() {
        runCHECallerId();
        runSTBCallerId();
        getTestTime();
    }

    @AfterMethod
    public void afterMethod() {
        stopCHECallerId();
        stopSTBCallerId();
    }
}
