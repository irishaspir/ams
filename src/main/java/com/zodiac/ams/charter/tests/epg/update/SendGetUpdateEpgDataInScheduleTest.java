package com.zodiac.ams.charter.tests.epg.update;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetUpdateEpgDataInScheduleTest extends StepsForGetUpdateEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_UPDATE_EPG_DATA)
    @Description("2.2.3 Send Get update EPG cheDataReceived in schedule, com.dob.charter.error-repeat-count, receive valid cheDataReceived ")
    @TestCaseId("21212")
    @Test()
    public void sendGetUpdateEpgDataReceiveDataInSchedule() {
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log = readAMSLog();
        createAttachment(log);
        assertTrue(epgLog.compareLogAMSWithPatternConnectionRefused(epgLog.getLogAMSForConnectionRefused(log)), PATTERN_EPG_LOG);
    }
}
