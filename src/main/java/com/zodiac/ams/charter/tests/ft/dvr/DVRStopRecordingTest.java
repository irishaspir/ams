package com.zodiac.ams.charter.tests.ft.dvr;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.handlers.UniversalListener;
import com.zodiac.ams.charter.helpers.ft.FtDbHelper;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.dvr.*;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.KeepPartial;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomObject;
import static com.zodiac.ams.charter.tests.FeatureList.DVR;


public class DVRStopRecordingTest extends DVRTest {
    
    private static final String POSITIVE = "Stop recording. Positive";
    private static final String NEGATIVE = "Stop recording. Negative";
    
    @Override
    public boolean needRestart(){
        return false;
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.1.1 [DVR][Stop][Keep] RecordingId is in db")
    @Test(priority=2)
    public void test_6_4_1_1() throws Exception {
        positiveImpl(KeepPartial.Keep);
    }
     
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.1.2 [DVR][Stop][Delete] RecordingId is in db")
    @Test(priority=2)
    public void test_6_4_1_2() throws Exception {
        positiveImpl(KeepPartial.Delete);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.1.3 [DVR][Stop] Stop several recordings")
    @Test(priority=2)
    public void test_6_4_1_3() throws Exception {
        positiveImpl(new KeepPartial[] {KeepPartial.Delete, KeepPartial.Keep});
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.2.1 [DVR][Stop] Error code '1 - ERROR, no operation has been accomplished'")
    @Test(priority=3)
    public void test_6_4_2_1() throws Exception {
        positiveErrorImpl(Error.Error1);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.2.2 [DVR][Stop] Error code '7 - DVR is not authorized on the STB'")
    @Test(priority=3)
    public void test_6_4_2_2() throws Exception {
        positiveErrorImpl(Error.Error7);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.2.3 [DVR][Stop] Error code '10 - DVR is not ready (still initializing)'")
    @Test(priority=3)
    public void test_6_4_2_3() throws Exception {
        positiveErrorImpl(Error.Error10);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.2.4 [DVR][Stop] Error code '13 - Invalid parameter was specified'")
    @Test(priority=3)
    public void test_6_4_2_4() throws Exception {
        positiveErrorImpl(Error.Error13);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.2.5 [DVR][Stop] Error code '16 - Unknown error'")
    @Test(priority=3)
    public void test_6_4_2_5() throws Exception {
        positiveErrorImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.2.6 [DVR][Stop] Error code '17 - STB is busy'")
    @Test(priority=3)
    public void test_6_4_2_6() throws Exception {
        positiveErrorImpl(Error.Error17);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.3.1 [DVR][Stop] Status '1 - ERROR, no operation has been accomplished'")
    @Test(priority=4)
    public void test_6_4_3_1() throws Exception {
        positiveImpl(Error.Error1);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.3.2 [DVR][Stop] Status '3 - No target program exists'")
    @Test(priority=4)
    public void test_6_4_3_2() throws Exception {
        positiveImpl(Error.Error3);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.3.3 [DVR][Stop] Status '13 - Invalid parameter was specified'")
    @Test(priority=4)
    public void test_6_4_3_3() throws Exception {
        positiveImpl(Error.Error13);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.3.4 [DVR][Stop] Status '16 - Unknown error'")
    @Test(priority=4)
    public void test_6_4_3_4() throws Exception {
        positiveImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.3.5 [DVR][Stop] Random statuses (successful and unsuccessful)")
    @Test(priority=4)
    public void test_6_4_3_5() throws Exception {
        positiveImpl(RandomUtils.getRandomObject(STATUSES, STATUSES.length, false));
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.3.6 [DVR][Stop] All statuses")
    @Test(priority=4)
    public void test_6_4_3_6() throws Exception {
        positiveImpl(STATUSES);
    }
    
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.4.1 [DVR][Stop][Keep][RecordingId] negative cases")
    @Test(priority=5)
    public void test_6_4_4_1() throws Exception {
        invalidRequestRecordId(KeepPartial.Keep);
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.4.2 [DVR][Stop][Delete][RecordingId] negative cases")
    @Test(priority=5)
    public void test_6_4_4_2() throws Exception {
        invalidRequestRecordId(KeepPartial.Delete);
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.4.3 [DVR][Stop][Delete][keepPartial] negative cases")
    @Test(priority=5)
    public void test_6_4_4_3() throws Exception {
        invalidRequesKeepPartial();
    }
    
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.5.1 [DVR][Stop] 'Count' is inadmissible")
    @Test(priority=5)
    public void test_6_4_5_1() throws Exception {
        negativeResponseImpl(p -> p.clearIdsStatuses());
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.5.2 [DVR][Stop] 'Count' is missing")
    @Test(priority=5)
    public void test_6_4_5_2() throws Exception {
        negativeResponseImpl(p -> p.setSize(StopRecordingResponse.NO_VALUE));
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.5.3 [DVR][Stop] \"Status\" is inadmissible")
    @Test(priority=5)
    public void test_6_4_5_3() throws Exception {
        positiveImpl(new KeepPartial[]{RandomUtils.getRandomObject(KeepPartial.values())}, 
            Error.Error0, new Error[]{Error.NON_EXISTENT_UNKNOWN});
    }
    
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.5.4 [DVR][Stop] \"Status\" is missing")
    @Test(priority=5)
    public void test_6_4_5_4() throws Exception {
        negativeResponseImpl(p -> {p.clearIdsStatuses(); p.setSize(1);});
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.4.5.5 [DVR][Stop] \"STBSpaceStatus\" is '0'")
    @Test(priority=5)
    public void test_6_4_5_5() throws Exception {
        positiveImpl(new KeepPartial[]{RandomUtils.getRandomObject(KeepPartial.values())}, 
            Error.Error0, new Error[]{Error.Error0}, p -> p.setSpaceStatus(SpaceStatus.get0()), false);
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.5.6 [DVR][Stop] \"STBSpaceStatus\" is missing")
    @Test(priority=5)
    public void test_6_4_5_6() throws Exception {
        negativeResponseImpl(p -> p.setSpaceStatus(null));
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.4.5.7 [DVR][Stop] Recording doesn't exist in DVR_STB")
    @Test(priority=5)
    public void test_6_4_5_7() throws Exception {
        noDvrStb();
    }
    
    
    private void invalidRequestRecordId(KeepPartial keep) throws Exception{
        invalidRequestImpl(p -> p.setKeepPartial(keep), 
            Arrays.asList(p -> p.setRecordingId("test"), p -> p.setRecordingId(""), p -> p.setRecordingId(null)), 
            400);
    }
    
    private void invalidRequesKeepPartial() throws Exception{
        invalidRequestImpl(null, 
            Arrays.asList(p -> p.setKeepPartial("test"), p -> p.setKeepPartial(""), p -> p.setKeepPartial(null)), 
            400);
    }
    
    private void invalidRequestImpl(Consumer<StopRecordingRequestParam> common, List<Consumer<StopRecordingRequestParam>> consumers, int expectedStatus) throws Exception{
        String uri = properties.getDVRUri();
        StringBuilder sb = new StringBuilder();
        int index = 0;
        for (Consumer<StopRecordingRequestParam> consumer: consumers){
            index++;
            try{
                StopRecordingRequest request =  StopRecordingRequest.getRandom(deviceId, 1);
                StopRecordingRequestParam param = request.getParam(0);
                if (null != common)
                    common.accept(param);
                consumer.accept(param);
                FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, request.toString());
                showHERequestToAMS(uri, request.toString()/*request*/, null /*expected response*/, httpResponse /*actual response*/);
                Assert.assertTrue(expectedStatus == httpResponse.getStatus(),"Expected status: " + expectedStatus + " Actual: " + httpResponse.getStatus());
            }
            catch(AssertionError error){
                sb.append(index).append(". ").append(error.getMessage()).append(" ");
            }
        }
        if (sb.length() > 0)
            Assert.assertTrue(false, sb.toString());
    }
    
    
    private void negativeResponseImpl(Consumer<StopRecordingResponse> consumer) throws Exception{
        positiveImpl(new KeepPartial[] {RandomUtils.getRandomObject(KeepPartial.values())}, Error.STB_INVALID_RESPONSE,
            new Error[]{Error.Error0}, consumer, false);
    }
    
    private void positiveErrorImpl(Error error) throws Exception{
        positiveImpl(new KeepPartial[] {RandomUtils.getRandomObject(KeepPartial.values())}, error, new Error[]{Error.Error0});
    }
    
    private void positiveImpl(KeepPartial keep) throws Exception{
        positiveImpl(keep, Error.Error0);
    }
    
    private void positiveImpl(KeepPartial[] keeps) throws Exception{
        Error[] statuses = new Error[keeps.length];
        Arrays.fill(statuses, Error.Error0);
        positiveImpl(keeps, Error.Error0, statuses);
    }
    
    private void positiveImpl(KeepPartial keep, Error status) throws Exception{
        positiveImpl(new KeepPartial[] {keep}, Error.Error0, new Error[]{status});
    }
    
    private void positiveImpl(Error status) throws Exception{
        positiveImpl(RandomUtils.getRandomObject(KeepPartial.values()), status);
    }
    
    private void positiveImpl(Error[] statuses) throws Exception{
        KeepPartial [] keeps = new KeepPartial[statuses.length];
        for (int i=0; i < statuses.length; i++)
            keeps[i] = RandomUtils.getRandomObject(KeepPartial.values());
        positiveImpl(keeps, Error.Error0, statuses);
    }
    
    private void positiveImpl(KeepPartial[] keeps, Error error, Error[] statuses) throws Exception{
        positiveImpl(keeps, error, statuses, null, false);
    }
    
    private void noDvrStb() throws Exception{
        positiveImpl(new KeepPartial[]{RandomUtils.getRandomObject(KeepPartial.values())},
            Error.Error0,
            new Error[] {Error.STB_INVALID_RESPONSE},
            null,
            true);
    }
    
    private void positiveImpl(KeepPartial[] keeps, Error error, Error[] statuses, Consumer<StopRecordingResponse> respConsumer,
        boolean noDvrStb) throws Exception{
        Assert.assertTrue(keeps.length == statuses.length, "KeepPartials and Statuses must heave the same size, but "+
            keeps.length + " != " + statuses.length);
        
         Set<String> missingCassandra = new HashSet(){{
            add("startTime"); 
            add("duration"); 
            add("unitingParameter");
        }}; 
        
        int id = getRandomInt(1, 1_000_000);
        
        List<Integer> ids  =  new ArrayList<>();
        List<Integer> types = new ArrayList<>();
        
        StopRecordingRequest request = new StopRecordingRequest(deviceId);
        for (int i=0; i < keeps.length; i++){
            ids.add(id);
            types.add(getRandomObject(Type.values()).getCode());
            request.addParam(new StopRecordingRequestParam(id, keeps[i]));
            id++;
        }
        
        StopRecordingResponse response = new StopRecordingResponse(request, Arrays.asList(statuses), SpaceStatus.getRandom());
        response.setError(error);
        StopRecordingResponse expResponse = new StopRecordingResponse(response);
        if (null != respConsumer)
            respConsumer.accept(response);
        
        SpaceStatus space = null != response.getSpaceStatus() ? response.getSpaceStatus() : SpaceStatus.get0();
        SpaceStatus spaceExp = (Error.Error0 == error) ? new SpaceStatus(space) : SpaceStatus.get0();
        
        prepareDB(ids.toArray(new Integer[]{}), types.toArray(new Integer[]{}));
        if (noDvrStb){
            FtDbHelper.deleteDB("DVR_STB", new HashMap(){{put("MAC_STR", deviceId);}});
            FtDbHelper.deleteCassandra("STB", new HashMap(){{put("DEVICEID", deviceId);}});
            spaceExp = new SpaceStatus();
        }
        
        List<DVRParam> dbDvrExp = new ArrayList<>();
        List<DVRParam> csDvrExp = new ArrayList<>();
        for (Integer rid: ids){
            dbDvrExp.add(DvrDbHelper.getDBDVRParam(deviceId, rid));
            csDvrExp.add(DvrDbHelper.getCassandraDVRParam(deviceId, (long)rid));
        }
        
        CharterStbEmulator stb = null;
        DataConsumer consumer =  null;
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20*1_000);
        
        try {
            Semaphore semaphore = new Semaphore(0);
            consumer = new DataConsumer(semaphore);
            stb = createCharterSTBEmulator(STB_PROPS);
            stb.registerConsumer(consumer, HandlerType.Universal);
            UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
            listener.setResponse(response.getData());            
            startCharterSTBEmulator(stb);
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, request.toString());
//            Thread.sleep(60*60*1_000);
            
            
            byte[] actual = consumer.getData();
            
            //show HE-AMS request/response
            showHERequestToAMS(uri, request.toString()/*request*/, expResponse.toString()/*expected response*/, httpResponse /*actual response*/);
            //show AMS-STB expected/actual request
            showAMSRequestToSTB(request.getData()/*expected*/, actual/*actual*/);
            //show expected/actual space status
            SpaceStatus dbSpace = DvrDbHelper.getDBSpaceStatus(deviceId);
            SpaceStatus csSpace = DvrDbHelper.getCassandraSpaceStatus(deviceId);
            showDBSpaceStatus(spaceExp, dbSpace);
            showCassandraSpaceStatus(spaceExp, csSpace);

            //verify response status
            Assert.assertTrue(200 == httpResponse.getStatus(), "Expected status: 200 Actual: " + httpResponse.getStatus());        
            //verify AMS-HE response
            verifyJSON(expResponse.toJSON(), new JSONObject((String)httpResponse.getEntity()),"Expected and actual AMS response must be the same");
            //verify AMS-STB request
            Assert.assertTrue(Arrays.equals(request.getData(), actual), "Expected and actual request from AMS to STB must be the same");
            //verify space status
            verifyJSON(spaceExp.toJSON(), dbSpace.toJSON(), "Expected and actual DB Space status must be the same");
            verifyJSON(spaceExp.toJSON(), csSpace.toJSON(), "Expected and actual Cassandra Space status must be the same");
            
            //show and verify DB/Cassandra iteem
            for (int i=0; i < ids.size(); i++){
                int rid = ids.get(i);
                
                DVRParam dbAct = DvrDbHelper.getDBDVRParam(deviceId, rid);
                DVRParam dbExp = dbDvrExp.get(i);
                DVRParam csAct = DvrDbHelper.getCassandraDVRParam(deviceId, (long)rid);
                DVRParam csExp = csDvrExp.get(i);
                showDBRecording(dbExp, dbAct);
                showCassandraRecording(csExp, csAct);
                verifyJSON(dbExp.toJSON(), dbAct.toJSON(), "Expected and actual information in DB DVR_RECORDING table must be the same");
                verifyJSON(csExp.toJSON(missingCassandra), csAct.toJSON(), "Expected and actual information in CASSANDRA RECORDINGS table must be the same");
            }
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }   
    
    private static final Error[] STATUSES = new Error[]{
        Error.Error0, Error.Error1, Error.Error3, Error.Error13, Error.Error16
    };
    
}