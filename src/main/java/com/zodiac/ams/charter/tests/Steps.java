package com.zodiac.ams.charter.tests;


import com.zodiac.ams.charter.runner.DBDataBuilder;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.store.TestKeyWordStore;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import com.zodiac.ams.common.ssh.SSHHelper;
import com.zodiac.ams.common.tomcat.ParserLog;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONObject;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.setKeysInSettingsSTBDataV2;
import static com.zodiac.ams.charter.helpers.DataUtils.decimalToHex;
import static com.zodiac.ams.charter.runner.Utils.defineIp;
import static com.zodiac.ams.charter.runner.Utils.isLocalIp;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.CHARTER_LOG;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.LOG_PATTERN;


public class Steps extends TestKeyWordStore {

    protected SoftAssert sa = new SoftAssert();
    protected Config config = new Config();
    protected ParserLog parserLog = new ParserLog();
    protected Date startTestTime;
    protected final int READ_LOG_TIMEOUT = 1;
    protected final String PATH_TO_LOG = String.format(LOG_PATTERN, config.getTomcatPath()) + "/" + CHARTER_LOG;
    protected List<String> log = new ArrayList<>();
    protected String UNKNOWN_DEVICE_ID = decimalToHex(unknownMac());
    private static int macLastDigit = Integer.parseInt(RandomStringUtils.randomNumeric(6));

    protected ArrayList<SettingsOption> options = allSettings();

    @Attachment(value = "Json attachment", type = "application/json")
    protected static byte[] createAttachment(JSONObject str) {
        Logger.debug("Attache  json " + str.toString());
        try {
            return str.toString().getBytes("utf-8");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read from json: ", e);
        }
    }

    @Attachment(value = "Text attachment", type = "text/csv")
    protected static byte[] createAttachment(String str) {
        Logger.debug(str);
        try {
            return str.getBytes("utf-8");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read from csv: ", e);
        }
    }

    @Attachment(value = "List attachment", type = "text/plain")
    protected static String createAttachment(List<String> log) {
        Logger.debug("Attache charter log from startTestTime: ");
        StringBuilder stringBuilder = new StringBuilder();
        for (String one : log) {
            stringBuilder.append(one).append("\n");
        }
        return stringBuilder.toString();
    }


    @Attachment(value = "List pattern attachment", type = "text/plain")
    protected static byte[] createPatternAttachment(List<Pattern> patterns) {
        Logger.debug("Attache charter log from startTestTime: ");
        StringBuilder stringBuilder = new StringBuilder();
        for (Pattern one : patterns) {
            stringBuilder.append(one).append("\n");
        }
        try {
            return stringBuilder.toString().getBytes("utf-8");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read from csv: ", e);
        }
    }

    @Step
    public void setDefaultValue(ArrayList<SettingsOption> options, String mac) {
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        for (int i = 0; i < options.size(); i++) {
            keys.add(i, options.get(i).getColumnName());
            values.add(i, "'" + options.get(i).getDefaultValue() + "'");
        }
        setKeysInSettingsSTBDataV2(mac, keys, values);
        Logger.info("Keys have been set as default");
    }

    @Step
    public String getDeviceIdByMac(String mac) {
        return decimalToHex(mac);
    }

    @BeforeSuite
    public void fill() {
        defineIp();
        new DBDataBuilder().fillDb();
    }

    @Step
    public static String unknownMac() {
        return MAC_NUMBER_2.substring(0, MAC_NUMBER_2.length() - 6) + macLastDigit;
    }

    @Step
    public List<String> readAMSLog() {
        if (isLocalIp()) {
            log = getLogFile();
            Logger.info("Get Log!");
        } else log = parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT);
        return log;
    }

    @Step
    public List<String> readAMSLog(String str) {
        Logger.info("isIpLocal = " + isLocalIp());
        if (isLocalIp()) {
            log = getLogFile(str);
            Logger.info("Get Log with ssh!");
        } else {
            Logger.info("Get Log !");
            log = parserLog.readLog(PATH_TO_LOG, startTestTime, str);
        }
        log.forEach(value -> Logger.info("AMS LOG !!!!!!!!!!!" + value));
        return log;
    }

    @Step
    public List<String> getLogFile() {
        long l = startTestTime.getTime() - 2000;
        Logger.info("startTestTime long" + String.valueOf(l));
        Date startTestTime1 = new Date(l);
        SSHHelper sshHelper = new SSHHelper("192.168.22.190", "irina.spiridonova", "6DpMorBR");
        Logger.info("Get log = " + sshHelper.getFile("charter.log", "/srv/tomcat7/logs/charter.log"));
        return parserLog.readLog("charter.log", startTestTime1, READ_LOG_TIMEOUT);
    }

    @Step
    public List<String> getLogFile(String str) {
        long l = startTestTime.getTime() - 2000;
        Date startTestTime1 = new Date(l);
        SSHHelper sshHelper = new SSHHelper("192.168.22.190", "irina.spiridonova", "6DpMorBR");
        Logger.info("Get log = " + sshHelper.getFile("charter.log", "/srv/tomcat7/logs/charter.log"));
        return parserLog.readLog("charter.log", startTestTime1, str);
    }

}
