package com.zodiac.ams.charter.tests.ft.defent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import org.testng.Assert;


public class ResponseVerifier implements IResponseVerifier {
    private final Map<String,String> params = new HashMap<>();
    
    ResponseVerifier(String macAddress, Map<String,String> params){
        params.put("macAddress", macAddress);
        params.putAll(params);
    }

    @Override
    public void verify(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            Iterator<String> it = params.keySet().iterator();
            while (it.hasNext()){
                String key = it.next();
                String expected = params.get(key);
                String actual = obj.optString(key);
                if (null != expected)
                    Assert.assertTrue(expected.equals(actual), "Key=" + key + " Expected=" + expected + " Actual=" + actual);
                else
                    Assert.assertTrue(!"".equals(actual), "Key=" + key + " Expected=any not empty value Actual=empty value");
            }
        }
        catch(Exception ex){
             Assert.assertTrue(false, "Unable to parse response: " +ex.getMessage());
        }
    }
    
}
