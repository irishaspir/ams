package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselCDN;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselDNCS;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSFTP;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselSTFactory;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsCDN;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsHTTP;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsST;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

public class PublisherTestAllDalincZip extends PublisherTestImpl {
    
    private CarouselHostsST hostsST;
    private CarouselHostsHTTP hostsHTTP;
    private CarouselHostsDNCS hostsDNCS;
    private CarouselHostsCDN hostsCDN;
    
    
    public PublisherTestAllDalincZip(){
        this.isZipped = true;
        this.aLocEnabled = true;
        this.dalinc = true;
        this.carouselType = CarouselType.BATCH_1;
    }
    
    
    void initExecutors(){
        //hostsST = new CarouselHostsST(properties, new CarouselType[] {carouselType, CarouselType.ST}, isZipped, aLocEnabled, dalinc);
        hostsHTTP = new CarouselHostsHTTP(properties, new CarouselType[] {carouselType, CarouselType.HTTP}, isZipped, aLocEnabled, dalinc);
        hostsDNCS = new CarouselHostsDNCS(properties, new CarouselType[] {carouselType, CarouselType.DNCS}, isZipped, aLocEnabled, dalinc);
        hostsCDN = new CarouselHostsCDN(properties, new CarouselType[] {carouselType, CarouselType.CDN}, isZipped, aLocEnabled, dalinc);
       
        //only HTTP may be root
        for (int i=0;i<hostsDNCS.size();i++){
            CarouselHost host = hostsDNCS.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());;
            ICarousel carousel = new CarouselDNCS(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        for (int i=0;i<hostsCDN.size();i++){
            CarouselHost host = hostsCDN.get(i);
            ICarousel carousel = new CarouselCDN(host, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        
        //TO DO: upload ???
        /*
        for (int i=0;i<hostsST.size();i++){
            CarouselHost host = hostsST.get(i);
            ICarousel carousel = CarouselSTFactory.getCarouselST(host,host.isZipped(), aLocEnabled);
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
        */
        
        for (int i=0;i<hostsHTTP.size();i++){
            CarouselHost host = hostsHTTP.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());
            ICarousel carousel;
            if (host.isRootNode()){
                carousel = new CarouselSFTP(host, executor, host.getLocation(), getBatchRootIniName(), host.isZipped());
                if (null == root){
                    root = carousel;
                    carouselsIn.add(root);
                }
            }
            else
                carousel = new CarouselSFTP(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            carouselsOut.add(carousel);
        }
    }
    
    
    @Override
    protected void generateServerXML(){
        initExecutors();
        Map<String,String> params = new HashMap<>();
        params.put("useAlternativeLocations", "true");
        params.put("useZippedZdtsIni", "true");  
        ServerXMLHelper helper=new ServerXMLHelper(properties, params, hostsHTTP, hostsST, hostsDNCS, hostsCDN);
        helper.save("server.xml");
        
    }

    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("8.2.20 Add new file on carousels. Expected result: file is not added, record is added to z_dts.ini")
    @Test(priority=1)
    @Override
    public void addNewFileOnCarousel() throws Exception{
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addNewFileOnCarouselDalinc();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("Add file on carousels.The file already exists. Expected result: file and record in z_dts.ini are not changed")
    @Test(priority=2)
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        //add +1 add- failed 
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addExistingFileOnCarousel();
        verifyRootZdts(root, carouselsOut, this.getCommonVersion()/*vers, false*/);
    }
    
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("8.2.27 Delete existing file from carousels. Expected result: file is not deleted, record in z_dts.ini is deleted")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarousel() throws Exception{
        //add +1 delete +1
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.deleteFileFromCarousel();
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("Delete file from carousels. The file and record in z_dts.ini don't exist. Expected result: records in z_dts.ini aren't changed")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.deleteFileFromCarouselNoFileNoRecord();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("8.2.21 Perform Update operation. The file already exists on carousels. Expected result: file is not updated, record in z_dts.ini is incremented.")
    @Test(priority=4)
    @Override
    public void updateExisting() throws Exception {
        //add+1 update+1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.updateExisting();
        verifyRootZdts(root, carouselsOut, this.getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("Perform Update operation. The file doesn't exists on carousels. Expected result: file is not added, z_dts.ini is not changed.")
    @Test(priority=5)
    @Override
    public void updateNoExisting() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.updateNoExisting();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("Perform ADD_MODIFY operation.The file already exists on carousels. Expected result: file is not updated, record in z_dts.ini is incremented.")
    @Test(priority=6)
    @Override
    public void addModifyExisting() throws Exception {
        //add+1 add_modify+1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.addModifyExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("Perform ADD_MODIFY operation.The file doesn't exist on carousels. Expected result: file is not added, record is added to z_dts.ini.")
    @Test(priority=7)
    @Override
    public void addModifyNoExisting() throws Exception {
        //add_modify +1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addModifyNoExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("Perform ADD_MODIFY_NO_VER_INC operation. The file already exists on carousels. Expected result: file is not updated, z_dts.ini is not updated.")
    @Test(priority=8)
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        //add+1 add_modify_no_ver_inc +1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.addModifyNoVerIncExisting();
        verifyRootZdts(root, carouselsOut, this.getCommonVersion(), true /*strict = false for CDN*/);
    }
    
    
    
    
    @Features("Publisher")
    @Stories("All types alternative location (mode=DALINC isZipped=true)")
    @Description("PublisherTest AMS log")
    @Test(priority=100)
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
    
}
