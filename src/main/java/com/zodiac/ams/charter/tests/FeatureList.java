package com.zodiac.ams.charter.tests;

public class FeatureList {

    public static final String SETTINGS = "[1 - Settings]";
    public static final String EPG = "[2 - EPG]";
    public static final String EPG_FILTERING = "[2 - EPG filtering]";
    public static final String CALLER_ID = "[3 - CallerId]";
    public static final String DSG = "[5 - DSG]";
    public static final String DVR = "[6 - DVR]";
    public static final String PPV = "[7 - PPV]";
    public static final String ROLLBACK = "[11 - Rollback]";
    public static final String ACTIVITY_CONTROL = "[13 - ActivityControl]";
    public static final String STB_LOOKUP = "[14 - STB Lookup]";
    public static final String STB = "STB";
    public static final String LINEUP = "[9 - Lineup]";
    public static final String CHANNEL_PACKAGES = "[16 - Channel packages]";
    public static final String SI = "SI";
    public static final String COMPANION_DEVICES = "[10 - Companion devices]";
    public static final String DATA_COLLECTION = "STBDataReceived collection";
    public static final String PUBLISHER = "[8 - Publisher]";
    public static final String START_TOMCAT = "Start AMS";
    public static final String CLEANUP = "[15 - Cleanup]";
    public static final String RECENTLY_WATCHED  = "[17 - Recently Watched]";
    public static final String REMINDER  = "[18 - Reminder]";
    public static final String TALKING_GUIDE  = "[19 - Talking Guide]";
    public static final String RUDP  = "[20 - RUDP Testing]";

    /*
     * The DC features.
     */
    public static final String DC_NUMERIC_VALUE = "4";
    public static final String DC_HEADER_NAME = "[" + DC_NUMERIC_VALUE + " - DC]";
}
