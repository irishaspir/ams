package com.zodiac.ams.charter.tests.callerId.subscription.delayedMode;

import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils.getAllFromSubscriptions;
import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostSettingsCallerIdNotification;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendPostSubscriptionRequestInDelayedModeDBModeIsNotActiveTest extends StepsForCallerIdDBModeIsNotActive {

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.1a Exit from DB mode to SUBSCRIBE mode by subscription")
    @TestCaseId("12345")
    @Test(priority = 3)
    public void exitFromDBModeToSubscribeModeBySubscription() {
        turnOffCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOn();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        waitThreeSeconds();
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterSubscriptions(DEVICE_ID_NUMBER_1) );
        runCHECallerId();
        waitTimeout();
        assertEquals(callerIdHttpDataUtils.getCallerIdSubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_1));
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternExitFromDBToSubscriptionMode(callerIDLog.getLogAMSForValid(log)));
        assertEquals(getAllFromSubscriptions(), NO_DATA_IN_DB);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.1b Exit from DB mode to SUBSCRIBE mode by unsubscription")
    @TestCaseId("12345")
    @Test(priority = 4)
    public void exitFromDBModeToSubscribeModeByUnsubscription() {
        turnOnCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOff();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        waitThreeSeconds();
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterUnsubscriptions(DEVICE_ID_NUMBER_1) );
        runCHECallerId();
        waitTimeout();
        assertEquals(callerIdHttpDataUtils.getCallerIdUnsubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_1));
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternExitFromDBToSubscriptionMode(callerIDLog.getLogAMSForValid(log)));
        assertEquals(getAllFromSubscriptions(), NO_DATA_IN_DB);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.4a Exit from DB mode to SUBSCRIBE mode for several STBs by subscription")
    @TestCaseId("12345")
    @Test(priority = 5)
    public void exitFromDBModeToSubscribeModeForMoreThanOneSTBBySubscription() {
        turnOffCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOn();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_2, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        waitThreeSeconds();
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterSubscriptions( DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2) );
        runCHECallerId();
        waitTimeout();
        assertEquals(callerIdHttpDataUtils.getCallerIdSubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2));
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternExitFromDBToSubscriptionMode(callerIDLog.getLogAMSForValid(log)));
        assertEquals(getAllFromSubscriptions(), NO_DATA_IN_DB );

    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.4b Exit from DB mode to SUBSCRIBE mode for several STBs by unsubscription")
    @TestCaseId("12345")
    @Test(priority = 6)
    public void exitFromDBModeToSubscribeModeForMoreThanOneSTBByUnsubscription() {
        turnOnCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOff();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_2, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        waitThreeSeconds();
        assertEquals(getAllFromSubscriptions(), callerIdDBModeHelper.createCharterUnsubscriptions(DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2) );
        runCHECallerId();
        waitTimeout();
        assertEquals(callerIdHttpDataUtils.getCallerIdUnsubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2));
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternExitFromDBToSubscriptionMode(callerIDLog.getLogAMSForValid(log)));
        assertEquals(getAllFromSubscriptions(), NO_DATA_IN_DB );
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.6a Send subscription request to more than nummacs (parameter from server.xml) STB")
    @TestCaseId("12345")
    @Test(priority = 1)
    public void sendSubscriptionRequestToMoreThanNummacsSTB() {
        turnOffCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOn();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_2, options), SUCCESS_200);
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_3, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        waitThreeSeconds();
        assertEquals(getAllFromSubscriptions(),
                callerIdDBModeHelper.createCharterSubscriptions(DEVICE_ID_NUMBER_3, DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2));
        runCHECallerId();
        waitTimeout();
        assertEquals(callerIdHttpDataUtils.getCallerIdSubscriptionUriPattern(), callerIdCHEEmulator.getFirstURI());
        assertInRange(callerIdCHEEmulator.getSubscribeFirstRequestBody(), DEVICE_ID_NUMBER_3, DEVICE_ID_NUMBER_2, DEVICE_ID_NUMBER_1);
        assertEquals(callerIdHttpDataUtils.getCallerIdSubscriptionUriPattern(), callerIdCHEEmulator.getSecondURI());
        assertInRangeTwo(callerIdCHEEmulator.getSubscribeSecondRequestBody(), DEVICE_ID_NUMBER_3, DEVICE_ID_NUMBER_2, DEVICE_ID_NUMBER_1);
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternExitFromDBToSubscriptionMode(callerIDLog.getLogAMSForValid(log)));
        assertEquals(getAllFromSubscriptions(), NO_DATA_IN_DB);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.DELAYED_SUBSCRIPTION)
    @Description("3.4.6b Send unsubscription request to more than nummacs (parameter from server.xml) STB")
    @TestCaseId("12345")
    @Test(priority = 2)
    public void sendUnsubscriptionToMoreThanNummacsSTBB() {
        turnOnCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOff();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_2, options), SUCCESS_200);
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_3, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        waitThreeSeconds();
        assertEquals(getAllFromSubscriptions(),
                callerIdDBModeHelper.createCharterUnsubscriptions(DEVICE_ID_NUMBER_3, DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2));
        runCHECallerId();
        waitTimeout();
        assertEquals(callerIdHttpDataUtils.getCallerIdUnsubscriptionUriPattern(), callerIdCHEEmulator.getFirstURI());
        assertInRange(callerIdCHEEmulator.getSubscribeFirstRequestBody(), DEVICE_ID_NUMBER_3, DEVICE_ID_NUMBER_2, DEVICE_ID_NUMBER_1);
        assertEquals(callerIdHttpDataUtils.getCallerIdUnsubscriptionUriPattern(), callerIdCHEEmulator.getSecondURI());
        assertInRangeTwo(callerIdCHEEmulator.getSubscribeSecondRequestBody(), DEVICE_ID_NUMBER_3, DEVICE_ID_NUMBER_2, DEVICE_ID_NUMBER_1);
        stopCHECallerId();
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternExitFromDBToSubscriptionMode(callerIDLog.getLogAMSForValid(log)));
        assertEquals(getAllFromSubscriptions(), NO_DATA_IN_DB);
    }

  }


