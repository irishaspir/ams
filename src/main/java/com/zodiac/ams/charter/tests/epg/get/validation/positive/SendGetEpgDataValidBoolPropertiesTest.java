package com.zodiac.ams.charter.tests.epg.get.validation.positive;

import com.zodiac.ams.charter.helpers.epg.DataForCsvHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataValidBoolPropertiesTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.20, 2.1.50 Send Get EPG data, receive file with valid values DVS")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidDvs(String inputDvs) {
        String csv = epgCsvHelper.createCsvWithDifferentDvs(inputDvs);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.15, 2.1.16 Send Get EPG data, receive file with valid values CC")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidCc(String inputCc) {
        String csv = epgCsvHelper.createCsvWithDifferentCc(inputCc);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.8, 2.1.59 Send Get EPG data, receive file with valid values SAP")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidSap(String inputSap){
        String csv = epgCsvHelper.createCsvWithDifferentSap(inputSap);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.62, 2.1.63 Send Get EPG data, receive file with valid values SURROUND")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidSurround(String inputSurround) {
        String csv = epgCsvHelper.createCsvWithDifferentSurround(inputSurround);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.60, 2.1.61 Send Get EPG data, receive file with valid values STEREO")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidStereo(String inputStereo) {
        String csv = epgCsvHelper.createCsvWithDifferentStereo(inputStereo);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.57, 2.1.58 Send Get EPG data, receive file with valid values NEW")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidNew(String inputNew) {
        String csv = epgCsvHelper.createCsvWithDifferentNew(inputNew);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.68, 2.1.69 Send Get EPG data, receive file with valid values RERUN")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidRerun(String inputRerun) {
        String csv = epgCsvHelper.createCsvWithDifferentRerun(inputRerun);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.55, 2.1.56 Send Get EPG data, receive file with valid values LIVE")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidLive(String inputLive) {
        String csv = epgCsvHelper.createCsvWithDifferentLive(inputLive);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.51, 2.1.52 Send Get EPG data, receive file with valid values HD")
    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithValidHd(String inputHd) {
        String csv = epgCsvHelper.createCsvWithDifferentHd(inputHd);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }
//
//    @Features(FeatureList.EPG)
//    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
//    @Description("2.1.53, 2.1.54 Send Get EPG data, receive file with valid values IS_SERIES ")
//    @Test (dataProvider = "dataForBooleanValues", dataProviderClass = DataForCsvHelper.class)
//    public void sendGetEpgDataReceiveFileWithValidIsSeries(String inputIsSeries) {
//        String csv = epgCsvHelper.createCsvWithDifferentIsSeries(inputIsSeries);
//        createAttachment(csv);
//        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
//        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
//        readAMSLog();
//        createAttachment(log);
//        List<String> logEpg = epgLog.getLogAMSForValidData(log);
//        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
//        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
//                patternEpgLog);
//        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()), patternEpgVerifierLog);
//        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
//    }

}