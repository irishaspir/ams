package com.zodiac.ams.charter.tests.epg.update;

import com.zodiac.ams.charter.tests.epg.StepsForEpg;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class StepsForGetUpdateEpg extends StepsForEpg {

    @BeforeMethod
    public void beforeTest(){
        cleanDB();
        startTestTime = getCurrentTimeTest();
        Logger.info("Start time: " + startTestTime);
    }

    @AfterMethod
    public void afterTest(){
        deleteEPGData();
    }
}
