package com.zodiac.ams.charter.tests.epg.get.negative;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataNegativeTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.91 Send Get EPG data for 1 past day")
    @TestCaseId("21212")
    @Test(priority = 2)
    public void sendGetEpgDataFor1PastDayReceiveError() {
        String csv = epgSampleBuilder.createCsv("24","60", -1);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgSampleBuilder.getCountRows(), epgSampleBuilder.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.94 Send Get EPG data, receive file with only head, AMS write error")
    @TestCaseId("21212")
    @Test(priority = 4)
    public void sendGetEpgDataReceiveFileWithOnlyHead() {
        createAttachment(epgSampleBuilder.createCsvOnlyHead());
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternMissingData(epgLog.getLogAMSForMissedData(log), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), NO_DATA_IN_DB);
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.74 Send Get EPG data, receive file with duplicate time, AMS write error")
    @TestCaseId("21212")
    @Test(priority = 3)
    public void sendGetEpgDataReceiveFileWithDuplicateTime() {
        String csv = epgCsvHelper.createInvalidCsvDuplicateTime();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternDuplicateTime(epgLog.getLogAMSForDuplicateData(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern
                (epgDataChecker.createPatternDuplicateTimeEpgVerifierLog(amountRows +1), runEPGVerifier()), PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()),  epgCsvHelper.expectedValidCsvWithSegmentsDuplicateTime());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.76 Send Get EPG data, receive file with program overlap, AMS write error")
    @TestCaseId("21212")
    @Test(priority = 1)
    public void sendGetEpgDataReceiveFileWithProgramOverlap() {
        String csv =  epgCsvHelper.createInvalidCsvProgramOverlap();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedValidCsvWithSegments());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.95 Send Get EPG data, receive file with duplicate columns, AMS write error")
    @TestCaseId("21212")
    @Test(priority = 5)
    public void sendGetEpgDataReceiveFileWithDuplicateColumns() {
        String csv = epgCsvHelper.createInvalidCsvWithDuplicateColumns();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternDuplicateFields(epgLog.getLogAMSForDuplicateFields(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), NO_DATA_IN_DB);
    }

//    @Features(FeatureList.GET_EPG_DATA)
//    @Stories(StoriesList.NEGATIVE)
//    @Description("Send Get EPG data, receive file with epg.request-days = -1 from server.xml, AMS write error")
//    @Test(enabled = false)
//    public void sendGetEpgDataReceiveFileWithRequestDaysMinus1() {
//    }
//
//    @Features(FeatureList.GET_EPG_DATA)
//    @Stories(StoriesList.NEGATIVE)
//    @Description("Send Get EPG data, receive file with epg.request-days = 0 from server.xml, AMS write error")
//    @Test(enabled = false)
//    public void sendGetEpgDataReceiveFileWithRequestDays0() {
//    }

}