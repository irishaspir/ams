package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC first channel number.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DcFirstChannelNumberTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_CHANNEL_NUMBER_NEGATIVE + "1 - Check without First channel number")
    @Description("4.06.1 [DC][First channel number] Check without First channel number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithoutFirstChannelNumber() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                // FirstChannelNumber must be null, this is the test purpose!
                .setFirstChannelNumber(null)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_CHANNEL_NUMBER_NEGATIVE + "2 - Check with incorrect First channel number")
    @Description("4.06.2 [DC][First channel number] Check with incorrect First channel number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><#%$#%#$%#><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectFirstChannelNumber1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                // FirstChannelNumber must be a garbage string , this is the test purpose!
                .setFirstChannelNumber("#%$#%#$%#")
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_CHANNEL_NUMBER_NEGATIVE + "2 - Check with incorrect First channel number")
    @Description("4.06.2 [DC][First channel number] Check with incorrect First channel number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890>< ><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectFirstChannelNumber2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                // FirstChannelNumber must be a spaces in string , this is the test purpose!
                .setFirstChannelNumber(" ")
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_CHANNEL_NUMBER_NEGATIVE + "2 - Check with incorrect First channel number")
    @Description("4.06.2 [DC][First channel number] Check with incorrect First channel number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><3243423423423423><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectFirstChannelNumber3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                // FirstChannelNumber must be a garbage string , this is the test purpose!
                .setFirstChannelNumber("3243423423423423")
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
