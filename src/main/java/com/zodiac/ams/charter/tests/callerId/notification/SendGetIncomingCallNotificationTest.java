package com.zodiac.ams.charter.tests.callerId.notification;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.callerId.CallerIdGetHelper.*;
import static com.zodiac.ams.charter.http.helpers.callerId.CallerIdStringDataUtils.*;
import static com.zodiac.ams.charter.rudp.callerId.message.CallerIdSTBRequestPattern.nullRequest;
import static com.zodiac.ams.charter.services.callerId.CallerIdOptions.*;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetIncomingCallNotificationTest extends StepsForCallerIdIncomingCall {
    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.1 Send Incoming call notification request to 1 STB")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationTo1STB() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestTo1STB(), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.CALL_TERMINATION_NOTIFICATION)
    @Description("3.1.3 Send Incoming and Call Termination notification request to more than 1 STB")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallAndCallTerminationNotificationToMoreThan1STB() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestToSeveralSTBs(), SUCCESS_200);
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
        assertEquals(sendGetCallTerminationNotificationRequestToSeveralSTBs(), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbCallTerminationNotificationRequestPattern(VALID_CALL_ID),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.6 Send Incoming call notification request with unknown ID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithUnknownID() {
        assertEquals(sendGetIncomingCallNotificationRequestID(UNKNOWN_DEVICE_ID), ERROR_203);
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
        createAttachment(readAMSLog());
        assertEquals(error203UnknownMAC(UNKNOWN_DEVICE_ID), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.7 Send Incoming call notification request with only letters (except A, F) in ID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithOnlyLettersInID() {
        assertEquals(sendGetIncomingCallNotificationRequestID(INVALID_ID_ONLY_LETTERS), ERROR_203);
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
        createAttachment(readAMSLog());
        assertEquals(error203InvalidMAC(INVALID_ID_ONLY_LETTERS), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.8 Send Incoming call notification request with special symbols in ID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithSpecialSymbolsInID() {
        assertEquals(sendGetIncomingCallNotificationRequestID(INVALID_ID_SPECIAL_SYMBOLS), ERROR_203);
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest(), CALLER_ID_REQUEST_IS_INCORRECT);
        createAttachment(readAMSLog());
        assertEquals(error203InvalidMAC(INVALID_ID_SPECIAL_SYMBOLS), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.9 Send Incoming call notification request with incorrect order of params")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithIncorrectOrderOfParams() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestIncorrectOrderOfParams(), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.10 Send Incoming call notification request with digits in CallerName parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithDigitsInCallerName() {
        parameters = createParametersList(CALLER_NAME_ONLY_DIGITS, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestCallerName(CALLER_NAME_ONLY_DIGITS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.11 Send Incoming call notification request with digits in CallerPhoneNumber parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithDigitsInCallerPhoneNumber() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestPhoneNumber(VALID_PHONE_NUMBER), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.12 Send Incoming call notification request with digits in CallId parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithDigitsInCallId() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, CALL_ID_ONLY_DIGITS);
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_ONLY_DIGITS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.14 Send Incoming call notification request with letters in CallerName parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLettersInCallerName() {
        parameters = createParametersList(CALLER_NAME_ONLY_LETTERS, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestCallerName(CALLER_NAME_ONLY_LETTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters));
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.15 Send Incoming call notification request with letters in CallerPhoneNumber parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLettersInPhoneNumber() {
        parameters = createParametersList(VALID_CALLER_NAME, PHONE_NUMBER_ONLY_LETTERS, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestPhoneNumber(PHONE_NUMBER_ONLY_LETTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.16 Send Incoming call notification request with letters in CallID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLettersInCallId() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, CALL_ID_ONLY_LETTERS);
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_ONLY_LETTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.17 Send Incoming call notification request with a lot of characters in CallerName parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLotOfCharactersInCallerName() {
        parameters = createParametersList(CALLER_NAME_MANY_CHARACTERS, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestCallerName(CALLER_NAME_MANY_CHARACTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.18 Send Incoming call notification request with a lot of characters in CallerPhoneNumber parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLotOfCharactersInCallerPhoneNumber() {
        parameters = createParametersList(VALID_CALLER_NAME, PHONE_NUMBER_MANY_CHARACTERS, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestPhoneNumber(PHONE_NUMBER_MANY_CHARACTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.19 Send Incoming call notification request with a lot of characters in CallId parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLotOfCharactersInCallId() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, CALL_ID_MANY_CHARACTERS);
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_MANY_CHARACTERS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.20 Send Incoming call notification request with a lot of characters in ID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithLotOfCharactersInID() {
        assertEquals(sendGetIncomingCallNotificationRequestID(ID_MANY_CHARACTERS), ERROR_203);
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), nullRequest());
        createAttachment(readAMSLog());
        assertEquals(error203InvalidMAC(ID_MANY_CHARACTERS), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.21d Send Incoming call notification request with mistake in ID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithMistakeInIDParameter() {
        assertEquals(sendGetIncomingCallNotificationRequestWithMistakeIDParameter(), ERROR_500);
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.21c Send Incoming call notification request with mistake in CallerName parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithMistakeInCallerNameParameter() {
        String request = createURIIncomingCallWithMistakeInCallerNameParameter();
        createAttachment(request);
        assertEquals(sendGetIncomingCallNotificationRequestWithMistakeCallerNameParameter(request), ERROR_500);
        assertTrue(responseString.contains(CALLER_NAME_IS_NULL_OR_EMPTY), CALLER_NAME_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.21b Send Incoming call notification request with mistake in CallerPhoneNumber parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithMistakeInCallerPhoneNumberParameter() {
        assertEquals(sendGetIncomingCallNotificationRequestWithMistakePhoneNumberParameter(), ERROR_500);
        assertTrue(responseString.contains(PHONE_NUMBER_IS_NULL_OR_EMPTY), PHONE_NUMBER_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.21a Send Incoming call notification request with mistake in CallID parameter")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithMistakeInCallIDParameter() {
        assertEquals(sendGetIncomingCallNotificationRequestWithMistakeCallIDParameter(), ERROR_500);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.22 Send Incoming call notification request with PhoneNumber separated by dash")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithPhoneNumberSeparatedByDash() {
        parameters = createParametersList(VALID_CALLER_NAME, PHONE_NUMBER_SEPARATED_BY_DASH, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestPhoneNumber(PHONE_NUMBER_SEPARATED_BY_DASH), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.23 Send Incoming call notification request with special symbols in CallerName")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithSpecialSymbolsCallerName() {
        parameters = createParametersList(CALLER_NAME_SPECIAL_SYMBOLS, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestCallerName(CALLER_NAME_SPECIAL_SYMBOLS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.24 Send Incoming call notification request with special symbols in CallerPhoneNumber")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithSpecialSymbolsInCallerPhoneNumber() {
        parameters = createParametersList(VALID_CALLER_NAME, PHONE_NUMBER_SPECIAL_SYMBOLS, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestPhoneNumber(PHONE_NUMBER_SPECIAL_SYMBOLS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.25 Send Incoming call notification request with special symbols in CallId")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithSpecialSymbolsInCallId() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, CALL_ID_SPECIAL_SYMBOLS);
        assertEquals(sendGetIncomingCallNotificationRequestCallID(CALL_ID_SPECIAL_SYMBOLS), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.26 Send Incoming call notification request with & symbol in the end URI")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithAmpersandInTheEndURI() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestWithAmpersandInTheEnd(), SUCCESS_200);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.27 Send Incoming call notification request with valid and unknown ID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithValidAndUnknownID() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestToSeveralSTBs(DEVICE_ID_NUMBER_1, UNKNOWN_DEVICE_ID), ERROR_203);
        createAttachment(readAMSLog());
        assertEquals(callerIdRudpHelper.getCallerIdStbRequest(), callerIdSTBRequestPattern.stbIncomingCallNotificationRequestPattern(parameters),
                CALLER_ID_REQUEST_IS_INCORRECT);
        assertEquals(error203UnknownMAC(UNKNOWN_DEVICE_ID), responseString);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.28a Send Incoming call notification request without symbol '=' after ID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutEqualsAfterID() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestWithoutEqualsAfterID(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), NULL, parameters));
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.28b Send Incoming call notification request without symbol '=' after CallID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutEqualsAfterCallID() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, NULL);
        assertEquals(sendGetIncomingCallNotificationRequestWithoutEqualsAfterCallID(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.28c Send Incoming call notification request without symbol '=' after CallerPhoneNumber")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutEqualsAfterCallerPhoneNumber() {
        parameters = createParametersList(VALID_CALLER_NAME, NULL, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestWithoutEqualsAfterPhoneNumber(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(PHONE_NUMBER_IS_NULL_OR_EMPTY), PHONE_NUMBER_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.28d Send Incoming call notification request without symbol '=' after CallerName")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutEqualsAfterCallerName() {
        parameters = createParametersList(NULL, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestWithoutEqualsAfterCallerName(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALLER_NAME_IS_NULL_OR_EMPTY), CALLER_NAME_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.29 Send Incoming call notification request without CallerName")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutCallerName() {
        parameters = createParametersList(NULL, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestWithoutCallerName(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALLER_NAME_IS_NULL_OR_EMPTY), CALLER_NAME_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.30 Send Incoming call notification request without CallerPhoneNumber")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutCallerPhoneNumber() {
        parameters = createParametersList(VALID_CALLER_NAME, NULL, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestWithoutPhoneNumber(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(PHONE_NUMBER_IS_NULL_OR_EMPTY), PHONE_NUMBER_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.31 Send Incoming call notification request without CallID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutCallID() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, NULL);
        assertEquals(sendGetIncomingCallNotificationRequestWithoutCallId(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.32 Send Incoming call notification request without CallerName data")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutCallerNameData() {
        parameters = createParametersList(EMPTY, VALID_PHONE_NUMBER, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestCallerName(EMPTY), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALLER_NAME_IS_NULL_OR_EMPTY), CALLER_NAME_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.33 Send Incoming call notification request without CallerPhoneNumber data")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutCallerPhoneNumberData() {
        parameters = createParametersList(VALID_CALLER_NAME, EMPTY, VALID_CALL_ID);
        assertEquals(sendGetIncomingCallNotificationRequestPhoneNumber(EMPTY), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(PHONE_NUMBER_IS_NULL_OR_EMPTY), PHONE_NUMBER_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.34 Send Incoming call notification request without CallId data")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutCallIdData() {
        parameters = createParametersList(VALID_CALLER_NAME, VALID_PHONE_NUMBER, EMPTY);
        assertEquals(sendGetIncomingCallNotificationRequestCallID(EMPTY), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), DEVICE_ID_NUMBER_1, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(CALL_ID_IS_NULL_OR_EMPTY), CALL_ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.35 Send Incoming call notification request without ID data")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutIDData() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestID(EMPTY), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), EMPTY, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.NEGATIVE + StoriesList.INCOMING_CALL_NOTIFICATION)
    @Description("3.1.36 Send Incoming call notification request without ID")
    @TestCaseId("12345")
    @Test()
    public void sendIncomingCallNotificationWithoutID() {
        parameters = createValidParametersList();
        assertEquals(sendGetIncomingCallNotificationRequestWithoutID(), ERROR_500);
        createAttachment(readAMSLog());
        assertTrue(callerIDLog.compareLogAMSWithPatternIncoming(callerIDLog.getLogAMSForValid(log), NULL, parameters),
                PATTERN_CALLER_ID_LOG);
        assertTrue(responseString.contains(ID_IS_NULL_OR_EMPTY), ID_IS_NULL_OR_EMPTY + IS_ABSENT_IN_RESPONSE);
    }
}



