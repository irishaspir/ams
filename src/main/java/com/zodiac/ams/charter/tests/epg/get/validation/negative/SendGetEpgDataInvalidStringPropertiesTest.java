package com.zodiac.ams.charter.tests.epg.get.validation.negative;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataInvalidStringPropertiesTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.45 Send Get EPG data, receive file with empty SHORT_TITLE, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithEmptyShort() {
        String csv = epgCsvHelper.createInvalidCsvEmptyShortTitle();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidShortTitle());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.34 Send Get EPG data, receive file with empty LONG_TITLE, AMS write error ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithEmptyLong() {
        String csv = epgCsvHelper.createInvalidCsvEmptyLongTitle();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg,
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidLongTitle());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.33 Send Get EPG data, receive file with empty LONG_TITLE and SHORT_TITLE, AMS write error ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithEmptyLongAndShortTitle() {
        String csv = epgCsvHelper.createInvalidCsvEmptyLongAndShortTitle();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(epgLog.getLogAMSForValidData(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsEmptyLongAndShortTitle());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.19 Send Get EPG data, receive file with empty DESCRIPTION AMS write error ")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithEmptyDescription() {
        String csv = epgCsvHelper.createInvalidCsvEmptyDescription();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternMissedDescription(epgLog.getLogAMSForMissedFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(),zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidDescription());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.48 Send Get EPG data, receive file with invalid TV_RATING, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidTVRating() {
        String csv = epgCsvHelper.createCsvWithInvalidTVRating();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidTVRating(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidTVRating());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.35 Send Get EPG data, receive file with invalid MPAA_RATING, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidMPAARating() {
        String csv = epgCsvHelper.createCsvWithInvalidMPAARating();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidMPAARating(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(),zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidMPAARating());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.10 Send Get EPG data, receive file with invalid ADVISORY, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidAdvisory() {
        String csv = epgCsvHelper.createCsvWithInvalidAdvisory();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidAdvisory(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
            PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidAdvisory());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.39 Send Get EPG data, receive file with invalid PROGRAM_ID, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidProgramId() {
        String csv = epgCsvHelper.createCsvWithInvalidProgramId();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidProgramId(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
       // assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
       //         PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidProgramId());
    }

//    @Features(FeatureList.GET_EPG_DATA)
//    @Stories(StoriesList.VALIDATION_NEGATIVE)
//    @Description("2.1.43 Send Get EPG data, receive file with invalid SERIES_IDENTIFIER, AMS write error ")
//    @Test (enabled = false, dataProvider = "dataForProgramId", dataProviderClass = DataForCsvHelper.class)  // not implemented validation rules
//    public void sendGetEpgDataReceiveFileWithDifferentSeriesIdentifier(String seriesIdent) {
//        String csv = epgCsvHelper.createCsvWithSeriesIdentifier(seriesIdent);
//        createAttachment(csv);
//
//        Assert.assertEquals(sendUpdateEpgDataRequest(),  success200);
//        Assert.assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), cheEmulatorEpg.getURIList());
//
//        log = readAMSLog();
//        createAttachment(log);
//
//        List<String> logEpg = epgLog.getLogAMSForValidData(log);
//        List<String> zdb = epgLog.getPublishedZdbFiles(logEpg);
//        copyZDB(zdb);
//
//        Assert.assertTrue(epgLog.compareLogAMSWithPatternValid(epgLog.getLogAMSForValidData(log), epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()));
//        Assert.assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()));
//        Assert.assertEquals(dbUtils.getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsSeriesIdentifier(seriesIdent));
//    }

}