package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ELEVEN;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_FIVE;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ONE;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_SIX;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_THREE;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_TWO;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;
import static org.testng.Assert.fail;

/**
 * The tests suite for General Checking(4.00.x).
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcGeneralCheckingTest extends StepsForDcEvent {
    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "1 - Adding Recording Info to AM Event from AMS DB")
    @Description("4.00.1 [DC] Adding Recording Info to AM Event from AMS DB" +
            "ADD the request LATER")
    public void sendMessageToAddingRecordingInfoToAMEventFromAMSDB() throws Exception {
        fail("The test method is not yet implemented");
    }


    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "2 - Adding Recording Info to AM Event from Cassandra DB")
    @Description("4.00.2 [DC] Adding Recording Info to AM Event from Cassandra DB" +
            "ADD the request LATER")
    public void sendMessageToAddingRecordingInfoToAMEventFromCassandraDB() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "3 - Canceling Recordings Info in Local Storage")
    @Description("4.00.3 [DC] Canceling Recordings Info in Local Storage" +
            "ADD the request LATER")
    public void sendMessageToCancelingRecordingsInfoInLocalStorage() throws Exception {
        fail("The test method is not yet implemented");
    }


    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "4 - Check creation CSV files with different events")
    @Description("4.00.4 [DC] Check creation CSV files with different events" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><7>, where eventNumbers==7,  which means we have 7 event(maybe be different once) in one request" +

            "{<Time delta><286><0><10057|0|50>}] , parameters are : 'destinationChannelNumber | reason | signalLevel'" +
            "{<Time delta><89><1><3|1484729891|1>}], parameters are : 'app | sessionId | reason'" +
            "{<Time delta><89><2><1484729891|8|3|1|10161>}], parameters are : 'sessionId | *sessionActivityId* | *screen* | reason | channel'" +
            "{<Time delta><89><3>< 10139 | 1 | 1 | 1>}], parameters are : 'channel | type | reason | screenMode'" +
            "{<Time delta><89><5><1484729891|8|3|2|3>}], parameters are :'sessionId | *sessionActivityId* | *screen* | reason | clientSessionID'" +
            "{<Time delta><89><6><10|0|30|40>}] , parameters are : 'internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent'" +
            "{<Time delta><89><11><3>}], parameter is : 'sessionActivityId'")
    public void sendMessageToCheckCreationCSVFilesWithDifferentEvents1() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 0;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;

        final int eventsNumber = 7;

        // parameters
        // for EVENT_ID_ZERO
        final int parameterEventIdZeroDestinationChannelNumber = 10057;
        final int parameterEventIdZeroReason = 0;
        final int parameterEventIdZeroSignalLevel = 50;
        // for EVENT_ID_ONE
        final int parameterEventIdOneAppType = 3;
        final int parameterEventIdOneSessionId = 1484729891;
        final int parameterEventIdOneReason = 1;
        // for EVENT_ID_TWO
        final int parameterEventIdTwoSessionId = 1484729891;
        final int parameterEventIdTwoSessionActivityId = 8;
        final int parameterEventIdTwoScreen = 3;
        final int parameterEventIdTwoReason = 1;
        final int parameterEventIdTwoChannel = 10161;
        // for EVENT_ID_THREE
        final int parameterEventIdThreeChannel = 10139;
        final int parameterEventIdThreeType = 1;
        final int parameterEventIdThreeReason = 1;
        final int parameterEventIdThreeScreenMode = 1;
        // for EVENT_ID_FIVE
        final int parameterEventIdFiveSessionId = 1484729891;
        final int parameterEventIdFiveSessionActivityId = 8;
        final int parameterEventIdFiveScreen = 3;
        final int parameterEventIdFiveReason = 2;
        final int parameterEventIdFiveClientSessionID = 3;
        // for EVENT_ID_SIX
        final int parameterEventIdSixInternalHddCapacity = 10;
        final int parameterEventIdSixExternalHddCapacity = 0;
        final int parameterEventIdSixInternalHddFreeSpacePercent = 30;
        final int parameterEventIdSixExternalHddFreeSpacePercent = 40;
        // for EVENT_ID_ELEVEN
        final int parameterEventIdElevenSessionActivityId = 3;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                // EVENT_ID_ZERO
                .startEvent()
                .setEventId(EVENT_ID_ZERO)

                .addParameter(parameterEventIdZeroDestinationChannelNumber)
                .addParameter(parameterEventIdZeroReason)
                .addParameter(parameterEventIdZeroSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_ONE
                .startEvent()
                .setEventId(EVENT_ID_ONE)

                .addParameter(parameterEventIdOneAppType)
                .addParameter(parameterEventIdOneSessionId)
                .addParameter(parameterEventIdOneReason)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_TWO
                .startEvent()
                .setEventId(EVENT_ID_TWO)

                .addParameter(parameterEventIdTwoSessionId)
                .addParameter(parameterEventIdTwoSessionActivityId)
                .addParameter(parameterEventIdTwoScreen)
                .addParameter(parameterEventIdTwoReason)
                .addParameter(parameterEventIdTwoChannel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_THREE
                .startEvent()
                .setEventId(EVENT_ID_THREE)

                .addParameter(parameterEventIdThreeChannel)
                .addParameter(parameterEventIdThreeType)
                .addParameter(parameterEventIdThreeReason)
                .addParameter(parameterEventIdThreeScreenMode)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)


                // EVENT_ID_FIVE
                .startEvent()
                .setEventId(EVENT_ID_FIVE)

                .addParameter(parameterEventIdFiveSessionId)
                .addParameter(parameterEventIdFiveSessionActivityId)
                .addParameter(parameterEventIdFiveScreen)
                .addParameter(parameterEventIdFiveReason)
                .addParameter(parameterEventIdFiveClientSessionID)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_SIX
                .startEvent()
                .setEventId(EVENT_ID_SIX)

                .addParameter(parameterEventIdSixInternalHddCapacity)
                .addParameter(parameterEventIdSixExternalHddCapacity)
                .addParameter(parameterEventIdSixInternalHddFreeSpacePercent)
                .addParameter(parameterEventIdSixExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_ELEVEN
                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)

                .addParameter(parameterEventIdElevenSessionActivityId)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                // EVENT_ID_ZERO
                .startEvent()
                .setEventId(EVENT_ID_ZERO)

                .addParameter(parameterEventIdZeroDestinationChannelNumber)
                .addParameter(parameterEventIdZeroReason)
                .addParameter(parameterEventIdZeroSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_ONE
                .startEvent()
                .setEventId(EVENT_ID_ONE)

                .addParameter(parameterEventIdOneAppType)
                .addParameter(parameterEventIdOneSessionId)
                .addParameter(parameterEventIdOneReason)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_TWO
                .startEvent()
                .setEventId(EVENT_ID_TWO)

                .addParameter(parameterEventIdTwoSessionId)
                .addParameter(parameterEventIdTwoSessionActivityId)
                .addParameter(parameterEventIdTwoScreen)
                .addParameter(parameterEventIdTwoReason)
                .addParameter(parameterEventIdTwoChannel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_THREE
                .startEvent()
                .setEventId(EVENT_ID_THREE)

                .addParameter(parameterEventIdThreeChannel)
                .addParameter(parameterEventIdThreeType)
                .addParameter(parameterEventIdThreeReason)
                .addParameter(parameterEventIdThreeScreenMode)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)


                // EVENT_ID_FIVE
                .startEvent()
                .setEventId(EVENT_ID_FIVE)

                .addParameter(parameterEventIdFiveSessionId)
                .addParameter(parameterEventIdFiveSessionActivityId)
                .addParameter(parameterEventIdFiveScreen)
                .addParameter(parameterEventIdFiveReason)
                .addParameter(parameterEventIdFiveClientSessionID)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_SIX
                .startEvent()
                .setEventId(EVENT_ID_SIX)

                .addParameter(parameterEventIdSixInternalHddCapacity)
                .addParameter(parameterEventIdSixExternalHddCapacity)
                .addParameter(parameterEventIdSixInternalHddFreeSpacePercent)
                .addParameter(parameterEventIdSixExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                // EVENT_ID_ELEVEN
                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)

                .addParameter(parameterEventIdElevenSessionActivityId)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(1)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddSessionStbTsbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "5 - Check creation STB.csv")
    @Description("4.00.5 [DC] Check creation STB.csv" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><3><10|20|-23410|40>}]")
    public void sendMessageToCheckCreationSTBCSV() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "6 - Creation file and directory for CSV")
    @Description("4.00.6 [DC] Creation file and directory for CSV" +
            "ADD the request LATER")
    public void sendMessageToCheckCreationFileAndDirectoryForCSV() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "7 - Deleting Recordings Info in Local Storage")
    @Description("4.00.7 [DC] Deleting Recordings Info in Local Storage" +
            "ADD the request LATER")
    public void sendMessageToCheckDeletingRecordingsInfoInLocalStorage() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "8 - Export Delayed DVR AM Events to CSV")
    @Description("4.00.8 [DC] Export Delayed DVR AM Events to CSV" +
            "ADD the request LATER")
    public void sendMessageToCheckExportDelayedDVRAMEventsToCSV() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "9 - Export Delayed DVR AM Events to CSV without additional information")
    @Description("4.00.9 [DC] Export Delayed DVR AM Events to CSV without additional information" +
            "ADD the request LATER")
    public void sendMessageToCheckExportDelayedDVRAMEventsToCSVWithoutAddInfo() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "10 - Inability to create a directory on the remote host")
    @Description("4.00.10 [DC] Inability to create a directory on the remote host" +
            "ADD the request LATER")
    public void sendMessageToCheckInabilityToCreateDirectoryOnRemoteHost() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "11 - Inability to delete files")
    @Description("4.00.11 [DC] Inability to delete files" +
            "ADD the request LATER")
    public void sendMessageToCheckInabilityToDeleteFiles() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "12 - Inability to move uploaded files to the /uploaded directory")
    @Description("4.00.12 [DC] Inability to move uploaded files to the /uploaded directory" +
            "ADD the request LATER")
    public void sendMessageToCheckInabilityToMoveUploadedFilesToUploadedDirectory() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "13 - removing files from the temporary AM directory")
    @Description("4.00.13 [DC] removing files from the temporary AM directory" +
            "ADD the request LATER")
    public void sendMessageToCheckRemovingFilesFromTemporaryAMDirectory() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "14 - saving CSV in a temporary storage for delayed DVR")
    @Description("4.00.14 [DC] saving CSV in a temporary storage for delayed DVR" +
            "ADD the request LATER")
    public void sendMessageToCheckSavingCSVInTemporaryStorageForDelayedDVR() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "15 - Terminating processing on condition the remote host does not exist")
    @Description("4.00.15 [DC] Terminating processing on condition the remote host does not exist" +
            "ADD the request LATER")
    public void sendMessageToCheckTerminatingProcessingOnConditionRemoteHostDoesNotExist() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_POSITIVE + "16 - Using the <charter.dc.sftp.password>")
    @Description("4.00.16 [DC] Using the <charter.dc.sftp.password>" +
            "ADD the request LATER")
    public void sendMessageToCheckUsingCharterDCSFTPPassword() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_NEGATIVE + "17 - An error  while reading int 7bit")
    @Description("4.00.17 [DC] An error  while reading int 7bit" +
            "ADD the request LATER")
    public void sendMessageToCheckErrorReading7bit() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_NEGATIVE + "18 - An error has occurred while reading a string")
    @Description("4.00.18 [DC] An error has occurred while reading a string" +
            "ADD the request LATER")
    public void sendMessageToCheckErrorReadingString() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_NEGATIVE + "19 - CSV file creation error")
    @Description("4.00.19 [DC] CSV file creation error" +
            "ADD the request LATER")
    public void sendMessageToCheckCSVFileCreationError() throws Exception {
        fail("The test method is not yet implemented");
    }

    /**
     * TODO : add later
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_GENERAL_CHECKING_NEGATIVE + "20 - Unable to create a directory or file for")
    @Description("4.00.20 [DC] Unable to create a directory or file for" +
            "ADD the request LATER")
    public void sendMessageToCheckUnableCreateDirectoryOrFile() throws Exception {
        fail("The test method is not yet implemented");
    }
}
