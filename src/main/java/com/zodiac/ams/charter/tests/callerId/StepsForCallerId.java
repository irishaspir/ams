package com.zodiac.ams.charter.tests.callerId;

import com.zodiac.ams.charter.emuls.che.callerId.CallerIdCHEEmulator;
import com.zodiac.ams.charter.emuls.stb.callerId.CallerIdSTBEmulator;
import com.zodiac.ams.charter.helpers.callerId.CallerIdDBModeHelper;
import com.zodiac.ams.charter.http.helpers.callerId.CallerIdStringDataUtils;
import com.zodiac.ams.charter.http.helpers.settings.json.JsonDataUtils;
import com.zodiac.ams.charter.http.listeners.callerId.CallerIdHttpDataUtils;
import com.zodiac.ams.charter.rudp.callerId.CallerIdRudpHelper;
import com.zodiac.ams.charter.rudp.callerId.message.CallerIdSTBRequestPattern;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.charter.tomcat.logger.callerId.CallerIDLogParser;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class StepsForCallerId extends Steps {

    protected CallerIdRudpHelper callerIdRudpHelper;
    protected CallerIdCHEEmulator callerIdCHEEmulator;
    private CallerIdSTBEmulator callerIdSTBEmulator;
    protected CallerIdSTBRequestPattern callerIdSTBRequestPattern = new CallerIdSTBRequestPattern();
    protected List<String> log = new ArrayList<>();
    protected CallerIDLogParser callerIDLog = new CallerIDLogParser();
    protected JsonDataUtils jsonDataUtils = new JsonDataUtils();
    protected CallerIdDBModeHelper callerIdDBModeHelper = new CallerIdDBModeHelper();
    protected CallerIdHttpDataUtils callerIdHttpDataUtils = new CallerIdHttpDataUtils();
    protected CallerIdStringDataUtils stringDataUtils = new CallerIdStringDataUtils();
    private int CALLER_ID_TIMEOUT=10;

    @Step
    protected void runCHECallerId() {
        callerIdCHEEmulator = new CallerIdCHEEmulator();
        callerIdCHEEmulator.registerConsumer();
        callerIdCHEEmulator.start();
        Logger.info("CHE Emulator's run");
    }

    @Step
    protected void stopCHECallerId() {
        callerIdCHEEmulator.stop();
        Logger.info("CHE Emulator's stopped");
    }

    @Step
    protected void runSTBCallerId() {
        callerIdSTBEmulator = new CallerIdSTBEmulator();
        callerIdSTBEmulator.registerConsumer();
        callerIdSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        callerIdRudpHelper = new CallerIdRudpHelper(callerIdSTBEmulator);
    }

    @Step
    protected void stopSTBCallerId() {
        callerIdSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
    }

    @Step
    public List<String> readAMSLog() {
        log = parserLog.readLog(PATH_TO_LOG, startTestTime, CALLER_ID_TIMEOUT);
        return log;
    }

    @Step
    protected void getTestTime() {
        startTestTime = getCurrentTimeTest();
    }
}
