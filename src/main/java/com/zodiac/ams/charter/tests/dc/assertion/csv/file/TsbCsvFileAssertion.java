package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.TsbCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventType;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEventTimeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMacEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSequenceNumberEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertStbSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTmsIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTsbEventReasonEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTsbEventScreenModeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertTsbEventTypeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputTimeDeltaSum;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.toLocalDateTime;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_3;

/**
 * The TSB CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class TsbCsvFileAssertion extends AbstractCsvFileAssertion<TsbCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public TsbCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<TsbCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        int eventsCount = calculateInputEventsCount(input);

        assertEntriesCountEquals(eventsCount, csvFileEntries.size());

        ListIterator<TsbCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputMessageType = input.getMessageType();

        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();

            int inputFirstEventNumber = sessionData.getFirstEventNumber();

            int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

            int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

            int sequenceNumberCounter = 1;

            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                TsbCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();

                List<Object> inputParameters = eventData.getParameters();
                Integer inputParameterChannel = (Integer) inputParameters.get(0);
                Integer inputParameterType = (Integer) inputParameters.get(1);
                Integer inputParameterReason = (Integer) inputParameters.get(2);
                Integer inputParameterScreenMode = (Integer) inputParameters.get(3);

                int inputTimestampPartMsec = eventData.getTimestampPartMsec();

                String macId = csvFileEntry.getMacId();
                Long mac = DOBUtil.string2mac(macId);
                String stbSessionId = csvFileEntry.getStbSessionId();
                Integer sequenceNumber = csvFileEntry.getSequenceNumber();
                Integer tmsId = csvFileEntry.getTmsId();
                LocalDateTime eventTime = csvFileEntry.getEventTime();
                TsbEventType eventType = csvFileEntry.getEventType();
                TsbEventReason reason = csvFileEntry.getReason();
                TsbEventScreenMode screenMode = csvFileEntry.getScreenMode();

                // mac assertion
                // the same <MAC ID> field as in TUNING.CSV
                assertMacEquals(mac, inputMac);
                // end of mac assertion

                // stbSessionId assertion
                // the same as <STB Session ID> field in TUNING.CSV
                String inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
                assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
                // end of stbSessionId assertion

                // sequenceNumber assertion
                // the same as <Sequence Number> field in TUNING.CSV
                Integer inputSequenceNumber = null;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputSequenceNumber = null;
                } else {
                    inputSequenceNumber = inputFirstEventNumber + (sequenceNumberCounter - 1);
                }
                assertSequenceNumberEquals(sequenceNumber, inputSequenceNumber);
                // end of sequenceNumber assertion

                // tmsId assertion
                // current event 'channel'
                Integer inputTmsId = inputParameterChannel;
                assertTmsIdEquals(tmsId, inputTmsId);
                // end of tmsId assertion

                // eventTime assertion
                // the same as <Event Start Time> field in TUNING.CSV
                long inputEventTimeMsec;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputEventTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
                } else {
                    inputEventTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                            + inputTimestampPartMsec;
                }
                LocalDateTime inputEventTime = toLocalDateTime(inputEventTimeMsec);
                assertEventTimeEquals(eventTime, inputEventTime);
                // end of eventTime assertion

                // eventType assertion
                assertTsbEventTypeEquals(eventType, TsbEventType.of(inputParameterType));
                // end of eventType assertion

                // reason assertion
                assertTsbEventReasonEquals(reason, TsbEventReason.of(inputParameterReason));
                // end of reason assertion

                // screenMode assertion
                assertTsbEventScreenModeEquals(screenMode, TsbEventScreenMode.of(inputParameterScreenMode));
                // end of screenMode assertion

                sequenceNumberCounter++;
            }
        }
    }
}
