package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.getSegments;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getRUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnySessionResult;
import java.util.concurrent.TimeUnit;
import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.CLIENT_SESSION_TIMEOUT;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckDataPacket;
import java.util.HashSet;
import java.util.Set;

public class TestingServiceN009 extends TestingService {
    
    TestingServiceN009(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = getTestingSession(sessionId);
            RUDPTestingPackage pk = getRUDPTestingPackage(sessionId);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (!getExpectedSuccessResult(sessionId,future))
                return;
            
            int count = pk.getSegmentsCount();
            int [] segments = getSegments(count,order);
            
            Set<Integer> receivedSegments = new HashSet<>();
            for (int segment: segments)
                receivedSegments.add(segment); 
            receivedSegments.add(segments.length);
            
            
            AckDataPacket ackDataPacket =new AckDataPacket(sessionId, receivedSegments);
            ackDataPacket.setRemoteAddress(sender.getRemoteAddress());
            
            for (int segment: segments){
                future=sender.sendSegment(sessionId,segment);
                if (!getExpectedSuccessResult(sessionId,future))
                    return;
                
                ChannelFuture cf = sender.sendResponse(ackDataPacket);
                if (!cf.await(CLIENT_SESSION_TIMEOUT, TimeUnit.MINUTES)){
                    ts.setSuccess(false);
                    ts.setErrorMsg("Unable to send ACK DATA packet. Request must be completed.");
                }
                else 
                if (!cf.isSuccess()){
                    ts.setSuccess(false);
                    ts.setErrorMsg("Unable to send ACK DATA packet. Error must not be detected.");   
                }
            }
        
            getExpectedAnySessionResult(sessionId);
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            if (null != this.countDown)
                countDown.countDown();
        }
    }
}
