package com.zodiac.ams.charter.tests.ft.dvr;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.test.charter.CharterStbEmulator;
import com.zodiac.ams.charter.helpers.ft.CassandraHelper;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.dvr.DvrDbHelper;
import com.zodiac.ams.charter.services.ft.dvr.SpaceStatus;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.bd.DBHelper;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.insertDB;

public class DVRTest extends FuncTest{
    
    protected static final Map<String, String> STB_PROPS = new HashMap<String,String>(){{
        put("dvr-enabled", String.valueOf(false));
        put("universal-enabled", String.valueOf(true));
        put("universal-names", "dvr/0,dvr/1,dvr/3");
    }};
    
    protected String deviceId;
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.save("server.xml");
    }
    
    @Override
    protected void init() {
        super.init();
        deviceId = properties.getDeviceId();
        
        DBHelper.disconnect();
        DBHelper.getConnection(properties.getAMSDbConn(),properties.getAMSDbUser(),properties.getAMSDbPwd());
      
        CassandraHelper.closeSession();
        CassandraHelper.openSession();
    }
    
    @Attachment(value = "HE request to AMS", type = "text/plain")
    protected String showHERequestToAMS(String uri, String request, String expected, FTHttpUtils.HttpResult actual) {
        StringBuilder sb = new StringBuilder();
        sb.append("URI: ").append(uri);
        sb.append("\n\nRequest:\n\n");
        if (null != request)
            sb.append(request);
        sb.append("\n\nResponse: ").append(actual.getStatus());
        sb.append("\n\nExpected:\n\n");
        if (null != expected)
            sb.append(expected);
        sb.append("\n\nActual:\n\n").append((String)actual.getEntity());
        return sb.toString();
    }
    
    @Attachment(value = "AMS request to STB", type = "text/plain")
    protected String showAMSRequestToSTB(byte [] expected,byte [] actual) {
        StringBuilder sb = new StringBuilder();
        sb.append("Expected:\n\n");
        if (null != expected)
            sb.append(Arrays.toString(expected));
        sb.append("\n\nActual:\n\n");
        if (null != actual)
            sb.append(Arrays.toString(actual));
        return sb.toString();
    }
    
    protected void verifyJSON(JSONObject json1, JSONObject json2, String message){
        verifyJSON(json1, json2, message, true);
    }
    
    protected void verifyJSON(JSONObject json1, JSONObject json2, String message, boolean strict){
        try {
            JSONAssert.assertEquals(json1, json2, strict);
        }
        catch(AssertionError ex){
            Assert.assertTrue(false, message);
        }
    }
    
    protected void sendMessage(byte[]  bytes){
        CharterStbEmulator stb = null;
        try {
            stb = createCharterSTBEmulator(STB_PROPS);
        
            ZodiacMessage message = new ZodiacMessage();
            message.setBodyBytes();
            message.setMessageId("0");
            message.setSender("dvr/1");
            message.setAddressee("dvr/1");
            message.setMac(FTConfig.getMac(deviceId));
            message.setFlag(ZodiacMessage.FL_PERSISTENT);
            message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
            message.setFlag(ZodiacMessage.FL_SENDER_NOT_ADRESSEE);
            //message.setFlag(ZodiacMessage.FL_COMPRESSED);
            message.setData(bytes);
        
            stb.getRUDPTransport().send(message);
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
    
    @Attachment(value = "Space status: DB DVR_STB table", type = "text/plain")
    protected String showDBSpaceStatus(SpaceStatus expected, SpaceStatus actual){
        return showImpl(expected, actual);
    }
    
    @Attachment(value = "Space status: Cassandra STB table", type = "text/plain")
    protected String showCassandraSpaceStatus(SpaceStatus expected, SpaceStatus actual){
        return showImpl(expected, actual);
    }
    
    @Attachment(value = "Priority vector: DB DVR_STB table", type = "text/plain")
    protected String showDBPriorityVector(List<Integer> expected, List<Integer> actual){
        return showImpl(Arrays.toString(expected.toArray(new Integer[]{})), 
            Arrays.toString(actual.toArray(new Integer[]{})));
    }
    
    @Attachment(value = "Priority vector: Cassandra STB table", type = "text/plain")
    protected String showCassandraPriorityVector(List<Integer> expected, List<Integer> actual){
        return showImpl(Arrays.toString(expected.toArray(new Integer[]{})), 
            Arrays.toString(actual.toArray(new Integer[]{})));
    }
    
    protected String showImpl(Object expected, Object actual){
        StringBuilder sb = new StringBuilder();
        sb.append("Expected:\n\n");
        if (null != expected)
            sb.append(expected.toString());
        sb.append("\n\nActual:\n\n");
        if (null != actual)
            sb.append(actual.toString());
        return sb.toString();
    }
    
    @Attachment(value = "Cassandra RECORDINGS table: RecordingParam item", type = "text/plain")
    protected String showCassandraRecording(Object expected, Object actual) {
        return showImpl(expected, actual);
    }
    
    
    @Attachment(value = "DB DVR_RECORDING table:  RecordingParam item", type = "text/plain")
    protected String showDBRecording(Object expected, Object actual) {
        return showImpl(expected, actual);
    }
    
    protected void prepareDB(Integer[] ids, Integer[] types){
        Assert.assertTrue(ids.length == types.length, "Arrays must have the same size, but " + ids.length + " != " + types.length);
        DvrDbHelper.clearRecording(deviceId);
        for (int i=0; i< ids.length; i++)
            DvrDbHelper.addRecording(deviceId, ids[i], types[i], 0 == i);
        verifyCount(ids.length);
    }
    
    protected void prepareDB(Integer id,Integer type){
        prepareDB(new Integer[]{id}, new Integer[]{type});
    }
    
    protected void prepareDB(Integer [] ids){
        DvrDbHelper.clearRecording(deviceId);
        for (int id: ids)
            DvrDbHelper.addRecording(deviceId, id);
        verifyCount(ids.length);
        insertDB(DvrDbHelper.DB_STB_TABLE,
            Arrays.asList("MAC","MAC_STR"),
            Arrays.asList(FTConfig.getMac(deviceId), deviceId));
    }
    
    protected void verifyCount(int expectedCount){
        DvrDbHelper.DBCounts expected = new  DvrDbHelper.DBCounts(expectedCount);
        DvrDbHelper.DBCounts actual = DvrDbHelper.getCounts(deviceId);
        Assert.assertTrue(expected.equals(actual), "Expected: " + expected + " Actual: " + actual);
    }
}
