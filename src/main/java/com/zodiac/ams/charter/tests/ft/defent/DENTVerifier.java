package com.zodiac.ams.charter.tests.ft.defent;

import com.zodiac.ams.charter.services.ft.defent.DefEntMessage;

interface DENTVerifier {
    void verify(DefEntMessage msg, byte[] response) throws Exception;
    
}
