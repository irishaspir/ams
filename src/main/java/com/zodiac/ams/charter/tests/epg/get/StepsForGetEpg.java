package com.zodiac.ams.charter.tests.epg.get;

import com.dob.tests.epg.verifier.EpgVerifier;
import com.dob.tests.epg.verifier.Verifier;
import com.dob.tests.epg.verifier.error.EpgError;
import com.zodiac.ams.charter.checkers.EpgDataChecker;
import com.zodiac.ams.charter.emuls.verifiers.epg.ConfigEpgVerifier;
import com.zodiac.ams.charter.helpers.epg.EpgCsvHelper;
import com.zodiac.ams.charter.tests.epg.StepsForEpg;
import com.zodiac.ams.charter.tomcat.command.TomcatHelper;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static java.lang.Thread.sleep;

public class StepsForGetEpg extends StepsForEpg {

    protected EpgDataChecker epgDataChecker = new EpgDataChecker();
    protected EpgCsvHelper epgCsvHelper = new EpgCsvHelper();
    protected int amountRows = 24;

    @Step
    protected void copyZDB(List<String> zdbFiles) {
        TomcatHelper.copyZdbFromDncsClientToEpgDataFolder(zdbFiles);
    }

    @Step
    protected List<String> runEPGVerifier() {
        Logger.info("EpgVerifier run");
        Verifier epgVerifier = null;
        try {
            epgVerifier = new EpgVerifier(new ConfigEpgVerifier().getConfig());
        } catch (Exception e) {
            e.printStackTrace();
        }
        epgVerifier.verify();
        List<EpgError> report = epgVerifier.getEpgErrors();
        if (report.size() != 0)
            for (EpgError error : report) {
                Logger.info("REPORT " + error);
            }
        return epgDataChecker.readEpgVerifierLog();
    }

    @BeforeMethod
    protected void beforeTest() {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cleanDB();
        runCHEEpg();
        startTestTime = getCurrentTimeTest();
        Logger.info("Start time: " + startTestTime);
    }

    @AfterMethod
    protected void afterTest() {
        stopCHEEpg();
     //   deleteEPGData();
    }
}
