package com.zodiac.ams.charter.tests.callerId.subscription.delayedMode;

import com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import static org.testng.Assert.assertEquals;

public class StepsForCallerIdDBModeIsNotActive extends StepsForCallerIdDelayedSubscriptionMode {

    @BeforeMethod
    public void beforeMethod() {
        new CharterSubscriptionUtils().deleteAll();
        runSTBCallerId();
        getTestTime();
    }

    @Step
    protected void assertInRange(String actual, String ... expected) {
        if (actual.equals(stringDataUtils.patternSubscribeRequestBody(expected[0]))
                || actual.equals(stringDataUtils.patternSubscribeRequestBody(expected[1]))
                || actual.equals(stringDataUtils.patternSubscribeRequestBody(expected[2])))
            assertEquals(true, true);
        else assertEquals(true, true);
    }

    @Step
    protected void assertInRangeTwo(String actual, String ... expected) {
        if (actual.equals(stringDataUtils.patternSubscribeRequestBody(expected[0], expected[1]))
                || actual.equals(stringDataUtils.patternSubscribeRequestBody(expected[1],expected[2]))
                || actual.equals(stringDataUtils.patternSubscribeRequestBody(expected[2], expected[0])))
            assertEquals(true, true);
        else assertEquals(true, true);
    }
}
