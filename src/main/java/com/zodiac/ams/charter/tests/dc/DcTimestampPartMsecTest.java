package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC timestampPartMsec.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcTimestampPartMsecTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIMESTAMP_PART_MSEC_NEGATIVE + "1 - Check without timestampPartMsec")
    @Description("4.010.1 [DC][timestampPartMsec] Check without timestampPartMsec" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><><0><10057|0|50>}]")
    public void sendMessageWithoutTimestampPartMsec() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                // timestampPartMsec must be null, this is the test purpose!
                .setTimestampPartMsec(null)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIMESTAMP_PART_MSEC_NEGATIVE + "2 - Check with incorrect timestampPartMsec")
    @Description("4.010.2 [DC][timestampPartMsec] Check with incorrect timestampPartMsec" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><ertert><0><10057|0|50>}]")
    public void sendMessageWithIncorrectTimestampPartMsec1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                // timestampPartMsec must be garbage, this is the test purpose!
                .setTimestampPartMsec("ertert")
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIMESTAMP_PART_MSEC_NEGATIVE + "2 - Check with incorrect timestampPartMsec")
    @Description("4.010.2 [DC][timestampPartMsec] Check with incorrect timestampPartMsec" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta>< ><0><10057|0|50>}]")
    public void sendMessageWithIncorrectTimestampPartMsec2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                // timestampPartMsec must be garbage, this is the test purpose!
                .setTimestampPartMsec(" ")
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIMESTAMP_PART_MSEC_NEGATIVE + "2 - Check with incorrect timestampPartMsec")
    @Description("4.010.2 [DC][timestampPartMsec] Check with incorrect timestampPartMsec" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><#@$%@%><0><10057|0|50>}]")
    public void sendMessageWithIncorrectTimestampPartMsec3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                // timestampPartMsec must be garbage, this is the test purpose!
                .setTimestampPartMsec("#@$%@%")
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

}
