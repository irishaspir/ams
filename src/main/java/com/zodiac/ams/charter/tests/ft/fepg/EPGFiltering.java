package com.zodiac.ams.charter.tests.ft.fepg;

import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.handlers.EPGFilteringDataHandler;
import com.dob.test.charter.iface.HEHandlerType;
import com.dob.test.charter.iface.IHEDataConsumer;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.fepg.EPGHelper;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;
import static com.zodiac.ams.charter.tests.FeatureList.EPG_FILTERING;

/**
 *
 * @author alexander.filippov
 */
public class EPGFiltering extends FuncTest{
    
    private static final String EPG_DATA_DIR="dataFEPG";
    
    private static String getEPGName(String name){
        return getFileFromJar(EPG_DATA_DIR,name);
    }
    
    
    private class EPGDataConsumer implements IHEDataConsumer{
        private final Semaphore semaphore;
        
        EPGDataConsumer(Semaphore semaphore){
            this.semaphore = semaphore;
        }
        
        @Override
        public void dataReceived(URI uri, byte[] data) {   
            if (null != data && 1 == data.length && EPGFilteringDataHandler.STOP_FLAG == data[0]) {
                semaphore.release();
            }
        }
    }
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.addEPGService("http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_EPG_FILTERING_CONTEXT,
                properties.getEPGController());
        helper.save("server.xml");
    }
    
    @Step("Prepare EPG file. name={0} empty={1}")
    private void prepareEPGFile(String name,boolean empty){
        logger.info("Prepare EPG file ...");
        if (null == name)
            name=properties.getEPGFile();
        String epg_name="epg.txt";
        String controller=properties.getEPGController();
        if (null != controller && !controller.trim().equals(""))
            epg_name="epg_"+controller+".txt";
        File epg=empty ? EPGHelper.createEmptyEPGFile(epg_name): 
            EPGHelper.createEPGFile(name, epg_name, properties.getEPGPeriod(),properties.getEPGMaxRecords());
        Assert.assertTrue(null != epg,"Unable to create EPG file");
    }
    
    @Step("Create CH Emulator")
    private LighweightCHEemulator createEmulator(boolean zipped){
        logger.info("Create Emulator ...");
        CHEmulatorHelper helper=new CHEmulatorHelper();
        helper.setPort(properties.getEmulatorPort());
        helper.setZipped(zipped);
        helper.setEPGFile("epg.txt");
        LighweightCHEemulator emulator=helper.createEmulator();
        Assert.assertTrue(null != emulator,"Unable to create emulator");
        return emulator;
    }
    
    @Step("Start CH Emulator")
    private void startEmulator(LighweightCHEemulator emulator){
        emulator.start();
    }
    
    
    private byte[] readFile(String name){
        try {
            return Files.readAllBytes(new File(name).toPath());
        }
        catch(IOException ex){
            logger.info(ex.getMessage());
            return null;
        }
    }
    
    //@Attachment(value = "Local EPG file", type = "text/csv")
    @Attachment(value = "Local EPG file", type = "text/plain")
    private byte[] getLocalEPGAttachment(String name){
        return readFile(name);
    }
    
    @Attachment(value = "Local EPG zipped file", type = "application/x-gzip")
    private byte[] getLocalEPGZippedAttachment(String name){
        return readFile(name);
    }
    
    //@Attachment(value = "Remote EPG file", type = "text/csv")
    @Attachment(value = "Remote EPG file", type = "text/plain")
    private byte[] getEPGRemoteAttachment(String name){
        return readFile(name);
    }
        
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send EPG file to AMS")
    @Test()
    public void EPGTest(){
        TestImpl(false,false);
    }
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send EPG file with header to AMS")
    @Test()
    public void EPGTestWithHeader(){
        TestImpl(getEPGName("epg.csv"),false,false);
    }
    
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send empty EPG file to AMS")
    @Test()
    public void EmptyEPGTest(){
        TestImpl(true,false);
    }
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send empty EPG file with header to AMS")
    @Test()
    public void EmptyEPGTestWithHeader(){
        TestImpl(getEPGName("epg_empty.csv"),false,false);
    }
    
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send zipped EPG file to AMS")
    @Test()
    public void ZippedEPGTest(){
        TestImpl(false,true);
    }
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send zipped EPG file with header to AMS")
    @Test()
    public void ZippedEPGTestWithHeader(){
        TestImpl(getEPGName("epg.csv"),false,true);
    }
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send empty zipped EPG file to AMS")
    @Test()
    public void EmptyZippedEPGTest(){
        TestImpl(true,true);
    }
    
    
    @Features(EPG_FILTERING)
    @Stories("Positive")
    @Description("Send empty zipped EPG file with header to AMS")
    @Test()
    public void EmptyZippedEPGTestWithHeader(){
        TestImpl(getEPGName("epg_empty.csv"),false,true);
    }
    
    
    
    private void TestImpl(boolean empty,boolean zipped){
        TestImpl(getEPGName(properties.getEPGFile()),empty,zipped);
    }
    
    private void TestImpl(String name,boolean empty,boolean zipped){
        Semaphore semaphore=new Semaphore(0);
        prepareEPGFile(name,empty);
        LighweightCHEemulator emulator=createEmulator(zipped);
        emulator.registerDataConsumer(new EPGDataConsumer(semaphore), HEHandlerType.EPG);
        startEmulator(emulator);
        
        Thread th=new Thread(){
            public void run(){
                try{
                    FTHttpUtils.doGet(properties.getEPGUri());
                }
                catch(Exception ex){
                    logger.error(ex.getMessage());
                }
            }
        };
        th.start();
        
        try {
            Assert.assertTrue(semaphore.tryAcquire(1,TimeUnit.MINUTES),"Semaphore must be acquired");
            th.join();
        }
        catch(InterruptedException ex){
            logger.error(ex.getMessage());
        }
         
        emulator.stop();
        Assert.assertTrue(executor.getFile("ams_filtered_result",properties.getAMSEpgFile()),"Unable to get AMS EPG file");
        if (zipped)
            getLocalEPGZippedAttachment("filtered_result");
        else
            getLocalEPGAttachment("filtered_result");
        getEPGRemoteAttachment("ams_filtered_result");
        Assert.assertTrue(FTUtils.contentEqual("filtered_result", zipped, "ams_filtered_result", false),"The files are different");
    }
    
}