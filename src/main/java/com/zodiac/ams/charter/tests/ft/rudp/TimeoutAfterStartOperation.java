package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.AMS_SESSION_TIMEOUT;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import org.jboss.netty.channel.ChannelFuture;

public class TimeoutAfterStartOperation extends ErrorOperation{
    
    TimeoutAfterStartOperation(){
        super(ErrorCodeType.FILEID);
    }
    
    @Override
    public boolean isSuccessAfterStart(int sessionId,ChannelFuture future) throws Exception{
        if (!getExpectedSuccessResult(sessionId,future))
            return false;
        Thread.sleep(AMS_SESSION_TIMEOUT+15_0000);
        return true;
    }
    
    @Override
    public boolean isSuccessAfterData(int sessionId,ChannelFuture future)  throws Exception {
        return getExpectedFailureResult(sessionId,future);
    }

    @Override
    public boolean isSuccessAfterSegment(int sessionId,ChannelFuture future, int segment, int index, int total)  throws Exception {
        return getExpectedFailureResult(sessionId,future);
    }
    
}
