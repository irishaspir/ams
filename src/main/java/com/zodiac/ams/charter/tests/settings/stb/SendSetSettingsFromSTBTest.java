package com.zodiac.ams.charter.tests.settings.stb;


import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.DataSettingsHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getAllKeysWithout65And62KeysFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getKeysFromSettingsSTBDataV2;
import static com.zodiac.ams.charter.http.helpers.settings.UrlHelper.setUriPattern;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.*;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_STB;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendSetSettingsFromSTBTest extends SetSettingsBeforeAfter {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_STB)
    @Description("1.3.1 Send setSettings from STB to AMS, AMS send setting to CHE, receive success, mf=1  ")
    @TestCaseId("12345")
    @Test(dataProvider = "allSettingsList", dataProviderClass = DataSettingsHelper.class)
    public void sendSetSettingsReceiveSettingIsSuccess( String name,SettingsOption setting) {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOption(setting)
                .build();
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), urlIsIncorrect);
        assertJsonEquals(cheEmulatorSettings.checkData(), jsonDataUtils.amsSetSettingsRequest(options, getDeviceIdByMac(mac)));
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getKeysFromSettingsSTBDataV2(options, mac), allKeys(options), wrongValueInDB);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_STB)
    @Description("1.3.1 Send setSettings from STB to AMS, AMS send settings to CHE, receive success")
    @Test()
    public void sendSetSettingsReceiveSettingsListAreSuccess() {
        String mac = MAC_NUMBER_2;
        delete62Key(options);
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), urlIsIncorrect);
        assertJsonEquals(cheEmulatorSettings.checkData(), jsonDataUtils.amsSetSettingsRequest(options, getDeviceIdByMac(mac)));
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getAllKeysWithout65And62KeysFromSettingsSTBDataV2(mac), allKeys(options), wrongValueInDB);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + SET_SETTINGS_FROM_STB)
    @Description("1.3.4 Send setSettings from STB to AMS, AMS send settings to CHE, settings exist in BD AMS, " +
            "settings is different on STB, successful merge, migration flag = 1")
    @Test()
    public void sendSetSettingsCheckSTBSettingInDB() {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(DEFAULT_AUDIO)
                .addOptions(CC_TEXT_COLOR)
                .addOptions(CHANNEL_BAR_DURATION)
                .addOptions(DVS)
                .addOptions(HD_AUTO_TUNE)
                .build();
        changeKeyValue(options, mac);
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), urlIsIncorrect);
        assertJsonEquals(jsonDataUtils.amsSetSettingsRequest(options, getDeviceIdByMac(mac)), cheEmulatorSettings.checkData());
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getKeysFromSettingsSTBDataV2(options, mac), allKeys(options), wrongValueInDB);
    }


    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + SET_SETTINGS_FROM_STB)
    @Description("1.3.4 Send setSetting from STB to AMS, AMS send settings to CHE, setting exist in BD AMS, " +
            "check CC_LANGUAGE Range,  migration flag = 1")
    @Test(dataProvider = "ccLanguageRange", dataProviderClass = DataSettingsHelper.class)
    public void sendSetSettingsCheckCcLanguageRange(int newValue) {
        String mac = MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOptions(CC_LANGUAGE)
                .build();
        ArrayList<String> response = settingsRudpHelper.sendSetSettings(mac, options, newValue);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbResponsePatternMethod1Ok(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), setUriPattern(), urlIsIncorrect);
        assertJsonEquals(jsonDataUtils.amsSetSettingsRequest(options, getDeviceIdByMac(mac), newValue), cheEmulatorSettings.checkData());
        createAttachment(cheEmulatorSettings.getJson());
        assertEquals(getKeysFromSettingsSTBDataV2(options, mac), allKeys(options, newValue), wrongValueInDB);
    }


}