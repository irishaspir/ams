package com.zodiac.ams.charter.tests.ft.defent;

public interface IResponseVerifier {
    void verify(String response);
    
}
