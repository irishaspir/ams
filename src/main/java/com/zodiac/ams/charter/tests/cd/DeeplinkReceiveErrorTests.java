package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.rudp.cd.message.CDRequestBuider;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.*;
import static com.zodiac.ams.charter.tests.StoriesList.DEEPLINK;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

/**
 * Created by SpiridonovaIM on 28.07.2017.
 */
public class DeeplinkReceiveErrorTests extends AssertLog {

    protected CDRequestBuider cdRequestBuider = new CDRequestBuider();
    protected List<CDProtocolOption> message;


    @BeforeMethod
    public void checkDataInBD() {
        startTestTime = getCurrentTimeTest();
    }


    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DEEPLINK)
    @Description("10.5.2 Message from device to AMS where <request id>=5, command is specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "deeplinkPositive", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendKeyPressReceiveError(String command) {
        runSTB(DEEPLINK_REQUEST_ID, ERROR_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createDeeplinkMessage(command);
        cdRudpHelper.sendRequest(deviceId, message, DEEPLINK_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertDeeplink(DEEPLINK_REQUEST_ID, command);
        assertErrorLog(log, deviceId, DEEPLINK_REQUEST_ID, ERROR_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DEEPLINK)
    @Description("10.5.3 Message from device to AMS where <request id>=5, command is specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "deeplinkPositive", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendKeyPressReceiveIncorrectParameter(String command) {
        runSTB(DEEPLINK_REQUEST_ID, INCORRECT_PARAMETER_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createDeeplinkMessage(command);
        cdRudpHelper.sendRequest(deviceId, message, DEEPLINK_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertDeeplink(DEEPLINK_REQUEST_ID, command);
        assertErrorLog(log, deviceId, DEEPLINK_REQUEST_ID, INCORRECT_PARAMETER_CD_ID, PARAMS);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DEEPLINK)
    @Description("10.5.4 Message from device to AMS where <request id>=5, command is specified  ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "deeplinkPositive", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendKeyPressReceiveStandByMode(String command) {
        runSTB(DEEPLINK_REQUEST_ID, STB_IN_STANDBY_MODE_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createDeeplinkMessage(command);
        cdRudpHelper.sendRequest(deviceId, message, DEEPLINK_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertDeeplink(DEEPLINK_REQUEST_ID, command);
        assertErrorLog(log, deviceId, DEEPLINK_REQUEST_ID, STB_IN_STANDBY_MODE_CD_ID, PARAMS);
    }

    @AfterMethod
    public void stop() {
        stopSTB();
    }

}
