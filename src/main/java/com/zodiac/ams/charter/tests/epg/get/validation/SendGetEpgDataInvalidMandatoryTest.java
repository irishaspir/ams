package com.zodiac.ams.charter.tests.epg.get.validation;

import com.zodiac.ams.charter.helpers.epg.DataForCsvHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataInvalidMandatoryTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.4 Send Get EPG data, receive file with out permanent columns")
    @TestCaseId("21212")
    @Test()
    public void sendGetEpgDataReceiveFileWithOutPermanentColumns() {
        String csv = epgCsvHelper.createInvalidCsvWithoutPermanent();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternDummy(logEpg,
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(),  zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternReturnZdb(), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsDummy());
     }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.2 Send Get EPG data, receive file with invalid or empty SERVICE_ID, AMS write error ")
    @TestCaseId("21212")
    @Test (dataProvider = "invalidStringProperty", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithEmptyServiceId(String incorrectServiceId) {
        String csv = epgCsvHelper.createInvalidCsvIncorrectServiceId(incorrectServiceId);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidServiceId(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder(), incorrectServiceId),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternReturnZdb(), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),  epgCsvHelper.expectedCsvWithSegmentsDummy());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.1 Send Get EPG data, receive file with invalid or empty DATE, AMS write error ")
    @TestCaseId("21212")
    @Test (dataProvider = "invalidStringProperty", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithIncorrectDate(String incorrectDate) {
        String csv = epgCsvHelper.createInvalidCsvIncorrectDate(incorrectDate);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidDate(epgLog.getLogAMSForInvalidDate(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder(),incorrectDate),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternReturnZdb(), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsDummy());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.3 Send Get EPG data, receive file with invalid or empty TIME, AMS write error ")
    @TestCaseId("21212")
    @Test (dataProvider = "invalidStringProperty", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithIncorrectTime(String incorrectTime) {
        String csv = epgCsvHelper.createInvalidCsvIncorrectTime(incorrectTime);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidTime(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder(), incorrectTime),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternReturnZdb(), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),  epgCsvHelper.expectedCsvWithSegmentsDummy());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.21, 2.1.22 Send Get EPG data, receive file with invalid or empty DURATION, AMS write error ")
    @TestCaseId("21212")
    @Test (dataProvider = "invalidStringProperty", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithIncorrectDuration(String incorrectDuration) {
        String csv = epgCsvHelper.createInvalidCsvIncorrectDuration(incorrectDuration);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidDuration(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(),  zdbDataUtisl.epgLastFolder(), incorrectDuration),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternReturnZdb(), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),  epgCsvHelper.expectedCsvWithSegmentsDummy());
    }

 }