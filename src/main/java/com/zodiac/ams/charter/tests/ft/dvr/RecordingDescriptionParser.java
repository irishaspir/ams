package com.zodiac.ams.charter.tests.ft.dvr;

import com.zodiac.ams.charter.services.ft.dvr.RecordingParam;
import com.zodiac.ams.charter.services.ft.dvr.RequestRecording;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecordingDescriptionParser {
    
    private static final String SERIES = "Series";
    private static final String SINGLE = "Single";
    private static final String MANUAL = "Manual";
    
    private static final String SPACE_IS_NEEDED = "Space is needed for new recordings";
    private static final String USER_DELETE = "User delete:";
    
    private static final String HD = "HD";
    private static final String SD = "SD";
    
    private static final String NEW_EPISODES = "New episodes only";
    private static final String ALL_EPISODES = "All episodes";
    
    private static final String WITH_DUPLICATES = "With duplicates";
    private static final String WITHOUT_DUPLICATES = "Without duplicates";
    
    private static final String SERVICE_INVALID = "serviceId is invalid";
    private static final String SERVICE_EMPTY = "serviceId is empty";
    private static final String SERVICE_MISSING = "serviceId is missing";

    private static final String CHANNEL_INVALID = "channelNumber is invalid";
    private static final String CHANNEL_EMPTY = "channelNumber is empty";
    private static final String CHANNEL_MISSING = "channelNumber is missing";

    private static final String START_TIME_INVALID = "startTime is invalid";
    private static final String START_TIME_EMPTY  = "startTime is empty";
    private static final String START_TIME_MISSING = "startTime is missing";

    private static final String DURATION_INVALID = "duration is invalid";
    private static final String DURATION_EMPTY = "duration is empty";
    private static final String DURATION_MISSING = "duration is missing";

    private static final String START_OFF_INVALID = "startOffset is invalid";
    private static final String START_OFF_EMPTY = "startOffset is empty";
    private static final String START_OFF_MISSING = "startOffset is missing";

    private static final String END_OFF_INVALID = "endOffset is invalid";
    private static final String END_OFF_EMPTY = "endOffset is empty";
    private static final String END_OFF_MISSING = "endOffset is missing";
    
    private static boolean matches(String description, String template){
        return Pattern.compile(".*" + template + "\\s*(:|\\]|$).*", Pattern.CASE_INSENSITIVE).matcher(description).matches();
    }
    
    //....[ key : value]....
    public static String getValue(String description, String key){
        Pattern pattern = Pattern.compile(".*\\s*\\[\\s*" + key + "\\s*:\\s*([^\\s\\]]+)\\s*\\].*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(description);
        return matcher.matches() ? matcher.group(1) : null;
    }
    
//    public static void setModify(ModifyRequest request, String description){
//        Type type = Type.fromString(getValue(description, "Type"));
//        Assert.assertTrue(null != type, "type must not be null");
//        
//        YesNo wholeSetFlag = YesNo.fromString(getValue(description, "wholeSetFlag"));
//        Assert.assertTrue(null != type, "wholeSetFlag must not be null");
//        
//        request.setType(type);
//        request.setWholeSetFlag(wholeSetFlag);
//    }
    
    
    private static void setService(RecordingParam param, String description){
        if (matches(description, SERVICE_INVALID))
            param.setServiceId("test");
        else
        if (matches(description, SERVICE_EMPTY))
            param.setServiceId("");
        else
        if (matches(description, SERVICE_MISSING))
            param.setServiceId(null);
    }
    
    private static void setChannel(RecordingParam param, String description){
        if (matches(description, CHANNEL_INVALID))
            param.setChannelNumber("test");
        else
        if (matches(description, CHANNEL_EMPTY))
            param.setChannelNumber("");
        else
        if (matches(description, CHANNEL_MISSING))
            param.setChannelNumber(null);
    }
    
    private static void setStartTime(RecordingParam param, String description){
        if (matches(description, START_TIME_INVALID))
            param.setStartTime("test");
        else
        if (matches(description, START_TIME_EMPTY))
            param.setStartTime("");
        else
        if (matches(description, START_TIME_MISSING))
            param.setStartTime(null);
    }
    
    private static void setDuration(RecordingParam param, String description){
        if (matches(description, DURATION_INVALID))
            param.setDuration("test");
        else
        if (matches(description, DURATION_EMPTY))
            param.setDuration("");
        else
        if (matches(description, DURATION_MISSING))
            param.setDuration(null);
    }
    
    private static void setStartOff(RecordingParam param, String description){
        if (matches(description, START_OFF_INVALID))
            param.setStartOffset("test");
        else
        if (matches(description, START_OFF_EMPTY))
            param.setStartOffset("");
        else
        if (matches(description, START_OFF_MISSING))
            param.setStartOffset(null);
    }
    
    private static void setEndOff(RecordingParam param, String description){
        if (matches(description, END_OFF_INVALID))
            param.setEndOffset("test");
        else
        if (matches(description, END_OFF_EMPTY))
            param.setEndOffset("");
        else
        if (matches(description, END_OFF_MISSING))
            param.setEndOffset(null);
    }
    
    
    static RequestRecording getRequestRecording(String deviceId, String description) throws Exception{
        return getRequestRecording(deviceId, description, true);
    }
    
    static RequestRecording getRequestRecording(String deviceId, String description, boolean suppressException) throws Exception{
        description = description.toUpperCase();
        
        RequestRecording request = new RequestRecording(deviceId, Method.SCHEDULE);
        
        RecordingParam param = null;
        
        if (matches(description, SERIES))
            param = RecordingParam.getRandom(Type.Series);
        else
        if (matches(description, SINGLE))
            param = RecordingParam.getRandom(Type.Single);
        else
        if (matches(description, MANUAL))
            param = RecordingParam.getRandom(Type.Manual);
        else
        if (!suppressException)
            throw new Exception("Invalid Type!");
        
        if (matches(description, SPACE_IS_NEEDED))
            param.setSaveDays(SaveDays.SPACE_IS_NEEDED);
        else
        if (matches(description, USER_DELETE))
            param.setSaveDays(SaveDays.USER_DELETE);
        else
        if (!suppressException)
            throw new Exception("Invalid SaveType!");
        
        
        if (matches(description, SD))
            param.setRecordingFormat(RecordingFormat.SD);
        else
        if (matches(description, HD))
            param.setRecordingFormat(RecordingFormat.HD);
        else
        if (!suppressException)
            throw new Exception("Invalid RecordingFormat!");
        
        
        if (matches(description, NEW_EPISODES))
            param.setEpisodesDefinition(EpisodesDefinition.NEW);
        else
        if (matches(description, ALL_EPISODES))
            param.setEpisodesDefinition(EpisodesDefinition.ALL);
        else
        if (!suppressException)
            throw new Exception("Invalid EpisodesDefinition!");
        
        
        if (matches(description, WITH_DUPLICATES))
            param.setRecordDuplicates(YesNo.Yes);
        else
        if (matches(description, WITHOUT_DUPLICATES))    
            param.setRecordDuplicates(YesNo.No);
        else
        if (!suppressException)
            throw new Exception("Invalid RecordDuplicates!");
        
        if (Type.Manual == param.getType()){
            WeekDays [] wd = WeekDays.fromDescription(description);
            if (0 == wd.length && !suppressException)
                throw new Exception("Invalid Repeat!");
            param.setRepeat(wd);
        }
        
        setService(param, description);
        setChannel(param, description);
        setStartTime(param, description);
        setDuration(param, description);
        setStartOff(param, description); 
        setEndOff(param, description);        
                
        request.addParam(param);
        return request;        
    }
    
}
