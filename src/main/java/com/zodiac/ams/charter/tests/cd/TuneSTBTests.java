package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.TUNE_STB_REQUEST_ID;
import static com.zodiac.ams.charter.tests.StoriesList.TUNE_STB;

public class TuneSTBTests extends CDBeforAfter {
    
    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.NEGATIVE + TUNE_STB)
    @Description("10.1.2 Message from device to AMS where <request id>=1, chanelNum and callsign aren't specified ")
    @TestCaseId("")
    @Issue("AMSTS17-41")
    @Test(priority = 1)
    public void sendTuneSTBReceiveError() {
        String deviceId = DEVICE_ID_NUMBER_1;
        cdRudpHelper.sendRequest(deviceId, TUNE_STB_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        byte[] data = cdstbEmulator.getData();
        assertLog(log, deviceId);
    }

    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + TUNE_STB)
    @Description("10.1.1 Message from device to AMS where <request id>=1, chanelNum and callsing are specified ")
    @TestCaseId("171347")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "chanelNumCallSign", dataProviderClass = CDDataProvider.class, priority = 3)
    public void sendTuneSTBReceiveSuccessChannelNumCallSignAre(int chanelNum, String calSign) {
        String mac = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createTuneSTBMessage(chanelNum, calSign);
        cdRudpHelper.sendRequest(mac, message, TUNE_STB_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertTuneSTB(TUNE_STB_REQUEST_ID, chanelNum, calSign);
    }
}
