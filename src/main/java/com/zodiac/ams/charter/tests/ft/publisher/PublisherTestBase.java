package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.services.ft.publisher.PublishTask;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import com.zodiac.ams.charter.tests.ft.IExcludeProvider;
import com.zodiac.ams.charter.tests.ft.IStoryProvider;
import static com.zodiac.ams.charter.tests.ft.publisher.PublisherTestImpl.ADD_MODIFY_NO_VER_INC_INC_VERSION_EXISTING;
import com.zodiac.ams.charter.tests.ft.IDescriptionProvider;

public class PublisherTestBase extends PublisherTestImpl implements IExcludeProvider, IStoryProvider, IDescriptionProvider{
    @Override
    public boolean isExcluded(String test) {
        return false;
    }

    @Override
    public String getStory() {
        return "Publisher";
    }

    @Override
    public String getDescription(String test, String oldDescription) {
        return oldDescription;
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Add new file on carousel. Expected result: file is added, record is added to z_dts.ini")
    @Test(priority=1)
    @Override
    public void addNewFileOnCarousel() throws Exception{
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addNewFileOnCarousel();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Add new file on carousel. No flags in tasks.zip. Expected result: server returns error 500")
    @Test(priority=1)
    @Override
    public void addNewFileOnCarouselNoFlags() throws Exception{
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.addNewFileOnCarouselNoFlags();
        verifyRootZdts(root, carouselsOut, vers, true/*strictCondition*/);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Add file on carousel.The file already exists. Expected result: file and record in z_dts.ini are not changed")
    @Test(priority=2)
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        //add +1 add- failed
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addExistingFileOnCarousel();
        verifyRootZdts(root, carouselsOut, getCommonVersion()/*vers, false*/);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Delete existing file from carousel. Expected result: file and record in z_dts.ini are deleted")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarousel() throws Exception{
        //add +1 delete +1
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.deleteFileFromCarousel();
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Delete existing file from carousel. Invalid carousel name is set in tasks.zip. Expected result: error message is returned")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.deleteFileFromCarouselInvalidCarousel();    
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Delete existing file from carousel. Invalid name is set in tasks.zip. Expected result: error message is returned")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarouselInvalidName() throws Exception {
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.deleteFileFromCarouselInvalidName();
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Delete file from carousel. The file and record in z_dts.ini don't exist. Expected result: records in z_dts.ini aren't changed")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.deleteFileFromCarouselNoFileNoRecord();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Delete file from carousel. The record in z_dts.ini exists, but file doesn't exist. Expected result: record in z_dts.ini is deleted")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarouselNoFile() throws Exception {
        //add +1 delete +1 /* - error*/
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.deleteFileFromCarouselNoFile(); 
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Delete file from carousel. The record in z_dts.ini doesn't exist, but file exists. Expected result: file and z_dts.ini aren't changed")
    @Test(priority=3)
    @Override
    public void deleteFileFromCarouselNoRecord() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.deleteFileFromCarouselNoRecord();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
     
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform Update operation. The file already exists on carousel.Expected result: file is updated, record in z_dts.ini is incremented.")
    @Test(priority=4)
    @Override
    public void updateExisting() throws Exception {
        //add+1 update+1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.updateExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform Update operation. Non-existent carousel is specified in tasks.zip. Expected result: file is not updated, error message is returned")
    @Test(priority=4)
    @Override
    public void updateExistingInvalidCarousel() throws Exception {
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.updateExistingInvalidCarousel();
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform Update operation. Non-existent name is specified in tasks.zip. Expected result: file is not updated, error message is returned ")
    @Test(priority=4)
    @Override
    public void updateExistingInvalidName() throws Exception {
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.updateExistingInvalidName();
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform Update operation. The file doesn't exists  on carousel. Expected result: file is not added, z_dts.ini is not changed.")
    @Test(priority=5)
    @Override
    public void updateNoExisting() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.updateNoExisting();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY operation.The file already exists on carousel. Expected result: file is updated, record in z_dts.ini is incremented.")
    @Test(priority=6)
    @Override
    public void addModifyExisting() throws Exception {
        //add+1 add_modify+1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.addModifyExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY operation.The file doesn't exist on carousel. Expected result: file is added, record is added to z_dts.ini.")
    @Test(priority=7)
    @Override
    public void addModifyNoExisting() throws Exception {
        //add_modify +1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addModifyNoExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY operation. The file doesn't exist on carousel. No flags are set in tasks.zip. Expected result: server returns error 500")
    @Test(priority=7)
    @Override
    public void addModifyNoExistingNoFlags() throws Exception{
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.addModifyNoExistingNoFlags();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY_NO_VER_INC operation.The file already exists on carousel. Expected result: file is updated, z_dts.ini is not updated.")
    @Test(priority=8)
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        //add+1 add_modify_no_ver_inc +1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.addModifyNoVerIncExisting();
        verifyRootZdts(root, carouselsOut, this.getCommonVersion(), true /*set CDN strict condition = false */);
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY_NO_VER_INC operation.The file doesn't exist on carousel. Expected result: file is added, record is added to z_dts.ini.")
    @Test(priority=9)
    @Override
    public void addModifyNoVerIncNoExisting() throws Exception {
        //add_modify_no_ver_inc +1
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addModifyNoVerIncNoExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform CLEAR_OLD operation.Expected result: all files and records in z_dts.ini conforming given criteria are deleted.")
    @Test(priority=10)
    @IRegExp(
        regExp="^file_(10_[1-3]{1,3})\\.txt$",
        validNames={"file_10_1.txt","file_10_21.txt","file_10_321.txt"},
        invalidNames={"file_10.txt","file_10_9.txt","file_10_51.txt","file_10_521.txt","file_10_1234.txt"})
    @Override
    public void clearOld() throws Exception{
        //add valid+invalid, clear
        long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut), 2);
        super.clearOld();
        verifyRootZdts(root, carouselsOut, vers, false);
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY operation. The record exists in z_dts.ini file but file doesn't exist on carousel."+
        "Expected result: File is added , record in z_dts.ini is incremented.")
    @Test(priority=11)
    @Override
    public void addModifyExistingNoFile() throws Exception {
        //add,addmod
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut), 2);
        super.addModifyExistingNoFile();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform Update operation. The record exists in z_dts.ini file but file doesn't exist on carousel."+
        "Expected result: File is added, record in z_dts.ini is incremented.")
    @Test(priority=12)
    @Override
    public void updateExistingNoFile() throws Exception {
        //add,update
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut), 2);
        super.updateExistingNoFile();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY operation. The record doesn't exist in z_dts.ini file but file exists on carousel."+
        "Expected result: File is updated , record is added to z_dts.ini.")
    @Test(priority=13)
    @Override
    public void addModifyExistingNoRecord() throws Exception {
        //addmodi
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addModifyExistingNoRecord();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform Update operation. The record doesn't exists in z_dts.ini file but file exists on carousel."+
        "Expected result: File is not updated, z_dts.ini is not changed.")
    @Test(priority=14)
    @Override
    public void updateExistingNoRecord() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.updateExistingNoRecord();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform INC_VERSION operation. The file already exists on carousel. Expected result: file is not updated, z_dts.ini is updated.")
    @Test(priority=15)
    @Override
    public void incVersionExisting() throws Exception {
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut),2);
        super.incVersionExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform INC_VERSION operation. The file doesn't exists on carousel. Expected result: file and z_dts.ini are not updated.")
    @Test(priority=16)
    @Override
    public void incVersionNoExisting() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.incVersionNoExisting();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY_NO_VER_INC + INC_VERSION operations in one batch. The file exists on carousel. Expected result: file and z_dts.ini are updated.")
    @Test(priority=80)
    @Override
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addModifyNoVerIncIncVersionExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Perform ADD_MODIFY_NO_VER_INC + INC_VERSION operations in one batch. The file doesn't exists on carousel. Expected result: file and z_dts.ini are updated.")
    @Test(priority=81)
    @Override
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        //long [] vers = incrVersions(getRootZdtsVersion(root,carouselsOut));
        super.addModifyNoVerIncIncVersionNoExisting();
        verifyRootZdts(root, carouselsOut, getCommonVersion());
    }
    
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Tasks.zip contains unknwon action. Expected result: Server returns 500 error.")
    @Test(priority=90)
    @Override
    public void unknownAction() throws Exception{
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.unknownAction();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("Tasks.zip contains server type. Expected result: Server returns 500 error.")
    @Test(priority=91)
    @Override
    public void unknownServer() throws Exception {
        long [] vers = getRootZdtsVersion(root,carouselsOut);
        super.unknownServer();
        verifyRootZdts(root, carouselsOut, vers, true);
    }
    
    @Features("Publisher")
    @Stories("STORIES")
    @Description("PublisherTest AMS log")
    @Test(priority=100)
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
    
}
