package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC first event number.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DcFirstEventNumberTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_EVENT_NUMBER_NEGATIVE + "1 - Check without First event number")
    @Description("4.07.1 [DC][First event number] Check without First event number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithoutFirstEventNumber() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                // first event number must be null, this is the test purpose!
                .setFirstEventNumber(null)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_EVENT_NUMBER_NEGATIVE + "2 - Check with incorrect First event number")
    @Description("4.07.2 [DC][First event number] Check with incorrect First event number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><           ><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectFirstEventNumber1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                // first event number must a long empty string, this is the test purpose!
                .setFirstEventNumber("           ")
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_EVENT_NUMBER_NEGATIVE + "2 - Check with incorrect First event number")
    @Description("4.07.2 [DC][First event number] Check with incorrect First event number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><FetfETt><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectFirstEventNumber2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                // first event number must a garbage string , this is the test purpose!
                .setFirstEventNumber("FetfETt")
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_FIRST_EVENT_NUMBER_NEGATIVE + "2 - Check with incorrect First event number")
    @Description("4.07.2 [DC][First event number] Check with incorrect First event number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><2452345234><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectFirstEventNumber3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                // first event number must a garbage string , this is the test purpose!
                .setFirstEventNumber("2452345234")
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
