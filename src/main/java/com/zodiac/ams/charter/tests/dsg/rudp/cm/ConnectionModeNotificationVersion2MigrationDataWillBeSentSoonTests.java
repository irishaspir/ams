package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.connectionMode;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.vendor;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.migrationDataWillBeSentSoon;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_2_STRING;

/**
 * Created by SpiridonovaIM on 29.06.2017.
 */
public class ConnectionModeNotificationVersion2MigrationDataWillBeSentSoonTests extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_2_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 2 AND <vendor> IS specified " +
            "migrationStart is MigrationDataWillBeSentSoon")
    @Test(priority = 2)
    public void sendVersion2RequestId0MigrationStartIsMigrationDataWillBeSentSoon() {
        message = version2Request.createMessage(mac, String.valueOf(migrationDataWillBeSentSoon));
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        List<String> log = readAMSLog();
        createAttachment(log);
        Logger.info("Response");
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        Logger.info("BD");
        //  assertFalse(isMacInSTBMetadata(mac));
        Logger.info("JSON");
        assertJson(mac,connectionMode, getBoxModel(), vendor);
        Logger.info("Log");
        assertLog(log, mac, connectionMode, "", migrationDataWillBeSentSoon);
    }

}
