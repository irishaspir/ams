package com.zodiac.ams.charter.tests;

public class StoriesList {
    public static final String ENVIRONMENT_PREPARATION = "Environment preparation";
    public static final String NEGATIVE = "[Negative tests] ";
    public static final String POSITIVE = "[Positive tests] ";

    //Settings
    public static final String SET_SETTINGS_FROM_STB = "2 - Send setSettings from STB";
    public static final String SET_SETTINGS_FROM_CHE = "4 - Send setSettings from CHE";
    public static final String SET_SETTINGS_FROM_CHE_CHECK_RANGE = "4 - Send setSettings from CHE and check range";
    public static final String GET_SETTINGS_FROM_CHE = "3 - Send getSettings from CHE ";
    public static final String GET_SETTINGS_FROM_STB = "1 - Send getSettings from STB";
    public static final String DELETE_SETTINGS_FROM_CHE = "5 - Send delete Settings from CHE ";

    //CallerId
    public static final String INCOMING_CALL_NOTIFICATION = " - Send incoming call notification request";
    public static final String CALL_TERMINATION_NOTIFICATION = " - Send call termination notification request";
    public static final String SUBSCRIBE = " - Send subscribe request";
    public static final String UNSUBSCRIBE = " - Send unsubscribe request";
    public static final String DELAYED_SUBSCRIPTION = " in Delayed Subscription mode";
    public static final String REAL_TIME_SUBSCRIPTION = " in Real-Time Subscription mode";

    //ActivityControl
    public static final String GET_STB_STATUS_REQUEST = "1 - Send get StbStatusByMac request";

    //Rollback
    public static final String ROLLBACK_TO_ROVI_FIRMWARE = "1 - \"Rollback To Rovi Firmware\" command";
    public static final String MIGRATION_START_NOTIFICATION = "2 - Send 'migrationStart' notification from STB";

    //EPG
    public static final String VALIDATION_NEGATIVE = "[Validation negative tests] ";
    public static final String VALIDATION_POSITIVE = "[Validation positive tests] ";
    public static final String GET_UPDATE_EPG_DATA = " 2 - Send update EPG cheDataReceived request";
    public static final String GET_EPG_DATA = " 1 - Send get EPG cheDataReceived request";

    //STB Lookup
    public static final String STB_LOOKUP_POSITIVE = " 1 - Send get STB Lookup request";
    public static final String STB_LOOKUP_NEGATIVE = " 2 - Send get STB Lookup request";

    //DSG
    public static final String DSG_CONNECTION_MODE_NOTIFICATION = "Connection Mode Notification";
    public static final String DSG_STB_PARAMETER_UPDATE = "STB parameters update";

    public static final String VERSION_0_STRING = "[Version 0] ";
    public static final String VERSION_1_STRING = "[Version 1] ";
    public static final String VERSION_2_STRING = "[Version 2] ";
    public static final String VERSION_3_STRING = "[Version 3] ";
    public static final String CHECK_SETTINGS = "Check validation of settings";
    public static final String CHECK_SETTINGS_IN_QUEUED = "Check settings are in queued";
    public static final String GET_STB_NETWORK_TYPE_REQUEST = "[HTTP] 1 - Send get StbNetworkType request";
    public static final String GET_STB_POPULATION_DISTRIBUTION_REQUEST = "[HTTP] 2 - Send get StbPopulationDistribution request";

    /*
     * The DC stories.
     */
    public static final String DC_EVENT_0_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".0.";
    public static final String DC_EVENT_0_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".0.";
    public static final String DC_EVENT_1_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".1.";
    public static final String DC_EVENT_1_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".1.";
    public static final String DC_EVENT_2_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".2.";
    public static final String DC_EVENT_2_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".2.";
    public static final String DC_EVENT_3_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".3.";
    public static final String DC_EVENT_3_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".3.";
    public static final String DC_EVENT_4_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".4.";
    public static final String DC_EVENT_4_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".4.";
    public static final String DC_EVENT_5_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".5.";
    public static final String DC_EVENT_5_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".5.";
    public static final String DC_EVENT_6_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".6.";
    public static final String DC_EVENT_6_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".6.";
    public static final String DC_EVENT_11_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".11.";
    public static final String DC_EVENT_11_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".11.";
    public static final String DC_EVENT_12_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".12.";
    public static final String DC_EVENT_12_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".12.";
    public static final String DC_EVENT_ID_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".011.";
    public static final String DC_EVENTS_NUMBER_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".08.";
    public static final String DC_FIRST_CHANNEL_NUMBER_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".06.";
    public static final String DC_FIRST_EVENT_NUMBER_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".07.";
    public static final String DC_GENERAL_CHECKING_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".00.";
    public static final String DC_GENERAL_CHECKING_POSITIVE = POSITIVE + FeatureList.DC_NUMERIC_VALUE + ".00.";
    public static final String DC_HUB_ID_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".02.";
    public static final String DC_MAX_REBOOT_PER_DAY_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".03.";
    public static final String DC_NODE_ID_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".01.";
    public static final String DC_SESSION_START_TIMESTAMP_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".05.";
    public static final String DC_SESSIONS_NUMBER_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".04.";
    public static final String DC_TIME_DELTA_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".09.";
    public static final String DC_TIMESTAMP_PART_MSEC_NEGATIVE = NEGATIVE + FeatureList.DC_NUMERIC_VALUE + ".010.";

    /*
     * CD
    */
    public static final String TUNE_STB = "Tune STB ";
    public static final String KEY_PRESS = "Key press ";
    public static final String DEEPLINK = "Deeplink ";
    public static final String DVR_RECORDING_PLAYBACK_START = "DVR Recording Playback Start ";
    public static final String DVR_RECORDING_TRICKMODE = "DVR Recording Trickmode ";
}
