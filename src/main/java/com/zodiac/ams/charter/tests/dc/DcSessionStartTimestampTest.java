package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC session start timestamp.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DcSessionStartTimestampTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSION_START_TIMESTAMP_NEGATIVE + "1 - Check without Session start timestamp")
    @Description("4.05.1 [DC][Session start timestamp] Check without Session start timestamp" +
            "<Node ID><Hub ID><0><2>" +
            "[<><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithoutSessionStartTimestamp() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                // SessionStartTimestamp must be null, this is the test purpose!
                .setSessionStartTimestamp(null)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSION_START_TIMESTAMP_NEGATIVE + "2 - Check with incorrect Session start timestamp")
    @Description("4.05.2 [DC][Session start timestamp] Check with incorrect Session start timestamp" +
            "<Node ID><Hub ID><0><2>" +
            "[<786457683747234872882784834665723443434><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionStartTimestamp1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                // SessionStartTimestamp must be a garbage string , this is the test purpose!
                .setSessionStartTimestamp("786457683747234872882784834665723443434")
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSION_START_TIMESTAMP_NEGATIVE + "2 - Check with incorrect Session start timestamp")
    @Description("4.05.2 [DC][Session start timestamp] Check with incorrect Session start timestamp" +
            "<Node ID><Hub ID><0><2>" +
            "[<werwerwer><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionStartTimestamp2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                // SessionStartTimestamp must be a garbage string , this is the test purpose!
                .setSessionStartTimestamp("werwerwer")
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_SESSION_START_TIMESTAMP_NEGATIVE + "2 - Check with incorrect Session start timestamp")
    @Description("4.05.2 [DC][Session start timestamp] Check with incorrect Session start timestamp" +
            "<Node ID><Hub ID><0><2>" +
            "[<#$%#%><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionStartTimestamp3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                // SessionStartTimestamp must be a garbage string , this is the test purpose!
                .setSessionStartTimestamp("#$%#%")
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
/**/
    @Stories(StoriesList.DC_SESSION_START_TIMESTAMP_NEGATIVE + "2 - Check with incorrect Session start timestamp")
    @Description("4.05.2 [DC][Session start timestamp] Check with incorrect Session start timestamp" +
            "<Node ID><Hub ID><0><2>" +
            "[<                  ><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectSessionStartTimestamp4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(0)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                // SessionStartTimestamp must be an string with spaces, this is the test purpose!
                .setSessionStartTimestamp("                  ")
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
