package com.zodiac.ams.charter.tests.dsg.rudp.update;

import com.zodiac.ams.charter.emuls.che.settings.SettingsCHEEmulator;
import com.zodiac.ams.charter.emuls.stb.settings.SettingsSTBEmulator;
import com.zodiac.ams.charter.rudp.dsg.message.update.STBParametersUpdateRequestBuilder;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.*;



/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public class STBParametersUpdateSteps extends Steps {

    protected SettingsSTBEmulator settingsSTBEmulator;
    protected STBParametersUpdateRequestBuilder stbParametersUpdateRequestBuilder;
    protected SettingsCHEEmulator settingsCHEEmulator;
    protected List<String> log = new ArrayList<>();
    private static int time = (int) (Math.random() * 3);


    @Step
    protected void runSTB() {
        settingsSTBEmulator = new SettingsSTBEmulator();
        settingsSTBEmulator.registerConsumer();
        settingsSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        stbParametersUpdateRequestBuilder = new STBParametersUpdateRequestBuilder(settingsSTBEmulator);
    }

    @Step
    protected void runSettingsCHE() {
        settingsCHEEmulator = new SettingsCHEEmulator();
        settingsCHEEmulator.registerConsumer();
        settingsCHEEmulator.start();
        Logger.info("CHE Emulator's run");
    }

    @Step
    protected void stopSTB() {
        settingsSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
    }

    @Step
    protected void stopSettingsCHE() {
        settingsCHEEmulator.stop();
        Logger.info("CHE Emulator's stopped");
    }

    @Step
    public static int changeConnectionMode() {
        time = (int) (Math.random() * 3);
        Object[] connectionMode = new Object[]{DOCSIS, ALOHA, DAVIC};
        if (3 == time) {
            time = 0;
        }
        return (int) connectionMode[time];

    }

}
