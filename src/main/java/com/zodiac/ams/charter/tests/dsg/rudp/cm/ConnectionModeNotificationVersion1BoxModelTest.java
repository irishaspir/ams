package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils.isMacInSTBLastActivity;
import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.isMacInMacIp;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.DAVIC;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_1_STRING;
import static org.testng.Assert.assertEquals;

public class ConnectionModeNotificationVersion1BoxModelTest extends DsgBeforeAfter {


    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.3 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified, " +
            "unknown mac, receive ERROR_VERSION3_PROTOCOL CODE = 0 ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test()
    public void sendVersion1RequestId0ConnectionModeUnknownMac() {
        String mac = unknownMac();
        Logger.info("Unknown MAC " + mac);
        message = version1Request.createMessage(mac, connectionMode, boxModel, vendor);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertEquals(true, isMacInMacIp(mac), UNKNOWN + MAC_IS_NOT_IN_MAC_IP);
        assertEquals(true, isMacInSTBLastActivity(mac), CHANGE_TIME_STAMP + AND + LAST_ACTIVITY_STAMP + NOT_EQUAL);
        assertLog(log, Long.parseLong(mac), msgType, connectionMode, boxModel, vendor);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
        assertJson(mac,connectionMode, getBoxModel(), vendor);
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> ISN'T specified,  " +
            "box model changes")
    @TestCaseId("123")
    @Issue("CHRAMS-270/:view/")
    @Test(dataProvider = "box models", dataProviderClass = DSGDataProvider.class, priority = 1)
    public void sendVersion1RequestId0BoxModelChanges(int boxModel, int vendor) {
        connectionMode = changeConnectionMode();
        message = version1Request.createMessage(mac, connectionMode, String.valueOf(boxModel));
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode, boxModel);

    //    assertDB(getModelId(UNKNOWN), vendor, boxModel, mac);
        assertJson(mac,connectionMode, boxModel, (Object) vendor);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified,  " +
            "box model unknown")
    @TestCaseId("123")
    @Issue("CHRAMS-270/:view/")
    @Test(priority = 1)
    public void sendVersion1RequestId0BoxModelUnknown() {
        connectionMode = changeConnectionMode();
        int boxModel = 8975;
        message = version1Request.createMessage(mac, connectionMode, String.valueOf(boxModel));
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode, boxModel);
     //   assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), boxModel, mac);
        assertJson(mac, connectionMode,boxModel, "Unknown");
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified " +
            "romId and VendorId changes")
    @Test(dataProvider = "romId and VendorId", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion1RequestId0VendorIsSpecifiedRomIdChange(int romId, int vendor) {
        connectionMode = DAVIC;
        message = version1Request.createMessage(mac, connectionMode, romId, vendor);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode, romId, vendor);
      //  assertDB(romId, vendor, getModelId(UNKNOWN), mac);
        assertJson(mac,connectionMode, getBoxModel(romId),(Object) vendor);
    }


}
