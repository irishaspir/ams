package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC hub id.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>*
 */
public class DcHubIdTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_HUB_ID_NEGATIVE + "1 - Check without Hub ID")
    @Description("4.02.1 [DC][Hub ID] Check without Hub ID" +
            "<Node ID><><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithoutHubId() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                // HubId must be a null, this is the test purpose!
                .setHubId(null)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_HUB_ID_NEGATIVE + "2 - Check with incorrect Hub ID")
    @Description("4.02.2 [DC][Hub ID] Check with incorrect Hub ID" +
            "<Node ID><      ><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectHubId1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                // HubId must be an long spaces in string, this is the test purpose!
                .setHubId("      ")
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_HUB_ID_NEGATIVE + "2 - Check with incorrect Hub ID")
    @Description("4.02.2 [DC][Hub ID] Check with incorrect Hub ID" +
            "<Node ID><ewrtwetwt><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectHubId2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                // HubId must be a garbage string , this is the test purpose!
                .setHubId("ewrtwetwt")
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_HUB_ID_NEGATIVE + "2 - Check with incorrect Hub ID")
    @Description("4.02.2 [DC][Hub ID] Check with incorrect Hub ID" +
            "<Node ID><@#%@$%><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectHubId3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                // HubId must be a garbage string , this is the test purpose!
                .setHubId("@#%@$%")
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057)
                .addParameter(0)
                .addParameter(50)
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
