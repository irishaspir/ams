package com.zodiac.ams.charter.tests.epg.get.validation.negative;

import com.zodiac.ams.charter.helpers.epg.DataForCsvHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendGetEpgDataInvalidBoolPropertiesTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.22 Send Get EPG data, receive file with invalid DVS, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidDvs() {
        String csv = epgCsvHelper.createCsvWithInvalidDvs();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidDvs(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidDvs());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.17 Send Get EPG data, receive file with invalid CC, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidCc() {
        String csv = epgCsvHelper.createCsvWithInvalidCc();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidCc(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidCc());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.36, 2.1.37, 2.1.38 Send Get EPG data, receive file with invalid values NEW, AMS write error ")
    @TestCaseId("21212")
    @Test (dataProvider = "dataForInvalidNew", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithInvalidNew(String valueRerun, String valueNew) {
        String csv = epgCsvHelper.createCsvInvalidNew(valueRerun);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidNew(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidNew(valueRerun, valueNew));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.36, 2.1.37, 2.1.38 Send Get EPG data, receive file with invalid values NEW if RERUN is invalid AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidNewInvalidRerun() {
        String csv = epgCsvHelper.createCsvInvalidNewInvalidRerun();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidNewInvalidRerun(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate (DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidNewInvalidRerun());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.40 Send Get EPG data, receive file with invalid SAP, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidSap(){
        String csv = epgCsvHelper.createCsvWithInvalidSap();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidSap(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidSap());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.47 Send Get EPG data, receive file with invalid SURROUND, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidSurround() {
        String csv = epgCsvHelper.createCsvWithInvalidSurround();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidSurround(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()), PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidSurround());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.46 Send Get EPG data, receive file with invalid STEREO, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidStereo() {
        String csv = epgCsvHelper.createCsvWithInvalidStereo();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidStereo(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidStereo());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.70 Send Get EPG data, receive file with invalid RERUN, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidRerun() {
        String csv = epgCsvHelper.createCsvWithInvalidRerun();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidRerun(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidRerun());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.30 Send Get EPG data, receive file with invalid LIVE, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidLive() {
        String csv = epgCsvHelper.createCsvWithInvalidLive();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidLive(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidLive());
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.27 Send Get EPG data, receive file with invalid HD, AMS write error ")
    @TestCaseId("21212")
    @Test ()
    public void sendGetEpgDataReceiveFileWithInvalidHd() {
        String csv = epgCsvHelper.createCsvWithInvalidHd();
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(),  SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        copyZDB(epgLog.getPublishedZdbFiles(epgLog.getLogAMSForValidData(log)));
        assertTrue(epgLog.compareLogAMSWithPatternInvalidHd(epgLog.getLogAMSForInvalidFiled(log),
                epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidHd());
    }

 //   @Features(FeatureList.EPG)
 //   @Stories(StoriesList.VALIDATION_NEGATIVE + StoriesList.GET_EPG_DATA)
//    @Description("2.1.29 Send Get EPG data, receive file with invalid IS_SERIES, AMS write error ")
//    @Test (enabled = false) // there is not "Invalid IS_SERIES field value: invalid" error in AMS log, not implemented?
//    public void sendGetEpgDataReceiveFileWithInvalidIsSeries() {
//        String csv = epgCsvHelper.createCsvWithInvalidIsSeries();
//        createAttachment(csv);
//        Assert.assertEquals(sendUpdateEpgDataRequest(),  success200);
//        Assert.assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), cheEmulatorEpg.getURIList());
//        log = readAMSLog();
//        createAttachment(log);
//        List<String> logEpg = epgLog.getLogAMSForValidData(log);
//        List<String> zdb = epgLog.getPublishedZdbFiles(logEpg);
//        copyZDB(zdb);
//        Assert.assertTrue(epgLog.compareLogAMSWithPatternInvalidIsSeries(epgLog.getLogAMSForInvalidFiled(log), epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()));
//        Assert.assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()));
//        Assert.assertEquals(dbUtils.getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsInvalidIsSeries());
//    }
}