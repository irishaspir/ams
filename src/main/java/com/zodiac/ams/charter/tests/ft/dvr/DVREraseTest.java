package com.zodiac.ams.charter.tests.ft.dvr;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.handlers.UniversalListener;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.dvr.*;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Status;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

import static com.zodiac.ams.charter.tests.FeatureList.DVR;

public class DVREraseTest extends DVRTest{
    
    @Override
    public boolean needRestart(){
        return false;
    }
            
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.1.1 [DVR][Erase] recordingId is present in STB")
    @Test(priority=2)
    public void test_6_2_1_1() throws Exception {
        positiveTestImpl(1);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.1.10 [DVR][Erase] Deleting record from db")
    @Test(priority=2)
    public void test_6_2_1_10() throws Exception {
        EraseRequest request = EraseRequest.getRandom(deviceId, 5);
        Integer [] ids = request.getIds();
        EraseResponse response = new EraseResponse(request, Error.Error0);
        positiveTestImpl(request, response);
        RecordedProgramDelta delta = new RecordedProgramDelta(new DeltaErase(ids));
        sendMessage(delta.getData());
//        Thread.sleep(60*60*1_000);
        verifyCount(0);
    }
    
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.1.2 [DVR][Erase] several recordingIds are present in STB")
    @Test(priority=2)
    public void test_6_2_1_2() throws Exception {
        positiveTestImpl(5);
    }
    
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    //@Stories("Erase recording. Positive")
    @Description("6.2.1.3 [DVR][Erase] recordingId isn't present in STB")
    @Test(priority=2)
    public void test_6_2_1_3() throws Exception {
        EraseRequest request = EraseRequest.getRandom(deviceId, 1);
        EraseResponse response = new EraseResponse(request, Error.Error0);
        Integer[] ids = request.getIds();
        //prepareDB(ids);
        DvrDbHelper.clearRecording(deviceId);
        stepsErasePositiveTest(request, response);
        verifyCount(0);
        //verifyCount(ids.length);
        //verifyStatus(ids, Status.MANUALLY_CANCELED.getCode());
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.1.4 [DVR][Erase] recordingId is empty")
    @Test(priority=2)
    public void test_6_2_1_4() throws Exception {
        negativeTestImpl(param -> param.setRecordingId(""), 400);
    }
    
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.1.5 [DVR][Erase] recordingId is missing")
    @Test(priority=2)
    public void test_6_2_1_5() throws Exception {
        negativeTestImpl(param -> {param.setRecordingId(null); param.setWholeSetFlag(null);}, 400);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.1.6 [DVR][Erase] Deleting one series")
    @Test(priority=2)
    public void test_6_2_1_6() throws Exception {
        positiveTestImpl(1, param -> param.setWholeSetFlag(YesNo.No));
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.1.7 [DVR][Erase] Deleting all series")
    @Test(priority=2)
    public void test_6_2_1_7() throws Exception {
        positiveTestImpl(1, param -> param.setWholeSetFlag(YesNo.Yes));
    }
    
    //6.2.1.8 [DVR][Erase] Deleting record from db
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.1.8 [DVR][Erase] wholeSetFlag is empty")
    @Test(priority=2)
    public void test_6_2_1_8() throws Exception {
        negativeTestImpl(param -> param.setWholeSetFlag(""), 400);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.1.9 [DVR][Erase] wholeSetFlag is invalid")
    @Test(priority=2)
    public void test_6_2_1_9() throws Exception {
        negativeTestImpl(param -> param.setWholeSetFlag("test"), 400);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.1 [DVR][Erase] Error code \"1 - ERROR, no operation has been accomplished\"")
    @Test(priority=3)
    public void test_6_2_2_1() throws Exception {
        errorImpl(Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.2 [DVR][Erase] Error code \"6 - Only a subset of the deletion request completed successfully\"")
    @Test(priority=3)
    public void test_6_2_2_2() throws Exception {
        errorImpl(Error.Error6);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.3 [DVR][Erase] Error code \"7 - DVR is not authorized on the STB\"")
    @Test(priority=3)
    public void test_6_2_2_3() throws Exception {
        errorImpl(Error.Error7);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.4 [DVR][Erase] Error code \"10 - DVR is not ready (still initializing)\"")
    @Test(priority=3)
    public void test_6_2_2_4() throws Exception {
        errorImpl(Error.Error10);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.5 [DVR][Erase] Error code \"13 - Invalid parameter was specified\"")
    @Test(priority=3)
    public void test_6_2_2_5() throws Exception {
        errorImpl(Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.6 [DVR][Erase] Error code \"16 - Unknown error\"")
    @Test(priority=3)
    public void test_6_2_2_6() throws Exception {
        errorImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.2.7 [DVR][Erase] Error code \"17 - STB is busy\"")
    @Test(priority=3)
    public void test_6_2_2_7() throws Exception {
        errorImpl(Error.Error17);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.3.1 [DVR][Erase] Status \"1 - ERROR, no operation has been accomplished\"")
    @Test(priority=4)
    public void test_6_2_3_1() throws Exception {
        errorStatusImpl(Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.3.2 [DVR][Erase] Status \"3 - No target program exists\"")
    @Test(priority=4)
    public void test_6_2_3_2() throws Exception {
        errorStatusImpl(Error.Error3);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.3.3 [DVR][Erase] Status \"13 - Invalid parameter was specified\"")
    @Test(priority=4)
    public void test_6_2_3_3() throws Exception {
        errorStatusImpl(Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.3.4 [DVR][Erase] Status \"16 - Unknown error\"")
    @Test(priority=4) 
    public void test_6_2_3_4() throws Exception {
        errorStatusImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.3.5 [DVR][Erase] Random status (successful and unsuccessful)")
    @Test(priority=4)
    public void test_6_2_3_5() throws Exception {
        Error [] template = new Error[]{Error.Error0, Error.Error1, Error.Error3, Error.Error13, Error.Error16};
        Error [] statuses = RandomUtils.getRandomObject(template, 10, false);
        EraseRequest request = EraseRequest.getRandom(deviceId, statuses.length);
        EraseResponse response = new EraseResponse(request, Error.Error0);
        int index = 0;
        List<EraseResponseParam> params = response.getParams();
        for (EraseResponseParam param: params)
            param.setStatusCode(statuses[index++]);
        positiveTestImpl(request, response);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.3.6 [DVR][Erase] All statuses")
    @Test(priority=4)
    public void test_6_2_3_6() throws Exception {
        Error [] statuses = new Error[]{Error.Error0, Error.Error1, Error.Error3, Error.Error13, Error.Error16};
        EraseRequest request = EraseRequest.getRandom(deviceId, statuses.length);
        EraseResponse response = new EraseResponse(request, Error.Error0);
        int index = 0;
        List<EraseResponseParam> params = response.getParams();
        for (EraseResponseParam param: params)
            param.setStatusCode(statuses[index++]);
        positiveTestImpl(request, response);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.1 [DVR][Erase] \"Count\" is inadmissible")
    @Test(priority=5)
    public void test_6_2_4_1() throws Exception {
        negativeRespImpl(response -> response.setCount(1_000), Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.2 [DVR][Erase] \"Count\" is missing")
    @Test(priority=5)
    public void test_6_2_4_2() throws Exception {
        negativeRespImpl(response -> response.setCount(-1), Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.3 [DVR][Erase] \"Status\" is inadmissible")
    @Test(priority=5)
    public void test_6_2_4_3() throws Exception {
        negativeRespPImpl(response -> response.setStatusCode(Error.NON_EXISTENT), Error.UNKNOWN_ERROR);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.4 [DVR][Erase] \"Status\" is missing")
    @Test(priority=5)
    public void test_6_2_4_4() throws Exception {
        negativeRespPImpl(response -> response.setStatusCode(null), Error.Error13);
    }
    
    
    @Features(DVR)
    @Stories("Erase recording. Positive")
    @Description("6.2.4.5 [DVR][Erase] \"STBSpaceStatus\" is \"0\"")
    @Test(priority=5)
    public void test_6_2_4_5() throws Exception {
        negativeRespImpl(response -> response.setSpaceStatus(SpaceStatus.get0()), Error.Error0);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.6 [DVR][Erase] \"STBSpaceStatus\" is missing")
    @Test(priority=5)
    public void test_6_2_4_6() throws Exception {
        negativeRespPImpl(response -> response.setStatusCode(null), Error.Error13);
    }
    
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.7 [DVR][Erase] \"Error code\" is inadmissible")
    @Test(priority=5)
    public void test_6_2_4_7() throws Exception {
        negativeRespImpl(response -> response.setErrorCode(Error.NON_EXISTENT.getCodeSTB()), Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Erase recording. Negative")
    @Description("6.2.4.8 [DVR][Erase] \"Error code\" is missing")
    @Test(priority=5)
    public void test_6_2_4_8() throws Exception {
        negativeRespImpl(response -> response.setErrorCode(null), Error.Error13);
    }
    
    private void negativeRespImpl(Consumer<EraseResponse> consumer, Error expected) throws Exception{
        EraseRequest request = EraseRequest.getRandom(deviceId, 1);
        EraseResponse response = new EraseResponse(request, Error.Error0);
        EraseResponse responseHE = new EraseResponse(response);
        if (null != consumer)
            consumer.accept(response);
        responseHE.getParam(0).setStatusCode(expected);
        positiveTestImpl(request, response, responseHE);
    }
    
    private void negativeRespPImpl(Consumer<EraseResponseParam> consumer, Error expected) throws Exception{
        EraseRequest request = EraseRequest.getRandom(deviceId, 1);
        EraseResponse response = new EraseResponse(request, Error.Error0);
        EraseResponse responseHE = new EraseResponse(response);
        if (null != consumer)
            consumer.accept(response.getParam(0));
        responseHE.getParam(0).setStatusCode(expected);
        positiveTestImpl(request, response, responseHE);
    }
    
    
            
    
    
//    @Features(DVR)
//    @Stories("AMS log")
//    @Description("DVR Erase AMS log")
//    @Test(priority=100)
//    public void getDvrAmsLog() throws Exception {
//        getServerXml();
//        stopAndgetAMSLog(true);
//    }
    
    private void positiveTestImpl() throws Exception{
        positiveTestImpl(1, null, null, null, null);
    }
    
    private void positiveTestImpl(int size) throws Exception{
        positiveTestImpl(size, null, null, null, null);
    }
    
    private void positiveTestImpl(int size,Consumer<EraseRequestParam> consumerReqP) throws Exception{
        positiveTestImpl(size, null, consumerReqP, null, null);
    }
    
    private void positiveTestImpl(EraseRequest request, EraseResponse response, EraseResponse responseHE) throws Exception{
        positiveTestImpl(request, response, responseHE, null, null, null, null);
    }
    
    private void positiveTestImpl(EraseRequest request, EraseResponse response) throws Exception{
        positiveTestImpl(request, response, response, null, null, null, null);
    }
    
    private void positiveTestImpl(EraseRequest request, EraseResponse response, EraseResponse responseHE,
            Consumer<EraseRequest> consumerReq, Consumer<EraseRequestParam> consumerReqP,
            Consumer<EraseResponse> consumerResp, Consumer<EraseResponseParam> consumerRespP) throws Exception{
        
        if (null != consumerReq)
            consumerReq.accept(request);
        if (null != consumerReqP){
            List<EraseRequestParam> params = request.getParams();
            if (null != params)
                params.forEach(param -> consumerReqP.accept(param));
        }
        
        if (null != consumerResp)
            consumerResp.accept(response);
        List<EraseResponseParam> params = response.getParams();
        if (null != consumerRespP && null != params)
            params.forEach(param -> consumerRespP.accept(param));
        
        Integer[] ids = request.getIds();
        List<Integer> good = new ArrayList<>();
        List<Integer> bad = new ArrayList<>();
        for (int i=0; i<ids.length; i++){
            int err = responseHE.getParams().get(i).getStatusCode();
            if (0 == err)
                good.add(ids[i]);
            else
                bad.add(ids[i]);
        }
        
        prepareDB(ids);
        stepsErasePositiveTest(request, response, responseHE);
        verifyCount(ids.length);
        verifyStatus(good.toArray(new Integer[]{}), Status.MANUALLY_CANCELED.getCode());   
        verifyStatus(bad.toArray(new Integer[]{}), 0);   
        
    }
    
    private void positiveTestImpl(int size, 
            Consumer<EraseRequest> consumerReq, Consumer<EraseRequestParam> consumerReqP,
            Consumer<EraseResponse> consumerResp, Consumer<EraseResponseParam> consumerRespP) throws Exception{
        
        EraseRequest request = EraseRequest.getRandom(deviceId, size);
        EraseResponse response = new EraseResponse(request, Error.Error0);
        positiveTestImpl(request, response, response, consumerReq, consumerReqP, consumerResp, consumerRespP);
    }
    
    private void errorImpl(Error error) throws Exception{
        positiveErrorTestImpl(p -> p.setErrorCode(error.getCodeSTB()), p -> p.setStatusCode(error));
    }
    
    private void errorStatusImpl(Error error) throws Exception{
        positiveErrorTestImpl(null, p -> p.setStatusCode(error));
    }
    
    private void positiveErrorTestImpl(Consumer<EraseResponse> consumerResp, Consumer<EraseResponseParam> consumerRespP) throws Exception{
        positiveTestImpl(1, null, null, consumerResp, consumerRespP);
    }
    
    
    private void negativeTestImpl(Consumer<EraseRequestParam> consumer, int expectedStatus) throws Exception{
        EraseRequest request = EraseRequest.getRandom(deviceId, 1);
        consumer.accept(request.getParam(0));
        Integer[] ids = request.getIds();
        prepareDB(ids);
        stepsEraseNegativeTest(request, expectedStatus);
        verifyCount(ids.length);
        verifyStatus(ids, 0);
    }
    
    private void verifyStatus(Integer[] recordingIds, int expected){
        for (int recordingId: recordingIds){
            int actual = DvrDbHelper.getDBRecStatus(deviceId, recordingId);
            Assert.assertTrue(expected == actual, "DB recording [" + recordingId + "] status: Expected=" + expected + " Actual=" + actual);
            actual = DvrDbHelper.getCassandraRecStatus(deviceId, recordingId);
            Assert.assertTrue(expected == actual, "Cassandra recording [" + recordingId + "] status: Expected=" + expected + " Actual=" + actual);
        }
    }
    
    private void stepsEraseNegativeTest(IDVRObject heRequest, int expectedStatus) throws Exception{
        String uri = properties.getDVRUri();
        FTHttpUtils.HttpResult amsResponse = FTHttpUtils.doPost(uri, heRequest.toString());
        
        showHERequestToAMS(uri, heRequest.toString()/*HE-AMS request*/, null /*AMS-HE expected response*/, 
                amsResponse/*AMS-HE actual response*/);
        
        Assert.assertTrue(expectedStatus == amsResponse.getStatus(), "Expected status: " + expectedStatus + " Actual: " + amsResponse.getStatus());
    }

    private void stepsErasePositiveTest(IDVRObject heRequest, IDVRObject stbResponse) throws Exception{
        stepsErasePositiveTest(heRequest, stbResponse, stbResponse);
    }
    
    /**
     * 
     * @param heRequest
     * @param stbResponse  STB -> AMS actual response
     * @param stbResponseHE  AMS -> HE expected response
     * @throws Exception 
     */
    private void stepsErasePositiveTest(IDVRObject heRequest, IDVRObject stbResponse, IDVRObject stbResponseHE) throws Exception{
        CharterStbEmulator stb = null;
        DataConsumer consumer =  null;
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20*1_000);
        
        try {
            Semaphore semaphore = new Semaphore(0);
            consumer = new DataConsumer(semaphore);
            stb = createCharterSTBEmulator(STB_PROPS);
            stb.registerConsumer(consumer, HandlerType.Universal);
            UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
            listener.setResponse(stbResponse.getData());            
            startCharterSTBEmulator(stb);
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult amsResponse = FTHttpUtils.doPost(uri, heRequest.toString());
//            Thread.sleep(60*60*1_000);

            byte[] actual = consumer.getData();
            
            showHERequestToAMS(uri, heRequest.toString()/*HE-AMS request*/, stbResponseHE.toString() /*AMS-HE expected response*/, 
                amsResponse/*AMS-HE actual response*/);
            showAMSRequestToSTB(heRequest.getData()/*AMS-STB expected request*/, actual/*AMS-STB actual request*/);
            
            Assert.assertTrue(200 == amsResponse.getStatus(), "Expected status: 200 Actual: " + amsResponse.getStatus());
            verifyJSON(stbResponseHE.toJSON(), new JSONObject((String)amsResponse.getEntity()),"Expected and actual AMS response must be the same");
            Assert.assertTrue(Arrays.equals(heRequest.getData(), actual), "Expected and actual request from AMS to STB must be the same");
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
}
