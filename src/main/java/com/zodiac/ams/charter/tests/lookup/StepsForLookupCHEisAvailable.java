package com.zodiac.ams.charter.tests.lookup;

import com.zodiac.ams.charter.emuls.che.lookup.LookupCHEEmulator;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class StepsForLookupCHEisAvailable extends StepsForLookup {

    @BeforeMethod
    protected void runCHELookup() {
        lookupCHEEmulator = new LookupCHEEmulator();
        lookupCHEEmulator.registerConsumer();
        lookupCHEEmulator.start();
        Logger.info("CHE Emulator's run");
        getTestTime();
    }

    @AfterMethod
    protected void stopCHE() {
        stopCHELookup();
    }
}
