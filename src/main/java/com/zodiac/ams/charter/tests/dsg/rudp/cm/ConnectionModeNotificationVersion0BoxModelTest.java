package com.zodiac.ams.charter.tests.dsg.rudp.cm;


import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils.isMacInSTBLastActivity;
import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.ALOHA;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.connectionMode;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.msgType;

import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_0_STRING;
import static org.testng.Assert.assertEquals;

public class ConnectionModeNotificationVersion0BoxModelTest extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.3 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified, " +
            "unknown mac, receive ERROR_VERSION3_PROTOCOL CODE = 0 ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(priority = 1)
    public void sendVersion0RequestId0ConnectionModeUnknownMac() {
        String mac = unknownMac();
        message = version0Request.createMessage(mac, connectionMode);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertEquals(true, isMacInMacIp(mac), UNKNOWN + MAC_IS_NOT_IN_MAC_IP);
        assertEquals(true, isMacInSTBLastActivity(mac), CHANGE_TIME_STAMP + AND + LAST_ACTIVITY_STAMP + NOT_EQUAL);
        assertJson(mac, connectionMode);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
     //   assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), getModelId(UNKNOWN), mac);
        assertLog(log, mac, msgType, connectionMode);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified,  " +
            "box model changes")
    @TestCaseId("123")
    @Issue("CHRAMS-270/:view/")
    @Test(dataProvider = "box models", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion0RequestId0BoxModelChanges(int boxModel, int vendor) {
        connectionMode = changeConnectionMode();
        message = version0Request.createMessage(mac, connectionMode, String.valueOf(boxModel));
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
     //   assertDB(getModelId(UNKNOWN), vendor, boxModel, mac);
        assertJson(mac, connectionMode, boxModel,(Object) vendor);
        assertLog(log, mac, msgType, connectionMode, boxModel);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified,  " +
            "box model unknown")
    @TestCaseId("123")
    @Issue("CHRAMS-270/:view/")
    @Test()
    public void sendVersion0RequestId0BoxModelUnknown() {
        int boxModel = 8975;
        connectionMode = changeConnectionMode();
        message = version0Request.createMessage(mac, connectionMode, boxModel);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
    //    assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), boxModel, mac);
        assertJson(mac, connectionMode, boxModel, "Unknown");
        assertLog(log, mac, msgType, connectionMode, boxModel);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> IS specified, " +
            "romId and VendorId changes")
    @Test(dataProvider = "romId and VendorId", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion0RequestId0VendorIsSpecifiedRomIdChange(int romId, int vendor) {
        connectionMode = ALOHA;
        message = version0Request.createMessage(mac, connectionMode, romId, vendor);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertJson(mac, connectionMode, getBoxModel(romId),(Object) vendor);
     //   assertDB(romId, vendor, getModelId(UNKNOWN), mac);
        assertLog(log, mac, msgType, connectionMode, romId, vendor);
    }


}
