package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.services.ft.publisher.*;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselCDN;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselDNCS;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.*;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getLocalTmpName;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getRemoteTmpName;


public class PublisherTestImpl extends FuncTest{
    
    static enum DNCS_MODE{NOT_REMOVE, REMOVE_ALL_EXCEPT_RECENT, REMOVE_OLD}
    
    static final Logger logger = LoggerFactory.getLogger(PublisherTestImpl.class);
    
    protected List<ICarousel> carouselsIn = new ArrayList<>();
    protected List<ICarousel> carouselsOut = new ArrayList<>();
    protected CarouselType carouselType;
    
    protected boolean isZipped = false;
    protected boolean aLocEnabled = false; 
    protected boolean dalinc = false;
    
    protected ICarousel root;
    protected long commonVersion = -1;
     
    static final int CREATE_BEFORE_FLAG = 0b0001;
    static final int DELETE_AFTER_CREATE_BEFORE_FLAG = 0b0010;
    static final int CREATE_BEFORE_NO_RECORD_FLAG = 0b00100;   
    
    static final long NO_VERSION = -1;
    static final int MAX_ITERATION = 2;
    static final long ITERATION_DELAY = 1000;
    
    static final int UNKNOWN = 0;
    static final int ADD_NEW_FILE_ON_CAROUSEL = 1;
    static final int ADD_NEW_FILE_ON_CAROUSEL_DALINC = 2;
    static final int ADD_EXISTING_FILE_ON_CAROUSEL = 3;
    static final int ADD_NEW_FILE_ON_CAROUSEL_NO_FLAGS = 4;
    static final int DELETE_FILE_FROM_CAROUSEL = 5;
    static final int DELETE_FILE_FROM_CAROUSEL_INVALID_CAROUSEL =6;
    static final int DELETE_FILE_FROM_CAROUSEL_INVALID_NAME =7;
    static final int DELETE_FILE_FROM_CAROUSEL_NO_FILE_NO_RECORD = 8;
    static final int DELETE_FILE_FROM_CAROUSEL_NO_FILE = 9;
    static final int DELETE_FILE_FROM_CAROUSEL_NO_RECORD = 11;
    static final int DELETE_FILE_FROM_CAROUSEL_AFTER_UPDATE = 12;
    static final int CLEAR_OLD = 10; //!!!
    static final int UPDATE_EXISTING = 13;
    static final int UPDATE_EXISTING_INVALID_CAROUSEL = 14;
    static final int UPDATE_EXISTING_INVALID_NAME = 15;
    static final int UPDATE_NO_EXISTING = 16;
    static final int ADD_MODIFY_EXISTING = 17;
    static final int ADD_MODIFY_NO_EXISTING = 18;
    static final int ADD_MODIFY_NO_EXISTING_NO_FLAGS = 19;
    static final int ADD_MODIFY_NO_VER_INC_EXISTING = 20;
    static final int ADD_MODIFY_NO_VER_INC_NO_EXISTING = 21;
    static final int ADD_MODIFY_NO_VER_INC_NO_EXISTING_NO_FLAGS = 22;
    static final int ADD_MODIFY_EXISTING_NO_FILE = 23;
    static final int UPDATE_EXISTING_NO_FILE = 24;
    static final int ADD_MODIFY_EXISTING_NO_RECORD = 25;
    static final int UPDATE_EXISTING_NO_RECORD = 26;
    static final int INC_VERSION_EXISTING  = 27;
    static final int INC_VERSION_NO_EXISTING = 28;
    
    static final int UNKNOWN_ACTION = 100;
    static final int UNKNOWN_SERVER = 101;
    
    static final int ADD_MODIFY_NO_VER_INC_INC_VERSION_EXISTING = 200;
    static final int ADD_MODIFY_NO_VER_INC_INC_VERSION_NO_EXISTING = 201;
     
    private String getName(String name){
        return getFileFromJar(PUBLISHER_DATA_DIR, name);
    }
    
    void putFileOnCarousel(ICarousel carousel,String local,String remoteName){
        Assert.assertTrue(carousel.putFileOnCarousel(local, remoteName),"Put command must return true");
    }
    
    String getCarouselFile(ICarousel carousel,String remoteName){
        return carousel.getCarouselFile(remoteName);
    }
    
    String getZippedCarouselFile(ICarousel carousel,String remoteName,String entryName){
        return carousel.getZippedCarouselFile(remoteName, entryName);
    }
    
    void deleteCarouselFile(ICarousel carousel,String remoteName){
        Assert.assertTrue(carousel.deleteCarouselFile(remoteName),"Delete command must return true");
    }
    
    
    String[] getUids(List<ICarousel> carousels){
        String [] uids = new String[carousels.size()];
        for (int i=0;i<uids.length;i++)
            uids[i] = carousels.get(i).getUid();
        return uids;
    }
    
    FTHttpUtils.HttpStringResult sendRequest(CarouselType type,PublishTasks tasks) throws Exception{
        return sendRequest(type, tasks, 200, false);
    }
    
    FTHttpUtils.HttpStringResult sendRequest(CarouselType type,PublishTasks tasks,boolean exit) throws Exception{
        return sendRequest(type, tasks, 200, exit);
    }
    
    FTHttpUtils.HttpStringResult sendRequest(CarouselType type,PublishTasks tasks,int expected,boolean exit) throws Exception{
        return sendRequest(type, tasks, expected, false, null, exit);
    }
            
    FTHttpUtils.HttpStringResult sendRequest(CarouselType type, PublishTasks tasks, int expected, boolean getTasks,
        ITasksUpdater updater, boolean exit) throws Exception{
        String uri = properties.getPublisherUri();
        TasksJSON js=new TasksJSON((null == updater) ? type : updater.getCarouselType(type), tasks);
        js.createZIP();
        if (exit)
            System.exit(0);
        FTHttpUtils.HttpStringResult response=(FTHttpUtils.HttpStringResult)FTHttpUtils.doMultipartPost(uri, new File(TasksJSON.ZIP_NAME));
        getHTTPPostStatus(uri,response);
        if (getTasks)
            getTasks();
        Assert.assertTrue(expected == response.getStatus(),"Response code must be "+expected);
        return response;
    }
    
    List<ZDtsItem> addImpl(int operation, List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, String source, String fileToAdd, boolean show) throws Exception{
        return addImpl(operation, carouselsIn, carouselsOut, source, fileToAdd, false, show);    
    }
    
    List<ZDtsItem> addImpl(int operation, List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, String source, String fileToAdd, boolean dalinc, boolean show) throws Exception{
        return addImpl(operation, carouselsIn, carouselsOut, source, fileToAdd, dalinc, show, 0); 
    }
     
    List<ZDtsItem> addImpl(int operation, List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, String source, String fileToAdd, boolean dalinc, 
        boolean show, int excludeMask) throws Exception{
        List<ZDtsItem> results = new ArrayList<>();
        
        PublishTasks tasks =new PublishTasks();
        for (ICarousel carousel: carouselsIn)
            tasks.addTask(new PublishTask(PublishTask.Action.ADD,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias()),fileToAdd,source);
        
        sendRequest(carouselType,tasks);
        getTasks();
        
        int index = 0;
        for (ICarousel carousel: carouselsOut) {
            if (isExcluded(excludeMask, index))
                results.add(null);
            else
                results.add(verify(operation,carousel,source,fileToAdd, dalinc ? 0 : ExpectedResult.FILE_EXISTS,
                    new ExpectedVersion(1,!aLocEnabled),show));
            index++;
        }
        
        return results;
    }
    
    void addImplNoFlags(int operation, List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, String source, String fileToAdd, boolean show) throws Exception{
        PublishTasks tasks =new PublishTasks();
        for (ICarousel carousel: carouselsIn){
            PublishTask task = new PublishTask(PublishTask.Action.ADD,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias());
            task.setFlags(null);
            tasks.addTask(task,fileToAdd,source); 
        }
        sendRequest(carouselType, tasks, 500, true, null, false);
    }
    
    
    private String getNamesFromList(List<String> names){
        if (null == names)
            return "";
        StringBuilder sb = new StringBuilder();
        for (String name: names){
            if (sb.length()>0)
                sb.append(",");
            sb.append("[").append(name).append("]");
        }
        return sb.toString();
    }
    
    private String getLatestNameFromList(List<String> names){
        if (null == names || 0 == names.size())
            return "";
        return names.get(names.size()-1);
    }
    
    /**
     * 
     * @param carousel
     * @param name for DNCS must contains name with prefix
     * @return DNCS: prefix/name -> name.prefix hash.timestamp <p> 
     * DNCS: name-> name -> name.timestamp <p>
     * CDN: name-> timestamp_name <p>
     * 
     */
    private String getFileName(ICarousel carousel, String name){
        if (carousel instanceof CarouselDNCS)
            return getLatestNameFromList(((CarouselDNCS)carousel).getFilesByName(name));
        if (carousel instanceof CarouselCDN)
            return getLatestNameFromList(((CarouselCDN)carousel).getFilesByName(name));
        return name;
    }
    
    /**
     * 
     * @param carousel
     * @param name only name without prefix even for DNCS!!!!
     * @return 
     */
    private String generateFileName(ICarousel carousel, String name){
        if (carousel instanceof CarouselDNCS)
            return ((CarouselDNCS)carousel).generateFileName(name, timeDiff);
        if (carousel instanceof CarouselCDN)
            return ((CarouselCDN)carousel).generateFileName(name, timeDiff);
        return name;
    }
    
    private String  getNameWithPrefix(ICarousel carousel, String name){
        String prefix = carousel.getCarouselHost().getSourcePrefix();
        if (null != prefix)
            return prefix+name;
        return name;
    }
    
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action action,
            int operation, int flags, int expected) throws Exception {
        updateImpl(carouselsIn, carouselsOut, action, operation, operation, flags, expected, GoldenFile.SECOND);
    }
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action action,
            int operation, int fileId, int flags, int expected) throws Exception {
        updateImpl(carouselsIn, carouselsOut, action, operation, fileId, flags, expected, GoldenFile.SECOND);
    }
    
    
    /**
     * Generates files with old timestamps for DNCS and CDN carousels
     * @param fileId 
     */
    void createOldFiles(int fileId){
        String source = "name"+fileId+".zdb";
        String file = createFile(fileId, 100);
        for (ICarousel carousel: carouselsOut){
            if ((carousel instanceof CarouselDNCS) || (carousel instanceof CarouselCDN)) {
                putFileOnCarousel(carousel, file, generateFileName(carousel,source));    
            }
        }
    }
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action action, 
            int operation, int flags, int expected, GoldenFile golden) throws Exception{
        updateImpl(carouselsIn, carouselsOut, action, operation, operation, flags, expected, golden);
    }
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action action, 
            int operation, int fileId, int flags, int expected, GoldenFile golden) throws Exception{
        updateImpl(carouselsIn, carouselsOut, action, operation, fileId, flags, expected, golden, 200, null, null);
    }
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action action, 
            int operation, int fileId, int flags, int expected, GoldenFile golden,int expectedStatus,
            ITasksUpdater updater, IResponseVerifier verifier) throws Exception{
        updateImpl(carouselsIn, carouselsOut, action, operation, fileId, flags, expected, golden, expectedStatus,
            updater, verifier, 0);
    }
    
    private static class ExpectedVersion{
        long version;
        boolean strictCondition;
        
        ExpectedVersion(long version, boolean strictCondition){
            this.version = version;
            Version pubVersion = FTConfig.getInstance().getPublihserVersion();
            switch(pubVersion){
                case VERSION_1:
                case VERSION_2:
                case VERSION_3:
                case VERSION_3_1:
                    this.strictCondition = strictCondition;
                    break;
                default:
                    this.strictCondition = false;
                    break;
            }
        }
    }
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action action, 
            int operation, int fileId, int flags, int expected, GoldenFile golden,int expectedStatus,
            ITasksUpdater updater, IResponseVerifier verifier, int excludeMask) throws Exception{
        updateImpl(carouselsIn, carouselsOut, new PublishTask.Action [] {action}, operation,  fileId, flags, expected, golden, expectedStatus,
            updater, verifier, excludeMask);
    }
    
//    private ExpectedVersion getDefaultExpectedVersion(int operation){
//        return new ExpectedVersion(operation == ADD_MODIFY_NO_VER_INC_INC_VERSION_NO_EXISTING ? 2L : 1L, !aLocEnabled);
//    }
    
    
    void updateImpl(List<ICarousel> carouselsIn, List<ICarousel> carouselsOut, PublishTask.Action[] actions, 
            int operation, int fileId, int flags, int expected, GoldenFile golden,int expectedStatus,
            ITasksUpdater updater, IResponseVerifier verifier, int excludeMask) throws Exception{
        String source = "name"+fileId+".zdb";
        
        String file1 = createFile(operation,1);
        String file2 = createFile(operation,2);
        
        List<ExpectedVersion> expectedVer = new ArrayList<>();
        for (int i=0;i<carouselsOut.size();i++)
            expectedVer.add(new ExpectedVersion(1L,!aLocEnabled));
        
        if (0 != (flags & CREATE_BEFORE_FLAG)){
            List<ZDtsItem> items = addImpl(UNKNOWN, carouselsIn, carouselsOut, source, file1, dalinc, false, excludeMask);
            if (dalinc){
                for (ICarousel carousel: carouselsOut)
                    putFileOnCarousel(carousel,file1,generateFileName(carousel,source));
            }
                            
            
            Assert.assertTrue(expectedVer.size() ==  items.size(), "Sizes of Expected Version array and ZDTS Items array must be the same");
            for (int i=0;i<items.size();i++){
                ZDtsItem item = items.get(i);
                if (null != item) {
                    long exp = item.getVersion();
                    boolean strict = true;
                    if (0 != (expected & ExpectedResult.RECORD_INCREMENTED)){
                        exp++;
                        strict = !aLocEnabled;
                    }
                    expectedVer.set(i, new ExpectedVersion(exp, strict));
                }
            }
            if (0 != (flags & DELETE_AFTER_CREATE_BEFORE_FLAG)){
                for (ICarousel carousel: carouselsOut){
                    // no prefix: <name>.<timestamp>  prefix: <name>.<prefix hash>.<timestamp>
                    if (carousel instanceof CarouselDNCS)
                        deleteCarouselFile(carousel,getFileName(carousel,getNameWithPrefix(carousel,source))); 
                    else
                    //<timestamp>_<name>    
                    if (carousel instanceof CarouselCDN)
                        deleteCarouselFile(carousel,getFileName(carousel,source));
                    else
                        deleteCarouselFile(carousel,source);
                }
                //System.exit(0);
            }
        }
        else 
        if (0 != (flags & CREATE_BEFORE_NO_RECORD_FLAG)){
            for (ICarousel carousel: carouselsOut)
                putFileOnCarousel(carousel,file1,generateFileName(carousel,source));
        }
        
        PublishTasks tasks = new PublishTasks();
        for (ICarousel carousel: carouselsIn){
            for (PublishTask.Action action: actions){
                PublishTask task = new PublishTask(action,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias());
                if (null != updater)
                    updater.update(carousel, task);
            
                tasks.addTask(task,
                    PublishTask.Action.DELETE == action ? null : file2,
                    PublishTask.Action.DELETE == action ? null : source);
            }
        }
        
        FTHttpUtils.HttpStringResult response = sendRequest(carouselType, tasks, expectedStatus, true /*show tasks*/, updater, false /*exit*/);
        
        
        if (null != verifier)
            verifier.verify(response);
        
        if (ExpectedResult.isNoRecordFlags(expected)){
            for (int i=0;i< expectedVer.size();i++)
                expectedVer.set(i, new ExpectedVersion(NO_VERSION, true));
        }
        
        List<ZDtsItem> items = new ArrayList<>();
        for (int i=0;i<carouselsOut.size();i++){
            if (!isExcluded(excludeMask,i))
                items.add(verify(operation , carouselsOut.get(i), source, (GoldenFile.SECOND == golden)? file2 : file1, expected, expectedVer.get(i), true));
        }
        setCommonVersion(items);
    }
    
    int getExcludeMask(int index){
        return getExcludeMask(new int[] {index});
    }
    
    int getExcludeMask(int[] indexes){
        int mask =0;
        for (int index: indexes)
            mask  = mask | (1 << index);
        return mask;
    }
    
    boolean isExcluded(int mask, int index){
        return 0 != (mask & (1 << index));
    }
    
    void verifyRootZdts(ICarousel rootCarousel, List<ICarousel> carousels, long expVersion, boolean skipCDN){
        long [] expVers = new long[carousels.size()];
        Arrays.fill(expVers, expVersion);
        verifyRootZdts(rootCarousel, carousels, expVers, true, skipCDN);
        
    }
    
    void verifyRootZdts(ICarousel rootCarousel, List<ICarousel> carousels, long expVersion){
        verifyRootZdts(rootCarousel, carousels, expVersion, false);
    }
    
    void verifyRootZdts(ICarousel rootCarousel, List<ICarousel> carousels, long [] expVers, boolean strictCondition){
        verifyRootZdts(UNKNOWN, rootCarousel, carousels, expVers, strictCondition, false);
    }
    
    void verifyRootZdts(ICarousel rootCarousel, List<ICarousel> carousels, long [] expVers, boolean strictCondition, boolean skipCDN){
        verifyRootZdts(UNKNOWN, rootCarousel, carousels, expVers, strictCondition, skipCDN);
    }
    
    void verifyRootZdts(int operation, ICarousel rootCarousel, List<ICarousel> carousels, long [] expVers, boolean strictCondition, boolean skipCDN){
        verifyRootZdts(operation, rootCarousel, carousels, expVers, strictCondition, skipCDN, 1, MAX_ITERATION);
    }
            
    void verifyRootZdts(ICarousel rootCarousel, List<ICarousel> carousels, long [] expVers, boolean strictCondition, boolean skipCDN,
            int iteration, int maxIteration){
        verifyRootZdts(UNKNOWN, rootCarousel, carousels, expVers, strictCondition, skipCDN, iteration, maxIteration);
    }
    
    void verifyRootZdts(int operation, ICarousel rootCarousel, List<ICarousel> carousels, long [] expVers, 
            boolean strictCondition, boolean skipCDN, int iteration, int maxIteration){
        if (!aLocEnabled)
            return;
        boolean ok = true;
        boolean only_one = true;
        List<String> rootNames = new ArrayList<>();
        List<String> oldRootNames =new ArrayList<>();
        List<ZDtsItem> items = new ArrayList<>();
        
        String rootName = ROOT_ZDTS_INI;
        if ((rootCarousel instanceof CarouselDNCS) || (rootCarousel instanceof CarouselCDN)){
            CarouselDNCS dncs = (rootCarousel instanceof CarouselDNCS) ? (CarouselDNCS)rootCarousel : null;
            CarouselCDN cdn = (rootCarousel instanceof CarouselCDN) ? (CarouselCDN)rootCarousel : null;
            
            rootNames = (null == cdn) ? dncs.getFilesByName(rootName) : cdn.getFilesByName(rootName);
            
            switch(getDNCSMode(operation)){
                case REMOVE_ALL_EXCEPT_RECENT:
                    only_one = (null == rootNames) || rootNames.size()<2; 
                    break;
                case REMOVE_OLD: 
                    oldRootNames = (null == cdn) ? CarouselDNCS.getOldFiles(rootNames, new Date().getTime()+timeDiff-Constants.MAX_TIMESTAMP):
                        CarouselCDN.getOldFiles(rootNames, new Date().getTime()+timeDiff-Constants.MAX_TIMESTAMP);
                    only_one = (null == oldRootNames) || 0 == oldRootNames.size();
                    break;
            }
            
            String latest = getLatestNameFromList(rootNames);
            if (null != latest && !latest.trim().equals(""))
                rootName = latest;
        }
        
        String zdts = /*isZipped ? getZippedCarouselFile(rootCarousel,ROOT_ZDTS_INI_ZIP,ROOT_ZDTS_INI) :*/getCarouselFile(rootCarousel,rootName);
        int index = 0;
        
        for (ICarousel carousel: carousels){
            long expVer = expVers[index++];
            ZDtsItem item =ZDtsItem.getItem(carousel.getRootName(), getRemoteTmpName(rootName));
            items.add(item);
            
            boolean strict = ((carousel instanceof CarouselCDN) && skipCDN) ? false : strictCondition;
            
            if (iteration == maxIteration)
                showRootZdtsItem(carousel, item, expVer, strict, iteration);
            
            if (iteration == maxIteration){
                switch(getDNCSMode(operation)){
                    case REMOVE_ALL_EXCEPT_RECENT:
                        Assert.assertTrue(only_one, "No more than one file with the same mask is allowed. Found: " + getNamesFromList(rootNames));
                        break;
                    case REMOVE_OLD:
                        Assert.assertTrue(only_one, "No old files are allowed. Found: " + getNamesFromList(oldRootNames));
                        break;
                }
            }
            
            if (NO_VERSION == expVer){
                ok = null == item;
                if (iteration == maxIteration)
                    Assert.assertTrue(ok, "ZDts item must be null");
            }
            else {
                ok = null != item;
                if (iteration == maxIteration)
                    Assert.assertTrue(ok, "ZDts item mustn't be null");
                if (ok)
                    ok = verfiyVersion(item.getVersion(), expVer, strict, iteration == maxIteration);
            }
            if (ok  && iteration != maxIteration)
                showRootZdtsItem(carousel, item, expVer, strict, iteration);
            
            if (!ok || !only_one)
                break;
        }
        
        if ((ok && only_one) || iteration == maxIteration)
            return;
        
        try{
            Thread.sleep(ITERATION_DELAY);
        }
        catch(Exception ex){
        }
        verifyRootZdts(rootCarousel, carousels, expVers, strictCondition, skipCDN, ++iteration, maxIteration);      
    }
    
    long[] getRootZdtsVersion(ICarousel rootCarousel, List<ICarousel> carousels){
        if (!aLocEnabled)
            return null;
        long [] result = new long[carousels.size()];
        int index=0;
        String rootName = ROOT_ZDTS_INI;
        if (rootCarousel instanceof CarouselDNCS){
            CarouselDNCS dncs = (CarouselDNCS)rootCarousel;
            List<String> rootNames = dncs.getFilesByName(rootName);
            String latest = getLatestNameFromList(rootNames);
            if (null != latest && !latest.trim().equals(""))
                rootName = latest;
        }
           
        String zdts = /*isZipped ? getZippedCarouselFile(rootCarousel,ROOT_ZDTS_INI_ZIP,ROOT_ZDTS_INI) : */getCarouselFile(rootCarousel,rootName);
        for (ICarousel carousel: carousels){
            ZDtsItem item = ZDtsItem.getItem(carousel.getRootName(), getRemoteTmpName(rootName));
            result[index++] = (null == item) ? NO_VERSION : item.getVersion();
        }
        return result;
    }
    
    long[] incrVersions(long [] versions){
        return incrVersions(versions, 1);
    }
    
    long[] incrVersions(long [] versions, long delta){
        if (null == versions)
            return null;
        for (int i=0; i<versions.length; i++)
            versions[i] = versions[i] + delta;
        return versions;
    }
    
    long getCommonVersion(){
        return aLocEnabled ? commonVersion : -1; 
    }

    void setCommonVersion(List<ZDtsItem> items){
        if (null == items || !aLocEnabled){
            commonVersion = -1;
            return;
        }
        long version = -1;
        StringBuilder sb = new StringBuilder();
        long prev = -1;
        boolean ok = true;
        for (ZDtsItem item: items){
            if (null == item)
                break;
            if (0 != sb.length())
                sb.append(",");
            version = item.getVersion();
            sb.append(version);
            if (ok){
                if (-1 != prev && version != prev)
                    ok = false;
                prev = version;
            }
        }
        if (aLocEnabled)
            Assert.assertTrue(ok,"All verisions must be the same. But found: " + sb.toString());
        commonVersion = version;
    }
    
    boolean verfiyVersion(long version, long expected, boolean strictCondition, boolean assertion){
        boolean ok;
        if (strictCondition){
            ok = version == expected;
            if (assertion)
                Assert.assertTrue(ok, "Version: "+version+". Expected: "+expected+".");
        }
        else {
            ok = version > expected;
            if (assertion)
                Assert.assertTrue(ok,"Version: "+version+". Expected: > "+expected+".");
        }
        return ok;
    }
    
    ZDtsItem verify(int operation,ICarousel carousel,String source,String localName,int expRes,ExpectedVersion expVer,boolean show){
        return verify(operation, carousel, source, localName, expRes, expVer, show, 1,  MAX_ITERATION);
    }
    
    ZDtsItem verify(int operation,ICarousel carousel,String source,String localName,int expRes,ExpectedVersion expVer,boolean show,
        int iteration, int maxIteration){
        
        String origin = source;
        String locationName = origin;
        
        boolean ok = true;
        boolean only_one = true;
        List<String> dncsNames = null;
        List<String> duplicates = null;
        
        String remoteName = source;
        
        source=getNameWithPrefix(carousel,source);   
        
        if ((carousel instanceof CarouselDNCS) || (carousel instanceof CarouselCDN)) {
            //DNCS location: bfs://assets-obj-103<//a/bc/def/ghij/name1.zdb> - the last path with prefix must be sent to carousel getLocation() method 
            if (carousel instanceof CarouselDNCS)
                locationName = source;
            CarouselDNCS dncs = (carousel instanceof CarouselDNCS) ? (CarouselDNCS)carousel : null;
            CarouselCDN cdn = (carousel instanceof CarouselCDN) ? (CarouselCDN)carousel : null;
            //CDN - without prefix!!!
            dncsNames = (null == cdn) ? dncs.getFilesByName(source) : cdn.getFilesByName(remoteName);
            remoteName = getLatestNameFromList(dncsNames);
            //CDN location: http://127.0.0.1:8080/folder/<1495182314350_name1.zdb> - only the last part must be sent to carousel getLocation() method
            //dalinc: http://127.0.0.1:8080/folder/<[UNKNOWN TIMESTAMP]_name1.zdb> need to verify via regexp!!!
            if (carousel instanceof CarouselCDN)
                locationName = dalinc ? origin : remoteName;
                
            switch(getDNCSMode(operation)){
                case REMOVE_ALL_EXCEPT_RECENT:
                    only_one = null == dncsNames || dncsNames.size()<2;          
                    duplicates = dncsNames;
                    break;
                case REMOVE_OLD:
                    duplicates = (null == cdn) ? CarouselDNCS.getOldFiles(dncsNames, new Date().getTime() + timeDiff - Constants.MAX_TIMESTAMP) :
                        CarouselCDN.getOldFiles(dncsNames, new Date().getTime() + timeDiff - Constants.MAX_TIMESTAMP);
                    only_one = null == duplicates || 0 == duplicates.size();
                    break;
            }
        }
            
        
        String local = CarouselImpl.getLocalFile(localName);
        String remote = getCarouselFile(carousel,remoteName);
        
        String zdtsName = isZipped ? ZDTS_INI_ZIP : ZDTS_INI;
        if ((carousel instanceof CarouselDNCS) || (carousel instanceof CarouselCDN)){
            CarouselDNCS dncs = (carousel instanceof CarouselDNCS) ? (CarouselDNCS)carousel : null;
            CarouselCDN cdn = (carousel instanceof CarouselCDN) ? (CarouselCDN)carousel : null;
            
            List<String> zdtsNames = (null == cdn) ? dncs.getFilesByName(zdtsName) : cdn.getFilesByName(zdtsName);
            zdtsName = getLatestNameFromList(zdtsNames);
            if (only_one){
                switch(getDNCSMode(operation, true)){
                    case REMOVE_ALL_EXCEPT_RECENT:
                        only_one = (null == zdtsNames) || zdtsNames.size()<2;
                        if (!only_one)
                            duplicates = zdtsNames;
                        break;
                    case REMOVE_OLD:
                        duplicates = (null == cdn) ? CarouselDNCS.getOldFiles(zdtsNames, new Date().getTime() + timeDiff - Constants.MAX_TIMESTAMP): 
                            CarouselCDN.getOldFiles(zdtsNames, new Date().getTime() + timeDiff - Constants.MAX_TIMESTAMP);
                        only_one = null == duplicates || 0 == duplicates.size();
                        break;
                }
            }
        }
           
        String zdts = isZipped ? getZippedCarouselFile(carousel,zdtsName,ZDTS_INI) : getCarouselFile(carousel,zdtsName);
        
        ZDtsItem item =ZDtsItem.getItem(source, getRemoteTmpName(isZipped ? ZDTS_INI : zdtsName));
        
        if (show && iteration == maxIteration) 
            showTest(carousel.getHost(),local,remote,zdts,item,expVer);
      
        if (iteration == maxIteration)
            Assert.assertTrue(only_one, "No duplicates are allowed. Found: " + getNamesFromList(duplicates));            
        
        if (NO_VERSION == expVer.version){
            ok = null == item;
            if (iteration == maxIteration)
                Assert.assertTrue(null == item, "ZDts item must be null");
        }
        else {
            ok = null != item;
            if (iteration == maxIteration)
                Assert.assertTrue(ok, "ZDts item mustn't be null");
            if (ok)
                ok = verfiyVersion(item.getVersion(), expVer.version, expVer.strictCondition, iteration == maxIteration);
            if (ok){
                String itemLocation = item.getLocation();
                if (dalinc && (carousel instanceof CarouselCDN))
                    itemLocation = itemLocation.replaceAll("/\\d+_", "/");
                //delete (no file,but record) for CDN 
                if ((carousel instanceof CarouselCDN) && ( null == locationName || locationName.trim().equals("")))
                    ok =true;
                else
                    ok = carousel.getLocation(locationName).equals(itemLocation);
            }
            if (iteration == maxIteration)
                Assert.assertTrue(ok, "Location: "+item.getLocation()+" Expected: "+carousel.getLocation(locationName));
        }
        if (!ExpectedResult.isFileFlags(expRes)){
            if (ok)
                ok = null == remote || "".equals(remote.trim());
            if (iteration == maxIteration)
                Assert.assertTrue(ok, source+" mustn't exist on the carousel");
        }
        else {
            if (ok)
                ok = FTUtils.stringsEqual(local, remote);
            if (iteration == maxIteration)
                Assert.assertTrue(ok, "Local file ["+localName+"] and file on the carousel ["+remoteName+"] must be the same");
        }
        
        if (show && ok && only_one && iteration != maxIteration)
            showTest(carousel.getHost(),local,remote,zdts,item,expVer);
        
        if ((ok && only_one) || iteration == maxIteration)
            return item;
        try{
            Thread.sleep(ITERATION_DELAY);
        }
        catch(Exception ex){
        }
        return verify(operation,carousel,origin,localName,expRes,expVer,show,++iteration,maxIteration);
    }
    
    @Attachment(value = "Root zdts item", type ="text/plain")
    String showRootZdtsItem(ICarousel carousel, ZDtsItem item, long expectedVersion, boolean strictCondition, int attempt){
        StringBuilder sb=new StringBuilder();
        sb.append("Target Host: ").append(carousel.getHost()).append(" Alias: ").append(carousel.getAlias()).
            append(" Attempt: ").append(attempt).append("\n");
        if (NO_VERSION != expectedVersion){
            sb.append("Expected version: ");
            if (!strictCondition)
                sb.append(">");
            sb.append(expectedVersion).append("\n");
        }
        sb.append(ROOT_ZDTS_INI+" item: ").append(item).append("\n");
        return sb.toString();
    }
    
    
    @Attachment(value = "Test result", type ="text/plain")
    String showTest(String host,String local,String remote,String zdts,ZDtsItem item,ExpectedVersion expectedVersion){
        StringBuilder sb=new StringBuilder();
        sb.append("Host: ").append(host).append("\n");
        sb.append("ZDtsItem: ").append(item).append("\n");
        if (NO_VERSION != expectedVersion.version){
            sb.append("Expected version: ");
            if (!expectedVersion.strictCondition)
                sb.append(">");
            sb.append(expectedVersion.version).append("\n");
        }
        sb.append(ZDTS_INI).append(":\n").append(zdts).append("\n");
        sb.append("Local file:\n").append(local).append("\n");
        sb.append("Remote file:\n").append(remote).append("\n");
        return sb.toString();
    }
 
    String createFile(int... ids){
        StringBuilder idStr=new StringBuilder();
        for (int id: ids)
            idStr.append(idStr.length()>0 ? "_" : "").append(id);
        return createFile("file_"+idStr.toString()+".txt");
    }
    
    String createFile(String name){
        return createFile(name,null);
    }
    
    String createFile(String name,String content){
        name = getLocalTmpName(name);
        try( PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name)))){
            if (null == content)
                out.print("FILE = "+name);
            else
                out.print(content);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        return name;
    }
    
    public void addNewFileOnCarousel() throws Exception{
        String file = createFile(ADD_NEW_FILE_ON_CAROUSEL);
        List<ZDtsItem> items = addImpl(ADD_NEW_FILE_ON_CAROUSEL, carouselsIn, carouselsOut, "name"+ADD_NEW_FILE_ON_CAROUSEL+".zdb", file, true);
        setCommonVersion(items);
    }
    
    public void addNewFileOnCarouselNoFlags() throws Exception{
        String file = createFile(ADD_NEW_FILE_ON_CAROUSEL_NO_FLAGS);
        addImplNoFlags(ADD_NEW_FILE_ON_CAROUSEL_NO_FLAGS, carouselsIn, carouselsOut, "name"+ADD_NEW_FILE_ON_CAROUSEL_NO_FLAGS+".zdb", file, true);
        setCommonVersion(null);
    }
    
    public void addNewFileOnCarouselDalinc() throws Exception{
        String file = createFile(ADD_NEW_FILE_ON_CAROUSEL_DALINC);
        List<ZDtsItem> items = addImpl(ADD_NEW_FILE_ON_CAROUSEL_DALINC, carouselsIn, carouselsOut, "name"+ADD_NEW_FILE_ON_CAROUSEL_DALINC+".zdb", file, true, true);
        setCommonVersion(items);
    }
    
    
    public void addExistingFileOnCarousel() throws Exception {   
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD,
            ADD_EXISTING_FILE_ON_CAROUSEL,CREATE_BEFORE_FLAG,ExpectedResult.getFileFlags(),GoldenFile.FIRST);
        //        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.ADD,
        //            ADD_EXISTING_FILE_ON_CAROUSEL, CREATE_BEFORE_FLAG, ExpectedResult.getFileIncFlags(),
        //            dalinc ? GoldenFile.FIRST : GoldenFile.SECOND);
    }
    
      
    
    public void deleteFileFromCarousel() throws Exception{
        String source = "name"+DELETE_FILE_FROM_CAROUSEL+".zdb";
        String file = createFile(DELETE_FILE_FROM_CAROUSEL);
        
        addImpl(UNKNOWN, carouselsIn, carouselsOut, source, file, dalinc, false);
        if (dalinc){
            for (ICarousel carousel: carouselsOut)
                putFileOnCarousel(carousel,file,generateFileName(carousel,source));
        }
        
        PublishTasks tasks = new PublishTasks();
        for (ICarousel carousel: carouselsIn)
            tasks.addTask(new PublishTask(PublishTask.Action.DELETE,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias()),null,null);
        
        sendRequest(carouselType, tasks);
        getTasks();
        
        for (ICarousel carousel: carouselsOut)
            verify(DELETE_FILE_FROM_CAROUSEL, carousel, source, file, dalinc ? ExpectedResult.FILE_EXISTS : 0, 
                new ExpectedVersion(NO_VERSION,true), true);
    }
    
    
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        String source = "name"+DELETE_FILE_FROM_CAROUSEL_NO_FILE_NO_RECORD+".zdb";
        String file = createFile(DELETE_FILE_FROM_CAROUSEL_NO_FILE_NO_RECORD);
        
        PublishTasks tasks = new PublishTasks();
        for (ICarousel carousel: carouselsIn)
            tasks.addTask(new PublishTask(PublishTask.Action.DELETE,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias()),null,null);
        
        sendRequest(carouselType, tasks);
        getTasks();
        
        for (ICarousel carousel: carouselsOut)
            verify(DELETE_FILE_FROM_CAROUSEL_NO_FILE_NO_RECORD,carousel,source,file,0,
                new ExpectedVersion(NO_VERSION,true), true);
    } 
    
    
    public void deleteFileFromCarouselNoFile() throws Exception {
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.DELETE, DELETE_FILE_FROM_CAROUSEL_NO_FILE,
            CREATE_BEFORE_FLAG|DELETE_AFTER_CREATE_BEFORE_FLAG, 0/*ExpectedResult.RECORD_EXISTS*/, GoldenFile.FIRST);
    }
    
    public void deleteFileFromCarouselNoRecord() throws Exception {
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.DELETE, DELETE_FILE_FROM_CAROUSEL_NO_FILE,
            CREATE_BEFORE_NO_RECORD_FLAG, ExpectedResult.FILE_EXISTS, GoldenFile.FIRST);
    }
    
    public void unknownAction() throws Exception {
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.UNKNOWN, UNKNOWN_ACTION, UNKNOWN_ACTION, 0, 0, GoldenFile.FIRST, 500, null, null);
    }
    
    public void unknownServer() throws Exception {
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.ADD, UNKNOWN_SERVER, UNKNOWN_SERVER, 0, 0, GoldenFile.FIRST, 500, 
            new InvalidServerTypeUpdater(), null);
    }
    
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.DELETE, 
            DELETE_FILE_FROM_CAROUSEL_INVALID_CAROUSEL, DELETE_FILE_FROM_CAROUSEL_INVALID_CAROUSEL,
            CREATE_BEFORE_FLAG, ExpectedResult.getFileFlags(), GoldenFile.FIRST, 
            200, new InvalidCarouselUpdater(), new InvalidResponseVerifier1());
    }
    
    public void deleteFileFromCarouselInvalidName() throws Exception {
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.DELETE, 
            DELETE_FILE_FROM_CAROUSEL_INVALID_NAME, DELETE_FILE_FROM_CAROUSEL_INVALID_NAME,
            CREATE_BEFORE_FLAG, ExpectedResult.getFileFlags(), GoldenFile.FIRST,
            200, new InvalidNameUpdater(), new InvalidResponseVerifier1());
    }
    
    public void deleteFileFromCarouselAfterUpdate() throws Exception {
        String source = "name"+DELETE_FILE_FROM_CAROUSEL_AFTER_UPDATE+".zdb";
        String file = createFile(DELETE_FILE_FROM_CAROUSEL_AFTER_UPDATE);
        
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,
            UNKNOWN,DELETE_FILE_FROM_CAROUSEL_AFTER_UPDATE,CREATE_BEFORE_FLAG,ExpectedResult.getFileIncFlags(),GoldenFile.SECOND);
        
        PublishTasks tasks = new PublishTasks();
        for (ICarousel carousel: carouselsIn)
            tasks.addTask(new PublishTask(PublishTask.Action.DELETE,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias()),null,null);
        
        sendRequest(carouselType, tasks);
        getTasks();
        
        for (ICarousel carousel: carouselsOut)
            verify(DELETE_FILE_FROM_CAROUSEL_AFTER_UPDATE,carousel,source,file,0,new ExpectedVersion(NO_VERSION,true),true);
    }
    
    DNCS_MODE getDNCSMode(int operation){
        return getDNCSMode(operation,false);
    }
    
    DNCS_MODE getDNCSMode(int operation,boolean isIniFile){
        if (!isIniFile && dalinc)
            return DNCS_MODE.NOT_REMOVE;
        switch (operation){
            case UPDATE_EXISTING:
            case ADD_MODIFY_EXISTING:
                return DNCS_MODE.REMOVE_OLD;
            case DELETE_FILE_FROM_CAROUSEL:
            case DELETE_FILE_FROM_CAROUSEL_AFTER_UPDATE:
                return (isIniFile) ? DNCS_MODE.REMOVE_OLD : DNCS_MODE.REMOVE_ALL_EXCEPT_RECENT;
            default:
                return DNCS_MODE.NOT_REMOVE;    
        }
    }
            
    public void updateExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,UPDATE_EXISTING,CREATE_BEFORE_FLAG,ExpectedResult.getFileIncFlags(),
            dalinc ? GoldenFile.FIRST : GoldenFile.SECOND);
    }
    
    public void updateExistingInvalidCarousel() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,
            UPDATE_EXISTING_INVALID_CAROUSEL, UPDATE_EXISTING_INVALID_CAROUSEL,
            CREATE_BEFORE_FLAG, ExpectedResult.getFileFlags(), GoldenFile.FIRST, 200, 
            new InvalidCarouselUpdater(), new InvalidResponseVerifier1());
    }
    
    public void updateExistingInvalidName() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,
            UPDATE_EXISTING_INVALID_NAME,UPDATE_EXISTING_INVALID_NAME,
            CREATE_BEFORE_FLAG, ExpectedResult.getFileFlags(), GoldenFile.FIRST, 200 , 
            new InvalidNameUpdater(), new InvalidResponseVerifier1());
    }
    
    public void updateNoExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,UPDATE_NO_EXISTING,0,0);
    }
    
    public void addModifyExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY,ADD_MODIFY_EXISTING,CREATE_BEFORE_FLAG,ExpectedResult.getFileIncFlags(),
            dalinc ? GoldenFile.FIRST : GoldenFile.SECOND);
    }
       
    public void addModifyNoExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY,ADD_MODIFY_NO_EXISTING,0,
            dalinc ? ExpectedResult.RECORD_EXISTS : ExpectedResult.getFileFlags());
    }
    
    public void addModifyNoExistingNoFlags() throws Exception{
        updateImpl(carouselsIn, carouselsOut, PublishTask.Action.ADD_MODIFY,
            ADD_MODIFY_NO_EXISTING_NO_FLAGS, ADD_MODIFY_NO_EXISTING_NO_FLAGS, 0, 0, GoldenFile.FIRST, /*200*/500, 
            new InvalidFlagsUpdater(), null);
    }
     
    public void addModifyNoVerIncExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY_NO_VER_INC,ADD_MODIFY_NO_VER_INC_EXISTING,CREATE_BEFORE_FLAG,ExpectedResult.getFileFlags(),
            dalinc ? GoldenFile.FIRST : GoldenFile.SECOND);
    }
    
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        updateImpl(carouselsIn, carouselsOut, 
            new PublishTask.Action[] {PublishTask.Action.INC_VERSION, PublishTask.Action.ADD_MODIFY_NO_VER_INC}, 
            ADD_MODIFY_NO_VER_INC_INC_VERSION_EXISTING, ADD_MODIFY_NO_VER_INC_INC_VERSION_EXISTING,
            CREATE_BEFORE_FLAG, ExpectedResult.getFileIncFlags(), GoldenFile.SECOND, 200, null, null, 0);
    }
    
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        updateImpl(carouselsIn, carouselsOut, 
            new PublishTask.Action[] {PublishTask.Action.INC_VERSION, PublishTask.Action.ADD_MODIFY_NO_VER_INC}, 
            ADD_MODIFY_NO_VER_INC_INC_VERSION_NO_EXISTING, ADD_MODIFY_NO_VER_INC_INC_VERSION_NO_EXISTING,
            0, ExpectedResult.getFileIncFlags(), GoldenFile.SECOND, 200, null, null, 0);
    }
    
    public void incVersionExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.INC_VERSION,INC_VERSION_EXISTING,CREATE_BEFORE_FLAG,ExpectedResult.getFileIncFlags(),
            GoldenFile.FIRST);
    }
    
    public void incVersionNoExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.INC_VERSION,INC_VERSION_NO_EXISTING,0,0,GoldenFile.FIRST);
    }
     
    public void addModifyNoVerIncNoExisting() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY_NO_VER_INC,ADD_MODIFY_NO_VER_INC_NO_EXISTING,0,ExpectedResult.getFileFlags());
    }
    
    public void addModifyNoVerIncNoExistingNoFlags() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY_NO_VER_INC,
            ADD_MODIFY_NO_VER_INC_NO_EXISTING_NO_FLAGS, ADD_MODIFY_NO_VER_INC_NO_EXISTING_NO_FLAGS,
            0,0,GoldenFile.FIRST,/*200*/500, new InvalidFlagsUpdater(), null);
    }
    
    
    
    public void clearOld() throws Exception{
        
        String regExpr = null;
        String [] valids = null;
        String [] invalids = null;
        
        Method method = FTAllureUtils.getYoungestMethod("clearOld");
        Annotation annotation = method.getAnnotation(IRegExp.class);

        if(annotation instanceof IRegExp){
            IRegExp iregexp  = (IRegExp) annotation;
            regExpr = iregexp.regExp();
            valids = iregexp.validNames();
            invalids = iregexp.invalidNames();
        }
        
        String source;
        String file;
        
        String [][] namesAll=new String[][]{valids,invalids};
        
        PublishTasks tasks = new PublishTasks();
        for (ICarousel carousel: carouselsIn){
            for (String [] names: namesAll){
                for (String name: names) {
                    source = name;
                    file = createFile(name);
                    tasks.addTask(new PublishTask(PublishTask.Action.ADD,carousel.getUid(),getNameWithPrefix(carousel,source),source,carousel.getAlias()),file,source);
                }
            }
        }
        sendRequest(carouselType,tasks);
        
        for (int i=0;i<carouselsOut.size();i++) {
            for (String [] names: namesAll) {
                for (String name: names) {
                    source = name;
                    file = getLocalTmpName(name);
                    verify(UNKNOWN, carouselsOut.get(i), source, file, ExpectedResult.FILE_EXISTS, new ExpectedVersion(1,!aLocEnabled), false);
                }
            }
        }
        
        tasks = new PublishTasks();
        for (ICarousel carousel: carouselsIn){
            source = "regexp.txt";
            file= createFile("regexp.txt",regExpr);
            tasks.addTask(new PublishTask(PublishTask.Action.CLEAR_OLD,carousel.getUid(),"","",carousel.getAlias(),regExpr,carousel.getCarouselHost().getSourcePrefix()),null,null);
        }
        
        sendRequest(carouselType,tasks);
        getTasks();
        
        for (int i=0;i<carouselsOut.size();i++) {
            //these file must be deleted
            for (String name: valids){
                source = name;
                file = getLocalTmpName(name);
                verify(CLEAR_OLD, carouselsOut.get(i), source, file, 0, new ExpectedVersion(NO_VERSION,true), true);
            }
            //these file must not be deleted
            for (String name: invalids){
                source = name;
                file = getLocalTmpName(name);
                verify(CLEAR_OLD, carouselsOut.get(i), source, file, ExpectedResult.FILE_EXISTS, new ExpectedVersion(1,!aLocEnabled), true);
            }
        }
    }

    public void addModifyExistingNoFile() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY,ADD_MODIFY_EXISTING_NO_FILE,CREATE_BEFORE_FLAG|DELETE_AFTER_CREATE_BEFORE_FLAG,ExpectedResult.getFileIncFlags());
    }
    
    public void updateExistingNoFile() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,UPDATE_EXISTING_NO_FILE,CREATE_BEFORE_FLAG|DELETE_AFTER_CREATE_BEFORE_FLAG,ExpectedResult.getFileIncFlags());
    }
    
    public void addModifyExistingNoRecord() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.ADD_MODIFY,ADD_MODIFY_EXISTING_NO_RECORD,CREATE_BEFORE_NO_RECORD_FLAG,ExpectedResult.getFileFlags());
    }
    
    public void updateExistingNoRecord() throws Exception {
        updateImpl(carouselsIn,carouselsOut,PublishTask.Action.UPDATE,UPDATE_EXISTING_NO_RECORD,CREATE_BEFORE_NO_RECORD_FLAG,ExpectedResult.FILE_EXISTS,GoldenFile.FIRST);
    }
    
    public void getPublisherTestAMSLog() throws Exception {
        getServerXml();
        stopAndgetAMSLog(true);
    } 
    
    static class ExpectedResult{
        static final int FILE_EXISTS        = 0b0001;
        static final int RECORD_EXISTS      = 0b0010;
        static final int RECORD_INCREMENTED = 0b0100;
        
        static int getFileFlags(){
            return FILE_EXISTS | RECORD_EXISTS;
        }
        
        static int getFileIncFlags(){
            return FILE_EXISTS | RECORD_INCREMENTED;
        }
        
        static boolean isFileFlags(int flag){
            return 0 != (flag & FILE_EXISTS);
        }
        
        static boolean isRecordFlags(int flag){
            return 0 != (flag & RECORD_EXISTS);
        }
        
        static boolean isRecordIncrementedFlags(int flag){
            return 0 != (flag & RECORD_INCREMENTED);
        }
        
        static boolean isNoRecordFlags(int flag){
            return !(isRecordFlags(flag) || isRecordIncrementedFlags(flag));
        }
    }
    
    enum GoldenFile{
        FIRST,SECOND;
    }
    
    @Attachment(value = "tasks.zip", type = "application/zip")
    public byte[] getTasks() throws IOException{
        return Files.readAllBytes(new File("tasks.zip").toPath());
    }
    
    protected static final String BATCH_ROOT_INI_1_2 = "z_dts.ini.root.batch_zip";
    protected static final String BATCH_ROOT_INI_3 = "z_dts.ini.root.batch_zip.3.0";
    
    protected String getBatchRootIniName(){
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch(version){
            case VERSION_1:
            case VERSION_2:
                return BATCH_ROOT_INI_1_2;
            default:
                return BATCH_ROOT_INI_3;
        }
    } 
}
