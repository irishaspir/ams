package com.zodiac.ams.charter.tests.ft.rwatched;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.iface.HEHandlerType;
import com.dob.test.charter.iface.IHEDataConsumer;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.rwatched.RChannels;
import com.zodiac.ams.charter.services.ft.rwatched.RSTBChannels;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.io.File;
import java.net.URI;

import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;
import static com.zodiac.ams.charter.tests.FeatureList.RECENTLY_WATCHED;

public class RWatchedSTBTest extends FuncTest{

    private static final String INVALID_MAC="XXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    private static final String R_DATA_DIR = "dataR";
    private static final long TIMEOUT =  10_000; //wait for TIMEOUT after CharterSTBEmulator shutdown for stbNotAvailable test


    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.addRWatchedService("http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_R_WATCHED_CONTEXT);
        helper.save("server.xml");
    }

    class RWatchedDataConsumer implements IHEDataConsumer{
        private String received;

        @Override
        public void dataReceived(URI uri, byte[] data) {
            received=new String(data);
            synchronized(sync){
                sync.notify();
            }
        }

        public String getReceived(){
            return received;
        }
    }



    private Object sync=new Object();


    @Attachment(value = "AMS to VHM request", type ="text/plain")
    private String getAMStoVHMRequest(String request){
        return getAMStoVHMRequest(request,null);
    }

    @Attachment(value = "AMS to VHM request", type ="text/plain")
    private String getAMStoVHMRequest(String request,RSTBChannels expected){
        return request+((null == expected) ? "" : "\nExpected: "+expected.toString());
    }

    private String getName(String name){
        return getFileFromJar(R_DATA_DIR, name);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Positive")
    @Description("Request: \"Recently Watched Updated\". The request sent from AMS Server to VHM, which contains recently watched channels.")
    @Test(priority=1)
    public void sendRequestToVHM() throws Exception{
        Assert.assertTrue(waitForAMS(AMS_TIMEOUT),"AMS is not active");

        CharterStbEmulator stb = null;
        LighweightCHEemulator he = null;

        try{
            RSTBChannels channels=new RSTBChannels(new File(getName("rwatched.json.3.ok")));
            RSTBChannels exp = getDefaultExpected(getName("rwatched.json.3.ok"));
            exp.setStatus(null);
            exp.setMAC(properties.getDeviceId());

            stb=createCharterSTBRWatchedEmulator(getName("rwatched.json.3.ok"));
            Assert.assertTrue(null != stb,"Unable to create charter STB emulator");

            RWatchedDataConsumer consumer=new RWatchedDataConsumer();
            he=createCharterHEEmulator(consumer,HEHandlerType.RWATCHED);
            Assert.assertTrue(null != he,"Unable to create charter HE emulator");
            startCharterHEEmulator(he);

            channels.send(stb, properties.getDeviceId());

            synchronized(sync){
                sync.wait();
            }

            String received = consumer.getReceived();
            getAMStoVHMRequest(received,exp);
            Assert.assertTrue(exp.equals(new RSTBChannels(received)),"Invalid response");
        }
        finally{
            if (null != he)
                he.stop();
            if (null != stb)
                stb.stop();
        }
    }

    private RSTBChannels getDefaultExpected(String name) throws Exception{
        RSTBChannels exp=new RSTBChannels(new File(name));
        //exp.setMAC(properties.getDeviceId());
        exp.setMAC(null);
        exp.setCount(null);
        exp.setStartNumber(null);
        exp.setStartTime(null);
        exp.setStatus("OK");
        //RChannels ch=new RChannels();
        //exp.setChannels(ch);
        return exp;
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Positive")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB Channel count =1")
    @Test(priority=2)
    public void sendRequestToAMS1() throws Exception{
        RSTBChannels exp = getDefaultExpected(getName("rwatched.json.1.ok"));
        testImpl(getName("rwatched.json.1.ok"),200,exp);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Positive")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB Channel count >1")
    @Test(priority=3)
    public void sendRequestToAMS() throws Exception{

        try{
            Thread.sleep(TIMEOUT);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }


        RSTBChannels exp = getDefaultExpected(getName("rwatched.json.3.ok"));
        testImpl(getName("rwatched.json.3.ok"),200,exp);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Positive")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB Channel count =0")
    @Test(priority=4)
    public void sendRequestToAMS0() throws Exception{
        RSTBChannels exp = getDefaultExpected(getName("rwatched.json.0.ok"));
        RChannels ch=new RChannels();
        exp.setChannels(ch);
        testImpl(getName("rwatched.json.0.ok"),200,exp);
    }


    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB returns empty response")
    @Test(priority=5)
    public void badResponse0() throws Exception{
        testImpl(getName("rwatched.json.0"),500);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB returns invalid response: Channel count > 0 , but startNumber,startTime are missing ")
    @Test(priority=6)
    public void badResponse1() throws Exception{
        testImpl(getName("rwatched.json.1"),500);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB returns invalid response: Channel count > 0 ,but channelList is missing ")
    @Test(priority=7)
    public void badResponse2() throws Exception{
        testImpl(getName("rwatched.json.2"),500);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB returns invalid response: lastWatchedTime in channelList is missing ")
    @Test(priority=8)
    public void badResponse3() throws Exception{
        testImpl(getName("rwatched.json.3"),500);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB returns invalid response: channelNumber in channelList is missing ")
    @Test(priority=9)
    public void badResponse4() throws Exception{
        testImpl(getName("rwatched.json.4"),500);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, MAC address is missing")
    @Test(priority=10)
    public void missingMACAddress(){
        testImpl(null,getName("rwatched.json.3.ok"),400,null,true);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, MAC address is invalid")
    @Test(priority=11)
    public void invalidMACAddress(){
        testImpl(INVALID_MAC,getName("rwatched.json.3.ok"),400,null,true);
    }

    @Features(RECENTLY_WATCHED)
    @Stories("Negative")
    @Description("Request: \"Force Recently Watched Update\". VHM sends request to AMS Server, STB is not available")
    @Test(priority=12)
    public void stbNotAvailable(){
        try{
            Thread.sleep(TIMEOUT);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        testImpl(properties.getDeviceId(),getName("rwatched.json.3.ok"),500,null,false);
    }


    private void testImpl(String name,int expectedCode){
        testImpl(properties.getDeviceId(),name,expectedCode,null,true);
    }

    private void testImpl(String name,int expectedCode,RSTBChannels expectedResponse){
        testImpl(properties.getDeviceId(),name,expectedCode,expectedResponse,true);
    }

    private void testImpl(String mac,String name,int expectedCode,RSTBChannels expectedResponse,boolean start){
        CharterStbEmulator emulator=null;
        if (start){
            emulator=createCharterSTBRWatchedEmulator(name);
            Assert.assertTrue(null != emulator,"Unable to create charter STB emulator");
            startCharterSTBEmulator(emulator);
        }

        try{
            String uri=properties.getRWatchUri()+((null == mac) ? "" : "?mac=" + mac);
            FTHttpUtils.HttpResult response=FTHttpUtils.doGet(uri);
            getHTTPGetStatus(uri,response,(null == expectedResponse) ? null : expectedResponse.toString());
            Assert.assertTrue(expectedCode == response.getStatus(),"Invalid status");
            if (null != expectedResponse)
                Assert.assertTrue(expectedResponse.equals(new RSTBChannels(response.toString())),"Invalid response");
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        finally{
            if (null != emulator)
                emulator.stop();
        }
    }

    @Features(RECENTLY_WATCHED)
    @Stories("AMS logs")
    @Description("Recenyly Watched Tests AMS log")
    @Test(priority=13)
    public void getRWatchedTestAMSLog() throws Exception {
        stopAndgetAMSLog(true);
    }
    
}
