package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.connectionMode;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.messageTypesMap;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.vendor;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_1_STRING;

public class ConnectionModeNotificationVersion1MessageTypesMapTests extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified" +
            "messageTypesMap changes")
    @Test(dataProvider = "messageTypesMap", dataProviderClass = DSGDataProvider.class)
    public void sendVersion1RequestId0VendorIsSpecifiedMessageTypesMapSettings(long messageTypesMap) {
        message = version1Request.createMessage(mac, messageTypesMap);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(messageTypesMap, mac);
        assertJson(mac, connectionMode, getBoxModel(),(Object) vendor);
        assertLog(log, mac, connectionMode, messageTypesMap);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified " +
            "messageTypesMap unknown")
    @Test()
    public void sendVersion1RequestId0VendorIsSpecifiedRomIdMessageTypesMapUnknown() {
        messageTypesMap = 5;
        message = version1Request.createMessage(mac, messageTypesMap);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(messageTypesMap, mac);
        assertJson(mac,connectionMode, getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, messageTypesMap);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified " +
            "all service use version 1 protocols")
    @Test()
    public void sendVersion1RequestId0VendorIsSpecifiedRomIdMessageTypesMapAllRange() {
        messageTypesMap = 73300775185L;
        message = version1Request.createMessage(mac, messageTypesMap);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(messageTypesMap, mac);
        assertJson(mac,connectionMode, getBoxModel(), (Object) vendor);
           assertLog(log, mac, connectionMode, messageTypesMap);
    }
}
