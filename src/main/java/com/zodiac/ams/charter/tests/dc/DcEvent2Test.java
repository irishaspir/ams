package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_TWO;

/**
 * The tests suite for DC event 2.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcEvent2Test extends StepsForDcEvent {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_POSITIVE + "1 - Check Activity.csv where some parameteres = 0")
    @Description("4.2.1 [DC][Event 2] Check Activity.csv where some parameteres = 0" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><0 | 0 | 0 | 0 | 0>}]")
    public void sendMessageCheckActivityCSVWhereSomeParameteresAreZero() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_TWO;
        // parameters
        final int parameterSessionId = 0;
        final int parameterSessionActivityId = 0;
        final int parameterScreen = 0;
        final int parameterReason = 0;
        final int parameterChannel = 0;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                .addParameter(parameterSessionId)
                .addParameter(parameterSessionActivityId)
                .addParameter(parameterScreen)
                .addParameter(parameterReason)
                .addParameter(parameterChannel)
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                .addParameter(parameterSessionId)
                .addParameter(parameterSessionActivityId)
                .addParameter(parameterScreen)
                .addParameter(parameterReason)
                .addParameter(parameterChannel)
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_POSITIVE + "2 - Check positive reason")
    @Description("4.2.2 [DC][Event 2] Check positive reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 8 | 3 | reason | 10161>}], where reason is from 1 to 207")
    public void sendMessageWithPositiveReason() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_TWO;
        // parameters
        final int parameterSessionId = 1484729891;
        final int parameterSessionActivityId = 8;
        final int parameterScreen = 3;
        final int parameterReason = randomParameterReasonForEvent2;
        final int parameterChannel = 10161;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                .addParameter(parameterSessionId)
                .addParameter(parameterSessionActivityId)
                .addParameter(parameterScreen)
                .addParameter(parameterReason)
                .addParameter(parameterChannel)
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                .addParameter(parameterSessionId)
                .addParameter(parameterSessionActivityId)
                .addParameter(parameterScreen)
                .addParameter(parameterReason)
                .addParameter(parameterChannel)
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion

    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_POSITIVE + "3 - Check positive sessionActivityId and screen")
    @Description("4.2.3 [DC][Event 2] Check positive sessionActivityId and screen" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | sessionActivityId | screen | 17 | 10161>}]")
    public void sendMessageWithPositiveSessionActivityIdAndScreen() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_TWO;
        // parameters
        final int parameterSessionId = 1484729891;
        final int parameterSessionActivityId = 29;
        final int parameterScreen = 76;
        final int parameterReason = 17;
        final int parameterChannel = 10161;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                .addParameter(parameterSessionId)
                .addParameter(parameterSessionActivityId)
                .addParameter(parameterScreen)
                .addParameter(parameterReason)
                .addParameter(parameterChannel)
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)
                .addParameter(parameterSessionId)
                .addParameter(parameterSessionActivityId)
                .addParameter(parameterScreen)
                .addParameter(parameterReason)
                .addParameter(parameterChannel)
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion

    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "4 - Check negative channel")
    @Description("4.2.4 [DC][Event 2] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 2 | 1 | 17 | 234234234234>}]")
    public void sendMessageWithIncorrectChannel1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(2) // sessionActivityId
                .addParameter(1) // screen
                .addParameter(17) // reason
                // channel must be incorrect, this is the test purpose!
                .addParameter("234234234234") // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "4 - Check negative channel")
    @Description("4.2.4 [DC][Event 2] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 2 | 1 | 17 | ewrqwerqrwe>}]")
    public void sendMessageWithIncorrectChannel2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(2) // sessionActivityId
                .addParameter(1) // screen
                .addParameter(17) // reason
                // channel must be a garbage, this is the test purpose!
                .addParameter("ewrqwerqrwe") // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "4 - Check negative channel")
    @Description("4.2.4 [DC][Event 2] Check negative channel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 2 | 1 | 17 | @#$@#$>}]")
    public void sendMessageWithIncorrectChannel3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(2) // sessionActivityId
                .addParameter(1) // screen
                .addParameter(17) // reason
                // channel must be a garbage, this is the test purpose!
                .addParameter("@#$@#$") // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "5 - Check negative reason")
    @Description("4.2.5 [DC][Event 2] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 7 | 1 | 3452354434 | 10161>}]")
    public void sendMessageWithIncorrectReason1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(7) // sessionActivityId
                .addParameter(1) // screen
                // reason must be incorrect, this is the test purpose!
                .addParameter("3452354434") // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "5 - Check negative reason")
    @Description("4.2.5 [DC][Event 2] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 7 | 1 | sdfsdf | 10161>}]")
    public void sendMessageWithIncorrectReason2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(7) // sessionActivityId
                .addParameter(1) // screen
                // reason must be garbage, this is the test purpose!
                .addParameter("sdfsdf") // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "5 - Check negative reason")
    @Description("4.2.5 [DC][Event 2] Check negative reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 7 | 1 | #$%#$% | 10161>}]")
    public void sendMessageWithIncorrectReason3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(7) // sessionActivityId
                .addParameter(1) // screen
                // reason must be garbage, this is the test purpose!
                .addParameter("#$%#$%") // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "6 - Check negative screen")
    @Description("4.2.6 [DC][Event 2] Check negative screen" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 2 | 345324444 | 17 | 10161>}]")
    public void sendMessageWithIncorrectScreen1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(2) // sessionActivityId
                // screen must be garbage, this is the test purpose!
                .addParameter("345324444") // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "6 - Check negative screen")
    @Description("4.2.6 [DC][Event 2] Check negative screen" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 2 | dfgdfg | 17 | 10161>}]")
    public void sendMessageWithIncorrectScreen2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(2) // sessionActivityId
                // screen must be garbage, this is the test purpose!
                .addParameter("dfgdfg") // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "6 - Check negative screen")
    @Description("4.2.6 [DC][Event 2] Check negative screen" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | 2 | #$#%$#$ | 17 | 10161>}]")
    public void sendMessageWithIncorrectScreen3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                .addParameter(2) // sessionActivityId
                // screen must be garbage, this is the test purpose!
                .addParameter("#$#%$#$") // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "7 - Check negative sessionActivityId")
    @Description("4.2.7 [DC][Event 2] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891| 834356346 | 3 | 17| 10161>}]")
    public void sendMessageWithIncorrectSessionActivityId1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("834356346") // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "7 - Check negative sessionActivityId")
    @Description("4.2.7 [DC][Event 2] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 |                 | 3 | 17| 10161>}]")
    public void sendMessageWithIncorrectSessionActivityId2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("                 ") // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "7 - Check negative sessionActivityId")
    @Description("4.2.7 [DC][Event 2] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729891 | etretrert | 3 | 17| 10161>}]")
    public void sendMessageWithIncorrectSessionActivityId3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("etretrert") // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "7 - Check negative sessionActivityId")
    @Description("4.2.7 [DC][Event 2] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><1484729890 | $#%#$%^#$% | 3 | 17| 10161>}]")
    public void sendMessageWithIncorrectSessionActivityId4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                .addParameter(1484729891) // sessionId
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("$#%#$%^#$%") // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "8 - Check negative sessionID")
    @Description("4.2.8 [DC][Event 2] Check negative sessionID" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><fasdsd | 8 | 3 | 17| 10161>}]")
    public void sendMessageWithIncorrectSessionId1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                // sessionId must be garbage, this is the test purpose!
                .addParameter("fasdsd") // sessionId
                .addParameter(8) // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "8 - Check negative sessionID")
    @Description("4.2.8 [DC][Event 2] Check negative sessionID" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><        | 8 | 3 | 17 | 10161>}]")
    public void sendMessageWithIncorrectSessionId2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                // sessionId must be garbage, this is the test purpose!
                .addParameter("        ") // sessionId
                .addParameter(8) // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "8 - Check negative sessionID")
    @Description("4.2.8 [DC][Event 2] Check negative sessionID" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><#$%#$%$% | 8 | 3 | 17 | 10161>}]")
    public void sendMessageWithIncorrectSessionId3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                // sessionId must be garbage, this is the test purpose!
                .addParameter("#$%#$%$%") // sessionId
                .addParameter(8) // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionId | *sessionActivityId* | *screen* | reason | channel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_2_NEGATIVE + "8 - Check negative sessionID")
    @Description("4.2.8 [DC][Event 2] Check negative sessionID" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><2><148472983452435234591 | 8 | 3 | 17 | 10161>}]")
    public void sendMessageWithIncorrectSessionId4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_TWO)
                // sessionId must be garbage, this is the test purpose!
                .addParameter("148472983452435234591") // sessionId
                .addParameter(8) // sessionActivityId
                .addParameter(3) // screen
                .addParameter(17) // reason
                .addParameter(10161) // channel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
