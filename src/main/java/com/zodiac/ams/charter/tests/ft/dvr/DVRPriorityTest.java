package com.zodiac.ams.charter.tests.ft.dvr;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.handlers.UniversalListener;
import com.zodiac.ams.charter.helpers.ft.FtDbHelper;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.dvr.DvrDbHelper;
import com.zodiac.ams.charter.services.ft.dvr.PriorityRequest;
import com.zodiac.ams.charter.services.ft.dvr.PriorityResponse;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

import static com.zodiac.ams.charter.tests.FeatureList.DVR;

public class DVRPriorityTest extends DVRTest {
    
    private static final String POSITIVE = "Set Recordings Priority. Positive";
    private static final String NEGATIVE = "Set Recordings Priority. Negative";
    private static final int S_OK = 200;
    private static final int BAD_REQUEST = 400;
    
    @Override
    public boolean needRestart(){
        return false;
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.1.1 [DVR][Prioritize] Priority_vector is valid")
    @Test(priority=2)
    public void test_6_5_1_1() throws Exception {
        positiveTestImpl();
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.5.1.2 [DVR][Prioritize] Priority_vector is not an array")
    @Test(priority=2)
    public void test_6_5_1_2() throws Exception {
        badRequest(p -> p.setPrioities(1_234_567));
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.5.1.3 [DVR][Prioritize] Priority_vector is empy")
    @Test(priority=2)
    public void test_6_5_1_3() throws Exception {
        badRequest(p -> p.setPrioities(""));
    }
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.5.1.4 [DVR][Prioritize] Priority_vector is missing")
    @Test(priority=2)
    public void test_6_5_1_4() throws Exception {
        badRequest(p -> p.setPrioities(null));
    }
    
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.1 [DVR][Prioritize] Error '1 - ERROR, no operation has been accomplished'")
    @Test(priority=3)
    public void test_6_5_2_1() throws Exception {
        errorImpl(Error.Error1);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.2 [DVR][Prioritize] Error '3 - No target program exists'")
    @Test(priority=3)
    public void test_6_5_2_2() throws Exception {
        errorImpl(Error.Error3);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.3 [DVR][Prioritize] Error '5 - No programs exist for the list'")
    @Test(priority=3)
    public void test_6_5_2_3() throws Exception {
        errorImpl(Error.Error5);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.4 [DVR][Prioritize] Error '7 - DVR is not authorized on the STB'")
    @Test(priority=3)
    public void test_6_5_2_4() throws Exception {
        errorImpl(Error.Error7);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.5 [DVR][Prioritize] Error '10 -DVR is not ready (still initializing)'")
    @Test(priority=3)
    public void test_6_5_2_5() throws Exception {
        errorImpl(Error.Error10);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.6 [DVR][Prioritize] Error '13 - Invalid parameter was specified'")
    @Test(priority=3)
    public void test_6_5_2_6() throws Exception {
        errorImpl(Error.Error13);
    }

    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.7 [DVR][Prioritize] Error '16 - Unknown error'")
    @Test(priority=3)
    public void test_6_5_2_7() throws Exception {
        errorImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories(POSITIVE)
    @Description("6.5.2.8 [DVR][Prioritize] Error '17 - STB is busy'")
    @Test(priority=3)
    public void test_6_5_2_8() throws Exception {
        errorImpl(Error.Error17);
    }
    
    
    @Features(DVR)
    @Stories(NEGATIVE)
    @Description("6.5.2.9 [DVR][Prioritize] Recording doesn't exist in DVR_STB")
    @Test(priority=3)
    public void test_6_5_2_9() throws Exception {
        testImpl(null, p -> p.setError(Error.STB_INVALID_RESPONSE), S_OK, true);
    }


    private void errorImpl(Error error) throws Exception{
        testImpl(null, p -> p.setError(error), S_OK, false);
    }   
    
    private void badRequest(Consumer<PriorityRequest> consumerReq) throws Exception {
        testImpl(consumerReq, null, BAD_REQUEST, false);
    }         
    
    private void positiveTestImpl() throws Exception{
        testImpl(null, null, S_OK, false);
    }
    
    private void testImpl(Consumer<PriorityRequest> consumerReq, Consumer<PriorityResponse> consumerResp, int expStatus, boolean noDvrStb) throws Exception{
        
        PriorityRequest request = PriorityRequest.getRandom(deviceId, 5);
        if (null != consumerReq)
            consumerReq.accept(request);
        
        PriorityResponse response = new PriorityResponse();
        if (null !=  consumerResp)
            consumerResp.accept(response);
        
        prepareDB(RandomUtils.getRandomInt(1, 1_000_000),
            RandomUtils.getRandomObject(Type.values()).getCode());
        
        if (noDvrStb){
            FtDbHelper.deleteDB("DVR_STB", new HashMap(){{put("MAC_STR", deviceId);}});
            FtDbHelper.deleteCassandra("STB", new HashMap(){{put("DEVICEID", deviceId);}});
        }
        
        CharterStbEmulator stb = null;
        DataConsumer consumer =  null;
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20*1_000);
        
        try {
            
            if (S_OK == expStatus){
                Semaphore semaphore = new Semaphore(0);
                consumer = new DataConsumer(semaphore);
                stb = createCharterSTBEmulator(STB_PROPS);
                stb.registerConsumer(consumer, HandlerType.Universal);
                UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
                listener.setResponse(response.getData());            
                startCharterSTBEmulator(stb);
            }
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, request.toString());
//            Thread.sleep(60*60*1_000);
            
            
            byte[] actual = (null == consumer) ?  null : consumer.getData();
            
            //show HE-AMS request/response
            showHERequestToAMS(uri, request.toString()/*request*/, (S_OK == expStatus) ? response.toString() : null/*expected response*/, httpResponse /*actual response*/);
            
            List<Integer> exp = null;
            List<Integer> dbPri = null;
            List<Integer> csPri = null;
            
            if (S_OK == expStatus){
                //show AMS-STB expected/actual request
                showAMSRequestToSTB(request.getData()/*expected*/, actual/*actual*/);
            
                //show expected/actual priority vector
                exp = Error.Error0 == response.getError() ? request.getPriorities() : new ArrayList<>();
                dbPri = DvrDbHelper.getDBPriorityVector(deviceId);
                csPri = DvrDbHelper.getCassandraPriorityVector(deviceId);
                showDBPriorityVector(exp, dbPri);
                showCassandraPriorityVector(exp, csPri);
            }
            
            //verify response status
            Assert.assertTrue(expStatus == httpResponse.getStatus(), "Expected status: " + expStatus + " Actual: " + httpResponse.getStatus());        
            
            if (200 == expStatus){
                //verify AMS-HE response
                verifyJSON(response.toJSON(), new JSONObject((String)httpResponse.getEntity()),"Expected and actual AMS response must be the same");
                //verify AMS-STB request
                Assert.assertTrue(Arrays.equals(request.getData(), actual), "Expected and actual request from AMS to STB must be the same");
                //verify priority vector
                Assert.assertTrue(Arrays.equals(exp.toArray(new Integer[]{}), dbPri.toArray(new Integer[]{})), 
                    "Expected and actual DB priority vector must be the same");
                Assert.assertTrue(Arrays.equals(exp.toArray(new Integer[]{}), csPri.toArray(new Integer[]{})), 
                    "Expected and actual Cassandra priority vector must be the same");
            }
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
    
}
