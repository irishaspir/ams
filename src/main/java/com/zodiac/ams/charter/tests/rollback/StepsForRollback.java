package com.zodiac.ams.charter.tests.rollback;

import com.zodiac.ams.charter.emuls.stb.rollback.RollbackSTBEmulator;
import com.zodiac.ams.charter.http.helpers.rollback.RollbackJsonDataUtils;
import com.zodiac.ams.charter.rudp.rollback.RollbackRudpHelper;
import com.zodiac.ams.charter.rudp.rollback.message.RollbackSTBRequestPattern;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.charter.tomcat.logger.rollback.RollbackLogParser;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class StepsForRollback extends Steps {

    private RollbackSTBEmulator rollbackSTBEmulator;
    protected RollbackRudpHelper rollbackRudpHelper;
    protected RollbackJsonDataUtils rollbackJsonDataUtils= new RollbackJsonDataUtils();
    protected RollbackSTBRequestPattern rollbackSTBRequestPattern = new RollbackSTBRequestPattern();
    protected RollbackLogParser rollbackLogParser = new RollbackLogParser();
    protected List<String> log = new ArrayList<>();

    @Step
    public List<String> readAMSLog() {
        log = parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT);
        return log;
    }

    @Step
    protected void runSTBRollback() {
        rollbackSTBEmulator = new RollbackSTBEmulator();
        rollbackSTBEmulator.registerConsumer();
        rollbackSTBEmulator.start();
        Logger.info("STB Emulator's run");
        rollbackRudpHelper = new RollbackRudpHelper(rollbackSTBEmulator);
    }

    @Step
    protected void runSTBRollbackWithErrorStatus() {
        rollbackSTBEmulator = new RollbackSTBEmulator(1);
        rollbackSTBEmulator.registerConsumer();
        rollbackSTBEmulator.start();
        Logger.info("STB Emulator's run");
        rollbackRudpHelper = new RollbackRudpHelper(rollbackSTBEmulator);
    }

    @Step
    protected void stopSTBRollback() {
        rollbackSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
    }

    @Step
    protected void getTestTime() {
        startTestTime = getCurrentTimeTest();
    }

}
