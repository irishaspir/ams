package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ELEVEN;
import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_SIX;
import static org.testng.Assert.fail;

/**
 * The tests suite for DC event 11.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcEvent11Test extends StepsForDcEvent {
    
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_11_POSITIVE + "1 - Check positive sessionActivityId")
    @Description("4.11.1 [DC][Event 11] Check positive sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><11>< sessionActivityId >}], where sessionActivityId is from 1 to 40")
    public void sendMessageWithPositiveSessionActivityId() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_ELEVEN;
        // parameters
        final int parameterSessionActivityId = randomParameterSessionActivityIdForEvent11;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterSessionActivityId)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterSessionActivityId)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_11_NEGATIVE + "2 - Check negative sessionActivityId")
    @Description("4.11.2 [DC][Event 11] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><11><0>}]")
    public void sendMessageWithIncorrectSessionActivityId1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter(0) // sessionActivityId
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_11_NEGATIVE + "2 - Check negative sessionActivityId")
    @Description("4.11.2 [DC][Event 11] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><11><iuyiuyiu>}]")
    public void sendMessageWithIncorrectSessionActivityId2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("iuyiuyiu") // sessionActivityId
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_11_NEGATIVE + "2 - Check negative sessionActivityId")
    @Description("4.11.2 [DC][Event 11] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><11><#$%#$%>}]")
    public void sendMessageWithIncorrectSessionActivityId3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("#$%#$%") // sessionActivityId
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_11_NEGATIVE + "2 - Check negative sessionActivityId")
    @Description("4.11.2 [DC][Event 11] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><11><       >}]")
    public void sendMessageWithIncorrectSessionActivityId4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter("       ") // sessionActivityId
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><sessionActivityId>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_11_NEGATIVE + "2 - Check negative sessionActivityId")
    @Description("4.11.2 [DC][Event 11] Check negative sessionActivityId" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><11><454356450>}]")
    public void sendMessageWithIncorrectSessionActivityId5() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ELEVEN)
                // sessionActivityId must be garbage, this is the test purpose!
                .addParameter(454356450) // sessionActivityId
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
