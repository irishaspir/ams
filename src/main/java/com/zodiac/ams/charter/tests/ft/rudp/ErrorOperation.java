package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import org.testng.Assert;

public class ErrorOperation extends Operation {
    
    private final ErrorCodeType expectedError;
    
    ErrorOperation(){
        this.expectedError = null;
    }
    
     ErrorOperation(ErrorCodeType expectedError){
        this.expectedError = expectedError;
    }
    
    @Override
    public void publish() throws Exception{
        for (int id: sessionsIds){
            if (null == expectedError)
                Publisher.showFileTransferStatictics(id, false);
            else
                Publisher.showErrorCode(id, expectedError);
        }
    }
    
    @Override
    public boolean isSuccess(){
        for (int id: sessionsIds){
            RUDPTestingSession ts = getTestingSession(id);
            Assert.assertTrue(null != ts, "RUDP Testing Session id="+id+" must not be null");
            Assert.assertTrue(ts.isSuccess(),ts.getErrorMsg());
            if (null != expectedError)
                Assert.assertTrue(expectedError.equals(getErrorCodeType(id)),"Error must be "+expectedError);
        }
        return true;
    }
    
    
    @Override
    public boolean isSuccessAfterSession(int sessionId) throws Exception{
        return getExpectedFailureSessionResult(sessionId);
    }
    
}
