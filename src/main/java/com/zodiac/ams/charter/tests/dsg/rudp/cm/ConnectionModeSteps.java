package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.emuls.che.dsg.DsgCHEEmulator;
import com.zodiac.ams.charter.emuls.stb.dsg.DSGSTBEmulator;
import com.zodiac.ams.charter.rudp.dsg.message.cm.Version0Request;
import com.zodiac.ams.charter.rudp.dsg.message.cm.Version1Request;
import com.zodiac.ams.charter.rudp.dsg.message.cm.Version2Request;
import com.zodiac.ams.charter.rudp.dsg.message.cm.Version3Request;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.*;
import static com.zodiac.ams.charter.runner.Utils.isLocalIp;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.boxModel;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.vendor;
import static org.testng.Assert.assertEquals;

public class ConnectionModeSteps extends Steps {

    protected DSGSTBEmulator dsgSTBEmulator;
    protected DsgCHEEmulator dsgCHEEmulator;
    protected Version0Request version0Request;
    protected Version1Request version1Request;
    protected Version2Request version2Request;
    protected Version3Request version3Request;
    protected List<String> log = new ArrayList<>();
    private static int time = (int) (Math.random() * 3);

    @Step
    protected void runSTBForDSG() {
        dsgSTBEmulator = new DSGSTBEmulator();
        dsgSTBEmulator.registerConsumer();
        dsgSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        version0Request = new Version0Request(dsgSTBEmulator);
        version1Request = new Version1Request(dsgSTBEmulator);
        version2Request = new Version2Request(dsgSTBEmulator);
        version3Request = new Version3Request(dsgSTBEmulator);
    }

    @Step
    protected void runCHE() {
        dsgCHEEmulator = new DsgCHEEmulator();
        dsgCHEEmulator.registerConsumer();
        dsgCHEEmulator.start();
        Logger.info("CHE Emulator's run");
    }

    @Step
    protected void stopSTB() {
        dsgSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
    }

    @Step
    protected void stopSettingsCHE() {
        dsgCHEEmulator.stop();
        Logger.info("CHE Emulator's stopped");
    }
    @Step
    public List<String> readAMSLog() {
        if (isLocalIp()) {
            log = getLogFile();
            Logger.info("Get Log!");
        } else log = parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT);
        return log;
    }

    @Step
    public static int changeConnectionMode() {
        time = (int) (Math.random() * 3);
        Object[] connectionMode = new Object[]{DOCSIS, ALOHA, DAVIC};
        if (3 == time) {
            time = 0;
        }
        return (int) connectionMode[time];
    }

    @Step
    public void assertDB(int romId, int vendor, int modelId, String mac) {
        assertEquals(romId, getRomIdFromSTBMetadata(mac), ROM_ID + NOT_EQUAL);
        assertEquals(vendor, getVendorIdFromSTBMetadata(mac), VENDOR_ID + NOT_EQUAL);
        assertEquals(modelId, getStbModelIdFromSTBMetadata(mac), BOX_MODEL_ID + NOT_EQUAL);
    }

    @Step
    private void assertDBDefault(String mac) {
        assertEquals(boxModel, getRomIdFromSTBMetadata(mac), ROM_ID + NOT_EQUAL);
        assertEquals(vendor, getVendorIdFromSTBMetadata(mac), VENDOR_ID + NOT_EQUAL);
        assertEquals(getModelId(UNKNOWN), getStbModelIdFromSTBMetadata(mac), BOX_MODEL_ID + NOT_EQUAL);
    }

    @Step
    public void assertDB(long messageTypesMap, String mac) {
        assertDBDefault(mac);
        assertEquals(messageTypesMap, getMsgTypesMap(mac), NOT_EQUAL);
    }

    @Step
    public void assertDB(long massageTypesMap, int hubId, String mac) {
        assertDB(massageTypesMap, mac);
        assertEquals(getHubIdFromSTBMetadata(mac), hubId, HUB_ID + NOT_EQUAL);
    }
}
