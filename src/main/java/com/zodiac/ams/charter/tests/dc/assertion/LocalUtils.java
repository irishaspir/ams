package com.zodiac.ams.charter.tests.dc.assertion;

import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

/**
 * The local utilities.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public class LocalUtils {
    /*
     * The private constructor.
     */
    private LocalUtils() {
        // do nothing here
    }

    /**
     * The method to calculate events count.
     *
     * @param input the CSV file assertion input
     */
    public static int calculateInputEventsCount(CsvFileAssertionInput input) {
        int counter = 0;
        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();
            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                counter++;
            }
        }

        return counter;
    }

    /**
     * The method to calculate time delta sum.
     *
     * @param sessionData the session data
     */
    public static int calculateInputTimeDeltaSum(CsvFileAssertionInput.SessionData sessionData) {
        int inputTimeDeltaSum = 0;

        // we need to iterate through the list of event data to calculate the sum of time deltas
        List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();
        for (CsvFileAssertionInput.EventData eventData1 : eventDataList) {
            int inputTimeDelta = eventData1.getTimeDelta();
            inputTimeDeltaSum += inputTimeDelta;
        }

        return inputTimeDeltaSum;
    }

    public static LocalDateTime toLocalDateTime(long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.of("UTC"));
    }

    public static String toMillisString(long millis) {
        StringBuilder result = new StringBuilder();
        if (millis < 100) {
            result.append(0);
        }
        if (millis < 10) {
            result.append(0);
        }
        result.append(millis);

        return result.toString();
    }

    public static boolean compareSafely(Integer valueObject1, int value2) {
        return (valueObject1 != null && valueObject1 == value2);
    }

    public static boolean compareSafelyAgainst0(Integer valueObject) {
        return compareSafely(valueObject, 0);
    }
}
