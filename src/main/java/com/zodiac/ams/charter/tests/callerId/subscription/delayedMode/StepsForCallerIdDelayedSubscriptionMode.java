package com.zodiac.ams.charter.tests.callerId.subscription.delayedMode;

import com.zodiac.ams.charter.tests.callerId.subscription.StepsForCallerIdSubscription;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class StepsForCallerIdDelayedSubscriptionMode extends StepsForCallerIdSubscription {

    @AfterMethod
    public void afterMethod() {
        stopSTBCallerId();
    }
}
