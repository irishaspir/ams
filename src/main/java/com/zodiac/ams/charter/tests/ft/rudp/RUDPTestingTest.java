package com.zodiac.ams.charter.tests.ft.rudp;

import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.iface.HEHandlerType;
import com.dob.test.charter.iface.IHEDataConsumer;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.rudp.AckHandler;
import com.zodiac.ams.charter.services.ft.rudp.IAckHandler;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPReceiverSession;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getSegmentSize;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import static com.zodiac.ams.charter.tests.FeatureList.RUDP;
import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.*;

/**
 *
 * @author alexander.filippov
 */
public class RUDPTestingTest extends FuncTest{

    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.addRUDPTestingService("http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_RUDP_TESTING_CONTEXT);
        helper.save("server.xml");
    }
        
    private static class RUDPTestingDataConsumer implements IHEDataConsumer{    
        @Override
        public void dataReceived(URI uri, byte[] data) {   
            int sessionId = FTUtils.getIntFromBytes(data);
            RUDPTestingSession ts = getTestingSession(sessionId);
            if (null != ts){
                ts.setRemoteData(data);
                ts.getSemaphore().release();
            }
        }
    }
   
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sends the file smaller than the segment size. "+
            "Expected result: The server receives file without an error. The sent file is identical to the received file.")
    @Test(priority=1)
    public void TC_RUDP_P_001() throws Exception{
        Assert.assertTrue(waitForAMS(AMS_TIMEOUT),"AMS is not active");
        sendImpl(getSegmentSize()/2, ORDER.EMPTY);
    }
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sends the file, the size of which is exactly equal to the size of the segment. "+
            "Expected result: The server receives file without an error. The sent file is identical to the received file.")
    @Test(priority=2)
    public void TC_RUDP_P_002() throws Exception{
        sendImpl(getSegmentSize(), ORDER.EMPTY);
    }
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sends a file larger than 1 kb (that it is divided at least into 3 segments). "+
            "Segments, except the last one are sent out of order. "+
            "Expected result: The server receives file without an error. The sent file is identical to the received file.")
    @Test(priority=3)
    public void TC_RUDP_P_003() throws Exception{
        sendImpl(3*getSegmentSize() + getSegmentSize()/2, ORDER.RANDOM_LAST_IS_LAST);
    }      
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sends a file, the size of which is not a multiple of the size of the segment, but the most of it. "+
            "The last segment of the file is sent first, the others - in any order. "+
            "Expected result: The server receives file without an error. The sent file is identical to the received file.")
    @Test(priority=4)
    public void TC_RUDP_P_004() throws Exception{
        sendImpl(4*getSegmentSize() + getSegmentSize()/2, ORDER.RANDOM_LAST_IS_FIRST);
    }
    
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sends the file which size is a multiple of the segment size, i.e. the CRC gets a separate segment. "+
            "CRC segment is sent first, and the rest - in any order. "+
            "Expected result: The server receives file without an error. The sent file is identical to the received file.")
    @Test(priority=5)
    public void TC_RUDP_P_005() throws Exception{
        sendImpl(getSize(true),ORDER.RANDOM_LAST_IS_FIRST);
    }
    
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sends a START packet and waits for confirmation, after which suspends file sending to the session timeout, "+
            "and then continues transfer DATA packets. "+
            "Expected result: in response to the sending of DATA packet FILEID error is returned")
    @Test(priority=6)
    public void TC_RUDP_P_006() throws Exception{
        sendImpl(1,getSize(),ORDER.EMPTY,new TimeoutAfterStartOperation(),true);
    }
                
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client opens a session and sends the the DATA packets awaiting acknowledgment from the server, "+
            "and then suspends the transfer of the timeout session. "+
            "After client continues transfer the remaining DATA packets. "+
            "Expected result: in response to the sending of DATA packet FILEID error is returned")
    @Test(priority=7)
    public void TC_RUDP_P_007() throws Exception{
        sendImpl(1,getSize(),ORDER.RANDOM,new TimeoutAfterSegmentOperation(),true);
    }
    
   
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client opens a session and the sends part of  DATA packet, waits for acknowledgment from the server,  "+
        "and then sends the ERROR Reset packet. After that clinet sends the rest of the DATA packets. "+
        "Expected result: in response to the sending of DATA packet after ERROR pecket the FILEID error is returned")
    @Test(priority=8)
    public void TC_RUDP_P_008() throws Exception{
        sendImpl(1,getSize(),ORDER.RANDOM,new ResetAfterSegmentOperation(),true);
    }
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client sets its segment size to the START package. "+
        "Expected result: The server confirms the size of the segment in the START ACK packet")
    @Test(priority=9)
    public void TC_RUDP_P_009() throws Exception{
        List<Integer> ids = sendImpl(getSize(), ORDER.EMPTY);
        int sessionId = ids.get(0);
        int size = null == RUDPTestingSendManager.getAckStartPacket(sessionId) ? 
                0 : RUDPTestingSendManager.getAckStartPacket(sessionId).getSegmentSize();
        int expected = getSegmentSize();
        Publisher.showSegmentSize(size, expected);
        Assert.assertTrue(size == expected, "Invalid segment size");
    }
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("AMS service sends a file to the client, which skips the first 9 START packets and responds to 10th. "+
            "Expected result: The file has been sent after the 10th attempt")
    @Test(priority=10)
    public void TC_RUDP_P_010() throws Exception{
        final int attempts = Integer.parseInt(properties.getProperty("start-attempts","10"));
        RUDPTestingSendManager sender = null;
        try{
            sender = new RUDPTestingSendManager(properties.getAMSHost(),
                properties.DEFAULT_RUDP_AMS_PORT,properties.DEFAULT_RUDP_LOCAL_PORT, properties.getDeviceId());
            sender.setAckHandler(new AckHandler(){
                private final AtomicInteger attempt = new AtomicInteger();
                    @Override
                    public boolean handleStartAck(StartPacket packet){
                    if (attempts == attempt.addAndGet(1)){
                        attempt.set(0);
                        return true;
                    }
                    else
                        return false;
                }
            });
            String uri = properties.getRUDPTestingUri();
            byte [] data = RUDPTestingUtils.getRandomBytes(200);
            FTHttpUtils.HttpResult response=FTHttpUtils.doPostDevId(uri,properties.getDeviceId(),data);
            Publisher.showHTTPStatus(uri, data, response);
            Assert.assertTrue(200 == response.getStatus(),"Response status must be 200");
            Assert.assertTrue(Arrays.equals(data, (byte[])response.getEntity()), "Request and response data must be the same");
        }
        finally{
            if (null != sender){
                sender.setAckHandler(null);
                sender.close();
            }
        }
    } 
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("AMS service sends a file to the client, which skips all START packets. "+
            "Expected result: AMS service raises an exception after 10th attempt to send START packet")
    @Test(priority=11)
    public void TC_RUDP_P_011() throws Exception{
        RUDPTestingSendManager sender = null;
        try{
            sender = new RUDPTestingSendManager(properties.getAMSHost(),
                properties.DEFAULT_RUDP_AMS_PORT,properties.DEFAULT_RUDP_LOCAL_PORT, properties.getDeviceId());
            String uri = properties.getRUDPTestingUri();
            byte [] data = RUDPTestingUtils.getRandomBytes(200);
            FTHttpUtils.HttpResult response=FTHttpUtils.doPostDevId(uri,properties.getDeviceId(),data);
            Publisher.showHTTPStatus(uri, data, response);
            Assert.assertTrue(500 == response.getStatus(),"Response status must be 500");
        }
        finally{
            if (null != sender)
                sender.close();
        }
    }
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("AMS service sends a file to the client. Client doesn't include the part of the received DATA packets to the DATA ACK packet."+
            "Expected result: AMS repeatedly sends packets which have no confromation to the client. "+
            "Eventually client receives the whole file. The sent file is identical to the received file")
    @Test(priority=12)
    public void TC_RUDP_P_012() throws Exception{
        RUDPTestingSendManager sender = null;
        final int attempts = Integer.parseInt(properties.getProperty("data-attempts","5"));
        try{
            sender = new RUDPTestingSendManager(properties.getAMSHost(),
                properties.DEFAULT_RUDP_AMS_PORT,properties.DEFAULT_RUDP_LOCAL_PORT, properties.getDeviceId());
            
            sender.setAckHandler(new AckHandler(){
                private ConcurrentMap<Integer,ConcurrentMap<Integer,AtomicInteger>> map =new ConcurrentHashMap<>() ;
                
                @Override
                public int handleDataAck(IRUDPReceiverSession session, DataPacket packet){
                    ConcurrentMap<Integer,AtomicInteger> segments = new ConcurrentHashMap<>();
                    ConcurrentMap<Integer,AtomicInteger> s = map.putIfAbsent(session.getSessionId(),segments);
                    boolean firstInSession = null == s;
                    segments = (null == s) ? segments : s;

                    AtomicInteger ai = new AtomicInteger();
                    AtomicInteger a = segments.putIfAbsent(packet.getSegmentNumber(), ai);
                    ai = (null == a) ? ai : a;
                    int attempt = ai.incrementAndGet();
                    
                    int flag = (firstInSession || attempt >= attempts) ? IAckHandler.RECEIVE_DATA : 0;
                    return flag | IAckHandler.SEND_DATA_ACK;
                }
            });
            
            String uri = properties.getRUDPTestingUri();
            byte [] data = RUDPTestingUtils.getRandomBytes(3*getSegmentSize(),3*getSegmentSize()+100);
            FTHttpUtils.HttpResult response=FTHttpUtils.doPostDevId(uri,properties.getDeviceId(),data);
            Publisher.showHTTPStatus(uri, data, response);
            Assert.assertTrue(200 == response.getStatus(),"Response status must be 200");
        }
        finally{
            if (null != sender){
                sender.setAckHandler(null);
                sender.close();
            }
        }
    }
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("Client doesn't respond on AMS DATA packages. "+
            "Expected result: AMS service raises an exception after the session timeout")
    @Test(priority=12)
    public void TC_RUDP_P_012_1() throws Exception{
        RUDPTestingSendManager sender = null;
        try{
            sender = new RUDPTestingSendManager(properties.getAMSHost(),
                properties.DEFAULT_RUDP_AMS_PORT,properties.DEFAULT_RUDP_LOCAL_PORT, properties.getDeviceId());
            sender.setAckHandler(new AckHandler(){
                @Override
                public int handleDataAck(IRUDPReceiverSession session, DataPacket packet){
                    return 0;
                }
             });
            String uri = properties.getRUDPTestingUri();
            byte [] data = RUDPTestingUtils.getRandomBytes(200);
            FTHttpUtils.HttpResult response=FTHttpUtils.doPostDevId(uri,properties.getDeviceId(),data);
            Publisher.showHTTPStatus(uri, data, response);
            Assert.assertTrue(500 == response.getStatus(),"Response status must be 500");
        }
        finally{
            if (null != sender){
                sender.setAckHandler(null);
                sender.close();
            }
        }
    }
    
    
    @Features(RUDP)
    @Stories("Positive")
    @Description("The client opens multiple sessions, and then sends an ERROR Reset packet with file-id = 0. "+
        "Expected result: sending any DATA packets with file-id of previously open sessions results in FILEID error is returned")
    @Test(priority=13)
    public void TC_RUDP_P_013() throws Exception{
        sendImpl(5,getSize(),ORDER.RANDOM,new ResetAfterSegmentOpeartion0(),true);
    }
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client opens two sessions at the same time with a single file-id. "+
        "The second START packet is sent without waiting for confirmation of the first. "+
        "Expected result: The server returns FILEID error after attempt to open one of the sessions")
    @Test(priority=14)
    public void TC_RUDP_N_001() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN001(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client opens a session waiting for confirmation from the server, and then re-sends the START package with the same file-id. "+
        "Expected result: The server returns FILEID error after attempt to open the second session")
    @Test(priority=15)
    public void TC_RUDP_N_002() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(ErrorCodeType.FILEID),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN002(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client opens a session and sends a DATA packet with file-id different from that which was specified in the START package. "+
            "Expected result: the server returns an error ERROR-ID in response to a DATA packet")
    @Test(priority=16)  
    public void TC_RUDP_N_003() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(ErrorCodeType.FILEID),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN003(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client sends a packet with an invalid type. "+
            "Expected result: The server response is not received within a specified time interval")
    @Test(priority=17)  
    public void TC_RUDP_N_004() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN004(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client sends a file that does not match the CRC32. "+
            "Expected result: The server returns an error ERROR-CRC32")
    @Test(priority=18)  
    public void TC_RUDP_N_005() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(ErrorCodeType.CRC32),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN005(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("Within client session sends a segment with the same number several times. "+
            "Expected result: The original file is received without errors")
    @Test(priority=19)  
    public void TC_RUDP_N_006() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new Operation(),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN006(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
            
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client sends a file larger than indicated at the opening session. "+
            "Expected result: The server sends an ERROR NACK packet in response")
    @Test(priority=19)  
    public void TC_RUDP_N_007() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(ErrorCodeType.NACK),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN007(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client sends an ACK packet with the file-id of open session. "+
            "Expected result: The server ignores the ACK packets")
    @Test(priority=21)  
    public void TC_RUDP_N_009() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new Operation(),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN009(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    
    @Features(RUDP)
    @Stories("Negative")
    @Description("The client sends one of the intermediate (not the last) DATA packet data with a smaller size. "+
            "Expected result: The server sends an ERROR NACK packet in response.")
    @Test(priority=22)  
    public void TC_RUDP_N_010() throws Exception{
        sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,getSize(),ORDER.RANDOM,new ErrorOperation(ErrorCodeType.NACK),true,null,new ITestingServiceProvider(){
            @Override
            public TestingService getTestingService(RUDPTestingSendManager sender, int sessionId, ORDER order, IOperation operation, CountDownLatch countDown) {
                return new TestingServiceN010(sender,sessionId,order,operation,countDown);
            }
        });
    }
    
    
    @Features(RUDP)
    @Stories("Sending and receiving a large number of files")
    @Description("A lot of clients simultaneously send files to AMS. Sending is repeated not less than 100 iterations.")
    @Test(priority=50,enabled=false)
    public void TC_RUDP_V_001() throws Exception{
        final AtomicInteger total = new AtomicInteger();
        final AtomicInteger failed = new AtomicInteger();
        LighweightCHEemulator emulator = null;
        CountDownLatch countDown = new CountDownLatch(MAX_NUMBER_OF_CLIENTS);

        try {
            emulator = createAndStartHEEmulator();
            ExecutorService pool = Executors.newFixedThreadPool(Math.min(MAX_NUMBER_OF_THREADS, MAX_NUMBER_OF_CLIENTS));
            int port = properties.DEFAULT_RUDP_LOCAL_PORT;
            for (int i = 0; i < MAX_NUMBER_OF_CLIENTS; i++) {
                pool.execute(new ClientTestingService(port++, emulator, countDown, total, failed));
            }

            boolean ok = countDown.await(CLIENT_TESTS_TIMEOUT_STRESS, TimeUnit.MINUTES);

            Publisher.showTestStatistics(total.get(), failed.get());

            Assert.assertTrue(ok, "Test is not responding");
            Assert.assertTrue(0 == failed.get(), "Some file transfer are failed");
        } catch (Exception ex) {
            Assert.assertTrue(false, ex.getMessage());
        } finally {
            if (null != emulator) {
                emulator.stop();
            }
        }
    }
    
    private class ClientTestingService implements Runnable {
        private final int localPort;
        private final LighweightCHEemulator emulator;
        private final CountDownLatch countDown;
        private final AtomicInteger total;
        private final AtomicInteger failed;
        
        private ClientTestingService(int localPort,LighweightCHEemulator emulator,CountDownLatch countDown,AtomicInteger total,AtomicInteger failed){
            this.localPort = localPort;
            this.emulator = emulator;
            this.countDown = countDown;
            this.total = total;
            this.failed = failed;
        }

        @Override
        public void run() {
            try{
                
                for (int i=0;i<MAX_NUMBER_OF_ITERATIONS;i++){
                    SilenceOperation op =new SilenceOperation();
                    sendImpl(localPort,MAX_NUMBER_OF_SESSIONS,getStressSize(),ORDER.RANDOM,op,true,emulator,null);
                    total.addAndGet(op.getTotalCount());
                    failed.addAndGet(op.getFailedCount());
                }
            }
            catch(Exception ex){     
            } 
            finally{
                countDown.countDown();
            }
        }
        
    }
    
     
    private LighweightCHEemulator createAndStartHEEmulator(){
        RUDPTestingDataConsumer consumer = new RUDPTestingDataConsumer();
        LighweightCHEemulator emulator=createCharterHEEmulator(consumer,HEHandlerType.RUDP_TESTING);
        Assert.assertTrue(null != emulator,"Unable to create Charter HE emulator");
        startCharterHEEmulator(emulator);
        return emulator;
    }
             
  
    private List<Integer> sendImpl(int size,ORDER order) throws Exception{
        return sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,1,size,order,new Operation(),true,null,null);
    }
    
    //properties.DEFAULT_RUDP_AMS_PORT,properties.DEFAULT_RUDP_LOCAL_PORT, properties.getDeviceId());
    private List<Integer> sendImpl(int threads,int size,ORDER order,IOperation operation,boolean publish) throws Exception{
        return sendImpl(properties.DEFAULT_RUDP_LOCAL_PORT,threads,size,order,operation,publish,null,null);
    }
   
    private List<Integer> sendImpl(int localPort,int threads,int size,ORDER order,IOperation operation,boolean publish,LighweightCHEemulator emulator,ITestingServiceProvider provider) throws Exception{
        List<Integer> result = new ArrayList<>();
        
        RUDPTestingSendManager sender =  null;
        boolean needStopHE = false;
        
        try{
            CountDownLatch countDown = new CountDownLatch(threads);
            int seg_size = getSegmentSize();
        
            ExecutorService pool = Executors.newFixedThreadPool(Math.min(MAX_NUMBER_OF_THREADS,threads));
        
            sender = new RUDPTestingSendManager(properties.getAMSHost(),
                properties.DEFAULT_RUDP_AMS_PORT,localPort, properties.getDeviceId());
            if (null != operation)
                operation.setRUDPTestingSendManager(sender);
        
            if (null == emulator) {
                needStopHE = true;
                emulator = createAndStartHEEmulator();
            }
                
        
            for (int i=0;i<threads;i++){
                int sessionId = sender.createSenderSession(size);
                if (null != operation)
                    operation.addSessionId(sessionId);
                result.add(sessionId);
                TestingService service = (null == provider) ? new TestingService(sender,sessionId,order,operation,countDown) : 
                    provider.getTestingService(sender, sessionId, order, operation, countDown);
                pool.execute(service);
            }
            
            boolean ok = countDown.await(CLIENT_TEST_TIMEOUT, TimeUnit.MINUTES);
            
            if (null != operation){
                if (publish)
                    operation.publish();
                operation.isSuccess();
            }
        }
        finally{
            
            if (null != sender)
                sender.close();
            if (null != emulator && needStopHE)
                emulator.stop();
        }
        return result;
    }
    
 
        
    @Features(RUDP)
    @Stories("AMS logs")
    @Description("RUDP Testing AMS log")
    @Test(priority=100)
    public void getRUDPTestingAMSLog() throws Exception {
        stopAndgetAMSLog(true);
    }
    
    
}
