package com.zodiac.ams.charter.tests.ft.rudp;

import java.util.ArrayList;
import java.util.List;


public class SessionsStatistic {
    final List<Integer> failed = new ArrayList<>();
    final int total;
    
    SessionsStatistic(List<Integer> failed,int total){
        this.failed.addAll(failed);
        this.total = total;
    }
    
    @Override
    public String toString(){
        StringBuilder sb =new StringBuilder();
        sb.append("TOTAL FILE TRANSFER: ").append(total).append(" FAILED: ").append(failed.size());
        if (failed.size() > 0){
            sb.append("\nFAILED:\n");
            for (int id: failed)
                sb.append("\n").append(Publisher.getFileStatistics(id));
        }
        return sb.toString();
    }
    
}
