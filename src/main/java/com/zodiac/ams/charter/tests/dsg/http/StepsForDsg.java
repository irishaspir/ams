package com.zodiac.ams.charter.tests.dsg.http;

import com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils;
import com.zodiac.ams.charter.http.helpers.dsg.DsgJsonDataUtils;
import com.zodiac.ams.charter.tests.Steps;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.insertToSTBMetadata;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class StepsForDsg extends Steps {

    protected DsgJsonDataUtils dsgJsonDataUtils = new DsgJsonDataUtils();

    @BeforeMethod
    public void getStartTestTime() {
        startTestTime = getCurrentTimeTest();
    }

    @BeforeClass
    public void fillDB() {
        cleanSTBMetadata();
        insertDataToSTBMetadata();
    }

    @AfterClass
    public void cleanDB() {
        cleanSTBMetadata();
    }

    @Step
    public void insertDataToSTBMetadata() {
        insertToSTBMetadata(MAC_NUMBER_1);
        insertToSTBMetadata(MAC_NUMBER_2);
        insertToSTBMetadata(MAC_NUMBER_3);
    }

    @Step
    protected void cleanSTBMetadata() {
        new STBMetadataUtils().deleteAll();
    }

    @Step
    public List<String> readAMSLog() {
        return parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT);
    }
}
