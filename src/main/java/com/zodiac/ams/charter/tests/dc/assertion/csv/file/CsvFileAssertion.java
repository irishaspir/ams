package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import java.util.List;

/**
 * The CSV file assertion interface.
 *
 * @param <E> the type of CSV file entry
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public interface CsvFileAssertion<E> {
    /**
     * The method to assert the list of CSV file entries.
     *
     * @param csvFileEntries the list of CSV file entries
     */
    void assertFile(List<E> csvFileEntries);
}
