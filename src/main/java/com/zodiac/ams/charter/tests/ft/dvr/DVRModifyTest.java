package com.zodiac.ams.charter.tests.ft.dvr;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.handlers.UniversalListener;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.FtDbHelper;
import com.zodiac.ams.charter.services.ft.dvr.DvrDbHelper;
import com.zodiac.ams.charter.services.ft.dvr.IDVRObject;
import com.zodiac.ams.charter.services.ft.dvr.ModifyRequest;
import com.zodiac.ams.charter.services.ft.dvr.ModifyResponse;
import com.zodiac.ams.charter.services.ft.dvr.RecordingParam;
import com.zodiac.ams.charter.services.ft.dvr.RequestRecording;
import com.zodiac.ams.charter.services.ft.dvr.ResponseRecording;
import com.zodiac.ams.charter.services.ft.dvr.ResponseRecordingParam;
import com.zodiac.ams.charter.tests.ft.DataConsumer;

import static com.zodiac.ams.charter.tests.FeatureList.DVR;
import static com.zodiac.ams.charter.tests.ft.dvr.DVRTest.STB_PROPS;
import com.zodiac.ams.common.http.FTHttpUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Semaphore;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomObject;
import com.zodiac.ams.charter.services.ft.dvr.DvrMessageUtils;
import com.zodiac.ams.charter.services.ft.dvr.ModifyRequests;
import com.zodiac.ams.charter.services.ft.dvr.SpaceStatus;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.EpisodesDefinition;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.RecordingFormat;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.SaveDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.WeekDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import java.util.HashMap;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;
import ru.yandex.qatools.allure.annotations.Description; 
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import com.zodiac.ams.charter.services.ft.dvr.IDVRSerializable;
import com.zodiac.ams.charter.services.ft.dvr.WholeDataRecorded;

public class DVRModifyTest extends DVRTest {
    
    @Override
    public boolean needRestart(){
        return false;
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.1.1 [DVR][Modify] Only mandatory parameters are present")
    @Test(priority=2)
    public void test_6_3_1_1_1() throws Exception {
        
        int id = getRandomInt(1, 1_000_000);
        int type = getRandomObject(Type.values()).getCode();
        prepareDB(id, type);
        
        ModifyRequest request = new ModifyRequest(deviceId, id, getRandomInt(1, 1_000));
        request.calculateMask();
        
        ModifyResponse response = ModifyResponse.getRandom(id);
        stepsModifyTest(request, response);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.1.1.2 [DVR][Modify][Mandatory parameters] negative cases")
    @Test(priority=2)
    public void test_6_3_1_1_2() throws Exception {
        List<Consumer<ModifyRequest>> consumers = Arrays.asList(
            p -> p.setRecordingId(""),
            p -> p.setRecordingId("test"),
            p -> p.setRecordingId(null),
            p -> p.setChannelNumber(""),
            p -> p.setChannelNumber("test"),
            p -> p.setChannelNumber(null)
        );
        invalidRequestImpl(consumers, 400);
    }
    
    private static final List<Consumer<ModifyRequest>> NEGATIVE_CONSUMERS =
        Arrays.asList(
            p -> p.setServiceId(""),
            p -> p.setServiceId("test"),
            p -> p.setStartTime(""),
            p -> p.setStartTime("test"),
            p -> p.setSaveDays(""),
            p -> p.setSaveDays("test"),
            p -> p.setStartOffset(""),
            p -> p.setStartOffset("test"),
            p -> p.setEndOffset(""),
            p -> p.setEndOffset("test"),
            p -> p.setRecordingFormat(""),
            p -> p.setRecordingFormat("test"),
            p -> p.setEpisodesDefinition(""),
            p -> p.setEpisodesDefinition("test"),
            p -> p.setUnitingParameter(""),
            p -> p.setUnitingParameter("test"),
            p -> p.setRecordDuplicates(""),
            p -> p.setRecordDuplicates("test"),
            p -> p.setRepeat(""),
            p -> p.setRepeat("test")
        );
    
    private static final List<Consumer<ModifyRequest>> POSITIVE_CONSUMERS = 
        Arrays.asList(
            p -> p.setServiceId(getRandomInt(1, 1_000)),
            p -> p.setStartTime((int) (System.currentTimeMillis()/1000)),
            p -> p.setDuration(getRandomInt(1, 100)),
            p -> p.setSaveDays(getRandomObject(SaveDays.values())),
            p -> p.setStartOffset(getRandomInt(1,5) * 60),
            p -> p.setEndOffset(getRandomInt(1,5) * 60),
            p -> p.setRecordingFormat(getRandomObject(RecordingFormat.values())),
            p -> p.setEpisodesDefinition(getRandomObject(EpisodesDefinition.values())),
            p -> p.setUnitingParameter(DvrMessageUtils.buildUnitingParam(getRandomInt(1, 1_000))),
            p -> p.setRecordDuplicates(getRandomObject(YesNo.values())),
            p -> p.setRepeat(WeekDays.getRandomWeekDays())
    );
    
    private static final List<Consumer<ModifyRequest>> POSITIVE_ALL_CONSUMER = 
        Arrays.asList(
            p -> {
                p.setServiceId(getRandomInt(1, 1_000));
                p.setStartTime((int) (System.currentTimeMillis()/1000));
                p.setDuration(getRandomInt(1, 100));
                p.setSaveDays(getRandomObject(SaveDays.values()));
                p.setStartOffset(getRandomInt(1,5) * 60);
                p.setEndOffset(getRandomInt(1,5) * 60);
                p.setRecordingFormat(getRandomObject(RecordingFormat.values()));
                p.setEpisodesDefinition(getRandomObject(EpisodesDefinition.values()));
                p.setUnitingParameter(DvrMessageUtils.buildUnitingParam(getRandomInt(1, 1_000)));
                p.setRecordDuplicates(getRandomObject(YesNo.values()));
                p.setRepeat(WeekDays.getRandomWeekDays());
            }
    );
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.1 [DVR][Modify][Type:Single][wholeSetFlag:No] positive cases")
    @Test(priority=2)
    public void test_6_3_1_2_1() throws Exception {
        positiveTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.10 [DVR][Modify][Type:Series][wholeSetFlag:Yes] positive cases")
    @Test(priority=2)
    public void test_6_3_1_2_10() throws Exception {
        positiveTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.11 [DVR][Modify][Type:Series][wholeSetFlag:Yes] All parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_11() throws Exception {
        positiveTestImplAll();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.12 [DVR][Modify][Type:Series][wholeSetFlag:Yes] Random parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_12() throws Exception {
        positiveTestImplRandom();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.13 [DVR][Modify][Type:Manual][wholeSetFlag:No] positive cases")
    @Test(priority=2)
    public void test_6_3_1_2_13() throws Exception {
        positiveTestImplRandom();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.14 [DVR][Modify][Type:Manual][wholeSetFlag:No] All parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_14() throws Exception {
        positiveTestImplAll();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.15 [DVR][Modify][Type:Manual][wholeSetFlag:No] Random parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_15() throws Exception {
        positiveTestImplRandom();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.16 [DVR][Modify][Type:Manual][wholeSetFlag:Yes] positive cases")
    @Test(priority=2)
    public void test_6_3_1_2_16() throws Exception {
        positiveTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.17 [DVR][Modify][Type:Manual][wholeSetFlag:Yes] All parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_17() throws Exception {
        positiveTestImplAll();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.18 [DVR][Modify][Type:Manual][wholeSetFlag:Yes] Random parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_18() throws Exception {
        positiveTestImplRandom();
    }

    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.2 [DVR][Modify][Type:Single][wholeSetFlag:No] All parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_2() throws Exception {
        positiveTestImplAll();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.3 [DVR][Modify][Type:Single][wholeSetFlag:No] random parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_3() throws Exception {
        positiveTestImplRandom();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.4 [DVR][Modify][Type:Single][wholeSetFlag:Yes] positive cases")
    @Test(priority=2)
    public void test_6_3_1_2_4() throws Exception {
        positiveTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.5 [DVR][Modify][Type:Single][wholeSetFlag:Yes] All parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_5() throws Exception {
        positiveTestImplAll();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.6 [DVR][Modify][Type:Single][wholeSetFlag:Yes] Random parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_6() throws Exception {
        positiveTestImplRandom();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.7 [DVR][Modify][Type:Series][wholeSetFlag:No] positive cases")
    @Test(priority=2)
    public void test_6_3_1_2_7() throws Exception {
        positiveTestImpl();
    }

    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.8 [DVR][Modify][Type:Series][wholeSetFlag:No] All parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_8() throws Exception {
        positiveTestImplAll();
    }

    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.1.2.9 [DVR][Modify][Type:Series][wholeSetFlag:No] Random parameters in request")
    @Test(priority=2)
    public void test_6_3_1_2_9() throws Exception {
        positiveTestImplRandom();
    }
    
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.1 [DVR][Modify] Status '1 - ERROR, no operation has been accomplished'")
    @Test(priority=3)
    public void test_6_3_2_1() throws Exception {
        invalidStatusImpl(Error.Error1);
    }
    
    private static final Error[] STATUSES = new Error[]{
        Error.Error0, Error.Error1, Error.Error3, Error.Error4, Error.Error8, 
        Error.Error12, Error.Error13, Error.Error15, Error.Error16, Error.Error18};
    
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.10 [DVR][Modify] Random statuses (successful and unsuccessful)")
    @Test(priority=3)
    public void test_6_3_2_10() throws Exception {
        allStatusesImpl(RandomUtils.getRandomObject(STATUSES));
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.11 [DVR][Modify] All statuses")
    @Test(priority=3)
    public void test_6_3_2_11() throws Exception {
        allStatusesImpl(Error.Error0);
    }
    
    private void allStatusesImpl(Error status) throws Exception {
        ModifyRequests requests = new ModifyRequests(deviceId); 
        List<Integer> ids = new ArrayList<>();
        List<Integer> types = new ArrayList<>();
        
        int id = getRandomInt(1, 1_000_000);
        
        for (Error stat: STATUSES){
            id++;
            Type type = getRandomObject(Type.values());
            types.add(type.getCode());
            ids.add(id);
            ModifyRequest request = ModifyRequest.getRandom(deviceId, id, type);
            request.calculateMask();
            requests.add(request);
        }
        
        prepareDB(ids.toArray(new Integer[]{}), types.toArray(new Integer[]{}));
        
        ModifyResponse response = ModifyResponse.getRandom(id, Error.Error0, status);
        stepsModifyTest(requests, response);
    }
        
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.2 [DVR][Modify] Status '3 - No target program exists'")
    @Test(priority=3)
    public void test_6_3_2_2() throws Exception {
        invalidStatusImpl(Error.Error3);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.3 [DVR][Modify] Status '4 - There is a schedule conflicts'")
    @Test(priority=3)
    public void test_6_3_2_3() throws Exception {
        invalidStatusImpl(Error.Error4);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.4 [DVR][Modify] Status '8 - The DVR is full'")
    @Test(priority=3)
    public void test_6_3_2_4() throws Exception {
        invalidStatusImpl(Error.Error8);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.5 [DVR][Modify] Status '12 - PPV program being scheduled for recording has not been purchased'")
    @Test(priority=3)
    public void test_6_3_2_5() throws Exception {
        invalidStatusImpl(Error.Error12);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.6 [DVR][Modify] Status '13 - Invalid parameter was specified'")
    @Test(priority=3)
    public void test_6_3_2_6() throws Exception {
        invalidStatusImpl(Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.7 [DVR][Modify] Status '15 - No subscription to the channel'")
    @Test(priority=3)
    public void test_6_3_2_7() throws Exception {
        invalidStatusImpl(Error.Error15);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.8 [DVR][Modify] Status '16 - Unknown error'")
    @Test(priority=3)
    public void test_6_3_2_8() throws Exception {
        invalidStatusImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.2.9 [DVR][Modify] Status '18 - No EPG available for the program'")
    @Test(priority=3)
    public void test_6_3_2_9() throws Exception {
        invalidStatusImpl(Error.Error18);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.3.1 [DVR][Modify] Error '1 - ERROR, no operation has been accomplished'")
    @Test(priority=4)
    public void test_6_3_3_1() throws Exception {
        invalidErrorImpl(Error.Error1);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.3.2 [DVR][Modify] Error '7 - DVR is not authorized on the STB'")
    @Test(priority=4)
    public void test_6_3_3_2() throws Exception {
        invalidErrorImpl(Error.Error7);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.3.3 [DVR][Modify] Error '10 - DVR is not ready (still initializing)'")
    @Test(priority=4)
    public void test_6_3_3_3() throws Exception {
        invalidErrorImpl(Error.Error10);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.3.4 [DVR][Modify] Error '13 - Invalid parameter was specified'")
    @Test(priority=4)
    public void test_6_3_3_4() throws Exception {
        invalidErrorImpl(Error.Error13);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.3.5 [DVR][Modify] Error '16 - Unknown error'")
    @Test(priority=4)
    public void test_6_3_3_5() throws Exception {
        invalidErrorImpl(Error.Error16);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.3.6 [DVR][Modify] Error '17 - STB is busy'")
    @Test(priority=4)
    public void test_6_3_3_6() throws Exception {
        invalidErrorImpl(Error.Error17);
    }
    
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.4.1.1 [DVR][Modify][Type:Single][wholeSetFlag:No] negative cases")
    @Test(priority=5)
    public void test_6_3_4_1_1() throws Exception {
        negativeTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.4.1.2 [DVR][Modify][Type:Single][wholeSetFlag:Yes] negative cases")
    @Test(priority=5)
    public void test_6_3_4_1_2() throws Exception {
        negativeTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.4.2.1 [DVR][Modify][Type:Series][wholeSetFlag:No] negative cases")
    @Test(priority=5)
    public void test_6_3_4_2_1() throws Exception {
        negativeTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.4.2.2 [DVR][Modify][Type:Series][wholeSetFlag:Yes] negative cases")
    @Test(priority=5)
    public void test_6_3_4_2_2() throws Exception {
        negativeTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.4.3.1 [DVR][Modify][Type:Manual][wholeSetFlag:No] negative cases")
    @Test(priority=5)
    public void test_6_3_4_3_1() throws Exception {
        negativeTestImpl();
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.4.3.2 [DVR][Modify][Type:Manual][wholeSetFlag:Yes] negative cases")
    @Test(priority=5)
    public void test_6_3_4_3_2() throws Exception {
        negativeTestImpl();
    }
    
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.1 [DVR][Modify] 'Status' is nonexistent")
    @Test(priority=5)
    public void test_6_3_5_1() throws Exception {
        invalidStatusImpl(Error.NON_EXISTENT_UNKNOWN);
    }
    
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.5.10 [DVR][Modify] 'STB scheduled checksum' is '0'")
    @Test(priority=5)
    public void test_6_3_5_10() throws Exception {
        positiveTestImplResponse(p -> p.setCrc(0L));
    }
     
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.11 [DVR][Modify] 'STBSpaceStatus' is missing")
    @Test(priority=5)
    public void test_6_3_5_11() throws Exception {
        positiveTestImplResponse(p -> {p.setSpaceStatus(null); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.5.12 [DVR][Modify] 'STBSpaceStatus' is '0'")
    @Test(priority=5)
    public void test_6_3_5_12() throws Exception {
        positiveTestImplResponse(p -> p.setSpaceStatus(SpaceStatus.get0()));   
    }
     
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.13 [DVR][Modify] 'PriorityVector' is missing")
    @Test(priority=5)
    public void test_6_3_5_13() throws Exception {
        positiveTestImplResponse(p -> {p.setPriority(null); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.14 [DVR][Modify] Second 'Count' is '0'")
    @Test(priority=5)
    public void test_6_3_5_14() throws Exception {
        List<Integer> priority = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setPriority(priority); p.setPrioritySize(0); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.15 [DVR][Modify] Second 'Count' is missing")
    @Test(priority=5)
    public void test_6_3_5_15() throws Exception {
        List<Integer> priority = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setPriority(priority); p.setPrioritySize(ModifyResponse.NO_VALUE); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
     
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.16 [DVR][Modify] Second 'RecordingId' is missing")
    @Test(priority=5)
    public void test_6_3_5_16() throws Exception {
        positiveTestImplResponse(p -> {p.setPriority(null); p.setPrioritySize(1); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Positive")
    @Description("6.3.5.17 [DVR][Modify] Second 'RecordingId' is '0'")
    @Test(priority=5)
    public void test_6_3_5_17() throws Exception {
        positiveTestImplResponse(p -> p.setPriority(Arrays.asList(0)));   
    }
     
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.18 [DVR][Modify] Quantity of second 'Recording ID' < second 'Count'")
    @Test(priority=5)
    public void test_6_3_5_18() throws Exception {
        List<Integer> priority = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setPriority(priority); p.setPrioritySize(priority.size()+1); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
     
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.19 [DVR][Modify][Oracle] Recording doesn't exist in DVR_Recordings, but it is in DVR_STB_List")
    @Test(priority=5)
    public void test_6_3_5_19() throws Exception {
        int id = getRandomInt(1, 1_000_000);
        int type = getRandomObject(Type.values()).getCode();
        prepareDB(id, type);
        DvrDbHelper.clearRecording(deviceId, false /*deleteSTB=false*/);
        
        ModifyRequest request = new ModifyRequest(deviceId, id, getRandomInt(1, 1_000));
        request.calculateMask();
        
        ModifyResponse response = ModifyResponse.getRandom(id);
        response.setStatus(Error.RECORD_NOT_FOUND);
        stepsModifyTest(request, response);
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.2 [DVR][Modify] 'Status' is empty")
    @Test(priority=5)
    public void test_6_3_5_2() throws Exception {
        positiveTestImplResponse(p -> p.setStatus(Error.STB_MISSING_STATUS));   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.20 [DVR][Modify][Oracle] Information about STB doesn't exist in DVR_STB_List, but recording is in DVR_Recordings")
    @Test(priority=5)
    public void test_6_3_5_20() throws Exception {
        int id = getRandomInt(1, 1_000_000);
        int type = getRandomObject(Type.values()).getCode();
        prepareDB(id, type);
        FtDbHelper.deleteDB("DVR_STB", new HashMap(){{put("MAC_STR", deviceId);}});
        
        ModifyRequest request = new ModifyRequest(deviceId, id, getRandomInt(1, 1_000));
        request.calculateMask();
        
        ModifyResponse response = ModifyResponse.getRandom(id);
        response.setStatus(Error.RECORD_NOT_FOUND);  //records must be deleted
        WholeDataRecorded wholeDataRecorded = new WholeDataRecorded();
        
        IDVRSerializable[] responses = new IDVRSerializable[]{response, wholeDataRecorded}; 
        
        stepsModifyTest(request, responses);
        
        SpaceStatus exp1 = wholeDataRecorded.getSpaceStatus();
        SpaceStatus act1 = DvrDbHelper.getDBSpaceStatus(deviceId);
        SpaceStatus act2 = DvrDbHelper.getCassandraSpaceStatus(deviceId);
        showDBSpaceStatus(exp1, act1);
        showCassandraSpaceStatus(exp1, act2);
        
        List<Integer> exp2 = wholeDataRecorded.getPriorityVector();
        List<Integer> act3 = DvrDbHelper.getDBPriorityVector(deviceId);
        List<Integer> act4 = DvrDbHelper.getCassandraPriorityVector(deviceId);
        showDBPriorityVector(exp2, act3);
        showDBPriorityVector(exp2, act4);
        
        verifyJSON(exp1.toJSON(), act1.toJSON(), "Expected and actual space status must be the same");
        Assert.assertTrue(Arrays.equals(exp2.toArray(new Integer[]{}), act3.toArray(new Integer[]{})), 
            "Expected and actual priority vector must be the same");
        
        verifyJSON(exp1.toJSON(), act2.toJSON(), "Expected and actual space status must be the same");
        Assert.assertTrue(Arrays.equals(exp2.toArray(new Integer[]{}), act4.toArray(new Integer[]{})), 
            "Expected and actual priority vector must be the same");
    }
            
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.3 [DVR][Modify] 'Conflict count' is '0' when 'Status' = 4")
    @Test(priority=5)
    public void test_6_3_5_3() throws Exception {
        List<Integer> conflicts = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setConflicts(conflicts); p.setConflictsSize(0); p.setStatus(Error.SCHEDULE_CONFLICT_ERROR);});   
    }
    
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.4 [DVR][Modify] 'Conflict count' > 0 when 'Status' != 4")
    @Test(priority=5)
    public void test_6_3_5_4() throws Exception {
        List<Integer> conflicts = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setConflicts(conflicts); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.5 [DVR][Modify] 'Conflict count' is empty")
    @Test(priority=5)
    public void test_6_3_5_5() throws Exception {
        List<Integer> conflicts = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setConflicts(conflicts); p.setConflictsSize(ModifyResponse.NO_VALUE); p.setStatus(Error.SCHEDULE_CONFLICT_ERROR);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.6 [DVR][Modify] 'Recording ID' is empty")
    @Test(priority=5)
    public void test_6_3_5_6() throws Exception {
        positiveTestImplResponse(p -> {p.setConflictsSize(1); p.setStatus(Error.SCHEDULE_CONFLICT_ERROR);});   
    }
    
    @Features(DVR)
     @Stories("Modify recording. Positive")
    @Description("6.3.5.7 [DVR][Modify] 'Recording ID' is '0'")
    @Test(priority=5)
    public void test_6_3_5_7() throws Exception {
        positiveTestImplResponse(p -> {p.setConflicts(Arrays.asList(0)); p.setStatus(Error.SCHEDULE_CONFLICT);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.8 [DVR][Modify] Quantity of 'Recording ID' < 'Count'")
    @Test(priority=5)
    public void test_6_3_5_8() throws Exception {
        List<Integer> conflicts = ModifyResponse.getRandomRecordsVector(10);
        positiveTestImplResponse(p -> {p.setConflicts(conflicts); p.setConflictsSize(conflicts.size()+1); p.setStatus(Error.SCHEDULE_CONFLICT_ERROR);});   
    }
    
    @Features(DVR)
    @Stories("Modify recording. Negative")
    @Description("6.3.5.9 [DVR][Modify] 'STB scheduled checksum' is missing")
    @Test(priority=5)
    public void test_6_3_5_9() throws Exception {
        positiveTestImplResponse(p -> {p.setCrc(null); p.setStatus(Error.STB_INVALID_RESPONSE);});   
    }
    
    private void negativeTestImpl() throws Exception{
        negativeTestImpl(NEGATIVE_CONSUMERS, 400);
    }
    
    private void negativeTestImpl(List<Consumer<ModifyRequest>> consumers, int expectedStatus) throws Exception{
        negativeTestImpl(() -> {return new ModifyRequest();}, consumers, expectedStatus);
    }
            
    private void negativeTestImpl(Supplier<ModifyRequest> supplier, List<Consumer<ModifyRequest>> consumers, int expectedStatus) throws Exception {
        String description = FTAllureUtils.getDescription();
        
        Type type = Type.fromString(RecordingDescriptionParser.getValue(description, "Type"));
        Assert.assertTrue(null != type, "type must not be null");
        
        YesNo wholeSetFlag = YesNo.fromString(RecordingDescriptionParser.getValue(description, "wholeSetFlag"));
        Assert.assertTrue(null != type, "wholeSetFlag must not be null");
        
        String uri = properties.getDVRUri();
        StringBuilder sb = new StringBuilder();
        int index = 0;
        
        for (Consumer<ModifyRequest> consumer: consumers){
            index++;
            try{
                int recordingId = getRandomInt(1,1_000_000);
                int channelNumber = getRandomInt(1, 1_000);
                
                ModifyRequest request = supplier.get();
                request.setDeviceId(deviceId);
                request.setRecordingId(recordingId);
                request.setChannelNumber(channelNumber);
                request.setType(type);
                request.setWholeSetFlag(wholeSetFlag);
                consumer.accept(request);
                //request.calculateMask();
                
                FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, request.toString());
                showHERequestToAMS(uri, request.toString()/*request*/, null /*expected response*/, httpResponse /*actual response*/);
                Assert.assertTrue(expectedStatus == httpResponse.getStatus(),"Expected status: " + expectedStatus + " Actual: " + httpResponse.getStatus());
            }
            catch(AssertionError error){
                sb.append(index).append(". ").append(error.getMessage()).append(" ");
            }
        }
        
        if (sb.length() > 0)
            Assert.assertTrue(false, sb.toString());
    }        
    
    private void positiveTestImplResponse(Consumer<ModifyResponse> consumer) throws Exception{
        int id = getRandomInt(1, 1_000_000);
        int type = getRandomObject(Type.values()).getCode();
        prepareDB(id, type);
        
        ModifyRequest request = new ModifyRequest(deviceId, id, getRandomInt(1, 1_000));
        request.calculateMask();
        
        ModifyResponse response = ModifyResponse.getRandom(id);
        if (null != consumer)
            consumer.accept(response);
        stepsModifyTest(request, response);
    }
        
    
    private void positiveTestImplRandom() throws Exception{
        positiveTestImpl(() -> {return ModifyRequest.getRandom();}, Arrays.asList(p -> {}));
    }
    
    private void positiveTestImplAll() throws Exception{
        positiveTestImpl(POSITIVE_ALL_CONSUMER);
    }
    
    private void positiveTestImpl() throws Exception{
        positiveTestImpl(POSITIVE_CONSUMERS);
    }
    
    private void positiveTestImpl(List<Consumer<ModifyRequest>> consumers) throws Exception{
        positiveTestImpl(() -> {return new ModifyRequest();}, consumers);
    }
        
    private void positiveTestImpl(Supplier<ModifyRequest> supplier, List<Consumer<ModifyRequest>> consumers) throws Exception {
        String description = FTAllureUtils.getDescription();
        
        Type type = Type.fromString(RecordingDescriptionParser.getValue(description, "Type"));
        Assert.assertTrue(null != type, "type must not be null");
        
        YesNo wholeSetFlag = YesNo.fromString(RecordingDescriptionParser.getValue(description, "wholeSetFlag"));
        Assert.assertTrue(null != type, "wholeSetFlag must not be null");
        
        StringBuilder error = new StringBuilder();
        int index = 0;
        for (Consumer<ModifyRequest> consumer: consumers){
            index++;
            try{
                int recordingId = getRandomInt(1,1_000_000);
                int channelNumber = getRandomInt(1, 1_000);
                
                ModifyRequest request = supplier.get();
                request.setDeviceId(deviceId);
                request.setRecordingId(recordingId);
                request.setChannelNumber(channelNumber);
                request.setType(type);
                request.setWholeSetFlag(wholeSetFlag);
                consumer.accept(request);
                request.calculateMask();
                
                ModifyResponse response = ModifyResponse.getRandom(recordingId);
                
                prepareDB(recordingId, type.getCode());
                    
                stepsModifyTest(request, response);
            }
            catch(AssertionError ex){
                error.append(index).append(". ").append(ex.getMessage()).append(" ");
            }
        }
        if (error.length() > 0)
            Assert.assertTrue(false, error.toString());
    }
    
    
    private void invalidRequestImpl(List<Consumer<ModifyRequest>> consumers, int expectedStatus) throws Exception{
        String uri = properties.getDVRUri();
        StringBuilder sb = new StringBuilder();
        int index = 0;
        for (Consumer<ModifyRequest> consumer: consumers){
            index++;
            try{
                ModifyRequest request = new ModifyRequest(deviceId, getRandomInt(1,1_000_000)/*recordingId*/, getRandomInt(1, 1_000)/*channelNumber*/);
                consumer.accept(request);
                FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, request.toString());
                showHERequestToAMS(uri, request.toString()/*request*/, null /*expected response*/, httpResponse /*actual response*/);
                Assert.assertTrue(expectedStatus == httpResponse.getStatus(),"Expected status: " + expectedStatus + " Actual: " + httpResponse.getStatus());
            }
            catch(AssertionError error){
                sb.append(index).append(". ").append(error.getMessage()).append(" ");
            }
        }
        if (sb.length() > 0)
            Assert.assertTrue(false, sb.toString());
    }
            
    
    
    private void stepsModifyTest(ModifyRequests heRequests, ModifyResponse stbResponse) throws Exception{
        CharterStbEmulator stb = null;
        DataConsumer consumer =  null;
        
        try {
            Semaphore semaphore = new Semaphore(0);
            consumer = new DataConsumer(semaphore);
            stb = createCharterSTBEmulator(STB_PROPS);
            stb.registerConsumer(consumer, HandlerType.Universal);
            UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
            listener.setResponse(stbResponse.getData());            
            startCharterSTBEmulator(stb);
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, heRequests.toString());
            
            byte[] actual = consumer.getData();
            showAndVerifyModify(heRequests, actual, stbResponse, uri, httpResponse, stbResponse.getError(), stbResponse.getStatus());
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
    
    private void stepsModifyTest(ModifyRequest heRequest, ModifyResponse stbResponse) throws Exception{
        CharterStbEmulator stb = null;
        DataConsumer consumer =  null;
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20*1_000);
        
        try {
            Semaphore semaphore = new Semaphore(0);
            consumer = new DataConsumer(semaphore);
            stb = createCharterSTBEmulator(STB_PROPS);
            stb.registerConsumer(consumer, HandlerType.Universal);
            UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
            listener.setResponse(stbResponse.getData());            
            startCharterSTBEmulator(stb);
            
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, heRequest.toString());
//            Thread.sleep(60*60*1_000);
            
            
            byte[] actual = consumer.getData();
            showAndVerifyModify(heRequest, actual, stbResponse, uri, httpResponse, stbResponse.getError(), stbResponse.getStatus());
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
    
    
    /**
     * 
     * @param heRequest
     * @param stbResponses [0] - main response [1..]-additional responses
     * @throws Exception 
     */
    private void stepsModifyTest(ModifyRequest heRequest, IDVRSerializable [] stbResponses) throws Exception{
        ModifyResponse stbResponse = (ModifyResponse) stbResponses[0];
        CharterStbEmulator stb = null;
        
        DataConsumer[] consumers =  null;
        byte [][] responses = null;
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20*1_000);
        
        try {
            consumers = new DataConsumer[stbResponses.length];
            responses = new byte[stbResponses.length][];
            
            stb = createCharterSTBEmulator(STB_PROPS);
            UniversalListener listener = (UniversalListener)stb.getMessageHandler(HandlerType.Universal);
            
            for (int i=0; i < stbResponses.length; i++){
                consumers[i] = new DataConsumer(new Semaphore(0));
                responses[i] = stbResponses[i].getData();
            }
            
            listener.registerDataConsumer(consumers);
            listener.setResponse(responses);
                       
            startCharterSTBEmulator(stb);
            
            
            String uri = properties.getDVRUri();
            FTHttpUtils.HttpResult httpResponse = FTHttpUtils.doPost(uri, heRequest.toString());
            
//            Thread.sleep(60*60*1_000);
            Thread.sleep(2_000);
            
            
            byte[] actual = consumers[0].getData();
            for (int i=1; i < consumers.length; i++)
                consumers[i].getData();
            showAndVerifyModify(heRequest, actual, stbResponse, uri, httpResponse, stbResponse.getError(), stbResponse.getStatus());
        }
        finally{
            if (null != stb)
                stb.stop();
        }
    }
    
    
    private boolean isValidErrorStatus(Error error,Error status){
        return Error.Error0 == error && 
            (Error.Error0 == status || Error.SCHEDULE_CONFLICT == status || Error.Error4 == status); 
    }
    
    /**
     * Only the last record is processed
     * @param request HE-AMS request
     * @param actualSTB actual AMS-STB request
     * @param response expected STB response
     * @param uri
     * @param httpResponse AMS-HE response
     * @param errorCode expected AMS-HE response error code 
     * @throws Exception 
     */
    private void showAndVerifyModify(ModifyRequests requests, byte[] actualSTB, ModifyResponse response, String uri,
        FTHttpUtils.HttpResult httpResponse, Error error, Error status) throws Exception{
        
        Set<String> missingCassandra = new HashSet(){{
            add("startTime"); 
            add("duration"); 
            add("unitingParameter");
        }}; 
        
        int last = requests.getRequests().size() - 1;
        int index = 1;
        
        StringBuilder message = new StringBuilder();
        
        for (int i=0; i <= last; i++){
            ModifyRequest he = new ModifyRequest(requests.getRequests().get(i)); 
        
            ModifyRequest heDvr = new ModifyRequest(he);
            heDvr.truncateToDvr();  //base DVR fields only
        
            ModifyResponse stb = new ModifyResponse(response);
        
            Integer id = he.getRecordingId();
        
            ModifyRequest db = DvrDbHelper.getDVRModifyRequest(FTConfig.getMac(deviceId), id);
            ModifyRequest dbDvr = ModifyRequest.getTruncated(db, heDvr); //base DVR fields only
        
            ModifyRequest cassandra = DvrDbHelper.getCassandraModifyRequest(deviceId, new Long(id));
            ModifyRequest cassandraDvr = ModifyRequest.getTruncated(cassandra, heDvr); //base DVR fields only
        
            //clear all expected parameters
            if (!isValidErrorStatus(error, status) || last != i){
                if (Error.RECORD_NOT_FOUND == status){
                    heDvr =new ModifyRequest((Method)null);
                }
                else
                    heDvr.clear();
            }
        
            
            if (last == i){
                //show HE-AMS request/response
                showHERequestToAMS(uri, requests.toString()/*request*/, response.toString()/*expected response*/, httpResponse /*actual response*/);
                //show AMS-STB expected/actual request
                showAMSRequestToSTB(requests.getRequests().get(last).getData()/*expected*/, actualSTB/*actual*/);
            }
        
            //show DB item
            showDBRecording(heDvr.toString()/*expected*/, dbDvr /*actual*/);
            //show Cassandra item
            showCassandraRecording(heDvr.toString()/*expected*/, cassandraDvr/*actual*/);
            
            try{
                if (last == i){
                    //verify status
                    Assert.assertTrue(200 == httpResponse.getStatus(),"Expected status: 200 Actual: "+ httpResponse.getStatus());
                    //verify AMS-HE response
                    verifyJSON(response.toJSON(), new JSONObject((String)httpResponse.getEntity()),"Expected and actual AMS response must be the same");
                    //verify AMS-STB request
                    Assert.assertTrue(Arrays.equals(he.getData(), actualSTB), "Expected and actual request from AMS to STB must be the same");
                }
        
                //verify DB & Cassandra
                verifyJSON(heDvr.toJSON(), dbDvr.toJSON(), "Expected and actual information in DB DVR_RECORDING table must be the same");
                verifyJSON(heDvr.toJSON(missingCassandra), cassandraDvr.toJSON(), "Expected and actual information in CASSANDRA RECORDINGS table must be the same");
            }
            catch(AssertionError ex){
                message.append(index).append(". ").append(ex.toString()).append(" ");
            }
            index++;
        }
        
        if (message.length()>0)
            Assert.assertTrue(false, message.toString());
    }
    
    
    /**
     * 
     * @param request HE-AMS request
     * @param actualSTB actual AMS-STB request
     * @param response expected STB response
     * @param uri
     * @param httpResponse AMS-HE response
     * @param errorCode expected AMS-HE response error code 
     * @throws Exception 
     */
    private void showAndVerifyModify(ModifyRequest request, byte[] actualSTB, ModifyResponse response, String uri,
        FTHttpUtils.HttpResult httpResponse, Error error, Error status) throws Exception{
        
        Set<String> missingCassandra = new HashSet(){{
            add("startTime"); 
            add("duration"); 
            add("unitingParameter");
        }}; 
        
        ModifyRequest he = new ModifyRequest(request); 
        
        ModifyRequest heDvr = new ModifyRequest(he);
        heDvr.truncateToDvr();  //base DVR fields only
        
        ModifyResponse stb = new ModifyResponse(response);
        
        Integer id = he.getRecordingId();
        
        ModifyRequest db = DvrDbHelper.getDVRModifyRequest(FTConfig.getMac(deviceId), id);
        ModifyRequest dbDvr = ModifyRequest.getTruncated(db, heDvr); //base DVR fields only
        
        ModifyRequest cassandra = DvrDbHelper.getCassandraModifyRequest(deviceId, new Long(id));
        ModifyRequest cassandraDvr = ModifyRequest.getTruncated(cassandra, heDvr); //base DVR fields only
        
        //clear all expected parameters
        if (!isValidErrorStatus(error, status)){
            if (Error.RECORD_NOT_FOUND == status)
                heDvr =new ModifyRequest((Method)null);
            else
                heDvr.clear();
        }
        
        //show HE-AMS request/response
        showHERequestToAMS(uri, request.toString()/*request*/, response.toString()/*expected response*/, httpResponse /*actual response*/);
        
        //show AMS-STB expected/actual request
        showAMSRequestToSTB(request.getData()/*expected*/, actualSTB/*actual*/);
        
        //show DB item
        showDBRecording(heDvr.toString()/*expected*/, dbDvr /*actual*/);
        //show Cassandra item
        showCassandraRecording(heDvr.toString()/*expected*/, cassandraDvr/*actual*/);
            
        
        //verify status
        Assert.assertTrue(200 == httpResponse.getStatus(),"Expected status: 200 Actual: "+ httpResponse.getStatus());
        
        //verify AMS-HE response
        verifyJSON(response.toJSON(), new JSONObject((String)httpResponse.getEntity()),"Expected and actual AMS response must be the same");
        
        //verify AMS-STB request
        Assert.assertTrue(Arrays.equals(he.getData(), actualSTB), "Expected and actual request from AMS to STB must be the same");
        
        //verify DB & Cassandra
        verifyJSON(heDvr.toJSON(), dbDvr.toJSON(), "Expected and actual information in DB DVR_RECORDING table must be the same");
        verifyJSON(heDvr.toJSON(missingCassandra), cassandraDvr.toJSON(), "Expected and actual information in CASSANDRA RECORDINGS table must be the same");
    }
    
    private void invalidErrorImpl(Error error) throws Exception{
        invalidErrorStatusImpl(error, Error.Error0);
    }
    
    private void invalidStatusImpl(Error status) throws Exception{
        invalidErrorStatusImpl(Error.Error0, status);
    }
    
    private void invalidErrorStatusImpl(Error error, Error status) throws Exception{
        int id = getRandomInt(1, 1_000_000);
        int type = getRandomObject(Type.values()).getCode();
        prepareDB(id, type);
        
        ModifyRequest request = new ModifyRequest(deviceId, id, getRandomInt(1, 1_000));
        request.calculateMask();
        
        ModifyResponse response = ModifyResponse.getRandom(id);
        response.setError(error);
        response.setStatus(status);
        stepsModifyTest(request, response);
    }
}
