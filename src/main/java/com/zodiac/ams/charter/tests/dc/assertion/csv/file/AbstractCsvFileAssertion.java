package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import java.util.List;

/**
 * The abstract CSV file assertion implementation.
 *
 * @param <E> the type of CSV file entry
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public abstract class AbstractCsvFileAssertion<E> implements CsvFileAssertion<E> {
    protected final CsvFileAssertionInput input;

    /**
     * The regular constructor.
     *
     * @param input the input
     */
    protected AbstractCsvFileAssertion(CsvFileAssertionInput input) {
        if (input == null) {
            throw new IllegalArgumentException("The input can't be empty");
        }

        this.input = input;
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    public abstract void assertFile(List<E> csvFileEntries);
}
