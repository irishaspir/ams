package com.zodiac.ams.charter.tests.ft.rudp;

import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.getSegments;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.setTestingSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getRUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.IDataPacketProvider;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacketSerializer;
import com.zodiac.ams.charter.services.ft.rudp.packet.PacketType;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedAnyResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureSessionResult;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;


public class TestingServiceN003 extends TestingService {
    
    private static final ErrorCodeType expected = ErrorCodeType.FILEID;
    
    TestingServiceN003(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    class DataPacketProvider implements IDataPacketProvider{
        
        private final int badSegmentNumber;
        private final int invalidSessionId;
        private final RUDPTestingSession ts;
        
        private DataPacketProvider(RUDPTestingSession ts, int badSegmentNumber,int invalidSessionId){
            this.ts = ts;
            this.badSegmentNumber = badSegmentNumber;
            this.invalidSessionId = invalidSessionId;
        }

        @Override
        public byte[] get(int sessionId, int segmentNumber, DataPacket dataPacket) {
            try {
                return  (segmentNumber == badSegmentNumber) ?
                    RUDPTestingUtils.generateDataPacket(PacketType.DATA.packetType, invalidSessionId, segmentNumber,dataPacket.getData()) :
                    DataPacketSerializer.toBytes(dataPacket);
            }
            catch(Exception ex){
                if (null != ts){
                    ts.setErrorMsg(ex.getMessage());
                    ts.setSuccess(false);
                }
                return null;
            }
        }
    }
    
    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = getTestingSession(sessionId);
            RUDPTestingPackage pk = getRUDPTestingPackage(sessionId);
            
            int mockId = sender.createSenderSession(ts.getRUDPTestingPackage().toBytes().length);
            setTestingSession(mockId,ts);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (!getExpectedSuccessResult(sessionId,future))
                return;
            
            int count = pk.getSegmentsCount();
            int [] segments = getSegments(count,order);
            int badIndex = segments.length /2;
            int badNumber = segments[badIndex];
            
            sender.setDataPacketProvider(sessionId, new DataPacketProvider(ts,badNumber,mockId));
            
            int index = 0;
            for (int segment: segments){
                future=sender.sendSegment(sessionId,segment);
                if (index < badIndex){
                    if (!getExpectedSuccessResult(sessionId,future))
                        return;
                }
                else 
                if (index == badIndex)
                    getExpectedFailureResult(sessionId,future,expected);
                else 
                    getExpectedAnyResult(sessionId,future);
                
                index++;
            }
        
            getExpectedFailureSessionResult(sessionId);
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            sender.removeDataPacketProvider(sessionId);
            if (null != this.countDown)
                countDown.countDown();
        }
    }
}
