package com.zodiac.ams.charter.tests.dc;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.common.logging.Logger;
import com.zodiac.ams.common.ssh.SSHHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.tests.dc.TestUtils.parseMacsString;

/**
 * The local file utilities.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
@SuppressWarnings("unused")
public class LocalFileUtils {
    private static final String DEFAULT_ENCODING = "UTF-8";

    // TODO fix later
    private static final String PREFIX_UNKNOWN = "0242AC110005";

    private static final String PREFIX_FILENAME_ACTIVITY = /*PREFIX_UNKNOWN + */"_Activity";
    // TODO fix later
    private static final String PREFIX_FILENAME_DVR = /*PREFIX_UNKNOWN + */"_DVR";
    private static final String PREFIX_FILENAME_HDD = /*PREFIX_UNKNOWN + */"_HDDEvent";
    private static final String PREFIX_FILENAME_SESSION = /*PREFIX_UNKNOWN + */"_Session";
    private static final String PREFIX_FILENAME_STB = /*PREFIX_UNKNOWN + */"_STB";
    private static final String PREFIX_FILENAME_TSB = /*PREFIX_UNKNOWN + */"_TSBInfo";
    private static final String PREFIX_FILENAME_TUNING = /*PREFIX_UNKNOWN + */"_Tuning";
    private static final String PREFIX_FILENAME_TUNER_USE = "_TunerUse";

    /*
     * The private constructor.
     */
    private LocalFileUtils() {
        // do nothing here
    }

    /**
     * The method to delete files.
     *
     * @param filenames the filenames
     * @return the count of deleted files
     */
    public static int deleteFiles(List<String> filenames) {
        if (CollectionUtils.isEmpty(filenames)) {
            throw new IllegalArgumentException("The filenames can't be null or empty");
        }

        int counter = 0;
        for (String filename : filenames) {
            if (new File(filename).delete()) {
                Logger.info("The file with filename = '{}' deleted", filename);

                counter++;
            } else {
                Logger.info("Failed to delete the file with filename = '{}'", filename);
            }
        }

        Logger.info("We have tried to delete '{}' files and successfully have deleted '{}' files ", filenames, counter);
        return counter;
    }

    /**
     * The method to read a CSV file.
     *
     * @param filename      the filename
     * @param separatorChar the separator char
     * @return the list of CSV file entries
     * @throws IOException in case of an I/O error while reading the file
     */
    public static List<List<String>> readCsvFile(String filename, char separatorChar) throws IOException {
        List<List<String>> result;

        File file = new File(filename);
        @SuppressWarnings("unchecked")
        List<String> lines = FileUtils.readLines(file, DEFAULT_ENCODING);
        if (CollectionUtils.isNotEmpty(lines)) {
            result = new ArrayList<>(lines.size());
            for (String line : lines) {
                if (StringUtils.isNoneBlank(line)) {
                    String[] lineArray = StringUtils.splitPreserveAllTokens(line, separatorChar);
                    lineArray = StringUtils.stripAll(lineArray);
                    result.add(Arrays.asList(lineArray));
                }
            }
        } else {
            result = null;
        }

        return result;
    }

    /**
     * The method to read the mac ip mapping file.
     *
     * @param file the file
     * @return the mac ip mapping
     */
    public static Map<InetAddress, List<Long>> readMacIpMappingFile(File file) {
        Map<InetAddress, List<Long>> result = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] pair = line.split(";");
                if (pair.length != 2) {
                    continue;
                }
                List<Long> macs = parseMacsString(pair[1]);
                InetAddress address = InetAddress.getByName(pair[0]);
                result.put(address, macs);
            }
        } catch (IOException ioe) {
            Logger.error("Exception was thrown while reading the mac ip mapping file", ioe);
        }

        return result;
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getActivityFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_ACTIVITY);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getDvrFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_DVR);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getHddFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_HDD);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getSessionFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_SESSION);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getStbFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_STB);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getTsbFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_TSB);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @return the filename
     */
    public static String getTuningFilename(String pathname) throws IOException {
        return getFilenameInternal1(pathname, PREFIX_FILENAME_TUNING);
    }

    public static String getTunerUseFilename(String pathname) throws IOException{
        return getFilenameInternal1(pathname, PREFIX_FILENAME_TUNER_USE);
    }

    /**
     * The method to get the filename.
     *
     * @param pathname the pathname
     * @param prefix   the prefix
     * @return the filename
     * @throws IOException if an I/O error occurs
     */
    private static String getFilenameInternal1(String pathname, String prefix) throws IOException {
        List<String> filenames = getFilenameInternal(pathname, prefix);
        if (filenames != null && filenames.size() == 1) {
            return filenames.get(0);
        }
        if(filenames == null || filenames.isEmpty()) {
            throw new IllegalStateException("Failed to get the files in the folder " + pathname +
                    " with the prefix " + prefix);
        } else {
            throw new IllegalStateException("Failed to get the filename; more than one file match the prefix" +
                    " or there is no matching: pathname = " + pathname + ", prefix = " + prefix + ", filenames = " + filenames);
        }
    }

    private static List<String> getFilenameInternal(String pathname, String prefix) throws IOException {
        return getFilenameInternalRemote(pathname, prefix);
    }

    /**
     * The method to get the list of filenames using the given prefix.
     *
     * @param pathname the pathname
     * @param prefix   the prefix
     * @return the list of filenames
     * @throws IOException if an I/O error occurs
     */
    private static List<String> getFilenameInternalLocal(String pathname, String prefix) throws IOException {
        File dir = new File(pathname);
        // TODO fix later
        File[] files = dir.listFiles(/*(FilenameFilter) new PrefixFileFilter(prefix)*/);
        if (files != null) {
            List<String> filenames = new ArrayList<>();
            for (File file : files) {
                String filename = file.getName();
                Logger.info("WE FOUND filename = '{}' with prefix of '{}'", filename, prefix);
                if (filename.contains(prefix)) {
                    //if (filename.startsWith(prefix)) {
                    Logger.info("The filename = '{}' matches the prefix of '{}'", filename, prefix);

                    filenames.add(file.getCanonicalPath());
                } else {
                    Logger.info("The filename = '{}' doesn't match the prefix of '{}'", filename, prefix);
                }
            }
            return filenames;
        }
        return null;
    }

    private static final Pattern PATTERN = Pattern.compile(".*\\s+([^\\s]+\\.csv)$");
    public static final String TEMP_DIR = "temp";
    private static SSHHelper executor;
    static {
        File f = new File(TEMP_DIR);
        if (!f.exists())
            f.mkdir();
        FTConfig cfg = FTConfig.getInstance();
        executor = new SSHHelper(cfg.getDCSftpHost(), cfg.getDCSftpLogin(), cfg.getDCSftpPwd());
    }

    private static List<String> getFilenameInternalRemote(String pathname, String prefix) throws IOException{
        List<String> result = null;
        String output = executor.getExecOutput("sudo ls -l "+pathname);
        String [] lines = output.split("\\n");
        for (String line: lines){
            Matcher m = PATTERN.matcher(line);
            if (m.matches()){
                String name = m.group(1);
                if (name.contains(prefix)) {
                    Logger.info("The filename = '{}' matches the prefix of '{}'", name, prefix);
                    if (null == result)
                        result = new ArrayList<>();
                    String local = TEMP_DIR + "/" + name;
                    String remote = pathname + "/" + name;
                    executor.getFile(local, remote);
                    executor.exec("sudo rm " + remote);
                    result.add(new File(local).getCanonicalPath());
                }
                else
                    Logger.info("The filename = '{}' doesn't match the prefix of '{}'", name, prefix);
            }
        }
        return result;
    }

}
