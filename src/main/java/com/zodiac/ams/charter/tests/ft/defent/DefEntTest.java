package com.zodiac.ams.charter.tests.ft.defent;

import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.handlers.DENTNewHandler;
import com.dob.test.charter.handlers.ErrorReportingHandler;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.defent.ChannelType;
import com.zodiac.ams.charter.services.ft.defent.DefEntMessage;
import com.zodiac.ams.charter.services.ft.defent.EntError;
import com.zodiac.ams.charter.services.ft.defent.Group0;
import com.zodiac.ams.charter.services.ft.defent.Group1;
import com.zodiac.ams.charter.services.ft.defent.Group2;
import com.zodiac.ams.charter.services.ft.defent.Group3;
import com.zodiac.ams.charter.services.ft.defent.GroupFactory;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.charter.tests.ft.IDescriptionProvider;
import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.zodiac.ams.charter.services.ft.defent.IGroup;
import com.zodiac.ams.charter.services.ft.defent.ISTBRequest;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.defent.STBMessage;
import com.zodiac.ams.common.http.FTHttpUtils;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import org.skyscreamer.jsonassert.JSONAssert;
import org.testng.Assert;
import java.net.URI;

public class DefEntTest extends FuncTest{
    private final static Logger LOG = LoggerFactory.getLogger(DefEntTest.class);
    private final static long MAX_TIMEOUT_MS = 2 * 60 * 1000; //2min
    
    private static final Pattern PATTERN_ERROR_TEST_NAME = Pattern.compile("sendingError(\\d+)");
    private static final String DEFENT_DATA_DIR = "dataDefEnt";
    private static final String DEFENT_ERROR_TEST = "error.txt" ;
    
    //private static final String ERROR_REPORTING_SERVICE_ID = "errorReporting";
    private static final String ERROR_REPORTING_SERVICE_ID = "E";
    
    private static final String ERROR_MIDDLE_URI = "?req=SetErrors";
    
    
    static List<EntError> errors;
    
    static {
        prepareSendingErrorTests();
    }
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        String uriErr = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_ERROR_REPORTING_CONTEXT;
        String uriDent = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_DENT_NEW_CONTEXT;
        helper.addErrorReportingService(uriErr);
        helper.addDefEntService(uriDent);
        helper.save("server.xml");
    }
    
    //all parameters and description  - see dataDefEnt/error.txt
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError1() throws Exception {Assert.assertTrue(waitForAMS(AMS_TIMEOUT),"AMS is not active"); sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError2() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError3() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError4() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError5() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError6() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError7() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError8() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError9() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError10() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError11() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError12() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError13() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError14() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError15() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError16() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError17() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError18() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError19() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError20() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError21() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError22() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError23() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError24() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError25() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError26() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError27() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError28() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError29() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError30() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError31() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError32() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError33() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError34() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError35() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError36() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError37() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError38() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError39() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError40() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError41() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError42() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError43() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError44() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError45() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError46() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError47() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError48() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError49() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError50() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError51() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError52() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError53() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError54() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError55() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError56() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError57() throws Exception {sendingErrorImpl();}
    @Test(priority=2) @Features("Entitlements") @Stories("Error reporting") @Description("") 
    public void sendingError58() throws Exception {sendingErrorImpl();}    
    
    
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.61 [Default Entitlements] Sending Error reporting. Check format of  request for GroupID=0") 
    @Test(priority=3) 
    public void test_12_2_61() throws Exception{
        List<EntError> e = getErrorsByGroup(0);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 5, true /*disctinct*/));
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.62 [Default Entitlements] Sending Error reporting. Check format of  request for GroupID=1") 
    @Test(priority=3) 
    public void test_12_2_62() throws Exception{
        List<EntError> e = getErrorsByGroup(1);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 5, false /*no disctinct, only 1*/));
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.63 [Default Entitlements] Sending Error reporting. Check format of  request for GroupID=2") 
    @Test(priority=3) 
    public void test_12_2_63() throws Exception{
        List<EntError> e = getErrorsByGroup(2);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 5, true /*disctinct*/));
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.64 [Default Entitlements] Sending Error reporting. Check format of  request for GroupID=3") 
    @Test(priority=3) 
    public void test_12_2_64() throws Exception{
        List<EntError> e = getErrorsByGroup(3);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 5, false /*no disctinct, only 4*/));
    }

    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.74 [Default Entitlements] Sending Error reporting. Sending 63 errors in one request of groupID=0") 
    @Test(priority=4) 
    public void test_12_2_74() throws Exception{
        List<EntError> e = getErrorsByGroup(0);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 63, false /*disctinct*/));
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.75 [Default Entitlements] Sending Error reporting. Sending 63 errors in one request of groupID=1") 
    @Test(priority=4) 
    public void test_12_2_75() throws Exception{
        List<EntError> e = getErrorsByGroup(0);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 63, false /*disctinct*/));
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.76 [Default Entitlements] Sending Error reporting. Sending 63 errors in one request of groupID=2") 
    @Test(priority=4) 
    public void test_12_2_76() throws Exception{
        List<EntError> e = getErrorsByGroup(0);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 63, false /*disctinct*/));
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.77 [Default Entitlements] Sending Error reporting. Sending 63 errors in one request of groupID=3") 
    @Test(priority=4) 
    public void test_12_2_77() throws Exception{
        List<EntError> e = getErrorsByGroup(0);
        sendingErrorImpl(RandomUtils.getRandomObject(e.toArray(new EntError[]{}), 63, false /*disctinct*/));
    }
    
    
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.4.3 [Default Entitlements] Error Notify. ErrorNotify request timed out") 
    @Test(priority=5) 
    public void test_12_4_3() throws Exception{
        sendingErrorImpl(RandomUtils.getRandomObject(errors.toArray(new EntError[]{})), 200, 61_000);
    }
    
    private static boolean SLEEP = false;
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.4.4 [Default Entitlements] Error Notify. STB Error Middle Interaction sends HTTP error code = 500 to AMS") 
    @Test(priority=5) 
    public void test_12_4_4() throws Exception{
//        Assert.assertTrue(waitForAMS(AMS_TIMEOUT));
        sendingErrorImpl(RandomUtils.getRandomObject(errors.toArray(new EntError[]{})), 500, 0);
    }
    
    
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.4.5 [Default Entitlements] Error Notify. STB Error Middle sends HTTP error code = 400 to AMS") 
    @Test(priority=5) 
    public void test_12_4_5() throws Exception{
        sendingErrorImpl(RandomUtils.getRandomObject(errors.toArray(new EntError[]{})), 400, 0);
    }
    
    
    @Features("Entitlements") 
    @Stories("Error reporting. Negative") 
    @Description("12.2.65 [Default Entitlements] Sending Error reporting. Value of application id is not in list of application id values") 
    @Test(priority=6) 
    public void test_12_2_65() throws Exception{
        EntError err = getRandomError(3);
        IGroup group = new Group3(properties.getDeviceId(), err, null);
        sendingErrorImpl(group, 200, 0);
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting. Negative") 
    @Description("12.2.66 [Default Entitlements] Sending Error reporting. Value of application screen id is not in list of screen id values") 
    @Test(priority=6) 
    public void test_12_2_66() throws Exception{
        EntError err = getRandomError(1);
        IGroup group = new Group1(properties.getDeviceId(), err, null);
        sendingErrorImpl(group, 200, 0);
    }
    
    
    @Features("Entitlements") 
    @Stories("Error reporting. Negative") 
    @Description("12.2.72 [Default Entitlements] Sending Error reporting. Value of channelType is not in set of  channelType values") 
    @Test(priority=6) 
    public void test_12_2_72() throws Exception{
        EntError err = getRandomError(0);
        IGroup group = new Group0(properties.getDeviceId(), err, null);
        sendingErrorImpl(group, 200, 0);
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting. Negative") 
    @Description("12.2.73 [Default Entitlements] Sending Error reporting. Value of reasonCode is not in list of reason values") 
    @Test(priority=6) 
    public void test_12_2_73() throws Exception{
        EntError err = getRandomError(2);
        IGroup group = new Group2(properties.getDeviceId(), err, null);
        sendingErrorImpl(group, 200, 0);
    }
    
    
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.67 [Default Entitlements] Sending Error reporting. Value of channelType = 0") 
    @Test(priority=7) 
    public void test_12_2_67() throws Exception{
        EntError err = getRandomError(0);
        IGroup group = new Group0(properties.getDeviceId(), err, ChannelType.LIVE);
        sendingErrorImpl(group, 200, 0);
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.68 [Default Entitlements] Sending Error reporting. Value of channelType = 1") 
    @Test(priority=7) 
    public void test_12_2_68() throws Exception{
        EntError err = getRandomError(0);
        IGroup group = new Group0(properties.getDeviceId(), err, ChannelType.VOD);
        sendingErrorImpl(group, 200, 0);
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.69 [Default Entitlements] Sending Error reporting. Value of channelType = 2") 
    @Test(priority=7) 
    public void test_12_2_69() throws Exception{
        EntError err = getRandomError(0);
        IGroup group = new Group0(properties.getDeviceId(), err, ChannelType.PPV);
        sendingErrorImpl(group, 200, 0);
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.70 [Default Entitlements] Sending Error reporting. Value of channelType = 3") 
    @Test(priority=7) 
    public void test_12_2_70() throws Exception{
        EntError err = getRandomError(0);
        IGroup group = new Group0(properties.getDeviceId(), err, ChannelType.EAS);
        sendingErrorImpl(group, 200, 0);
    }
    
    @Features("Entitlements") 
    @Stories("Error reporting") 
    @Description("12.2.71 [Default Entitlements] Sending Error reporting. Value of channelType = 4") 
    @Test(priority=7) 
    public void test_12_2_71() throws Exception{
        EntError err = getRandomError(0);
        IGroup group = new Group0(properties.getDeviceId(), err, ChannelType.APP);
        sendingErrorImpl(group, 200, 0);
    }
                                    
    
    @Features("Entitlements") 
    @Stories("Entitlements AMS log")
    @Description("Entitlements AMS log")
    @Test(priority=100)
    public void getDentAmsLog() throws Exception {
        getServerXml();
        stopAndgetAMSLog(true);
    }
    
    private EntError getRandomError(int group){
        List<EntError> list = getErrorsByGroup(group);
        return RandomUtils.getRandomObject(list.toArray(new EntError[]{}));
    }
    
    
    private EntError[] getRandomErrors(){
        int count = RandomUtils.getRandomInt(1, 5);
        if (0 == count)
            return new EntError[]{};
        List<EntError> list = getErrorsByGroup(0);
        return RandomUtils.getRandomObject(list.toArray(new EntError[]{}), count, true);
    }
                            
                            
    
    private void sendingErrorImpl() throws Exception {
        sendingErrorImpl(new EntError[]{getEntError()});
    }
    
    private void sendingErrorImpl(EntError[] errors) throws Exception{   
        sendingErrorImpl(errors, 200 /*HE Response*/, 0 /*HE Delay*/);
    }
    
    private void sendingErrorImpl(EntError error, int heResponse, long heDelay) throws Exception{   
        sendingErrorImpl(new EntError[]{error}, heResponse, heDelay);
    }
    
    
    private void sendingErrorImpl(EntError[] errors, int heResponse, long heDelay) throws Exception{   
        IGroup group = GroupFactory.getGroup(properties.getDeviceId(), errors);
        sendingErrorImpl(group, heResponse, heDelay);
    }
    
    
    
    /**
     * 
     * @param message HE response body; empty if message is null
     * @param httpResponse HE response code; no start HE emulator if httpResponse = 0
     * @param expectedHTTPParams HE expected URI parameters
     * @param stbRequest
     * @param verifier
     * @throws Exception 
     */
    private void stbToAmsImpl(DefEntMessage message, int httpResponse, Map<String,String> expectedHTTPParams,
        ISTBRequest stbRequest, DENTVerifier verifier) throws Exception{
        
        LighweightCHEemulator he = null;
        CharterStbEmulator stb = null;
        EntError [] err = null;
        
        Semaphore semaphoreHEErr = null;
        DataConsumer  consumerHEErr = null;
        
        try {
//            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            Thread.sleep(20_000);
//            
            if (0 != httpResponse){
                if (null != message)
                    err = message.getErrors();
                CHEmulatorHelper helper =new CHEmulatorHelper(); 
                helper.setProperty(CHEmulatorHelper.DENT_NEW_CONTEXT_PROP, CHEmulatorHelper.DEFAULT_DENT_NEW_CONTEXT + "/" + properties.getDeviceId());
                he = helper.createEmulator();
                DENTNewHandler handler = (DENTNewHandler)he.getHandler(DENTNewHandler.class);
                handler.setResponseCode(httpResponse);
                if (null != message)
                    handler.setResponseBody(message.toString());
                handler.setMandatoryParams(expectedHTTPParams);
                if (null != err){
                    semaphoreHEErr = new Semaphore(0);
                    consumerHEErr = new DataConsumer(semaphoreHEErr);
                    he.registerDataConsumer(consumerHEErr, HEHandlerType.ERROR_REPORTING);
                }
                startCharterHEEmulator(he);
            }
            
        
            stb = createCharterSTBEmulator();
            startCharterSTBEmulator(stb);
            
            ZodiacMessage msg = new ZodiacMessage();
            msg.setBodyBytes();
            msg.setMessageId(String.valueOf(0));
            msg.setSender("DENT"); 
            msg.setAddressee("DENT");
            msg.setMac(Long.parseLong(properties.getDeviceId(), 16));
            msg.setData(stbRequest.getData());
            //msg.setFlag(ZodiacMessage.FL_PERSISTENT);
            msg.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
            //msg.setFlag(ZodiacMessage.FL_COMPRESSED);
            ZodiacMessage response = (ZodiacMessage)stb.getRUDPTransport().send(msg, MAX_TIMEOUT_MS);
            
            if (null != message)
                showHEExpectedResponse(message.toString());
            
            if (null != verifier)
                verifier.verify(message, response.getData());
            if (null != consumerHEErr)
                verifyErrorMiddle(message, consumerHEErr);
        }
        finally{
            if (null != he)
                he.stop();
            if (null != stb)
                stb.stop();
        }
    }
    
    
    
    //==========================================================================
    // STB -> AMS
    //==========================================================================
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.1 [Default Entitlements] Default Entitlements. GetEntitlements requestId=0. Good path") 
    @Test(priority=8) 
    public void test_12_3_1() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), false, RandomUtils.getRandomIntArray(), getRandomErrors());
        stbToAmsImpl(message, 200,  new HashMap(){{put("requestor","AMS");}}, new STBMessage(), new PackageVerifier());
    }
    
    
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.12 [Default Entitlements] getEntitlements request from STB till LRM server. Good path") 
    @Test(priority=8) 
    public void test_12_3_12() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), false, RandomUtils.getRandomIntArray(), getRandomErrors());
        stbToAmsImpl(message, 200,  new HashMap(){{put("requestor","AMS");}}, new STBMessage(), new PackageVerifier());
    }
            
    
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.13 [Default Entitlements] getEntitlements request. Connection to LRM is timeouted") 
    @Test(priority=9) 
    public void test_12_3_13() throws Exception {
        stbToAmsImpl(null, 0 /*HE Response status*/,  null /*HE mandatory params*/, new STBMessage(), new ErrorVerifier(3));
    }
    
    
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.14 [Default Entitlements] getEntitlements request. Get empty lineup JSON") 
    @Test(priority=9) 
    public void test_12_3_14() throws Exception {
        stbToAmsImpl(null /*HE Response body*/, 200 /*HE Response status*/,  new HashMap(){{put("requestor","AMS");}} /*HE mandatory params*/, 
            new STBMessage(), new ErrorVerifier(1));
    }
    
                                    
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.15 [Default Entitlements] getEntitlements request. HTTP error code = 204 from LRM") 
    @Test(priority=9) 
    public void test_12_3_15() throws Exception {
        stbToAmsImpl(null /*HE Response body*/, 204 /*HE Response status*/,  new HashMap(){{put("requestor","AMS");}} /*HE mandatory params*/, 
            new STBMessage(), new ErrorVerifier(3));
    }

                                    
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.16 [Default Entitlements] getEntitlements request. HTTP error code = 400 from LRM") 
    @Test(priority=9) 
    public void test_12_3_16() throws Exception {
        stbToAmsImpl(null /*HE Response body*/, 400 /*HE Response status*/,  new HashMap(){{put("requestor","AMS");}} /*HE mandatory params*/, 
            new STBMessage(), new ErrorVerifier(3));
    }
    
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.17 [Default Entitlements] getEntitlements request. HTTP error code = 404 from LRM") 
    @Test(priority=9) 
    public void test_12_3_17() throws Exception {
        stbToAmsImpl(null /*HE Response body*/, 404 /*HE Response status*/,  new HashMap(){{put("requestor","AMS");}} /*HE mandatory params*/, 
            new STBMessage(), new ErrorVerifier(3));
    }
            
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.18 [Default Entitlements] getEntitlements request. HTTP error code = 500 from LRM") 
    @Test(priority=9) 
    public void test_12_3_18() throws Exception {
        stbToAmsImpl(null /*HE Response body*/, 500 /*HE Response status*/,  new HashMap(){{put("requestor","AMS");}} /*HE mandatory params*/, 
            new STBMessage(), new ErrorVerifier(3));
    }
     
            
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.3 [Default Entitlements] Default Entitlements. GetEntitlements requestId=0. Response with error code = 4") 
    @Test(priority=9) 
    public void test_12_3_3() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), true, RandomUtils.getRandomIntArray(), getRandomErrors());
        stbToAmsImpl(message /*HE Response body*/, 200 /*HE Response status*/,  new HashMap(){{put("requestor","AMS");}} /*HE mandatory params*/, 
            new STBMessage(), new ErrorVerifier(4));
    }
    
    //==========================================================================
    //Middle -> AMS -> STB
    //==========================================================================
    
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.19 [Default Entitlements] setEntitlements request from Notification server to STB. Good path") 
    @Test(priority=10)
    public void test_12_3_19() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), false, RandomUtils.getRandomIntArray(), getRandomErrors());
        amsToStbImpl(message, true /*startStb*/, null /*stbParams*/, 200 /*expectedStatus*/, new ResponseVerifierOK(properties.getDeviceId()));
    }
     
    
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.20 [Default Entitlements] setEntitlements request from Notification server to STB. STB is unavailable for set new entitlements") 
    @Test(priority=10)
    public void test_12_3_20() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), false, RandomUtils.getRandomIntArray());
        amsToStbImpl(message, false /*startStb*/, null /*stbParams*/, 500 /*expectedStatus*/, new ResponseVerifierFAILED(properties.getDeviceId()));
    }
    
    @Features("Entitlements") 
    @Stories("Entitlements. Negative") 
    @Description("12.3.21 [Default Entitlements] setEntitlements request from Notification server to STB. Validation of response is failed") 
    @Test(priority=10)
    public void test_12_3_21() throws Exception {
        amsToStbImpl(null, false /*startStb*/, null /*stbParams*/, 400 /*expectedStatus*/, new ResponseVerifierFAILED(properties.getDeviceId()));
    }
    
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.4 [Default Entitlements] Default entitlements. GetEntitlements requestId=3. STB response with 0 error code") 
    @Test(priority=10)
    public void test_12_3_4() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), true, RandomUtils.getRandomIntArray(), getRandomErrors());
        amsToStbImpl(message, true /*startStb*/, null /*stbParams*/, 200 /*expectedStatus*/, new ResponseVerifierOK(properties.getDeviceId()));
    }
    
    @Features("Entitlements") 
    @Stories("Entitlements") 
    @Description("12.3.5 [Default Entitlements] Default entitlements. GetEntitlements requestId=3. STB response with 1 error code") 
    @Test(priority=10)
    public void test_12_3_5() throws Exception {
        DefEntMessage message = new DefEntMessage(properties.getDeviceId(), true, RandomUtils.getRandomIntArray(), getRandomErrors());
        amsToStbImpl(message, true /*startStb*/, new HashMap(){{put("dent-is-error-return",String.valueOf(true));}} /*stbParams*/,
            200 /*expectedStatus*/, new ResponseVerifierFAILED(properties.getDeviceId()));
    }
    
    public void amsToStbImpl(DefEntMessage message, boolean startStb, Map<String,String> stbParams, int expectedStatus, IResponseVerifier verifier) throws Exception {
        //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //Thread.sleep(20_000);
        
        LighweightCHEemulator he = null;
        CharterStbEmulator stb = null;
        
        Semaphore semaphoreSTB = new Semaphore(0);
        DataConsumer consumerSTB = new DataConsumer(semaphoreSTB);
        
        Semaphore semaphoreHE = new Semaphore(0);
        DataConsumer  consumerHE = new DataConsumer(semaphoreHE);
        
        try {
            if (startStb){
                stb = createCharterSTBEmulator(stbParams);
                stb.registerConsumer(consumerSTB, HandlerType.Entitlements);
                startCharterSTBEmulator(stb);
                
                if (null != message && null != message.getErrors()){
                    he = createCharterHEEmulator(consumerHE, HEHandlerType.ERROR_REPORTING);
                    startCharterHEEmulator(he);
                }
            }
            String body = (null == message) ? "INVALID REQUEST" : message.toString();
        
            FTHttpUtils.HttpResult response=FTHttpUtils.doPost(properties.getDENTUri(), body);
            showHeToAmsRequest(properties.getDENTUri(), body, response);
            Assert.assertTrue(expectedStatus == response.getStatus(), "Expected status: " + expectedStatus + " Actual: " + response.getStatus());
            if (null != verifier)
                verifier.verify((String)response.getEntity());
            
            if (startStb){
                byte [] bytes = consumerSTB.getData();
                STBMessage expected  = new STBMessage(message);
                STBMessage actual = new STBMessage(bytes);
                showAmsToStbRequest(expected, actual);
                Assert.assertTrue(expected.equals(actual), "Expected: " + expected + " Actual: " + actual);
            }
            
            if (null != he)
                verifyErrorMiddle(message, consumerHE);
        }
        finally{
            if (null != stb)
                stb.stop();
            if (null != he)
                he.stop();
        }
        
    }
    
    private void verifyErrorMiddle(DefEntMessage message, DataConsumer consumer){
        String expected =message.errorsToJSON().toString();
        String actual = new String(consumer.getData());
        URI uri = consumer.getURI();
        showSTBErrorMiddleRequest(uri, expected, actual);
        try {
            JSONAssert.assertEquals(new JSONObject(expected), new JSONObject(actual), false);
        }
        catch(AssertionError ex){
            Assert.assertTrue(false, "Expected and actual JSON must be the same");
        }
        //MASTER 453
        //Assert.assertTrue(uri.toString().endsWith(ERROR_MIDDLE_URI), "Request to STB Error Middle must contain " + ERROR_MIDDLE_URI);
    }
    
    @Attachment(value = "STB Error Middle Request", type = "text/plain")
    protected String showSTBErrorMiddleRequest(URI uri, String expected, String actual) {
        StringBuilder sb = new StringBuilder();
        return sb.append("URI: ").append(uri).
            append("\n\nExpected:\n\n").append(expected).
            append("\n\nActual:\n\n").append(actual).toString();
    }
    
    @Attachment(value = "HE to AMS Request", type = "text/plain")
    protected String showHeToAmsRequest(String uri, String request, FTHttpUtils.HttpResult response) {
        StringBuilder sb = new StringBuilder();
        sb.append("URI: ").append(uri).
        append("\n\nRequest:\n\n").append(request).
        append("\n\nResponse: ").append(response.getStatus()).append("\n\n").append(response.getEntity());
        return sb.toString();
    }
    
    @Attachment(value = "AMS to STB Request", type = "text/plain")
    protected String showAmsToStbRequest(STBMessage expected, STBMessage actual) {
        StringBuilder sb = new StringBuilder();
        sb.append("Expected:\n\n").append(expected).append("\n\nActual:\n\n").append(actual);
        return sb.toString();
    }
     
    /**
     * 
     * @param errors
     * @param response HE response
     * @param delay HE delay in ms
     * @throws Exception 
     */
    private void sendingErrorImpl(IGroup group, int heResponse, long heDelay) throws Exception{   
        LighweightCHEemulator he = null;
        CharterStbEmulator stb = null;
        
        try {
            Semaphore semaphore = new Semaphore(0);
            
            DataConsumer  consumer = new DataConsumer(semaphore);
            he = createCharterHEEmulator(consumer, HEHandlerType.ERROR_REPORTING);
            ErrorReportingHandler handler = (ErrorReportingHandler) he.getHandler(ErrorReportingHandler.class);
            handler.setResponse(heResponse);
            handler.setDelay(heDelay);
            startCharterHEEmulator(he);
        
            stb = createCharterSTBEmulator();
            startCharterSTBEmulator(stb);
        
            ZodiacMessage message = new ZodiacMessage();
            message.setBodyBytes();
            message.setMessageId(String.valueOf(0));
            message.setSender(ERROR_REPORTING_SERVICE_ID); 
            message.setAddressee(ERROR_REPORTING_SERVICE_ID);
            message.setMac(Long.parseLong(properties.getDeviceId(), 16));
            message.setData(group.getMessage());
            
            //message.setFlag(ZodiacMessage.FL_PERSISTENT);
            message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
            //message.setFlag(ZodiacMessage.FL_COMPRESSED);
        
            ZodiacMessage response = null;
            if (0 == heDelay)
                response = (ZodiacMessage)stb.getRUDPTransport().send(message);
            else
                response = (ZodiacMessage)stb.getRUDPTransport().send(message, heDelay+10_000);
                
            DOBCountingInputStream in = new DOBCountingInputStream(new ByteArrayInputStream(response.getData()));
            int error = DOB7bitUtils.decodeUInt(in)[0];
            Assert.assertTrue(0 == error, "Invalid AMS Response. Error must be 0, but found "+error);
            
            String expected = group.toJSON().toString();
            String actual = new String(consumer.getData());
                    
            showErrorRequest(expected, actual);
            try {
                JSONAssert.assertEquals(group.toJSON(), new JSONObject(actual), false);
            }
            catch(AssertionError ex){
                Assert.assertTrue(false, "Expected and actual JSON must be the same");
            }
        }
        finally {
            if (null != he)
                he.stop();
            if (null != stb)
                stb.stop();
        }
    }
    
    
    @Attachment(value = "HE response", type = "text/plain")
    protected String showHEExpectedResponse(String request) {
        return request;
    }
    
    @Attachment(value = "AMS to HeadEnd request", type = "text/plain")
    protected String showErrorRequest(String expected, String actual) {
        StringBuilder sb = new StringBuilder();
        sb.append("Expected:\n\n").append(expected).append("\n\nActual:\n\n").append(actual);
        return sb.toString();
    }
    
    private EntError getEntError() {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : elements) {
            String name = element.getMethodName();
            Matcher matcher = PATTERN_ERROR_TEST_NAME.matcher(name);
            if (!matcher.matches())
                continue;
            Method method;
            try{
                method = DefEntTest.class.getDeclaredMethod(name);
            }
            catch(NoSuchMethodException ex){
                continue;
            }
            //method.setAccessible(true);
            return errors.get(Integer.parseInt(matcher.group(1))-1);
        }
        return null;
    }
   
    
    private static  void  prepareSendingErrorTests(){
        String name = FTConfig.getFileFromJar(DEFENT_DATA_DIR, DEFENT_ERROR_TEST);
        
        //#|group|error|description|error code|tarrantula test
        errors = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(name));){
            String line;
            while (null != (line = in.readLine())){
                line = line.trim();
                if (line.startsWith("#") || line.equals(""))
                    continue;
                String [] tokens = line.split("\\|");
                errors.add(new EntError(tokens[2], Integer.parseInt(tokens[4]), tokens[3], Integer.parseInt(tokens[1]), tokens.length>5 ? tokens[5] : null));
            }
            
            FTAllureUtils.replaceDescription(DefEntTest.class, new IDescriptionProvider(){
                @Override
                public String getDescription(String test, String oldDescription) {
                    Matcher matcher = PATTERN_ERROR_TEST_NAME.matcher(test);
                    if (matcher.matches()){
                        int id = Integer.parseInt(matcher.group(1));
                        EntError error = errors.get(id-1);
                        String tarantulaId = error.getComment();
                        return ((null == tarantulaId) ? "" : tarantulaId + " ") + "[Default Entitlements] Sending " + error.getError() + " error from STB to AMS";
                    }
                    return oldDescription;
                }
            });
            
        }
        catch(Exception ex){
            LOG.error("prepareSendingErrorTests exception: {}", ex.getMessage());
        }
    }
    
    private static List<EntError> getErrorsByGroup(int group){
        List<EntError> result = new ArrayList<>();
        Iterator <EntError> it = errors.iterator();
        while (it.hasNext()){
            EntError e = it.next();
            if (group == e.getGroup())
                result.add(e);
        }
        return result;
    }
}
