package com.zodiac.ams.charter.tests.settings.stb;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.*;
import static com.zodiac.ams.charter.http.helpers.settings.UrlHelper.getUriPattern;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;
import static com.zodiac.ams.charter.tests.StoriesList.GET_SETTINGS_FROM_STB;
import static org.testng.Assert.assertEquals;


public class SendGetSettingsFromSTBTest extends GetSettingsBeforeAfter {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + GET_SETTINGS_FROM_STB)
    @Description("1.1.2 Send getSettings from STB to AMS, settings exist in BD AMS, " +
            "Server migration flag = 0, receive errorCode = 0 and settings list")
    @TestCaseId("169543")
    @Test()
    public void sendGetSettingsReceiveErrorCode0SettingsList() {
        String mac = MAC_NUMBER_1;
        setMigrationFlagNullInSettingsSTB(mac);
        ArrayList<String> response = settingsRudpHelper.sendGetSettings(mac);
        createAttachment(readAMSLog());
        assertEquals(response, settingsSTBResponse.getSettingsSTBResponsePattern(options), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), nullResponse(), requestToSTBMustBeAbsent);
        assertEquals(migrateFlagEqual1(), getMigrationFlagFromSettingsSTB(mac), mfIsIncorrect);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + GET_SETTINGS_FROM_STB)
    @Description("1.1.1 Send getSettings from STB to AMS, settings exist in BD AMS, " +
            "Server migration flag = 1, receive errorCode = 0 and null settings list")
    @TestCaseId("169544")
    @Test()
    public void sendGetSettingsReceiveErrorCode0SettingsListIsEmpty() {
        String mac = MAC_NUMBER_1;
        setMigrationFlagOneInSettingSTB(mac);
        ArrayList<String> response = settingsRudpHelper.sendGetSettings(mac);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        sa.assertEquals(response, settingsSTBResponse.stbErrorCodeNull(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), nullResponse(), requestToSTBMustBeAbsent);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE + GET_SETTINGS_FROM_STB)
    @Description("1.1.3 Send getSettings from STB to AMS, settings don't exist in BD AMS, AMS send getSetting to CHE," +
            " settings exists in BD CHE, receive settings list")
    @TestCaseId("169542")
    @Test()
    public void sendGetSettingsReceiveErrorCode0ReceiveDelta() {
        cleanBDForSettings();
        String mac = MAC_NUMBER_3;
        String deviceId = DEVICE_ID_NUMBER_3;
        ArrayList<String> response = settingsRudpHelper.sendGetSettings(mac);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.getSettingsSTBResponsePattern(delete55And59And60Keys(allSettings())),
                INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), getUriPattern(deviceId), urlIsIncorrect);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + GET_SETTINGS_FROM_STB)
    @Description("1.1.4 Send getSettings from STB to AMS, settings don't exist in BD AMS, settings don't exists in BD CHE")
    @TestCaseId("169545")
    @Test()
    public void sendGetSettingsReceiveError500() {
        cleanBDForSettings();
        String mac = MAC_NUMBER_4;
        String deviceId = DEVICE_ID_NUMBER_4;
        ArrayList<String> response = settingsRudpHelper.sendGetSettings(mac);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, settingsSTBResponse.stbErrorCodeNull(), INCORRECT_SETTINGS);
        assertEquals(cheEmulatorSettings.getURI(), getUriPattern(deviceId), urlIsIncorrect);
    }

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.NEGATIVE + GET_SETTINGS_FROM_STB)
    @Description("1.1.6 Send getSettings from STB to AMS, settings don't exist in BD AMS, AMS send getSetting to CHE, CHE is unavailable, " +
            "returned ZodiacPackage instead ZodiacMessageBuilder !!!!")
    @TestCaseId("169583")
    @Test()
    public void sendGetSettingsReceiveRunTimeException() {
        stopCHESettings();
        ArrayList<String> response = settingsRudpHelper.sendGetSettings(MAC_NUMBER_1);
        assertEquals(response, settingsSTBResponse.stbErrorCodeNull(), INCORRECT_SETTINGS);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
    }
}
