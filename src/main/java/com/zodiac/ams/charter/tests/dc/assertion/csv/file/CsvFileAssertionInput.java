package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import java.util.ArrayList;
import java.util.List;

/**
 * The CSV file assertion input.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
@SuppressWarnings("unused")
public class CsvFileAssertionInput {
    private List<Long> macs;
    private Long mac;

    private int messageType;

    private String messageId;
    private String sender;
    private String addresse;

    private int hubId;
    private int maxRebootsPerDay;
    private int nodeId;
    private List<SessionData> sessionDataList;
    private int sessionsNumber;
    private List<String> stringList;
    private int stringsNumber;

    /*
     * The event data class.
     */
    public static class EventData {
        private int eventId;
        private List<Object> parameters;
        private int timeDelta;
        private int timestampPartMsec;

        /*
         * The private constructor.
         */
        private EventData() {
            // do nothing here
        }

        public int getEventId() {
            return eventId;
        }

        public List<Object> getParameters() {
            return parameters;
        }

        public int getTimeDelta() {
            return timeDelta;
        }

        public int getTimestampPartMsec() {
            return timestampPartMsec;
        }

        /**
         * The nested builder class.
         */
        public static class Builder {
            private int eventId;
            private List<Object> parameters;
            private int timeDelta;
            private int timestampPartMsec;

            private SessionData.Builder parentBuilder;

            /*
             * The private constructor.
             */
            private Builder(SessionData.Builder parentBuilder) {
                this.parentBuilder = parentBuilder;
            }

            public Builder setEventId(int eventId) {
                this.eventId = eventId;
                return this;
            }

            public Builder addParameter(Object parameter) {
                if (parameters == null) {
                    parameters = new ArrayList<>();
                }
                parameters.add(parameter);
                return this;
            }

            public Builder setTimeDelta(int timeDelta) {
                this.timeDelta = timeDelta;
                return this;
            }

            public Builder setTimestampPartMsec(int timestampPartMsec) {
                this.timestampPartMsec = timestampPartMsec;
                return this;
            }

            public SessionData.Builder endEvent() {
                return endEvent(1);
            }

            public SessionData.Builder endEvent(int repeatCount) {
                if (repeatCount < 1) {
                    throw new IllegalArgumentException("The repeat count can't be less than 1");
                }

                for (int i = 0; i < repeatCount; i++) {
                    EventData eventData = build();
                    parentBuilder.addEventData(eventData);
                }
                return parentBuilder;
            }

            public EventData build() {
                EventData target = new EventData();
                target.eventId = eventId;
                target.parameters = parameters;
                target.timeDelta = timeDelta;
                target.timestampPartMsec = timestampPartMsec;

                return target;
            }
        }
    }

    /*
     * The session data class.
     */
    public static class SessionData {
        private List<EventData> eventDataList;
        private int eventsNumber;
        private int firstChannelNumber;
        private int firstEventNumber;
        private int sessionStartTimestamp;

        /*
         * The private constructor.
         */
        private SessionData() {
            // do nothing here
        }

        public List<EventData> getEventDataList() {
            return eventDataList;
        }

        public int getEventsNumber() {
            return eventsNumber;
        }

        public int getFirstChannelNumber() {
            return firstChannelNumber;
        }

        public int getFirstEventNumber() {
            return firstEventNumber;
        }

        public int getSessionStartTimestamp() {
            return sessionStartTimestamp;
        }

        /**
         * The nested builder class.
         */
        public static class Builder {
            private List<EventData> eventDataList;
            private int eventsNumber;
            private int firstChannelNumber;
            private int firstEventNumber;
            private int sessionStartTimestamp;

            private CsvFileAssertionInput.Builder parentBuilder;

            /*
             * The private constructor.
             */
            private Builder(CsvFileAssertionInput.Builder parentBuilder) {
                this.parentBuilder = parentBuilder;
            }

            public Builder addEventData(EventData eventData) {
                if (eventDataList == null) {
                    eventDataList = new ArrayList<>();
                }
                eventDataList.add(eventData);
                return this;
            }

            public Builder setEventsNumber(int eventsNumber) {
                this.eventsNumber = eventsNumber;
                return this;
            }

            public Builder setFirstChannelNumber(int firstChannelNumber) {
                this.firstChannelNumber = firstChannelNumber;
                return this;
            }

            public Builder setFirstEventNumber(int firstEventNumber) {
                this.firstEventNumber = firstEventNumber;
                return this;
            }

            public Builder setSessionStartTimestamp(int sessionStartTimestamp) {
                this.sessionStartTimestamp = sessionStartTimestamp;
                return this;
            }

            public EventData.Builder startEvent() {
                return new EventData.Builder(this);
            }

            public CsvFileAssertionInput.Builder endSession() {
                return endSession(1);
            }

            public CsvFileAssertionInput.Builder endSession(int repeatCount) {
                if (repeatCount < 1) {
                    throw new IllegalArgumentException("The repeat count can't be less than 1");
                }

                for (int i = 0; i < repeatCount; i++) {
                    SessionData sessionData = build();
                    parentBuilder.addSessionData(sessionData);
                }
                return parentBuilder;
            }

            public SessionData build() {
                SessionData target = new SessionData();
                target.eventDataList = eventDataList;
                target.eventsNumber = eventsNumber;
                target.firstChannelNumber = firstChannelNumber;
                target.firstEventNumber = firstEventNumber;
                target.sessionStartTimestamp = sessionStartTimestamp;

                return target;
            }
        }
    }

    /*
     * The private constructor.
     */
    private CsvFileAssertionInput() {
        // do nothing here
    }

    public List<Long> getMacs() {
        return macs;
    }

    public Long getMac() {
        return mac;
    }

    public int getMessageType() {
        return messageType;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getSender() {
        return sender;
    }

    public String getAddresse() {
        return addresse;
    }

    public int getHubId() {
        return hubId;
    }

    public int getMaxRebootsPerDay() {
        return maxRebootsPerDay;
    }

    public int getNodeId() {
        return nodeId;
    }

    public List<SessionData> getSessionDataList() {
        return sessionDataList;
    }

    public int getSessionsNumber() {
        return sessionsNumber;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public int getStringsNumber() {
        return stringsNumber;
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private List<Long> macs;
        private Long mac;

        private int messageType;

        private String messageId;
        private String sender;
        private String addresse;

        private int hubId;
        private int maxRebootsPerDay;
        private int nodeId;
        private List<SessionData> sessionDataList;
        private int sessionsNumber;
        private List<String> stringList;
        private int stringsNumber;

        public Builder setMacs(List<Long> macs) {
            this.macs = macs;
            return this;
        }

        public Builder setMac(Long mac) {
            this.mac = mac;
            return this;
        }

        public Builder setMessageType(int messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setMessageId(String messageId) {
            this.messageId = messageId;
            return this;
        }

        public Builder setSender(String sender) {
            this.sender = sender;
            return this;
        }

        public Builder setAddresse(String addresse) {
            this.addresse = addresse;
            return this;
        }

        public Builder setHubId(int hubId) {
            this.hubId = hubId;
            return this;
        }

        public Builder setMaxRebootsPerDay(int maxRebootsPerDay) {
            this.maxRebootsPerDay = maxRebootsPerDay;
            return this;
        }

        public Builder setNodeId(int nodeId) {
            this.nodeId = nodeId;
            return this;
        }

        public Builder addSessionData(SessionData sessionData) {
            if (sessionDataList == null) {
                sessionDataList = new ArrayList<>();
            }
            sessionDataList.add(sessionData);
            return this;
        }

        public Builder setSessionsNumber(int sessionsNumber) {
            this.sessionsNumber = sessionsNumber;
            return this;
        }

        public Builder addString(String string) {
            if (stringList == null) {
                stringList = new ArrayList<>();
            }
            stringList.add(string);
            return this;
        }

        public Builder setStringsNumber(int stringsNumber) {
            this.stringsNumber = stringsNumber;
            return this;
        }

        public SessionData.Builder startSession() {
            return new SessionData.Builder(this);
        }

        public CsvFileAssertionInput build() {
            CsvFileAssertionInput target = new CsvFileAssertionInput();
            target.macs = macs;
            target.mac = mac;

            target.messageType = messageType;

            target.messageId = messageId;
            target.sender = sender;
            target.addresse = addresse;

            target.hubId = hubId;
            target.maxRebootsPerDay = maxRebootsPerDay;
            target.nodeId = nodeId;
            target.sessionDataList = sessionDataList;
            target.sessionsNumber = sessionsNumber;
            target.stringList = stringList;
            target.stringsNumber = stringsNumber;

            return target;
        }
    }
}
