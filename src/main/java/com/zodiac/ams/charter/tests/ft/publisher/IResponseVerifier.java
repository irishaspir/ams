package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.common.http.FTHttpUtils;

public interface IResponseVerifier {
    boolean verify(FTHttpUtils.HttpStringResult response);    
}
