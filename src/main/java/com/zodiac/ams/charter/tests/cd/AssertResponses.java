package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.rudp.cd.listener.CDListener;
import com.zodiac.ams.charter.rudp.cd.message.CDPatternBuilder;
import com.zodiac.ams.common.logging.Logger;

import java.util.List;

import static org.testng.Assert.assertEquals;

/**
 * Created by SpiridonovaIM on 25.07.2017.
 */
public class AssertResponses extends StepsForCD {

    private CDPatternBuilder cdPatternBuilder = new CDPatternBuilder();

    private List getResponse() {
        byte[] data = cdstbEmulator.getData();
        List<String> list = new CDListener(data).parseMessage();
        list.forEach(value -> Logger.info("RESPONSE:" + value));
        return list;
    }

    public void assertTuneSTB(int requestId, int chanelNum, String callSign) {
        assertEquals(getResponse(), cdPatternBuilder.cdRequestPattern(requestId, chanelNum, callSign));
    }

    public void assertTuneSTB(int requestId, int chanelNum) {
        assertTuneSTB(requestId, chanelNum, "");
    }

    public void assertKeyPress(int requestId, int key) {
        assertEquals(getResponse(), cdPatternBuilder.cdRequestPattern(requestId, key));
    }

    public void assertDeeplink(int requestId, String command) {
        assertEquals(getResponse(), cdPatternBuilder.cdRequestPattern(requestId, command));
    }

    public void assertPlayback(int requestId, int recordingId, int offset) {
        assertEquals(getResponse(), cdPatternBuilder.cdRequestPattern(requestId, recordingId, offset));
    }

    public void assertPlayback(int requestId, int recordingId) {
        assertEquals(getResponse(), cdPatternBuilder.cdRequestPattern(requestId, recordingId));
    }

    public void assertTrickMode(int requestId, int mode, int speed, int interval) {
        assertEquals(getResponse(), cdPatternBuilder.cdRequestPattern(requestId, mode, speed, interval));
    }



}
