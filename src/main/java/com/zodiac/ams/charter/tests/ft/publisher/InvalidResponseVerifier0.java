package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.common.http.FTHttpUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class InvalidResponseVerifier0 implements IResponseVerifier {
    
    protected final static String ERROR_TEXT = "Publisher servlet error";

    @Override
    public boolean verify(FTHttpUtils.HttpStringResult response) {
        try {
            JSONObject obj = new JSONObject(response.getEntity());
            String str = obj.getString("errorText");
            if (!ERROR_TEXT.equalsIgnoreCase(str.trim()))
                Assert.assertTrue(false,"Expected error text: \""+ERROR_TEXT+"\" Found: \""+str+"\""); 
        }
        catch(Exception ex){
             Assert.assertTrue(false,"Exception: "+ex.getMessage());
        }
        return true;
    }
    
}
