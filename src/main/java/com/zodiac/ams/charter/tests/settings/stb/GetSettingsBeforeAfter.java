package com.zodiac.ams.charter.tests.settings.stb;


import com.zodiac.ams.charter.emuls.che.settings.SettingsSampleBuilder;
import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class GetSettingsBeforeAfter extends StepsForSettings {

    @BeforeMethod
    public void checkDataInBD() {
        new SettingsSampleBuilder(DEVICE_ID_NUMBER_3, "./dataCHE", "/sample1.xml").messageBuild();
        setDefaultValue(options, MAC_NUMBER_1);
        runSTB();
        runCHESettings();
        startTestTime = getCurrentTimeTest();

    }

    @AfterMethod
    public void stop() {
        stopSTB();
        stopCHESettings();
    }
}
