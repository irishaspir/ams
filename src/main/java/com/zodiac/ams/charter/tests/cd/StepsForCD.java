package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.emuls.stb.cd.CDSTBEmulator;
import com.zodiac.ams.charter.rudp.cd.message.CDRudpHelper;
import com.zodiac.ams.charter.tests.Steps;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by SpiridonovaIM on 19.07.2017.
 */
public class StepsForCD extends Steps {

    protected CDSTBEmulator cdstbEmulator;
    protected CDRudpHelper cdRudpHelper;

    @Step
    protected void runSTB() {
        cdstbEmulator = new CDSTBEmulator();
        cdstbEmulator.registerConsumer();
        cdstbEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        cdRudpHelper = new CDRudpHelper(cdstbEmulator);
    }

    @Step
    protected void runSTB(int id, int code, int params) {
        cdstbEmulator = new CDSTBEmulator(id, code, params);
        cdstbEmulator.registerConsumer();
        cdstbEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        cdRudpHelper = new CDRudpHelper(cdstbEmulator);
    }

    @Step
    protected void stopSTB() {
        cdstbEmulator.stop();
        Logger.info("STB Emulator's stopped");
    }


}
