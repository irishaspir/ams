package com.zodiac.ams.charter.tests;

import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.tomcat.command.TomcatHelper.getStartTomcatTime;


public class StartAmsTest extends Steps {

    @Features(FeatureList.START_TOMCAT)
    @Stories(StoriesList.ENVIRONMENT_PREPARATION)
    @Description("Start tomcat ")
    @Test()
    public void start() {
        createAttachment(parserLog.readLog(PATH_TO_LOG, getStartTomcatTime(), READ_LOG_TIMEOUT));
        createAttachment(readFromFile("etc/server.xml"));
    }

    private List<String> readFromFile(String path) {
        List<String> dataFromFile = new ArrayList<>();
        Logger.info("Read " + path);
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                dataFromFile.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataFromFile;
    }

}
