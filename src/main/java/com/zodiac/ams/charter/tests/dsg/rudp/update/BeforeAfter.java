package com.zodiac.ams.charter.tests.dsg.rudp.update;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dsg.message.DSGSTBResponseBuilder;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.connectionMode;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public class BeforeAfter extends STBParametersUpdateSteps {

    protected DSGSTBResponseBuilder dsgstbResponseBuilder = new DSGSTBResponseBuilder();
    protected ZodiacMessage message;

    @BeforeClass
    public void setDefaultSettings() {
        setDefaultValue(options, MAC_NUMBER_1);
        setDefaultValue(options, MAC_NUMBER_2);
        setDefaultValue(options, MAC_NUMBER_3);
    }

    @BeforeClass
    public void start() {
        runSTB();
        runSettingsCHE();
    }

    @BeforeMethod
    public void getTime() {
        startTestTime = getCurrentTimeTest();
        connectionMode = changeConnectionMode();
    }

    @AfterClass
    public void stop() {
        stopSTB();
        stopSettingsCHE();

    }
}
