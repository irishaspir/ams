package com.zodiac.ams.charter.tests.rollback.rollbackToRoviFirmware;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.rollback.RollbackPostHelper.*;
import static com.zodiac.ams.charter.http.helpers.rollback.RollbackStringDataUtils.*;
import static com.zodiac.ams.charter.rudp.rollback.message.RollbackSTBRequestPattern.nullRequest;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SendRollbackToRoviCommandValidationJSONTest extends StepsForRollbackSTBOn{

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.9 Send \"Rollback To Rovi Firmware\" command without body ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithoutBody() {
        assertEquals(sendRollbackToRoviFirmwareWithoutBody(), ERROR_500);
        createAttachment(readAMSLog());
        createAttachment(responseString);
        assertEquals(error500WithoutBody(), responseString.trim(), ROLLBACK_RESPONSE_IS_INCORRECT);
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.10 Send \"Rollback To Rovi Firmware\" command with mistake in 'devices' parameter")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithMistakeInDevicesParameter() {
        assertEquals(sendRollbackToRoviFirmwareWithMistakeInDevicesParameter(DEVICE_ID_NUMBER_1), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getResponseIfRequestBodyEmptyJson());
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
        assertTrue(rollbackLogParser.compareLogAMSWithPatternEmptyJsonBody(rollbackLogParser.getLogAMSForValid(log)),
                PATTERN_ROLLBACK_LOG);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.11 Send \"Rollback To Rovi Firmware\" command with empty JSON in body ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithEmptyJsonInBody() {
        assertEquals(sendRollbackToRoviFirmwareWithEmptyJson(), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertJsonEquals(responseJson, rollbackJsonDataUtils.getResponseIfRequestBodyEmptyJson());
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
        assertTrue(rollbackLogParser.compareLogAMSWithPatternEmptyJsonBody(rollbackLogParser.getLogAMSForValid(log)),
                PATTERN_ROLLBACK_LOG);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.12 Send \"Rollback To Rovi Firmware\" command with string in body ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithStringInBody() {
        assertEquals(sendRollbackToRoviFirmwareStringInsteadOfJsonInBody(), ERROR_500);
        createAttachment(readAMSLog());
        createAttachment(responseString);
        assertEquals(error500WithStringInBody(), responseString.trim(), ROLLBACK_RESPONSE_IS_INCORRECT);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.13 Send \"Rollback To Rovi Firmware\" command without JSON Array symbols in body ")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithoutJsonArraySymbolsInBody() {
        assertEquals(sendRollbackToRoviFirmwareWithoutJsonArrayBrackets(DEVICE_ID_NUMBER_1), ERROR_500);
        createAttachment(readAMSLog());
        createAttachment(responseString);
        assertEquals(error500WithoutJsonArrayBracketsInBody(), responseString.trim(), ROLLBACK_RESPONSE_IS_INCORRECT);
    }

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.NEGATIVE + StoriesList.ROLLBACK_TO_ROVI_FIRMWARE)
    @Description("11.1.14 Send \"Rollback To Rovi Firmware\" command without device_id's list")
    @TestCaseId("12345")
    @Test()
    public void sendRollbackToRoviFirmwareCommandWithoutDeviceIds() {
        assertEquals(sendRollbackToRoviFirmwareWithoutDeviceIds(), SUCCESS_200);
        createAttachment(readAMSLog());
        createAttachment(responseJson);
        assertEquals(rollbackRudpHelper.getRollbackStbRequest(), nullRequest(), ROLLBACK_REQUEST_IS_INCORRECT);
    }

}
