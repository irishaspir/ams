package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.common.logging.Logger;
import org.json.JSONObject;
import ru.yandex.qatools.allure.annotations.Step;

import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.getModelId;
import static com.zodiac.ams.charter.http.helpers.dsg.DsgHttpDataUtils.dsgPostPattern;
import static com.zodiac.ams.charter.runner.TestRunner.AMS_VERSION;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.boxModel;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

public class JsonSteps extends LogSteps {


    private JSONObject cheJson;

    @Step
    public Object getBoxModel() {
        return getBoxModel(boxModel);
    }

    public int getBoxModel(int boxModel) {
        return (AMS_VERSION < 390) ? getModelId(UNKNOWN) : boxModel;
    }

    @Step
    public JSONObject getJson(JSONObject object) {
        if (AMS_VERSION >= 390)
           // return getJsonObjectWithoutNullValue();
            return object;
        else return object;
    }

    @Step
    public void getCheJson() {
        do {
            cheJson = dsgCHEEmulator.checkData();
        }
        while (cheJson.toString().contains("Caller ID Notification"));
        createAttachment(cheJson);
    }

    @Step
    public void assertJson(String mac, int connectionMode) {
        assertJson(getJson(dsgPostPattern(getDeviceIdByMac(mac), connectionMode)));
    }

    @Step
    public void assertJson(String mac, int connectionMode, Object boxModel) {
        JSONObject object1 = dsgPostPattern(getDeviceIdByMac(mac), connectionMode, boxModel);
        JSONObject object = getJson(object1);
        assertJson(object);
    }

    @Step
    public void assertJson(String mac, int connectionMode, Object boxModel, Object vendor) {
        JSONObject object1 = dsgPostPattern(getDeviceIdByMac(mac), connectionMode, boxModel,(vendor instanceof String)?999:(int) vendor);
        JSONObject object = getJson(object1);
        assertJson(object);
    }

    @Step
    public void assertJson(String mac, int connectionMode, int boxModel,int vendor, int hubId) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac), connectionMode, boxModel,vendor, hubId));
        assertJson(object);
    }

//    @Step
//    public void assertJson(String mac, int boxModel, int hubId, int serviceGroupId) {
//        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac), boxModel, hubId, serviceGroupId));
//        Logger.info("JSON pattern" + object.toString());
//        createAttachment(object);
//        assertJsonEquals(object, cheJson);
//    }

    @Step
    public void assertJson(String mac,int connectionMode, int boxModel,int vendor, int hubId, int serviceGroupId, Object loadBalancer) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac), connectionMode, boxModel, vendor,hubId, serviceGroupId, loadBalancer));
        assertJson(object);
    }

    @Step
    public void assertJson(String mac, int connectionMode,int boxModel,int vendor, int hubId, int serviceGroupId, Object loadBalancer, Object swVersion) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac),connectionMode, boxModel,vendor, hubId, serviceGroupId, loadBalancer, swVersion));
        assertJson(object);
    }

    @Step
    public void assertJson(String mac, int connectionMode,int boxModel,int vendor, int hubId, int serviceGroupId, Object loadBalancer, Object swVersion, Object sdvServiceGroupId) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac), connectionMode,boxModel, vendor,hubId, serviceGroupId, loadBalancer, swVersion, sdvServiceGroupId));
        assertJson(object);
    }

    @Step
    public void assertJson(String mac, int connectionMode,int boxModel,int vendor, int hubId, int serviceGroupId, Object loadBalancer, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac),connectionMode, boxModel,vendor, hubId, serviceGroupId, loadBalancer, swVersion, sdvServiceGroupId, cmMacAddress));
        assertJson(object);
    }

    @Step
    public void assertJson(String mac, int connectionMode,int boxModel,int vendor, int hubId, int serviceGroupId, Object loadBalancer, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, Object serialNumber) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac),connectionMode, boxModel,vendor, hubId, serviceGroupId, loadBalancer, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber));
        assertJson(object);
    }

    @Step
    public void assertJson(String mac,int connectionMode, Object boxModel,int vendor, int hubId, int serviceGroupId, Object loadBalancer, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, Object serialNumber, Object cmIp) {
        JSONObject object = getJson(dsgPostPattern(getDeviceIdByMac(mac),connectionMode, boxModel,vendor, hubId, serviceGroupId, loadBalancer, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, cmIp));
        assertJson(object);
    }

    private void assertJson(JSONObject object) {
        Logger.info("JSON pattern: " + object.toString());
        createAttachment(object);
        getCheJson();
        assertJsonEquals(object, cheJson);
    }
}
