package com.zodiac.ams.charter.tests.cd;

import com.zodiac.ams.charter.rudp.cd.message.CDRequestBuider;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.CompanionDevice.*;
import static com.zodiac.ams.charter.tests.StoriesList.DVR_RECORDING_TRICKMODE;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

/**
 * Created by SpiridonovaIM on 31.07.2017.
 */
public class DVRRecordingTrickmodeReceiveErrorTests extends AssertLog {

    protected CDRequestBuider cdRequestBuider = new CDRequestBuider();
    protected List<CDProtocolOption> message;


    @BeforeMethod
    public void checkDataInBD() {
        startTestTime = getCurrentTimeTest();
    }


    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_TRICKMODE)
    @Description("10.7.1  Message from device to AMS where <request id>=7, mode and speed and interval are specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "modeAndSpeedAndInterval", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendTrickModeReceiveError(int mode, int speed, int interval) {
        runSTB(DVR_TRICKMODE_REQUEST_ID, ERROR_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createTrickModeMessage(mode, speed, interval);
        cdRudpHelper.sendRequest(deviceId, message, DVR_TRICKMODE_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertTrickMode(DVR_TRICKMODE_REQUEST_ID, mode, speed, interval);
        assertErrorLog(log, deviceId, DVR_TRICKMODE_REQUEST_ID, ERROR_CD_ID, PARAMS);
    }


    @Features(FeatureList.COMPANION_DEVICES)
    @Stories(StoriesList.POSITIVE + DVR_RECORDING_TRICKMODE)
    @Description("10.7.2  Message from device to AMS where <request id>=7, mode and speed and interval are specified ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test(dataProvider = "modeAndSpeedAndInterval", dataProviderClass = CDDataProvider.class, priority = 2)
    public void sendTrickModeReceiveIncorrectParams(int mode, int speed, int interval) {
        runSTB(DVR_TRICKMODE_REQUEST_ID, INCORRECT_PARAMETER_CD_ID, PARAMS);
        String deviceId = DEVICE_ID_NUMBER_1;
        message = cdRequestBuider.createTrickModeMessage(mode, speed, interval);
        cdRudpHelper.sendRequest(deviceId, message, DVR_TRICKMODE_REQUEST_ID);
        log = readAMSLog();
        createAttachment(log);
        assertTrickMode(DVR_TRICKMODE_REQUEST_ID, mode, speed, interval);
        assertErrorLog(log, deviceId, DVR_TRICKMODE_REQUEST_ID, INCORRECT_PARAMETER_CD_ID, PARAMS);
    }

    @AfterMethod
    public void stop() {
        stopSTB();
    }


}
