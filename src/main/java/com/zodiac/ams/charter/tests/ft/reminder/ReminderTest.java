package com.zodiac.ams.charter.tests.ft.reminder;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.transport.iface.IDataConsumer;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.reminder.ReminderHelper;
import com.zodiac.ams.charter.services.ft.reminder.ReminderHelper.Operations;
import com.zodiac.ams.charter.services.ft.reminder.ReminderResponseHelper;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.http.FTHttpUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.concurrent.*;

import static com.zodiac.ams.charter.tests.FeatureList.REMINDER;

public class ReminderTest extends FuncTest {
    
    private final static int MAX_LIMIT = 32;
    private final static String INVALID_ID = "XXXX";
    private final static int BAD_REQUEST_RESPONSE =  400;
    private final static int STB_IS_NOT_AVAILABLE = 500;
    private final static int BOX_NOT_REGISTERED = 400;
    private final static int MAX_NUMBER_EXCEEDED = 409; 
    private final static String MAX_NUMBER_EXCEEDED_MSG = "REM-009 maximum number of requests to the service exceeded";
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.addReminderService();
        helper.save("server.xml");
    }
    
    @Test(priority=1)
    @Features(REMINDER)
    @Description("Add one reminder. Correct case.")
    public void addOneReminder(){    
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId());
        expected.addCode(0);
        
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15),expected,200); 
    }
    
    
    @Test(priority=2)
    @Features(REMINDER)
    @Description("Add some reminders. Correct case.")
    public void addSomeReminders() {
        
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId());
        expected.addCode(0);
        expected.addCode(0);   
        
        reminderTestImpl(properties.getDeviceId(),new ReminderHelper.Reminder[]{
                new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15),
                new ReminderHelper.Reminder(Operations.ADD, 28, 1,"EP002960010115", 16)
            },
            expected,200);
    }
    
    @Test(priority=3)
    @Features(REMINDER)
    @Description("Add reminder. Past time.")
    public void addReminderPastTime() {
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),2);
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, -1,"EP002960010113", 15),expected,200);
    }
    
    
    @Test(priority=4)
    @Features(REMINDER)
    @Description("Add reminder. Unknown channel.")
    public void addReminderUnknownChannel() {
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),3);
        reminderTestImpl(createCharterSTBEmulator(3),properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15),expected,200);
    }
    
    @Test(priority=5)
    @Features(REMINDER)
    @Description("Add reminder. Maximum NVM updates reached.")
    public void addReminderMaxNVMReached(){
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),5);
        reminderTestImpl(createCharterSTBEmulator(5),properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15),expected,200);
    }
    
    @Test(priority=6)
    @Features(REMINDER)
    @Description("Add reminder. Reminders limit exceeded on STB")
    public void addReminderLimitExceeded(){
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),1);
        
        ReminderHelper request=new ReminderHelper(properties.getDeviceId());
        
        int length=MAX_LIMIT+1;
        ReminderHelper.Reminder[][] rmds=new ReminderHelper.Reminder[length][1];
        for (int i=0;i<length;i++)
            rmds[i][0]=new ReminderHelper.Reminder(Operations.ADD,i+27,i,"EP002960010113", i+15);
              
        reminderTestImpl(request,rmds,expected,200);
    }
    
    
    @Test(priority=7)
    @Features(REMINDER)
    @Description("Add reminder. STB  is not registered.")
    public void addRemnderNotRegistered(){
        ReminderResponseHelper expected=new ReminderResponseHelper(INVALID_ID,"Failed");
        reminderTestImpl(INVALID_ID,
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15),expected,BOX_NOT_REGISTERED);
    }
    
    @Test(priority=8)
    @Features(REMINDER)
    @Description("Add reminder. STB is not available.")
    public void addReminderNotAvailable(){
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),"Failed");
        ReminderHelper request=new ReminderHelper(properties.getDeviceId());
        request.addReminder(new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15));
        try {
            FTHttpUtils.HttpResult response=FTHttpUtils.doPost(properties.getRemindersUri(),request.toString());
            getHTTPPostStatus(properties.getRemindersUri(),request,response);
            Assert.assertTrue(STB_IS_NOT_AVAILABLE == response.getStatus(),"Invalid status");     
            //Assert.assertTrue(ReminderResponseHelper.equals(expected.toJSON(),response.getMessage()),"Invalid response");
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            Assert.assertTrue(false,ex.getMessage());
        }            
    }
    
    @Test(priority=9)
    @Features(REMINDER)
    @Description("Add reminder. DeviceID is missing.")
    public void addReminderNoDeviceId(){
        reminderTestImpl(null,
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15),null,BAD_REQUEST_RESPONSE);
    }
    
    @Test(priority=10)
    @Features(REMINDER)
    @Description("Add reminder. Reminders array is empty.")
    public void addReminderNoRemindersInArray(){
        //reminderTestImpl(properties.getDeviceId(),new ReminderHelper.Reminder[] {},null,200);
        reminderTestImpl(properties.getDeviceId(),new ReminderHelper.Reminder[] {},null,BAD_REQUEST_RESPONSE);
    }
    
    @Test(priority=11)
    @Features(REMINDER)
    @Description("Add reminder. NO reminders array in the body.")
    public void addReminderNoRemindersArray(){
        ReminderHelper request=new ReminderHelper(properties.getDeviceId(),false);
        //reminderTestImpl(request,new ReminderHelper.Reminder[][]{null},null,200);
        reminderTestImpl(request,new ReminderHelper.Reminder[][]{null},null,BAD_REQUEST_RESPONSE);
    }
    
    @Test(priority=12)
    @Features(REMINDER)
    @Description("Add reminder. Operation is missing.")
    public void addReminderNoOperation(){
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(null, 27, 1,"EP002960010113", 15),null,BAD_REQUEST_RESPONSE);
    }
    
    @Test(priority=13)
    @Features(REMINDER)
    @Description("Add reminder. ChannelNumber is missing.")
    public void addReminderNoChannelNumber(){
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD,null,1,"EP002960010113", 15),null,BAD_REQUEST_RESPONSE);
    }
    
    @Test(priority=14)
    @Features(REMINDER)
    @Description("Add reminder. ProgramStart is missing.")
    public void addReminderNoProgramStart(){
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, null,"EP002960010113", 15),null,BAD_REQUEST_RESPONSE);
    }
    
    /*
    @Test(priority=15)
    @Features(REMINDER)
    @Description("Add reminder. ProgramId is missing.")
    public void addReminderNoProgramId(){
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,null, 15),null,BAD_REQUEST_RESPONSE);
    }
    */
    
    @Test(priority=16)
    @Features(REMINDER)
    @Description("Add reminder. Offset is missing.")
    public void addReminderNoOffset(){
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", null),null,BAD_REQUEST_RESPONSE);
    }
    
    private void reminderTestImpl(String id,ReminderHelper.Reminder reminder,ReminderResponseHelper expectedResponse,int expectedCode){
        reminderTestImpl(new ReminderHelper(id),new ReminderHelper.Reminder[][]{new ReminderHelper.Reminder[]{reminder}},expectedResponse,expectedCode);
    }
    
    private void reminderTestImpl(CharterStbEmulator emulator,String id,ReminderHelper.Reminder reminder,ReminderResponseHelper expectedResponse,int expectedCode){
        reminderTestImpl(emulator,new ReminderHelper(id),new ReminderHelper.Reminder[][]{new ReminderHelper.Reminder[]{reminder}},expectedResponse,expectedCode);
    }
    
    private void reminderTestImpl(CharterStbEmulator emulator,String id,ReminderHelper.Reminder[] reminders,ReminderResponseHelper expectedResponse,int expectedCode){
        reminderTestImpl(emulator,new ReminderHelper(id),new ReminderHelper.Reminder[][]{reminders},expectedResponse,expectedCode);
    }
    
    private void reminderTestImpl(String id,ReminderHelper.Reminder[] reminders,ReminderResponseHelper expectedResponse,int expectedCode){
        reminderTestImpl(new ReminderHelper(id),new ReminderHelper.Reminder[][]{reminders},expectedResponse,expectedCode);
    }
    
    private void reminderTestImpl(ReminderHelper request,ReminderHelper.Reminder[][] reminders,
        ReminderResponseHelper expectedResponse,int expectedCode){
        reminderTestImpl(null,request,reminders,expectedResponse,expectedCode);
    }
    
    private void reminderTestImpl(CharterStbEmulator emulator,ReminderHelper request,ReminderHelper.Reminder[][] reminders,
        ReminderResponseHelper expectedResponse,int expectedCode){
        if (null == emulator)
            emulator=createCharterSTBEmulator(0);
        Assert.assertTrue(null != emulator,"Unable to create charter STB emulator");
        startCharterSTBEmulator(emulator);
        
        try{
            for (int i=0;i<reminders.length;i++){
                ReminderHelper.Reminder[] rm=reminders[i];
                request.clear();
                if (null != rm)
                    request.addReminders(rm);
                FTHttpUtils.HttpResult response=FTHttpUtils.doPost(properties.getRemindersUri(),request.toString());
                if (i == reminders.length-1){
                    getHTTPPostStatus(properties.getRemindersUri(),request,response);
                    Assert.assertTrue(expectedCode == response.getStatus(),"Invalid status");
                    if (null != expectedResponse)
                        Assert.assertTrue(ReminderResponseHelper.equals(expectedResponse.toJSON(),response.toString()),"Invalid response");
                }
            }
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        finally{
            emulator.stop();
        }      
    }
    
    @Test(priority=17)
    @Features(REMINDER)
    @Description("Delete reminder. Correct case.")
    public void deleteReminder(){
        
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),0);
        
        ReminderHelper request=new ReminderHelper(properties.getDeviceId());
        
        ReminderHelper.Reminder add=new ReminderHelper.Reminder(Operations.ADD, 27, 1,"EP002960010113", 15);
        ReminderHelper.Reminder delete=new ReminderHelper.Reminder(Operations.DELETE, 27, add.getProgramStart(), "EP002960010113", 15);
        
        reminderTestImpl(request,new ReminderHelper.Reminder[][] {
                new ReminderHelper.Reminder[]{add},
                new ReminderHelper.Reminder[]{delete}
            },
            expected,200);
    }
    
    @Test(priority=18)
    @Features(REMINDER)
    @Description("Delete reminder. Reminder doesn't exist.")
    public void deleteReminderNotExists(){
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),4);
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.DELETE, 27, 1, "EP002960010113", 15),expected,200); 
    }
    
    @Test(priority=19)
    @Features(REMINDER)
    @Description("Delete reminder. Maximum NVM updates reached")
    public void deleteReminderMaxNvm(){
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),5);
        reminderTestImpl(createCharterSTBEmulator(5),properties.getDeviceId(),
                new ReminderHelper.Reminder(Operations.DELETE, 27, 1, "EP002960010113", 15),expected,200); 
    }
 
    @Test(priority=20)
    @Features(REMINDER)
    @Description("Purge reminders. Correct case")
    public void purgeReminders(){
        ReminderResponseHelper expected=new ReminderResponseHelper(properties.getDeviceId(),0);
        reminderTestImpl(properties.getDeviceId(),
            new ReminderHelper.Reminder(Operations.PURGE),expected,200); 
    }
    
    @Test(priority=21)
    @Features(REMINDER)
    @Description("Add reminder. The maximum limit of the simultaneously receiving reminders is exceeded.")
    public void addReminderLimitIsExceeded() throws Exception{
        int count = properties.getMaxReminders() + 1;
        FTHttpUtils.HttpResult [] results = new FTHttpUtils.HttpResult[count];
        Semaphore [] semaphores = new Semaphore[count];
        for (int i=0;i<semaphores.length;i++)
            semaphores[i] = new Semaphore(0);
        
        CountDownLatch countDown = new CountDownLatch(count);
        
        ExecutorService pool = Executors.newFixedThreadPool(count);
        for (int i=0;i<count;i++)
            pool.execute(new ReminderExecutor(results,i,countDown,semaphores));
        Assert.assertTrue(countDown.await(1, TimeUnit.MINUTES),"All threads must be finished");
        
        boolean ok = false;
        for (FTHttpUtils.HttpResult result: results){
            getHTTPPostStatus(properties.getRemindersUri(), result); 
            if (MAX_NUMBER_EXCEEDED == result.getStatus())
                ok = true;
        }
        
        Assert.assertTrue(200 == results[0].getStatus(),"Response must be 200");
        Assert.assertTrue(ok,"Response must be "+MAX_NUMBER_EXCEEDED);
    }
    
    private class ReminderExecutor implements Runnable{
        private final FTHttpUtils.HttpResult [] results;
        private final int index;
        private final CountDownLatch countDown; 
        private final Semaphore [] semaphores;
        
        private static final int START_CHANNEL = 20;
        private static final int START_OFFSET = 15;
        
        private ReminderExecutor(FTHttpUtils.HttpResult [] results,int index,CountDownLatch countDown,Semaphore[] semaphores){
            this.results = results;
            this.index = index;
            this.countDown = countDown;
            this.semaphores = semaphores;
        }
        
        @Override
        public void run() {
            CharterStbEmulator emulator =  null;
            try{
                if (0 == index){
                    emulator = createCharterSTBEmulator(0);
                    startCharterSTBEmulator(emulator);
                    emulator.registerConsumer(new IDataConsumer(){
                        @Override
                        public void dataReceived(byte[] data) {
                            for (Semaphore semaphore: semaphores)
                                semaphore.release();
                            try{
                                Thread.sleep(5_000);
                            }
                            catch(Exception ex){
                            }
                        }}, HandlerType.Reminder);
                }
                
                ReminderHelper request;
                if (0 == index )
                    request = new ReminderHelper(properties.getDeviceId());
                else {
                    //here there is exception on AMS, so send real device Id
                    //request = new ReminderHelper("UNKNOWN_"+index);
                    request = new ReminderHelper(properties.getDeviceId());
                }
                ReminderHelper.Reminder reminder = 
                    new ReminderHelper.Reminder(Operations.ADD, START_CHANNEL+index, 1, "PROGRAM_"+index , START_OFFSET+index);
                request.addReminder(reminder);
                
                if (0 != index)
                    semaphores[index].tryAcquire(1, TimeUnit.MINUTES);
                
                results[index] =  FTHttpUtils.doPost(properties.getRemindersUri(),request.toString());  
            }
            catch(Exception ex){
            }
            finally{
                try{
                    if (null != emulator)
                        emulator.stop();
                }
                finally {
                    countDown.countDown();
                }
            }
        } 
    }
    
    @Features(REMINDER)
    @Stories("AMS logs")
    @Description("ReminderTest AMS log")
    @Test(priority=100)
    public void getReminderTestAMSLog() throws Exception {
        stopAndgetAMSLog(true);
    }
}
