package com.zodiac.ams.charter.tests.cd;

import org.testng.annotations.DataProvider;

import static com.zodiac.ams.charter.services.cd.CDProtocolOptions.KEY;
import static com.zodiac.ams.charter.services.cd.CDProtocolOptions.MODE;
import static com.zodiac.ams.charter.services.cd.CDProtocolOptions.SPEED;

/**
 * Created by SpiridonovaIM on 24.07.2017.
 */
public class CDDataProvider {

    @DataProvider(name = "pressKey")
    public static Object[][] getKey() {
        String[] keyRange = KEY.getOption().getRange();
        Object[][] objects = new Object[Integer.valueOf(keyRange[1])][1];
        for (int i = Integer.valueOf(keyRange[0]); i <= Integer.valueOf(keyRange[1]); i++) {
            objects[i - 1][0] = i;
        }
        return objects;
    }


    @DataProvider(name = "chanelNum")
    public static Object[][] getChanelNum() {
        return new Object[][]{
                {0},
                {13},
                {700},
                {710},
                {800},
        };
    }


    @DataProvider(name = "chanelNumCallSign")
    public static Object[][] getChanelNumAndCallSign() {
        return new Object[][]{
                {0, "FOX"},
                {13, ""},
                {700, "$&#&^$#%$#"},
                {710, "1256342"},
                {800, " "},
        };
    }

    @DataProvider(name = "deeplinkNegative")
    public static Object[][] getDeeplinkNegative() {
        return new Object[][]{
                {"jhglig"},
                {"%$^&$"},
                {"3286536524"},
                {"jh;kdsjhu8"},
                {"/s/avn/"},
                {""},
        };
    }

    @DataProvider(name = "deeplinkPositive")
    public static Object[][] getDeeplinkPositive() {
        return new Object[][]{
                {"/s/companion/hereiam"},
                {"/s/companion/linked"},
        };
    }


    @DataProvider(name = "recordingId")
    public static Object[][] getRecordingId() {
        return new Object[][]{
                {13},
                {0},
                {710},
                {800},
        };
    }

    @DataProvider(name = "recordingIdAndPlaybackOffset")
    public static Object[][] getPlaybackParams() {
        return new Object[][]{
                {13, 13},
                {0, 0},
                {710, 18},
                {800, 800},
        };
    }

    @DataProvider(name = "mode")
    public static Object[][] getMode() {
        String[] modeRange = MODE.getOption().getRange();
        Object[][] objects = new Object[Integer.valueOf(modeRange[1]) + 1][1];
        for (int i = Integer.valueOf(modeRange[0]); i <= Integer.valueOf(modeRange[1]); i++) {
            objects[i][0] = i;
        }
        return objects;
    }

    @DataProvider(name = "modeAndSpeed")
    public static Object[][] getModeAndSpeed() {
        String[] modeRange = MODE.getOption().getRange();
        String[] speedRange = SPEED.getOption().getRange();
        int size = (Integer.valueOf(modeRange[1]) + 1) * (Integer.valueOf(speedRange[1]) + 1);
        Object[][] objects = new Object[size][2];
        int i = 0;
        while (i < size) {
            for (int x = Integer.valueOf(modeRange[0]); x <= Integer.valueOf(modeRange[1]); x++) {
                for (int j = Integer.valueOf(speedRange[0]); j <= Integer.valueOf(speedRange[1]); j++) {
                    objects[i][0] = x;
                    objects[i][1] = j;
                    i++;
                }
            }
        }
        return objects;
    }

    @DataProvider(name = "modeAndSpeedAndInterval")
    public static Object[][] getModeAndSpeedAndInterval() {
        String[] modeRange = MODE.getOption().getRange();
        String[] speedRange = SPEED.getOption().getRange();
        int size = (Integer.valueOf(modeRange[1]) + 1) * (Integer.valueOf(speedRange[1]) + 1);
        Object[][] objects = new Object[size][3];
        int i = 0;
        while (i < size) {
            for (int x = Integer.valueOf(modeRange[0]); x <= Integer.valueOf(modeRange[1]); x++) {
                for (int j = Integer.valueOf(speedRange[0]); j <= Integer.valueOf(speedRange[1]); j++) {
                    objects[i][0] = x;
                    objects[i][1] = j;
                    objects[i][2] = j;
                    i++;
                }
            }
        }
        return objects;
    }


}
