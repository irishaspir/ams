package com.zodiac.ams.charter.tests.ft.defent;

import com.zodiac.ams.charter.tests.ft.DataConsumer;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.util.DOB7bitCoder;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.LighweightCHEemulator;
import com.dob.test.charter.handlers.DENTNewHandler;
import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper;
import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.defent.dsg.ConnectionMode;
import com.zodiac.ams.charter.services.ft.defent.dsg.DSGMessage32;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.bd.DBHelper;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.List;
import java.util.concurrent.Semaphore;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import static com.zodiac.ams.charter.services.ft.defent.dsg.Constants.*;
import com.zodiac.ams.charter.services.ft.defent.dsg.DSGMessage30;
import com.zodiac.ams.charter.services.ft.defent.dsg.IDSGMessage;
import com.zodiac.ams.charter.services.ft.defent.dsg.Vendor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

public class DefEntDSGTest extends FuncTest{
    private static final String DSG_SERVICE_ID = "dsg";
    private static final byte[] GARBAGE = new byte[] {1, 2, 3, 4, 5, 0};
    
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm X");     
    
    
    private String deviceId;
    private  JSONObject expectedSettings;
    
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        String uriDent = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_DENT_NEW_CONTEXT;
        //use err reporting as settings context
        String uriSettings = "http://"+properties.getLocalHost()+":"+properties.getEmulatorPort()+CHEmulatorHelper.DEFAULT_ERROR_REPORTING_CONTEXT;
        helper.addDSGService(uriDent, uriSettings);
        helper.save("server.xml");
    }
    
    @Override
    protected void init() {
        super.init();
        deviceId = properties.getDeviceId();
        expectedSettings = getExpectedSettings();
          
        DBHelper.disconnect();
        DBHelper.getConnection(properties.getAMSDbConn(),properties.getAMSDbUser(),properties.getAMSDbPwd());
    }
    
    private void prepareSTBMetadata(int vendorId){
        prepareSTBMetadata(vendorId, true);
    }
    
    private void prepareSTBMetadata(int vendorId, boolean insert){
        prepareSTBMetadata(deviceId, vendorId, insert);
    }
    
    public static void prepareSTBMetadata(String deviceId, int vendorId, boolean insert){
        DBHelper.update("delete from STB_METADATA where MAC=?", Long.parseLong(deviceId, 16));
        if (insert)
            DBHelper.update("insert into STB_METADATA (MAC, MAC_STR, REGISTR_TIME_STAMP, "+
                //453
                "LAST_ACT_TIME_STAMP, MODE_CHANGE_TIME_STAMP, BUILD_DATE, "+
                "VENDOR_ID) VALUES(?,?,?,?,?,?,?)", 
                Long.parseLong(deviceId, 16), deviceId, System.currentTimeMillis()/1000,
                //453
                System.currentTimeMillis()/1000, System.currentTimeMillis()/1000, System.currentTimeMillis()/1000,
                vendorId);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.1 [Default Entitlements] Request for updating of cmIp. cmIp is not null") 
    @Test(priority=2)
    public void test_12_5_1() throws Exception {
        Assert.assertTrue(waitForAMS(AMS_TIMEOUT),"AMS is not active");
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.10 [Default Entitlements] Request for updating of sdvServiceGroupId. sdvServiceGroupId is null") 
    @Test(priority=2)
    public void test_12_5_10() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setSdvServiceGroupId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.11 [Default Entitlements] Request for updating of SGId/TSId . SGId/TSId is null") 
    @Test(priority=2)
    public void test_12_5_11() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setServiceGroupId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.12 [Default Entitlements] Request for updating of SGId/TSId. SGId/TSId is not null") 
    @Test(priority=2)
    public void test_12_5_12() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.14 [Default Entitlements] Request for updating. Send all parameters as not NULL") 
    @Test(priority=2)
    public void test_12_5_14() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.15 [Default Entitlements] Request for updating. Send all parameters as NULL") 
    @Test(priority=2)
    public void test_12_5_15() throws Exception {
        DSGMessage32 msg = DSGMessage32.getNULL(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.2 [Default Entitlements] Request for updating of cmIp. cmIp is null") 
    @Test(priority=2)
    public void test_12_5_2() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setCmIp(null);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.3 [Default Entitlements] Request for updating of connectionMode. connectionMode is not null") 
    @Test(priority=2)
    public void test_12_5_3() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }

    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.4 [Default Entitlements] Request for updating of connectionMode. connectionMode is null") 
    @Test(priority=2)
    public void test_12_5_4() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setConnectionMode(null);
        testDSGImpl(msg);
    }
    
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.5 [Default Entitlements] Request for updating of hubId/VctId . hubId/VctId is not null") 
    @Test(priority=2)
    public void test_12_5_5() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.6 [Default Entitlements] Request for updating of hubId/VctId . hubId/VctId is null") 
    @Test(priority=2)
    public void test_12_5_6() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setHubId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.7 [Default Entitlements] Request for updating of loadBalancerIpAddress. loadBalancerIpAddress is not null") 
    @Test(priority=2)
    public void test_12_5_7() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.8 [Default Entitlements] Request for updating of loadBalancerIpAddress. loadBalancerIpAddress is null") 
    @Test(priority=2)
    public void test_12_5_8() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setLoadBalancerIpAddress(null);
        testDSGImpl(msg);
    }
    
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.9 [Default Entitlements] Request for updating of sdvServiceGroupId. sdvServiceGroupId is not null") 
    @Test(priority=2)
    public void test_12_5_9() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.57 [Default Entitlements] Verification response on request with requestID=2") 
    @Test(priority=2)
    public void test_12_5_57() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG.Negative") 
    @Description("12.5.58 [Default Entitlements] STB sends not all parametes for DSG3 protocol and request requestID=2. Missing declared in 'content' parameter") 
    @Test(priority=2)
    public void test_12_5_58() throws Exception {
        DSGMessage32 msg = DSGMessage32.getRandom(deviceId);
        msg.setDisabledKey(AMSIP);
        DefEntDSGTest.this.testDSGImpl(msg, 1 /*expectedError*/);
    }
    
    
    
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.16 [Default Entitlements] Sending DSG requestID=0 request to Settings Middle") 
    @Test(priority=3)        
    public void test_12_5_16() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    } 
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.17 [Default Entitlements] Sending DSG requestID=0 request to Settings Middle. Some parameters are NULL") 
    @Test(priority=3)        
    public void test_12_5_17() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setHubId(-1);
        msg.setServiceGroupId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.22 [Default Entitlements] Support of AMS IP parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_22() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.23 [Default Entitlements] Support of AMS IP parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_23() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setLoadBalancerIpAddress(null);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.24 [Default Entitlements] Support of CM IP parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_24() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.25 [Default Entitlements] Support of CM IP parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_25() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setCmIp(null);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.26 [Default Entitlements] Support of CM MAC Address parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_26() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.27 [Default Entitlements] Support of CM MAC Address parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_27() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setCmMacAddress(0);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.31 [Default Entitlements] Support of Host MAC Address parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_31() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setCmMacAddress(0);
        testDSGImpl(msg);
    }

    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.33 [Default Entitlements] Support of Host RF IP parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_33() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.34 [Default Entitlements] Support of Host RF IP parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_34() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.35 [Default Entitlements] Support of Hub ID parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_35() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.36 [Default Entitlements] Support of Hub ID parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_36() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setHubId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.37 [Default Entitlements] Support of Model parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_37() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.39 [Default Entitlements] Support of new DSG 3 protocol. Cisco vendor") 
    @Test(priority=3)        
    public void test_12_5_39() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVendor(Vendor.Cisco);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.40 [Default Entitlements] Support of new DSG 3 protocol. General case") 
    @Test(priority=3)        
    public void test_12_5_40() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.41 [Default Entitlements] Support of new DSG 3 protocol. Humax vendor") 
    @Test(priority=3)        
    public void test_12_5_41() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVendor(Vendor.Humax);
        testDSGImpl(msg);
    }

    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.42 [Default Entitlements] Support of new DSG 3 protocol. Motorola vendor") 
    @Test(priority=3)        
    public void test_12_5_42() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVendor(Vendor.Motorola);
        testDSGImpl(msg);
    }

    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.43 [Default Entitlements] Support of new DSG 3 protocol. Pace vendor") 
    @Test(priority=3)        
    public void test_12_5_43() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVendor(Vendor.Pace);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.44 [Default Entitlements] Support of SDV SGID parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_44() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.45 [Default Entitlements] Support of SDV SGID parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_45() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setSdvServiceGroupId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.46 [Default Entitlements] Support of Serial Number parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_46() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.47 [Default Entitlements] Support of Serial Number parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_47() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setSerialNumber(null);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.48 [Default Entitlements] Support of SGID parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_48() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.49 [Default Entitlements] Support of SGID parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_49() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setServiceGroupId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.50 [Default Entitlements] Support of SW Version parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_50() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.51 [Default Entitlements] Support of SW Version parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_51() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setSwVersion(0);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.52 [Default Entitlements] Support of TSID parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_52() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.53 [Default Entitlements] Support of TSID parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_53() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setServiceGroupId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.54 [Default Entitlements] Support of VCT ID parameter. Null value") 
    @Test(priority=3)        
    public void test_12_5_54() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setHubId(-1);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.55 [Default Entitlements] Support of VCT ID parameter. Not null value") 
    @Test(priority=3)        
    public void test_12_5_55() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG.Negative") 
    @Description("12.5.56 [Default Entitlements] Non supported value of connectionMode in DSG 3 request") 
    @Test(priority=3)        
    public void test_12_5_56() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setConnectionMode(null);
        testDSGImpl(msg, 1);
    }
    
    @Features("Entitlements") 
    @Stories("DSG.Negative") 
    @Description("12.5.59 [Default Entitlements] STB sends not all parametes for DSG3 protocol. Corrupted 7bit encoding") 
    @Test(priority=3)        
    public void test_12_5_59() throws Exception {
        testDSGImpl(null, 1);
    }
    
    @Features("Entitlements") 
    @Stories("DSG.Negative") 
    @Description("12.5.61 [Default Entitlements] STB sends not all parametes for DSG3 protocol. Missing ending mandatory parameters") 
    @Test(priority=3)        
    public void test_12_5_61() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setTruncated(true);
        //https://ts.developonbox.ru/!/dashboard/#/CHRAMS-332/:view/
        //testDSGImpl(msg, 1);
        testDSGImpl(msg, 1, true/*suppress error*/, 200);
    }
    
    //--------------------------------------------------------------------------
    //DSG 0,1,2
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.28 [Default Entitlements] Support of DSG 0 protocol") 
    @Test(priority=40)        
    public void test_12_5_28_1() throws Exception {
        //Assert.assertTrue(waitForAMS(AMS_TIMEOUT),"AMS is not active");
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVersion(0);
        msg.setFieldsCount(1);
        msg.setConnectionMode(ConnectionMode.Aloha);
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("[Default Entitlements] Support of DSG 0 protocol (2)") 
    @Test(priority=41)        
    public void test_12_5_28_2() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVersion(0);
        msg.setFieldsCount(2);
        msg.setConnectionMode(ConnectionMode.DAVIC); //make cashe update
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("[Default Entitlements] Support of DSG 0 protocol (3)") 
    @Test(priority=42)        
    public void test_12_5_28_3() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVersion(0);
        msg.setFieldsCount(3);
        msg.setConnectionMode(ConnectionMode.DOCSIS); //make cashe update
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.29 [Default Entitlements] Support of DSG 1 protocol") 
    @Test(priority=43)        
    public void test_12_5_29() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVersion(1);
        msg.setConnectionMode(ConnectionMode.Aloha); //make cashe update
        testDSGImpl(msg);
    }
    
    @Features("Entitlements") 
    @Stories("DSG") 
    @Description("12.5.30 [Default Entitlements] Support of DSG 2 protocol") 
    @Test(priority=44)        
    public void test_12_5_30() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        msg.setVersion(2);
        msg.setConnectionMode(ConnectionMode.DAVIC); //make cashe update
        testDSGImpl(msg);
    }
    //--------------------------------------------------------------------------
    
    
    @Features("Entitlements") 
    @Stories("DSG.Negative") 
    @Description("12.5.20 [Default Entitlements] Settings Middle responds by HTTP error code = 404") 
    @Test(priority=5)        
    public void test_12_5_20() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg, 0, 404);
    }
    
    
    @Features("Entitlements") 
    @Stories("DSG.Negative") 
    @Description("12.5.21 [Default Entitlements] Settings Middle responds by HTTP error code = 502") 
    @Test(priority=5)        
    public void test_12_5_21() throws Exception {
        DSGMessage30 msg = DSGMessage30.getRandom(deviceId);
        testDSGImpl(msg, 0, 502);
    }
    
            
    
    private JSONObject getExpectedSettings(){
        JSONObject obj= new JSONObject();
        obj.put("deviceId", deviceId);
        obj.put("reminderNotification", String.valueOf(true));
        return obj;
    }
    
    private void testDSGImpl(IDSGMessage msg) throws Exception{   
        testDSGImpl(msg, 0, 200);
    }
    
    private void testDSGImpl(IDSGMessage msg, int expectedError) throws Exception{   
        testDSGImpl(msg, expectedError, 200);
    }
    
    private void testDSGImpl(IDSGMessage msg, int expectedError, int heResponse) throws Exception{   
        testDSGImpl(msg, expectedError, false, heResponse);
    }
    
    private void testDSGImpl(IDSGMessage msg, int expectedError, boolean suppressError, int heResponse) throws Exception{   
        
//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        Thread.sleep(20_000);
//       
        DataConsumer consumerSettings = null;
        Semaphore semaphoreSettings = null;
        
        boolean updateOnly = false;
        
        if (msg instanceof DSGMessage30){
            DSGMessage30 msg30 = (DSGMessage30)msg;
            prepareSTBMetadata(msg30.getVendor().getCode(), true/*RandomUtils.getRandomBoolean()*/ /*insert record into STB_METADATA*/);
            if (0 == expectedError){
                semaphoreSettings = new Semaphore(0);
                consumerSettings = new DataConsumer(semaphoreSettings);
            }
        }
        else {
            updateOnly = true;
            prepareSTBMetadata(Vendor.Motorola.getCode());
        }
            
        
        STBMetadata before = new STBMetadata(deviceId);
        
        LighweightCHEemulator he = null;
        DataConsumer  consumer =  null;
        
        CharterStbEmulator stb = null;
        
        try {
            if (0 == expectedError){
                Semaphore semaphore = new Semaphore(0);
            
                consumer = new DataConsumer(semaphore);
                he = createCharterHEEmulator(consumer, HEHandlerType.DENT_NEW);
                if (200 != heResponse){
                    DENTNewHandler handler = (DENTNewHandler) he.getHandler(DENTNewHandler.class);
                    handler.setResponseCode(heResponse);
                }
                if (null != consumerSettings)
                    he.registerDataConsumer(consumerSettings, HEHandlerType.ERROR_REPORTING);
                startCharterHEEmulator(he);
            }
            
            stb = createCharterSTBEmulator();
            startCharterSTBEmulator(stb);
        
            ZodiacMessage message = new ZodiacMessage();
            message.setBodyBytes();
            message.setMessageId(String.valueOf(0));
            int version = (null == msg) ?  DEFAULT_VERSION : msg.getVersion();
            message.setSender(DSG_SERVICE_ID + "/" + version); 
            message.setAddressee(DSG_SERVICE_ID + "/" + version);
            message.setMac(Long.parseLong(properties.getDeviceId(), 16));
            
            byte [] data = (null == msg) ? GARBAGE : msg.getMessage();
            message.setData(data);
            
            //message.setFlag(ZodiacMessage.FL_PERSISTENT);
            //if (!updateOnly)
                message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
            message.setFlag(ZodiacMessage.FL_SENDER_NOT_ADRESSEE);
            //message.setFlag(ZodiacMessage.FL_COMPRESSED);
        
            ZodiacMessage response = null;
            response = (ZodiacMessage)stb.getRUDPTransport().send(message);
//            Thread.sleep(60 * 60 * 1_000);
                
            DOBCountingInputStream in = /*updateOnly ? null : */new DOBCountingInputStream(new ByteArrayInputStream(response.getData()));
            int error = /*updateOnly ? 0:*/DOB7bitUtils.decodeUInt(in)[0];
            Date actualDt = null;
            Date expectedDt = null;
            if (0 == error){
                if (!updateOnly){
                    int timestamp = DOB7bitCoder.decodeBigInt(in);
                    int timezoneOffset = DOB7bitCoder.decodeBigInt(in);
                    actualDt = getAMSActualTime(timestamp);
                    expectedDt = getAMSExpectedTime(timezoneOffset);
                    showAmsRUDPResponse(error, true, timestamp, timezoneOffset);
                }
                else
                    showAmsRUDPResponse(error, false, 0, 0);
            }
            
            Assert.assertTrue(suppressError ?  error == 0 : error == expectedError, "Expected error = " + expectedError + ", but actual = " + error);
            
            if (0 == error && !updateOnly){
                Assert.assertTrue(null != expectedDt, "Expected AMS Date must not be null");
                Assert.assertTrue(null != actualDt, "Actual AMS Date must not be null");
                long diff  = TimeUnit.DAYS.convert(actualDt.getTime() - expectedDt.getTime(), TimeUnit.MILLISECONDS);
                Assert.assertTrue(Math.abs(diff)<1, "Actual and expected AMS time must be the same");
            }
            
            String actual = null;
            URI heURI = null;
            if (null != consumer){
                actual = new String(consumer.getData());
                heURI = consumer.getURI();
                showAmsToHERequest(heURI.toString(), msg.toString(), actual);
            }
            
            String actualSettings = null;
            if (null != consumerSettings){
                actualSettings = new String(consumerSettings.getData());
                showAmsToHESettingsRequest(expectedSettings.toString(), actualSettings);
            }
            
            
            STBMetadata expectedDB = (0 == expectedError || suppressError) ? null : before; 
            STBMetadata actualDB = new STBMetadata(deviceId);
            showSTBDbMetadata(null == expectedDB ? new STBMetadata((IDSGMessage)msg) : expectedDB , actualDB);
            if (null == expectedDB)
                Assert.assertTrue(actualDB.equals(msg), "Expected and actual STB metadata must be the same");
            else
                Assert.assertTrue(actualDB.equals(expectedDB), "Expected and actual STB metadata must be the same");
            
            
            if (null != consumer){
                try {
                    JSONAssert.assertEquals(msg.toJSONArray(), getJSONArray(new JSONObject(actual)), false);
                }
                catch(AssertionError ex){
                    Assert.assertTrue(false, "Expected and actual HE request must be the same");
                }
            }
            
            if (null != consumerSettings){
                try {
                    JSONAssert.assertEquals(expectedSettings, new JSONObject(actualSettings), false);
                }
                catch(AssertionError ex){
                    Assert.assertTrue(false, "Expected and actual HE settings request must be the same");
                }
            }
        }
        finally {
            if (null != he)
                he.stop();
            if (null != stb)
                stb.stop();
        }
    }
    
    public JSONArray getJSONArray(JSONObject json){
        return json.getJSONObject("settings").getJSONObject("attributes").getJSONArray("options");
    }
    
    
    private TimeZone getAMSTimeZone(int timezoneOffset){
        int tz = timezoneOffset/100;
        String tzs = "GMT" + (tz<0 ? "-" : "+") + tz;
        return TimeZone.getTimeZone(tzs);
    }
    
    private Date getAMSActualTime(int timestamp){
        return new Date(timestamp * 1_000L);
    }
    
    private Date getAMSExpectedTime(int timezoneOffset){
        DATE_FORMAT.setTimeZone(getAMSTimeZone(timezoneOffset));
        try {
            return DATE_FORMAT.parse(DATE_FORMAT.format(new Date()));
        }
        catch(ParseException ex){
            return null;
        }
    }
    
    
    @Attachment(value = "AMS to STB Response", type = "text/plain")
    protected String showAmsRUDPResponse(int error, boolean showDate, int timestamp, int timezoneOffset) {
        StringBuilder sb = new StringBuilder();
        sb.append("Error=").append(error);
        if (showDate){
            sb.append(" Timestamp=").append(timestamp).append(" TimezoneOffset=").append(timezoneOffset);
            TimeZone tz = getAMSTimeZone(timezoneOffset);
            DATE_FORMAT.setTimeZone(tz);
            sb.append("\n\nAMS Date: ").append(DATE_FORMAT.format(getAMSActualTime(timestamp)));
            sb.append("\n\nCurrent Date: ").append(DATE_FORMAT.format(getAMSExpectedTime(timezoneOffset)));
        }
        return sb.toString();
    }
    
    
    @Attachment(value = "AMS to HE Request", type = "text/plain")
    protected String showAmsToHERequest(String uri, String expected, String actual) {
        StringBuilder sb = new StringBuilder();
        sb.append("URI: ").append(uri).append("\n\n").
            append("Expected:\n\n").append(expected).append("\n\nActual:\n\n").append(actual);
        return sb.toString();
    }
    
    @Attachment(value = "AMS to HE Settings Request", type = "text/plain")
    protected String showAmsToHESettingsRequest(String expected, String actual){
        StringBuilder sb = new StringBuilder();
        sb.append("Expected:\n\n").append(expected).append("\n\nActual:\n\n").append(actual);
        return sb.toString();
    }
    
    @Attachment(value = "STB_METADATA", type = "text/plain")
    protected String showSTBDbMetadata(STBMetadata expected, STBMetadata actual) {
        StringBuilder sb = new StringBuilder();
        sb.append("Expected:\n\n").append(expected.toString()).append("\n\nActual:\n\n").append(actual.toString());
        return sb.toString();
    }
    
    
    private static class STBMetadata{
        private long stb_mode = -1;
        private long msg_types_map = -1; 
        private long rom_id = -1;
        private long stb_model_id = -1;
        private long vendor_id = -1;
        private long hubid = -1;
        
        
        private STBMetadata(IDSGMessage message){
            if (message instanceof DSGMessage30){
                DSGMessage30 msg = (DSGMessage30)message;
                //if (msg.getVersion() >= 2 && msg.getMigrationStart())
                //    return;
                stb_mode = null == msg.getConnectionMode() ?  -1 : msg.getConnectionMode().ordinal();
                if (0 == msg.getVersion() && msg.getFiledsCount() < 3){
                    if (2 == msg.getFiledsCount())
                        stb_model_id = msg.getBoxModel();
                    return;
                }
                
                rom_id = msg.getBoxModel();
                vendor_id = null == msg.getVendor() ? -1 : msg.getVendor().getCode();
                
                if (msg.getVersion() >= 1)
                    msg_types_map = msg.getMessageTypesMap();
                if (msg.getVersion() >= 3)
                    hubid = msg.getHubId();
            }
            if (message instanceof DSGMessage32){
                DSGMessage32 msg = (DSGMessage32)message;
                stb_mode = null == msg.getConnectionMode() ?  -1 : msg.getConnectionMode().ordinal();
                hubid = msg.getHubId();
            }
        }
        
        private STBMetadata(String deviceId){
            List<Object[]> list = DBHelper.select("select STB_MODE, MSG_TYPES_MAP, ROM_ID, STB_MODEL_ID, VENDOR_ID, HUBID "+
                "from STB_METADATA where MAC=?", Long.parseLong(deviceId, 16));
            if (0 == list.size())
                return;
            Object[] obj = list.get(0);
            if (null != obj[0])
                stb_mode = ((Number)obj[0]).longValue();
            if (null != obj[1])
                msg_types_map = ((Number)obj[1]).longValue();
            if (null != obj[2])
                rom_id = ((Number)obj[2]).longValue();
            if (null != obj[3])
                stb_model_id = ((Number)obj[3]).longValue();
            if (null != obj[4])
                vendor_id = ((Number)obj[4]).longValue();
            if (null != obj[5])
                hubid = ((Number)obj[5]).longValue();
        }
        
        @Override
        public boolean equals(Object obj){
            if (obj instanceof STBMetadata){
                STBMetadata stb = (STBMetadata) obj;
                return  stb.stb_mode == stb_mode &&
                    stb.msg_types_map == msg_types_map &&
                    stb.rom_id == rom_id &&
                    stb.stb_model_id == stb_model_id &&
                    stb.vendor_id == vendor_id &&
                    stb.hubid == hubid;
            }
            else
            if (obj instanceof DSGMessage32){
                DSGMessage32 msg = (DSGMessage32)obj;
                long stb_mode2 = null == msg.getConnectionMode() ?  -1 : msg.getConnectionMode().ordinal();
                return stb_mode2 == stb_mode && msg.getHubId() == hubid;
            }
            else
            if (obj instanceof DSGMessage30){
                DSGMessage30 msg = (DSGMessage30)obj;
                long stb_mode2 = null == msg.getConnectionMode() ?  -1 : msg.getConnectionMode().ordinal();
                long vendor_id2 = null == msg.getVendor() ? -1 : msg.getVendor().getCode();
                long rom_id2 = msg.getBoxModel();
                long hubid2 = msg.getHubId();
                long msg_types_map2 = msg.getMessageTypesMap();
                
                //if (msg.getVersion() >=2 && msg.getMigrationStart())
                //    stb_mode2 = vendor_id2 = rom_id2 = hubid2 = msg_types_map2 = -1;
                
                boolean ok = stb_mode2 == stb_mode;
                if (0 == msg.getVersion() && msg.getFiledsCount() < 3){
                    if (2 == msg.getFiledsCount())
                        ok = ok && rom_id2 == stb_model_id;
                    return ok;
                }
                //https://ts.developonbox.ru/!/dashboard/#/CHRAMS-416/:view/
                ok = ok && rom_id2 == rom_id;
                ok = ok && vendor_id2 == vendor_id;
                if (msg.getVersion() >=1 )
                    ok = ok && msg_types_map2 == msg_types_map; 
                if (msg.getVersion() >=3 )
                    ok = ok && hubid2 == hubid;
                return ok;
            }
            return false;
        }
        
        @Override
        public String toString(){
            StringBuilder sb = new StringBuilder();
            sb.append("STB_MODE=").append(-1 == stb_mode ? "" : stb_mode);
            sb.append(" MSG_TYPES_MAP=").append(-1 == msg_types_map ? "" : msg_types_map);
            sb.append(" ROM_ID=").append(-1 == rom_id ? "" : rom_id);
            sb.append(" STB_MODEL_ID=").append(-1 == stb_model_id ? "" : stb_model_id);
            sb.append(" VENDOR_ID=").append(-1 == vendor_id ? "" : vendor_id);
            sb.append(" HUBID=").append(-1 == hubid ? "" : hubid);
            return sb.toString();
        }
    }
    
    
    @Features("Entitlements") 
    @Stories("Entitlements DSG AMS log")
    @Description("Entitlements DSG AMS log")
    @Test(priority=100)
    public void getDentDsgAmsLog() throws Exception {
        getServerXml();
        stopAndgetAMSLog(true);
    }
    
    
    
}
