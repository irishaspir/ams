package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.ServerXMLHelper;
import com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselDNCS;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import com.zodiac.ams.charter.tests.ft.FTAllureUtils;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

public class PublisherTestDNCS extends PublisherTestBase {
    
    private static final Map<String,String> tarantulaIds = new HashMap<String,String>(){{
        put("addNewFileOnCarousel", "8.1.3");
        put("addNewFileOnCarouselNoFlags", "8.1.38");
        put("addExistingFileOnCarousel", "8.2.48");
        put("deleteFileFromCarousel", "8.2.16");
        put("deleteFileFromCarouselInvalidCarousel", "8.1.45");
        put("deleteFileFromCarouselNoFile", "8.2.72");
        put("deleteFileFromCarouselNoRecord", "8.2.52");
        put("updateExisting", "8.2.7");
        put("addModifyNoVerIncExisting", "8.2.32");
        put("addModifyNoVerIncNoExisting", "8.1.24");
        put("addModifyNoVerIncNoExistingNoFlags", "8.1.42");
        put("clearOld", "8.1.34");
        put("incVersionExisting", "8.1.31");
        put("incVersionNoExisting", "8.2.58");
        put("unknownAction", "8.1.18");
        put("unknownServer", "8.1.2");
    }};
    
    public PublisherTestDNCS(){
        FTAllureUtils.copyAnnotations(PublisherTestBase.class , PublisherTestDNCS.class);
        FTAllureUtils.replaceStory(PublisherTestDNCS.class, this);
        FTAllureUtils.replaceDescription(PublisherTestDNCS.class, this);
        FTAllureUtils.exclude(PublisherTestDNCS.class, this);
        
        this.isZipped = false;
        this.aLocEnabled = false;
        this.dalinc = false;
        this.carouselType = CarouselType.DNCS;
    }
    
    @Override
    public String getStory(){
        return "DNCS Publisher (zipped=false)";
    }
    
    @Override
    public String getDescription(String test, String oldDescription){
        String id = tarantulaIds.get(test);
        return (null == id) ? oldDescription : id + " " + oldDescription;
    }
    
    @Override
    public boolean isExcluded(String test) {
        return false;
    }
    
      
    @Override
    protected void generateServerXML(){
        ServerXMLHelper helper=new ServerXMLHelper(properties);
        helper.save("server.xml");
        initExecutors();
        
    }
    
    void initExecutors(){
        CarouselHostsDNCS hosts = new CarouselHostsDNCS(properties,isZipped,aLocEnabled,dalinc);
        for (int i=0;i<hosts.size();i++){
            CarouselHost host = hosts.get(i);
            SSHHelper executor = new SSHHelper(host.getHost(), host.getUser(), host.getPwd());;
            ICarousel carousel = new CarouselDNCS(host, executor, host.getLocation(), host.isZipped());
            carousel.createRemoteCarouselDir();
            if (0 == i)
                carouselsIn.add(carousel);
            carouselsOut.add(carousel);
        }
    }
    
    @Test
    @Override
    public void addNewFileOnCarousel() throws Exception{
        super.addNewFileOnCarousel();
    }
    
    @Test
    @Override
    public void addNewFileOnCarouselNoFlags() throws Exception{
        super.addNewFileOnCarouselNoFlags();
    }
    
    @Test
    @Override
    public void addExistingFileOnCarousel() throws Exception {   
        super.addExistingFileOnCarousel();
    }
    
    @Test
    @Override
    public void deleteFileFromCarousel() throws Exception{
        //super.deleteFileFromCarousel();
        super.deleteFileFromCarouselAfterUpdate();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidCarousel() throws Exception {
        super.deleteFileFromCarouselInvalidCarousel();    
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselInvalidName() throws Exception {
        super.deleteFileFromCarouselInvalidName();
    } 
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFileNoRecord() throws Exception {
        super.deleteFileFromCarouselNoFileNoRecord();
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoFile() throws Exception {
        super.deleteFileFromCarouselNoFile(); 
    }
    
    @Test
    @Override
    public void deleteFileFromCarouselNoRecord() throws Exception {
        super.deleteFileFromCarouselNoRecord();
    }
     
    @Test
    @Override
    public void updateExisting() throws Exception {
        createOldFiles(UPDATE_EXISTING);
        super.updateExisting();
    }
    
    @Test
    @Override
    public void updateExistingInvalidCarousel() throws Exception {
        super.updateExistingInvalidCarousel();
    }
    
    @Test
    @Override
    public void updateExistingInvalidName() throws Exception {
        super.updateExistingInvalidName();
    }
      
    @Test
    @Override
    public void updateNoExisting() throws Exception {
        super.updateNoExisting();
    }
    
    @Test
    @Override
    public void addModifyExisting() throws Exception {
        createOldFiles(ADD_MODIFY_EXISTING);
        super.addModifyExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExisting() throws Exception {
        super.addModifyNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoExistingNoFlags() throws Exception{
        super.addModifyNoExistingNoFlags();
    }
        
    @Test
    @Override
    public void addModifyNoVerIncExisting() throws Exception {
        super.addModifyNoVerIncExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncNoExisting() throws Exception {
        super.addModifyNoVerIncNoExisting();
    }
    
    @Test
    @Override
    public void clearOld() throws Exception{
        super.clearOld();
    }
    
    @Test
    @Override
    public void addModifyExistingNoFile() throws Exception {
        super.addModifyExistingNoFile();
    }
    
    @Test
    @Override
    public void updateExistingNoFile() throws Exception {
        super.updateExistingNoFile();
    }
    
    @Test
    @Override
    public void addModifyExistingNoRecord() throws Exception {
        super.addModifyExistingNoRecord();
    }
    
    @Test
    @Override
    public void updateExistingNoRecord() throws Exception {
        super.updateExistingNoRecord();
    }
    
    @Test
    @Override
    public void incVersionExisting() throws Exception {
        super.incVersionExisting();
    }
    
    @Test
    @Override
    public void incVersionNoExisting() throws Exception {
        super.incVersionNoExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionExisting() throws Exception {
        super.addModifyNoVerIncIncVersionExisting();
    }
    
    @Test
    @Override
    public void addModifyNoVerIncIncVersionNoExisting() throws Exception {
        super.addModifyNoVerIncIncVersionNoExisting();
    }
    
    @Test
    @Override
    public void unknownAction() throws Exception{
        super.unknownAction();
    }
    
    @Test
    @Override
    public void unknownServer() throws Exception {
        super.unknownServer();
    }
    
    @Test
    @Override
    public void getPublisherTestAMSLog() throws Exception {
        super.getPublisherTestAMSLog();
    }
}
