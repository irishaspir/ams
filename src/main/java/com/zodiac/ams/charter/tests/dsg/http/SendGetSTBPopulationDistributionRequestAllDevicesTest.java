package com.zodiac.ams.charter.tests.dsg.http;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.json.JSONObject;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.setSTBModeInSTBMetadata;
import static com.zodiac.ams.charter.http.helpers.dsg.DsgGetHelper.responseJson;
import static com.zodiac.ams.charter.http.helpers.dsg.DsgGetHelper.sendGetStbPopulationDistributionRequest;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.*;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendGetSTBPopulationDistributionRequestAllDevicesTest extends StepsForDsg {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.1 Send valid GET STB POPULATION DISTRIBUTION REQUEST if there are no any devices")
    @TestCaseId("171556")
    @Test
    public void sendValidGetStbPopulationDistributionRequestDevicesAreAbsent() {
        cleanSTBMetadata();
        assertEquals(sendGetStbPopulationDistributionRequest(), SUCCESS_200);
        JSONObject response = responseJson;
        assertJsonEquals(response, dsgJsonDataUtils.createStbPopulationDistributionResponseJson(0, 0, 0));
        createAttachment(response);
        createAttachment(readAMSLog());
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + StoriesList.GET_STB_POPULATION_DISTRIBUTION_REQUEST)
    @Description("5.2.2 Send valid GET STB POPULATION DISTRIBUTION REQUEST if all device types are presented")
    @TestCaseId("171556")
    @Test
    public void sendValidGetStbPopulationDistributionRequestWithDevicesAllTypes() {
        setSTBModeInSTBMetadata(DAVIC, MAC_NUMBER_1);
        setSTBModeInSTBMetadata(DOCSIS, MAC_NUMBER_2);
        setSTBModeInSTBMetadata(ALOHA, MAC_NUMBER_3);
        assertEquals(sendGetStbPopulationDistributionRequest(), SUCCESS_200);
        JSONObject response = responseJson;
        assertJsonEquals(response, dsgJsonDataUtils.createStbPopulationDistributionResponseJson(1, 1, 1));
        createAttachment(response);
        createAttachment(readAMSLog());
    }
}
