package com.zodiac.ams.charter.tests.dc.assertion.csv.file;

import com.dob.ams.common.util.DOBUtil;
import com.zodiac.ams.charter.csv.file.entry.HddCsvFileEntry;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;

import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEntriesCountEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertEventTimeEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertExternalHddCapacityEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertExternalHddFreeSpacePercentEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertInternalHddCapacityEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertInternalHddFreeSpacePercentEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertMacEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertSequenceNumberEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.AssertionUtils.assertStbSessionIdEquals;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputEventsCount;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.calculateInputTimeDeltaSum;
import static com.zodiac.ams.charter.tests.dc.assertion.LocalUtils.toLocalDateTime;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_3;

/**
 * The HDD CSV file assertion implementation.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class HddCsvFileAssertion extends AbstractCsvFileAssertion<HddCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param input the input
     */
    public HddCsvFileAssertion(CsvFileAssertionInput input) {
        super(input);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileAssertion#assertFile(List)
     */
    @Override
    @SuppressWarnings("UnnecessaryLocalVariable")
    public void assertFile(List<HddCsvFileEntry> csvFileEntries) {
        if (CollectionUtils.isEmpty(csvFileEntries)) {
            throw new IllegalArgumentException("The csv file entries can't be null or empty");
        }

        int eventsCount = calculateInputEventsCount(input);

        assertEntriesCountEquals(eventsCount, csvFileEntries.size());

        ListIterator<HddCsvFileEntry> csvFileEntriesIterator = csvFileEntries.listIterator();

        Long inputMac = input.getMac();
        String inputMacId = DOBUtil.mac2string(inputMac);

        int inputMessageType = input.getMessageType();

        List<CsvFileAssertionInput.SessionData> sessionDataList = input.getSessionDataList();
        for (CsvFileAssertionInput.SessionData sessionData : sessionDataList) {
            List<CsvFileAssertionInput.EventData> eventDataList = sessionData.getEventDataList();

            int inputFirstEventNumber = sessionData.getFirstEventNumber();

            int inputSessionStartTimestamp = sessionData.getSessionStartTimestamp();

            int inputTimeDeltaSum = calculateInputTimeDeltaSum(sessionData);

            int sequenceNumberCounter = 1;

            for (CsvFileAssertionInput.EventData eventData : eventDataList) {
                HddCsvFileEntry csvFileEntry = csvFileEntriesIterator.next();

                List<Object> inputParameters = eventData.getParameters();
                Integer inputParameterInternalHddCapacity = (Integer) inputParameters.get(0);
                Integer inputParameterExternalHddCapacity = (Integer) inputParameters.get(1);
                Integer inputParameterInternalHddFreeSpacePercent = (Integer) inputParameters.get(2);
                Integer inputParameterExternalHddFreeSpacePercent = (Integer) inputParameters.get(3);

                int inputTimestampPartMsec = eventData.getTimestampPartMsec();

                String macId = csvFileEntry.getMacId();
                Long mac = DOBUtil.string2mac(macId);
                String stbSessionId = csvFileEntry.getStbSessionId();
                Integer sequenceNumber = csvFileEntry.getSequenceNumber();
                LocalDateTime eventTime = csvFileEntry.getEventTime();
                Integer internalHddCapacity = csvFileEntry.getInternalHddCapacity();
                Integer externalHddCapacity = csvFileEntry.getExternalHddCapacity();
                Integer internalHddFreeSpacePercent = csvFileEntry.getInternalHddFreeSpacePercent();
                Integer externalHddFreeSpacePercent = csvFileEntry.getExternalHddFreeSpacePercent();

                // mac assertion
                // the same <MAC ID> field as in TUNING.CSV
                assertMacEquals(mac, inputMac);
                // end of mac assertion

                // stbSessionId assertion
                // the same as <STB Session ID> field in TUNING.CSV
                String inputStbSessionId = "Z" + "_" + inputMacId + "_" + inputSessionStartTimestamp;
                assertStbSessionIdEquals(stbSessionId, inputStbSessionId);
                // end of stbSessionId assertion

                // sequenceNumber assertion
                // the same as <Sequence Number> field in TUNING.CSV
                Integer inputSequenceNumber = null;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputSequenceNumber = null;
                } else {
                    inputSequenceNumber = inputFirstEventNumber + (sequenceNumberCounter - 1);
                }
                assertSequenceNumberEquals(sequenceNumber, inputSequenceNumber);
                // end of sequenceNumber assertion

                // eventTime assertion
                // the same as <Event Start Time> field in TUNING.CSV
                long inputEventTimeMsec;
                if (inputMessageType < MESSAGE_TYPE_3) {
                    inputEventTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L;
                } else {
                    inputEventTimeMsec = (inputSessionStartTimestamp + inputTimeDeltaSum) * 1000L
                            + inputTimestampPartMsec;
                }
                LocalDateTime inputEventTime = toLocalDateTime(inputEventTimeMsec);
                assertEventTimeEquals(eventTime, inputEventTime);
                // end of eventTime assertion

                // internalHddCapacity assertion
                // if current event 'internalHDDCapacity' == 0 -> Empty
                // otherwise -> current event 'internalHDDCapacity' value
                Integer inputInternalHddCapacity = inputParameterInternalHddCapacity;
                assertInternalHddCapacityEquals(internalHddCapacity, inputInternalHddCapacity);
                // end of internalHddCapacity assertion

                // externalHddCapacity assertion
                // if current event 'externalHDDCapacity' == 0 -> Empty
                // otherwise -> current event 'externalHDDCapacity' value
                Integer inputExternalHddCapacity = inputParameterExternalHddCapacity;
                assertExternalHddCapacityEquals(externalHddCapacity, inputExternalHddCapacity);
                // end of externalHddCapacity assertion

                // internalHddFreeSpacePercent assertion
                // current event 'internalHDDFreeSpace' == 0 -> Empty
                // otherwise -> current event 'internalHDDFreeSpace' value
                Integer inputInternalHddFreeSpacePercent = inputParameterInternalHddFreeSpacePercent;
                assertInternalHddFreeSpacePercentEquals(internalHddFreeSpacePercent, inputInternalHddFreeSpacePercent);
                // end of internalHddFreeSpacePercent assertion

                // externalHddFreeSpacePercent assertion
                // current event 'externalHDDFreeSpace' == 0 -> Empty
                // otherwise -> current event 'externalHDDFreeSpace' value
                Integer inputExternalHddFreeSpacePercent = inputParameterExternalHddFreeSpacePercent;
                assertExternalHddFreeSpacePercentEquals(externalHddFreeSpacePercent, inputExternalHddFreeSpacePercent);
                // end of externalHddFreeSpacePercent assertion

                sequenceNumberCounter++;
            }
        }
    }
}
