package com.zodiac.ams.charter.tests.epg.get.validation.integration;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class SendGetEpgDataValuesIntegrationTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.1 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAA_NC() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"NC-17\"", "Drama", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"NC-17\"", "\"Drama\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAA_NR_Adult() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"NR-ADULT\"", "Drama", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"NR-ADULT\"", "\"Drama\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Genre_Adult() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Adult", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Adult\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Genre_Adult_Only() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Adults only", "AC"});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Adults only\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Genre_Erotic() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Erotic", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Erotic\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Genre_Mature() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Mature", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Mature\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Genre_Uncensored() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Uncensored", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Uncensored\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Genre_FantasyAndAdult() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy, Adult", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy, Adult\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_RP() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"RP\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"RP\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_AC() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_AL() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"AL\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"AL\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_GL() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"GL\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"GL\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_MV() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"MV\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"MV\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_V() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"V\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"V\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_GV() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"GV\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"GV\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_BN() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"BN\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"BN\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_N() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"N\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"N\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_Advisory_SSC() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"\"", "Fantasy", "\"SSC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"\"", "\"Fantasy\"", "\"SSC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAARating_R_Genre_Adult() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"R\"", "Adult", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"R\"", "\"Adult\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAARating_G_Genre_AdultsOnly() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"G\"", "Adults only", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"G\"", "\"Adults only\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAARating_PG_Genre_Erotic() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"PG\"", "Erotic", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"PG\"", "\"Erotic\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAARating_PG13_Genre_Mature() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"PG-13\"", "Mature", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"PG-13\"", "\"Mature\"", "\"AC\""}));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.3.2 Send Get EPG data, receive file with different valid values GENRES")
    @Test ()
    public void sendGetEpgDataReceiveFileWith_MPAARating_R_Genre_Uncensored() {
        String csv = epgCsvHelper.createCsvWithMPAARatingGenreAdvisory(new String[]{"\"R\"", "Uncensored", "\"AC\""});
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate( DateTimeHelper.getSegmentsDaysFromDb()),
                epgCsvHelper.expectedCsvWithSegmentsMPAARatingGenreAdvisory(new String[]{"\"R\"", "\"Uncensored\"", "\"AC\""}));
    }

}
