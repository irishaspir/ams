package com.zodiac.ams.charter.tests.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.IStartPacketProvider;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedSuccessResult;
import static com.zodiac.ams.charter.tests.ft.rudp.Operation.getExpectedFailureResult;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorCodeType;

public class TestingServiceN002 extends TestingService {
    private static final ErrorCodeType expected = ErrorCodeType.FILEID;
     
    class StartPacketProvider implements IStartPacketProvider{
        
        @Override
        public StartPacket get(StartPacket packet) {
            int ds = packet.getDataSize();
            int nds;
            int ss = packet.getSegmentSize();
            int nss;
            
            do {
                nds = new Random().nextInt(ds)+1;
                packet.setDataSize(nds);
            }
            while (nds == ds);
            
            do {
                nss = new Random().nextInt(ss)+1;
                packet.setSegmentSize(nss);
            }
            while (nss == ss);
            
            return packet;
        }        
    }
    
    TestingServiceN002(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        super(sender,sessionId,order,operation,countDown);
    }
    
    
    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = getTestingSession(sessionId);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (!getExpectedSuccessResult(sessionId,future))
                return;
            
            ts.addMsg("The Original Ack Start packet: "+ts.getAckStartPacket());
            ts.setAckStartPacket(null, true);
            ts.getRUDPSenderSession().clearStartAckReceived();
           
            sender.setStartPacketProvider(sessionId,new StartPacketProvider());
            
            future=sender.sendStart(sessionId);
            getExpectedFailureResult(sessionId,future,expected);
            
            ts.addMsg("The Second Ack Start packet: "+ts.getAckStartPacket());
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            sender.removeStartPacketProvider(sessionId);
            if (null != this.countDown)
                countDown.countDown();
        }
    }

    
}
