package com.zodiac.ams.charter.tests.ft;

public interface IExcludeProvider {
    boolean isExcluded(String test);
    
}
