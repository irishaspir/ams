package com.zodiac.ams.charter.tests.rollback.migrationFlagNotification;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dsg.message.DSGSTBResponseBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.migrationDataWillBeSentSoon;
import static org.testng.Assert.assertTrue;


public class SendMigrationStartNotificationFromSTB extends StepsForRollbackWithDSG {

    private DSGSTBResponseBuilder dsgstbResponseBuilder = new DSGSTBResponseBuilder();

    @Features(FeatureList.ROLLBACK)
    @Stories(StoriesList.POSITIVE + StoriesList.MIGRATION_START_NOTIFICATION)
    @Description("11.2.1 Send 'migrationStart' notification from STB")
    @TestCaseId("12345")
    @Test()
    public void sendMigrationStartNotification() {
        ZodiacMessage message = version2Request.createMessage(MAC_NUMBER_1, String.valueOf(migrationDataWillBeSentSoon));
        List<String> stbResponse = version2Request.sendAndParseRequest(message);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(MAC_NUMBER_1), ERROR + IN_DSG_RESPONSE);
        createAttachment(readAMSLog());
        checkDB();
        assertTrue(rollbackLogParser.compareLogAMSWithPatternDSGIntegration(rollbackLogParser.getLogAMSForValid(log),
                DEVICE_ID_NUMBER_1), PATTERN_ROLLBACK_LOG);

    }
}
