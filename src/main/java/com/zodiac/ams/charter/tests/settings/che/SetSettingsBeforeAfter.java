package com.zodiac.ams.charter.tests.settings.che;


import com.zodiac.ams.charter.emuls.che.settings.SettingsSampleBuilder;

import com.zodiac.ams.charter.tests.settings.StepsForSettings;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.File;

import static com.zodiac.ams.common.helpers.DateTimeHelper.*;


public class SetSettingsBeforeAfter extends StepsForSettings {

    @BeforeMethod
    public void allSettingsWithout59And60Keys() {
        setDefaultValue(options, MAC_NUMBER_2);
        delete55And59And60Keys(options);
        startTestTime = getCurrentTimeTest();
    }

    @BeforeClass
    protected void runEmulators() {
        new SettingsSampleBuilder(DEVICE_ID_NUMBER_2, "./dataCHE", "/sample1.xml").messageBuild();
        setDefaultValue(options, MAC_NUMBER_2);
        runCHESettings();
        runSTB();
    }

    @AfterClass
    protected void stopEmulators() {
        stopCHESettings();
        stopSTB();
        new File("./dataCHE/sample1.xml").delete();
    }
}
