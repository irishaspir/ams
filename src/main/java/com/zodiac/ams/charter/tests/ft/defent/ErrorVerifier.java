package com.zodiac.ams.charter.tests.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.zodiac.ams.charter.services.ft.defent.DefEntMessage;
import java.io.ByteArrayInputStream;
import org.testng.Assert;

class ErrorVerifier implements DENTVerifier{
    private final int expected;
    
    ErrorVerifier(int error){
        this.expected = error;
    }

    @Override
    public void verify(DefEntMessage msg, byte[] response) throws Exception {
        try (DOBCountingInputStream in = new DOBCountingInputStream(new ByteArrayInputStream(response));){
            int error = DOB7bitUtils.decodeUInt(in)[0];
            Assert.assertTrue(expected == error, "Expected error=" + expected + ", but actual=" + error);
        }
    }
    
}
