package com.zodiac.ams.charter.tests.epg.get.validation.positive;

import com.zodiac.ams.charter.helpers.epg.DataForCsvHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.epg.get.StepsForGetEpg;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.bd.ams.tables.EpgDataUtils.getStringRowByDate;
import static com.zodiac.ams.charter.http.helpers.epg.EpgGetHelper.sendUpdateEpgDataRequest;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class SendGetEpgDataValidStringPropertiesTest extends StepsForGetEpg {

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.25, 2.1.26 Send Get EPG data, receive file with different valid values GENRES")
    @Test(dataProvider = "dataForGenres", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithGenres(String inputGenres, String outputGenres) {
        String csv = epgCsvHelper.createCsvWithDifferentGenres(inputGenres);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        log = readAMSLog();
        createAttachment(log);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsGenres(outputGenres));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.65 Send Get EPG data, receive file with different valid values TV_RATING")
    @Test(dataProvider = "dataForTVRating", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithDifferentTVRating(String inputTVRating, String outputTVRating) {
        String csv = epgCsvHelper.createCsvWithTVRating(inputTVRating);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        log = readAMSLog();
        createAttachment(log);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsTVrating(outputTVRating));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.67 Send Get EPG data, receive file with different valid values MPAA_RATING")
    @Test(dataProvider = "dataForMPAARating", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithDifferentMPAARating(String inputMPAARating, String outputMPAARating) {
        String csv = epgCsvHelper.createCsvWithMPAARating(inputMPAARating);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        log = readAMSLog();
        createAttachment(log);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsMPAArating(outputMPAARating));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.9 Send Get EPG data, receive file with different valid values ADVISORY")
    @Test(dataProvider = "dataForAdvisory", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithDifferentAdvisory(String inputAdvisory, String outputAdvisory) {
        String csv = epgCsvHelper.createCsvWithAdvisory(inputAdvisory);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        log = readAMSLog();
        createAttachment(log);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        List<Pattern> epgPattern = epgLog.createPattern(epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder());
        createPatternAttachment(epgPattern);
        createAttachment(logEpg);
        boolean x = epgLog.compareLogAMSWithPatternValid(logEpg, epgPattern);
        assertTrue(x, PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsAdvisory(outputAdvisory));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.66 Send Get EPG data, receive file with different valid values PROGRAM_ID")
    @Test(dataProvider = "dataForProgramId", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithDifferentProgramId(String programType) {
        String csv = epgCsvHelper.createCsvWithProgramId(programType);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        log = readAMSLog();
        createAttachment(log);
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsProgramId(programType));
    }

    @Features(FeatureList.EPG)
    @Stories(StoriesList.VALIDATION_POSITIVE + StoriesList.GET_EPG_DATA)
    @Description("2.1.64 Send Get EPG data, receive file with different valid values SERIES_IDENTIFIER")
    @Test(dataProvider = "dataForProgramId", dataProviderClass = DataForCsvHelper.class)
    public void sendGetEpgDataReceiveFileWithDifferentSeriesIdentifier(String programType) {
        String csv = epgCsvHelper.createCsvWithSeriesIdentifier(programType);
        createAttachment(csv);
        assertEquals(sendUpdateEpgDataRequest(), SUCCESS_200);
        log = readAMSLog();
        createAttachment(log);
        assertEquals(epgHttpDataUtils.getEpgDataUriListPattern(), epgCHEEmulator.getURIList());
        List<String> logEpg = epgLog.getLogAMSForValidData(log);
        copyZDB(epgLog.getPublishedZdbFiles(logEpg));
        assertTrue(epgLog.compareLogAMSWithPatternValid(logEpg, epgCsvHelper.getCountRows(), epgCsvHelper.getCountServices(), zdbDataUtisl.epgLastFolder()),
                PATTERN_EPG_LOG);
        assertTrue(epgDataChecker.compareEpgVerifierLogWithPattern(epgDataChecker.createPatternValidEpgVerifierLog(amountRows), runEPGVerifier()),
                PATTERN_EPG_VERIFIER_LOG);
        assertEquals(getStringRowByDate(DateTimeHelper.getSegmentsDaysFromDb()), epgCsvHelper.expectedCsvWithSegmentsSeriesIdentifier(programType));
    }

}
