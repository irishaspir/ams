package com.zodiac.ams.charter.tests.rollback.migrationFlagNotification;

import com.zodiac.ams.charter.bd.ams.tables.*;
import com.zodiac.ams.charter.emuls.stb.settings.SettingsSTBEmulator;
import com.zodiac.ams.charter.rudp.dsg.message.cm.Version2Request;

import com.zodiac.ams.charter.tests.rollback.StepsForRollback;
import com.zodiac.ams.common.logging.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.allure.annotations.Step;

import static com.zodiac.ams.charter.bd.ams.tables.AppChanAuthServicesUtils.getAllFromAppChanAuthServicesByMac;
import static com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils.getAllFromSubscriptionsByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrHistoryUtils.getAllFromDvrHistoryByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrMigrCHEMtdReqUtils.getAllFromDvrMigrCHEMtdReqByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrMigrForCHEUtils.getAllFromDvrMigrForCHEByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrMigrForSTBUtils.getAllFromDvrMigrForSTBByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrRecordingsUtils.getAllFromDvrRecordingsByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrSTBUtils.getAllFromDvrSTBByMac;
import static com.zodiac.ams.charter.bd.ams.tables.DvrUnsyncSTBUtils.getAllFromDvrUnsyncSTBByMac;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.getAllSettingsSTBDataV2ByMac;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.getAllSettingsByMacFromSettingsSTB;
import static org.testng.Assert.assertEquals;

public class StepsForRollbackWithDSG extends StepsForRollback {

    protected Version2Request version2Request;
    private  SettingsSTBEmulator settingsSTBEmulator;

    @BeforeMethod
    protected void runSTB() {
        settingsSTBEmulator = new SettingsSTBEmulator();
        settingsSTBEmulator.registerConsumer();
        settingsSTBEmulator.start();
        Logger.info("STB Emulator's run without timeout");
        version2Request = new Version2Request(settingsSTBEmulator);
        getTestTime();
        cleanDB();
        insertDataToDB();
    }

    @Step
    private void cleanDB(){
        new STBMetadataUtils().deleteAll();
        new CharterSubscriptionUtils().deleteAll();
        new DvrHistoryUtils().deleteAll();
        new DvrRecordingsUtils().deleteAll();
        new DvrUnsyncSTBUtils().deleteAll();
        new DvrSTBUtils().deleteAll();
        new DvrMigrForCHEUtils().deleteAll();
        new DvrMigrCHEMtdReqUtils().deleteAll();
        new DvrMigrForSTBUtils().deleteAll();
        new AppChanAuthServicesUtils().deleteAll();
        new STBMetadataUtils().deleteAll();
        new STBLastActivityUtils().deleteAll();
    }

    @Step
    private void insertDataToDB(){
     //   STBMetadataUtils.insertToSTBMetadata(Long.parseLong(MAC_NUMBER_1), DEVICE_ID_NUMBER_1);
        CharterSubscriptionUtils.insertRowToCharterSubscription(DEVICE_ID_NUMBER_1);
        DvrSTBUtils.insertToDvrSTB(MAC_NUMBER_1, DEVICE_ID_NUMBER_1);
        DvrMigrForSTBUtils.insertToDvrMigrForSTB(MAC_NUMBER_1);
        DvrMigrCHEMtdReqUtils.insertToDvrMigrCHEMtdReq(MAC_NUMBER_1);
        DvrMigrForCHEUtils.insertToDvrMigrForCHE(MAC_NUMBER_1);
        DvrRecordingsUtils.insertToDvrRecordings(MAC_NUMBER_1);
        AppChanAuthServicesUtils.insertRowToAppChanAuthServices(MAC_NUMBER_1, DEVICE_ID_NUMBER_1);
        DvrHistoryUtils.insertToDvrHistory(MAC_NUMBER_1);
        DvrUnsyncSTBUtils.insertToDvrUnsyncSTB(MAC_NUMBER_1);
    }

    @Step
    protected void checkDB(){
        assertEquals(getAllFromDvrHistoryByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllFromDvrSTBByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllFromDvrMigrCHEMtdReqByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllFromDvrMigrForCHEByMac(MAC_NUMBER_1), NO_DATA_IN_DB);

        assertEquals(getAllFromDvrMigrForSTBByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllFromDvrUnsyncSTBByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllFromDvrRecordingsByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllFromSubscriptionsByMac(MAC_NUMBER_1), NO_DATA_IN_DB);

        assertEquals(getAllFromAppChanAuthServicesByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(new STBMetadataUtils().getAllSTBMetadataByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllSettingsByMacFromSettingsSTB(MAC_NUMBER_1), NO_DATA_IN_DB);
        assertEquals(getAllSettingsSTBDataV2ByMac(MAC_NUMBER_1), NO_DATA_IN_DB);
    }

    @AfterMethod
    protected void afterMethod() {
        settingsSTBEmulator.stop();
        Logger.info("STB Emulator's stopped");
        cleanDB();
    }

}
