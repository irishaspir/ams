package com.zodiac.ams.charter.tests.callerId.subscription.realTimeMode;

import com.zodiac.ams.charter.tests.callerId.subscription.StepsForCallerIdSubscription;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class StepsForCallerIdRealTimeSubscriptionMode extends StepsForCallerIdSubscription {

    @BeforeMethod
    public void beforeMethod() {
        runCHECallerId();
        runSTBCallerId();
        getTestTime();
    }

    @AfterMethod
    public void afterMethod() {
        stopCHECallerId();
        stopSTBCallerId();
    }

}
