package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC Time delta.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcTimeDeltaTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIME_DELTA_NEGATIVE + "1 - Check without Time delta")
    @Description("4.09.1 [DC][Time delta] Check without Time delta" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<><286><0><10057|0|50>}]")
    public void sendMessageWithoutTimeDelta() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                // TimeDelta must be null, this is the test purpose!
                .setTimeDelta(null)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIME_DELTA_NEGATIVE + "2 - Check with incorrect Time delta")
    @Description("4.09.2 [DC][Time delta] Check with incorrect Time delta" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<ererrwer><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectTimeDelta1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                // TimeDelta must be garbage, this is the test purpose!
                .setTimeDelta("ererrwer")
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIME_DELTA_NEGATIVE + "2 - Check with incorrect Time delta")
    @Description("4.09.2 [DC][Time delta] Check with incorrect Time delta" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<@#$%@#%><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectTimeDelta2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                // TimeDelta must be garbage, this is the test purpose!
                .setTimeDelta("@#$%@#%")
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_TIME_DELTA_NEGATIVE + "2 - Check with incorrect Time delta")
    @Description("4.09.2 [DC][Time delta] Check with incorrect Time delta" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<  ><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectTimeDelta3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                // TimeDelta must be garbage, this is the test purpose!
                .setTimeDelta("  ")
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
