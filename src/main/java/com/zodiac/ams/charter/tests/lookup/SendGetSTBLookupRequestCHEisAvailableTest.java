package com.zodiac.ams.charter.tests.lookup;

import com.zodiac.ams.charter.http.helpers.lookup.LookupGetHelper;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.lookup.LookupGetHelper.responseJson;
import static com.zodiac.ams.charter.http.helpers.lookup.LookupGetHelper.sendGetStbStatusRequestMac;
import static com.zodiac.ams.charter.http.helpers.lookup.LookupJsonDataUtils.nullResponse;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.jvnet.jaxb2_commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;

public class SendGetSTBLookupRequestCHEisAvailableTest extends StepsForLookupCHEisAvailable {

    @Features(FeatureList.STB_LOOKUP)
    @Stories(StoriesList.NEGATIVE + StoriesList.STB_LOOKUP_NEGATIVE)
    @Description("14.2.2 Send GET STB LOOKUP REQUEST, MAC is unknown")
    @TestCaseId("167174")
    @Test()
    public void sendGetStbLookupRequestMacIsUnknown() {
        assertEquals(sendGetStbStatusRequestMac(UNKNOWN_DEVICE_ID), ERROR_500);
        createAttachment(readAMSLog());
        assertJsonEquals(responseJson, nullResponse());
    }

    @Features(FeatureList.STB_LOOKUP)
    @Stories(StoriesList.NEGATIVE + StoriesList.STB_LOOKUP_NEGATIVE)
    @Description("14.2.3 Send GET STB LOOKUP REQUEST, MAC is absent")
    @TestCaseId("167176")
    @Test()
    public void sendGetStbLookupRequestMacIsAbsent() {
        assertEquals(sendGetStbStatusRequestMac(EMPTY), ERROR_500);
        createAttachment(readAMSLog());
        assertJsonEquals(responseJson, nullResponse());
    }
}
