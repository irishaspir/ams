package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC Events number.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcEventsNumberTest extends StepsForDc {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENTS_NUMBER_NEGATIVE + "1 - Check without Events number")
    @Description("4.08.1 [DC][Events number] Check without Events number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithoutEventsNumber() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                // EventsNumber must be null, this is the test purpose!
                .setEventsNumber(null)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(null)
                .endEvent() //

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENTS_NUMBER_NEGATIVE + "2 - Check with incorrect Events number")
    @Description("4.08.2 [DC][Events number] Check with incorrect Events number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><  >" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectEventsNumber1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                // EventsNumber must be garbage, this is the test purpose!
                .setEventsNumber("  ")

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(null)
                .endEvent() //

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENTS_NUMBER_NEGATIVE + "2 - Check with incorrect Events number")
    @Description("4.08.2 [DC][Events number] Check with incorrect Events number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><dsfsdf>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectEventsNumber2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                // EventsNumber must be garbage, this is the test purpose!
                .setEventsNumber("dsfsdf")

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(null)
                .endEvent() //

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><Parameters>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENTS_NUMBER_NEGATIVE + "2 - Check with incorrect Events number")
    @Description("4.08.2 [DC][Events number] Check with incorrect Events number" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><#$%@#$%>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithIncorrectEventsNumber3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                // EventsNumber must be garbage, this is the test purpose!
                .setEventsNumber("#$%@#$%")

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(0) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(null)
                .endEvent() //

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
