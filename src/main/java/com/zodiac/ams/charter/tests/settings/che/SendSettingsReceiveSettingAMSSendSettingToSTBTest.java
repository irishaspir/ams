package com.zodiac.ams.charter.tests.settings.che;

import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.settings.DataSettingsHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostSettingsDeviseIdNames;
import static com.zodiac.ams.charter.tests.StoriesList.SET_SETTINGS_FROM_CHE;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

/**
 * Created by SpiridonovaIM on 10.04.2017.
 */
public class SendSettingsReceiveSettingAMSSendSettingToSTBTest extends SetSettingsBeforeAfter {

    @Features(FeatureList.SETTINGS)
    @Stories(StoriesList.POSITIVE+SET_SETTINGS_FROM_CHE)
    @Description("1.4.6 Send POST setSettings from CHE  to AMS, settings are valid, STB is available, receive setting, " +
            "AMS sends Settings to a Box (POST HTTP://host:port/ams/settings)" +
            "key 55, key 59 and key 60 are never send from CHE")
    @TestCaseId("12345")
    @Test(dataProvider = "settingsListWithout55And59And60Keys", dataProviderClass = DataSettingsHelper.class)
    public void sendSettingsReceiveSettingAMSSendSettingToSTB(String name, SettingsOption setting) {
        String mac=MAC_NUMBER_2;
        ArrayList<SettingsOption> options = SettingsOption.create()
                .addOption(setting)
                .build();
        changeKeyValue(options, mac);
        String response = sendPostSettingsDeviseIdNames(getDeviceIdByMac(mac), options);
        createAttachment(SettingsPostHelper.responseJson);
        createAttachment(parserLog.readLog(PATH_TO_LOG, startTestTime, READ_LOG_TIMEOUT));
        assertEquals(response, SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        assertEquals(settingsRudpHelper.getSettingsStbResponse(), settingsSTBResponse.stbSetSettingsRequestPattern(options), INCORRECT_SETTINGS);
    }
}
