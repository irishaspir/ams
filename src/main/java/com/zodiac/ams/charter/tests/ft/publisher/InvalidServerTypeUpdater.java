package com.zodiac.ams.charter.tests.ft.publisher;

import com.zodiac.ams.charter.services.ft.publisher.PublishTask;
import com.zodiac.ams.charter.services.ft.publisher.carousel.ICarousel;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;

public class InvalidServerTypeUpdater implements ITasksUpdater{

    @Override
    public void update(ICarousel carousel, PublishTask task) {
    }

    @Override
    public CarouselType getCarouselType(CarouselType type) {
        return CarouselType.UNKNWON;
    }
    
}
