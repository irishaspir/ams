package com.zodiac.ams.charter.tests.dc.common;

/**
 * The message type constants.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public interface MessageType {
    /*
     * 1 - basic message format
     */
    int MESSAGE_TYPE_1 = 1;
    /*
     * 2 - basic message format extended with additional parameters in Event ID=0 and Event ID=2 (see Chapter 3)
     */
    int MESSAGE_TYPE_2 = 2;
    /*
     * 3 - the same as in 'MessageType'=2 with the following enhancement:
     * additional parameters <First event number>, <timestampPartMsec>  in message format (see Chapter 2.2)
     * updated events: EventID=1, and EventID=2 (see Chapter 3)
     * new events: Event ID=3, EventID=4, EventID=5, EventID=6, EventID=7, EventID=8, EventID=9, and EventID=10 (see Chapter 3)
     */
    int MESSAGE_TYPE_3 = 3;
    /*
     * 4 - the same as in 'MessageType'=3 with the following enhancement:
     * new events: EventID=11 and EventID=12  (see Chapter 3)
     * <Lineup ID> and <STB timezone> removed from the message format (see Chapter 2.2)
     * new parameter <Heartbeat> in message format (see Chapter 2.2)
     */
    int MESSAGE_TYPE_4 = 4;
    /*
     * 5 - the same as in 'messageType'=4 with the following enhancement:
     * <Strings #> and <Strings Array> added (see Chapter 2.2)
     * new events: EventID=13, EventID=14  (see Chapter 3)
     */
    int MESSAGE_TYPE_5 = 5;
}
