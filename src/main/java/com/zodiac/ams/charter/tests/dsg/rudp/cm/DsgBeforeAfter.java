package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dsg.message.DSGSTBResponseBuilder;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.insertToSTBMetadata;

import static com.zodiac.ams.charter.runner.Utils.getMacFromRange;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.connectionMode;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;

public class DsgBeforeAfter extends JsonSteps {

    protected String mac;
    protected DSGSTBResponseBuilder dsgstbResponseBuilder = new DSGSTBResponseBuilder();
    protected ZodiacMessage message;

    @BeforeClass
    public void startEmulatorsForDsg() {
        runSTBForDSG();
        runCHE();
    }

    @BeforeMethod
    public void getTimeForDSG() {
        connectionMode = changeConnectionMode();
        startTestTime = getCurrentTimeTest();
        mac = getMacFromRange();
        insertToSTBMetadata(mac);

    }

    @AfterClass
    public void stopEmulatorsForDSG() {
        stopSTB();
        stopSettingsCHE();
    }
}
