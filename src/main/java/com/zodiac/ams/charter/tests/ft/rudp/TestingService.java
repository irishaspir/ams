package com.zodiac.ams.charter.tests.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingPackage;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager;
import org.jboss.netty.channel.ChannelFuture;
import static com.zodiac.ams.charter.tests.ft.rudp.RUDPTestingUtils.getSegments;
import com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSession;
import java.util.concurrent.CountDownLatch;

class TestingService implements Runnable{
    final RUDPTestingSendManager sender;
    final int sessionId;
    final IOperation operation;
    final ORDER order;
    final CountDownLatch countDown;
                            
    TestingService(RUDPTestingSendManager sender,int sessionId,ORDER order,IOperation operation,CountDownLatch countDown){
        this.sender = sender;
        this.sessionId =  sessionId;
        this.order = order;
        this.operation = operation;
        this.countDown = countDown; 
    }

    @Override
    public void run() {
        RUDPTestingSession ts =  null;
        try{
            ts = sender.getTestingSession(sessionId);
            RUDPTestingPackage pk = sender.getRUDPTestingPackage(sessionId);
            
            ChannelFuture future=sender.sendStart(sessionId);
            if (null != operation && !operation.isSuccessAfterStart(sessionId,future))
                return;
            
            int count = pk.getSegmentsCount();
            int [] segments = getSegments(count,order);
            
             if (null == segments){
                future=sender.sendData(sessionId);
                if (null != operation && !operation.isSuccessAfterData(sessionId,future))
                    return;
            }
            else {
                int index = 0;
                for (int segment: segments){
                    future=sender.sendSegment(sessionId,segment);
                    if (null != operation && !operation.isSuccessAfterSegment(sessionId,future, segment, index++, segments.length))
                        return;
                }
            }
             if (null != operation)
                 operation.isSuccessAfterSession(sessionId);
        }
        catch(Exception ex){
            if (null != ts){
                ts.setSuccess(false);
                ts.setErrorMsg(ex.getMessage());
            }
        }
        finally{
            if (null != this.countDown)
                countDown.countDown();
        }
    }
    
}
