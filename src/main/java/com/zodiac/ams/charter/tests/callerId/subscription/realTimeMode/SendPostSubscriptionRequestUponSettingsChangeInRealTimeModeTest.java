package com.zodiac.ams.charter.tests.callerId.subscription.realTimeMode;

import com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;

import static com.zodiac.ams.charter.bd.ams.tables.CharterSubscriptionUtils.getAllFromSubscriptions;
import static com.zodiac.ams.charter.http.helpers.settings.SettingsPostHelper.sendPostSettingsCallerIdNotification;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendPostSubscriptionRequestUponSettingsChangeInRealTimeModeTest extends StepsForCallerIdRealTimeSubscriptionMode {
    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.SUBSCRIBE + StoriesList.REAL_TIME_SUBSCRIPTION)
    @Description("3.3.1 Sent POST request for STB subscription after turning on 'Caller ID Notification' setting")
    @TestCaseId("12345")
    @Test()
    public void sendSubscriptionUponSettingsChange() {
        turnOffCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOn();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_1, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        createAttachment(readAMSLog());
        assertEquals(callerIdHttpDataUtils.getCallerIdSubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_1));
        assertEquals(getAllFromSubscriptions(),NO_DATA_IN_DB);
    }

    @Features(FeatureList.CALLER_ID)
    @Stories(StoriesList.POSITIVE + StoriesList.UNSUBSCRIBE + StoriesList.REAL_TIME_SUBSCRIPTION)
    @Description("3.3.3 Sent POST request for STB unsubscription after turning off 'Caller ID Notification' setting")
    @TestCaseId("12345")
    @Test()
    public void sendUnsubscriptionUponSettingsChange() {
        turnOnCallerIdNotification();
        ArrayList<SettingsOption> options = setCallerIdNotificationOff();
        assertEquals(sendPostSettingsCallerIdNotification(DEVICE_ID_NUMBER_2, options), SUCCESS_200);
        assertJsonEquals(jsonDataUtils.patternSuccessResponse(options), SettingsPostHelper.responseJson);
        createAttachment(readAMSLog());
        assertEquals(callerIdHttpDataUtils.getCallerIdUnsubscriptionUriPattern(), callerIdCHEEmulator.getURI());
        assertEquals(callerIdCHEEmulator.getSubscribeRequestBody(), stringDataUtils.patternSubscribeRequestBody(DEVICE_ID_NUMBER_2));
        assertEquals(getAllFromSubscriptions(),NO_DATA_IN_DB);
    }

}
