package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_1_STRING;
import static java.lang.Thread.sleep;

public class ConnectionModeNotificationVersion1ConnectionModeTest extends DsgBeforeAfter {


    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> ISN'T specified, " +
            "UNKNOWN connection mode, receive ERROR_VERSION3_PROTOCOL CODE = 1 ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test()
    public void sendVersion1RequestId0ConnectionModeUNKNOWN() {
        int unknownConnectionMode = 9;
        message = version1Request.createMessage(mac, unknownConnectionMode);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> ISN'T specified, " +
            "connection mode changes, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(dataProvider = "connectionMode", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendversion1RequestId0ConnectionModeIs(int connectionModeOldValue, int connectionModeNewValue) {

        message = version1Request.createMessage(mac, connectionModeOldValue);
        version1Request.sendAndParseRequest(message);
        message = version1Request.createMessage(mac, connectionModeNewValue);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + ERROR + IN_DSG_RESPONSE);
      //  assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), getModelId(UNKNOWN), mac);
        assertJson(mac, connectionModeNewValue);
      //  assertEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
        assertLog(log, mac, msgType, connectionModeOldValue);
        assertLog(log, mac, msgType, connectionModeNewValue);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified, " +
            "connection mode changes, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(dataProvider = "connectionMode", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendversion1RequestId0VendorIsSpecifiedConnectionModeIs(int connectionModeOldValue, int connectionModeNewValue) {

        message = version1Request.createMessage(mac, connectionModeOldValue, boxModel, vendor);
        version1Request.sendAndParseRequest(message);
        message = version1Request.createMessage(mac, connectionModeNewValue, boxModel, vendor);
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertJson(mac,connectionModeNewValue, getBoxModel(), vendor);
      //  assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
     //   assertEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
        assertLog(log, mac, msgType, connectionModeOldValue);
        assertLog(log, mac, msgType, connectionModeNewValue);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> ISN'T specified, " +
            "connection mode doesn't change, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(priority = 1)
    public void sendversion1RequestId0ConnectionModeDoesNotChange() {
        message = version1Request.createMessage(mac, connectionMode);
        version1Request.sendAndParseRequest(message);
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertJson(mac, connectionMode);
    //    assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), getModelId(UNKNOWN), mac);
      //  assertNotEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
        assertLog(log, mac, msgType, connectionMode);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_1_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified, " +
            "connection mode doesn't change, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(priority = 1)
    public void sendversion1RequestId0VendorIsSpecifiedConnectionModeDoesNotChange() {
        message = version1Request.createMessage(mac, connectionMode, boxModel, vendor);
        version1Request.sendAndParseRequest(message);
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> stbResponse = version1Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertJson(mac,connectionMode, getBoxModel(), vendor);
     //   assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
     //   assertNotEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
        assertLog(log, mac, msgType, connectionMode);
    }

}
