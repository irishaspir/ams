package com.zodiac.ams.charter.tests.lookup;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static com.zodiac.ams.charter.http.helpers.lookup.LookupGetHelper.*;
import static com.zodiac.ams.charter.http.helpers.lookup.LookupJsonDataUtils.nullResponse;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.testng.Assert.assertEquals;

public class SendGetSTBLookupRequestCHEisUnavailableTest extends StepsForLookup {

    @Features(FeatureList.STB_LOOKUP)
    @Stories(StoriesList.NEGATIVE + StoriesList.STB_LOOKUP_NEGATIVE)
    @Description("14.2.1 Send GET STB LOOKUP REQUEST, CHE is unavailable")
    @TestCaseId("167190")
    @Test()
    public void sendGetStbLookupRequestCHEIsUnavailable() {
        getTestTime();
        assertEquals(sendGetStbStatusRequestMac(DEVICE_ID_NUMBER_1), ERROR_500);
        createAttachment(readAMSLog());
        assertJsonEquals(responseJson, nullResponse());
    }
}
