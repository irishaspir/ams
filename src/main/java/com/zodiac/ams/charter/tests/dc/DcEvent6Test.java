package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_SIX;

/**
 * The tests suite for DC event 5.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcEvent6Test extends StepsForDcEvent {
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "1 - Check positive externalHddCapacity")
    @Description("4.6.1 [DC][Event 6] Check positive externalHddCapacity" +
            "<Node ID><Hub ID><5><2> " +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10| 3455620 | 30| 40 >}] ")
    public void sendMessageWithPositiveExternalHddCapacity1() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 3455620;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "1 - Check positive externalHddCapacity")
    @Description("4.6.1 [DC][Event 6] Check positive externalHddCapacity" +
            "<Node ID><Hub ID><5><2> " +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10 | 0 | 30| 40 >}]")
    public void sendMessageWithPositiveExternalHddCapacity2() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 0;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "1 - Check positive externalHddCapacity")
    @Description("4.6.1 [DC][Event 6] Check positive externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10 | 20 | 30| 40 >}]")
    public void sendMessageWithPositiveExternalHddCapacity3() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "2 - Check positive externalHddFreeSpacePercent")
    @Description("4.6.2 [DC][Event 6] Check positive externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10| 20 | 30 | 343245354 >}]")
    public void sendMessageWithPositiveExternalHddFreeSpacePercent1() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 343245354;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "2 - Check positive externalHddFreeSpacePercent")
    @Description("4.6.2 [DC][Event 6] Check positive externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10 | 20 | 30 | 0 >}]")
    public void sendMessageWithPositiveExternalHddFreeSpacePercent2() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 0;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "2 - Check positive externalHddFreeSpacePercent")
    @Description("4.6.2 [DC][Event 6] Check positive externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2> " +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10 | 20 | 30 | 40 >}]")
    public void sendMessageWithPositiveExternalHddFreeSpacePercent3() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "3 - Check positive internalHddCapacity")
    @Description("4.6.3 [DC][Event 6] Check positive internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10| 20 | 30| 40 >}]")
    public void sendMessageWithPositiveInternalHddCapacity1() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "3 - Check positive internalHddCapacity")
    @Description("4.6.3 [DC][Event 6] Check positive internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 0 | 20 | 30| 40 >}]")
    public void sendMessageWithPositiveInternalHddCapacity2() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 0;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "3 - Check positive internalHddCapacity")
    @Description("4.6.3 [DC][Event 6] Check positive internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 34560 | 20 | 30| 40 >}]")
    public void sendMessageWithPositiveInternalHddCapacity3() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 34560;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "4 - Check positive internalHddFreeSpacePercent")
    @Description("4.6.4 [DC][Event 6] Check positive internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2> " +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10| 20 | 43566456 | 40 >}] ")
    public void sendMessageWithPositiveInternalHddFreeSpacePercent1() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 43566456;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "4 - Check positive internalHddFreeSpacePercent")
    @Description("4.6.4 [DC][Event 6] Check positive internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10 | 20 | 0 | 40 >}] ")
    public void sendMessageWithPositiveInternalHddFreeSpacePercent2() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 0;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_POSITIVE + "4 - Check positive internalHddFreeSpacePercent")
    @Description("4.6.4 [DC][Event 6] Check positive internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6>< 10 | 20 | 30 | 40 >}]")
    public void sendMessageWithPositiveInternalHddFreeSpacePercent3() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_SIX;
        // parameters
        final int parameterInternalHddCapacity = 10;
        final int parameterExternalHddCapacity = 20;
        final int parameterInternalHddFreeSpacePercent = 30;
        final int parameterExternalHddFreeSpacePercent = 40;

        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                // parameters
                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)
                //
                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterInternalHddCapacity)
                .addParameter(parameterExternalHddCapacity)
                .addParameter(parameterInternalHddFreeSpacePercent)
                .addParameter(parameterExternalHddFreeSpacePercent)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityHddStbCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "5 - Check negative externalHddCapacity")
    @Description("4.6.5 [DC][Event 6] Check negative externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|-23410|30|40>}]")
    public void sendMessageWithIncorrectExternalHddCapacity1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                // externalHddCapacity must be garbage, this is the test purpose!
                .addParameter(-23410) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "5 - Check negative externalHddCapacity")
    @Description("4.6.5 [DC][Event 6] Check negative externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|3454676678783423134|30|40>}]")
    public void sendMessageWithIncorrectExternalHddCapacity2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                // externalHddCapacity must be garbage, this is the test purpose!
                .addParameter("3454676678783423134") // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "5 - Check negative externalHddCapacity")
    @Description("4.6.5 [DC][Event 6] Check negative externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|dfsdfasdf|30|40>}]")
    public void sendMessageWithIncorrectExternalHddCapacity3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                // externalHddCapacity must be garbage, this is the test purpose!
                .addParameter("dfsdfasdf") // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "5 - Check negative externalHddCapacity")
    @Description("4.6.5 [DC][Event 6] Check negative externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10| #$%#$%|30|40>}]")
    public void sendMessageWithIncorrectExternalHddCapacity4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                // externalHddCapacity must be garbage, this is the test purpose!
                .addParameter(" #$%#$%") // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "5 - Check negative externalHddCapacity")
    @Description("4.6.5 [DC][Event 6] Check negative externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10| 5345.52345|30|40>}]")
    public void sendMessageWithIncorrectExternalHddCapacity5() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                // externalHddCapacity must be garbage, this is the test purpose!
                .addParameter("5345.52345") // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "5 - Check negative externalHddCapacity")
    @Description("4.6.5 [DC][Event 6] Check negative externalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10| 34534,345435|30|40>}]")
    public void sendMessageWithIncorrectExternalHddCapacity6() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                // externalHddCapacity must be garbage, this is the test purpose!
                .addParameter("34534,345435") // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "6 - Check negative externalHddFreeSpacePercent")
    @Description("4.6.6 [DC][Event 6] Check negative externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|30|-23410>}]")
    public void sendMessageWithIncorrectExternalHddFreeSpacePercent1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                // externalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("-23410") // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "6 - Check negative externalHddFreeSpacePercent")
    @Description("4.6.6 [DC][Event 6] Check negative externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|30|3454676678783423134>}]")
    public void sendMessageWithIncorrectExternalHddFreeSpacePercent2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                // externalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("3454676678783423134 ") // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "6 - Check negative externalHddFreeSpacePercent")
    @Description("4.6.6 [DC][Event 6] Check negative externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|30|dfsdfasdf>}]")
    public void sendMessageWithIncorrectExternalHddFreeSpacePercent3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                // externalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("dfsdfasdf") // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "6 - Check negative externalHddFreeSpacePercent")
    @Description("4.6.6 [DC][Event 6] Check negative externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|30|#$%#$%>}]")
    public void sendMessageWithIncorrectExternalHddFreeSpacePercent4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                // externalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("#$%#$%") // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "6 - Check negative externalHddFreeSpacePercent")
    @Description("4.6.6 [DC][Event 6] Check negative externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|30|35345.52345>}]")
    public void sendMessageWithIncorrectExternalHddFreeSpacePercent5() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                // externalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("35345.52345") // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "6 - Check negative externalHddFreeSpacePercent")
    @Description("4.6.6 [DC][Event 6] Check negative externalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|30|34534,345435>}]")
    public void sendMessageWithIncorrectExternalHddFreeSpacePercent6() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                // externalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("34534,345435") // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "7 - Check negative internalHddCapacity")
    @Description("4.6.7 [DC][Event 6] Check negative internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><-23410|20|30|40>}]")
    public void sendMessageWithIncorrectInternalHddCapacity1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                // internalHddCapacity must be garbage, this is the test purpose!
                .addParameter(-23410) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "7 - Check negative internalHddCapacity")
    @Description("4.6.7 [DC][Event 6] Check negative internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><3454676678783423134|20|30|40>}]")
    public void sendMessageWithIncorrectInternalHddCapacity2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                // internalHddCapacity must be garbage, this is the test purpose!
                .addParameter("3454676678783423134") // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "7 - Check negative internalHddCapacity")
    @Description("4.6.7 [DC][Event 6] Check negative internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><dfsdfasdf |20|30|40>}]")
    public void sendMessageWithIncorrectInternalHddCapacity3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                // internalHddCapacity must be garbage, this is the test purpose!
                .addParameter("dfsdfasdf ") // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "7 - Check negative internalHddCapacity")
    @Description("4.6.7 [DC][Event 6] Check negative internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><#$%#$% |20|30|40>}]")
    public void sendMessageWithIncorrectInternalHddCapacity4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                // internalHddCapacity must be garbage, this is the test purpose!
                .addParameter("#$%#$% ") // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "7 - Check negative internalHddCapacity")
    @Description("4.6.7 [DC][Event 6] Check negative internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><35345.52345|20|30|40>}]")
    public void sendMessageWithIncorrectInternalHddCapacity5() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                // internalHddCapacity must be garbage, this is the test purpose!
                .addParameter("35345.52345") // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "7 - Check negative internalHddCapacity")
    @Description("4.6.7 [DC][Event 6] Check negative internalHddCapacity" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><34534,345435|20|30|40>}]")
    public void sendMessageWithIncorrectInternalHddCapacity6() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                // internalHddCapacity must be garbage, this is the test purpose!
                .addParameter("34534,345435") // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                .addParameter(30) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "8 - Check negative internalHddFreeSpacePercent")
    @Description("4.6.8 [DC][Event 6] Check negative internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|-23410|40>}]")
    public void sendMessageWithIncorrectInternalHddFreeSpacePercent1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                // internalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter(-23410) // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "8 - Check negative internalHddFreeSpacePercent")
    @Description("4.6.8 [DC][Event 6] Check negative internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20| 3454676678783423134|40>}]")
    public void sendMessageWithIncorrectInternalHddFreeSpacePercent2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                // internalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter(" 3454676678783423134") // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "8 - Check negative internalHddFreeSpacePercent")
    @Description("4.6.8 [DC][Event 6] Check negative internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20| dfsdfasdf |40>}]")
    public void sendMessageWithIncorrectInternalHddFreeSpacePercent3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                // internalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("dfsdfasdf") // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "8 - Check negative internalHddFreeSpacePercent")
    @Description("4.6.8 [DC][Event 6] Check negative internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20| #$%#$% |40>}]")
    public void sendMessageWithIncorrectInternalHddFreeSpacePercent4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                // internalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("#$%#$%") // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "8 - Check negative internalHddFreeSpacePercent")
    @Description("4.6.8 [DC][Event 6] Check negative internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20| 35345.52345 | 40>}]")
    public void sendMessageWithIncorrectInternalHddFreeSpacePercent5() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                // internalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("35345.52345") // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><internalHddCapacity | externalHddCapacity | internalHddFreeSpacePercent | externalHddFreeSpacePercent>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_6_NEGATIVE + "8 - Check negative internalHddFreeSpacePercent")
    @Description("4.6.8 [DC][Event 6] Check negative internalHddFreeSpacePercent" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><14>" +
            "{<Time delta><89><6><10|20|34534,345435|40>}]")
    public void sendMessageWithIncorrectInternalHddFreeSpacePercent6() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_SIX)
                .addParameter(10) // internalHddCapacity
                .addParameter(20) // externalHddCapacity
                // internalHddFreeSpacePercent must be garbage, this is the test purpose!
                .addParameter("34534,345435") // internalHddFreeSpacePercent
                .addParameter(40) // externalHddFreeSpacePercent
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }
}
