package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tomcat.logger.dsg.DsgLogParser;
import com.zodiac.ams.common.logging.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Arrays;
import java.util.List;

import static com.zodiac.ams.charter.runner.TestRunner.AMS_VERSION;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;
import static com.zodiac.ams.charter.tomcat.logger.dsg.DsgLogKeyWords.*;
import static org.testng.Assert.assertTrue;

/**
 * Created by SpiridonovaIM on 13.06.2017.
 */
public class LogSteps extends ConnectionModeSteps {

    private DsgLogParser logParser = new DsgLogParser();

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, String serialNumber, Object cmIp, Object swVersionTmp) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup, loadBalancerIp, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, cmIp);
        assertTrue(logParser.isSwVersionTmpInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(swVersionTmp)),
                ERROR + String.format(SW_VERSION_TMP_PATTERN, cmIp) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, String serialNumber, Object cmIp) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup, loadBalancerIp, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber);
        assertTrue(logParser.isCmIpRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(cmIp)),
                ERROR + String.format(CM_IP_PATTERN, cmIp) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, String serialNumber) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup, loadBalancerIp, swVersion, sdvServiceGroupId, cmMacAddress);
        assertTrue(logParser.isSerialNumberRowInAMSLog(log, getDeviceIdByMac(mac), serialNumber),
                ERROR + String.format(SERIAL_NUMBER_PATTERN, serialNumber) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object cmMacAddress) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup, loadBalancerIp, swVersion, sdvServiceGroupId);
        assertTrue(logParser.isCmMacAddressRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(cmMacAddress).toLowerCase()),
                ERROR + String.format(CM_MAC_ADDRESS_PATTERN, cmMacAddress) + NOT_IN_LOG);
    }


    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup, loadBalancerIp, swVersion);
        assertTrue(logParser.isSdvServiceGroupIdRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(sdvServiceGroupId)),
                ERROR + String.format(SDV_SERVICE_GROUP_ID_PATTERN, sdvServiceGroupId) + NOT_IN_LOG);
    }


    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp, Object swVersion) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup, loadBalancerIp);
        String pattern = (AMS_VERSION < 390) ? SW_VERSION_PATTERN : NEW_SW_VERSION_PATTERN;
        assertTrue(logParser.isSwVersionRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(swVersion)),
                ERROR + String.format(pattern, swVersion) + NOT_IN_LOG);
    }

//    @Step
//    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup, Object loadBalancerIp) {
//        assertLog(log, mac, connectionMode, error, hubId, serviceGroup);
//        assertTrue(logParser.isLoadBalancerIpDRowInAMSLog(log, getDeviceIdByMac(mac), loadBalancerIp),
//                ERROR + String.format(LOAD_BALANCER_IP_PATTERN, loadBalancerIp) + NOT_IN_LOG);
//    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup,
                          Object loadBalancerIp) {
        assertLog(log, mac, connectionMode, error, hubId, serviceGroup);
        assertTrue(logParser.isLoadBalancerIpDRowInAMSLogLB(log, getDeviceIdByMac(mac), loadBalancerIp),
                ERROR + String.format(LOAD_BALANCER_IP_PATTERN, loadBalancerIp) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId, int serviceGroup) {
        assertLog(log, mac, connectionMode, error, hubId);
        assertTrue(logParser.isServiceGroupIDRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(serviceGroup)),
                ERROR + String.format(SERVICE_GROUP_ID_PATTERN, serviceGroup) + NOT_IN_LOG);
        assertTrue(logParser.isTSIDRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(serviceGroup)),
                ERROR + String.format(TS_ID_PATTERN, serviceGroup) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, Object hubId) {
        assertLogWithVersion2Default(log, mac, connectionMode, error);
        assertTrue(logParser.isHubIDRowInAMSLog(log, getDeviceIdByMac(mac), hubId),
                ERROR + String.format(HUB_ID, hubId) + NOT_IN_LOG);
    }


    //Version2
    @Step
    public void assertLogWithVersion2Default(List<String> log, String mac, int connectionMode, String error) {
        assertLog(log, mac, connectionMode, error, migrationStart);
    }

    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, String error, int migrationStart) {
        assertLogWithVersion1Default(log, mac, connectionMode);
        assertTrue(logParser.isMigrationStartRowInAMSLog(log, getDeviceIdByMac(mac), migrationStart),
                ERROR + String.format(MIGRATION_START_PATTERN, migrationStart) + NOT_IN_LOG);
        if (error.length() > 0)
            assertTrue(logParser.isPWUPWBInAMSLog(log, getDeviceIdByMac(mac), error),
                    ERROR + ERROR_VERSION3_PROTOCOL + NOT_IN_LOG);
    }


    //Version1
    @Step
    public void assertLog(List<String> log, String mac, int connectionMode, long msgTypesMap) {
        assertLogWithVersion0Default(log, mac, connectionMode);
        assertTrue(logParser.isMessageTypeMapRowInAMSLog(log, getDeviceIdByMac(mac), hexString(Long.toHexString(msgTypesMap))),
                ERROR + String.format(MSG_TYPES_MAP_PATTERN, Long.toHexString(msgTypesMap)) + NOT_IN_LOG);

    }

    @Step
    private void assertLogWithVersion1Default(List<String> log, String mac, int connectionMode) {
        assertLog(log, mac, connectionMode, messageTypesMap);
    }

    //Version0
    @Step
    public void assertLogWithVersion0Default(List<String> log, String mac, int connectionMode) {
        assertLog(log, mac, msgType, connectionMode, boxModel, vendor);
    }

    @Step
    public void assertLog(List<String> log, long mac, int msgType, int connectionMode, int boxModel, int vendorId) {
        assertLog(log, String.valueOf(mac), msgType, connectionMode, boxModel, vendorId);
    }

    @Step
    public void assertLog(String error, List<String> log, long mac, int msgType, int connectionMode, int boxModel, int vendorId) {
        assertLog(log, String.valueOf(mac), msgType, connectionMode, boxModel, vendorId);
        assertTrue(logParser.isInvalidCacheLoaderExceptionInLog(log, String.valueOf(mac)),
                ERROR + String.format(error, mac) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int msgType, int connectionMode, int boxModel, int vendorId) {
        assertLog(log, mac, msgType, connectionMode, boxModel);
        assertTrue(logParser.isVendorIdRowInAMSLog(log, getDeviceIdByMac(mac), vendorId),
                ERROR + String.format(VENDOR_ID_PATTERN, vendorId) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int msgType, int connectionMode, int boxModel) {
        assertLog(log, mac, msgType, connectionMode);
        assertTrue(logParser.isBoxModelRomIdRowInAMSLog(log, getDeviceIdByMac(mac), String.valueOf(boxModel)),
                ERROR + String.format(BOX_MODEL_ROM_ID_PATTERN, boxModel) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac, int msgType, int connectionMode) {
        assertLog(log, mac, msgType);
        assertTrue(logParser.isConnectionModeIntRowInAMSLog(log, getDeviceIdByMac(mac), connectionMode),
                ERROR + String.format(CONNECTION_MODE_INT_PATTERN, connectionMode) + NOT_IN_LOG);
    }


    @Step
    public void assertLog(List<String> log, String mac, int msgType) {
        Logger.info("=======================");
//        assertTrue(logParser.isMessageTypeRowInAMSLog(log, getDeviceIdByMac(mac), msgType),
//                ERROR + String.format(MSG_TYPE_PATTERN, msgType) + NOT_IN_LOG);
    }

    @Step
    public void assertLog(List<String> log, String mac) {
        Logger.info("=======================");
        assertTrue(logParser.isMessageTypeRowInAMSLog(log, getDeviceIdByMac(mac), msgType),
                ERROR + String.format(MSG_TYPE_PATTERN, msgType) + NOT_IN_LOG);
    }

    private String hexString(String mTM) {
        char[] pattern = "0000000000000000".toCharArray();
        char[] array = mTM.toCharArray();
        int j = mTM.length() - 1;
        for (int i = pattern.length - 1; i >= 0; i--) {
            if (j >= 0) {
                pattern[i] = array[j];
                j--;
            }
        }
        return Arrays.toString(pattern);
    }

}
