package com.zodiac.ams.charter.tests.ft.talkingguide;

import com.zodiac.ams.common.http.FTHttpUtils;
import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.charter.services.ft.talkingguide.TGUtils;
import com.zodiac.ams.charter.services.ft.talkingguide.TalkingGuideItem;
import com.zodiac.ams.charter.services.ft.talkingguide.TalkingGuideMockServer;
import java.util.Arrays;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;

public class TalkingGuideTestImpl extends FuncTest{
    
    protected static final String DATA_DIR = "dataTalkingGuide";
    protected static final String DEFAULT_CODING = "pcm16_8";
    protected static final String REMOTE_CACHE_DIR = "TalkingGuideCache";
    
    protected static final long MAX_TIMEOUT_POSITIVE = 5;
    protected static final long MAX_TIMEOUT_NEGATIVE = 1;
    
    protected String getRemoteCacheDir(){
        return executor.getHomeDir()+"/"+REMOTE_CACHE_DIR;
    }
    
    
    protected void createRemoteCacheDir(){
        String home = executor.getHomeDir();
        executor.exec("cd "+home+"; rm -rf "+REMOTE_CACHE_DIR+"; mkdir "+REMOTE_CACHE_DIR+"; chmod a=rwx "+REMOTE_CACHE_DIR);
    } 
    
    protected static String getName(String name){
        return getFileFromJar(DATA_DIR,name);
    }
    
    protected String getURI(String body){
        return getURI(DEFAULT_CODING,body);
    }
            
    protected String getURI(String coding,String body){
        return getURI(coding,body,properties.getDeviceId());
    }
    
    protected String getURI(String coding,String text,String mac){
        return properties.getTalkingGuideUri()+"?coding="+coding+"&body="+text+"&mac="+mac;
    }
    
    @Attachment(value = "STB to AMS request", type ="text/plain")
    protected String getSTBtoAMSRequest(String uri,FTHttpUtils.HttpResult result){
        return "URI: "+uri+" Response:"+result.getStatus();
    }
    
    
    @Attachment(value = "AMS to TTS request", type ="text/plain")
    protected String getAMStoTTSRequest(String request,String text,String expected){
        String result="TTS: "+properties.getLocalHost()+":"+properties.getTTSPort()+"\nREQUEST:\n"+request;
        if (null != text)
            result+="\n\n\nTEXT: "+text;
        if (null != expected)
            result+="\nEXPECTED TEXT: "+expected;
        return result; 
    }
    
    protected class GetExecutor extends Thread{
        private String uri;
        private FTHttpUtils.HttpResult response;
        
        GetExecutor(String uri){
            this.uri = uri;
        }
        
        @Override
        public void run(){
            try{
                response=FTHttpUtils.doGet(uri,false);
            }
            catch(Exception ex){
                logger.error(ex.getMessage());
            }
        }
        
        public FTHttpUtils.HttpResult getResponse(){
            return response;
        }
    }
    
    
    protected void negativeTestImpl(String text,String soundName,String confName,int expectedResponse) throws Exception{
        testImpl(text,soundName,confName,true,expectedResponse);
    }
    
    protected void negativeTestImpl(TalkingGuideMockServer server,String text,String soundName,String confName,int expectedResponse) throws Exception{
        testImpl(server,text,soundName,confName,true,expectedResponse);
    }
    
    protected void positiveTestImpl(String text,String soundName,String confName) throws Exception{
        testImpl(text,soundName,confName,false,200);
    }
    
    protected void positiveTestImpl(TalkingGuideMockServer server,String text,String soundName,String confName) throws Exception{
        testImpl(server,text,soundName,confName,false,200);
    }
    
    protected void testImpl(String text,String soundName,String confName,boolean raiseError,int expectedResponse) throws Exception{
        TalkingGuideMockServer server = null;
        try {
            
            server=new TalkingGuideMockServer(Integer.parseInt(properties.getTTSPort()));
            server.start();
            
            testImpl(server, text, soundName, confName, raiseError, expectedResponse);
        }
        finally{
            if (null != server)
                server.shutdown();
        }
    }
    
    protected void testImpl(TalkingGuideMockServer server,String text,String soundName,String confName,boolean raiseError,int expectedResponse) throws Exception{
        TalkingGuideItem item = new TalkingGuideItem(soundName);
        item.setError(raiseError);
        
        String uri = getURI(TGUtils.parseHTTPRequestString(text));
        String expected = TGUtils.parseString(text, confName);
        
        GetExecutor thread=new GetExecutor(uri);
        thread.start();
           
        boolean b=server.addTalkingGuideItem(item,MAX_TIMEOUT_POSITIVE,TimeUnit.MINUTES);
        Assert.assertTrue(b,"No request from AMS to Talking Guide mock server");
        
        
        Future<?> future=item.getFuture();
        try{
            future.get(MAX_TIMEOUT_POSITIVE,TimeUnit.MINUTES);
        }
        catch(TimeoutException ex){
            Assert.assertTrue(false,"Unable to get result from Talking Guide mock server");
        }
        thread.join(MAX_TIMEOUT_POSITIVE*60*1000);
        
        FTHttpUtils.HttpResult response = thread.getResponse();
        
        getSTBtoAMSRequest(uri,response);
        getAMStoTTSRequest(item.toString(),item.getText(),expected);
        
        Assert.assertTrue(expected.equals(item.getText()),"Invalid body");
        if (0 != expectedResponse)
            Assert.assertTrue(expectedResponse == thread.getResponse().getStatus(),"Invalid response");
        if (!raiseError)
            Assert.assertTrue(Arrays.equals(item.getSound(), (byte[])response.getEntity()), "The real and expected sounds are not equals");
    }
}
