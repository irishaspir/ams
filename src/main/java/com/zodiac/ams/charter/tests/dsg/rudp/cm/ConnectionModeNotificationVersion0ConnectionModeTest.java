package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;

import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils.getSTBLastActivityLastTime;
import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;

import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.getModeChangeTimeStampInDateFormat;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_0_STRING;
import static java.lang.Thread.sleep;
import static org.testng.Assert.assertEquals;

public class ConnectionModeNotificationVersion0ConnectionModeTest extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.NEGATIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified, " +
            "UNKNOWN connection mode, receive ERROR_VERSION3_PROTOCOL CODE = 1 ")
    @TestCaseId("123")
    @Issue("AMSTS17-41")
    @Test()
    public void sendVersion0RequestId0ConnectionModeIsUNKNOWN() {
        int unknownConnectionMode = 9;
        message = version0Request.createMessage(mac, unknownConnectionMode);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        createAttachment(readAMSLog());
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified, " +
            "connection mode changes, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(dataProvider = "connectionMode", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion0RequestId0ConnectionModeIs(int connectionModeOldValue, int connectionModeNewValue) {
        message = version0Request.createMessage(mac, connectionModeOldValue);
        version0Request.sendAndParseRequest(message);
        message = version0Request.createMessage(mac, connectionModeNewValue);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertJson(mac, connectionModeNewValue);
     //   assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), getModelId(UNKNOWN), mac);
        assertEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
        //  assertLog(log, mac, msgType, connectionModeOldValue);
        assertLog(log, mac, msgType, connectionModeNewValue);

    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> ISN'T specified, " +
            "connection mode doesn't change, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(priority = 1)
    public void sendVersion0RequestId0ConnectionModeDoesNotChange() {
        connectionMode = changeConnectionMode();
        message = version0Request.createMessage(mac, connectionMode);
        version0Request.sendAndParseRequest(message);
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertJson(mac, connectionMode);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertNotEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
        assertLog(log, mac, msgType, connectionMode);
      //  assertDB(getModelId(UNKNOWN), getVendorId(UNKNOWN), getModelId(UNKNOWN), mac);
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> IS specified, " +
            "connection mode changes, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(dataProvider = "connectionMode", dataProviderClass = DSGDataProvider.class, priority = 2)
    public void sendVersion0RequestId0VendorIsSpecifiedConnectionIs(int connectionModeOldValue, int connectionModeNewValue) {
        message = version0Request.createMessage(mac, connectionModeOldValue, boxModel, vendor);
        version0Request.sendAndParseRequest(message);
        message = version0Request.createMessage(mac, connectionModeNewValue, boxModel, vendor);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertJson(mac, connectionModeNewValue, getBoxModel(), vendor);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
    //    assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
        assertLog(log, mac, msgType, connectionModeNewValue);
     //   assertEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
    }


    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_0_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.1 Message from STB to AMS where <request id>=0, 'messageType' = 0 AND <vendor> IS specified, " +
            "connection mode doesn't change, receive ERROR_VERSION3_PROTOCOL CODE = 0")
    @TestCaseId("167772")
    @Test(priority = 1)
    public void sendVersion0RequestId0VendorIsSpecifiedConnectionModeDoesNotChange() {
        connectionMode = changeConnectionMode();
        message = version0Request.createMessage(mac, connectionMode, boxModel, vendor);
        version0Request.sendAndParseRequest(message);
        List<String> stbResponse = version0Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertJson(mac, connectionMode, getBoxModel(),vendor);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.successResponse(mac), ERROR + IN_DSG_RESPONSE);
        assertLog(log, mac, msgType, connectionMode);
    //    assertDB(boxModel, vendor, getModelId(UNKNOWN), mac);
   //     assertNotEquals(getModeChangeTimeStampInDateFormat(mac), getSTBLastActivityLastTime(mac), CHANGE_TIME_STAMP + LAST_ACTIVITY_STAMP + EQUAL);
    }
}
