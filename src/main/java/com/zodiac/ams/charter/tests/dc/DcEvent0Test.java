package com.zodiac.ams.charter.tests.dc;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.dc.message.LocalMessageBuilder;
import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import com.zodiac.ams.charter.tests.dc.assertion.csv.file.CsvFileAssertionInput;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.zodiac.ams.charter.tests.dc.common.EventId.EVENT_ID_ZERO;

/**
 * The tests suite for DC Event 0.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcEvent0Test extends StepsForDcEvent {
    
    @Override
    protected boolean needAmsRestart(){
        return true;
    }
    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test(priority=0)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><0><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057|0|50>}]")
    public void sendMessageWithPositiveReasons1() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 0;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 10;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 0;
        final int parameterSignalLevel = 50;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 286;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;
        

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion
        //Thread.sleep(60*60*1000);

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test(priority=0)
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><2>" +
            "{<Time delta><43><0><10057|1|33>}]")
    public void sendMessageWithPositiveReasons2() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 2;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 1;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 43;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;
        
        //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //Thread.sleep(20_000);

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
        //Thread.sleep(60* 60 * 1_000);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><55><2>" +
            "[<Session start timestamp><1><1><4>" +
            "{<Time delta><54><0><10057|2|1>}]")
    public void sendMessageWithPositiveReasons3() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 55;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 4;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 2;
        final int parameterSignalLevel = 1;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 54;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><6>" +
            "{<Time delta><66><0><10057|3|100>}]")
    public void sendMessageWithPositiveReasons4() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 6;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 3;
        final int parameterSignalLevel = 100;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 66;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><65>" +
            "{<Time delta><78><0><10057|4|99>}]")
    public void sendMessageWithPositiveReasons5() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 65;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 4;
        final int parameterSignalLevel = 99;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 78;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><1>" +
            "{<Time delta><66><0><10057|5|33>}]")
    public void sendMessageWithPositiveReasons6() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 1;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 5;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 66;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><8>" +
            "{<Time delta><5><0><10057|6|33>}]")
    public void sendMessageWithPositiveReasons7() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 8;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 6;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 5;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><5>" +
            "{<Time delta><4><0><10057|7|33>}]")
    public void sendMessageWithPositiveReasons8() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 5;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 7;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 4;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><5>" +
            "{<Time delta><56><0><10057|8|33>}]")
    public void sendMessageWithPositiveReasons9() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 5;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 8;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 56;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><46>" +
            "{<Time delta><77><0><10057|9|33>}]")
    public void sendMessageWithPositiveReasons10() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 46;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 9;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 77;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><6>" +
            "{<Time delta><6><0><10057|10|33>}]")
    public void sendMessageWithPositiveReasons11() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 6;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 10;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 6;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV files parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_POSITIVE + "1 - Check positive reasons")
    @Description("4.0.1 [DC][Event 0] Check positive reasons" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><14>" +
            "{<Time delta><89><0><10057|11|33>}]")
    public void sendMessageWithPositiveReasons12() throws Exception {
        final int hubId = HUB_ID;
        final int maxRebootsPerDay = 5;
        final int nodeId = NODE_ID;
        final int sessionsNumber = 2;
        final int eventsNumber = 14;
        final int eventId = EVENT_ID_ZERO;
        // parameters
        final int parameterDestinationChannelNumber = 10057;
        final int parameterReason = 11;
        final int parameterSignalLevel = 33;
        //
        final int timeDelta = TIME_DELTA;
        final int timestampPartMsec = 89;
        final int firstChannelNumber = 1;
        final int firstEventNumber = 1;
        final int sessionStartTimestamp = 1484729890;

        // message sending and assertion
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseCorrectFormat(message);
        // end of message sending and assertion

        // CSV files parsing and assertion
        CsvFileAssertionInput csvFileAssertionInput = createCsvFileAssertionInputBuilder()
                .setHubId(hubId)
                .setMaxRebootsPerDay(maxRebootsPerDay)
                .setNodeId(nodeId)
                .setSessionsNumber(sessionsNumber)

                .startSession()
                .setEventsNumber(eventsNumber)

                .startEvent()
                .setEventId(eventId)

                .addParameter(parameterDestinationChannelNumber)
                .addParameter(parameterReason)
                .addParameter(parameterSignalLevel)

                .setTimeDelta(timeDelta)
                .setTimestampPartMsec(timestampPartMsec)
                .endEvent(eventsNumber)

                .setFirstChannelNumber(firstChannelNumber)
                .setFirstEventNumber(firstEventNumber)
                .setSessionStartTimestamp(sessionStartTimestamp)
                .endSession(sessionsNumber)

                .build();

        parseAssertAndDeleteActivityStbTuningCsvFile(csvFileAssertionInput);
        // end of CSV filesZ parsing and assertion
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "2 - Check without reason")
    @Description("4.0.2 [DC][Event 0] Check without reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057||50>}]")
    public void sendMessageWithoutReason() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                // reason must be null, this is the test purpose!
                .addParameter(null) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "3 - Check with incorrect reason")
    @Description("4.0.3 [DC][Event 0] Check with incorrect reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057| a |50>}]")
    public void sendMessageWithIncorrectReason1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                // reason must be a garbage string, this is the test purpose!
                .addParameter("a") //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "3 - Check with incorrect reason")
    @Description("4.0.3 [DC][Event 0] Check with incorrect reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057| $ |50>}]")
    public void sendMessageWithIncorrectReason2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                // reason must be a garbage string, this is the test purpose!
                .addParameter("$") //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "3 - Check with incorrect reason")
    @Description("4.0.3 [DC][Event 0] Check with incorrect reason" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057| 194673 |50>}]")
    public void sendMessageWithIncorrectReason3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                // reason must be a garbage, this is the test purpose!
                .addParameter(194673) //reason
                .addParameter(50)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "4 - Check without destinationChannelNumber")
    @Description("4.0.4 [DC][Event 0] Check without destinationChannelNumber" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><14>" +
            "{<Time delta><89><0><|11|33>}]")
    public void sendMessageWithoutDestinationChannelNumber() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                // destinationChannelNumber must be null, this is the test purpose!
                .addParameter(null) // destinationChannelNumber
                .addParameter(11) //reason
                .addParameter(33)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "5 - Check with incorrect destinationChannelNumber")
    @Description("4.0.5 [DC][Event 0] Check with incorrect destinationChannelNumber" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><14>" +
            "{<Time delta><89><0><34566456|11|33>}]")
    public void sendMessageWithIncorrectDestinationChannelNumber1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                // destinationChannelNumber must be a garbage, this is the test purpose!
                .addParameter(34566456) // destinationChannelNumber
                .addParameter(11) //reason
                .addParameter(33)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "5 - Check with incorrect destinationChannelNumber")
    @Description("4.0.5 [DC][Event 0] Check with incorrect destinationChannelNumber" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><14>" +
            "{<Time delta><89><0><retyrety|11|33>}]")
    public void sendMessageWithIncorrectDestinationChannelNumber2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                // destinationChannelNumber must be a garbage string, this is the test purpose!
                .addParameter("retyrety") // destinationChannelNumber
                .addParameter(11) //reason
                .addParameter(33)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "5 - Check with incorrect destinationChannelNumber")
    @Description("4.0.5 [DC][Event 0] Check with incorrect destinationChannelNumber" +
            "<Node ID><Hub ID><5><2>" +
            "[<Session start timestamp><1><1><14>" +
            "{<Time delta><89><0><#%^^#%^|11|33>}]")
    public void sendMessageWithIncorrectDestinationChannelNumber3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(14)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                // destinationChannelNumber must be a garbage string, this is the test purpose!
                .addParameter("#%^^#%^") // destinationChannelNumber
                .addParameter(11) //reason
                .addParameter(33)  //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(89)
                .endEvent(14)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "7 - Check with no signalLevel")
    @Description("4.0.7 [DC][Event 0] Check with no signalLevel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057 | 194673 |>}]")
    public void sendMessageWithoutSignalLevel() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(194673) //reason
                // signalLevel must be null, this is the test purpose!
                .addParameter(null) //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "6 - Check with incorrect signalLevel")
    @Description("4.0.6 [DC][Event 0] Check with incorrect signalLevel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057 | 194673 |-245>}]")
    public void sendMessageWithIncorrectSignalLevel1() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(194673) //reason
                // signalLevel must be a garbage, this is the test purpose!
                .addParameter(-245) //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "6 - Check with incorrect signalLevel")
    @Description("4.0.6 [DC][Event 0] Check with incorrect signalLevel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057 | 194673 |fdsf>}]")
    public void sendMessageWithIncorrectSignalLevel2() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(194673) //reason
                // signalLevel must be a garbage string, this is the test purpose!
                .addParameter("fdsf") //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "6 - Check with incorrect signalLevel")
    @Description("4.0.6 [DC][Event 0] Check with incorrect signalLevel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057 | 194673 | >}]")
    public void sendMessageWithIncorrectSignalLevel3() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(194673) //reason
                // signalLevel must be a garbage string, this is the test purpose!
                .addParameter(" ") //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

    /**
     * <Node ID><Hub ID><Max Reboots Per Day><Sessions #>
     * [<Session start timestamp><First channel number><First event number><Events number>
     * {<Time delta><timestampPartMsec><Event ID><destinationChannelNumber | reason | signalLevel>}]
     */
    @Test
    @Features(FeatureList.DC_HEADER_NAME)
    @Stories(StoriesList.DC_EVENT_0_NEGATIVE + "6 - Check with incorrect signalLevel")
    @Description("4.0.6 [DC][Event 0] Check with incorrect signalLevel" +
            "<Node ID><Hub ID><5><2>" +
            "[<1484729890><1><1><10>" +
            "{<Time delta><286><0><10057 | 194673 |#$%%%>}]")
    public void sendMessageWithIncorrectSignalLevel4() throws Exception {
        LocalMessageBuilder messageBuilder = createLocalMessageBuilder()
                .setHubId(HUB_ID)
                .setMaxRebootsPerDay(5)
                .setNodeId(NODE_ID)
                .setSessionsNumber(2)

                .startSession()
                .setEventsNumber(10)

                .startEvent()
                .setEventId(EVENT_ID_ZERO)
                .addParameter(10057) // destinationChannelNumber
                .addParameter(194673) //reason
                // signalLevel must be a garbage string, this is the test purpose!
                .addParameter("#$%%%") //signalLevel
                .setTimeDelta(TIME_DELTA)
                .setTimestampPartMsec(286)
                .endEvent(10)

                .setFirstChannelNumber(1)
                .setFirstEventNumber(1)
                .setSessionStartTimestamp(1484729890)
                .endSession(2);

        ZodiacMessage message = messageBuilder.build();
        sendMessageAndAssertResponseIncorrectFormat(message);
    }

}
