package com.zodiac.ams.charter.tests.dsg.rudp.cm;

import com.zodiac.ams.charter.tests.FeatureList;
import com.zodiac.ams.charter.tests.StoriesList;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.zodiac.ams.charter.helpers.AssertBetweenRange.assertBetweenAllowableRange;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.messageTypesMap;
import static com.zodiac.ams.charter.tests.StoriesList.DSG_CONNECTION_MODE_NOTIFICATION;
import static com.zodiac.ams.charter.tests.StoriesList.VERSION_3_STRING;

/**
 * Created by SpiridonovaIM on 26.06.2017.
 */
public class ConnectionModeNotificationVersion3MessageTypesMapTests extends DsgBeforeAfter {

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified" +
            "messageTypesMap changes")
    @Test(dataProvider = "messageTypesMap", dataProviderClass = DSGDataProvider.class)
    public void sendVersion3RequestId0VendorIsSpecifiedMessageTypesMapSettings(long messageTypesMap) {
        String mac = MAC_NUMBER_4;
        message = version3Request.createMessage(mac, messageTypesMap);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(messageTypesMap, mac);
    //    assertJson(mac, connectionMode,getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, messageTypesMap);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified" +
            "messageTypesMap unknown")
    @Test()
    public void sendVersion3RequestId0VendorIsSpecifiedRomIdMessageTypesMapUnknown() {
        String mac = MAC_NUMBER_1;
        messageTypesMap = 5;
        message = version3Request.createMessage(mac, messageTypesMap);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
      //  assertDB(messageTypesMap, mac);
     //   assertJson(mac, connectionMode,getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, messageTypesMap);
    }

    @Features(FeatureList.DSG)
    @Stories(StoriesList.POSITIVE + VERSION_3_STRING + DSG_CONNECTION_MODE_NOTIFICATION)
    @Description("5.0.2 Message from STB to AMS where <request id>=0, 'messageType' = 1 AND <vendor> IS specified" +
            "all service use version 1 protocols")
    @Test()
    public void sendVersion3RequestId0VendorIsSpecifiedRomIdMessageTypesMapAllRange() {
        String mac = MAC_NUMBER_1;
        messageTypesMap = 73300775185L;
        message = version3Request.createMessage(mac, messageTypesMap);
        List<String> stbResponse = version3Request.sendAndParseRequest(message);
        createAttachment(stbResponse.toString());
        log = readAMSLog();
        createAttachment(log);
        assertBetweenAllowableRange(stbResponse, dsgstbResponseBuilder.errorResponse(mac), ERROR + IN_DSG_RESPONSE);
     //   assertDB(messageTypesMap, mac);
      //  assertJson(mac, connectionMode,getBoxModel(), vendor);
        assertLog(log, mac, connectionMode, messageTypesMap);
    }

}
