package com.zodiac.ams.charter.checkers;

import com.zodiac.ams.charter.emuls.verifiers.epg.ConfigEpgVerifier;
import com.zodiac.ams.common.logging.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class EpgDataChecker {

    private ConfigEpgVerifier configEpgVerifier = new ConfigEpgVerifier();
    private final int countSegments = 14; //com.dob.charter.epg.request-days="14" from server.xml

    public List<String> readEpgVerifierLog() {
        List<String> log = new ArrayList<>();
        File file = new File(configEpgVerifier.getEpgVerifierLog());

        try {
            try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
                String s;
                while ((s = in.readLine()) != null) {
                    if (Pattern.compile("[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3} INFO  c.d.t.e.v.EpgVerifier").matcher(s).find()) {
                        log.add(s);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return log;
    }

    public List<Pattern> createPatternValidEpgVerifierLog(int countRows) {
        List<Pattern> patterns = createPatternReturnZdb();
        patterns.add(Pattern.compile(EpgVerifierKeyWords.verifyTime));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.allCsvRows.replace("{count}", Integer.toString(countRows + 1))));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.actualCsvRows.replace("{count}", Integer.toString(countRows))));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.actualXmlRows.replace("{count}", Integer.toString(countRows * countSegments))));
        return patterns;
    }

    public List<Pattern> createPatternDuplicateTimeEpgVerifierLog(int countRows) {
        List<Pattern> patterns = createPatternReturnZdb();
        patterns.add(Pattern.compile(EpgVerifierKeyWords.verifyTime));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.allCsvRows.replace("{count}", Integer.toString(countRows + 1))));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.actualCsvRows.replace("{count}", Integer.toString(countRows))));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.actualXmlRows.replace("{count}", Integer.toString((countRows-1) * countSegments))));
        return patterns;
    }

    public List<Pattern> createPatternWithGapEpgVerifierLog(int countRows, int breakPeriod) {
        List<Pattern> patterns = createPatternReturnZdb();
         for (int i=0;i< breakPeriod;i++)
            patterns.add(Pattern.compile(EpgVerifierKeyWords.gap));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.verifyTime));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.allCsvRows.replace("{count}", Integer.toString(countRows + 1))));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.actualCsvRows.replace("{count}", Integer.toString(countRows))));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.actualXmlRows.replace("{count}", Integer.toString(countRows * countSegments + breakPeriod*countSegments))));
        return patterns;
    }

    public  List<Pattern> createPatternReturnZdb(){
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(EpgVerifierKeyWords.returnZdbHdr));
        for (int i=0;i< countSegments;i++)
            patterns.add(Pattern.compile(EpgVerifierKeyWords.returnZdb));
        patterns.add(Pattern.compile(EpgVerifierKeyWords.processZdb));
        return patterns;
    }

    public boolean compareEpgVerifierLogWithPattern(List<Pattern> patterns, List<String> logs) {
        boolean verified = false;
        for (int i = 0; i < patterns.size(); i++) {
            if (patterns.get(i).matcher(logs.get(i)).find()) {
                verified = true;
            }
            else {
                verified = false;
                Logger.info("Log from AMS: " + logs.get(i) + "\n");
                Logger.info("Pattern log: " + patterns.get(i) + "\n");
                break;
            }
        }
        return verified;
    }
}

