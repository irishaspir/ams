package com.zodiac.ams.charter.checkers;

public interface EpgVerifierKeyWords {

    String EPG_VERIFIER = "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3} INFO  c.d.t.e.v.EpgVerifier - ";
    String returnZdb =  EPG_VERIFIER +  "return: ipg.+";
    String returnZdbHdr =  EPG_VERIFIER + "return: ipg-hdr.zdb.+";
    String processZdb = EPG_VERIFIER + "processZdb time \\[[0-9]+:[0-9]+:[0-9]+.[0-9]{3}\\]";
    String verifyTime = EPG_VERIFIER + "Verify time time \\[[0-9]+:[0-9]+:[0-9]+.[0-9]{3}\\]";
    String allCsvRows = EPG_VERIFIER + "Csv lines processed : {count}";
    String actualCsvRows = EPG_VERIFIER + "Actual Csv lines : {count}";
    String actualXmlRows = EPG_VERIFIER + "Actual XML lines : {count}";
    String gap = EPG_VERIFIER + "GAP found for line# = [0-9]+";
}
