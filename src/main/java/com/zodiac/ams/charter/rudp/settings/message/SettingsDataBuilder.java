package com.zodiac.ams.charter.rudp.settings.message;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.logging.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettingsDataBuilder {

    private static volatile Map<Integer, String> settingsMap = new HashMap<>();
    private static Integer[] keysArray = null;
    private static int method = 0; // 0 - getSettings, 1 - setSettings
    private static int boxModel = 0; // 0- unknown
    private final int GET = 0;
    private final int SET = 1;

    public SettingsDataBuilder(ArrayList<SettingsOption> options) {
        this.method = SET;
        List<Integer> keysList = new ArrayList<>();
        for (int i = 0; i < options.size(); i++) {
            keysList.add(options.get(i).getColumnNumber());
            settingsMap.put(options.get(i).getColumnNumber(),
                    options.get(i).getDefaultValue());
        }
        keysArray = keysList.toArray(new Integer[keysList.size()]);
    }

    public SettingsDataBuilder() {
        this.method = GET;
    }

    public byte[] generateSetMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(DOB7bitUtils.encodeUInt(method));
            baos.write(DOB7bitUtils.encodeUInt(boxModel));
            baos.write(DOB7bitUtils.encodeUInt(settingsMap.size()));
            for (int i = 0; i < keysArray.length; i++) {
                Integer key = keysArray[i];
                baos.write(DOB7bitUtils.encodeUInt(key));
                String values = settingsMap.get(keysArray[i]);
                switch (key) {
                    case 27:
                    case 37:
                    case 41:
                    case 53:
                    case 54:
                        baos.write(0x00);
                        break;
                    case 58:
                        baos.write(0x01);
                        break;
                    case 35:
                    case 44:
                    case 62:
                        baos.write(DOB7bitUtils.encodeString(values));
                        break;
                    default:
                        String value;
                        if (values == null) {
                            value = "0";
                        } else {
                            value = values;
                        }
                        baos.write(DOB7bitUtils.encodeUInt(Integer.valueOf(value)));
                }
            }
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            Logger.error("Couldn't create send message " + e.getLocalizedMessage());
        }
        return body;
    }

    public byte[] generateSetMessage(int x) {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(DOB7bitUtils.encodeUInt(method));
            baos.write(DOB7bitUtils.encodeUInt(boxModel));
            baos.write(DOB7bitUtils.encodeUInt(settingsMap.size()));
            for (int i = 0; i < keysArray.length; i++) {
                Integer key = keysArray[i];
                baos.write(DOB7bitUtils.encodeUInt(key));
                String values = settingsMap.get(keysArray[i]);
                switch (key) {
                    case 25:
                        baos.write(DOB7bitUtils.encodeUInt(x));
                        break;
                    case 27:
                    case 37:
                    case 41:
                    case 53:
                    case 54:
                        baos.write(0x00);
                        break;
                    case 58:
                        baos.write(0x01);
                        break;
                    case 35:
                    case 44:
                    case 62:
                        baos.write(DOB7bitUtils.encodeString(values));
                        break;
                    default:
                        String value;
                        if (values == null) {
                            value = "0";
                        } else {
                            value = values;
                        }
                        baos.write(DOB7bitUtils.encodeUInt(Integer.valueOf(value)));
                }
            }
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            Logger.error("Couldn't create send message " + e.getLocalizedMessage());
        }
        return body;
    }


    public byte[] generateGetMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(DOB7bitUtils.encodeUInt(method));
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            Logger.error("Couldn't create get message " + e.getLocalizedMessage());
        }
        return body;
    }


}
