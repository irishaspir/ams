package com.zodiac.ams.charter.rudp.dc.message;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacResponse;

import static com.zodiac.ams.common.rudp.ErrorCodes.getReasonByError;

/**
 * The local response message parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public class LocalResponseMessageParser {
    /*
     * Zodiac message types.
     */
    private static final int MESSAGE_TYPE_DEFAULT = 126;
    private static final int MESSAGE_TYPE_RESPONSE = 122;

    private static final int MESSAGE_RESPONSE_CODE_INCORRECT_FORMAT = 4;

    private final IZodiacMessage responseMessage;

    /**
     * The regular constructor.
     *
     * @param responseMessage the response message
     */
    public LocalResponseMessageParser(IZodiacMessage responseMessage) {
        if (responseMessage == null) {
            throw new IllegalArgumentException("The response message can't be null");
        }
        this.responseMessage = responseMessage;
    }

    /**
     * The method to check if the message contains an error reason.
     *
     * @return {@code true} if the message contains an error reason, {@code false} otherwise
     */
    public boolean isErrorReason() {
        return (getResponseCode() > 0);
    }

    /**
     * The method to check if the message is correct.
     *
     * @return {@code true} if the message is correct, {@code false} otherwise
     */
    public boolean isCorrectFormat() {
        int responseCode = getResponseCode();
        return responseCode == 0;
    }

    /**
     * The method to check if the message contains an error reason.
     *
     * @return {@code true} if the message contains an error reason, {@code false} otherwise
     */
    public boolean isIncorrectFormat() {
        int responseCode = getResponseCode();
        return responseCode > 0 && getResponseCode() == MESSAGE_RESPONSE_CODE_INCORRECT_FORMAT;
    }

    /**
     * The method to get an error reason.
     *
     * @return the error reason
     */
    public String getErrorReason() {
        String errorReason = null;
        int responseCode = getResponseCode();
        if (responseCode > 0) {
            errorReason = getReasonByError(responseCode);
        }

        return errorReason;
    }

    /**
     * The method to get a response code.
     *
     * @return the response code
     */
    public int getResponseCode() {
        if (responseMessage instanceof ZodiacResponse
                && responseMessage.getType() == MESSAGE_TYPE_RESPONSE) {
            return ((ZodiacResponse) responseMessage).getRespCode();
        }

        return 0;
    }
}
