package com.zodiac.ams.charter.rudp;

import com.zodiac.ams.common.logging.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RudpUtils {

    public static Long[] readMacsAsLongArray(String mac) {
        final List<Long> macs = parseSinglesAndRanges(mac, MAC_TO_LONG);
        return macs.toArray(new Long[macs.size()]);
    }

    private static List<Long> parseSinglesAndRanges(String from, RudpUtils.StringToLongConverter converter) {
        final List<Long> macs = new ArrayList<>();
        for (String rangeOrSingle : from.trim().split("\\s*,\\s*")) {
            if (rangeOrSingle.contains("-")) {
                macs.addAll(parseRange(rangeOrSingle));
            } else {
                macs.add(Long.parseLong(rangeOrSingle));
            }
        }
        return macs;
    }

    private static List<Long> parseRange(String range) {
        final List<Long> macs = new ArrayList<>();
        String[] bounds = range.split("\\s*-\\s*");
        if (bounds.length != 2) throw new IllegalArgumentException("Invalid range: " + range);
        final long leftBound = Long.parseLong(bounds[0]);
        final long rightBound = Long.parseLong(bounds[1]);
        for (long mac = leftBound; mac <= rightBound; ++mac) {
            macs.add(mac);
        }
        return macs;
    }

    private static final RudpUtils.StringToLongConverter MAC_TO_LONG = new RudpUtils.StringToLongConverter() {
        @Override
        public Long convert(String from) {
            return Long.valueOf(from, 16);
        }
    };

    private interface StringToLongConverter {
        Long convert(String from);
    }

    private static Map<InetAddress, Long[]> readMacIpMappingFile(File file) {
        Map<InetAddress, Long[]> result = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] pair = line.split(";");
                if (pair.length != 2) {
                    continue;
                }
                Long[] macRange = readLongMacsRange(pair[1]);
                InetAddress address = InetAddress.getByName(pair[0]);
                result.put(address, macRange);
            }
        } catch (IOException ioe) {
            Logger.error("Error when read mac ip mapping file", ioe);
            System.exit(-1);
        }
        return result;
    }

    // TODO: before all ranges takes without right bound, now with
    private static Long[] readLongMacsRange(String macsRange) {
        return readMacsAsLongArray(macsRange);
    }

}
