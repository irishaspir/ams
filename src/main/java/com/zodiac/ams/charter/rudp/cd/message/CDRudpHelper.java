package com.zodiac.ams.charter.rudp.cd.message;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.cd.CDSTBEmulator;
import com.zodiac.ams.charter.rudp.RudpHelper;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;
import com.zodiac.ams.charter.tcp.TCPClient;
import org.apache.commons.lang.RandomStringUtils;

import java.util.List;

public class CDRudpHelper extends RudpHelper {

    private int messageId;

    public CDRudpHelper(CDSTBEmulator cdstbEmulator) {
        super(cdstbEmulator);
    }

    private void cdRequest(byte[] message) {
        new TCPClient(message).send();
    }

    public byte[] buildMessage(String deviceId, List<CDProtocolOption> options, int id) {
        messageId = createMessageId();
        return new CDRequestBuilder(messageId, deviceId, options, id).buildMessage(true);
    }

    public byte[] buildMessage(String deviceId, int id) {
        messageId = createMessageId();
        return new CDRequestBuilder(messageId, deviceId, id).buildMessage(true);
    }

    public void sendRequest(String deviceId, List<CDProtocolOption> options, int id) {
        cdRequest(buildMessage(deviceId, options, id));
    }

    public void sendRequest(String mac, int id) {
        cdRequest(buildMessage(mac, id));
    }

    public int getMessageId() {
        return messageId;
    }

    private int createMessageId() {
        return Integer.parseInt(RandomStringUtils.randomNumeric(5));
    }

    private byte[] createErrorCode(int code) {
        return new CDResponseBuilder(code).generateMessage();
    }

    public ZodiacMessage createResponse(int code, String deviceId) {
      return new CDRequestBuilder(messageId, deviceId).buildMessage(createErrorCode(code));
    }

    private IZodiacMessage cdRequest(ZodiacMessage message) {
        return sendRequest(message);
    }

    public IZodiacMessage sendAndParseRequest(ZodiacMessage message){
        return cdRequest(message);
    }
    


}
