package com.zodiac.ams.charter.rudp.dc.message;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.common.logging.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.rudp.dc.message.LocalMessageUtils.encodeAndWrite32BitBigEndianString;
import static com.zodiac.ams.charter.rudp.dc.message.LocalMessageUtils.encodeAndWriteString;
import static com.zodiac.ams.charter.rudp.dc.message.LocalMessageUtils.encodeAndWriteUIntString;
import static com.zodiac.ams.charter.rudp.dc.message.LocalMessageUtils.encodeAndWriteUInt16String;
import static com.zodiac.ams.charter.tests.dc.common.MessageType.MESSAGE_TYPE_5;
import static com.zodiac.ams.charter.rudp.dc.message.LocalMessageUtils.UINT16;

/**
 * The local message builder.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public class LocalMessageBuilder {
    private List<Long> macs;
    private Long mac;

    private int messageType;

    private String messageId;
    private String sender;
    private String addresse;

    private String hubId;
    private String maxRebootsPerDay;
    private String nodeId;
    private List<SessionData> sessionDataList;
    private String sessionsNumber;
    private List<String> stringList;
    private String stringsNumber;

    /*
     * The event data class.
     */
    private static class EventData {
        private String eventId;
        private List<String> parameters;
        private String timeDelta;
        private String timestampPartMsec;
        
        /*
         * The private constructor.
         */
        private EventData() {
            // do nothing here
        }

        /*
         * The method to write the byte array of data.
         * Important. Don't change the order of writing.
         */
        private void writeData(ByteArrayOutputStream data) throws IOException {
            encodeAndWriteUIntString(data, timeDelta);

            encodeAndWriteUIntString(data, timestampPartMsec);

            encodeAndWriteUIntString(data, eventId);

            if (parameters != null) {
                for (String parameter : parameters) {
                    if (null != parameter && parameter.startsWith(UINT16)) //"uin16123" must be coded as encodeAndWriteUInt16String("123")
                        encodeAndWriteUInt16String(data, parameter.substring(UINT16.length()));
                    else
                        encodeAndWriteUIntString(data, parameter);
                }
            }
        }

        /**
         * The nested builder class.
         */
        public static class Builder {
            private String eventId;
            private List<String> parameters;
            private String timeDelta;
            private String timestampPartMsec;

            private SessionData.Builder parentBuilder;

            /*
             * The private constructor.
             */
            private Builder(SessionData.Builder parentBuilder) {
                this.parentBuilder = parentBuilder;
            }

            public Builder setEventId(int eventId) {
                return setEventId(String.valueOf(eventId));
            }

            public Builder setEventId(String eventId) {
                this.eventId = eventId;
                return this;
            }

            public Builder addParameter(int parameter) {
                return addParameter(String.valueOf(parameter));
            }

            public Builder addParameter(String parameter) {
                if (parameters == null) {
                    parameters = new ArrayList<>();
                }
                parameters.add(parameter);
                return this;
            }

            public Builder setTimeDelta(int timeDelta) {
                return setTimeDelta(String.valueOf(timeDelta));
            }

            public Builder setTimeDelta(String timeDelta) {
                this.timeDelta = timeDelta;
                return this;
            }

            public Builder setTimestampPartMsec(int timestampPartMsec) {
                return setTimestampPartMsec(String.valueOf(timestampPartMsec));
            }

            public Builder setTimestampPartMsec(String timestampPartMsec) {
                this.timestampPartMsec = timestampPartMsec;
                return this;
            }

            public SessionData.Builder endEvent() {
                return endEvent(1);
            }

            public SessionData.Builder endEvent(int repeatCount) {
                if (repeatCount < 1) {
                    throw new IllegalArgumentException("The repeat count can't be less than 1");
                }

                for (int i = 0; i < repeatCount; i++) {
                    EventData eventData = build();
                    parentBuilder.addEventData(eventData);
                }
                return parentBuilder;
            }

            public EventData build() {
                EventData target = new EventData();
                target.eventId = eventId;
                target.parameters = parameters;
                target.timeDelta = timeDelta;
                target.timestampPartMsec = timestampPartMsec;

                return target;
            }
        }
    }

    /*
     * The session data class.
     */
    private static class SessionData {
        private List<EventData> eventDataList;
        private String eventsNumber;
        private String firstChannelNumber;
        private String firstEventNumber;
        private String sessionStartTimestamp;

        /*
         * The private constructor.
         */
        private SessionData() {
            // do nothing here
        }

        /*
         * The method to write the byte array of data.
         * Important. Don't change the order of writing.
         */
        private void writeData(ByteArrayOutputStream data) throws IOException {
            encodeAndWrite32BitBigEndianString(data, sessionStartTimestamp);

            encodeAndWriteUIntString(data, firstChannelNumber);

            encodeAndWriteUIntString(data, firstEventNumber);

            encodeAndWriteUIntString(data, eventsNumber);

            if (eventDataList != null) {
                for (EventData eventData : eventDataList) {
                    eventData.writeData(data);
                }
            }
        }

        /**
         * The nested builder class.
         */
        public static class Builder {
            private List<EventData> eventDataList;
            private String eventsNumber;
            private String firstChannelNumber;
            private String firstEventNumber;
            private String sessionStartTimestamp;

            private LocalMessageBuilder parentBuilder;

            /*
             * The private constructor.
             */
            private Builder(LocalMessageBuilder parentBuilder) {
                this.parentBuilder = parentBuilder;
            }

            public Builder addEventData(EventData eventData) {
                if (eventDataList == null) {
                    eventDataList = new ArrayList<>();
                }
                eventDataList.add(eventData);
                return this;
            }

            public Builder setEventsNumber(int eventsNumber) {
                return setEventsNumber(String.valueOf(eventsNumber));
            }

            public Builder setEventsNumber(String eventsNumber) {
                this.eventsNumber = eventsNumber;
                return this;
            }

            public Builder setFirstChannelNumber(int firstChannelNumber) {
                return setFirstChannelNumber(String.valueOf(firstChannelNumber));
            }

            public Builder setFirstChannelNumber(String firstChannelNumber) {
                this.firstChannelNumber = firstChannelNumber;
                return this;
            }

            public Builder setFirstEventNumber(int firstEventNumber) {
                return setFirstEventNumber(String.valueOf(firstEventNumber));
            }

            public Builder setFirstEventNumber(String firstEventNumber) {
                this.firstEventNumber = firstEventNumber;
                return this;
            }

            public Builder setSessionStartTimestamp(int sessionStartTimestamp) {
                return setSessionStartTimestamp(String.valueOf(sessionStartTimestamp));
            }

            public Builder setSessionStartTimestamp(String sessionStartTimestamp) {
                this.sessionStartTimestamp = sessionStartTimestamp;
                return this;
            }

            public EventData.Builder startEvent() {
                return new EventData.Builder(this);
            }

            public LocalMessageBuilder endSession() {
                return endSession(1);
            }

            public LocalMessageBuilder endSession(int repeatCount) {
                if (repeatCount < 1) {
                    throw new IllegalArgumentException("The repeat count can't be less than 1");
                }

                for (int i = 0; i < repeatCount; i++) {
                    SessionData sessionData = build();
                    parentBuilder.addSessionData(sessionData);
                }
                return parentBuilder;
            }

            public SessionData build() {
                SessionData target = new SessionData();
                target.eventDataList = eventDataList;
                target.eventsNumber = eventsNumber;
                target.firstChannelNumber = firstChannelNumber;
                target.firstEventNumber = firstEventNumber;
                target.sessionStartTimestamp = sessionStartTimestamp;

                return target;
            }
        }
    }

    /**
     * The default constructor.
     */
    public LocalMessageBuilder() {
    }

    public LocalMessageBuilder setMacs(List<Long> macs) {
        this.macs = macs;
        return this;
    }

    public LocalMessageBuilder setMac(Long mac) {
        this.mac = mac;
        return this;
    }

    public LocalMessageBuilder setMessageType(int messageType) {
        this.messageType = messageType;
        return this;
    }

    public LocalMessageBuilder setMessageId(String messageId) {
        this.messageId = messageId;
        return this;
    }

    public LocalMessageBuilder setSender(String sender) {
        this.sender = sender;
        return this;
    }

    public LocalMessageBuilder setAddresse(String addresse) {
        this.addresse = addresse;
        return this;
    }

    public LocalMessageBuilder setHubId(int hubId) {
        return setHubId(String.valueOf(hubId));
    }

    public LocalMessageBuilder setHubId(String hubId) {
        this.hubId = hubId;
        return this;
    }

    public LocalMessageBuilder setMaxRebootsPerDay(int maxRebootsPerDay) {
        return setMaxRebootsPerDay(String.valueOf(maxRebootsPerDay));
    }

    public LocalMessageBuilder setMaxRebootsPerDay(String maxRebootsPerDay) {
        this.maxRebootsPerDay = maxRebootsPerDay;
        return this;
    }

    public LocalMessageBuilder setNodeId(int nodeId) {
        return setNodeId(String.valueOf(nodeId));
    }

    public LocalMessageBuilder setNodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public LocalMessageBuilder addSessionData(SessionData sessionData) {
        if (sessionDataList == null) {
            sessionDataList = new ArrayList<>();
        }
        sessionDataList.add(sessionData);
        return this;
    }

    public LocalMessageBuilder setSessionsNumber(int sessionsNumber) {
        return setSessionsNumber(String.valueOf(sessionsNumber));
    }

    public LocalMessageBuilder setSessionsNumber(String sessionsNumber) {
        this.sessionsNumber = sessionsNumber;
        return this;
    }

    public LocalMessageBuilder addString(String string) {
        if (stringList == null) {
            stringList = new ArrayList<>();
        }
        stringList.add(string);
        return this;
    }

    public LocalMessageBuilder setStringsNumber(int stringsNumber) {
        return setStringsNumber(String.valueOf(stringsNumber));
    }

    public LocalMessageBuilder setStringsNumber(String stringsNumber) {
        this.stringsNumber = stringsNumber;
        return this;
    }

    public SessionData.Builder startSession() {
        return new SessionData.Builder(this);
    }

    /*
     * The method to build the byte array of cheDataReceived.
     * Important. Don't change the order of writing.
     *
     * @return the built byte array of cheDataReceived
     */
    private byte[] buildData() {
        try (ByteArrayOutputStream data = new ByteArrayOutputStream()) {

            encodeAndWriteUIntString(data, nodeId);

            encodeAndWriteUIntString(data, hubId);

            encodeAndWriteUIntString(data, maxRebootsPerDay);

            if (messageType >= MESSAGE_TYPE_5) {
                encodeAndWriteUIntString(data, stringsNumber);

                if (stringList != null) {
                    for (String string : stringList) {
                        encodeAndWriteString(data, string);
                    }
                }
            }

            encodeAndWriteUIntString(data, sessionsNumber);

            if (sessionDataList != null) {
                for (SessionData sessionData : sessionDataList) {
                    sessionData.writeData(data);
                }
            }

            return data.toByteArray();
        } catch (IOException e) {
            Logger.error("Exception was thrown while building data: " + e.getLocalizedMessage());
        }

        return null;
    }

    public ZodiacMessage build() {
        ZodiacMessage message = new ZodiacMessage();
        message.setBodyBytes();
        message.setMessageId(messageId);
        message.setSender(sender);
        message.setAddressee(addresse + "/" + messageType);
        message.setMac(mac);
        message.setFlag(ZodiacMessage.FL_PERSISTENT);
        message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
//        message.setFlag(ZodiacMessage.FL_COMPRESSED);
        message.setData(buildData());

        Logger.info("The Zodiac message has been built");

        return message;
    }
}
