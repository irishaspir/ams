package com.zodiac.ams.charter.rudp.dc;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.common.logging.Logger;

/**
 * The local rudp helper.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class LocalRudpHelper {
    private STBEmulator stbEmulator;

    public LocalRudpHelper(STBEmulator stbEmulator) {
        this.stbEmulator = stbEmulator;
    }

    /**
     * The method to send message.
     *
     * @param message the message
     * @return the response message
     */
    public IZodiacMessage sendMessage(ZodiacMessage message) {
        Logger.info("Sending message to AMS: " + message.toString());
        IZodiacMessage responseMessage = stbEmulator.getServerRudpOne().send(message);
        Logger.debug("The response from AMS is: " + responseMessage);

        return responseMessage;
    }

    /**
     * The message to clean data.
     */
    public void cleanData() {
        stbEmulator.cleanData();
    }
}

