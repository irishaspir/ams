package com.zodiac.ams.charter.rudp.cd.message;

import com.dob.ams.util.DOB7bitUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by SpiridonovaIM on 28.07.2017.
 */
public class CDResponseBuilder {

    private int code;

    public CDResponseBuilder(int code) {
        this.code = code;
    }

    public byte[] generateMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

            try {
            baos.write(DOB7bitUtils.encodeUInt(code));
            } catch (IOException e) {
                e.printStackTrace();
            }

        try {
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }


}
