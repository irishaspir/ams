package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version0Data.setConnectionModeBoxModelVendor;
import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version1Data.createParamsList;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgRequests.CONNECTION_MODE_CHANGED_NOTIFICATIONS;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgVersionProtocol.VERSION_1;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version1Request extends ConnectionModeChangeNotificationRequestBuilder {

    public Version1Request(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    public ZodiacMessage createMessage(String mac, long messageTypesMap) {
        return buildMessage(true, VERSION_1, mac, createParamsList(setConnectionModeBoxModelVendor(CONNECTION_MODE_CHANGED_NOTIFICATIONS), String.valueOf(messageTypesMap)));
    }

    public ZodiacMessage createMessage(String mac) {
        return buildMessage(true, VERSION_1, mac, setConnectionModeBoxModelVendor(CONNECTION_MODE_CHANGED_NOTIFICATIONS));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, String boxModel) {
        return buildMessage(VERSION_1, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, int boxModel, int vendor) {
        return buildMessage(VERSION_1, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel, vendor));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, String romId, int vendor) {
        return buildMessage(VERSION_1, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, romId, vendor));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode) {
        return buildMessage(VERSION_1, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode));
    }

}
