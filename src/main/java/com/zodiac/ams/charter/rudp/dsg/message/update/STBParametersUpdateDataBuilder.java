package com.zodiac.ams.charter.rudp.dsg.message.update;

import com.zodiac.ams.charter.services.dsg.STBParametersUpdateOption;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.rudp.RudpKeyWordStore.DsgVersionProtocol.VERSION_2;
import static com.zodiac.ams.charter.services.dsg.STBParametersUpdateOptions.*;
import static com.zodiac.ams.charter.services.dsg.STBParametersUpdateOptions.REQUEST_ID;


/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public class STBParametersUpdateDataBuilder {

    public static List<STBParametersUpdateOption> version3(int content, int hubId, int serviceGroupId, int loadBalancerIpAddress, int sdvServiceGroupId, int cmIp, int connectionMode) {
        return setOption(version3(content, hubId, serviceGroupId, loadBalancerIpAddress, sdvServiceGroupId, cmIp), CONNECTION_MODE.getOption(), connectionMode);
    }

    public static List<STBParametersUpdateOption> version3(int content, int hubId, int serviceGroupId, int loadBalancerIpAddress, int sdvServiceGroupId, int cmIp) {
        return setOption(version3(content, hubId, serviceGroupId, loadBalancerIpAddress, sdvServiceGroupId), CM_IP.getOption(), cmIp);
    }

    public static List<STBParametersUpdateOption> version3(int content, int hubId, int serviceGroupId, int loadBalancerIpAddress, int sdvServiceGroupId) {
        return setOption(version3(content, hubId, serviceGroupId, loadBalancerIpAddress), SDV_SERVICE_GROUP_ID.getOption(), sdvServiceGroupId);
    }

    public static List<STBParametersUpdateOption> version3(int content, int hubId, int serviceGroupId, int loadBalancerIpAddress) {
        return setOption(version3(content, hubId, serviceGroupId), LOAD_BALANSER_ADDRESS.getOption(), loadBalancerIpAddress);
    }

    public static List<STBParametersUpdateOption> version3(int content, int hubId, int serviceGroupId) {
        return setOption(version3(content, hubId), SERVICE_GROUP_ID.getOption(), serviceGroupId);
    }

    public static List<STBParametersUpdateOption> version3(int content, int hubId) {
        return setOption(version3(content), HUB_ID.getOption(), hubId);
    }

    public static List<STBParametersUpdateOption> version3(int content) {
        return setOption(version3(), CONTENT.getOption(), content);
    }


    public static List<STBParametersUpdateOption> version3() {
        return setOptionValue(createOption(REQUEST_ID.getOption()), REQUEST_ID.getOption().getName(), VERSION_2);
    }

    private static List<STBParametersUpdateOption> setOption(List<STBParametersUpdateOption> set, STBParametersUpdateOption option, Object value) {
        List<STBParametersUpdateOption> options = createOptionSet(set, option);
        setOptionValue(options, option.getName(), value);
        return options;
    }

    private static ArrayList<STBParametersUpdateOption> createOption(STBParametersUpdateOption option) {
        return STBParametersUpdateOption.create()
                .addOption(option)
                .build();
    }

    private static List<STBParametersUpdateOption> createOptionSet(List<STBParametersUpdateOption> options, STBParametersUpdateOption option) {
        return STBParametersUpdateOption.create()
                .addListOptions(options)
                .addOption(option)
                .build();
    }

    private static List<STBParametersUpdateOption> setOptionValue(List<STBParametersUpdateOption> options, String name, Object optionValue) {
        options.forEach(value -> {
            if (value.getName().equals(name)) {
                value.setValue(String.valueOf(optionValue));
            }
        });
        return options;
    }


}
