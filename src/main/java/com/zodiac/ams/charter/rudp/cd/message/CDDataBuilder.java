package com.zodiac.ams.charter.rudp.cd.message;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static com.zodiac.ams.charter.services.cd.CDProtocolOptions.CALLSIGN;
import static com.zodiac.ams.charter.services.cd.CDProtocolOptions.DEEPLINK;


public class CDDataBuilder {

    private List<CDProtocolOption> options;

    public CDDataBuilder(List<CDProtocolOption> options) {
        this.options = options;
    }

    public byte[] generateMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        options.forEach(value -> {
            try {
                if (value.getName().equals(CALLSIGN.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeString(value.getValue()));
              else   if (value.getName().equals(DEEPLINK.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeString(value.getValue()));
                else baos.write(DOB7bitUtils.encodeUInt(Integer.parseInt(value.getValue())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        try {
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }
}

