package com.zodiac.ams.charter.rudp;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;

public interface STBRequestBuilder {

    ZodiacMessage buildMessage();
}
