package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version0Data.createParamsList;
import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version0Data.setConnectionModeBoxModelVendor;
import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version1Data.setMessageTypesMap;
import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version3Data.version3;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgRequests.CONNECTION_MODE_CHANGED_NOTIFICATIONS;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgVersionProtocol.VERSION_3;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version3Request extends ConnectionModeChangeNotificationRequestBuilder {


    public Version3Request(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion,
                                       Object sdvServiceGroupId, Object mcMacAdress, String serialNumber, Object cmIp, Object swVersionTmp) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress, serialNumber, cmIp, swVersionTmp));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object mcMacAdress, String serialNumber, Object cmIp) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress, serialNumber, cmIp));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object mcMacAdress, String serialNumber) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress, serialNumber));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId, Object mcMacAdress) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion, Object sdvServiceGroupId) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp, swVersion));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId, Object loadBalancerIp) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId, loadBalancerIp));
    }

    public ZodiacMessage createMessage(String mac, int hubId, int serviceGroupId) {
        return buildMessage(true, VERSION_3, mac, version3(hubId, serviceGroupId));
    }

    public ZodiacMessage createMessage(String mac, Object hubId) {
        return buildMessage(true, VERSION_3, mac, version3(hubId));
    }


    public ZodiacMessage createMessage(String mac, int connectionMode, String boxModel) {
        return buildMessage(VERSION_3, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, int boxModel, int vendor) {
        return buildMessage(VERSION_3, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel, vendor));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode) {
        return buildMessage(VERSION_3, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode));
    }

    public ZodiacMessage createMessage(String mac, long messageTypesMap) {
        return buildMessage(true, VERSION_3, mac, Version1Data.createParamsList(setConnectionModeBoxModelVendor(CONNECTION_MODE_CHANGED_NOTIFICATIONS), String.valueOf(messageTypesMap)));
    }


    public ZodiacMessage createMessage(String mac) {
        return buildMessage(true, VERSION_3, mac, setMessageTypesMap(CONNECTION_MODE_CHANGED_NOTIFICATIONS));
    }

    public ZodiacMessage createMessage(String mac, String migrationStart) {
        return buildMessage(true, VERSION_3, mac, Version3Data.createParamsList(setMessageTypesMap(CONNECTION_MODE_CHANGED_NOTIFICATIONS), migrationStart));
    }

}
