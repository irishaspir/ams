package com.zodiac.ams.charter.rudp.dsg.message.update;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.dsg.STBParametersUpdateOption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import static com.zodiac.ams.charter.services.dsg.STBParametersUpdateOptions.CM_IP;
import static com.zodiac.ams.charter.services.dsg.STBParametersUpdateOptions.CONTENT;
import static com.zodiac.ams.charter.services.dsg.STBParametersUpdateOptions.LOAD_BALANSER_ADDRESS;


public class STBParametersUpdateRudpBuilder {

    private List<STBParametersUpdateOption> options;

    public STBParametersUpdateRudpBuilder(List<STBParametersUpdateOption> options) {
        this.options = options;
    }

    public byte[] generateMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        options.forEach(value -> {
            try {
                if (value.getName().equals(CONTENT.getOption().getName()))
                    baos.write(ByteBuffer.allocate(4).putInt(Integer.parseInt(value.getValue())).array());
                 //   baos.write(DOB7bitUtils.encodeInt(Integer.parseInt(value.getValue())));
                else if (value.getName().equals(LOAD_BALANSER_ADDRESS.getOption().getName()))
                    baos.write(ByteBuffer.allocate(4).putInt(Integer.parseInt(value.getValue())).array());
                else if (value.getName().equals(CM_IP.getOption().getName()))
                    baos.write(ByteBuffer.allocate(4).putInt(Integer.parseInt(value.getValue())).array());
                else baos.write(DOB7bitUtils.encodeUInt(Integer.parseInt(value.getValue())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        try {
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }


}              //ByteBuffer.allocate(4).putInt(content).array()
