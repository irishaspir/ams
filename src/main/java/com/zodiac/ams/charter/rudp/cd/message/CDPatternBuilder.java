package com.zodiac.ams.charter.rudp.cd.message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SpiridonovaIM on 25.07.2017.
 */
public class CDPatternBuilder {

    private List<String> resquest(int requestId) {
        List<String> str = new ArrayList<>();
        str.add("Received CD message:\n");
        str.add(requestId + "\n");
        return str;
    }

    public List<String> cdRequestPattern(int requestId, Object... args) {
        List<String> str = resquest(requestId);
        switch (requestId) {
            case 1:
            case 6: {
                str.add(args[0] + "\n");
                if (args.length > 1)  str.add(args[1] + "\n");
                break;
            }
            case 2:
            case 5: {
                str.add(args[0] + "\n");
                break;
            }
            case 7: {
                str.add(args[0] + "\n");
                str.add(args[1] + "\n");
                if (args.length > 2) str.add(args[2] + "\n");
                break;
            }
        }
        return str;
    }
}
