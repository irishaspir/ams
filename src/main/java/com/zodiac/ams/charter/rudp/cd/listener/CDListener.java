package com.zodiac.ams.charter.rudp.cd.listener;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacResponse;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.zodiac.ams.charter.rudp.STBResponseListener;
import com.zodiac.ams.charter.store.RudpKeyWordStore;
import com.zodiac.ams.common.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.common.rudp.ErrorCodes.getReasonByError;


public class CDListener extends RudpKeyWordStore implements STBResponseListener {

    private IZodiacMessage message;
    private byte[] response;

    public CDListener(IZodiacMessage message) {
        this.message = message;
    }

    public CDListener(byte[] message) {
        this.response = message;
    }

    private void defineMessage() {
        if (message.getType() == ZODIAC_MESSAGE_TYPE)
            this.response = ((ZodiacMessage) message).getData();
        else if (message.getType() == ZODIAC_RESPONSE_TYPE) {
            byte respCode = ((ZodiacResponse) message).getRespCode();
            throw new RuntimeException("Transport error:  " + getReasonByError(respCode));
        } else throw new RuntimeException(" Couldn't define message");

    }

    public List<String> parseMessage() {
        Logger.info("Define response from AMS");
        if (response == null) {
            defineMessage();
        }
        List<String> builder = new ArrayList<>();
        builder.add("Received CD message:\n");
        try {
            DOBCountingInputStream dcis = new DOBCountingInputStream(new ByteArrayInputStream(response));
            int method = DOB7bitUtils.decodeUInt(dcis)[0];
            builder.add(method + "\n");
            switch (method) {
                case 1: {
                    int channel = DOB7bitUtils.decodeUInt(dcis)[0];
                    String callsign = DOB7bitUtils.decodeString(dcis);
                    builder.add(channel + "\n");
                    builder.add(callsign + "\n");
                    break;
                }
                case 2: {
                    int key = DOB7bitUtils.decodeUInt(dcis)[0];
                    builder.add(key + "\n");
                    break;
                }
                case 5: {
                    String key = DOB7bitUtils.decodeString(dcis);
                    builder.add(key+"\n");
                    break;
                }
                case 6: {
                    int recordingId = DOB7bitUtils.decodeUInt(dcis)[0];
                    int offset = DOB7bitUtils.decodeUInt(dcis)[0];
                    builder.add(recordingId + "\n");
                    builder.add(offset + "\n");
                    break;
                }
                case 7: {
                    int mode = DOB7bitUtils.decodeUInt(dcis)[0];
                    int speed = DOB7bitUtils.decodeUInt(dcis)[0];
                    int interval = DOB7bitUtils.decodeUInt(dcis)[0];
                    builder.add(mode + "\n");
                    builder.add(speed + "\n");
                    builder.add(interval + "\n");
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Can not parse server message", e);
        }
        return builder;
    }


}
