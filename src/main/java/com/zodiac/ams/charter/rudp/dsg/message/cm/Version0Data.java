package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.util.List;

import static com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOptions.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.*;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version0Data extends ConnectionModeChangeNotificationDataBuilder {

    private static List<ConnectionModeProtocolOption> createParamsList(int requestId) {
        return setOptionValue(createOption(REQUEST_ID.getOption()), REQUEST_ID.getOption().getName(), requestId);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(int requestId, Object connectionMode) {
        return setOption(createParamsList(requestId), CONNECTION_MODE.getOption(), connectionMode);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(int requestId, Object connectionMode, int vendor) {
        return setOption(createParamsList(requestId, connectionMode), VENDOR.getOption(), vendor);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(int requestId, Object connectionMode, Object boxModel) {
        return setOption(createParamsList(requestId, connectionMode), BOX_MODEL.getOption(), boxModel);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(int requestId, Object connectionMode, String romId) {
        return setOption(createParamsList(requestId, connectionMode), ROM_ID.getOption(), romId);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(int requestId, Object connectionMode, Object boxModel, Object vendor) {
        return setOption(createParamsList(requestId, connectionMode, boxModel), VENDOR.getOption(), vendor);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(int requestId, Object connectionMode, String romId, Object vendor) {
        return setOption(createParamsList(requestId, connectionMode, romId), VENDOR.getOption(), vendor);
    }

    protected static List<ConnectionModeProtocolOption> setConnectionModeBoxModelVendor(int requestId) {
        return createParamsList(requestId, connectionMode, boxModel, vendor);
    }


}
