package com.zodiac.ams.charter.rudp.callerId.message;

import java.util.ArrayList;
import java.util.List;

public class CallerIdSTBRequestPattern {

    private static final String INCOMING_CALL = "1";
    private static final String CALL_TERMINATION = "2";

    public List<String> stbIncomingCallNotificationRequestPattern(List<String> parameters) {
        List<String> dataZodiacMessage= new ArrayList<>();
        dataZodiacMessage.add(INCOMING_CALL);
        for (String param:parameters) {
            dataZodiacMessage.add(param);}
        return dataZodiacMessage;
    }

    public List<String> stbCallTerminationNotificationRequestPattern(String parameter) {
        List<String> dataZodiacMessage= new ArrayList<>();
        dataZodiacMessage.add(CALL_TERMINATION);
        dataZodiacMessage.add(parameter);
        return dataZodiacMessage;
    }

    public static List<String> nullRequest(){
        return null;
    }

}
