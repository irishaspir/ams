package com.zodiac.ams.charter.rudp.dsg.message.update;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.rudp.RudpHelper;
import com.zodiac.ams.charter.rudp.dsg.listener.DSGSTBResponseListener;
import com.zodiac.ams.charter.services.dsg.STBParametersUpdateOption;

import java.util.List;

import static com.zodiac.ams.charter.rudp.dsg.message.update.STBParametersUpdateDataBuilder.version3;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgVersionProtocol.VERSION_3;

/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public class STBParametersUpdateRequestBuilder extends RudpHelper {

    public STBParametersUpdateRequestBuilder(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    public IZodiacMessage dsgRequest(ZodiacMessage message) {
        return sendRequest(message);
    }

    public ZodiacMessage buildMessage(int version, String mac, List<STBParametersUpdateOption> options) {
        return new STBParametersUpdateMessageBuilder(version, mac, options).buildMessage();
    }

    public ZodiacMessage buildMessage(boolean changeAddresseeAndSender, int version, String mac, List<STBParametersUpdateOption> options) {
        return new STBParametersUpdateMessageBuilder(version, mac, options).buildMessage(changeAddresseeAndSender);
    }

    public List<String> parseDsgResponse(IZodiacMessage message) {
        return new DSGSTBResponseListener(message).parseMessage();
    }

    public ZodiacMessage protocolVersion3(String mac) {
        return buildMessage(VERSION_3, mac, version3());
    }

    public ZodiacMessage protocolVersion3(String mac, int content) {
        return buildMessage(VERSION_3, mac, version3(content));
    }

    public ZodiacMessage protocolVersion3(String mac, int content, int hubId) {
        return buildMessage(VERSION_3, mac, version3(content, hubId));
    }

    public ZodiacMessage protocolVersion3(boolean mix, String mac) {
        return buildMessage(mix, VERSION_3, mac, version3());
    }

    public ZodiacMessage protocolVersion3(boolean mix, String mac, int content) {
        return buildMessage(mix, VERSION_3, mac, version3(content));
    }

    public ZodiacMessage protocolVersion3(boolean mix, String mac, int content, int hubId) {
        return buildMessage(mix, VERSION_3, mac, version3(content, hubId));
    }

    public ZodiacMessage protocolVersion3(boolean mix, String mac, int content, int hubId, int serviceGroupId, int loadBalancerIpAddress, int sdvServiceGroupId, int cmIp, int connectionMode) {
        return buildMessage(mix, VERSION_3, mac, version3(content, hubId, serviceGroupId, loadBalancerIpAddress, sdvServiceGroupId, cmIp, connectionMode));
    }


}
