package com.zodiac.ams.charter.rudp.rollback.listeners;

import com.dob.ams.util.DOBCountingInputStream;
import com.google.common.primitives.Bytes;
import com.zodiac.ams.common.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RollbackListener {

    private byte[] response;

    public RollbackListener(byte[] message) {
        this.response = message;
    }

    public List<String> parseMessage() {
        Logger.info("Define request from AMS");
        List<String> data = new ArrayList<>();
        try {
            DOBCountingInputStream dcis = new DOBCountingInputStream(new ByteArrayInputStream(response));
            String request_id =  Integer.toString(dcis.read());
            List<Byte> parameter = new ArrayList<>();
            int value;
            while((value = dcis.read())!=-1)
                parameter.add((byte) value);
            data.add(request_id);
            if (parameter.size()!=0)
                data.addAll(Arrays.asList(new String(Bytes.toArray(parameter))));

        } catch (IOException e) {
            throw new RuntimeException("Can not parse server message", e);
        }
        return data;
    }
}
