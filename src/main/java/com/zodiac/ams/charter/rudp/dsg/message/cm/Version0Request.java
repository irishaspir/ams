package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version0Data.createParamsList;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgRequests.CONNECTION_MODE_CHANGED_NOTIFICATIONS;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgVersionProtocol.VERSION_0;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version0Request extends ConnectionModeChangeNotificationRequestBuilder {

    public Version0Request(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, int boxModel, int vendor) {
        return buildMessage(VERSION_0, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel, vendor));
    }

    public ZodiacMessage createMessage(String mac, Object connectionMode, int vendor) {
        return buildMessage(VERSION_0, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, vendor));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, String boxModel) {
        return buildMessage(VERSION_0, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode) {
        return buildMessage(VERSION_0, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode));
    }








}
