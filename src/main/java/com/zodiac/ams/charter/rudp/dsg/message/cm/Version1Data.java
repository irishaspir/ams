package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.util.List;

import static com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOptions.MESSAGE_TYPE_MAP;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.messageTypesMap;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version1Data extends Version0Data {


    public static List<ConnectionModeProtocolOption> setMessageTypesMap(int requestId) {
        return setOption(setConnectionModeBoxModelVendor(requestId), MESSAGE_TYPE_MAP.getOption(), messageTypesMap);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(List<ConnectionModeProtocolOption> set, Object value) {
        return setOption(set, MESSAGE_TYPE_MAP.getOption(), value);
    }





}
