package com.zodiac.ams.charter.rudp.callerId;

import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.rudp.RudpHelper;

public class CallerIdRudpHelper extends RudpHelper {

    public CallerIdRudpHelper(STBEmulator stbEmulator) {
        super(stbEmulator);
    }
}
