package com.zodiac.ams.charter.rudp.settings;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.rudp.RudpHelper;
import com.zodiac.ams.charter.rudp.settings.listeners.SettingsListener;
import com.zodiac.ams.charter.rudp.settings.message.SettingsSTBRequestBuilder;

import java.util.ArrayList;

/**
 * Created by SpiridonovaIM on 20.04.2017.
 */
public class SettingsRudpHelper extends RudpHelper {

    public SettingsRudpHelper(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    private IZodiacMessage getSettingsRequest(String mac) {
        return sendRequest(new SettingsSTBRequestBuilder(mac).buildMessage());
    }

    private IZodiacMessage setSettingsRequest(String mac, ArrayList options) {
        return sendRequest(new SettingsSTBRequestBuilder(mac, options).buildMessage());
    }

    private IZodiacMessage setSettingsRequest(String mac, ArrayList options, int x) {
        return sendRequest(new SettingsSTBRequestBuilder(mac, options, x).buildMessage());
    }

    public ArrayList<String> sendGetSettings(String mac) {
        return new SettingsListener(getSettingsRequest(mac)).parseMessage();
    }

    public ArrayList<String> sendSetSettings(String mac, ArrayList options) {
        return new SettingsListener(setSettingsRequest(mac, options)).parseMessage();
    }

    public ArrayList<String> sendSetSettings(String mac, ArrayList options, int x) {
        return new SettingsListener(setSettingsRequest(mac, options, x)).parseMessage();
    }

    public ArrayList<String> sendSetSettingsInvalid(String mac, ArrayList options) {
        return new SettingsListener(setSettingsRequest(mac, options)).parseMessage();
    }
}
