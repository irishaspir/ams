package com.zodiac.ams.charter.rudp.cd.message;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.rudp.STBRequestBuilder;
import com.zodiac.ams.charter.services.cd.CDProtocolOption;
import com.zodiac.ams.common.logging.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static com.zodiac.ams.charter.rudp.RudpUtils.readMacsAsLongArray;
import static com.zodiac.ams.charter.store.RudpKeyWordStore.Addressees.CD_ADDRESSEE;
import static com.zodiac.ams.charter.store.RudpKeyWordStore.Senders.CD_SENDER;


public class CDRequestBuilder implements STBRequestBuilder {
    private String sender = CD_SENDER;
    private String addressee = CD_ADDRESSEE;
    private String mac;
    private int messageId;
    private byte[] data = null;
    private int requestId;

    public CDRequestBuilder(int messageId, String mac, List<CDProtocolOption> options, int requestId) {
        this.messageId = messageId;
        this.mac = mac;
        this.requestId = requestId;
        this.data = new CDDataBuilder(options).generateMessage();
    }

    public CDRequestBuilder(int messageId, String mac, int requestId) {
        this.messageId = messageId;
        this.mac = mac;
        this.requestId = requestId;
    }

    public CDRequestBuilder(int messageId, String mac) {
        this.messageId = messageId;
        this.mac = mac;
    }

    public ZodiacMessage buildMessage() {
        ZodiacMessage message = new ZodiacMessage();
        message.setSender(sender);
        message.setAddressee(addressee);
        message.setMac(readMacsAsLongArray(mac)[0]);
        message.setMessageId(String.valueOf(messageId));
        message.setData(data);
        message.setFlag(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_PERSISTENT);
        message.setFlags(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_RETAIN_SOURCE);
        message.setFlag(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_COMPRESSED);
        Logger.info("Message to AMS has been built");
        return message;
    }

    public ZodiacMessage buildMessage(byte[] data) {
        ZodiacMessage message = new ZodiacMessage();
        message.setSender(sender);
        message.setAddressee(addressee);
        message.setMac(readMacsAsLongArray(mac)[0]);
        message.setMessageId(String.valueOf(messageId));
        message.setData(data);
        message.setFlag(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_PERSISTENT);
        message.setFlags(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_RETAIN_SOURCE);
        message.setFlag(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_COMPRESSED);
        Logger.info("Message to AMS has been built");
        return message;
    }

    public byte[] buildMessage(boolean x) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(DOB7bitUtils.encodeUInt(messageId));
            baos.write(DOB7bitUtils.encodeString(mac));
            baos.write(DOB7bitUtils.encodeUInt(requestId));
            if (data != null) baos.write(data);
            byte[] size = DOB7bitUtils.encodeUInt(baos.size());
            byte[] body = baos.toByteArray();
            baos.reset();
            baos.write(size);
            baos.write(body);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logger.info("Message to AMS has been built");
        return baos.toByteArray();

    }
}
