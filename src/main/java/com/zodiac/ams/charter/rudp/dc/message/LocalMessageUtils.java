package com.zodiac.ams.charter.rudp.dc.message;

import com.dob.ams.util.DOB7bitUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * The local message utilities.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
@SuppressWarnings("unused")
public class LocalMessageUtils {
    
    public static final String UINT16 = "uint16";
    
    /*
     * The private constructor.
     */
    private LocalMessageUtils() {
        // do nothing here
    }

    /**
     * The method to get the 32 bit big endian byte array representation of a given integer value.
     *
     * @param value the integer value
     * @return the 32 bit big endian byte array representation
     */
    public static byte[] to32BitBigEndian(int value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        byteBuffer.putInt(value);
        byteBuffer.flip();
        return byteBuffer.array();
    }

    /**
     * The method to encode string representation of an integer value.
     *
     * @param value the integer value
     * @return the 32 bit byte array representation
     */
    public static byte[] encode32BitBigEndianString(String value) {
        return encodeIntStringInternal(value, a32BitBigEndianEncoder);
    }

    /**
     * The method to encode and write string representation of an integer value.
     *
     * @param outputStream the output stream
     * @param value        the integer value
     * @throws IOException if an I/O error occurs while writing
     */
    public static void encodeAndWrite32BitBigEndianString(OutputStream outputStream, String value) throws IOException {
        if (value != null) {
            outputStream.write(encode32BitBigEndianString(value));
        }
    }

    /**
     * The method to encode string value.
     *
     * @param value the string value
     * @return the 7 bit byte array representation
     */
    public static byte[] encodeString(String value) {
        return DOB7bitUtils.encodeString(value);
    }

    /**
     * The method to encode and write string value.
     *
     * @param outputStream the output stream
     * @param value        the string value
     * @throws IOException if an I/O error occurs while writing
     */
    public static void encodeAndWriteString(OutputStream outputStream, String value) throws IOException {
        if (value != null) {
            outputStream.write(encodeString(value));
        }
    }

    /**
     * The method to encode string representation of an unsigned integer value.
     *
     * @param value the unsigned integer value
     * @return the 7 bit byte array representation
     */
    public static byte[] encodeUIntString(String value) {
        return encodeIntStringInternal(value, uIntEncoder);
    }
    
    public static byte[] encodeUInt16String(String value){
        return encodeIntStringInternal(value, uInt16Encoder);
    }

    /**
     * The method to encode and write string representation of an unsigned integer value.
     *
     * @param outputStream the output stream
     * @param value        the unsigned integer value
     * @throws IOException if an I/O error occurs while writing
     */
    public static void encodeAndWriteUIntString(OutputStream outputStream, String value) throws IOException {
        if (value != null) {
            outputStream.write(encodeUIntString(value));
        }
    }
    
    public static void encodeAndWriteUInt16String(OutputStream outputStream, String value) throws IOException {
        if (value != null) {
            outputStream.write(encodeUInt16String(value));
        }
    }
    

    private static byte[] encodeIntStringInternal(String value, IntEncoder intEncoder) {
        int uIntValue = parseUIntNumbersString(value);
        return uIntValue > -1 ? intEncoder.encode(uIntValue)
                : DOB7bitUtils.encodeString(value);
    }

    private interface IntEncoder {
        byte[] encode(int value);
    }

    
    private static IntEncoder  uInt16Encoder = new IntEncoder(){
        @Override
        public byte[] encode(int value) {
            return new byte[] {(byte)(value >>> 8), (byte) value};
        }
    };
        
    private static IntEncoder a32BitBigEndianEncoder = new IntEncoder() {
        @Override
        public byte[] encode(int value) {
            return to32BitBigEndian(value);
        }
    };

    private static IntEncoder uIntEncoder = new IntEncoder() {
        @Override
        public byte[] encode(int value) {
            return DOB7bitUtils.encodeUInt(value);
        }
    };

    /**
     * The method to parse an unsigned integer numbers string.
     *
     * @param value the unsigned integer numbers string
     * @return the unsigned integer numbers or -1 if an invalid string is passed
     */
    public static int parseUIntNumbersString(String value) {
        int result;
        try {
            result = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            result = -1;
        }
        return result;
    }
}
