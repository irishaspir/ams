package com.zodiac.ams.charter.rudp.settings.message;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;


import java.io.*;
import java.net.InetAddress;
import java.util.*;

public class StbRequestMessageBuilder {

    private static volatile Map<Integer, String> settingsMap = new HashMap<>();
    private static String sender = "charterSettings";
    private static String addressee = "charterSettings";
    private static Integer[] keysArray = null;
    private static int method = 0; // 0 - getSettings, 1 - setSettings
    private static int boxModel = 0; // 0- unknown
    private static Map<InetAddress, Long[]> ipMacMap = null;
    private static Long[] macs = null;

    public StbRequestMessageBuilder(String mac, ArrayList<SettingsOption> options, int method) {
        this(mac, method);
        List<Integer> keysList = new ArrayList<>();
        for (int i = 0; i < options.size(); i++) {
            keysList.add(options.get(i).getColumnNumber());
            settingsMap.put(options.get(i).getColumnNumber(),
                    options.get(i).getDefaultValue());
        }
        keysArray = keysList.toArray(new Integer[keysList.size()]);
    }

    public StbRequestMessageBuilder(String mac, int method) {
        Config conf = new Config();

        if (conf.getMacIpMappingFile() == null) {
            macs = readMacsAsLongArray(mac);
        } else {
            ipMacMap = readMacIpMappingFile(conf.getMacIpMappingFile());
        }
        this.method = method;
    }


    private byte[] generateSendMessage() {
        byte[] body = null;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(DOB7bitUtils.encodeUInt(method));
            baos.write(DOB7bitUtils.encodeUInt(boxModel));
            baos.write(DOB7bitUtils.encodeUInt(settingsMap.size()));
            for (int i = 0; i < keysArray.length; i++) {
                Integer key = keysArray[i];
                baos.write(DOB7bitUtils.encodeUInt(key));
                String values = settingsMap.get(keysArray[i]);

                switch (key) {
                    case 27:
                    case 37:
                    case 41:
                    case 53:
                    case 54:
                        baos.write(0x00);
                        break;
                    case 58:
                        baos.write(0x01);
                        break;
                    case 35:
                    case 44:
                    case 62:
                        baos.write(DOB7bitUtils.encodeString(values));
                        break;
                    default:
                        String value;
                        if (values == null) {
                            value = "0";
                        } else {
                            value = values;
                        }
                        baos.write(DOB7bitUtils.encodeUInt(Integer.valueOf(value)));
                }
            }
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            Logger.error("Couldn't create send message " + e.getLocalizedMessage());
        }
        return body;
    }


    private byte[] generateGetMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(DOB7bitUtils.encodeUInt(method));
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            Logger.error("Couldn't create get message " + e.getLocalizedMessage());
        }
        return body;
    }

    public ZodiacMessage buildMessage() {
        int messageId = 10012;
        ZodiacMessage message = new ZodiacMessage();
        message.setBodyBytes();
        message.setMessageId(String.valueOf(messageId));
        message.setSender(sender);
        message.setAddressee(addressee);
        message.setMac(macs[0]);
        message.setFlag(ZodiacMessage.FL_PERSISTENT);
        message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
        message.setFlag(ZodiacMessage.FL_COMPRESSED);
        if (1 == method)
            message.setData(generateSendMessage());
        else message.setData(generateGetMessage());
        Logger.info("Message to AMS has been built");
        return message;
    }


    private static Long[] readMacsAsLongArray(String property) {
        final List<Long> macs = parseSinglesAndRanges(property, MAC_TO_LONG);
        return macs.toArray(new Long[macs.size()]);
    }

    private static List<Long> parseSinglesAndRanges(String from, StringToLongConverter converter) {
        final List<Long> macs = new ArrayList<>();
        for (String rangeOrSingle : from.trim().split("\\s*,\\s*")) {
            if (rangeOrSingle.contains("-")) {
                macs.addAll(parseRange(rangeOrSingle));
            } else {
                macs.add(Long.parseLong(rangeOrSingle));
            }
        }
        return macs;
    }

    private static List<Long> parseRange(String range) {
        final List<Long> macs = new ArrayList<>();
        String[] bounds = range.split("\\s*-\\s*");
        if (bounds.length != 2) throw new IllegalArgumentException("Invalid range: " + range);
        final long leftBound = Long.parseLong(bounds[0]);
        final long rightBound = Long.parseLong(bounds[1]);
        for (long mac = leftBound; mac <= rightBound; ++mac) {
            macs.add(mac);
        }
        return macs;
    }

    private static final StringToLongConverter MAC_TO_LONG = new StringToLongConverter() {
        @Override
        public Long convert(String from) {
            return Long.valueOf(from, 16);
        }
    };

    private interface StringToLongConverter {
        Long convert(String from);
    }

    private static Map<InetAddress, Long[]> readMacIpMappingFile(File file) {
        Map<InetAddress, Long[]> result = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] pair = line.split(";");
                if (pair.length != 2) {
                    continue;
                }
                Long[] macRange = readLongMacsRange(pair[1]);
                InetAddress address = InetAddress.getByName(pair[0]);
                result.put(address, macRange);
            }
        } catch (IOException ioe) {
            Logger.error("Error when read mac ip mapping file", ioe);
            System.exit(-1);
        }

        return result;
    }

    // TODO: before all ranges takes without right bound, now with
    private static Long[] readLongMacsRange(String macsRange) {
        return readMacsAsLongArray(macsRange);
    }

}
