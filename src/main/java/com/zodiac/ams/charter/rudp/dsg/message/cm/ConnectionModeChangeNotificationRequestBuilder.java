package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.rudp.RudpHelper;
import com.zodiac.ams.charter.rudp.dsg.listener.DSGSTBResponseListener;
import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.util.List;

public class ConnectionModeChangeNotificationRequestBuilder extends RudpHelper {

    public ConnectionModeChangeNotificationRequestBuilder(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    private IZodiacMessage dsgRequest(ZodiacMessage message) {
        return sendRequest(message);
    }

    public ZodiacMessage buildMessage(int version, String mac, List<ConnectionModeProtocolOption> options) {
        return new DSGSTBRequestBuilder(version, mac, options).buildMessage();
    }

    public ZodiacMessage buildMessage(boolean changeAddresseeAndSender, int version, String mac, List<ConnectionModeProtocolOption> options) {
        return new DSGSTBRequestBuilder(version, mac, options).buildMessage(changeAddresseeAndSender);
    }

    private List<String> parseDsgResponse(IZodiacMessage message) {
        return new DSGSTBResponseListener(message).parseMessage();
    }

    public List<String> sendAndParseRequest(ZodiacMessage message){
       return parseDsgResponse(dsgRequest(message));
    }

}
