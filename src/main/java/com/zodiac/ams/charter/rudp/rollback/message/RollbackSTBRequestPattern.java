package com.zodiac.ams.charter.rudp.rollback.message;

import java.util.ArrayList;
import java.util.List;

public class RollbackSTBRequestPattern {

    private String ROLLBACK_TO_ROVI_REQUEST = "0";

    public List<String> rollbackToRoviFirmwareRequest() {
        List<String> request = new ArrayList<>();
        request.add(ROLLBACK_TO_ROVI_REQUEST);
        return request;
    }

    public static List<String> nullRequest(){
        return null;
    }

}
