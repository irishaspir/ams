package com.zodiac.ams.charter.rudp.settings.listeners;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacResponse;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.zodiac.ams.charter.store.RudpKeyWordStore;
import com.zodiac.ams.charter.rudp.STBResponseListener;
import com.zodiac.ams.common.logging.Logger;


import java.io.*;
import java.util.*;

import static com.zodiac.ams.common.rudp.ErrorCodes.getReasonByError;


public class SettingsListener extends RudpKeyWordStore implements STBResponseListener {

    private IZodiacMessage message;
    private byte[] response;

    public SettingsListener(IZodiacMessage message) {
        this.message = message;
    }

    public SettingsListener(byte[] message) {
        this.response = message;
    }

    private void defineMessage() {
        if (message.getType() == ZODIAC_MESSAGE_TYPE)
            this.response = ((ZodiacMessage) message).getData();
        else if (message.getType() == ZODIAC_RESPONSE_TYPE) {
            byte respCode = ((ZodiacResponse) message).getRespCode();
            throw new RuntimeException("Transport error:  " + getReasonByError(respCode));
        } else throw new RuntimeException(" Couldn't define message");

    }

    public ArrayList<String> parseMessage() {
        Logger.info("Define response from AMS");
        if (response == null) {
            defineMessage();
        }
        ArrayList<String> builder = new ArrayList<String>();
        builder.add("Received settings message: \n");
        try {
            DOBCountingInputStream dcis = new DOBCountingInputStream(new ByteArrayInputStream(response));
            if (1 == response.length) {
                builder.add("\terror code = " + DOB7bitUtils.decodeUInt(dcis)[0] + "\n");
            }
            if (2 == response.length) {
                builder.add("\tmethod = " + DOB7bitUtils.decodeUInt(dcis)[0] + "\n");
                builder.add("\terror code = " + DOB7bitUtils.decodeUInt(dcis)[0] + "\n");

            }
            if (response.length > 2) {
                int method = DOB7bitUtils.decodeUInt(dcis)[0];
                if (method == 3) {
                    builder.add("\tmethod = " + method + "\n");
                    int count = DOB7bitUtils.decodeUInt(dcis)[0];
                    builder.add("\tcount = " + count + "\n");
                    builder.addAll(parseMessage(dcis, count));
                } else {
                    builder.add("\tmethod = " + method + "\n");
                    builder.add("\terror code = " + DOB7bitUtils.decodeUInt(dcis)[0] + "\n");
                    int count = DOB7bitUtils.decodeUInt(dcis)[0];
                    builder.add("\tcount = " + count + "\n");
                    builder.addAll(parseMessage(dcis, count));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Can not parse server message", e);
        }
        return builder;
    }


    private List<String> parseMessage(DOBCountingInputStream response, int count) {
        List<String> builder = new ArrayList<>();
        try {
            for (int i = 0; i < count; i++) {
                int key = DOB7bitUtils.decodeUInt(response)[0];
                String str = "\t\t" + key + " = ";
                int mode;
                int cnt;
                switch (key) {
                    case 48:
                        builder.add(str + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                        break;
                    case 27:
                    case 37:
                        mode = DOB7bitUtils.decodeUInt(response)[0];
                        String means = "";
                        for (cnt = 0; cnt < mode; ++cnt)
                            means = means + DOB7bitUtils.decodeUInt(response)[0] + ";";
                        builder.add(str + mode + means + "\n");
                        break;
                    case 35:
                    case 44:
                    case 62:
                        builder.add(str + DOB7bitUtils.decodeString(response) + "\n");
                        break;
                    case 41:
                        cnt = DOB7bitUtils.decodeUInt(response)[0];
                        builder.add(str + cnt + "\n");
                        //     builder.add(str + cnt + "|\n");    //? in DB must be 41|
                        if (cnt != 0) {
                            for (int j = 0; j < cnt; j++) {
                                builder.add("\t\tdays = " + DOB7bitUtils.decodeUInt(response)[0]);
                                builder.add("\t\thours = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                            }
                        }
                        break;
                    case 53:
                    case 54:
                        cnt = DOB7bitUtils.decodeUInt(response)[0];
                        builder.add(str + cnt + "\n");
                        for (int j = 0; j < cnt; j++) {
                            int formatType = DOB7bitUtils.decodeUInt(response)[0];
                            builder.add("\t\tformat type = " + formatType);
                            if (formatType == 0)
                                builder.add("\t\tweekdays mask = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                            else {
                                builder.add("\t\tmonth = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                                builder.add("\t\tday = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                            }
                            builder.add("\t\ttime = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                        }
                        break;
                    case 55:
                        cnt = DOB7bitUtils.decodeUInt(response)[0];
                        builder.add(str + cnt + "\n");
                        for (int j = 0; j < cnt; j++) {
                            builder.add("\t\tmonth = " + DOB7bitUtils.decodeUInt(response)[0]);
                            builder.add("\t\tday = " + DOB7bitUtils.decodeUInt(response)[0]);
                            builder.add("\t\ttime = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                        }
                        break;
                    case 58:
                        mode = DOB7bitUtils.decodeUInt(response)[0];
                        builder.add(str + mode + "\n");
                        if (mode == 2) {
                            builder.add("\t\t" + "daylight time startDSGService = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                            builder.add("\t\t" + "daylight time duration = " + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                        }
                        break;
                    default:
                        builder.add(str + DOB7bitUtils.decodeUInt(response)[0] + "\n");
                }
            }
            Collections.sort(builder);
        } catch (IOException e) {
            Logger.debug("Can not parse server message", e);
        }
        return builder;
    }

}
