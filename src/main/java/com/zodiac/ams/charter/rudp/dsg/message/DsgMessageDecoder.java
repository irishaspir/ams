package com.zodiac.ams.charter.rudp.dsg.message;

import com.dob.ams.util.DOBCountingInputStream;

import java.io.IOException;


public class DsgMessageDecoder {

    private final static int PROTOCOL_VERSION_MIN = 0;
    private final static int PROTOCOL_VERSION_MAX = 3;

    public static int decodeBigInt(DOBCountingInputStream in) throws IOException {
        return
                ((int) (in.readByte() & 0xff) << 24) |
                        ((int) (in.readByte() & 0xff) << 16) |
                        ((int) (in.readByte() & 0xff) << 8) |
                        ((int) (in.readByte() & 0xff));
    }

    public static int decodeShortInt(DOBCountingInputStream in) throws IOException {
        return
                ((int) (in.readByte() & 0xff) << 8) |
                        ((int) (in.readByte() & 0xff));
    }



    private static long decodeCmMacAddress(DOBCountingInputStream in) throws IOException {
        return
                ((long) (in.readByte() & 0xff) << 40) |
                        ((long) (in.readByte() & 0xff) << 32) |
                        ((long) (in.readByte() & 0xff) << 24) |
                        ((long) (in.readByte() & 0xff) << 16) |
                        ((long) (in.readByte() & 0xff) << 8) |
                        ((long) (in.readByte() & 0xff));
    }
}

