package com.zodiac.ams.charter.rudp.dsg.listener;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacResponse;
import com.dob.ams.util.DOBCountingInputStream;
import com.zodiac.ams.charter.store.RudpKeyWordStore;
import com.zodiac.ams.charter.rudp.STBResponseListener;
import com.zodiac.ams.charter.rudp.dsg.message.DsgMessageDecoder;
import com.zodiac.ams.common.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.converterUnixToString;
import static com.zodiac.ams.common.rudp.ErrorCodes.getReasonByError;

public class DSGSTBResponseListener extends RudpKeyWordStore implements STBResponseListener {

    private IZodiacMessage message;
    private byte[] response;
    private int errorCode;
    private byte[] timeStamp;
    private byte[] timeZone;

    public DSGSTBResponseListener(IZodiacMessage message) {
        this.message = message;
    }

    private void defineMessage() {
        if (message.getType() == ZODIAC_MESSAGE_TYPE)
            this.response = ((ZodiacMessage) message).getData();
        else if (message.getType() == ZODIAC_RESPONSE_TYPE) {
            byte respCode = ((ZodiacResponse) message).getRespCode();
            throw new RuntimeException("Transport error:  " + getReasonByError(respCode));
        } else throw new RuntimeException(" Couldn't define message");
    }

    private void defineErrorCode() {
        errorCode = response[0] & 255;
    }

    private void defineTimeStamp() {
        timeStamp = new byte[4];
        int j = 0;
        if (response.length > 5) {
            for (int i = 1; i < 5; i++) {
                timeStamp[j] = response[i];
                j++;
            }
        } else Logger.error("EPG response has another format in TimeStamp value");
    }

    private void defineTimeZone() {
        timeZone = new byte[4];
        int j = 0;
        if (response.length < 8 && response.length > 5) {
            for (int i = 5; i < 7; i++) {
                timeZone[j] = response[i];
                j++;
            }
        } else Logger.error("EPG response has another format in  TimeZone value");

    }

    @Override
    public List<String> parseMessage() {
        Logger.info("Define response from AMS");
        if (response == null) {
            defineMessage();
        }
        List<String> builder = new ArrayList<>();
        builder.add("Received DSG message: \n");

        try {
            if (1 == response.length) {
                defineErrorCode();
                builder.add("\terror code = " + errorCode + "\n");
            }
            if (response.length > 1) {
                defineErrorCode();
                defineTimeStamp();
                defineTimeZone();
                builder.add("\terror code = " + errorCode + "\n");
                builder.add("\ttimeStamp = " + converterUnixToString(convertor(timeStamp)) + "\n");
                builder.add("\ttimeZone = " + convertor(timeZone) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logger.info("Response: " + builder.toString());
        return builder;
    }

    private int convertor(byte[] m) throws IOException {
        return DsgMessageDecoder.decodeShortInt(new DOBCountingInputStream(new ByteArrayInputStream(m)));
    }

}
