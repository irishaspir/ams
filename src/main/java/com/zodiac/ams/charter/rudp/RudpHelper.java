package com.zodiac.ams.charter.rudp;


import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.rudp.callerId.listeners.CallerIdListener;
import com.zodiac.ams.charter.rudp.rollback.listeners.RollbackListener;
import com.zodiac.ams.charter.rudp.settings.listeners.SettingsListener;
import com.zodiac.ams.common.logging.Logger;
import com.zodiac.ams.charter.store.RudpKeyWordStore;

import java.util.ArrayList;
import java.util.List;


public class RudpHelper extends RudpKeyWordStore {

    private STBEmulator stbEmulator;
    private long TIMEOUT = 1;

    public RudpHelper(STBEmulator stbEmulator) {
        this.stbEmulator = stbEmulator;
    }

    public ArrayList<String> getSettingsStbResponse() {
        byte[] message = stbEmulator.getData();
        if (message != null) {
            ArrayList<String> list = new SettingsListener(message).parseMessage();
            Logger.info(list.toString());
            return list;
        } else return null;
    }

    public List<String> getCallerIdStbRequest() {
        byte[] message = stbEmulator.getData(TIMEOUT);
        if (message != null) {
            List<String> list = new CallerIdListener(message).parseMessage();
            Logger.info(list.toString());
            return list;
        } else return null;
    }

    public List<String> getRollbackStbRequest() {
        byte[] message = stbEmulator.getData(TIMEOUT);
        if (message != null) {
            List<String> list = new RollbackListener(message).parseMessage();
            Logger.info(list.toString());
            return list;
        } else return null;
    }


    protected IZodiacMessage sendRequest(ZodiacMessage message) {
        Logger.info("Send message to AMS from STB " + message.toString());
        IZodiacMessage response = stbEmulator.getServerRudpOne().send(message);

        //  Logger.debug("Response from AMS   " + response.toString());
        return response;
    }

    public void cleanData(){
        stbEmulator.cleanData();
    }
}

