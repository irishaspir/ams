package com.zodiac.ams.charter.rudp;

public class RudpKeyWordStore {

    protected final int ZODIAC_MESSAGE_TYPE = 126;
    protected final int ZODIAC_RESPONSE_TYPE = 122;

    public class DsgVersionProtocol{
        public static final int VERSION_0 = 0;
        public static final int VERSION_1 = 1;
        public static final int VERSION_2 = 2;
        public static final int VERSION_3 = 3;
    }



}
