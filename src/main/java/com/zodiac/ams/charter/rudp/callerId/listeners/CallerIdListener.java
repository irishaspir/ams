package com.zodiac.ams.charter.rudp.callerId.listeners;

import com.dob.ams.util.DOBCountingInputStream;
import com.google.common.primitives.Bytes;
import com.zodiac.ams.common.logging.Logger;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CallerIdListener {

    private byte[] response;

    public CallerIdListener(byte[] message) {
        this.response = message;
    }

    public List<String> parseMessage() {
        Logger.info("Define response from AMS");
        List<String> data = new ArrayList<>();
         try {
            DOBCountingInputStream dcis = new DOBCountingInputStream(new ByteArrayInputStream(response));
               String request_id =  Integer.toString(dcis.read());
               List<Byte> parameter = new ArrayList<>();
               int value;
               while((value = dcis.read())!=-1)
                    parameter.add((byte) value);
               data.add(request_id);
               data.addAll(Arrays.asList(new String(Bytes.toArray(parameter)).split("\n")));

        } catch (IOException e) {
            throw new RuntimeException("Can not parse server message", e);
        }
        return data;
    }
}
