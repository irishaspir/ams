package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import static com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOptions.*;


public class DSGDataBuilder {

    private List<ConnectionModeProtocolOption> options;
    private StbMessageTypeMapBuilder messageTypeMapBuilder = new StbMessageTypeMapBuilder();

    public DSGDataBuilder(List<ConnectionModeProtocolOption> options) {
        this.options = options;
    }

    public byte[] generateMessage() {
        byte[] body = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        options.forEach(value -> {
            try {
                if (value.getName().equals(CM_IP.getOption().getName()))
                    baos.write(ByteBuffer.allocate(4).putInt(Integer.parseInt(value.getValue())).array());
                else if (value.getName().equals(SERIAL_NUMBER.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeString(value.getValue()));
                else if (value.getName().equals(SW_VERSION_TMP.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeString(value.getValue()));
                else if (value.getName().equals(MESSAGE_TYPE_MAP.getOption().getName()))
                    baos.write(ByteBuffer.allocate(8).putLong(Long.parseLong(value.getValue())).array());
                else if (value.getName().equals(CM_MAC_ADDRESS.getOption().getName()))
                    baos.write(ByteBuffer.allocate(8).putLong(Long.valueOf(value.getValue(), 16)).array(), 2, 6);
                else if (value.getName().equals(LOAD_BALANCER_IP_ADDRESS.getOption().getName()))
                    baos.write(ByteBuffer.allocate(4).putInt(Integer.parseInt(value.getValue())).array());
                else if (value.getName().equals(HUB_ID.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeInt(Integer.parseInt(value.getValue())));
                else if (value.getName().equals(SERVICE_GROUP_ID.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeInt(Integer.parseInt(value.getValue())));
                else if (value.getName().equals(SDV_SERVICE_GROUP_ID.getOption().getName()))
                    baos.write(DOB7bitUtils.encodeInt(Integer.parseInt(value.getValue())));
                else baos.write(DOB7bitUtils.encodeUInt(Integer.parseInt(value.getValue())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        try {
            body = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }
}

