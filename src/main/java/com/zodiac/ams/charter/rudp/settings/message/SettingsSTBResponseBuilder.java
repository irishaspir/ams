package com.zodiac.ams.charter.rudp.settings.message;


import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.logging.Logger;

import java.util.ArrayList;
import java.util.Collections;

import static com.zodiac.ams.charter.bd.ams.tables.SettingsKeysUtils.getSupportedSettingsValuesArray;

public class SettingsSTBResponseBuilder {

    public ArrayList<String> getSettingsSTBResponsePattern(ArrayList<SettingsOption> options) {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received settings message: \n");
        str.add("\tmethod = 0" + "\n");
        str.add("\terror code = 0" + "\n");
        str.add("\tcount = " + options.size() + "\n");
        //only for getSettings from STB
        ArrayList<String> optionsList = sortSettings(options);
        for (int i = 0; i < optionsList.size(); i++) {
            if (optionsList.get(i).contains("41 =")) {
                String str1 = optionsList.get(i).replace("|", "");
                optionsList.set(i, str1);
                Logger.info("41: " + optionsList.get(i));
            }
        }
        str.addAll(optionsList);
        return str;
    }

    private ArrayList<String> sortSettings(ArrayList<SettingsOption> options) {
        ArrayList<String> str = new ArrayList<>();
        for (SettingsOption one : options) {
            switch (one.getColumnNumber()) {
                case 27:
                case 37:
                case 41:
                case 53:
                case 54:
                    str.add("\t\t" + one.getColumnNumber() + " = " + one.getDefaultValue().replace('|', ' ').trim() + "\n");  //cause | need only in BD
                    break;
                case 58:
                    str.add("\t\t" + one.getColumnNumber() + " = " + one.getDefaultValue().replace('|', ' ').trim() + "\n");
                    break;
                default:
                    str.add("\t\t" + one.getColumnNumber() + " = " + one.getDefaultValue() + "\n");
            }
        }
        Collections.sort(str);
        return str;
    }

    private ArrayList<String> sortSettings62(ArrayList<SettingsOption> options) {
        ArrayList<String> str = new ArrayList<>();
        for (SettingsOption one : options) {
            if (one.getColumnNumber() == 41)
                str.add("\t\t" + one.getColumnNumber() + " = " + one.getDefaultValue() + "|" + "\n");
            else str.add("\t\t" + one.getColumnNumber() + " = " + one.getDefaultValue() + "\n");
            if (one.getColumnNumber() == 62) str.add("\t\t" + one.getColumnNumber() + " = " + "0" + "\n");
        }
        Collections.sort(str);
        return str;
    }

    public ArrayList<String> stbSetSettingsRequestPattern(ArrayList<SettingsOption> options, String newValue) {
        ArrayList<String> str = new ArrayList<>();
        String newRow = null;
        str.add("Received settings message: \n");
        str.add("\tmethod = 3" + "\n");
        str.add("\tcount = " + options.size() + "\n");
        str.addAll(sortSettings(options));
        String[] value = getSupportedSettingsValuesArray(options.get(0).getName());
        for (String one : value) {
            String[] dV = one.split("=");
            if (dV[1].equals(newValue)) {
                newRow = str.get(str.size() - 1);
                newRow = newRow.replaceAll("=.*", "= " + dV[0]);
            }
        }

        str.set(str.size() - 1, newRow);
        return str;
    }

    public ArrayList<String> stbSetSettingsRequestPattern(ArrayList<SettingsOption> options) {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received settings message: \n");
        str.add("\tmethod = 3" + "\n");
        str.add("\tcount = " + options.size() + "\n");
        str.addAll(sortSettings(options));
        return str;
    }

    public ArrayList<String> stbResponsePatternWithKey62Equal0(ArrayList<SettingsOption> options) {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received settings message: \n");
        str.add("\tmethod = 3" + "\n");
        str.add("\tcount = " + options.size() + "\n");
        str.addAll(sortSettings62(options));
        return str;
    }

    public ArrayList<String> stbSetSettingsRequestPatternEachSecondName(ArrayList<SettingsOption> options) {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received settings message: \n");
        str.add("\tmethod = 3" + "\n");
        for (int i = 0; i < options.size(); i++) {
            options.set(i, null);
            i++;
        }
        for (int i = 0; i < options.size(); i++) {
            if (options.get(i) == null) options.remove(i);
        }
        str.add("\tcount = " + options.size() + "\n");
        str.addAll(sortSettings(options));
        Logger.debug(str.toString());
        return str;
    }


    public ArrayList<String> stbErrorCodeNull() {
        ArrayList<String> builder = new ArrayList<String>();
        builder.add("Received settings message: \n");
        builder.add("\terror code = 0" + "\n");
        return builder;
    }

    public ArrayList<String> stbResponsePatternMethod3Ok() {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received settings message: \n");
        str.add("\tmethod = 3" + "\n");
        str.add("\terror code = 0" + "\n");
        return str;
    }

    public ArrayList<String> stbResponsePatternMethod1Ok() {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received settings message: \n");
        str.add("\tmethod = 1" + "\n");
        str.add("\terror code = 0" + "\n");
        return str;
    }


}
