package com.zodiac.ams.charter.rudp;

import java.io.IOException;
import java.util.List;

public interface STBResponseListener {

     List<String> parseMessage() throws IOException;
}
