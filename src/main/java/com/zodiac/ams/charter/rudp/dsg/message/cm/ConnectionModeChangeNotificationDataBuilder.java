package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.util.ArrayList;
import java.util.List;


public class ConnectionModeChangeNotificationDataBuilder {


    public static List<ConnectionModeProtocolOption> setOption(List<ConnectionModeProtocolOption> set, ConnectionModeProtocolOption option, Object value) {
        List<ConnectionModeProtocolOption> options = createOptionSet(set, option);
        setOptionValue(options, option.getName(), value);
        return options;
    }

    public static ArrayList<ConnectionModeProtocolOption> createOption(ConnectionModeProtocolOption option) {
        return ConnectionModeProtocolOption.create()
                .addOption(option)
                .build();
    }

    public static List<ConnectionModeProtocolOption> createOptionSet(List<ConnectionModeProtocolOption> options, ConnectionModeProtocolOption option) {
        return ConnectionModeProtocolOption.create()
                .addListOptions(options)
                .addOption(option)
                .build();
    }

    public static List<ConnectionModeProtocolOption> setOptionValue(List<ConnectionModeProtocolOption> options, String name, Object optionValue) {
        options.forEach(value -> {
            if (value.getName().equals(name)) {
                value.setValue(String.valueOf(optionValue));
            }
        });
        return options;
    }

}
