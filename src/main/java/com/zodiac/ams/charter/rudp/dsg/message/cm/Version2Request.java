package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version0Data.setConnectionModeBoxModelVendor;
import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version1Data.setMessageTypesMap;
import static com.zodiac.ams.charter.rudp.dsg.message.cm.Version2Data.createParamsList;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgRequests.CONNECTION_MODE_CHANGED_NOTIFICATIONS;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgVersionProtocol.VERSION_2;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version2Request extends ConnectionModeChangeNotificationRequestBuilder {


    public Version2Request(STBEmulator stbEmulator) {
        super(stbEmulator);
    }

    public ZodiacMessage createMessage(String mac, String migrationStart) {
        return buildMessage(true, VERSION_2, mac, createParamsList(setMessageTypesMap(CONNECTION_MODE_CHANGED_NOTIFICATIONS), migrationStart));
    }

    public ZodiacMessage createMessage(String mac) {
        return buildMessage(true, VERSION_2, mac, setMessageTypesMap(CONNECTION_MODE_CHANGED_NOTIFICATIONS));
    }


    public ZodiacMessage createMessage(String mac, int connectionMode, String boxModel) {
        return buildMessage(VERSION_2, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode, int boxModel, int vendor) {
        return buildMessage(VERSION_2, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode, boxModel, vendor));
    }

    public ZodiacMessage createMessage(String mac, int connectionMode) {
        return buildMessage(VERSION_2, mac, createParamsList(CONNECTION_MODE_CHANGED_NOTIFICATIONS, connectionMode));
    }

    public ZodiacMessage createMessage(String mac, long messageTypesMap) {
        return buildMessage(true, VERSION_2, mac, Version1Data.createParamsList(setConnectionModeBoxModelVendor(CONNECTION_MODE_CHANGED_NOTIFICATIONS), String.valueOf(messageTypesMap)));
    }

}
