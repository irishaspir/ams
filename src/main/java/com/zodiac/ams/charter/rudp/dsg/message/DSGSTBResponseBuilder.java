package com.zodiac.ams.charter.rudp.dsg.message;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils.getSTBLastActivityStringList;

public class DSGSTBResponseBuilder {

    public ArrayList<String> successDSGSTBResponsePattern(int errorCode, String timestamp) {
        ArrayList<String> str = new ArrayList<>();
        str.add("Received DSG message: \n");
        str.add("\terror code = " + errorCode + "\n");
        str.add("\ttimeStamp = " + timestamp + "\n");
        str.add("\ttimeZone = 55300\n");
        return str;
    }

    public List<String> successResponse(String mac) {
        List<String> list = getSTBLastActivityStringList(mac);
        return successDSGSTBResponsePattern(0, list.get(0));
    }

    public List<String> errorResponse(String mac) {
        List<String> list = getSTBLastActivityStringList(mac);
        return successDSGSTBResponsePattern(1, list.get(0));
    }



}
