package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.util.List;

import static com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOptions.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgVersionProtocol.VERSION_0;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version3Data extends Version2Data {

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion,
                                                              Object sdvServiceGroupId, Object mcMacAdress, Object serialNumber, Object cmIp, Object swVersionTmp) {
        return setOption(version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress, serialNumber, cmIp), SW_VERSION_TMP.getOption(), swVersionTmp);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion,
                                                              Object sdvServiceGroupId, Object mcMacAdress, Object serialNumber, Object cmIp) {
        return setOption(version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress, serialNumber), CM_IP.getOption(), cmIp);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion,
                                                              Object sdvServiceGroupId, Object mcMacAdress, Object serialNumber) {
        return setOption(version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId, mcMacAdress), SERIAL_NUMBER.getOption(), serialNumber);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion,
                                                              Object sdvServiceGroupId, Object mcMacAdress) {
        return setOption(version3(hubId, serviceGroupId, loadBalancerIp, swVersion, sdvServiceGroupId), CM_MAC_ADDRESS.getOption(), mcMacAdress);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion,
                                                              Object sdvServiceGroupId) {
        return setOption(version3(hubId, serviceGroupId, loadBalancerIp, swVersion), SDV_SERVICE_GROUP_ID.getOption(), sdvServiceGroupId);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp, Object swVersion) {
        return setOption(version3(hubId, serviceGroupId, loadBalancerIp), SW_VERSION.getOption(), swVersion);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId, Object loadBalancerIp) {
        return setOption(version3(hubId, serviceGroupId), LOAD_BALANCER_IP_ADDRESS.getOption(), loadBalancerIp);
    }

    public static List<ConnectionModeProtocolOption> version3(int hubId, int serviceGroupId) {
        return setOption(version3(hubId), SERVICE_GROUP_ID.getOption(), serviceGroupId);
    }

    public static List<ConnectionModeProtocolOption> version3(Object hubId) {
        return setOption(setMigrationStart(VERSION_0), HUB_ID.getOption(), hubId);
    }

}
