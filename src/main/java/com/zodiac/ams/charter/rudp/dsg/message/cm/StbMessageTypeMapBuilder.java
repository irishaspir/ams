package com.zodiac.ams.charter.rudp.dsg.message.cm;

/**
 * Created by SpiridonovaIM on 06.06.2017.
 */
public class StbMessageTypeMapBuilder {

    private long messageTypesMap;

    private void putValueToBitSet(byte value, int offset) {
        messageTypesMap |= (((long) (value) & 0xF) << offset);
    }

    public StbMessageTypeMapBuilder setPPVMessageType(byte type) {
        putValueToBitSet(type, 0);
        return this;
    }

    public StbMessageTypeMapBuilder setSettingsMessageType(byte type) {
        putValueToBitSet(type, 4);
        return this;
    }

    public StbMessageTypeMapBuilder setCallerIdMessageType(byte type) {
        putValueToBitSet(type, 8);
        return this;
    }

    public StbMessageTypeMapBuilder setCompanionDevicesMessageType(byte type) {
        putValueToBitSet(type, 12);
        return this;
    }

    public StbMessageTypeMapBuilder setDvrMessageType(byte type) {
        putValueToBitSet(type, 16);
        return this;
    }

    public StbMessageTypeMapBuilder setDvrMigrationMessageType(byte type) {
        putValueToBitSet(type, 20);
        return this;
    }

    public StbMessageTypeMapBuilder setMotorolaPackagesMessageType(byte type) {
        putValueToBitSet(type, 24);
        return this;
    }

    public StbMessageTypeMapBuilder setEpgForMotorola2kMessageType(byte type) {
        putValueToBitSet(type, 28);
        return this;
    }

    public StbMessageTypeMapBuilder setInteractiveChannelAuthorizationMessageType(byte type) {
        putValueToBitSet(type, 32);
        return this;
    }

    public long build() {
        return messageTypesMap;
    }
    
}
