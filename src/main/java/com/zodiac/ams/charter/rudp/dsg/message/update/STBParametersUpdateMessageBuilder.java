package com.zodiac.ams.charter.rudp.dsg.message.update;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.STBRequestBuilder;
import com.zodiac.ams.charter.services.dsg.STBParametersUpdateOption;
import com.zodiac.ams.common.logging.Logger;
import org.apache.commons.lang.RandomStringUtils;

import java.util.List;

import static com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_RETAIN_SOURCE;
import static com.zodiac.ams.charter.rudp.RudpUtils.readMacsAsLongArray;


public class STBParametersUpdateMessageBuilder implements STBRequestBuilder {

    private String sender = "dsg";
    private String addressee = "dsg/";
    private int version;
    private String mac;
    private int messageId = Integer.parseInt(RandomStringUtils.randomNumeric(5));
    private byte[] data;

    public STBParametersUpdateMessageBuilder(int version, String mac, List<STBParametersUpdateOption> options) {
        this.version = version;
        this.mac = mac;
        createData(options);
    }

    private void createData(List<STBParametersUpdateOption> options) {
        data = new STBParametersUpdateRudpBuilder(options).generateMessage();
    }

    @Override
    public ZodiacMessage buildMessage() {
        ZodiacMessage message = new ZodiacMessage();
        message.setSender(sender);
        Long mac1 = readMacsAsLongArray(mac)[0];
        message.setMac(mac1);
        message.setAddressee(addressee + version);
        message.setMessageId(String.valueOf(messageId));
        message.setData(data);
        message.setFlag(FL_RETAIN_SOURCE);
        message.setFlag(ZodiacMessage.FL_SENDER_NOT_ADRESSEE);
        Logger.info("Message to AMS has been built");
        return message;
    }

    public ZodiacMessage buildMessage(boolean changeAddresseeAndSender) {
        ZodiacMessage message = buildMessage();
        if (changeAddresseeAndSender) {
            message.setAddressee(sender);
            message.setSender(addressee + version);
        }
        return message;
    }


}
