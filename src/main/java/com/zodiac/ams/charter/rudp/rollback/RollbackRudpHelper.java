package com.zodiac.ams.charter.rudp.rollback;

import com.zodiac.ams.charter.emuls.stb.STBEmulator;
import com.zodiac.ams.charter.rudp.RudpHelper;

public class RollbackRudpHelper extends RudpHelper {

    public RollbackRudpHelper(STBEmulator stbEmulator) {
        super(stbEmulator);
    }
}
