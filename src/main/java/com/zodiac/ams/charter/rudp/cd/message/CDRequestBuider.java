package com.zodiac.ams.charter.rudp.cd.message;

import com.zodiac.ams.charter.services.cd.CDProtocolOption;

import java.util.List;

import static com.zodiac.ams.charter.services.cd.CDProtocolOptions.*;

/**
 * Created by SpiridonovaIM on 24.07.2017.
 */
public class CDRequestBuider {

    public static List<CDProtocolOption> setOption(List<CDProtocolOption> set, CDProtocolOption option, Object value) {
        List<CDProtocolOption> options = createOptionSet(set, option);
        setOptionValue(options, option.getName(), value);
        return options;
    }

    public static List<CDProtocolOption> createOption(CDProtocolOption option) {
        return CDProtocolOption.create()
                .addOption(option)
                .build();
    }

    public static List<CDProtocolOption> createOptionSet(List<CDProtocolOption> options, CDProtocolOption option) {
        return CDProtocolOption.create()
                .addListOptions(options)
                .addOption(option)
                .build();
    }

    public static List<CDProtocolOption> setOptionValue(List<CDProtocolOption> options, String name, Object optionValue) {
        options.forEach(value -> {
            if (value.getName().equals(name)) {
                value.setValue(String.valueOf(optionValue));
            }
        });
        return options;
    }

    public List<CDProtocolOption> createKeyPressMessage(int key) {
        return setOptionValue(createOption(KEY.getOption()), KEY.getOption().getName(), key);
    }

    public List<CDProtocolOption> createTuneSTBMessage(int chanNum, String callSign) {
        return setOption(createTuneSTBMessage(chanNum), CALLSIGN.getOption(), callSign);
    }

    public List<CDProtocolOption> createTuneSTBMessage(int chanNum) {
        return setOptionValue(createOption(CHANNEL_NUM.getOption()), CHANNEL_NUM.getOption().getName(), chanNum);
    }

    public List<CDProtocolOption> createDeeplinkMessage(String command) {
        return setOptionValue(createOption(DEEPLINK.getOption()), DEEPLINK.getOption().getName(), command);
    }

    public List<CDProtocolOption> createPlayBackMessage(int recordingId) {
        return setOptionValue(createOption(RECORDING_ID.getOption()), RECORDING_ID.getOption().getName(), recordingId);
    }

    public List<CDProtocolOption> createPlayBackMessage(int recordingId, int offSet) {
        return setOption(createPlayBackMessage(recordingId), PLAYBACK_OFFSET.getOption(), offSet);
    }

    public List<CDProtocolOption> createTrickModeMessage(int mode) {
        return setOptionValue(createOption(MODE.getOption()), MODE.getOption().getName(), mode);
    }

    public List<CDProtocolOption> createTrickModeMessage(int mode, int speed) {
        return setOption(createTrickModeMessage(mode), SPEED.getOption(), speed);
    }

    public List<CDProtocolOption> createTrickModeMessage(int mode, int speed, int interval) {
        return setOption(createTrickModeMessage(mode, speed), INTERVAL.getOption(), interval);
    }
}
