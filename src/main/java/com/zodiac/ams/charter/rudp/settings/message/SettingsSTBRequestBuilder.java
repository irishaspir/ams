package com.zodiac.ams.charter.rudp.settings.message;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.zodiac.ams.charter.rudp.STBRequestBuilder;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.logging.Logger;
import org.apache.commons.lang.RandomStringUtils;


import java.util.*;

import static com.zodiac.ams.charter.rudp.RudpUtils.readMacsAsLongArray;
import static com.zodiac.ams.charter.store.RudpKeyWordStore.Addressees.SETTINGS_ADDRESSEE;
import static com.zodiac.ams.charter.store.RudpKeyWordStore.Senders.SETTINGS_SENDER;

public class SettingsSTBRequestBuilder implements STBRequestBuilder {

    private static String sender = SETTINGS_SENDER;
    private static String addressee = SETTINGS_ADDRESSEE;
    private static String mac;
    private int messageId = Integer.parseInt(RandomStringUtils.randomNumeric(5));
    private byte[] data;

    public SettingsSTBRequestBuilder(String mac, ArrayList<SettingsOption> options, int x) {
        this.mac = mac;
        this.data = new SettingsDataBuilder(options).generateSetMessage(x);
    }

    public SettingsSTBRequestBuilder(String mac, ArrayList<SettingsOption> options) {
        this.mac = mac;
        this.data = new SettingsDataBuilder(options).generateSetMessage();
    }

    public SettingsSTBRequestBuilder(String mac) {
        this.mac = mac;
        this.data = new SettingsDataBuilder().generateGetMessage();
    }

    public ZodiacMessage buildMessage() {
        ZodiacMessage message = new ZodiacMessage();
        message.setSender(sender);
        message.setAddressee(addressee);
        message.setMac(readMacsAsLongArray(mac)[0]);
        message.setMessageId(String.valueOf(messageId));
        message.setData(data);
        message.setFlag(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_PERSISTENT);
        message.setFlags(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_RETAIN_SOURCE);
        message.setFlag(com.dob.ams.transport.zodiac.msg.ZodiacMessage.FL_COMPRESSED);
        Logger.info("Message to AMS has been built");
        return message;
    }
}
