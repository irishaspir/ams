package com.zodiac.ams.charter.rudp.dsg.message.cm;

import com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOption;

import java.util.List;

import static com.zodiac.ams.charter.services.dsg.ConnectionModeProtocolOptions.MIGRATION_START;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.DsgData.migrationStart;

/**
 * Created by SpiridonovaIM on 23.06.2017.
 */
public class Version2Data extends Version1Data {


    public static List<ConnectionModeProtocolOption> setMigrationStart(int version) {
        return setOption(setMessageTypesMap(version), MIGRATION_START.getOption(), migrationStart);
    }

    public static List<ConnectionModeProtocolOption> createParamsList(List<ConnectionModeProtocolOption> set, Object value) {
        return setOption(set, MIGRATION_START.getOption(), value);
    }


}
