package com.zodiac.ams.charter.emuls.che;

import com.dob.test.charter.iface.IHEDataConsumer;

import java.net.URI;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CHEDataReceived implements IHEDataConsumer {

    protected byte[] data = null;
    private URI uri = null;
    private List<URI> listUri = new ArrayList<>();
    private List<byte[]> listData = new ArrayList<>();

    public byte[] getData() {
        return data;
    }

    public URI getUri() {
        return uri;
    }

    public void cleanUri() {
        uri = null;
    }

    public List<URI> getListUri() {
        return listUri;
    }

    public List<byte[]> getListData() {
        return listData;
    }

    public void cleanData() {
        data = null;
    }

    @Override
    public void dataReceived(URI uri, byte[] bytes) {
        this.data = bytes;
        this.uri = uri;
        listUri.add(uri);
        listData.add(data);
        System.out.println();
    }

    public byte[] getData(long timeout) {
        if (data != null) {
            return data;
        }
        final CountDownLatch cdl = new CountDownLatch(1);
        final Timer waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (data != null) {
                    cdl.countDown();
                }
            }
        }, 0, 100);
        try {
            cdl.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        waitTimer.cancel();
        waitTimer.purge();
        return data;
    }

    public List<byte[]> getListData(long timeout) {
        if (listData != null) {
            return listData;
        }
        final CountDownLatch cdl = new CountDownLatch(1);
        final Timer waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (listData.size() > 0) {
                    cdl.countDown();
                }
            }
        }, 0, 100);
        try {
            cdl.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        waitTimer.cancel();
        waitTimer.purge();
        return listData;
    }

    public URI getUri(long timeout) {
        if (uri != null) {
            return uri;
        }
        final CountDownLatch cdl = new CountDownLatch(1);
        final Timer waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (uri != null) {
                    cdl.countDown();
                }
            }
        }, 0, 100);
        try {
            cdl.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        waitTimer.cancel();
        waitTimer.purge();
        return uri;
    }


    public List<URI> getListUri(long timeout) {
        if (listUri.size() > 0) {
            return listUri;
        }
        final CountDownLatch cdl = new CountDownLatch(1);
        final Timer waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (listUri.size() > 0) {
                    cdl.countDown();
                }
            }
        }, 0, 100);
        try {
            cdl.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        waitTimer.cancel();
        waitTimer.purge();
        return listUri;
    }
}
