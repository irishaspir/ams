package com.zodiac.ams.charter.emuls.verifiers.epg;

import java.io.IOException;
import java.util.Properties;

public class ConfigEpgVerifier {

    private final String configEpgVerifier = "properties/epg_verifier.properties";
    private final String EPG_VERIFIER_LOG = "epg_verifier.log";
    private Properties config;


    public ConfigEpgVerifier() {
        Properties config = new Properties();
        try {
            config.load(ConfigEpgVerifier.class.getClassLoader().getResourceAsStream(configEpgVerifier));

        } catch (IOException e) {
            throw new RuntimeException("Can't load config: " + e);
        }
        this.config = config;
    }

    public String getEpgVerifierLog() {
        return EPG_VERIFIER_LOG;
    }

    public Properties getConfig() {
        return config;
    }
}
