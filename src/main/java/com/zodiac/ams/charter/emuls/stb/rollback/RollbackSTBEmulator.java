package com.zodiac.ams.charter.emuls.stb.rollback;

import com.dob.test.charter.handlers.HandlerType;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

import java.io.IOException;

public class RollbackSTBEmulator extends STBEmulator {

    public RollbackSTBEmulator() {
        super();
    }

    public RollbackSTBEmulator(int status) {
        try {
            config.load(STBEmulator.class.getClassLoader().getResourceAsStream(localStbPropertiesFileName));
            config.put("rollback.status", Integer.toString(status));
        } catch (IOException e) {
            e.printStackTrace();
            }
    }

    public boolean registerConsumer() {
        initializeSTBEmulator();
        return emulator.registerConsumer(stbDataReceived, HandlerType.Rollback);
    }

 }