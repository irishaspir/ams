package com.zodiac.ams.charter.emuls.che.lookup;

import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.emuls.che.CHEEmulator;

import java.io.IOException;
import java.util.Properties;

public class LookupCHEEmulator extends CHEEmulator {

    public LookupCHEEmulator() {
        super();
    }

    public LookupCHEEmulator(String code) {
        Properties config = new Properties();
        try {
            config.load(CHEEmulator.class.getClassLoader().getResourceAsStream(localChePropertiesFileName));
            config.put("stb-lookup-code", code);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.config = config;
    }


    public boolean registerConsumer() {
        initializeCHEEmulator();
        return emulator.registerDataConsumer(cheDataReceived, HEHandlerType.STB_LOOKUP);
    }

}
