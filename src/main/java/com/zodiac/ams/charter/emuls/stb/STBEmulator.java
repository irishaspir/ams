package com.zodiac.ams.charter.emuls.stb;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.transport.ServerRudpOne;
import com.zodiac.ams.common.emulators.IEmulator;

import java.io.IOException;
import java.util.Properties;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.PATH_TO_PROPERTIES;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.STB_PROPERTIES;

abstract public class STBEmulator implements IEmulator {

    protected static final String localStbPropertiesFileName = PATH_TO_PROPERTIES + STB_PROPERTIES;

    protected CharterStbEmulator emulator;
    protected STBDataReceived stbDataReceived = new STBDataReceived();
    protected Properties config = new Properties();

    public STBEmulator() {
        try {
            config.load(STBEmulator.class.getClassLoader().getResourceAsStream(localStbPropertiesFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public STBEmulator(String timeout) {
        try {
            config.load(STBEmulator.class.getClassLoader().getResourceAsStream(localStbPropertiesFileName));
            config.put("response-delay-ms", timeout);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public STBEmulator(int id, int code, int params) {
        try {
            config.load(STBEmulator.class.getClassLoader().getResourceAsStream(localStbPropertiesFileName));
            config.put("cd.responses." + id, code + " " + params);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void initializeSTBEmulator() {
        try {
            emulator = new CharterStbEmulator(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ServerRudpOne getServerRudpOne() {
        return emulator.getRUDPTransport();
    }

    public byte[] getData() {
        byte[] message = stbDataReceived.getData();
        stbDataReceived.cleanData();
        return message;
    }

    public byte[] getData(long timeout) {
        byte[] message = stbDataReceived.getData(timeout);
        stbDataReceived.cleanData();
        return message;
    }

    public void cleanData() {
        stbDataReceived.cleanData();
    }

    public void start() {
        emulator.start();
    }

    public void stop() {
        emulator.stop();
    }
}