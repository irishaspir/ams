package com.zodiac.ams.charter.emuls;


import com.zodiac.ams.common.logging.Logger;

import java.io.File;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.*;

public class StbEmulatorConfig extends EmulatorsConfig {

    private static final String STB_PROPERTY_FILE = PATH_TO_PROPERTIES + STB_PROPERTIES;
    private static final String STB_HOST = "host";
    private static String propertiesPath = "";

    static {
        File jarPath = new File(StbEmulatorConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        propertiesPath = jarPath.getParentFile().getAbsolutePath();
        //  propertiesPath = "properties";
        Logger.debug("Read properties: " + propertiesPath);
    }

    public StbEmulatorConfig() {
        super(propertiesPath + File.separator + STB_PROPERTY_FILE);
    }

    public String getStbHost() {
        return getProperty(STB_HOST);
    }


}
