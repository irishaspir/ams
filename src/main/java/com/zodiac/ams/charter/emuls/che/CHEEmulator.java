package com.zodiac.ams.charter.emuls.che;

import com.dob.test.charter.LighweightCHEemulator;
import com.zodiac.ams.common.emulators.IEmulator;
import com.zodiac.ams.common.logging.Logger;

import java.io.IOException;
import java.net.URI;
import java.util.*;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.CHE_PROPERTIES;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.PATH_TO_PROPERTIES;
import static java.lang.Thread.sleep;

/**
 * Created by SpiridonovaIM on 12.01.2017.
 */
abstract public class CHEEmulator implements IEmulator {

    protected static final String localChePropertiesFileName = PATH_TO_PROPERTIES + CHE_PROPERTIES;
    protected LighweightCHEemulator emulator;
    protected CHEDataReceived cheDataReceived = new CHEDataReceived();
    private int TIMEOUT = 40;
    protected Properties config;

    public CHEEmulator() {
        Properties config = new Properties();
        try {
            config.load(CHEEmulator.class.getClassLoader().getResourceAsStream(localChePropertiesFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.config = config;
    }

    protected void initializeCHEEmulator(){
        try {
            emulator = new LighweightCHEemulator(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] getData() {
        byte[] message = cheDataReceived.getData();
        cheDataReceived.cleanData();
        return message;
    }

    public byte[] getData(long timeout) {
        byte[] message = cheDataReceived.getData(timeout);
        cheDataReceived.cleanData();
        return message;
    }


    public String waitResponse() {
        int i = 0;
        while (cheDataReceived.getUri() == null) {
            try {
                sleep(1000);
                i++;
            } catch (InterruptedException e) {
                Logger.error("CHE don't answer ");
                return null;
            }
            if (i == TIMEOUT) {
                Logger.debug("Response is absent during " + TIMEOUT + " seconds");
                return ("Response is absent during " + TIMEOUT + " seconds");
            }
        }
        return "";
    }


    public String getURI() {
        if (waitResponse().equals("")) {
            URI uri = cheDataReceived.getUri();
            cheDataReceived.cleanUri();
            Logger.info("URI on CHE emulator is " + uri.toString());
            return uri.toString();
        } else return null;
    }


    public String getURI(long timeout) {
        if (waitResponse().equals("")) {
            URI uri = cheDataReceived.getUri(timeout);
            cheDataReceived.cleanUri();
            Logger.info("URI on CHE emulator is " + uri.toString());
            return uri.toString();
        } else return null;
    }


    public List getURIList(int sizeList) {
        List<URI> listUri = new ArrayList<>();
        if (waitResponse().equals("")) {
            do {
                listUri.clear();
                if (sizeList == 14) listUri.addAll(new ArrayList<>(new HashSet<>(cheDataReceived.getListUri())));
                else listUri.addAll(cheDataReceived.getListUri());
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (listUri.size() < sizeList);
            Logger.info("Get list URI from CHE emulator, list size is  " + listUri.size());
        }
        Collections.sort(listUri);
        return listUri;
    }

    public List getDataList(int sizeList) {
        List<byte[]> listData = new ArrayList<>();
        if (waitResponse().equals("")) {
            do {
                listData.clear();
                listData.addAll(cheDataReceived.getListData());
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (listData.size() < sizeList);
            Logger.info("Get list cheDataReceived from CHE emulator, list size is  " + listData.size());
        }
        return listData;
    }

    public List getURIList(long timeout) {
        List<URI> listUri = new ArrayList<>();
        if (waitResponse().equals("")) {
            do {
                listUri.clear();
                listUri.addAll(cheDataReceived.getListUri(timeout));
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (listUri.size() < 14);
            Logger.info("Get list URI from CHE emulator, list size is  " + listUri.size());
        }
        Collections.sort(listUri);
        return listUri;
    }


    public void cleanData() {
        cheDataReceived.cleanUri();
        cheDataReceived.getData();
    }

    public void start() {
        emulator.start();
    }

    public void stop() {
        emulator.stop();
    }


}
