package com.zodiac.ams.charter.emuls.che.settings;


import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.emuls.che.CHEEmulator;
import com.zodiac.ams.common.logging.Logger;
import org.json.JSONObject;

public class SettingsCHEEmulator extends CHEEmulator {

    private JSONObject json = null;

    public SettingsCHEEmulator() {
        super();
    }

    public boolean registerConsumer() {
        cheDataReceived.cleanData();
        initializeCHEEmulator();
        return emulator.registerDataConsumer(cheDataReceived, HEHandlerType.SETTINGS);
    }

    public JSONObject checkData() {
        //check type of Data in future
        return convertDataToJson(getData());
    }

    private JSONObject convertDataToJson(byte[] message) {
        StringBuilder data_string = new StringBuilder();
        for (int i = 0; i < message.length; i++) {
            data_string.append((char) message[i]);
        }
        Logger.info("Data from CHE emulator is " + data_string.toString());
        return json = new JSONObject(data_string.toString());
    }

    public JSONObject getJson() {
        return json;
    }

}
