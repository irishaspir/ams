package com.zodiac.ams.charter.emuls.che.epg;

import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.emuls.che.CHEEmulator;

import java.util.List;

public class EpgCHEEmulator extends CHEEmulator {

    public EpgCHEEmulator() {
        super();
    }

    public boolean registerConsumer() {
        cheDataReceived.cleanData();
        initializeCHEEmulator();
        return emulator.registerDataConsumer(cheDataReceived, HEHandlerType.EPG);
    }

    public List getURIList() {
        return getURIList(14);
    }
}
