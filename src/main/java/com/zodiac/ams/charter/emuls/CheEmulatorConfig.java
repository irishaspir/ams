package com.zodiac.ams.charter.emuls;

import com.zodiac.ams.common.logging.Logger;

import java.io.File;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.*;

public class CheEmulatorConfig extends EmulatorsConfig {

    private static final String EPG_CONTEXT = "epg-context";
    private static final String EPG_FILE = "epg-file";
    private static final String APP_CHANEL_AMS_URL = "app-chanel-ams-url";
    private static String propertiesPath = "";
    private static final String CHE_PROPERTY_FILE = PATH_TO_PROPERTIES + CHE_PROPERTIES;

    static {
        File jarPath = new File(CheEmulatorConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        propertiesPath = jarPath.getParentFile().getAbsolutePath();
        //  propertiesPath = "properties";
        Logger.debug("Read properties: " + propertiesPath);
    }

    public CheEmulatorConfig() {
        super(propertiesPath + File.separator + CHE_PROPERTY_FILE);
    }

    public String getEpgContext() {
        return getProperty(EPG_CONTEXT);
    }

    public String getEpgFile() {
        // return getProperty(EPG_FILE);
        return "./epgData/_epg.csv";
    }

    public String getAppChanelAmsUrl() {
        return getProperty(APP_CHANEL_AMS_URL);
    }


}
