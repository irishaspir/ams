package com.zodiac.ams.charter.emuls.stb.settings;

import com.dob.test.charter.handlers.HandlerType;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

/**
 * Created by SpiridonovaIM on 21.04.2017.
 */
public class SettingsSTBEmulator extends STBEmulator {

    public SettingsSTBEmulator() {
        super();
    }

    public SettingsSTBEmulator(String timeout) {
        super(timeout);
    }

    public boolean registerConsumer() {
        initializeSTBEmulator();
        return emulator.registerConsumer(stbDataReceived, HandlerType.Settings);
    }

}
