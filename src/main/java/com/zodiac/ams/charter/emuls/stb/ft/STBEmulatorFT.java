package com.zodiac.ams.charter.emuls.stb.ft;

import com.dob.test.charter.CharterStbEmulator;
import com.dob.test.charter.handlers.HandlerType;
import com.dob.test.transport.iface.IMessageHandler;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.common.emulators.IEmulator;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class STBEmulatorFT implements IEmulator{
    
    private static final long DEFAULT_TIMEOUT_MS = 2 * 60 * 1000; //2min
    private static final String DIR_NAME = "properties";
    private static final String FILE_NAME = "stb.properties";
    private static final Logger LOG = LoggerFactory.getLogger(STBEmulatorFT.class);
    private static volatile Properties properties;
    
    private CharterStbEmulator emulator;
    private Semaphore semaphore;
    private STBDataConsumerFT consumer;
    private long timeout;
    private HandlerType type;
    
    private Properties getProperties(){
        if (null == properties){
            synchronized (STBEmulatorFT.class){
                if (null == properties){
                    properties = new Properties();
                    try (BufferedInputStream in = new BufferedInputStream( new FileInputStream(FTConfig.getFileFromJar(DIR_NAME, FILE_NAME)))){
                        properties.load(in);
                    }
                    catch(IOException ex){
                        LOG.error("STBEmulatorFT getConfig error: {}", ex.getMessage());
                    }
                }
            }
        }
        return properties;
    }
    
    public STBEmulatorFT(HandlerType type) {
        this(type, DEFAULT_TIMEOUT_MS);
    }
    
    public STBEmulatorFT(HandlerType type, long timeout) {
        try{
            this.type = type;
            this.timeout = timeout;
            emulator = new CharterStbEmulator(getProperties());
            semaphore = new Semaphore(0);
            consumer = new STBDataConsumerFT(semaphore);
            emulator.registerConsumer(consumer, type);
        }
        catch(IOException ex){
            LOG.error("STBEmulatorFT() error: {}", ex.getMessage());
        }
    }
    
    public IMessageHandler getMessageHandler(){
        return emulator.getMessageHandler(type);
    }

    @Override
    public void start() {
        emulator.start();
    }

    @Override
    public void stop() {
        emulator.stop();
    }

    @Override
    public boolean registerConsumer() {
        throw new IllegalStateException("Consumer is already registered.");
    }

    @Override
    public byte[] getData() {
        boolean ok = false;
        try {
            ok = semaphore.tryAcquire(timeout, TimeUnit.MILLISECONDS);
            if (!ok)
                LOG.error("STBEmulatorFT getData: unable to acquire semaphore");
        } 
        catch (InterruptedException ex) {
            LOG.error("STBEmulatorFT getData error: {}", ex.getMessage());
        }
        return ok ?  consumer.getData() : null;
    }
    
}
