package com.zodiac.ams.charter.emuls.stb;

import com.dob.test.transport.iface.IDataConsumer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by SpiridonovaIM on 11.11.2016.
 */
public class STBDataReceived implements IDataConsumer {

    private byte[] data = null;

    @Override
    public void dataReceived(byte[] bytes) {
        this.data = bytes;
    }

    public  byte[] getData() {
        return data;
    }

    public void cleanData() {
        data = null;
    }

    public  byte[] getData(long timeout) {
        if (data != null) {
            return data;
        }
        final CountDownLatch cdl = new CountDownLatch(1);
        final Timer waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (data != null) {
                    cdl.countDown();
                }
            }
        }, 0, 100);
        try {
            cdl.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        waitTimer.cancel();
        waitTimer.purge();
        return data;
    }

}
