package com.zodiac.ams.charter.emuls.stb.ft;

import com.dob.test.transport.iface.IDataConsumer;
import java.util.concurrent.Semaphore;

public class STBDataConsumerFT implements IDataConsumer{
    
    private byte[] data;
    private final Semaphore semaphore;
    
    public STBDataConsumerFT(Semaphore semaphore){
        this.semaphore = semaphore;
    }
    

    @Override
    public void dataReceived(byte[] data) {
        this.data = data;
        if (null != semaphore)
            semaphore.release();
    }
    
    public byte[] getData() {
        return data;
    }
}
