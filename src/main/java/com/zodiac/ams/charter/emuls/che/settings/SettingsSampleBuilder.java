package com.zodiac.ams.charter.emuls.che.settings;


import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.charter.services.settings.SettingsOptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class SettingsSampleBuilder {

    private String deviceId;
    private String path;
    private ArrayList<SettingsOption> list;
    private String name;

    public SettingsSampleBuilder(String id, ArrayList<SettingsOption> list, String path, String name) {
        this.list = list;
        this.deviceId = id;
        this.path = path;
        this.name = name;
    }

    public SettingsSampleBuilder(String id, String path, String name) {
        this.deviceId = id;
        list = new ArrayList<>();
        for (SettingsOptions one : Arrays.asList(SettingsOptions.values())) {
            list.add(one.getOption());
        }
        this.path = path;
        this.name = name;
    }

    public ArrayList<SettingsOption> getList() {
        return list;
    }

    public void messageBuild() {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement("settings");
            rootElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:noNamespaceSchemaLocation", "SettingsDescriptionSchema.xsd");
            doc.appendChild(rootElement);
            Element group = doc.createElement("group");
            rootElement.appendChild(group);
            Element id = doc.createElement("id");
            id.appendChild(doc.createTextNode("STB" + deviceId));
            group.appendChild(id);
            Element type = doc.createElement("type");
            type.appendChild(doc.createTextNode("STB"));
            group.appendChild(type);
            for (SettingsOption one : list) {
                Element option = doc.createElement("option");
                group.appendChild(option);

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(one.getName()));
                option.appendChild(name);

                Element value = doc.createElement("value");
                value.appendChild(doc.createTextNode(one.getValue()));
                option.appendChild(value);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(path + name));

            transformer.transform(source, result);
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
