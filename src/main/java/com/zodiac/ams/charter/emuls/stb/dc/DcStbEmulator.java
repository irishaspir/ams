package com.zodiac.ams.charter.emuls.stb.dc;

import com.dob.test.charter.handlers.HandlerType;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

/**
 * The DC STB emulator.
 *
 * @author <a href="mailto:tigranabaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class DcStbEmulator extends STBEmulator {
    public DcStbEmulator() {
        super();
    }

    public DcStbEmulator(String timeout) {
        super(timeout);
    }

    public boolean registerConsumer() {
        // TODO fix later
        initializeSTBEmulator();
        return emulator.registerConsumer(stbDataReceived, HandlerType.Settings);
        //return false;
    }
}
