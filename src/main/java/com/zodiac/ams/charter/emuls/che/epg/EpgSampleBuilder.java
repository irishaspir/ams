package com.zodiac.ams.charter.emuls.che.epg;

import com.zodiac.ams.charter.emuls.CheEmulatorConfig;
import com.zodiac.ams.charter.http.helpers.epg.csv.CsvDataUtils;
import com.zodiac.ams.charter.services.epg.EpgOption;
import com.zodiac.ams.charter.services.epg.EpgOptions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.zodiac.ams.charter.services.epg.EpgOptions.SERVICE_ID;

public class EpgSampleBuilder {

    private CheEmulatorConfig cheEmulatorConfig=new CheEmulatorConfig();
    private CsvDataUtils csvDataUtils = new CsvDataUtils();

    private String filename = cheEmulatorConfig.getEpgFile();
    private String[] serviceId;
    private int amountRows;
    private int amountServices;
    private String startTime = "00:00";

    public String createCsvOnlyHead() {
        StringBuilder str = null;
        try {
            new File( filename).createNewFile();
            FileWriter file = new FileWriter(filename);
            str = new StringBuilder();
            for (EpgOptions one : Arrays.asList(EpgOptions.values())) {
                str.append(one.getOption().getName() + "|");
            }
            str.deleteCharAt(str.lastIndexOf("|"));
            str.append("\n");
            file.append(str);
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    private String getServiceId() {
        return csvDataUtils.getServiseId();
    }

    public String[] getServiceIdList() {
        return serviceId;
    }

    public int getCountRows() {
        return amountRows;
    }

    public int getCountServices() {
        return amountServices;
    }

    public String createCsv(String count, String duration) {
        return createCsv(0, 1, count, duration, "0", "0");
    }

    public String createCsv(String count) {
        return createCsv(0, 1, count, "0", "0", "0");
    }

    public String createCsv(String count, int amountDay) {
        return createCsv(amountDay, 1, count, "0", "0", "0");
    }

    public String createCsv(String count, String duration, int amountDay) {
        return createCsv(amountDay, 1, count, duration, "0", "0");
    }

    public String createCsv(String count, String duration, String breakTime, String breakPeriod) {
        return createCsv(0, 1, count, duration, breakTime, breakPeriod);
    }

    public String createCsv(String count, String breakTime, String breakPeriod) {
        return createCsv(0, 1, count, "0", breakTime, breakPeriod);
    }

    public String createCsv(int amountServiceId, String count, String duration) {
        return createCsv(0, amountServiceId, count, duration, "0", "0");
    }

    public String createCsv(int amountServiceId, String count) {
        return createCsv(0, amountServiceId, count, "0", "0", "0");
    }

    public String createCsv(int amountDay, int amountServiceId, String count, String duration, String breakTime, String breakPeriod) {
        String csvFite = "";
        createCsvOnlyHead();
        serviceId = new String[amountServiceId];
        amountRows = Integer.parseInt(count);
        amountServices = amountServiceId;
        for (int i = 0; i < amountServiceId; i++) {
            csvFite = csvFite + writeDataInCsvFile(createData(amountDay, count, duration, breakTime, breakPeriod), true);
            serviceId[i] = getServiceId();
        }
        return csvFite;
    }

    private StringBuilder createData(int amountDay, String count, String duration, String breakTime, String breakPeriod) {
        return csvDataUtils.createDate(startTime, amountDay, duration, count, breakTime, breakPeriod);
    }

    public String writeDataInCsvFile(StringBuilder data, boolean append) {
        try {
            FileWriter file = new FileWriter(filename, append);
            file.append(data);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data.toString();

    }

    private void createOptions() {

        ArrayList<EpgOption> option = EpgOption.create()
                .addOptions(SERVICE_ID)
                .build();


    }

}
