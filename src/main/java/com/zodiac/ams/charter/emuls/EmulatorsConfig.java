package com.zodiac.ams.charter.emuls;


import com.zodiac.ams.charter.rudp.RudpHelper;
import com.zodiac.ams.common.common.AbstractPropertyHelper;

import java.net.URL;

/**
 * Created by SpiridonovaIM on 30.01.2017.
 */
public class EmulatorsConfig extends AbstractPropertyHelper {


    protected EmulatorsConfig(URL configPropertyName) {
        super(configPropertyName);
    }

    protected EmulatorsConfig(String configPropertyName) {
        super(configPropertyName);
    }

    public static String takeConfigFileFromResource(String name) {
        return RudpHelper.class.getClassLoader().getResource(name).getFile();
    }



}
