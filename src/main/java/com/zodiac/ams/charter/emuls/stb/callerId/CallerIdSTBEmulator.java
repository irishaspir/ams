package com.zodiac.ams.charter.emuls.stb.callerId;

import com.dob.test.charter.handlers.HandlerType;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;


public class CallerIdSTBEmulator extends STBEmulator {

    public CallerIdSTBEmulator() {
        super();
    }

    public boolean registerConsumer() {
        initializeSTBEmulator();
        return emulator.registerConsumer(stbDataReceived, HandlerType.CallerId);
    }

 }