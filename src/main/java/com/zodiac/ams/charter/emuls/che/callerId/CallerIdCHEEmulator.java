package com.zodiac.ams.charter.emuls.che.callerId;


import com.dob.test.charter.iface.HEHandlerType;
import com.zodiac.ams.charter.emuls.che.CHEEmulator;
import com.zodiac.ams.common.logging.Logger;


import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CallerIdCHEEmulator extends CHEEmulator {
    public CallerIdCHEEmulator() {
        super();
    }

    public boolean registerConsumer() {
        cheDataReceived.cleanData();
        initializeCHEEmulator();
        return emulator.registerDataConsumer(cheDataReceived, HEHandlerType.CALLER_ID);
    }
    public String getSubscribeRequestBody() {
        byte[] responseBody = getData();
        Logger.info("RESPONSE " + responseBody);
        if (responseBody == null) return null;
        else return new String(responseBody);
    }

    private List<String> getSubscribeRequestListBody() {
        List<byte[]> dataList= getDataList(2);
        List<String> responseBodyList = new ArrayList<>();
        if (dataList == null) return null;
        else {
            dataList.forEach(data -> responseBodyList.add(new String(data)));
            responseBodyList.forEach( responseBody -> Logger.info("RESPONSE: " + responseBody));
        }
        Collections.sort(responseBodyList);
        return responseBodyList;
    }

    public String getSubscribeFirstRequestBody(){
        List<String> responseBodyList = getSubscribeRequestListBody();
        if (responseBodyList == null) return null;
        else return responseBodyList.get(0);
    }

    public String getSubscribeSecondRequestBody(){
        List<String> responseBodyList = getSubscribeRequestListBody();
        if (responseBodyList == null) return null;
        else return responseBodyList.get(1);
    }

    public Object getFirstURI() {
        List<URI> uriList = getURIList(2);
        if (uriList == null) return null;
        else return uriList.get(0).toString().trim();
    }

    public Object getSecondURI() {
        List<URI> uriList = getURIList(2);
        if (uriList == null) return null;
        else return uriList.get(1).toString().trim();
    }

}

