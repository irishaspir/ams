package com.zodiac.ams.charter.emuls.stb.dsg;

import com.dob.test.charter.handlers.HandlerType;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

/**
 * Created by SpiridonovaIM on 21.04.2017.
 */
public class DSGSTBEmulator extends STBEmulator {

    public DSGSTBEmulator() {
        super();
    }

    public DSGSTBEmulator(String timeout) {
        super(timeout);
    }

    public boolean registerConsumer() {
        initializeSTBEmulator();
        return emulator.registerConsumer(stbDataReceived, HandlerType.Settings);
    }
}
