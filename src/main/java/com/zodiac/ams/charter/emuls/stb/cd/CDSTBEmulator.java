package com.zodiac.ams.charter.emuls.stb.cd;

import com.dob.test.charter.handlers.HandlerType;
import com.zodiac.ams.charter.emuls.stb.STBEmulator;

/**
 * Created by SpiridonovaIM on 21.04.2017.
 */
public class CDSTBEmulator extends STBEmulator {

    public CDSTBEmulator() {
        super();
    }

    public CDSTBEmulator(String timeout) {
        super(timeout);
    }

    public CDSTBEmulator(int id, int code, int params) {
        super(id, code, params);
    }

    public boolean registerConsumer() {
        initializeSTBEmulator();
        return emulator.registerConsumer(stbDataReceived, HandlerType.CompanionDevices);
    }
}
