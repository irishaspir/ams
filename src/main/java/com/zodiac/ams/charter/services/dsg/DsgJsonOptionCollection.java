package com.zodiac.ams.charter.services.dsg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SpiridonovaIM on 16.08.2017.
 */
public class DsgJsonOptionCollection {

    private ArrayList<DsgJsonOption> list = new ArrayList<>();

    public DsgJsonOptionCollection addOption(DsgJsonOption option) {
        list.add(new DsgJsonOption(option.getName(), option.getColumnName(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public DsgJsonOptionCollection addOptions(DsgJsonOptions option) {
        list.add(new DsgJsonOption(option.getOption().getName(), option.getOption().getColumnName(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<DsgJsonOption> build() {
        return list;
    }

    public DsgJsonOptionCollection addListOptions(List<DsgJsonOption> list) {
        this.list.addAll(list);
        return this;
    }

}
