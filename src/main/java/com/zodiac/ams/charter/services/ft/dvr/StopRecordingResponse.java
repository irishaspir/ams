package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import org.json.JSONArray;

public class StopRecordingResponse implements IDVRObject {
    public static final int NO_VALUE = -1;
    
    private Error error = Error.Error0;
    private List<Integer> ids;
    private List<Error> statuses;
    private SpaceStatus spaceStatus;
    private Integer size;
    
    public StopRecordingResponse(StopRecordingRequest request, List<Error> statuses, SpaceStatus spaceStatus){
        ids = new ArrayList<>();
        List<StopRecordingRequestParam> params = request.getParams();   
        for (StopRecordingRequestParam param: params)
            ids.add(JSONUtils.getInt(param.getRecordingId()));
        this.statuses = statuses;
        this.spaceStatus = spaceStatus;
    }
    
    public StopRecordingResponse(StopRecordingResponse response){
        this.error = response.error;
        if (null != response.ids){
            this.ids = new ArrayList<>();
            this.ids.addAll(response.ids);
        }
        if (null != response.statuses){
            this.statuses = new ArrayList<>();
            this.statuses.addAll(response.statuses);
        }
        if (null != response.spaceStatus)
            this.spaceStatus = new SpaceStatus(response.spaceStatus);
    }
    
    public void clearIdsStatuses(){
        ids = new ArrayList<>();
        statuses = new ArrayList<>();
    }
    
    public void setSpaceStatus(SpaceStatus spaceStatus){
        this.spaceStatus = spaceStatus;
    }
    
    public void setError(Error error){
        this.error = error;
    }
    
    public int size(){
        return null == statuses ? 0 : statuses.size();
    }
    
    public void setSize(Integer size){
        this.size = size;
    }
    
    public Error getStatus(int index){
        return null == statuses ? null : statuses.get(index);
    }
    
    public List<Error> getStatuses(){
        return statuses;
    }
    
    public SpaceStatus getSpaceStatus(){
        return spaceStatus;
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        for (int i=0; i< ids.size(); i++){
            JSONObject obj = new JSONObject();
            obj.put("recordingId", ids.get(i));
            if (null != error && Error.Error0 != error)
                obj.put("statusCode", error.getCodeHE());
            else
                obj.put("statusCode", statuses.get(i).getCodeHE());
            arr.put(obj);
        }
        json.put("result", arr);
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(error.getCodeSTB()));
            if (null != size){
                if (NO_VALUE != size)
                    out.write(DOB7bitUtils.encodeUInt(size));
            }
            else
                out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(ids.size())));
            for (int i=0; i < ids.size(); i++)
                out.write(DOB7bitUtils.encodeUInt(statuses.get(i).getCodeSTB()));
            if (null != spaceStatus)
                out.write(spaceStatus.getData());
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
