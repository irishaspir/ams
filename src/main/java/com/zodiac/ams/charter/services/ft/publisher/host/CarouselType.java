package com.zodiac.ams.charter.services.ft.publisher.host;


public enum CarouselType {
    HTTP("HTTP"),BATCH_1("BATCH_1"),BATCH_2("BATCH_2"),ST("ST"),DNCS("DNCS"),DNCS_PREFIX("DNCS_PREFIX"),CDN("CDN"),
        UNKNWON("UNKNWON"),PPV("PPV");
    
    private final String type;
    
    private CarouselType(String type){
        this.type = type;
    }
    
    @Override
    public String toString(){
        return type;
    }
    
}
