package com.zodiac.ams.charter.services.ft.publisher.carousel;

public enum CarouselSTType {
    ETV("ETV"),OCAP("OCAP"),DB("DB");
    private final String type;
    
    private CarouselSTType(String type){
        this.type = type;
    }
}
