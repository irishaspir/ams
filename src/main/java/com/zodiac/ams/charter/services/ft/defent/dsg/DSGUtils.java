package com.zodiac.ams.charter.services.ft.defent.dsg;

import java.util.StringTokenizer;

public class DSGUtils {
    
    public static String ip2string(int ip) {
        return (ip >>> 24) + "." + (ip >>> 16 & 0xFF) + "." + (ip >>> 8 & 0xFF) + "." + (ip & 0xFF);
    }

    public static int string2ip(String ip) {
        int x = 0;
        StringTokenizer st = new StringTokenizer(ip, ".");
        while (st.hasMoreTokens()){
            x <<= 8;
            x |= Integer.parseInt(st.nextToken());
        }
	return x;
    }
}
