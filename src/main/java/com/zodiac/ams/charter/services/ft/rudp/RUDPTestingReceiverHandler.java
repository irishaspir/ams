package com.zodiac.ams.charter.services.ft.rudp;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessagePackage;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckDataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckResponsePacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckStartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacketSerializer;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacketSerializer;
import com.zodiac.ams.charter.services.ft.rudp.packet.PacketType;
import com.zodiac.ams.charter.services.ft.rudp.packet.ResetErrorResponsePacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacketSerializer;
import com.zodiac.ams.charter.services.ft.rudp.packet.WrongSessionIdErrorResponsePacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPReceiverSession;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSenderSession;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSession;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Set;
import java.util.zip.CRC32;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import com.dob.ams.transport.zodiac.util.ZodiacTransportUtils;

public class RUDPTestingReceiverHandler extends SimpleChannelUpstreamHandler {
    private static final Logger log = LoggerFactory.getLogger(RUDPTestingReceiverHandler.class);
    
    private final RUDPTestingSendManager sender;
    private IAckHandler handler;
    
    
    public RUDPTestingReceiverHandler(RUDPTestingSendManager sender){
        this.sender = sender;
    }
    
    void setAckHandler(IAckHandler handler){
        this.handler = handler;
    }
    
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (e.getMessage() instanceof ChannelBuffer) {
            ChannelBuffer channelBuffer = (ChannelBuffer) e.getMessage();
            channelBuffer.markReaderIndex();
            byte packetTypeByte = channelBuffer.readByte();
            PacketType pt = PacketType.fromByte(packetTypeByte);
            if (pt == null) {
                byte[] bytes = new byte[channelBuffer.readableBytes()+1];
                bytes[0] = packetTypeByte;
                channelBuffer.readBytes(bytes, 1, channelBuffer.readableBytes());
                log.warn("RUDP TESTING CLIENT: unknown packet type, remote address: {}, bytes: {}, sending upstream w/o rudp decoding", e.getRemoteAddress(),  Arrays.toString(bytes));
                channelBuffer.resetReaderIndex();
                ctx.sendUpstream(e);
                return;
            }
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
            baos.write(packetTypeByte);

            while (channelBuffer.readable()) {
                baos.write(channelBuffer.readByte());
            }
            
            byte[] bytes = baos.toByteArray();
            InetSocketAddress remoteAddress = (InetSocketAddress) e.getRemoteAddress();
            switch (pt) {
                case START: //disabled by default
                    if (null != handler)
                        handleStartPacket(bytes, ctx, remoteAddress);
                    break;
                case DATA:  //disabled by default
                    if (null != handler)
                        handleDataPacket(bytes, ctx, e, remoteAddress);
                    break;
                case ACK:
                    handleAckPacket(bytes, ctx, remoteAddress);
                    break;
                case ERROR:
                    handleErrorPacket(bytes, ctx, remoteAddress);
                    break;
                default:
                    log.error("RUDP TESTING CLIENT: unhandled packet type {} received, bytes: {}, sending to upstream w/o rudp decoding", pt, Arrays.toString(bytes));
                    channelBuffer.resetReaderIndex();
                    ctx.sendUpstream(e);
            }     
        } 
        else {
            log.error("RUDP TESTING CLEINT: receiver can handle only ChannelBuffer messages, not of class {}, sending to upstream w/o rudp decoding", e.getMessage().getClass().getSimpleName());
            ctx.sendUpstream(e);
        }
    }
    
   
    
    /**
     * Handle DATA packet, used when client receives messages from AMS
     * 
     * @param packetBytes
     * @param ctx
     * @param remoteAddress 
     */
    private void handleDataPacket(byte[] packetBytes, ChannelHandlerContext ctx, MessageEvent e, InetSocketAddress remoteAddress) {
        try {
            DataPacket dataPacket = DataPacketSerializer.parse(packetBytes);
            if (null == dataPacket ) {
                log.error("[RUDP TESTING] error when parsing DATA packet, channel: {}, bytes: {}", ctx.getChannel(), Arrays.toString(packetBytes));
            }
            log.debug("[RUDP TESTING] get DATA {} from address: {} ", dataPacket, remoteAddress);
            
            int sessionId = dataPacket.getFileId();
            IRUDPSession sess = getSession(sessionId);
            
            if (null == sess) {
                    log.warn("[RUDP TESTING] cannot retrieve session for DATA: {} address: {}", dataPacket, remoteAddress);
                    enqueueWrongSessionIdErrorResponse(remoteAddress, dataPacket);
            } 
            else {
                if (sess instanceof IRUDPReceiverSession) {
                    IRUDPReceiverSession session = (IRUDPReceiverSession) sess;
                    
                    int result = IAckHandler.RECEIVE_DATA | IAckHandler.SEND_DATA_ACK;
                    
                    if (null != handler)
                        result = handler.handleDataAck(session, dataPacket);
                       
                    if (0 == (result & IAckHandler.RECEIVE_DATA))
                        log.debug("[RUDP TESTING {}] Receiving DATA is REJECTED : {}", sessionId, dataPacket);
                    else
                        session.add(dataPacket);
                    
                    if (0 == (result & IAckHandler.SEND_DATA_ACK))
                        log.debug("[RUDP TESTING {}] Sending DATA ACK is REJECTED : {}", sessionId, dataPacket);
                    else
                        enqueueDataAck(remoteAddress, dataPacket, session);
                    
                    if (0 == result)
                        return;

                    if (session.allSegmentsReceived() && session.tryMarkUpstreamSent()) {
                        // this is bytes of the message
                        byte[] bytes = session.getBytesWoLast4();
                        // this is bytes of CRC32 value
                        byte[] crc32bytes = session.getLast4Bytes();
                        CRC32 crc32 = new CRC32();
                        crc32.update(bytes);
                        long crc32val = crc32.getValue();
                                       
                        ZodiacMessagePackage pckg = ZodiacTransportUtils.decodePackage(bytes);
                        ZodiacMessage msg = (ZodiacMessage)pckg.getMessages().get(0);
                        
                        log.debug("[RUDP TESTING {}] ZODIAC PACKET is received: {}", sessionId, pckg);
                        
                        int idS = sender.createSenderSession(pckg.getMac(),msg);
                        sender.sendZodiacPackage(idS);
                    } 
                    else {
                        if (session.isUpstreamSent()) {
                            log.debug("[RUDP TESTING {}] ZODIAC PACKET is already received. DATA packet:{}", sessionId, dataPacket);
                        }
                    }
                } 
                else {
                    log.error("[RUDP TESTING {}] session is not RUDP1ReceiverSession", sessionId);
                }
            }
        } catch (Throwable t) {
            log.error("[RUDP TESTING] error when handling DATA packet from address: {}, channel: {}", remoteAddress, ctx.getChannel());
        }
    }
    
    
    /**
     * Handle ACK packets, used when client sends messages to AMS
     * @param packetBytes
     * @param ctx
     * @param remoteAddress
     * @throws IOException 
     */
    private void handleAckPacket(byte[] packetBytes, ChannelHandlerContext ctx, InetSocketAddress remoteAddress) throws IOException {
        byte ackPacketTypeByte = packetBytes[1];
        PacketType ackPt = PacketType.fromByte(ackPacketTypeByte);
        if (ackPt == null) {
            log.warn("[RUDP TESTING] ACK PacketType is unknown, byte: {}, channel: {}", ackPacketTypeByte, ctx.getChannel());
            return;
        }
        switch (ackPt) {
            case START: {
                AckStartPacket ackStartPacket = StartPacketSerializer.parseAck(packetBytes);
                if (ackStartPacket == null) {
                    log.error("[RUDP TESTING] cannot parse ACK-START: bytes={} address={}", Arrays.toString(packetBytes), remoteAddress);
                    return;
                }
                log.debug("[RUDP TESTING] get ACK-START {} from address {}", ackStartPacket, remoteAddress);
                int sessionId = ackStartPacket.getFileId();
                
                IRUDPSession sess = getSession(sessionId);
                if (null == sess ) {
                    log.warn("[RUDP TESTING {}] cannot retrieve session for ACK-START {} address: {}", sessionId, ackStartPacket, remoteAddress);
                    enqueueWrongSessionIdErrorResponse(remoteAddress, ackStartPacket);
                } 
                else {
                    if (sess instanceof IRUDPSenderSession) {
                        IRUDPSenderSession session = (IRUDPSenderSession)sess;
                        session.startAckReceived(true);
                        RUDPTestingSession ts = sender.getTestingSession(sessionId);
                        ts.setAckStartPacket(ackStartPacket, true);
                    } 
                    else 
                        log.error("[RUDP TESTING {}] session is not RUDP1SenderSession", sessionId);
                }
                break;
            }
            case DATA: {
                AckDataPacket ackDataPacket = DataPacketSerializer.parseAck(packetBytes);
                if (ackDataPacket == null) {
                    log.error("[RUDP TESTING] error when parsing ACK-DATA packet, bytes: {}, channel: {}", Arrays.toString(packetBytes), ctx.getChannel());
                    return;
                }
                log.debug("[RUDP TESTING] get ACK-DATA {} from {}", ackDataPacket, remoteAddress);
                
                int sessionId = ackDataPacket.getFileId();
                
                IRUDPSession sess = getSession(sessionId);
                if (null == sess) {
                    log.warn("[RUDP TESTING {}] cannot retrieve session for ACK-DATA {} address: {}", sessionId, ackDataPacket, remoteAddress);
                    enqueueWrongSessionIdErrorResponse(remoteAddress, ackDataPacket);    
                } 
                else {
                    if (sess instanceof IRUDPSenderSession) {
                        IRUDPSenderSession session = (IRUDPSenderSession)sess;
                        session.dataAckReceived(ackDataPacket);
                    } 
                    else 
                        log.error("[RUDP TESTING {}] session is not RUDP1SenderSession", sessionId);
                }
                break;
            }
        }
    }
            
   
    /**
     * Handle ERROR packets, used when client receives and sends messages from/to AMS
     * @param packetBytes
     * @param ctx
     * @param remoteAddress
     * @throws Exception 
     */
    private void handleErrorPacket(byte[] packetBytes, ChannelHandlerContext ctx, InetSocketAddress remoteAddress) throws Exception {
        byte packetType = packetBytes[0];
        PacketType pt = PacketType.fromByte(packetType);
        if (pt != PacketType.ERROR) {
            throw new IllegalStateException("[RUDP TESTING] Packet should have ERROR type!");
        }
        
        ErrorPacket errorPacket = ErrorPacketSerializer.parseErrorPacket(packetBytes);
        int sessionId = errorPacket.getFileId();

        log.debug("[RUDP TESTING {}] get ERROR {} from address {} ", sessionId, errorPacket, remoteAddress);

        IRUDPSession sess = getSession(sessionId);
        if (null == sess) {
            log.warn("[RUDP TESTING {}] cannot retrieve session for ERROR {}", sessionId, errorPacket);
            enqueueWrongSessionIdErrorResponse(remoteAddress, errorPacket);
        } 
        else {
            if (sess instanceof IRUDPSenderSession) {
                IRUDPSenderSession session = (IRUDPSenderSession)sess;
                session.setErrorPacket(errorPacket);
            } 
            else 
            if (sess instanceof IRUDPReceiverSession) {
                IRUDPReceiverSession session = (IRUDPReceiverSession)sess;
            }
        }
    }
    
    
    /**
     * Handle START packet, used when client receives messages from AMS 
     * @param packetBytes
     * @param ctx
     * @param remoteAddress 
     */
    private void handleStartPacket(byte[] packetBytes, ChannelHandlerContext ctx, InetSocketAddress remoteAddress) {
        try {
            StartPacket startPacket = StartPacketSerializer.parse(packetBytes);
            log.debug("[RUDP TESTING] get START {} from address {}", startPacket, remoteAddress);
            if (null != handler && !handler.handleStartAck(startPacket)){
                log.debug("[RUDP TESTING] START is REJECTED : {}",startPacket);
                return;
            }
            sender.createReceiverSession(startPacket);
            enqueueStartAck(remoteAddress, startPacket);
        } 
        catch (Throwable t) {
            log.error("[RUDP TESTING] START packet error: {} channel: {} address: {}", t, ctx.getChannel(), remoteAddress);
        }
    }
    
    
    private void enqueueDataAck(InetSocketAddress remoteAddress, DataPacket dataPacket, IRUDPReceiverSession session) {
        Set<Integer> set = session.getReceivedSegments();
        if (!CollectionUtils.isEmpty(set)) {
            AckDataPacket ack = new AckDataPacket(dataPacket.getFileId(), set);
            ack.setRemoteAddress(remoteAddress);
            sender.sendResponse(ack);
        } 
        else {
            log.warn("[RUDP TESTING] do not send ACK because get NULL when trying to get received segments for DATA packet: {}. ", dataPacket);
        }
    }

    private void enqueueStartAck(InetSocketAddress remoteAddress, StartPacket startPacket) {
        AckStartPacket ack = new AckStartPacket(startPacket);
        ack.setRemoteAddress(remoteAddress);
        sender.sendResponse(ack);
    }
     
     private void enqueueWrongSessionIdErrorResponse(InetSocketAddress remoteAddress, DataPacket dataPacket) {
        WrongSessionIdErrorResponsePacket errorPacket = new WrongSessionIdErrorResponsePacket();
        errorPacket.setFileId(dataPacket.getFileId());
        errorPacket.setRemoteAddress(remoteAddress);
        sender.sendResponse(errorPacket);
    }

    private void enqueueWrongSessionIdErrorResponse(InetSocketAddress remoteAddress, AckResponsePacket ackResponsePacket) {
        WrongSessionIdErrorResponsePacket errorPacket = new WrongSessionIdErrorResponsePacket();
        errorPacket.setFileId(ackResponsePacket.getFileId());
        errorPacket.setRemoteAddress(remoteAddress);
        sender.sendResponse(errorPacket);
    }

    private void enqueueWrongSessionIdErrorResponse(InetSocketAddress remoteAddress, ErrorPacket receivedErrorPacket) {
        WrongSessionIdErrorResponsePacket errorPacket = new WrongSessionIdErrorResponsePacket();
        errorPacket.setFileId(receivedErrorPacket.getFileId());
        errorPacket.setRemoteAddress(remoteAddress);
        sender.sendResponse(errorPacket);
    }

    private void enqueueResetErrorResponse(InetSocketAddress remoteAddress, AckResponsePacket ackResponsePacket) {
        ResetErrorResponsePacket errorPacket = new ResetErrorResponsePacket();
        errorPacket.setFileId(ackResponsePacket.getFileId());
        errorPacket.setRemoteAddress(remoteAddress);
        sender.sendResponse(errorPacket);
    }
    
}
