
package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.common.ssh.SSHHelper;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.*;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class CarouselSFTP extends CarouselImpl{
    
    final SSHHelper executor;
    final String location;
    final String rootTemplate; 
    
    public CarouselSFTP(CarouselHost host,SSHHelper executor,String location,String rootTemplate,boolean isZipped){
        super(host.getServerType(), isZipped);
        this.host = host;
        this.executor = executor;
        this.location = location;
        this.rootTemplate = rootTemplate; 
    }
    
    public CarouselSFTP(CarouselHost host,SSHHelper executor,String location,boolean isZipped){
        super(host.getServerType(), isZipped);
        this.host = host;
        this.executor = executor;
        this.location = location;
        this.rootTemplate = null;
    }

    @Override
    public boolean putFileOnCarousel(String localName, String remoteName) {
        String remote = executor.getHomeDir()+"/"+location+"/"+remoteName;
        return executor.putFile(localName, remote);
    }

    @Override
    public String getCarouselFile(String remoteName) {
        String local = getRemoteTmpName(remoteName);
        String remote = executor.getHomeDir()+"/"+location+"/"+remoteName;
        executor.getFile(local, remote);
        return getLocalFile(local);
    }
    
    @Override
    public String getZippedCarouselFile(String remoteName,String entryName){
        String localZip = getRemoteTmpName(remoteName);
        String localEntry = getRemoteTmpName(entryName);
        
        String remote = executor.getHomeDir()+"/"+location+"/"+remoteName;
        executor.getFile(localZip, remote);
        String result = getLocalZippedFile(localZip,entryName);
        
        try{
            Files.write(new File(localEntry).toPath(), result.getBytes());
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return null;
        }
        return result;
    }
    
    @Override
    public boolean deleteCarouselFile(String remoteName) {
        String remote = executor.getHomeDir()+"/"+location+"/"+remoteName;
        boolean ok = executor.exec("rm "+remote);
        if (ok){
            String content = getCarouselFile(remoteName);
            ok = null == remote || "".equals(content.trim());
        }
        return ok;
    }
    
    @Override
    public boolean createRemoteCarouselDir(){
        String home = executor.getHomeDir();
        boolean ok = executor.exec("cd "+home+"; rm -rf "+location+"; mkdir "+location+"; chmod 777 "+location);
        if (ok){
            if (isZipped){
                String target = getLocalTmpName(ZDTS_INI);
                ok = zipFile(getName(ZDTS_INI), target, ZDTS_INI);
                if (ok)
                    ok=executor.putFile(target,home+"/"+location+"/"+ZDTS_INI_ZIP);
                /*
                if (ok && null != rootTemplate){
                    target = getLocalTmpName(rootTemplate);
                    ok = zipFile(getName(rootTemplate), target, ROOT_ZDTS_INI);
                    if (ok)
                        ok=executor.putFile(target,home+"/"+location+"/"+ROOT_ZDTS_INI_ZIP);
                }
                */
            }
            else {
                ok = executor.putFile(getName(ZDTS_INI),home+"/"+location+"/"+ZDTS_INI);
                //if (ok && null != rootTemplate)
                //    ok = executor.putFile(getName(rootTemplate),home+"/"+location+"/"+ROOT_ZDTS_INI);
            }
            
            if (ok && null != rootTemplate)
                ok = executor.putFile(getName(rootTemplate),home+"/"+location+"/"+ROOT_ZDTS_INI);
        }
        return ok;
    }
    
    @Override
    public String getLocation(String name){
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch (version){
            case VERSION_1:
            case VERSION_2:
                return "http://"+getHost()+":8080/prefetch/"+getUid()+"/"+name;
            default:
                return "http://hostname:8080/prefetch/"+name;
        }
    }
    
}
