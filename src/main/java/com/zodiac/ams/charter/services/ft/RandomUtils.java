package com.zodiac.ams.charter.services.ft;

import com.zodiac.ams.charter.services.ft.defent.ApplicationId;
import com.zodiac.ams.charter.services.ft.defent.ChannelType;
import com.zodiac.ams.charter.services.ft.defent.Reason;
import com.zodiac.ams.charter.services.ft.defent.ScreenId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomUtils {
    private static final int MIN_RANDOM_INT = 1;
    private static final int MAX_RANDOM_INT = 101;
    private final static int MIN_LENGTH = 1;
    private final static int MAX_LENGTH = 10;
    
    public static int getRandomInt(){
        return new Random().nextInt(MAX_RANDOM_INT);
    }
    
    public static boolean getRandomBoolean(){
        return (0 != new Random().nextInt(2));
    }
    
    public static <T> T getRandomObject(T[] values){
        return getRandomObject(values, 1, false)[0];
    }
    
    
    public static <T> T[] getRandomObject(T[] values, int count, boolean distinct){
        Random random = new Random();
        
        List<Object> source = new ArrayList<>();
        source.addAll(Arrays.asList(values));
        
        T[] target = Arrays.copyOf(values, count);
        
        for (int i=0; i<count;  i++){
            int index = random.nextInt(source.size());
            target[i] = (T)source.get(index);
            if (distinct)
                source.remove(index);
        }
        
        return target;
    }
    
    
    public static ChannelType getRandomChannelType(){
        return RandomUtils.getRandomObject(ChannelType.values());
    }
    
    public static Reason getRandomReason(){
        return RandomUtils.getRandomObject(Reason.values());
    }
    
    public static ScreenId getRandomScreenId(){
        return RandomUtils.getRandomObject(ScreenId.values());
    }
    
    public static ApplicationId getRandomApplicationId(){
        return RandomUtils.getRandomObject(ApplicationId.values());
    }
    
    public static int getRandomInt(int min, int max){
        Random random =  new Random();
        return min + random.nextInt(max-min+1);
    }
    
    
    public static int[] getRandomIntArray(){
        return getRandomIntArray(MIN_LENGTH, MAX_LENGTH, MIN_RANDOM_INT, MAX_RANDOM_INT, true);
    }
    
    public static int[] getRandomIntArray(int minCount, int maxCount, int minValue, int maxValue, boolean distinct){
        int count = getRandomInt(minCount, maxCount);
        List<Integer> list = new ArrayList<>();
        for (int i=0; i<count; i++){
            while (true){
                int rnd  = getRandomInt(minValue, maxValue);
                if (!distinct || !list.contains(rnd)){
                    list.add(rnd);
                    break;
                }
            }
        }
        int [] result = new int[list.size()];
        int index = 0;
        for (Integer i: list)
            result[index++] = i;
        return result;
    }
    
    public static String getRandomIp(){
        return "192" + "." + getRandomInt(0,100) + "." + getRandomInt(0,100) + "." + getRandomInt(1,100);
    }
}
