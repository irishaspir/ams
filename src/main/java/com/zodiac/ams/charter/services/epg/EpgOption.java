package com.zodiac.ams.charter.services.epg;

/**
 * Created by SpiridonovaIM on 23.12.2016.
 */
public class EpgOption {

    private String name;
    private String columnName;
    private String value;
    private String defaultValue;
    private String[] range;


    public EpgOption(String name, String columnName, String values, String defaultValue, String[] range) {
        this.name = name;
        this.columnName = columnName;
        this.value = values;
        this.defaultValue = defaultValue;
        this.range = range;
    }

    public EpgOption() {
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String[] getRange() {
        return range;
    }


    public void setValue(String value) {
        this.value = value;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setRange(String[] range) {
        this.range = range;
    }

    public static EpgOptionsCollection create() {
        return new EpgOptionsCollection();
    }

}
