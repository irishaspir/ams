package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.sandt.bx.data.TSName;
import com.sandt.bx.databroadcast.api.DataBroadcastApplicationsAPI;
import com.sandt.bx.databroadcast.client.DataBroadcastApplicationsClient;
import com.sandt.util.ReportingException;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getRemoteTmpName;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselST.logger;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class CarouselST_DB extends CarouselST{
    private DataBroadcastApplicationsAPI dbApplicationsAPI;
    
    public CarouselST_DB(CarouselHost host,boolean isZipped) {
        super(host,isZipped);                
        dbApplicationsAPI = new DataBroadcastApplicationsClient(transportManager);
    }

    @Override
    public boolean deleteApplication() {
        try {
            dbApplicationsAPI.deleteApplication(new TSName(application));        
            return true;
        }
        catch(ReportingException ex){
            logger.error("deleteApplication error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        } 
    }

    @Override
    public boolean exportApplication() {
        try (BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(application));){
            dbApplicationsAPI.exportApplication(new TSName(application), out);
            return true;
        }
        catch(ReportingException ex){
            logger.error("exportApplication error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        }
        catch(Exception ex){
            logger.error("exportApplication error: {} application: {}", ex.getMessage(), application);
            return false;
        }
    }

    @Override
    public boolean putFileOnCarousel(String local, String remoteName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getCarouselFile(String remoteName) {
        try {
            byte [] bytes =  dbApplicationsAPI.retrieveFileContent(new TSName(application), remoteName);
            Files.write(new File(getRemoteTmpName(remoteName)).toPath(),bytes);
            return new String(bytes);
        }
        catch (ReportingException ex){
            logger.error("getCarouselFile error: {}  {} application: {} file: {}", ex.getDetailMessage() ,ex.getMessage(), application, remoteName);
            return null;
        }
        catch (Exception ex){
            logger.error("getCarouselFile error: {} application: {} file: {}", ex.getMessage(), application, remoteName);
            return null;
            
        }
    }
    
    @Override
    public String getZippedCarouselFile(String remoteName,String entryName){
        try {
            byte [] bytes =  dbApplicationsAPI.retrieveFileContent(new TSName(application), remoteName);
            return getZippedCarouselFile(bytes,remoteName,entryName);
        }
        catch (ReportingException ex){
            logger.error("getZippedCarouselFile error: {}  {} application: {} file: {}", ex.getDetailMessage() ,ex.getMessage(), application, remoteName);
            return null;
        }
        catch (Exception ex){
            logger.error("getZippedCarouselFile error: {} application: {} file: {}", ex.getMessage(), application, remoteName);
            return null;        
        }
    }

    @Override
    public boolean deleteCarouselFile(String remoteName) {
        try {
            dbApplicationsAPI.removeFile(new TSName(application),remoteName);        
            return true;
        }
        catch(ReportingException ex){
            logger.error("deleteCarouselFile error: {} {} application: {} file: {}", ex.getDetailMessage(), ex.getMessage(), application, remoteName);
            return false;
        }
    }

    @Override
    public boolean createRemoteCarouselDir() {
        String template = application+"_template"+(host.isRootNode() ? "_root" : "")+(isZipped ? "_zip":"");
        try{
            deleteApplication();
            dbApplicationsAPI.copyApplication(new TSName(template), new TSName(application), true);
            return true;
        }
        catch(ReportingException ex){
            logger.error("createRemoteCarouselDir error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        }
    }
    
}
