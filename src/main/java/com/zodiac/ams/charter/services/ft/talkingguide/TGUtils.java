package com.zodiac.ams.charter.services.ft.talkingguide;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TGUtils {
    
    public static String parseString(String string,String confName) {
        Properties props=new Properties();
        if (null != confName){
            try (BufferedReader in= new BufferedReader(new FileReader(confName))){
                props.load(in);
            }
            catch(IOException ex){
            }
        }
        
        String result = string;
        Pattern p = Pattern.compile("[^\\{\\}]*(\\{[^\\{\\}]*\\}).*");
        
        while (true){
             Matcher m = p.matcher(result);
             if (!m.matches())
                 break;
             int start = m.start(1);
             int end = m.end(1);
             String key = m.group(1);
             String value = props.getProperty(key);
             if (null == value)
                 value = key.substring(1,key.length()-1);
             result = result.substring(0,start)+value+result.substring(end);
         }
        return result;
    }
    
    
    public static String parseHTTPRequestString(String request){
        return request.replace(" ","%20").replace("{","%7B").replace("}","%7D");
        
    }    
}
