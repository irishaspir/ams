package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;

public class WholeDataRecorded implements IDVRSerializable {
    private Error error = Error.Error0;
    private List<Integer> recordedVector;
    private Long crc;
    private SpaceStatus spaceStatus;
    private List<Integer> priorityVector;
    private List<Integer> deletedVector;
    
    public WholeDataRecorded(){
        recordedVector = new ArrayList<>();
        
        CRC32 crc = new CRC32();
        crc.update(String.valueOf(getRandomInt(1, 1_000_000)).getBytes());
        this.crc = crc.getValue();
        
        spaceStatus = SpaceStatus.getRandom();
        
        priorityVector = new ArrayList<>();
        int psize = getRandomInt(0, 5);
        for (int i=0; i<psize; i++)
            priorityVector.add(getRandomInt(1, 1_000_000));
        
        deletedVector = new ArrayList<>();
    }
    
    public SpaceStatus getSpaceStatus(){
        return spaceStatus;
    }
    
    public List<Integer> getPriorityVector(){
        return priorityVector;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(error.getCodeSTB()));
            out.write(DOB7bitUtils.encodeUInt(recordedVector.size()));
            out.write(DOB7bitUtils.encodeLong(crc));
            out.write(spaceStatus.getData());
            out.write(DOB7bitUtils.encodeUInt(priorityVector.size()));
            for (int id: priorityVector)
                out.write(DOB7bitUtils.encodeUInt(id));
            out.write(DOB7bitUtils.encodeUInt(deletedVector.size()));
            for (int id: deletedVector)
                out.write(DOB7bitUtils.encodeUInt(id));
            return out.toByteArray();
        }
    }
}
