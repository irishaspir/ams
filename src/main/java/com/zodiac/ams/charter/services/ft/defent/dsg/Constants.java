package com.zodiac.ams.charter.services.ft.defent.dsg;

public class Constants {
    
    public static final String MAC = "STB MAC Address";
    
    public static final String VCTID = "VCT ID";   //DAC
    //https://ts.developonbox.ru/!/dashboard/#/CHRAMS-330/:view/
    public static final String TSID = "TSID";
    //public static final String TSID = "SGID";
    
    public static final String HUBID = "Hub ID";   //DNCS
    public static final String SGID = "SGID";
    
    public static final String AMSIP = "AMSIP";   
    public static final String SDVSGID = "SDV SGID";    
    public static final String CMIP = "CM IP";                    
    public static final String CONNECTIONMODE = "Connection Mode"; //connectionMode" ;    
    
    public static final String MODEL = "Model";
    public static final String CMMAC="CM MAC Address";
    public static final String SN = "Serial Number";
    public static final String SW = "SW Version";
    
    public static final String HOSTRFIP = "Host RF IP";
    
    public static final byte DEFAULT_VERSION = 3; 
    
    public static final int INVALID_CONNECTION =  777;
    
//    public final static String ATTR_STB_MAC = "STB MAC Address";
//    public final static String ATTR_HUBID = "Hub ID";
//    public final static String ATTR_VCTID = "VCT ID";
//    public final static String ATTR_SGID = "SGID";
//    public final static String ATTR_AMSIP = "AMSIP";
//    public final static String ATTR_STBMODEL = "Model";
//    public final static String ATTR_TSID = "TSID";
//    public final static String ATTR_HOSTRFIP = "Host RF IP";
//    public final static String ATTR_SWVERSION = "SW Version";
//    public final static String ATTR_SDVSGID = "SDV SGID";
//    public final static String ATTR_SERIAL_NUMBER = "Serial Number";
//    public final static String ATTR_CMMACADDRESS = "CM MAC Address";
//    public final static String ATTR_CMIP = "CM IP";
//    public final static String ATTR_CONNECTION_MODE = "Connection Mode";
//    public final static String ATTR_HOST_POWER_STATUS = "Host Power Status";
//    public final static String ATTR_STB_AUTH_STATUS = "STB Auth Status"; 
//    public final static String ATTR_SWMW_VERSION = "SWMW Version"; 
//    public final static String ATTR_SW_VERSION = "SW Version"; 
//    public final static String ATTR_BOOTUP_TIME = "Bootup Time"; 
//    public final static String ATTR_BUILD_DATE = "Build Date"; 
//    public final static String ATTR_AVN_COOKIE = "AVN Cookie"; 

}




