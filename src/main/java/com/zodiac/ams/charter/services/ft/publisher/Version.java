package com.zodiac.ams.charter.services.ft.publisher;

public enum Version {
    VERSION_1("1.0"), 
    VERSION_2("2.0"), 
    VERSION_3("3.0"), 
    VERSION_3_1("3.1"), //DNCS location:  bfs://assets-obj-102/a/b/c/d/name1.zdb -> bfs://assets-obj-102/name1.zdb 
    VERSION_3_2("3.2")  //root.zdts.name:  <serverType>:<uid>:{<carousel>/}<file> -> <uid>:{<carousel>/}<file>
    ;

    private final String version;
    
    private Version(String version){
        this.version = version;
    }
    
    public String getVersion(){
        return version;
    }
    
    public static Version fromVersion(String ver){
        Version[] versions = Version.values();
        for (Version version: versions){
            if (version.getVersion().equals(ver))
                return version;
        }
        return null;
    }
}
