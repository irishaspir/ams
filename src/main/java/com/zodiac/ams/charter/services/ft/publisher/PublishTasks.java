package com.zodiac.ams.charter.services.ft.publisher;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class PublishTasks {
    
    static class TaskDescription{
        private PublishTask task;
        private String source;
        private String fileToAdd;
        
        private TaskDescription(PublishTask task,String fileToAdd,String source){
            this.task = task;
            this.fileToAdd = fileToAdd;
            this.source = source;
        }
        
        PublishTask getTask(){
            return task;
        }
        
        String getSource(){
            return source;
        }
        
        String getFileName(){
            return fileToAdd;
        }
    }
    
    private static final String TASK_FIELD = "task";
    private static final String SOURCE_FIELD = "sourceFile";
    
    private List<TaskDescription> tasks = new ArrayList<>();
    
    int size(){
        return tasks.size();
    }
    
    TaskDescription getTask(int index){
        return tasks.get(index);
    }
    
    public void addTask(PublishTask task,String fileToAdd,String source){
        tasks.add(new TaskDescription(task,fileToAdd,source));   
    }
    
    JSONArray toJSONArray(){
        JSONArray arr = new JSONArray();
        for (TaskDescription task: tasks){
            JSONObject obj=new JSONObject();
            if (null != task.task)
                obj.put(TASK_FIELD, task.task.toJSON());
            if (null != task.source)
                obj.put(SOURCE_FIELD,task.source);
            arr.put(obj);
        }
        return arr;
    }
    
    
    
}
