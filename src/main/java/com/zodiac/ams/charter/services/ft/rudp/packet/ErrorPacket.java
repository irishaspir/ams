package com.zodiac.ams.charter.services.ft.rudp.packet;

import java.io.Serializable;

public interface ErrorPacket extends Serializable {
    int getCRC16();
    ErrorCodeType getErrorCode();
    int getFileId();
}
