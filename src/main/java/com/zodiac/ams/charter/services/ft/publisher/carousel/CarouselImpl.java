package com.zodiac.ams.charter.services.ft.publisher.carousel;

import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.*;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public abstract class CarouselImpl implements ICarousel {
    
    protected static final Logger logger = LoggerFactory.getLogger(CarouselImpl.class);
    protected boolean isZipped;
    protected CarouselType type;
    protected CarouselHost host;
    
    protected CarouselImpl(CarouselType type,boolean isZipped){
        this.type = type;
        this.isZipped = isZipped;
        File [] files = new File[]{new File(TMP_LOCAL),new File(TMP_REMOTE)};
        for (File file: files){
            delete(file);
            file.mkdirs();
        }
    }
    
    @Override
    public CarouselHost getCarouselHost(){
        return host;
    }
    
    @Override
    public CarouselType getType(){
        return type;
    }
    
    @Override
    public boolean isZipped(){
        return isZipped;
    }
    
    public static String getName(String name){
        return getFileFromJar(PUBLISHER_DATA_DIR, name);
    }

    public static String getLocalTmpName(String name) {
        return TMP_LOCAL+"/"+name; 
    }
    
    public static String getRemoteTmpName(String name) {
        return TMP_REMOTE+"/"+name; 
    }
    
    public static String getLocalZippedFile(String local,String entryName){
        ZipFile zip = null;
        byte[] buffer = new byte[2_048];
        String result = null;
        try{
            zip = new ZipFile(local);
            Enumeration<? extends ZipEntry> entries = zip.entries();
            while(entries.hasMoreElements()){
                ZipEntry entry = entries.nextElement();
                if (entry.getName().equals(entryName)){
                    try (InputStream in=zip.getInputStream(entry);
                        ByteArrayOutputStream out = new ByteArrayOutputStream()){
                        int read;
                        while (-1 !=(read=in.read(buffer)))
                            out.write(buffer,0,read);
                        out.flush();
                        result = new String(out.toByteArray());
                    }
                    break;
                }
            }
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        }
        finally{
            if (null != zip){
                try{
                    zip.close();
                }
                catch(IOException ex){
                    logger.error(ex.getMessage());
                }
            }
        }
        return result;
    }
    
    public static String getLocalFile(String local){
        try{
            return new String(Files.readAllBytes(new File(local).toPath()));
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
    
    private void delete(File file){
        if (!file.exists())
            return;
        if (file.isDirectory()) {
            for (File f : file.listFiles())
                delete(f);     
        } 
        file.delete();
    }
    
    protected boolean zipFile(String source,String target,String name){
        byte [] buffer = new byte[4_096];
        int read;
        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(target));
                FileInputStream in = new FileInputStream(new File(source))){
            ZipEntry entry = new ZipEntry(name);
            out.putNextEntry(entry);
            while (-1 != (read = in.read(buffer)))
                out.write(buffer, 0, read);
            out.closeEntry();
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return false;
        }
        return true;
    }
    
    protected String getZippedCarouselFile(byte[]  bytes,String remoteName,String entryName) throws Exception{
        String localZip = getRemoteTmpName(remoteName);
        String localEntry = getRemoteTmpName(entryName);
        Files.write(new File(localZip).toPath(),bytes);
        String result = getLocalZippedFile(localZip,entryName);
        Files.write(new File(localEntry).toPath(), result.getBytes());
        return result;
    }
    
    @Override
    public String getUid(){
        return host.getUid();
    }
    
    @Override
    public String getHost(){
        return host.getHost();
    }
     
    @Override
    public String getAlias() {
        return host.getAlias();
    }

    @Override
    public String getRootName() {
        return host.getRootName();
    }
    
}
