package com.zodiac.ams.charter.services.ft.ppv;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class PPVMetaData {
    
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-ddHH:mm");
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    int sourceid;
    int sid;
    String date;
    String time;
    int length;
    String flags;
    int eid;
    int costpennies;
    String preview;
    String purchase;
    int cancel;
    int advert;
    String title;
    
    private int getInt(String token){
        int result = 0;
        if (null != token){
            try {
                result = Integer.parseInt(token.trim());
            }
            catch(NumberFormatException ex){
            }
        }
        return result;
    }
    
    private String getString(String token){
        String result = null;
        if (null != token){
            result = token;
            if (result.startsWith("\""))
                result = result.substring(1);
            if (result.endsWith("\""))
                result = result.substring(0, result.length()-1);
        }
        return (null == result) ? "" : result;
    }
    
    //  0       1   2    3     4      5    6     7          8       9        10     11    12
    //SOURCEID|SID|DATE|TIME|LENGTH|FLAGS|EID|COSTPENNIES|PREVIEW|PURCHASE|CANCEL|ADVERT|TITLE
    //1801|296|"2014-08-31"|"23:00"|179|"IR_X"|1794154|499|"0..0"|"5..145"|0|145|"UFC 177 DILLASHAW"
    //.....
    public PPVMetaData(String line){
        String [] tokens = line.split("\\|");
        sourceid = getInt(tokens[0]);
        sid = getInt(tokens[1]);
        date = getString(tokens[2]);
        time = getString(tokens[3]);
        length = getInt(tokens[4]);
        flags = getString(tokens[5]);
        eid = getInt(tokens[6]);
        costpennies = getInt(tokens[7]);
        preview = getString(tokens[8]);
        purchase = getString(tokens[9]);
        cancel = getInt(tokens[10]);
        advert = getInt(tokens[11]);
        title = getString(tokens[12]);
    }
    
    //from DB
    public PPVMetaData(Object[] tokens){
        sourceid = ((Number)tokens[0]).intValue();
        sid = ((Number)tokens[1]).intValue();
        date = (String)tokens[2];
        time = (String)tokens[3];
        length = ((Number)tokens[4]).intValue();
        flags = (String)tokens[5];
        eid = ((Number)tokens[6]).intValue();
        costpennies = ((Number)tokens[7]).intValue();
        preview = (String)tokens[8];
        purchase = (String)tokens[9];
        cancel = ((Number)tokens[10]).intValue();
        advert = ((Number)tokens[11]).intValue();
        title = (String)tokens[12];
    }
    
    public static List<PPVMetaData> getMetaData(List<Object[]> input){
        List<PPVMetaData> result = new ArrayList<>();
        for (Object[] tokens: input)
            result.add(new PPVMetaData(tokens));
        return result;
    }
    
    public static List<PPVMetaData> getMetaData(String input){
        List<PPVMetaData> result = new ArrayList<>();
        String [] lines = input.split("\\n"); 
        for (int i=1; i<lines.length; i++)  //the first line - header
            result.add(new PPVMetaData(lines[i]));
        return result;
    }
    
    
    @Override
    public String toString(){
        return sourceid+"|"+sid+"|"+date+"|"+time+"|"+length+"|"+flags+"|"+eid+"|"+costpennies+"|"+preview+"|"+purchase+"|"+cancel+"|"+advert+"|"+title;
    }
    
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof PPVMetaData))
            return false;
        PPVMetaData other = (PPVMetaData) obj;
        return  this.sourceid == other.sourceid &&
            this.sid == other.sid &&
            this.date.equalsIgnoreCase(other.date) &&
            this.time.equalsIgnoreCase(other.time) &&
            this.length == other.length &&
            this.flags.equalsIgnoreCase(other.flags) &&
            this.eid == other.eid &&
            this.costpennies == other.costpennies &&
            this.preview.equalsIgnoreCase(other.preview) &&
            this.purchase.equalsIgnoreCase(other.purchase) &&
            this.cancel == other.cancel &&
            this.advert == other.advert &&
            this.title.equalsIgnoreCase(other.title);
    }
    
    public Date getDate(){
        Date dt = null;
        try {
            dt = DATE_FORMAT.parse(date+time);
        }
        catch(ParseException ex){
        }
        return dt;
    }
    
    public boolean isValid(Date from, Date to){
        Date dt = getDate();
        if (null == dt || sourceid <= 0 || length <= 0 || eid <= 0)
            return false;
        if (null != from  && dt.before(from))
            return false;
        if (null != to && (dt.equals(to) || dt.after(to)))
            return false;
        return true;
    }
}
