package com.zodiac.ams.charter.services.dsg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SpiridonovaIM on 31.05.2017.
 */
public enum ConnectionModeProtocolOptions {

    REQUEST_ID("msgId", "", "0", "", new String[]{}),

    CONNECTION_MODE("connectionMode", "", "-1", "", new String[]{}),

    ROM_ID("romId", "", "-1", "", new String[]{}),

    BOX_MODEL("boxModel", "", "-1", "", new String[]{}),

    VENDOR("vendor", "", "-1", "", new String[]{}),

    MESSAGE_TYPE_MAP("messageTypesMap", "", "-1", "", new String[]{}),

    MIGRATION_START("migrationStart", "", "-1", "", new String[]{}),

    HUB_ID("hubId", "", "-1", "", new String[]{}),

    SERVICE_GROUP_ID("serviceGroupId", "", "-1", "", new String[]{}),

    LOAD_BALANCER_IP_ADDRESS("loadBalancerIpAddress", "", "-1", "", new String[]{}),

    SW_VERSION("swVersion", "", "-1", "", new String[]{}),

    SDV_SERVICE_GROUP_ID("sdvServiceGroupId", "", "-1", "", new String[]{}),

    CM_MAC_ADDRESS("cmMacAddress", "", "-1", "", new String[]{}),

    SERIAL_NUMBER("serialNumber", "", "-1", "", new String[]{}),

    CM_IP("cmIp", "", "-1", "", new String[]{}),

    SW_VERSION_TMP("swVersionTmp", "", "-1", "", new String[]{}),
    ;

    private ConnectionModeProtocolOption dsgOption;

    ConnectionModeProtocolOptions(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        dsgOption = new ConnectionModeProtocolOption(name, columnName, value, defaultValueInDB, range);
    }

    public ConnectionModeProtocolOption getOption() {
        return dsgOption;
    }

    public static List<ConnectionModeProtocolOption> allDsgProtocolOptions() {
        ArrayList<ConnectionModeProtocolOption> options = new ArrayList<>();
        Arrays.asList(ConnectionModeProtocolOptions.values()).forEach(value -> options.add(value.getOption()));
        return options;
    }

    public static List<ConnectionModeProtocolOption> dsgProtocolOptions() {
        ArrayList<ConnectionModeProtocolOption> options = new ArrayList<>();
        Arrays.asList(ConnectionModeProtocolOptions.values()).forEach(value -> {
            if (!value.getOption().getValue().equals("-1")) options.add(value.getOption());
        });
        return options;
    }


}
