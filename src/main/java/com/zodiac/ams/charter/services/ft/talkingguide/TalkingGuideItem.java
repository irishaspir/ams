package com.zodiac.ams.charter.services.ft.talkingguide;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TalkingGuideItem {
    
    private final static String TEXT_KEY = "Text";
    private final static String VOICE_KEY = "Voice";
    private final static String LANG_KEY = "Lang";
    private final static String DEVICE_ID_KEY = "DeviceId";
      
    Semaphore semaphore=new Semaphore(0);
    Future<?> future;
    String soundName;
    byte [] sound;
    String content;
    String header;
    JSONObject json;
    boolean error;
      
    private final Logger logger = LoggerFactory.getLogger(TalkingGuideItem.class);
        
    public TalkingGuideItem(String soundName){
        this.soundName =  soundName;
        try{
            sound = Files.readAllBytes(Paths.get(soundName));
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
    }
    
    public void setError(boolean error){
        this.error =  error;
    }
    
    public String getSoundName(){
        return soundName;
    }
    
    public byte[] getSound(){
        return sound;
    }
    
    public void setContent(String content){
        logger.info("Set content to: {}",content);
        this.content = content;
        json= new JSONObject(content);
    }
    
    public void setHeader(String header){
        logger.info("Set header to: {}",header);
        this.header = header;
    }
    
    public String getText(){
        return (null == json) ? null : json.optString(TEXT_KEY,null);
    }
    
    public String getContent(){
        return content;
    }
    
    public String getHeader(){
        return header;
    }
    
    public Future<?> getFuture(){
        return future;
    }
    
    @Override
    public String toString(){
        return header+"\n"+content;
    }
}
