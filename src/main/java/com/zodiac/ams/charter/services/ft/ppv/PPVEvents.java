package com.zodiac.ams.charter.services.ft.ppv;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class PPVEvents {
    List<PPVEvent> events;
    
    public PPVEvents(){
    }
    
    public PPVEvents(List<PPVEvent> events){
        this.events = events;
    }
    
    public void addEvent(PPVEvent event){
        if (null == events)
            events = new ArrayList<>();
        events.add(event);
    }
    
    public JSONArray toJSONArray(){
        JSONArray array = new JSONArray();
        if (null != events){
            for (PPVEvent event: events)
                array.put(event.toJSON());
        }
        return array;
    }
    
    @Override
    public String toString(){
        return toJSONArray().toString();
    }
    
}
