package com.zodiac.ams.charter.services.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomChannelType;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

public class Group0 extends GroupImpl{
    
    private static final int BAD_CHANNEL = 777;
    
    
    int lineupId;
    int timeStampBase;
    int [] timeStampDelta;
    int [] channelNumber; //453 - tmsServiceID
    ChannelType[] channelType;
    
    
    public Group0(String mac, EntError error,ChannelType channelType){
        this(mac, new EntError[]{error}, new ChannelType[] {channelType});
    }
    
    public Group0(String mac, EntError error){
        this(mac, new EntError[]{error}, null);
    }
     
    public Group0(String mac, EntError[] errors) {
        this(mac, errors, null);
    }
            
    public Group0(String mac, EntError[] errors, ChannelType[] channelType) {
        super(mac, errors);
        lineupId = getRandomInt();
        timeStampBase = getNow();
        
        timeStampDelta = new int[errors.length];
        
        channelNumber = new int[errors.length];
        this.channelType = (null == channelType) ? new ChannelType[errors.length] : channelType;
        
        for (int i=0; i<errors.length; i++){
            timeStampDelta[i] = getRandomInt(); 
            channelNumber[i] = getRandomInt();
            if (null == channelType)
                this.channelType[i] = getRandomChannelType();
        }
    }
    
    @Override
    public byte[] getMessage() throws Exception{
        byte [] result = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(DOB7bitUtils.encodeUInt(requestId)); 
            baos.write(getGroupDescription(0,errors.length)); //group,count
            //453
            //baos.write(DOB7bitUtils.encodeUInt(lineupId)); 
            baos.write(DOB7bitUtils.encodeUInt(timeStampBase)); 
            for (int i=0; i< errors.length; i++){
                baos.write(DOB7bitUtils.encodeUInt(errors[i].getCode())); 
                baos.write(DOB7bitUtils.encodeUInt(timeStampDelta[i])); 
                baos.write(DOB7bitUtils.encodeUInt(channelNumber[i]));
                //453
                //if (null == channelType[i])
                //    baos.write(DOB7bitUtils.encodeUInt(BAD_CHANNEL));
                //else
                //    baos.write(DOB7bitUtils.encodeUInt(channelType[i].ordinal()));  
            }
            baos.flush();
            result = baos.toByteArray();
        }
        return result;
    }
    
    @Override
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        //453
        //obj.put("macAddress", mac);
        JSONArray array = new JSONArray();
        for (int i=0; i< errors.length; i++){
            JSONObject o =new JSONObject();
            o.put("id", mac); //453
            o.put("errorCode", errors[i].getError());
            o.put("errorDescription", errors[i].getDescription());
            //453
            //o.put("timestamp", String.valueOf(timeStampBase + timeStampDelta[i]));
            //o.put("lineupId", String.valueOf(lineupId));
            //o.put("channelNumber", String.valueOf(channelNumber[i]));
            //if (null == channelType[i])
            //    o.put("channelType", "");
            //else
            //    o.put("channelType", channelType[i].toString());
            //453
            o.put("additionalInfo",String.format("timestamp=%d, tmsServiceID=%d", timeStampBase + timeStampDelta[i], channelNumber[i]));
            array.put(o);
        }
        //453
        //obj.put("errors", array);
        //453
        obj.put("stbError", array);
        return obj;
    }
    
}