package com.zodiac.ams.charter.services.ft.dvr;

import java.util.HashSet;
import java.util.Set;
import org.json.JSONObject;

public interface IDVRObject extends IDVRSerializable{
    
    default JSONObject toJSON(){
        return toJSON(new HashSet<>());
    }
    
    JSONObject toJSON(Set<String> excluded);
}
