package com.zodiac.ams.charter.services.settings;

import java.util.ArrayList;


public class SettingsOptionsCollection {

   private ArrayList<SettingsOption> list = new ArrayList<SettingsOption>();

    public SettingsOptionsCollection addOption(SettingsOption option) {
        list.add(new SettingsOption(option.getName(), option.getColumnName(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public SettingsOptionsCollection addOptions(SettingsOptions option) {
        list.add(new SettingsOption(option.getOption().getName(), option.getOption().getColumnName(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<SettingsOption> build() {
        return list;
    }

}
