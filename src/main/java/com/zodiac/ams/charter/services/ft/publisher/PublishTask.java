package com.zodiac.ams.charter.services.ft.publisher;

import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;

public class PublishTask {
    
    private static String ACTION_FIELD =  "action";
    private static String UIDS_FIELD = "uids";
    private static String NAME_FIELD = "name";
    private static String FILE_NAME_FIELD = "fileName";
    private static String CAROUSEL_FIELD = "carousel";
    private static String DELETE_MASK_FIELD = "deleteMask";
    private static String ZDTS_PREFIX_FIELD = "zdtsPrefix";
    
    private static String FLAGS_FIELD = "flags";
    private static String DEFAULT_FLAGS = "1";
    
    
    
    
    
    public static enum Action{
        ADD("ADD"),UPDATE("UPDATE"),DELETE("DELETE"),ADD_MODIFY("ADD_MODIFY"),ADD_MODIFY_NO_VER_INC("ADD_MODIFY_NO_VER_INC"),
            CLEAR_OLD("CLEAR_OLD"),INC_VERSION("INC_VERSION"),UNKNOWN("UNKNOWN");
        private String action;
        private Action(String action){
            this.action =action;
        }
        @Override
        public String toString(){
            return action;
        }
    }
    
    private String action;
    private String [] uids;
    private String name;
    private String fileName;
    private String carousel;
    private String flags;
    private String deleteMask;
    private String zdtsPrefix;
    
    private void init(Action action,String [] uids,String name,String fileName,String carousel,String deleteMask,String zdtsPrefix){
        this.action = action.toString();
        this.uids = uids;
        this.name = name;
        this.fileName = fileName;
        this.carousel =  carousel;
        this.flags = DEFAULT_FLAGS;
        this.deleteMask = deleteMask;
        this.zdtsPrefix = zdtsPrefix;
    }
    
    public PublishTask(Action action,String uid,String name,String fileName,String carousel){
        init(action,new String[] {uid},name,fileName,carousel,null,null);
    }
    
    public PublishTask(Action action,String [] uids,String name,String fileName,String carousel){
        init(action,uids,name,fileName,carousel,null,null);
    }
    
    public PublishTask(Action action,String uid,String name,String fileName,String carousel,String deleteMask){
        init(action,new String[] {uid},name,fileName,carousel,deleteMask,null);
    }
    
    public PublishTask(Action action,String uid,String name,String fileName,String carousel,String deleteMask,String zdtsPrefix){
        init(action,new String[] {uid},name,fileName,carousel,deleteMask,zdtsPrefix);
    }
    
    
    public PublishTask(Action action,String [] uids,String name,String fileName,String carousel,String deleteMask){
        init(action,uids,name,fileName,carousel,deleteMask,null);
    }
    
    public PublishTask(Action action,String [] uids,String name,String fileName,String carousel,String deleteMask,String zdtsPrefix){
        init(action,uids,name,fileName,carousel,deleteMask,zdtsPrefix);
    }
    
    
    public void setFlags(String flags){
        this.flags = flags;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setCarousel(String carousel){
        this.carousel = carousel;
    }
    
    JSONObject toJSON(){
         JSONObject  json=new JSONObject();
         if (null != action)
             json.put(ACTION_FIELD, action);
         if (null != name)
             json.put(NAME_FIELD, name);
         if (null != fileName)
             json.put(FILE_NAME_FIELD, fileName);
         if (null != carousel)
             json.put(CAROUSEL_FIELD,carousel);
         if (null != deleteMask)
             json.put(DELETE_MASK_FIELD, deleteMask);
         if (null != zdtsPrefix)
             json.put(ZDTS_PREFIX_FIELD, zdtsPrefix);
         if (null != flags)
             json.put(FLAGS_FIELD, flags);
         //if (null != uids)
         //   json.put(UIDS_FIELD,new JSONArray(Arrays.asList(uids)));
         return json;
     }
    
}
