package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;

public class RecordedProgramDelta implements IDVRSerializable{
    
    private final IDVRSerializable delta;
    private final long crc;
    private final SpaceStatus spaceStatus;
    private final List<Integer> priorityVector;
    
    public RecordedProgramDelta(IDVRSerializable delta){
        this.delta = delta;
        spaceStatus = SpaceStatus.getRandom();
        priorityVector = new ArrayList<>();
        int psize = getRandomInt(0, 5);
        for (int i=0; i<psize; i++)
            priorityVector.add(getRandomInt(1, 1_000_000));
        CRC32 crc = new CRC32();
        crc.update(String.valueOf(getRandomInt(1, 1_000_000)).getBytes());
        this.crc = crc.getValue();
    }
    
    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(Method.RECORDED_PROGRAM_UPDATE.getCode()));
            out.write(delta.getData());
            out.write(DOB7bitUtils.encodeLong(crc));
            out.write(spaceStatus.getData());
            out.write(DOB7bitUtils.encodeUInt(priorityVector.size()));
            for (int id: priorityVector)
                out.write(DOB7bitUtils.encodeUInt(id));
            return out.toByteArray();
        }
    }
}
