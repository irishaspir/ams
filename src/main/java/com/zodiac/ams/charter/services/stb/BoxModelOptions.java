package com.zodiac.ams.charter.services.stb;


import java.util.List;


import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.UNKNOWN_ENVIRONMENT;




public class BoxModelOptions {

    private List<String> dataFromDB;
    private int modelId;
    private String roumId;
    private int vendorId;


    public BoxModelOptions(List<String> dataFromDB) {
        this.dataFromDB = dataFromDB;
        setValueFromBDData();
    }

    private void setValueFromBDData() {
            modelId = !isDataNull(0) ? convertToInt(0) : UNKNOWN_ENVIRONMENT;
            roumId = getData(1);
            vendorId = !isDataNull(0) ? convertToInt(2) : UNKNOWN_ENVIRONMENT;
    }

    public int getModelId() {
        return modelId;
    }

    public String getRoumId() {
        return roumId;
    }

    public int getVendorId() {
        return vendorId;
    }

    private int convertToInt(int i) {
        return Integer.parseInt(dataFromDB.get(i));
    }

    private String getData(int i) {
        return dataFromDB.get(i);
    }

    private boolean isDataNull(int i) {
        return getData(i).equals("null");
    }

}
