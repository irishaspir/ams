package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.dob.ams.transport.zodiac.rudp1.util.NetworkOrderBitUtils;
import com.dob.ams.util.DOB7bitUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AckStartPacket extends AbstractAckResponsePacket {
    private int maxSegmentQtyPerSession=1;
    private int segmentSize;
    private int dataSize;

    public AckStartPacket() {
    }

    public AckStartPacket(StartPacket sp) {
        this(1, sp.getSegmentSize(), sp.getFileId(), sp.getDataSize());
    }

    public AckStartPacket(int maxSegmentQtyPerSession, int segmentSize, int fileId, int dataSize) {
        this.maxSegmentQtyPerSession = maxSegmentQtyPerSession;
        this.segmentSize = segmentSize;
        this.dataSize = dataSize;
        setFileId(fileId);
    }

    public int getSegmentSize() {
        return segmentSize;
    }

    public void setSegmentSize(int segmentSize) {
        this.segmentSize = segmentSize;
    }

    public int getDataSize() {
        return dataSize;
    }

    public void setDataSize(int dataSize) {
        this.dataSize = dataSize;
    }

    @Override
    public String toString() {
        return "AckStartPacket{" +
                "maxSegmentQtyPerSession=" + maxSegmentQtyPerSession +
                ", segmentSize=" + segmentSize +
                ", fileId=" + getFileId() +
                ", dataSize=" + dataSize +
                '}';
    }

    @Override
    public byte[] toBytes() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);

            baos.write(PacketType.ACK.packetType);
            baos.write(PacketType.START.packetType);
            baos.write(NetworkOrderBitUtils.i2b2(segmentSize));

            baos.write(maxSegmentQtyPerSession & 0xFF);
            baos.write(DOB7bitUtils.encodeUInt(getFileId()));
            baos.write(DOB7bitUtils.encodeUInt(dataSize));

            return baos.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Should not happen",e);
        }
    }
}
