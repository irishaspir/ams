package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.WeekDays;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import org.json.JSONObject;
import static com.zodiac.ams.charter.services.ft.dvr.JSONUtils.getInt;
import static com.zodiac.ams.charter.services.ft.dvr.JSONUtils.setField;

public class RecordingParam extends DVRParam implements IDVRObject{
    
    public RecordingParam(){
    }
    
    public RecordingParam(DVRParam param){
        super(param);
    }
    
    public RecordingParam(RecordingParam param){
        super(param);
    }
    
    public static RecordingParam getTruncated(RecordingParam source, RecordingParam template){
        RecordingParam target =  new RecordingParam(source);
        truncate(target, template);
        return target;
    }
    
    public static RecordingParam getRandom(Type type){
        RecordingParam rec = new RecordingParam();
        random(rec, type);
        return rec;
    }
    
    public static RecordingParam getRandom(){
        return RecordingParam.getRandom(getRandomObject(Type.values()));
    }
    
    
    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(getInt(serviceId)));
            out.write(DOB7bitUtils.encodeBigInt(getInt(startTime)));
            out.write(DOB7bitUtils.encodeUInt(getInt(duration))); 
            out.write(DvrMessageUtils.getUnitedFlagsFromObj(saveDays, recordingFormat, episodesDefinition, recordDuplicates));
            out.write(DOB7bitUtils.encodeUInt(type.getCode()));
            out.write(DOB7bitUtils.encodeUInt(getInt(startOffset)));
            out.write(DOB7bitUtils.encodeUInt(getInt(endOffset)));
            out.write(DOB7bitUtils.encodeUInt(DvrMessageUtils.buildUnitingParam(unitingParameter)));
            if (repeat instanceof WeekDays[])
                out.write(DOB7bitUtils.encodeUInt(WeekDays.getWeekDaysMask((WeekDays[])repeat)));
            else
                out.write(DOB7bitUtils.encodeUInt(0));
            out.write(DOB7bitUtils.encodeUInt(getInt(channelNumber)));
            return out.toByteArray();
        }
    }
    
    
    @Override
    public JSONObject toJSON(Set<String> excluded){
        JSONObject obj = new JSONObject();
        setField(obj, "recordingId", recordingId);
        setField(obj, "serviceId", serviceId);
        setField(obj, "channelNumber", channelNumber);
        setField(obj, "startTime", startTime);
        setField(obj, "duration", duration);
        if (null != saveDays)
            obj.put("saveDays", saveDays.toString());
        if (null != type)
            obj.put("type", type.toString());
        setField(obj, "startOffset", startOffset);
        setField(obj, "endOffset", endOffset);
        if (null != recordingFormat)
            obj.put("recordingFormat", recordingFormat.toString());
        if (null != episodesDefinition)
            obj.put("episodesDefinition", episodesDefinition.toString());
        if (null != unitingParameter)
            obj.put("unitingParameter", unitingParameter);
        if (null != recordDuplicates)
            obj.put("recordDuplicates", recordDuplicates.toString());
        if (repeat instanceof WeekDays[])
            obj.put("repeat", WeekDays.toString((WeekDays[])repeat));
        else
        if (null != repeat)
            obj.put("repeat", repeat.toString());
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
