package com.zodiac.ams.charter.services.ft.rudp;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessagePackage;
import com.dob.ams.transport.zodiac.util.ZodiacTransportUtils;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckStartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSenderSession;
import com.zodiac.ams.charter.services.ft.rudp.session.RUDP1SessionRegistryService;
import com.zodiac.ams.charter.services.ft.rudp.packet.ResponsePacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPReceiverSession;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSession;
import com.zodiac.ams.charter.services.ft.rudp.session.RUDP1ReceiverSession;
import com.zodiac.ams.charter.services.ft.rudp.session.RUDP1SenderSession;
import com.zodiac.ams.charter.services.ft.rudp.session.SessionKey;
import java.net.InetSocketAddress;
import java.net.PortUnreachableException;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelDownstreamHandler;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.jboss.netty.util.HashedWheelTimer;
import org.jboss.netty.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RUDPTestingSendManager {
    
    private static final int MAX_SESSION_ID = Short.MAX_VALUE;
    private static final Logger logger = LoggerFactory.getLogger(RUDPTestingSendManager.class);
    
    
    final InetSocketAddress localAddress;
    final InetSocketAddress remoteAddress;
    final ConnectionlessBootstrap bootstrap;
    final Channel datagramChannel;
    static RUDPTestingTransportConfig config = new RUDPTestingTransportConfig();
    final Timer timer = new HashedWheelTimer(Executors.defaultThreadFactory(), 5, TimeUnit.MILLISECONDS, 0x2000);
    private static final ConcurrentMap<Integer,RUDPTestingSession> sessions = new ConcurrentHashMap<>();
    private String deviceId;
    private final RUDPTestingSenderHandler senderHandler;
    private final RUDPTestingReceiverHandler receiverHandler;
    
   
    public RUDPTestingSendManager(String remoteHost,int remotePort,int localPort, String deviceId) throws Exception{
    
        localAddress = new InetSocketAddress(localPort);
        remoteAddress = new InetSocketAddress(remoteHost,remotePort);
        this.deviceId = deviceId;
        senderHandler = new RUDPTestingSenderHandler(this);
        receiverHandler = new RUDPTestingReceiverHandler(this);
        
        
        NioDatagramChannelFactory datagramChannelFactory = new NioDatagramChannelFactory(Executors.newCachedThreadPool());
        bootstrap = new ConnectionlessBootstrap(datagramChannelFactory);
       
        bootstrap.setPipeline(
            Channels.pipeline(
                new RudpExceptionsCatcher(),            // dummy upstream handler
                new RudpResponsePacketEncoder(),        // ResponsePacket -> ChannelBuffer
                senderHandler,
                receiverHandler
            )
        );
        
        datagramChannel = bootstrap.bind(localAddress);
        logger.info("RUDP TESTING CLIENT: RUDPTestingSendManager init OK, bindAddress: {}", localAddress);
    }
    
    
    public void setAckHandler(IAckHandler handler){
        receiverHandler.setAckHandler(handler);
    }
    
    public void setStartPacketProvider(int sessionId,IStartPacketProvider provider){
        senderHandler.setStartPacketProvider(sessionId, provider);
    }
    
    public void setDataPacketProvider(int sessionId,IDataPacketProvider provider){
        senderHandler.setDataPacketProvider(sessionId, provider);
    }
    
    public void removeStartPacketProvider(int sessionId){
        senderHandler.removeStartPacketProvider(sessionId);
    }
    
    public void removeDataPacketProvider(int sessionId){
        senderHandler.removeDataPacketProvider(sessionId);
    }
    
    
    public InetSocketAddress getRemoteAddress(){
        return remoteAddress;
    }
    
    public static int getSegmentSize(){
        return config.getRudpSegmentSize();
    }
    
    public static IRUDPSession getSession(int sessionId){
        return sessions.get(sessionId).getRUDPSession();
    }
    
    public static IRUDPSenderSession getSenderSession(int sessionId){
        return sessions.get(sessionId).getRUDPSenderSession();
    }
    
    public static IRUDPReceiverSession getReceiverSession(int sessionId){
        return sessions.get(sessionId).getRUDPReceiverSession();
    }
    
    public static RUDPTestingSession getTestingSession(int sessionId){
        return sessions.get(sessionId);
    }
    
    public static void setTestingSession(int sessionId,RUDPTestingSession ts){
        sessions.put(sessionId, ts);
    }
    
    public static RUDPTestingPackage getRUDPTestingPackage(int sessionId){
        return sessions.get(sessionId).pckg;
    }
    
    public static SessionKey getSessionKey(int sessionId){
        return sessions.get(sessionId).getRUDPSenderSession().getSessionKey();
    }
    
    public static ErrorPacket getErrorPacket(int sessionId){
        return sessions.get(sessionId).getRUDPSenderSession().getErrorPacket();
    }
        
    public static AckStartPacket getAckStartPacket(int sessionId){
        return sessions.get(sessionId).getAckStartPacket();
    }
     
    public int createReceiverSession(StartPacket start){
        int sessionId = start.getFileId();
        synchronized(sessions){
            IRUDPReceiverSession session  = new RUDP1ReceiverSession(remoteAddress, sessionId, start.getDataSize(), start.getSegmentSize()); 
            RUDPTestingSession ts = new RUDPTestingSession(null, session);
            sessions.put(sessionId, ts);
        }
        return sessionId;
    }
    
    public int createSenderSession(long mac,ZodiacMessage source) throws Exception{
        int sessionId;
        synchronized(sessions){
            sessionId = generateUniqueSessionId();
            RUDPTestingPackage pckg = RUDPTestingPackage.createPacket(mac,source); 
            IRUDPSenderSession session = new RUDP1SenderSession(remoteAddress, sessionId, pckg.toBytes(), getSegmentSize()); 
            RUDPTestingSession ts = new RUDPTestingSession(pckg,session);
            sessions.put(sessionId, ts);
        }
        return sessionId;
    }
    
    public int createSenderSession(int size) throws Exception{
        int sessionId;
        synchronized(sessions){
            sessionId = generateUniqueSessionId();
            RUDPTestingPackage pckg = RUDPTestingPackage.createPacket(deviceId,size,getSegmentSize(),sessionId); 
            IRUDPSenderSession session = new RUDP1SenderSession(remoteAddress, sessionId, pckg.toBytes(), getSegmentSize()); 
            RUDPTestingSession ts = new RUDPTestingSession(pckg,session);
            sessions.put(sessionId, ts);
        }
        return sessionId;
    }
    
    
    public int getSize(int sessionId) throws Exception{
        return sessions.get(sessionId).pckg.toBytes().length;
    }
    
    public void setSize(int sessionId,int size) throws Exception{
        RUDPTestingSession ts = sessions.get(sessionId);
        ts.pckg = RUDPTestingPackage.createPacket(deviceId,size,getSegmentSize(),sessionId); 
        ts.session = new RUDP1SenderSession(remoteAddress, sessionId, ts.pckg.toBytes(), getSegmentSize()); 
    }
    
    public static int generateUniqueSessionId() {
        int sessionId;
        synchronized(sessions){
            do {
                sessionId = Math.abs(new Random().nextInt(MAX_SESSION_ID));
            } 
            while(sessions.containsKey(sessionId));
        }
        return sessionId;
    }
    
    
    public void close() {
        if (null != datagramChannel) 
            datagramChannel.close();
            
        if (null != bootstrap) 
            bootstrap.releaseExternalResources();
        
        logger.info("RUDP TESTING CLIENT: RUDPTestingSendManager closed");
    }
     
    public ChannelFuture sendResponse(final ResponsePacket responsePacket) {
        logger.info("RUDP TESTING CLIENT: going to send RESPONSE packet: {} to address {}", responsePacket, remoteAddress);
        ChannelFuture writeFuture = datagramChannel.write(responsePacket, remoteAddress);
        writeFuture.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                logger.info("RUDP TESTING CLIENT: sent RESPONSE packet {} OK to address {}", responsePacket, remoteAddress);
            }
        });
        return writeFuture;
    }
    
    public ChannelFuture sendZodiacPackage(final int sessionId){
        return sendStartAndDataImpl(sessionId,false);
    }
            
    public ChannelFuture sendStart(final int sessionId){
        return sendStartAndDataImpl(sessionId,true);
    }
    
    private ChannelFuture sendStartAndDataImpl(final int sessionId,boolean startOnly){
        final String action = startOnly ? "START packet" : "ZODIAC MESSAGE packet";
        logger.info("RUDP TESTING CLIENT session = {}: going to send {} to address {}", sessionId, action, remoteAddress);
        ChannelFuture writeFuture = datagramChannel.write(new RUDPTestingStartMessage(sessionId,startOnly), remoteAddress);
        writeFuture.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                logger.info("RUDP TESTING CLIENT session = {}: {} is sent to address {}", sessionId, action, remoteAddress);
            }
        });
        return writeFuture;
    }
    
    public ChannelFuture sendData(final int sessionId){
        return sendDataImpl(sessionId,RUDPTestingDataMessage.ALL_SEGMENTS);
    }
    
    public ChannelFuture sendSegment(final int sessionId, final int segment){
        return sendDataImpl(sessionId,segment);
    }
    
    private ChannelFuture sendDataImpl(final int sessionId,final int segment){
        logger.info("RUDP TESTING CLIENT session = {}: going to send DATA segment={} to address {}", sessionId, segment,remoteAddress);
        ChannelFuture writeFuture = datagramChannel.write(new RUDPTestingDataMessage(sessionId,segment), remoteAddress);
        writeFuture.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                logger.info("RUDP TESTING CLIENT session = {}: DATA segment={} is sent to address {}", sessionId, segment, remoteAddress);
            }
        });
        return writeFuture;
    }
    
    /**
     * Convert ResponsePacket -> ChannelBuffer.
     * Other classes are sent downstream unchanged
     */
    private static class RudpResponsePacketEncoder extends SimpleChannelDownstreamHandler {
        @Override
        public void writeRequested(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
            if (e.getMessage() instanceof ResponsePacket) {
                ResponsePacket responsePacket = (ResponsePacket) e.getMessage();
                final byte[] packetBytes = responsePacket.toBytes();
                logger.info("RUDP TESTING CLIENT: RudpResponsePacketEncoder: converts ResponsePacket {} to bytes {}, send to address {}", responsePacket, Arrays.toString(packetBytes), e.getRemoteAddress());
                Channels.write(ctx, e.getFuture(), ChannelBuffers.wrappedBuffer(packetBytes), e.getRemoteAddress());
            } 
            else {
                logger.info("RUDP TESTING CLIENT: RudpResponsePacketEncoder send message of class {} downstream w/o encoding", e.getMessage().getClass().getCanonicalName());
                ctx.sendDownstream(e);
            }
        }
    }
    
    /**
     * At least one upstream handler should be in the pipeline
     */
    private static class RudpExceptionsCatcher extends SimpleChannelUpstreamHandler {
        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
            Throwable cause = e.getCause();
            if (cause instanceof PortUnreachableException) 
                logger.error("RUDP TESTING CLIENT: error in ResponseSenderRunnable, ack port unreachable");
            else
                logger.error("RUDP TESTING CLIENT: error in ResponseSenderRunnable", cause);
        }
    }
    
    /**
     * Convert ZodiacMessagePackage -> ChannelBuffer
     */
    /*
    private static class ZodiacEncoder4Rudp extends SimpleChannelDownstreamHandler {
        @Override
        public void writeRequested(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
            if (e.getMessage() instanceof ZodiacMessagePackage) {
                ZodiacMessagePackage zmp = (ZodiacMessagePackage) e.getMessage();
                byte[] encodedZmp = ZodiacTransportUtils.encodePackage(zmp);
                logger.debug("RUDP TESTING CLIENT: ZodiacEncoder4Rudp: converts ZodiacMessagePackage {} to bytes {} (length: {}), send to address {}", zmp, Arrays.toString(encodedZmp), encodedZmp.length, e.getRemoteAddress());
                Channels.write(ctx, e.getFuture(), ChannelBuffers.wrappedBuffer(encodedZmp), e.getRemoteAddress());
                return;
            }
            logger.trace("RUDP: ZodiacEncoder4Rudp send message of class {} downstream w/o encoding", e.getMessage().getClass().getCanonicalName());
            ctx.sendDownstream(e);
        }
    }*/
}
