package com.zodiac.ams.charter.services.dsg;

/**
 * Created by SpiridonovaIM on 26.05.2017.
 */
public class DsgJsonOption {

    private String name;
    private String columnName;
    private String value;
    private String defaultValue;
    private String[] range;


    public DsgJsonOption(String name, String columnName, String values, String defaultValue, String[] range) {
        this.name = name;
        this.columnName = columnName;
        this.value = values;
        this.defaultValue = defaultValue;
        this.range = range;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String[] getRange() {
        return range;
    }

    public static DsgJsonOptionCollection create() {
        return new DsgJsonOptionCollection();
    }
    
}
