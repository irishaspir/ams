package com.zodiac.ams.charter.services.stb;


import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.UNKNOWN_ENVIRONMENT;
import static com.zodiac.ams.charter.store.TestKeyWordStore.UNKNOWN;

public class Vendors {

    private List<VendorOptions> vendorsList = new ArrayList<>();
    private List<List<String>> dataFromDB;

    public Vendors(List<List<String>> dataFromDB) {
        this.dataFromDB = dataFromDB;
        setVendorsOptions();
    }

    private void setVendorsOptions() {
        for (int i = 0; i < dataFromDB.size(); i++)
            vendorsList.add(new VendorOptions(dataFromDB.get(i)));
    }

    public int getVendorId(String name) {
        for (VendorOptions vendor : vendorsList) {
            if (name.equals(vendor.getVendorName()))
                return vendor.getVendorId();
        }
        return UNKNOWN_ENVIRONMENT;
    }

    public String getVendorName(int id) {
        for (VendorOptions vendor : vendorsList) {
            if (id == vendor.getVendorId())
                return vendor.getVendorName();
        }
        return UNKNOWN;
    }


}
