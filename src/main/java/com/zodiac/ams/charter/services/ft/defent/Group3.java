package com.zodiac.ams.charter.services.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomApplicationId;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

public class Group3 extends GroupImpl {
    private final static int BAD_APPLICATION_ID = 7777;
    
    int timeStampBase;
    int [] timeStampDelta;
    ApplicationId [] applicationID;	
    
    public Group3(String mac, EntError error, ApplicationId applicationID) {
        this(mac, new EntError[]{error}, new ApplicationId[] {applicationID});
    }
    
    public Group3(String mac, EntError error) {
        this(mac, new EntError[] {error});
    }
    
    public Group3(String mac, EntError[] errors) {
        this(mac, errors, null);
    }
            
    public Group3(String mac, EntError[] errors, ApplicationId[] applicationID) {
        super(mac, errors);
        timeStampBase = getNow();
        
        timeStampDelta = new int[errors.length]; 
        this.applicationID = (null == applicationID) ? new ApplicationId[errors.length] : applicationID;
        
        for (int i=0; i<errors.length; i++){
            timeStampDelta[i] = getRandomInt(); 
            if (null == applicationID)
                this.applicationID[i] = getRandomApplicationId();
        }
    }
    
    @Override
    public byte[] getMessage() throws Exception{
        byte [] result = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(DOB7bitUtils.encodeUInt(requestId)); 
            baos.write(getGroupDescription(3,errors.length)); //group,count
            //baos.write(DOB7bitUtils.encodeUInt(timeStampBase)); 
            baos.write((timeStampBase >> 24) & 0xFF);
            baos.write((timeStampBase >> 16) & 0xFF);
            baos.write((timeStampBase >> 8) & 0xFF);
            baos.write(timeStampBase & 0xFF);
            for (int i=0; i<errors.length; i++){
                baos.write(DOB7bitUtils.encodeUInt(errors[i].getCode())); 
                baos.write(DOB7bitUtils.encodeUInt(timeStampDelta[i])); 
                if (null == applicationID[i])
                    baos.write(DOB7bitUtils.encodeUInt(BAD_APPLICATION_ID));
                else
                    baos.write(DOB7bitUtils.encodeUInt(applicationID[i].getCode()));
            }
            baos.flush();
            result = baos.toByteArray();
        }
        return result;
    }
    
    @Override
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        //453
        //obj.put("macAddress", mac);
        JSONArray array = new JSONArray();
        for (int i=0; i<errors.length; i++){
            JSONObject o =new JSONObject();
            //453
            o.put("id", mac);
            o.put("errorCode", errors[i].getError());
            o.put("errorDescription", errors[i].getDescription());
            //453
            //o.put("timestamp", String.valueOf(timeStampBase + timeStampDelta[i]));
            //if (null == applicationID[i])
            //    o.put("applicationID", "");
            //else
            //    o.put("applicationID", applicationID[i].toApplicationIdString());
            //453
            o.put("additionalInfo",String.format("timestamp=%d, applicationID=%s", 
                timeStampBase + timeStampDelta[i], null ==  applicationID[i] ? "" : applicationID[i].toApplicationIdString()));
            array.put(o);
        }
        //453
        //obj.put("errors", array);
        //453
        obj.put("stbError", array);
        return obj;
    }
    
}
