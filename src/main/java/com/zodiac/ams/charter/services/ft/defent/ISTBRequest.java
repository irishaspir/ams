package com.zodiac.ams.charter.services.ft.defent;

public interface ISTBRequest {
    byte [] getData();
}
