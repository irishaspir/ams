package com.zodiac.ams.charter.services.stb;

import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.UNKNOWN_ENVIRONMENT;

public class VendorOptions {

    private int vendorId;
    private String vendorName;
    private List<String> dataFromDB;


    public VendorOptions(List<String> dataFromDB) {
        this.dataFromDB = dataFromDB;
        setValueFromBDData();
    }

    private void setValueFromBDData() {
        vendorId = !isDataNull(0) ? convertToInt(0) : UNKNOWN_ENVIRONMENT;
        vendorName = getData(1);
    }

    public int getVendorId() {
        return vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }


    private int convertToInt(int i) {
        return Integer.parseInt(dataFromDB.get(i));
    }

    private String getData(int i) {
        return dataFromDB.get(i);
    }

    private boolean isDataNull(int i) {
        return getData(i).equals("null");
    }


}
