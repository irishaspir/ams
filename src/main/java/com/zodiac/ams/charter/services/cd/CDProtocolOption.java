package com.zodiac.ams.charter.services.cd;

/**
 * Created by SpiridonovaIM on 31.05.2017.
 */
public class CDProtocolOption {
    private String name;
    private String requestId;
    private String value;
    private String defaultValue;
    private String[] range;


    public CDProtocolOption(String name, String requestId, String values, String defaultValue, String[] range) {
        this.name = name;
        this.requestId = requestId;
        this.value = values;
        this.defaultValue = defaultValue;
        this.range = range;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String[] getRange() {
        return range;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static CDOptionsCollection create() {
        return new CDOptionsCollection();
    }
}
