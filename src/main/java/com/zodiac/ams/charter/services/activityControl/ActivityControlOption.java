package com.zodiac.ams.charter.services.activityControl;

public class ActivityControlOption {

    private String nameJSON;
    private String valueJSON;

    public ActivityControlOption(String nameJSON, String valueJSON) {
        this.nameJSON = nameJSON;
        this.valueJSON = valueJSON;
    }

    public String getValueJSON() {
        return valueJSON;
    }

    public String getNameJSON() {
        return nameJSON;
    }

}
