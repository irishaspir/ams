package com.zodiac.ams.charter.services.ft.rudp.packet;

import java.io.Serializable;
import java.net.InetSocketAddress;

public interface ResponsePacket extends Serializable {
    InetSocketAddress getRemoteAddress();
    void setRemoteAddress(InetSocketAddress remoteAddress);

    byte[] toBytes();
}
