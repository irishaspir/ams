package com.zodiac.ams.charter.services.ft.rudp;

/**
 * Config for RUDP
 */
public class RUDPTestingTransportConfig {

    public final static int RUDP_AUTO_PURGE_TIMEOUT_MINS=15;
    public final static int RUDP_SEND_STARTPACKET_MAX_RETRIES=10;
    public final static int RUDP_SEND_DATAPACKET_MAX_RETRIES=10;
    public final static int RUDP_SEGMENT_SIZE=400;
    public final static int RUDP_STARTPACKET_TIMEOUT_MILLIS=5000;
    public final static int RUDP_DATAPACKET_TIMEOUT_MILLIS=5;
    public final static int RUDP_DATA_PACKET_PORTION_TIMEOUT_MILLIS=2*1000;

    /**
     * Timeout (in minutes) when sessions from session registry are automatically removed if no actions registered.
     * Action can be any input/output packet.
     */
    private final int rudpAutoPurgeTimeoutMins;

    /**
     * Max retries to send START packet. If no answer received java.io.IOException is thrown
     */
    private final int rudpSendStartPacketMaxRetries;

    /**
     * Max retries to send DATA packets. If no ACK received that all DATA packets are passed well, java.io.IOException is thrown
     */
    private final int rudpSendDataPacketMaxRetries;

    /**
     * RUDP Segment size
     */
    private final int rudpSegmentSize;

    /**
     * Timeout between sequential START packets
     */
    private final int rudpStartPacketTimeoutMillis;

    /**
     * Timeout between sequential DATA packets inside one PORTION
     */
    private final int rudpDataPacketTimeoutMillis;

    /**
     * Timeout between sequentional PORTIONs of DATA packets to be sent
     */
    private final int rudpDataPacketPortionTimeoutMillis;

    /**
     * Count segments to send in one moment time.
     */
    private int rudpSegmentBalkCount = Integer.MAX_VALUE;

    public RUDPTestingTransportConfig() {
        this(
                RUDP_AUTO_PURGE_TIMEOUT_MINS,
                RUDP_SEND_STARTPACKET_MAX_RETRIES,
                RUDP_SEND_DATAPACKET_MAX_RETRIES,
                RUDP_SEGMENT_SIZE,
                RUDP_STARTPACKET_TIMEOUT_MILLIS,
                RUDP_DATAPACKET_TIMEOUT_MILLIS,
                RUDP_DATA_PACKET_PORTION_TIMEOUT_MILLIS
        );
    }

    public RUDPTestingTransportConfig(int rudpAutoPurgeTimeoutMins, int rudpSendStartPacketMaxRetries, int rudpSendDataPacketMaxRetries, int rudpSegmentSize, int rudpStartPacketTimeoutMillis, int rudpDataPacketTimeoutMillis, int rudpDataPacketPortionTimeoutMillis) {
        this.rudpAutoPurgeTimeoutMins = rudpAutoPurgeTimeoutMins;
        this.rudpSendStartPacketMaxRetries = rudpSendStartPacketMaxRetries;
        this.rudpSendDataPacketMaxRetries = rudpSendDataPacketMaxRetries;
        this.rudpSegmentSize = rudpSegmentSize;
        this.rudpStartPacketTimeoutMillis = rudpStartPacketTimeoutMillis;
        this.rudpDataPacketTimeoutMillis = rudpDataPacketTimeoutMillis;
        this.rudpDataPacketPortionTimeoutMillis = rudpDataPacketPortionTimeoutMillis;
    }

    public int getRudpAutoPurgeTimeoutMins() {
        return rudpAutoPurgeTimeoutMins;
    }

    public int getRudpSendStartPacketMaxRetries() {
        return rudpSendStartPacketMaxRetries;
    }

    public int getRudpSendDataPacketMaxRetries() {
        return rudpSendDataPacketMaxRetries;
    }

    public int getRudpSegmentSize() {
        return rudpSegmentSize;
    }

    public int getRudpStartPacketTimeoutMillis() {
        return rudpStartPacketTimeoutMillis;
    }

    public int getRudpDataPacketTimeoutMillis() {
        return rudpDataPacketTimeoutMillis;
    }

    public int getRudpDataPacketPortionTimeoutMillis() {
        return rudpDataPacketPortionTimeoutMillis;
    }

    public int getRudpSegmentBalkCount() {
        return rudpSegmentBalkCount;
    }

    public void setRudpSegmentBalkCount(int rudpSegmentBalkCount) {
        this.rudpSegmentBalkCount = rudpSegmentBalkCount;
    }

    @Override
    public String toString() {
        return "RudpTransportConfig{" +
                "rudpAutoPurgeTimeoutMins=" + rudpAutoPurgeTimeoutMins +
                ", rudpSendStartPacketMaxRetries=" + rudpSendStartPacketMaxRetries +
                ", rudpSendDataPacketMaxRetries=" + rudpSendDataPacketMaxRetries +
                ", rudpSegmentSize=" + rudpSegmentSize +
                ", rudpStartPacketTimeoutMillis=" + rudpStartPacketTimeoutMillis +
                ", rudpDataPacketTimeoutMillis=" + rudpDataPacketTimeoutMillis +
                ", rudpDataPacketPortionTimeoutMillis=" + rudpDataPacketPortionTimeoutMillis +
                ", rudpSegmentBalkCount=" + rudpSegmentBalkCount +
                '}';
    }
}
