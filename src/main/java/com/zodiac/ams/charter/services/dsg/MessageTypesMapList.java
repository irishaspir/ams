package com.zodiac.ams.charter.services.dsg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SpiridonovaIM on 06.06.2017.
 */
public enum MessageTypesMapList {

    PPV("PPV", "", "0", "", new String[]{"0"}),

    SETTINGS("Settings", "", "0", "", new String[]{"0"}),

    CALLER_ID("Caller ID", "", "0", "", new String[]{"0"}),

    COMPANION_DEVICES("Companion devices", "", "0", "", new String[]{"0"}),

    DVR("DVR", "", "0", "", new String[]{"0", "1", "2", "3"}),

    DVR_MIGRATION("DVR Migration", "", "0", "", new String[]{"0"}),

    MOTOROLA_PACKAGES("Motorola Packages", "", "0", "", new String[]{"0"}),

    EPG_MOTOROLA(" EPG for Motorola 2k", "", "0", "", new String[]{"0"}),

    INTERACTIVE_CHANEL_AUTHORIZATION("Interactive Channel Authorization", "", "0", "", new String[]{"0"});

    private MessageTypesMapOption messageTypesMapList;

    MessageTypesMapList(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        messageTypesMapList = new MessageTypesMapOption(name, columnName, value, defaultValueInDB, range);
    }

    public MessageTypesMapOption getOption() {
        return messageTypesMapList;
    }

    public static List<MessageTypesMapOption> allMessageTypesMapOptions() {
        ArrayList<MessageTypesMapOption> options = new ArrayList<>();
        Arrays.asList(MessageTypesMapList.values()).forEach(value -> options.add(value.getOption()));
        return options;
    }

    public static List<String[]> allMessageTypesMapOptionsWithRange() {
        ArrayList<String[]> options = new ArrayList<>();
        allMessageTypesMapOptions().forEach(value -> {
             for (int i=0;i<value.getRange().length;i++){
                 options.add(new String[]{value.getName(),value.getRange()[i]});
             }
        });
        return options;
    }


}
