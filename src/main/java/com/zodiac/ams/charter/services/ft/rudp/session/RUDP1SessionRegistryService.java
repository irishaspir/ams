package com.zodiac.ams.charter.services.ft.rudp.session;

import java.net.InetSocketAddress;
import java.util.Collection;

/**
 * This services stores RUDP1 sessions.
 * It is thread-safe
 *
 * @param <T>
 */
public interface RUDP1SessionRegistryService<T> extends AutoCloseable {
    IRUDPSession getSession(T key);

    void putSession(T key, IRUDPSession session);

    boolean removeSession(T key);

    boolean containsSession(T key);

    Collection<T> keys();

    void close() throws Exception;

    IRUDPSenderSession createSenderSession(InetSocketAddress remoteAddress, int sessionId, byte[] dataBytes, int segmentSize);

    IRUDPReceiverSession createReceiverSession(InetSocketAddress remoteAddress, int sessionId, int dataSize, int segmentSize);
}
