package com.zodiac.ams.charter.services.callerId;


public interface CallerIdOptions {

    String SPECIAL_SYMBOLS = "!@-=!*()_!-!";
    String NULL = "null";

    String INVALID_ID_ONLY_LETTERS = "QQWWRRTTYYUU";
    String INVALID_ID_SPECIAL_SYMBOLS = SPECIAL_SYMBOLS;

    String VALID_CALLER_NAME = "TestName007";
    String CALLER_NAME_ONLY_DIGITS = "12344687";
    String CALLER_NAME_ONLY_LETTERS = "TestName";
    String CALLER_NAME_SPECIAL_SYMBOLS = SPECIAL_SYMBOLS;

    String VALID_PHONE_NUMBER = "1234567890";
    String PHONE_NUMBER_ONLY_LETTERS = "ABCDabcd";
    String PHONE_NUMBER_SEPARATED_BY_DASH = "123-123-123";
    String PHONE_NUMBER_SPECIAL_SYMBOLS = SPECIAL_SYMBOLS;

    String VALID_CALL_ID = "jh4xh83aayhi7a78dda";
    String VALID_SECOND_CALL_ID = "jh4xh83aayhi7a7879";
    String CALL_ID_ONLY_DIGITS = "1234567890";
    String CALL_ID_ONLY_LETTERS = "ABCDabcd";
    String CALL_ID_SPECIAL_SYMBOLS = SPECIAL_SYMBOLS;
}
