package com.zodiac.ams.charter.services.ft.publisher.host;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

public class CarouselHostsHTTP extends CarouselHosts{
    
    private static final String CAROUSELS = "http.carousels";
    
    public CarouselHostsHTTP(FTConfig props, CarouselType[] types, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        super(props, types, isZipped, aLocEnabled, dalinc);
    }
    
    public CarouselHostsHTTP(FTConfig props, CarouselType type, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        super(props, type, isZipped, aLocEnabled, dalinc);
    }
    
    public CarouselHostsHTTP(FTConfig props, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        super(props, CarouselType.HTTP, isZipped, aLocEnabled, dalinc);
    }
    
    
    @Override
    public String getProperty(String name){
        String value = super.getProperty(name);
        if (null != value)
            return value;
        switch(name){
            case CAROUSELS:
                return getCarousels();
        }
        return null;
    }
    
    protected String getCarousels(){
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch(version){
            case VERSION_1:
            case VERSION_2:
                return getCarousels2();
            default:
                return getCarousels3();
        }
    }
    
    protected String getCarousels2(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts)
            sb.append(sb.length()>0 ? ";" : "").append(host.getAlias()).
                append(":").
                append(host.getLocation()).
                append(":").
                append(host.getUid()).
                append(":").
                append("http").
                append(":").
                append(host.getHost()).
                append(":8080:/prefetch/").
                append(host.getUid());
        return sb.toString();
    }
    
    /**
     * Only unique aliases are allowed
     * dncs.carousels="assets-alias:assets-oob1:bfs:assets-oob1"
     * @return 
     */
    protected String getCarousels3(){
        StringBuilder sb = new StringBuilder();
        Set<String> aliases = new HashSet<>();
        for (CarouselHost host: hosts){
            String alias = host.getAlias();
            if (!aliases.add(alias))
                continue;
            sb.append(sb.length()>0 ? ";" : "").append(alias).
                append(":").
                append(host.getLocation()).
                append(":").
                append("http").
                append(":").
                //append(host.getHost()).
                //append(":8080:/prefetch/");
                //append(host.getUid());
                append("hostname").
                append(":8080:/prefetch");
        }
        return sb.toString();
    }    
}
