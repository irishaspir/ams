package com.zodiac.ams.charter.services.ft.rudp.session;

/**
 * Created by anton on 30.06.16.
 */
public interface IClusterRouting {

    boolean executeOnMember(SessionKey key, Runnable task);
}
