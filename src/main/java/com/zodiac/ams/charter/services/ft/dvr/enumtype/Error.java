package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum Error {
    Error0(0, 0),
    Error1(1, 13),
    Error2(2, 13),
    Error3(3, 3),
    Error4(4, 4),
    
    SCHEDULE_CONFLICT(4,4),
    
    Error5(5, 3),
    Error6(6, 13),
    Error7(7, 13),
    Error8(8 ,5),
    Error10(10, 7),
    Error11(11,8),
    Error12(12,9),
    Error13(13, 10),
    
    INAVLID_PARAMETER(13, 10),
    
    Error14(14 ,11),
    Error15(15 ,12),
    Error16(16, 13),
    
    UNKNOWN_ERROR(16, 13),
    
    Error17(17 ,13),
    Error18(18 ,14),
    Error19(19 ,13),
    
    NON_EXISTENT(1_000, 1_000),
    NON_EXISTENT_UNKNOWN(2_000, 13),
    
    STB_INVALID_RESPONSE(0, 10), //send 10 to HE
    STB_MISSING_STATUS(-1, 10),
    SCHEDULE_CONFLICT_ERROR(4, 10),
    RECORD_NOT_FOUND(0,0);
            
    private final int codeSTB;
    private final int codeHE;
    
    private Error(int codeSTB, int codeHE){
        this.codeSTB = codeSTB;
        this.codeHE = codeHE;
    }
    
    public int getCodeSTB(){
        return codeSTB;
    }
    
    public int getCodeHE(){
        return codeHE;
    }
    
    public static Error[] getRecordingErrors(){
        return new Error[]{
            Error0, Error1, Error3, Error4, Error8,
            Error11, Error12, Error13, Error14, Error15, Error16, Error18};
        }
}
