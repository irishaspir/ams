package com.zodiac.ams.charter.services.epg;


import static com.zodiac.ams.common.helpers.DateTimeHelper.getTodayStr;

public enum EpgOptions {

    SERVICE_ID("SERVICE_ID", "SERVICE_ID", "16834", "", new String[]{}),

    DATE("DATE", "DATE_VAL", getTodayStr(), getTodayStr(), new String[]{}),

    TIME("TIME", "TIME_VAL", "00:00", "", new String[]{}),

    DURATION("DURATION", "DURATION", "30", "", new String[]{"15", "30", "45", "60", "90", "120", "180"}),

    SHORT_TITLE("SHORT_TITLE", "SHORT_TITLE", "Sabbath School...", "", new String[]{"Teen Beats", "Party Favorites", "MC Kidz Only", "Sabbath School..."}),

    LONG_TITLE("LONG_TITLE", "LONG_TITLE", "Sabbath School Study Hour", "",
            new String[]{"3ABN Today Live", "Give Me the Bible", "Revelation of Hope", "Battles of Faith", "Sabbath School Study Hour"}),

    EPISODE_TITLE("EPISODE_TITLE", "EPISODE_TITLE", "Pilot", "", new String[]{}),

    SEASON("SEASON", "SEASON", "1", "", new String[]{"1","2","3","4","5"}),

    EPISODE("EPISODE", "EPISODE", "1", "", new String[]{"1","2","3","4","5"}),

    DESCRIPTION("DESCRIPTION", "DESCRIPTION", "Doug Batchelor of Amazing Facts hosts in-depth biblical insights for your quarterly lessons.", "",
            new String[]{"Inspirational guests and music.",
                    "Kenneth Cox brings an in-depth look at important truths of the Bible a variety of religious topics.",
                    "Inspirational guests, health tips, music and vegetarian cuisine.",
                    "Ty Gibson, James Rafferty, David Asscherick and Jeffrey Rosario in dialogue on",
                    "Doug Batchelor of Amazing Facts hosts in-depth biblical insights for your quarterly lessons."}),


    GENRES("GENRES", "GENRES", "Religious", "", new String[]{"Adventure,Drama", "Fantasy","Religious"}),

    CAST("CAST", "CAST", "Jeff Bridges,Matt Damon,Josh Brolin,Barry Pepper,Hailee Steinfeld", "", new String[]{"Jeff Bridges,Matt Damon,Josh Brolin,Barry Pepper,Hailee Steinfeld", ""}),

    NEW("NEW", "NEW", "N", "", new String[]{"N", "Y"}),

    DVS("DVS", "DVS", "N", "", new String[]{"Y", "N"}),

    CC("CC", "CC", "N", "", new String[]{"N", "Y"}),

    SAP("SAP", "SAP", "N", "", new String[]{"N", "Y"}),

    SURROUND("SURROUND", "SURROUND", "N", "", new String[]{"N", "Y"}),

    STEREO("STEREO", "STEREO", "N", "", new String[]{"N", "Y"}),

    TV_RATING("TV_RATING", "TV_RATING", "", "", new String[]{}),

    MPAA_RATING("MPAA_RATING", "MPAA_RATING", "", "", new String[]{}),

    RERUN("RERUN", "RERUN", "Y", "", new String[]{}),

    LIVE("LIVE", "LIVE", "N", "", new String[]{"N", "Y"}),

    HD("HD", "HD", "N", "", new String[]{"N", "Y"}),

    ADVISORY("ADVISORY", "ADVISORY", "", "", new String[]{"AC", "AL"}),

    ISMOVIE("ISMOVIE", "ISMOVIE", "N", "", new String[]{}),

    BLACKWHITE("BLACKWHITE", "BLACKWHITE", "N", "", new String[]{}),

    ANIMATED("ANIMATED", "ANIMATED", "N", "", new String[]{}),

    HAFLSTARS("HAFLSTARS", "HAFLSTARS", "", "", new String[]{}),

    YEAR("YEAR", "YEAR", "2014", "", new String[]{"2000","2001","2016","1992","1985"}),

    PROGRAM_ID("PROGRAM_ID", "PROGRAM_ID", "SH014406540000", "", new String[]{}),

    SERIES_IDENTIFIER("SERIES_IDENTIFIER", "SERIES_IDENTIFIER", "SH014406540000", "", new String[]{}),

    HOMETEAMID("HOMETEAMID", "HOMETEAMID", "0", "", new String[]{}),

    HOMETEAM("HOMETEAM", "HOMETEAM", "", "", new String[]{}),

    AWAYTEAMID("AWAYTEAMID", "AWAYTEAMID", "0", "", new String[]{}),

    AWAYTEAM("AWAYTEAM", "AWAYTEAM", "", "", new String[]{}),

    IS_SERIES("IS_SERIES", "IS_SERIES", "N", "", new String[]{"Y", "N"}),

    //UPDATE_TIMESTAMP("UPDATE_TIMESTAMP","UPDATE_TIMESTAMP","","",new String[]{})
    ;


    private EpgOption epgOption;

    EpgOptions(String name, String columnName, String value, String defaultValue, String[] range) {
        epgOption = new EpgOption(name, columnName, value, defaultValue, range);
    }

    public EpgOption getOption() {
        return epgOption;
    }

}
