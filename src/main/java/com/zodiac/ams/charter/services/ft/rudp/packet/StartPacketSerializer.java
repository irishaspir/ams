package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.zodiac.ams.charter.services.ft.rudp.util.NetworkOrderBitUtils;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class StartPacketSerializer {

    private static final Logger log = LoggerFactory.getLogger(StartPacketSerializer.class);

    public static AckStartPacket parseAck(byte[] bytes) throws IOException {
        PacketType pt = PacketType.fromByte(bytes[0]);

        if (pt == PacketType.ACK) {
            PacketType ackPt = PacketType.fromByte(bytes[1]);
            if (ackPt == PacketType.START) {
                int segmentSize = NetworkOrderBitUtils.b2i(bytes[2], bytes[3]);
                int maxSegmentQtyPerSession = bytes[4];

                ByteArrayInputStream bais = new ByteArrayInputStream(bytes, 5, bytes.length);
                int[] a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                int fileId = a[0];

                bais = new ByteArrayInputStream(bytes, 5 + a[1], bytes.length);
                a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                int fileSize = a[0];

                return new AckStartPacket(maxSegmentQtyPerSession, segmentSize, fileId, fileSize);
            } else {
                log.error("Not ACK-START packet");
                return null;
            }
        } else {
            log.error("Not ACK packet");
            return null;
        }
    }

    public static StartPacket parse(byte[] bytes) throws IOException {
        byte packetTypeByte = bytes[0];
        int segmentSize = NetworkOrderBitUtils.b2i(bytes[1], bytes[2]);
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes, 3, bytes.length);
        int[] a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
        int fileId = a[0];

        bais = new ByteArrayInputStream(bytes, 3 + a[1], bytes.length);
        a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
        int fileSize = a[0];

        return new StartPacket(segmentSize, fileId, fileSize);
    }

    public static byte[] toBytes(StartPacket p) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);

        baos.write(PacketType.START.packetType);

        try {
            {
                byte[] segmentSizeBytes = NetworkOrderBitUtils.i2b2(p.getSegmentSize());
                baos.write(segmentSizeBytes);
            }

            {
                byte[] fileIdBytes = DOB7bitUtils.encodeUInt(p.getFileId());
                baos.write(fileIdBytes);
            }

            {
                byte[] dataSizeBytes = DOB7bitUtils.encodeUInt(p.getDataSize());
                baos.write(dataSizeBytes);
            }

            return baos.toByteArray();
        } catch (IOException ioe) {
            throw new IllegalStateException("Should not happen", ioe);
        }
    }
}
