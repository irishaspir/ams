package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.IOException;
import java.util.Set;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;

public class PriorityResponse implements IDVRObject{
    
    private Error error = Error.Error0;
    
    public PriorityResponse(){   
    }
    
    public PriorityResponse(Error error){
        this.error = error;
    }
    
    public void setError(Error error){
        this.error = error;
    }
    
    public Error getError(){
        return error;
    }

    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("statusCode", error.getCodeHE());
        arr.put(obj);
        json.put("result", arr);
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(error.getCodeSTB()));
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
