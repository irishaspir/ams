package com.zodiac.ams.charter.services.ft.ppv;

import org.json.JSONObject;

public class PPVEvent {
    private static final String COMMAND = "Command";
    private static final String PARAMETERS = "Parameters";
    
    PPVCommand command;
    PPVParameters parameters;
    
    public PPVEvent(PPVCommand command, PPVParameters parameters){
        this.command = command;
        this.parameters = parameters;
    }
    
    public JSONObject toJSON(){
        JSONObject json = new JSONObject();
        if (null != command)
            json.put(COMMAND, command.toString());
        if (null != parameters)
            json.put(PARAMETERS, parameters.toJSONArray());
        return json;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
}
