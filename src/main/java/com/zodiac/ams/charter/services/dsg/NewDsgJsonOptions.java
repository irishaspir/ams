package com.zodiac.ams.charter.services.dsg;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum NewDsgJsonOptions {

    CONNECTION_MODE("Connection Mode", "STB_MODE", "","", new String[]{}),

    STB_MAC_ADDRESS("STB MAC Address", "MAC_STR", "000000000000", "", new String[]{}),

    HUB_ID("Hub ID", "HUBID", "null", "", new String[]{}),

    VCT_ID("VCT ID", "", "null", "", new String[]{}),

    SGID("SGID", "", "0", "", new String[]{}),

    AMSIP("AMSIP", "", "0.0.0.0", "", new String[]{}),

    MODEL("Model", "", "null", "", new String[]{}),

    TSID("TSID", "", "0", "", new String[]{}),

    HOST_RF_IP("Host RF IP", "", "127.0.0.1", "", new String[]{}),

    SW_VERSION("SW Version", "", "", "", new String[]{}),

    SDV_SGID("SDV SGID", "", "0", "", new String[]{}),

    SERIAL_NUMBER("Serial Number", "", "null", "", new String[]{}),

    CM_MAC_ADDRESS("CM MAC Address", "", "null", "", new String[]{}),

    CM_IP("CM IP", "", "0.0.0.0", "", new String[]{}),

    SW_VERSION_TMP("SW Version", "", "", "", new String[]{}),
    ;


    private DsgJsonOption dsgOption;

    NewDsgJsonOptions(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        dsgOption = new DsgJsonOption(name, columnName, value, defaultValueInDB, range);
    }

    public DsgJsonOption getOption() {
        return dsgOption;
    }


    public static List<DsgJsonOption> allDsgOptions() {
        ArrayList<DsgJsonOption> options = new ArrayList<>();
        for (NewDsgJsonOptions one : Arrays.asList(NewDsgJsonOptions.values())) {
            options.add(one.getOption());
        }
        return options;
    }
}
