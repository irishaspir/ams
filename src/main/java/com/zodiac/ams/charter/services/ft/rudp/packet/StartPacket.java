package com.zodiac.ams.charter.services.ft.rudp.packet;

import java.io.Serializable;

public class StartPacket implements Serializable {
    private int segmentSize;
    private int fileId;
    private int dataSize;

    public StartPacket() {
    }

    public StartPacket(int segmentSize, int fileId, int dataSize) {
        this.segmentSize = segmentSize;
        this.fileId = fileId;
        this.dataSize = dataSize;
    }

    public int getSegmentSize() {
        return segmentSize;
    }

    public void setSegmentSize(int segmentSize) {
        this.segmentSize = segmentSize;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public int getDataSize() {
        return dataSize;
    }

    public void setDataSize(int dataSize) {
        this.dataSize = dataSize;
    }

    @Override
    public String toString() {
        return "StartPacket{" +
                "segmentSize=" + segmentSize +
                ", fileId=" + fileId +
                ", dataSize=" + dataSize +
                '}';
    }
}
