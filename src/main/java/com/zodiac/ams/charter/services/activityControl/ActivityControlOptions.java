package com.zodiac.ams.charter.services.activityControl;

import static com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils.getSTBLastActivityLastTimeToDate;
import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.getModeChangeTimeStamp;
import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.getRegisterTimeStamp;
import static com.zodiac.ams.charter.helpers.DataUtils.converterUnixToDate;
import static com.zodiac.ams.charter.helpers.DataUtils.converterUnixToDateFormat;
import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_1;
import static com.zodiac.ams.charter.store.TestKeyWordStore.MAC_NUMBER_1;


public enum ActivityControlOptions {

    STB_MAC("deviceId", DEVICE_ID_NUMBER_1),

    REGISTRATION_TIME("registrationTime", converterUnixToDateFormat( converterUnixToDate(getRegisterTimeStamp(MAC_NUMBER_1)))),

    LAST_ACTIVITY_TIME("lastActivityTime", converterUnixToDateFormat(getSTBLastActivityLastTimeToDate(MAC_NUMBER_1))),

    NETWORK_TYPE("networkType", "ALOHA"),

    LAST_NETWORK_CHANGE_TIME("lastNetworkChangeTime",converterUnixToDateFormat( converterUnixToDate(getModeChangeTimeStamp(MAC_NUMBER_1))));

    private ActivityControlOption activityControlOption;

    ActivityControlOptions(String nameJSON, String valueJSON) {
        activityControlOption = new ActivityControlOption(nameJSON, valueJSON);
    }

    public ActivityControlOption getOption() {
        return activityControlOption;
    }
}
