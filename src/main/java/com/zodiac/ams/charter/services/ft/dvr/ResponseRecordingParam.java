package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.util.ArrayList;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import java.util.Set;
import org.json.JSONArray;

public class ResponseRecordingParam implements IDVRObject{
    public static final int  SCHEDULE_CONFLICT = 4;
    
    private Integer recordingId;
    private Integer statusCode;
    private List<Integer> conflicts;
    private Integer unitingParameter;
    
    private Integer serviceId; //from HE
    private Integer startTime; //from HE
    
    public ResponseRecordingParam(){
    }
    
    public ResponseRecordingParam(ResponseRecordingParam param){
        recordingId = param.recordingId;
        statusCode = param.statusCode;
        serviceId = param.serviceId; 
        startTime = param.startTime;
        unitingParameter = param.unitingParameter;
        if (null != param.conflicts){
            conflicts = new ArrayList<>();
            conflicts.addAll(param.conflicts);
        }
    }
    
    public static ResponseRecordingParam getRandom(boolean repeat, Integer status){
        ResponseRecordingParam rr = new ResponseRecordingParam();
        rr.recordingId = getRandomInt(1, 1_000_000);
        rr.statusCode = status; 
        if (status == Error.Error0.getCodeSTB() && repeat)
            rr.unitingParameter = getRandomInt(1, 1_000);
        if (status == Error.SCHEDULE_CONFLICT.getCodeSTB()){
            int cs = getRandomInt(1, 5);
            rr.conflicts = new ArrayList<>();
            for (int i=0; i < cs; i++)
                rr.conflicts.add(getRandomInt(1, 1_000_000));
        }
        return rr;
    }
    
    public void setUnitingParameter(Integer unitingParameter){
        this.unitingParameter = unitingParameter;
    } 
    
    public void setRecordingId(Integer recordingId){
        this.recordingId = recordingId;  
    }
    
    public void setServiceId(Integer serviceId){
        this.serviceId = serviceId;
    }
    
    public void setStatusCode(Integer status){
        this.statusCode = status;
    }
    
    public void setStartTime(int startTime){
        this.startTime = startTime;
    }
    
    public Integer getRecordingId(){
        return recordingId;
    }
    
    public Integer getUnitingParameter(){
        return unitingParameter;
    }
    
    @Override
    public byte[]  getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            if (null != recordingId)
                out.write(DOB7bitUtils.encodeUInt(recordingId));
            if (null != statusCode)
                out.write(DOB7bitUtils.encodeUInt(statusCode));
            if (null != unitingParameter)
                out.write(DOB7bitUtils.encodeUInt(unitingParameter));
            if (null != conflicts){
                out.write(DOB7bitUtils.encodeUInt(conflicts.size()));
                for (int conflict: conflicts)
                    out.write(DOB7bitUtils.encodeUInt(conflict));
            }
            return out.toByteArray();
        }
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject obj = new JSONObject();
        if (null != serviceId && !excluded.contains("serviceId"))
            obj.put("serviceId", serviceId.intValue());
        if (null != statusCode && !excluded.contains("statusCode"))
            obj.put("statusCode", statusCode.intValue());
        if (null != recordingId && !excluded.contains("recordingId"))
            obj.put("recordingId", recordingId.intValue());
        if (null != startTime && !excluded.contains("startTime"))
            obj.put("startTime", startTime.intValue());
        if (null != conflicts){
            JSONArray arr = new JSONArray();
            for (int conflict: conflicts){
                JSONObject obj1 = new JSONObject();
                obj1.put("recordingId", conflict);
                arr.put(obj1);
            }
            obj.put("conflictRecordings", arr);
        }
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}

