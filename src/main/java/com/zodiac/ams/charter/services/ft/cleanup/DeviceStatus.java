package com.zodiac.ams.charter.services.ft.cleanup;

import org.json.JSONObject;

public class DeviceStatus {
    public static final String OK = "OK";
    public static final String FAILED = "Failed";
    
    
    private static final String DEVICE_ID = "deviceId";
    private static final String STATUS = "status";
    private static final String ERROR_MESSAGE = "errorMessage";
    
    private final String deviceId;
    private final String status;
    private final String errorMessage;
    
    public DeviceStatus(String deviceId){
        this(deviceId, OK, null);
    }
    
    public DeviceStatus(String deviceId, String status, String errorMessage){
        this.deviceId = deviceId;
        this.status = status;
        this.errorMessage = errorMessage; 
    }
    
    
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        if (null != deviceId)
            obj.put(DEVICE_ID, deviceId);
        if (null != status)
            obj.put(STATUS, status);
        if (null != errorMessage)
            obj.put(ERROR_MESSAGE, errorMessage);
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
