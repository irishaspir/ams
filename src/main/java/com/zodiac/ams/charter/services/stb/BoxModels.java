package com.zodiac.ams.charter.services.stb;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.store.RudpKeyWordStore.UNKNOWN_ENVIRONMENT;

public class BoxModels {

    private List<BoxModelOptions> boxModelsList = new ArrayList<>();
    private List<List<String>> dataFromDB;

    public BoxModels(List<List<String>> dataFromDB) {
        this.dataFromDB = dataFromDB;
        setBoxModelsOptions();
    }

    private void setBoxModelsOptions() {
        for (int i = 0; i < dataFromDB.size(); i++)
            boxModelsList.add(new BoxModelOptions(dataFromDB.get(i)));
    }

    public int getBoxModelId(String name) {
        for (BoxModelOptions boxModel : boxModelsList) {
            if (name.equals(boxModel.getRoumId()))
                return boxModel.getModelId();
        }
        return UNKNOWN_ENVIRONMENT;
    }

    public List<BoxModelOptions> getBoxModelsList() {
        return boxModelsList;
    }
}
