package com.zodiac.ams.charter.services.ft.defent.dsg;

import org.json.JSONArray;

public abstract class DSGMessage implements IDSGMessage{
    protected int version = Constants.DEFAULT_VERSION;
    protected byte requestId;
    protected String mac;
    protected ConnectionMode connectionMode;
    protected int boxModel; 
    protected Vendor vendor;
    protected long messageTypesMap;
    protected boolean migrationStart;
    protected int hubId; 
    protected int serviceGroupId; 
    protected String loadBalancerIpAddress; 
    protected int swVersion; 
    protected int sdvServiceGroupId; 
    protected long cmMacAddress; 
    protected String serialNumber;
    protected String cmIp;
    
    public DSGMessage(byte requestId,String mac){
        this.requestId = requestId;
        this.mac = mac;
    }
    
    public void setVersion(int version){
        this.version = version;
    }
    
    public int getVersion(){
        return version;
    }
    
    public void setConnectionMode(ConnectionMode connectionMode){
        this.connectionMode = connectionMode;
    }
    
    public ConnectionMode getConnectionMode(){
        return connectionMode;
    }
    
    public void setBoxModel(int boxModel){
        this.boxModel = boxModel;
    }
    
    public int getBoxModel(){
        return boxModel;
    }
    
    public void setVendor(Vendor vendor){
        this.vendor = vendor;
    }
    
    public Vendor getVendor(){
        return vendor;
    }
    
    public void setMessageTypesMap(long messageTypesMap){
        this.messageTypesMap = messageTypesMap;
    }
    
    public long getMessageTypesMap(){
        return messageTypesMap;
    }
    
    public void setMigrationStart(boolean migrationStart){
        this.migrationStart = migrationStart;
    }
    
    public boolean getMigrationStart(){
        return migrationStart;
    }
    
    public void setHubId(int hubId){
        this.hubId = hubId;
    }
    
    public int getHubId(){
        return hubId;
    }
    
    public void setServiceGroupId(int serviceGroupId){
        this.serviceGroupId = serviceGroupId;
    }
    
    public int getServiceGroupId(){
        return serviceGroupId;
    }
    
    public void setLoadBalancerIpAddress(String loadBalancerIpAddress){
        this.loadBalancerIpAddress = loadBalancerIpAddress;
    }
    
    public String getLoadBalancerIpAddress(){
        return loadBalancerIpAddress;
    }
    
    public void setSwVersion(int swVersion){
        this.swVersion = swVersion;
    }
    
    public int getSwVersion(){
        return swVersion;
    }
    
    public void setSdvServiceGroupId(int sdvServiceGroupId){
        this.sdvServiceGroupId = sdvServiceGroupId;
    }
    
    public int getSdvServiceGroupId(){
        return sdvServiceGroupId;
    }
    
    public void setCmMacAddress(long cmMacAddress){
        this.cmMacAddress = cmMacAddress;
    }
    
    public long getCmMacAddress(){
        return cmMacAddress;
    }
    
    public void setSerialNumber(String serialNumber){
        this.serialNumber = serialNumber;
    }
    
    public String getSerialNumber(){
        return serialNumber;
    }
    
    public void setCmIp(String cmIp){
        this.cmIp = cmIp;
    }
    
    public String getCmIp(){
        return cmIp;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
    @Override
    public JSONArray toJSONArray(){
        return toJSON().getJSONObject("settings").getJSONObject("attributes").getJSONArray("options");
    }
    
    protected byte[] encodeMac(long mac){
        byte[] result = new byte[6];
        result[0] = (byte)((mac>>40) & 0xff); 
        result[1] = (byte)((mac>>32) & 0xff); 
        result[2] = (byte)((mac>>24) & 0xff); 
        result[3] = (byte)((mac>>16) & 0xff); 
        result[4] = (byte)((mac>>8) & 0xff); 
        result[5] = (byte)(mac & 0xff); 
        return result;
    }
}
