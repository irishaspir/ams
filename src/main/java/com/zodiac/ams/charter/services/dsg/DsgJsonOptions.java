package com.zodiac.ams.charter.services.dsg;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum DsgJsonOptions {

    STB_MAC_ADDRESS("STB MAC Address", "MAC_STR", "000000000000", "", new String[]{}),

    CONNECTION_MODE("Connection Mode", "STB_MODE", "","", new String[]{}),

    HUB_ID("Hub ID", "HUBID", "-1", "", new String[]{}),

    VCT_ID("VCT ID", "", "-1", "", new String[]{}),

    SGID("SGID", "", "0", "", new String[]{}),

    AMSIP("AMSIP", "", "0.0.0.0", "", new String[]{}),

    MODEL("Model", "", "0", "", new String[]{}),

    VENDOR("Vendor", "", "M", "", new String[]{}),

    TSID("TSID", "", "0", "", new String[]{}),

    HOST_RF_IP("Host RF IP", "", "127.0.0.1", "", new String[]{}),

    SW_VERSION("SW Version", "", "", "", new String[]{}),

    SW_VERSION_TMP("SW Version", "", "", "", new String[]{}),

    SDV_SGID("SDV SGID", "", "0", "", new String[]{}),

    SERIAL_NUMBER("Serial Number", "", "null", "", new String[]{}),

    CM_MAC_ADDRESS("CM MAC Address", "", "000000000000", "", new String[]{}),

    CM_IP("CM IP", "", "0.0.0.0", "", new String[]{});

    private DsgJsonOption dsgOption;

    DsgJsonOptions(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        dsgOption = new DsgJsonOption(name, columnName, value, defaultValueInDB, range);
    }

    public DsgJsonOption getOption() {
        return dsgOption;
    }


    public static List<DsgJsonOption> allDsgOptions() {
        ArrayList<DsgJsonOption> options = new ArrayList<>();
        for (DsgJsonOptions one : Arrays.asList(DsgJsonOptions.values())) {
            options.add(one.getOption());
        }
        return options;
    }
}
