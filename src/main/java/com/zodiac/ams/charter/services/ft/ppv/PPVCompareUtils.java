package com.zodiac.ams.charter.services.ft.ppv;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PPVCompareUtils {
    
    final static Logger LOG = LoggerFactory.getLogger(PPVCompareUtils.class);
    
    private final static Pattern MIN_MAX_PATTERN = Pattern.compile("(\\d+)\\s*\\.{2,}\\s*(\\d+)|(\\d+)");
    
    //"1"    -> {0,1}
    //"1..2" -> {1,2}
    private static int[] getMinMax(String str){
        int [] result = new int[]{0,0};
        Matcher m = MIN_MAX_PATTERN.matcher(str);
        if (m.matches()){
            if (null != m.group(1) && null != m.group(2)){
                result[0] = Integer.parseInt(m.group(1));
                result[1] = Integer.parseInt(m.group(2));
            }
            else
                result[1] = Integer.parseInt(m.group(0));
        }
        return result;
    }
    
    
    public static boolean equal(PPVMetaData data1, PPVMetaDataZDB data2){
        try {
            if ((long)data2.showing_time * 1000L != data1.getDate().getTime())
                return false;
            if (data1.advert != data2.advert)
                return false;
            if (data1.length != data2.duration)
                return false;
            int [] purchase  = getMinMax(data1.purchase);
            if (purchase[0] != data2.purchase_lower || purchase[1] != data2.purchase_higher)
                return false;
            if (data1.sourceid != data2.service_id)
                return false;
            int [] preview = getMinMax(data1.preview);
            if (preview[0] != data2.review_lower || preview[1] != data2.review_higher)
                return false;
            if (!data1.title.equalsIgnoreCase(data2.title))
                return false;
            if (data1.eid  != data2.eid)
                return false;
            return true;
        }
        catch(Exception ex){
            LOG.error("equal method exception: {}", ex.getMessage());
        }
        return false;
    }
    
    //source must be sorted before call this method
    public static List<PPVMetaData> filterByDate(List<PPVMetaData> source, Date start, Date stop){
        List<PPVMetaData> result =  new ArrayList<>();
        try{
            for (PPVMetaData md: source){
                Date dt = md.getDate();
                if (dt.equals(stop) || dt.after(stop))
                    break;
                if (dt.equals(start) || dt.after(start))
                    result.add(md);
            }
        }
        catch(Exception ex){
            LOG.error("fileterByDate method exception: {}", ex.getMessage());
        }
        return result;
    }
}
