        package com.zodiac.ams.charter.services.ft.publisher;

public class Constants {
    public static final String ZDTS_INI = "z_dts.ini";
    public static final String ROOT_ZDTS_INI = "root_z_dts.ini";
    public static final String ZDTS_INI_ZIP = "z_dts.zip";
    public static final String ROOT_ZDTS_INI_ZIP = "root_z_dts.zip";
    public static final String ZDTS_PROP = "zdts.name";
    public static final String TMP_LOCAL = "temp/local";
    public static final String TMP_REMOTE = "temp/remote";
    public static final String PUBLISHER_DATA_DIR = "dataPublisher";    
    public static final String MODES_PROP = "server.modes";
    public static final String TYPES_PROP = "server.types";
    public static final String ROOT_ZDTS_PROP = "root.zdts.name"; 
    public static final int MAX_MINUTES = 5;
    public static final long MAX_TIMESTAMP = (MAX_MINUTES+1)*60*1000;
}
