package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by anton on 18.05.16.
 */
public class DataPacketArray implements DataSerializable {
    private static final DataPacket NULL = new DataPacket(-1, -1, new byte[0]);
    private static final DataPacket[] EMPTY_ARRAY = new DataPacket[0];

    private DataPacket[] array;

    public DataPacketArray() {
        array = EMPTY_ARRAY;
    }

    public DataPacketArray(int size) {
        this.array = new DataPacket[size];
        Arrays.fill(array, NULL);
    }

    public DataPacket get(int segmentNumber) {
        if(segmentNumber >= 0 && segmentNumber < array.length) {
            return array[segmentNumber];
        }
        return null;
    }

    public void set(int segmentNumber, DataPacket dp) {
        array[segmentNumber] = dp;
    }


    public boolean isEmpty(int segmentNumber) {
        final DataPacket dataPacket = array[segmentNumber];
        return dataPacket.getFileId() == NULL.getFileId() && dataPacket.getSegmentNumber() == NULL.getSegmentNumber();
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        int size = array == null ? 0 : array.length;
        out.writeInt(size);
        for (int i = 0; i < size; i++) {
            array[i].writeData(out);
        }
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        int size = in.readInt();
        array = new DataPacket[size];
        for (int i = 0; i < size; i++) {
            final DataPacket dataPacket = new DataPacket();
            dataPacket.readData(in);
            array[i] = dataPacket;
        }
    }
}
