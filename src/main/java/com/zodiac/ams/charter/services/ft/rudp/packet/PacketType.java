package com.zodiac.ams.charter.services.ft.rudp.packet;

public enum PacketType {
    START(0x1),
    DATA(0x2),
    ACK(0x3),
    ERROR(0x4);

    public final byte packetType;

    PacketType(int packetType) {
        this.packetType = (byte) packetType;
    }

    public static PacketType fromByte(byte b) {
        int t = 0xFF & b;
        return fromInt(t);
    }

    public static PacketType fromInt(int b) {
        for (PacketType p : PacketType.values()) {
            if (p.packetType == b) {
                return p;
            }
        }
        return null;
    }
}
