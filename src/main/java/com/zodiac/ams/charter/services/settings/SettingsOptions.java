package com.zodiac.ams.charter.services.settings;


import java.util.ArrayList;
import java.util.Arrays;

public enum SettingsOptions {

    DEFAULT_AUDIO("Default Audio", "KEY1", "English", "0", new String[]{"0", "1", "2", "3"}),

    DVS("DVS", "KEY2", "Off", "0", new String[]{"0", "1"}),

    STEREO_AUDIO("Stereo Audio", "KEY3", "Standard", "1", new String[]{"0", "1", "2"}),

    VIDEO_OUTPUT_FORMAT("Video Output Format", "KEY4", "720p", "2", new String[]{"0", "1", "2", "3", "4"}),

    SD_ON_HD_ASPECT_RATIO("SD on HD Aspect Ratio", "KEY5", "Pillar Bars", "0", new String[]{"0", "1"}),

    HD_ON_SD_ASPECT_RATIO("HD on SD Aspect Ratio", "KEY6", "Letterbox", "0", new String[]{"0", "1"}),

    CHANNEL_BAR_POSITION("Channel Bar Position", "KEY7", "Bottom", "0", new String[]{"0", "1"}),

    MINI_GUIDE_DISPLAY_POSITION("Mini Guide Display Position", "KEY8", "Bottom", "0", new String[]{"0", "1"}),

    HD_AUTO_TUNE("HD Auto Tune", "KEY9", "On", "1", new String[]{"0", "1"}),

    CHANNEL_BAR_DURATION("Channel Bar Duration", "KEY10", "5", "5", new String[]{"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"}),

    INFO_BANNER_DURATION("Info Banner Duration", "KEY11", "15", "15", new String[]{"5", "10", "15", "20", "25", "30"}),

    PROGRESS_BAR_DURATION("Progress Bar Duration", "KEY12", "5", "5", new String[]{"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"}),

    MINI_GUIDE_DURATION("Mini Guide Duration", "KEY13", "5", "5", new String[]{"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"}),

    ENABLE_CLOSE_CAPTION("Enable Close Captioning", "KEY14", "Off", "0", new String[]{"0", "1"}),

    CC_CAPTION_BACKGROUND_COLOR("CC Caption Background Color", "KEY15", "Black", "0", new String[]{"1", "2", "3", "4", "5", "6", "7", "0"}),

    CC_TEXT_COLOR("CC Text Color", "KEY16", "Black", "0", new String[]{"1", "2", "3", "4", "5", "6", "7", "0"}),

    CC_TEXT_SIZE("CC Text Size", "KEY17", "Medium", "1", new String[]{"1", "2", "0"}),

    CC_PRESENTATION_MODE("CC Presentation Mode", "KEY18", "Pop On", "0", new String[]{"1", "2", "0"}),

    CC_TEXT_OPACITY("CC Text Opacity", "KEY19", "Opaque", "0", new String[]{"0", "1"}),

    CC_FONTS("CC Fonts", "KEY20", "Monospaced Sans Serif", "2", new String[]{"0", "1", "2", "3", "4", "5", "6"}),

    CC_BACKGROUND_OPACITY("CC Background Opacity", "KEY21", "Opaque", "0", new String[]{"1", "2", "0"}),

    CC_CHARACTER_EDGE_ATTRIBUTES("CC Character Edge Attributes", "KEY22", "None", "0", new String[]{"1", "2", "3", "4", "0"}),

    CC_WINDOW_COLOR("CC Window Color", "KEY23", "White", "1", new String[]{"1", "2", "3", "4", "5", "6", "7", "0"}),

    CC_WINDOW_OPACITY("CC Window Opacity", "KEY24", "Opaque", "0", new String[]{"1", "2", "0"}),

    CC_LANGUAGE("CC Language", "KEY25", "CCTV1", "0", new String[]{"1", "2", "3", "4", "5","0"}),

    CC_CAPTION_SIMPLIFICATION("CC Caption Simplification", "KEY26", "Standard", "0", new String[]{"0", "1"}),

    FAVORITES("Favorites", "KEY27", "", "0|", new String[]{"3|1|2|55|"}),

    CHANNEL_FILTERS("Channel Filters", "KEY28", "", "0", new String[]{"1", "2", "4", "8"}),

    CHANNEL_SORTING("Channel Sorting", "KEY29", "By Number", "0", new String[]{"0", "1"}),

    AC_OUTLET("AC Outlet", "KEY30", "Unswitched", "1", new String[]{"0", "1"}),

    POWER_ON_CHANNEL("Power On Channel", "KEY31", "", "0", new String[]{"0","1"}),

    AUTO_POWER_OFF("Auto Power Off", "KEY32", "On", "1", new String[]{"0", "1"}),

    FRONT_CHANNEL_DISPLAY("Front Panel Display", "KEY33", "Channel", "1", new String[]{"1", "2", "3"}),

    TUNE_AWAY_WARNING("Tune Away Warning", "KEY34", "On", "1", new String[]{"0", "1"}),

    PARENTAL_CONTROL_PIN("Parental Control PIN", "KEY35", "0000", "0000", new String[]{"1111"}),

    ENABLE_DISABLE_PARENTAL_CONTROLS("Enable/Disable Parental Controls", "KEY36", "Off", "0", new String[]{"0", "1"}),

    CHANNEL_LOCKS("Channel Locks", "KEY37", "", "0|", new String[]{"3|1|2|55|"}),

    RATING_LOCKS("Rating Locks", "KEY38", ",,,", "0", new String[]{"126", "124", "120", "112", "96", "64", "3968","3840", "3584","3072", "2048", "1", "4096"}),

    CONTENT_LOCKS("Content Locks", "KEY39", "", "0", new String[]{"32", "64", "128", "256", "512", "1024", "2048", "4096", "8192", "16384"}),

    HIDE_ADULT_TITLES("Hide Adult Content", "KEY40", "On", "1", new String[]{"0", "1"}),

    TIME_BLOCKS("Time Blocks", "KEY41", "", "0|", new String[]{"1|2|660|120|"}),

    CALLER_ID_NOTIFICATION("Caller ID Notification", "KEY42", "Off", "0", new String[]{"0", "1"}),

    CALLER_ID_DISPLAY("Caller ID Display", "KEY43", "Top", "0", new String[]{"0", "1"}),

    PURCHASE_PIN("Purchase PIN", "KEY44", "0000", "0000", new String[]{"1111"}),

    PURCHASE_PIN_ACTIVATE("Purchase PIN Active", "KEY45", "On", "1", new String[]{"0", "1"}),

    AUDIO_OUTPUT("Audio Output", "KEY46", "HDMI", "2", new String[]{"0", "1", "2"}),

    TUNER_CONFLICT_REMINDER("Tuner Conflict Reminder", "KEY47", "2", "2", new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"}),

    LIVE_PROGRAM_EXTENSION("Live Program Extension", "KEY48", "Select by program", "1", new String[]{"0", "1", "60"}),


    WARNING_ON_TUNE_TO_RECORDING("Warning on Tune to Recording", "KEY49", "On", "1", new String[]{"0","1"}),


    LIVE_CONTENT_BUFFERING("Live Content Buffering", "KEY50", "60", "60", new String[]{"0", "30", "60", "90", "120", "125", "180"}),

    RECORD_DUPLICATES("Record Duplicates", "KEY51", "Off", "0", new String[]{"0", "1"}),

    SLEEP_TIMER("Sleep Timer", "KEY52", "Off", "0", new String[]{"0", "1", "2", "3", "4", "5"}),

    POWER_OFF_TIMER("Power Off Timer", "KEY53", "", "0|", new String[]{"1|0|2|780|"}),

    POWER_ON_TIMER("Power On Timer", "KEY54", "", "0|", new String[]{"1|0|2|720|"}),

    REMINDER_TIMER("Reminder Timer", "KEY55", "0", "0", new String[]{"1"}),     //not supported

    FRONT_PANEL_DISPLAY_BRIGHTNESS("Front Panel Display Brightness", "KEY56", "High", "2", new String[]{"0", "1", "2"}),

    TIME_DISPLAY("Time Display", "KEY57", "12", "0", new String[]{"0", "1"}),

    GUIDE_COLOR_THEME("Guide Color Theme", "KEY58", "Dark", "1|", new String[]{"0|"}),   //"Variable (6AM-7PM)"   ,"Dark"

    ON_DEMAND_LOCK("On Demand Lock", "KEY59", "Off", "0", new String[]{"0", "1"}),  // не могут приходить с che

    MESSAGE_INDICATOR("Message Indicator", "KEY60", "On", "1", new String[]{"0", "1"}), // не могут приходить с che

    GUIDE_PRESENTATION("Guide Presentation", "KEY61", "Standard Definition (4:3)", "1", new String[]{"0", "1"}), //"Standard Definition (4:3)", "High Definition (16:9)"

    ON_DEMAND_PIN("On Demand PIN", "KEY62", "0000", "0000", new String[]{"1111"}),

    GUIDE_NARRATION("Guide Narration", "KEY63", "Off", "0", new String[]{"1", "0"}),

    ENERGY_SAVING_MODE("Energy Savings Mode", "KEY64", "High", "1", new String[]{"0", "1"}),

   // TUNE_TO_RECORDING_WARNING("Tune To Recording Warning", "KEY65", "Off", "0", new String[]{"0", "1"}),

    TURN_ON_OFF_REMINDER("Turn On/Off Reminders", "KEY66", "On", "1", new String[]{"0", "1"});


    private SettingsOption settingsOption;

    SettingsOptions(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        settingsOption = new SettingsOption(name, columnName, value, defaultValueInDB, range);
    }

    public SettingsOption getOption() {
        return settingsOption;
    }

    public static ArrayList<SettingsOption> allSettings() {
        ArrayList<SettingsOption> options = new ArrayList<>();
        for (SettingsOptions one : Arrays.asList(SettingsOptions.values())) {
            options.add(one.getOption());
        }
        return options;
    }


}
