package com.zodiac.ams.charter.services.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.packet.AckStartPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPReceiverSession;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSenderSession;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSession;
import java.util.concurrent.Semaphore;


public class RUDPTestingSession {
    IRUDPSession session;
    RUDPTestingPackage pckg;
    final Semaphore semaphore = new Semaphore(0);
    private AckStartPacket ackStartPacket;
    boolean success = true;
    String errorMsg;
    final StringBuilder message = new StringBuilder();
    byte [] remoteData;
    
        
    RUDPTestingSession(RUDPTestingPackage pckg,IRUDPSession session){
        this.pckg = pckg;
        this.session = session;
    }
    
    public IRUDPSession getRUDPSession(){
        return session;
    }
    
    public IRUDPSenderSession getRUDPSenderSession(){
        return (session instanceof IRUDPSenderSession) ? (IRUDPSenderSession)session : null;
    }
    
    public IRUDPReceiverSession getRUDPReceiverSession(){
        return (session instanceof IRUDPReceiverSession) ? (IRUDPReceiverSession)session : null;
    }
    
    public RUDPTestingPackage getRUDPTestingPackage(){
        return pckg;
    }
    
    public synchronized void setAckStartPacket(AckStartPacket ackStartPacket,boolean replace){
        if (replace || null == ackStartPacket)
            this.ackStartPacket = ackStartPacket;
    }
    
    public synchronized AckStartPacket getAckStartPacket(){
        return ackStartPacket;
    }
    
    public boolean isSuccess(){
        return success;
    }
    
    public void setSuccess(boolean success){
        this.success = success;
    }
    
    public synchronized String getErrorMsg(){
        return errorMsg;
    }
    
    public synchronized String getMsg(){
        return message.toString();
    }
    
    public synchronized void addMsg(String msg){
        message.append(msg).append("\n");
    }
    
    public synchronized void setErrorMsg(String errorMsg){
        setErrorMsg(errorMsg,false);
    }
    
    public synchronized void setErrorMsg(String errorMsg,boolean replace){
        if (replace || null == this.errorMsg || this.errorMsg.trim().equals(""))
            this.errorMsg = errorMsg;
    }
    
    public Semaphore getSemaphore(){
        return semaphore;
    }
    
    public byte[] getRemoteData(){
        return remoteData;
    }
    
    public void setRemoteData(byte[] remoteData){
        this.remoteData = remoteData;
    }
}
