package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum Method {
    ERASE(3), SCHEDULE(4), MODIFY(8), STOP(10), PRIORITIZE(11), RECORDED_PROGRAM_UPDATE(21);
    
    private final int code;
    
    private Method(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
}
