
package com.zodiac.ams.charter.services.ft.rudp;

import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessagePackage;
import com.dob.ams.transport.zodiac.util.ZodiacTransportUtils;
import com.zodiac.ams.common.helpers.FTUtils;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;


public class RUDPTestingPackage {
    
    private final static String SERVICE_ID = "RUDP_TESTING";
    private ZodiacMessagePackage pckg;
    private ZodiacMessage message;
    private int segmentSize;
    
    private RUDPTestingPackage(){
    }
    
    
    public byte[] toBytes() throws Exception{
        return (null == pckg) ? null : ZodiacTransportUtils.encodePackage(pckg);
    }
    
    public byte[] getData(){
        return (null == message) ? null : message.getData();
    }
    
    public int getSegmentsCount() throws Exception{
        int length = toBytes().length+4;
        return length/segmentSize + (0 == length%segmentSize ? 0 : 1);
    }
    
    static RUDPTestingPackage createPacket(long mac, ZodiacMessage source){
        RUDPTestingPackage result = new RUDPTestingPackage();
        
        ZodiacMessagePackage pk = new ZodiacMessagePackage();
        pk.setMac(mac);
        
        int id = Integer.parseInt(source.getMessageId());
        
        ZodiacMessage msg = new ZodiacMessage();
        msg.setBodyBytes();
        msg.setSender(SERVICE_ID);
        msg.setAddressee(SERVICE_ID);
        msg.setMac(mac);
        msg.setData(source.getData());
        msg.setCorrelationId(""+ id++);
        msg.setMessageId(""+id);
        msg.setFlag(ZodiacMessage.FL_ANSWER);
        
        pk.addMessage(msg);
                      
        result.message = msg;
        result.pckg = pk;
        return result;
    }
    
    private static RUDPTestingPackage createPacket(String id,byte[] data){
        ZodiacMessage message = new ZodiacMessage();
        message.setBodyBytes();
        message.setSender(SERVICE_ID);
        message.setAddressee(SERVICE_ID);
        message.setMac(Long.parseLong(id,16));
        message.setData(data);
      
        ZodiacMessagePackage pckg = new ZodiacMessagePackage();
        pckg.setMac(message.getMac());
        pckg.addMessage(message);
        
        RUDPTestingPackage result = new RUDPTestingPackage(); 
        result.message = message;
        result.pckg = pckg;
        return result;
    }
    
    private static RUDPTestingPackage createPacket(String id,String name) throws Exception{
        return createPacket(id,Files.readAllBytes(Paths.get(name)));
    }
    
    static RUDPTestingPackage createPacket(String id,int length,int segmentSize,int fileId) throws Exception{
        RUDPTestingPackage tmp = createPacket(id,new byte[]{});
        
        int data_size = length - tmp.toBytes().length;
        if (data_size<0)
            throw new Exception("Unable to create message with length="+length);
        
        Random random = new Random();
        //+-2 bytes?????????
        for (int it=0;it<10;it++){
            byte [] data = new byte[data_size];
            FTUtils.writeInt(fileId, data);
            for (int i=4;i<data_size;i++)
                data[i] = (byte)(random.nextInt(128) & 0xFF);
        
            RUDPTestingPackage result = createPacket(id,data);
            result.segmentSize = segmentSize;
            int l = result.toBytes().length;
            if (l == length)
                return result;
            if (l>length)
                data_size--;
            else 
                data_size++;
        }
        
        throw new Exception("Unable to create packet with given length="+length);
    }
    
}
