package com.zodiac.ams.charter.services.ft.defent.dsg;

public enum Vendor {
    unknown(0), Cisco(10)/*DNCS*/, Humax(20), Motorola(30)/*DAC*/, Pace(40);
    
    private final int code;
    
    private Vendor(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
}
