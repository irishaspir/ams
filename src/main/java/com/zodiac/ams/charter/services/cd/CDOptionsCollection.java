package com.zodiac.ams.charter.services.cd;


import java.util.ArrayList;
import java.util.List;


public class CDOptionsCollection {

    private ArrayList<CDProtocolOption> list = new ArrayList<>();

    public CDOptionsCollection addOption(CDProtocolOption option) {
        list.add(new CDProtocolOption(option.getName(), option.getRequestId(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public CDOptionsCollection addOptions(CDProtocolOptions option) {
        list.add(new CDProtocolOption(option.getOption().getName(), option.getOption().getRequestId(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<CDProtocolOption> build() {
        return list;
    }

    public CDOptionsCollection addListOptions(List<CDProtocolOption> list) {
        this.list.addAll(list);
        return this;
    }

}
