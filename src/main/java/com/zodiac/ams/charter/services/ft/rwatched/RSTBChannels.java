package com.zodiac.ams.charter.services.ft.rwatched;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dob.test.charter.CharterStbEmulator;
import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import java.io.ByteArrayOutputStream;
import java.util.TimeZone;


public class RSTBChannels {
    static final String MAC_PROP = "mac";
    static final String STATUS_PROP = "status";
    static final String ERROR_PROP = "errorMessage";
    static final String COUNT_PROP = "count";
    static final String START_NUMBER_PROP = "startNumber";
    static final String START_TIME_PROP =  "startTime";
    
    static DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
    
    private final static Logger logger = LoggerFactory.getLogger(RSTBChannels.class);
    
    String mac;
    String status;
    String error;
    String count;
    String startNumber;
    String startTime;
    RChannels channels;
    
     public static void setTimeZone(String id){
        if (null == id || id.trim().equals(""))
            format.setTimeZone(TimeZone.getDefault());
        else
            format.setTimeZone(TimeZone.getTimeZone(id));
    }
    
    private void init(String string) throws Exception{
        JSONObject json = new JSONObject(string);
        mac = json.optString(MAC_PROP,null);
        status = json.optString(STATUS_PROP,null);
        error = json.optString(ERROR_PROP,null);
        count = json.optString(COUNT_PROP,null);
        //startNumber = json.optString(START_NUMBER_PROP,null);
        //startTime = json.optString(START_TIME_PROP,null);
        channels = null;
        JSONArray array = json.optJSONArray(RChannels.CHANNELS_PROP);
        if (null != array){
            channels = new RChannels();
            for (int i=0;i<array.length();i++){
                JSONObject obj=array.getJSONObject(i);
                String ch=obj.optString(RChannel.CHANNEL_PROP,null);
                String tm=obj.optString(RChannel.TIME_PROP,null);
                RChannel channel=new RChannel(ch,tm);
                channels.addChannel(channel);
                if (0 == i){
                    startNumber = ch;
                    startTime = tm;
                }
            }
        }
    }
    
    private void init(File file) throws Exception{
        String string;
        try (BufferedReader in = new BufferedReader(new FileReader(file))){
            StringBuilder sb=new StringBuilder();
            String line;
            while (null != (line=in.readLine())){
                sb.append(line);
                sb.append("\n");
            }
            string = sb.toString();
        }
        init(string);
    }
    
    public RSTBChannels(){
    }
    
    public RSTBChannels(String string) throws Exception{
        init(string);
    }
    
    public RSTBChannels(File file) throws Exception{
        init(file);
    }
    
    public void setCount(String count){
        this.count = count;
    }
    
    public void setMAC(String mac){
        this.mac = mac;
    }
    
    public void setStatus(String status){
        this.status = status;
    }
    
    public void setError(String error){
        this.error = error;
    }
    
    public void setStartNumber(String startNumber){
        this.startNumber = startNumber;
    }
    
    public void setStartTime(String startTime){
        this.startTime = startTime;
    }
    
    public void setChannels(RChannels channels){
        this.channels = channels;
    }
    
    public int getStartNumber(){
        return (null == startNumber) ? 0 : Integer.parseInt(startNumber);
    }
    
    public int getStartTimeMins(){
        if (null == startTime)
            return 0;
        try{
            Date dt=format.parse(startTime);
            return (int)(dt.getTime()/60_000L);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        return 0;   
    }
    
    public JSONObject toJSONObject(){
        JSONObject json=new JSONObject();
        if (null != mac)
            json.put(MAC_PROP,mac);
        if (null != status)
            json.put(STATUS_PROP,status);
        if (null != error)
            json.put(ERROR_PROP,error);
        if (null != count)
            json.put(COUNT_PROP,count);
        //if (null != startNumber)
        //    json.optString(START_NUMBER_PROP,startNumber);
        //if (null != startTime)
        //    json.put(START_TIME_PROP,startTime);
        if (null != channels){
            JSONArray array=channels.toJSONArray();
            json.put(RChannels.CHANNELS_PROP,array);
        }
        return json;
    }
    
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof RSTBChannels))
            return false;
        RSTBChannels ch=(RSTBChannels)obj;
        if (!FTUtils.stringsEqual(mac, ch.mac) || !FTUtils.stringsEqual(status, ch.status) || !FTUtils.stringsEqual(error, ch.error) ||
            !FTUtils.stringsEqual(count, ch.count)) // || !Utils.equals(startNumber, ch.startNumber) || !Utils.equals(startTime, ch.startTime))
            return false;
        if (null == channels && null == ch.channels)
            return true;
        return (null == channels) ? false : channels.equals(ch.channels);
    }
    
    @Override
    public String toString(){
        return toJSONObject().toString();
    }
    
    public void save(String name) throws IOException {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name)))){
            out.print(toString());   
        }
    }
    
    private byte[] getMessageData() {
        byte[] body = null;
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()){
            int channelCount=0;
            int sNumber=getStartNumber();
            int sTime=getStartTimeMins();
            
            baos.write(DOB7bitUtils.encodeUInt(0));
            
            if (null != channels){
                channelCount=channels.size();
                logger.info("Generated STB Message: ChannelCount={}",channelCount);
                baos.write(DOB7bitUtils.encodeUInt(channelCount));
            }
            
            if (null != startNumber){
                logger.info("Generated STB Message: StartNumber={}",startNumber);
                baos.write(DOB7bitUtils.encodeUInt(sNumber));
            }
            if (null != startTime){
                logger.info("Generated STB Message: StartTime={} mins={}",startTime,sTime);
                baos.write(DOB7bitUtils.encodeUInt(sTime));
            }
            
            if (null != channels){
                for (int i=1;i<channels.size();i++){
                    RChannel channel=channels.getChannel(i);
                    if (null != channel.getChannelNumber()){
                        int diff=channel.getChannel()-sNumber;
                        logger.info("Generated STB Message: Channel's number={} Diff={}",channel.getChannelNumber(),diff);
                        baos.write(DOB7bitUtils.encodeInt(diff));
                    }
                    if (null != channel.getLastWatchedTime()){
                        int diff=channel.getTimeMins()-sTime;
                        logger.info("Generated STB Message: Channel's last watched time={} Diff={}",channel.getLastWatchedTime(),diff);
                        baos.write(DOB7bitUtils.encodeUInt(diff));
                    }
                }
            }
            baos.flush();
            body = baos.toByteArray();
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
        return body;
    }
     
    
    
    public  void send(CharterStbEmulator emulator,String id) throws Exception{
        ZodiacMessage message = new ZodiacMessage();
        message.setBodyBytes();
        message.setMessageId(String.valueOf(0));
        message.setSender("R");//recwcht");
        message.setAddressee("R");//"recwcht");
        message.setMac(Long.parseLong(id,16));
        
        //message.setFlag(ZodiacMessage.FL_PERSISTENT);
        //message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
        //message.setFlag(ZodiacMessage.FL_COMPRESSED);
        
        message.setData(this.getMessageData());
        
        IZodiacMessage response = emulator.getRUDPTransport().send(message);
        //logger.info("AMS response: {}",response.toString());
    }    
}
