package com.zodiac.ams.charter.services.ft.defent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class GroupImpl implements IGroup{
    protected final static Logger LOG = LoggerFactory.getLogger(GroupImpl.class);
     
    protected static final int [] CHANNEL_TYPE = {};
    protected static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    protected final String mac;
    protected final EntError[] errors;
    protected final int requestId = 0;
    
    public GroupImpl(String mac, EntError error){
        this(mac, new EntError[] {error});
    }
    
    public GroupImpl(String mac, EntError[] errors){
        this.mac = mac;
        this.errors = errors;
    }
    

    
    protected static byte getGroupDescription(int group, int count){
        return (byte) (((group & 0x3)<<6) | (count & 0x3F));
    }
    
    protected static int getNow() {
        try {
            return (int)(DATE_FORMAT.parse(DATE_FORMAT.format(new Date())).getTime() / 60_000L);
        }
        catch(ParseException ex){
            LOG.error("getNow: {}", ex.getMessage());
            return 0;
        }
    }
}
