package com.zodiac.ams.charter.services.ft.dvr;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.zodiac.ams.charter.helpers.ft.CassandraHelper;
import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.helpers.ft.FtDbHelper;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.EpisodesDefinition;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.RecordingFormat;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.SaveDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import com.zodiac.ams.common.bd.DBHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.selectDB;
import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.selectCassandra;
import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.deleteDB;
import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.deleteCassandra;
import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.insertDB;
import static com.zodiac.ams.charter.helpers.ft.FtDbHelper.insertCassandra;
import java.sql.Clob;
import java.util.Arrays;

public class DvrDbHelper {
    
    private static final String SQL_SELECT_RECORDING = 
        "SELECT " +
	"RECORDING_ID,RECORDING_TYPE,SERVICE_ID,START_TIME,DURATION,UNITED_FLAGS,START_OFFSET," +
	"END_OFFSET,ACTUAL_START,ACTUAL_DURATION,UNITING_PARAM,BOOKMARK,RECORDING_STATUS," +
	"STORAGE,REPEAT_VAL,CHANNEL_NUMBER,BOOKMARK_TIME,ADD_DATA_CRC,RECORDING_CRC,UPDATE_TIMESTAMP "+
        "FROM DVR_RECORDINGS "+    
        "WHERE MAC=%d  AND RECORDING_ID=%d";
    
    
    private static final String CQL_SELECT_RECORDINGS =
        "SELECT DEVICEID,RECORDINGID," + 
        "channelid,channelnumber,duplicate,episodedef,keepuntil,recformat,rectype,startoffset,endoffset,repeat " +
        "FROM RECORDINGS WHERE DEVICEID='%s' and RECORDINGID=%d";
    
    public static class DBCounts{
        public final int dbCount;
        public final int cassandraCount;
        
        public DBCounts(){
            this(0,0);
        }
        
        public DBCounts(int count){
            this(count, count);
        }
        
        public DBCounts(int dbCount,int cassandraCount){
            this.dbCount = dbCount;
            this.cassandraCount = cassandraCount;
        }
        
        @Override
        public String toString(){
            return "DB Count=" + dbCount + " Cassandra Count=" + cassandraCount;
        }
        
        @Override
        public boolean equals(Object obj){
            if (obj instanceof DBCounts){
                DBCounts dbc = (DBCounts)obj;
                return dbCount == dbc.dbCount && cassandraCount == dbc.cassandraCount;
            }
            return false;
        }
    }
    
    
    public static final String DB_RECORDING_TABLE = "DVR_RECORDINGS";
    public static final String DB_STB_TABLE = "DVR_STB";
    public static final String CS_RECORDING_TABLE = "RECORDINGS";
    public static final String CS_STB_TABLE = "STB";
    
    
    private static Map<String,Object> getDBMacFilter(String deviceId){
        return new HashMap(){{put("MAC",FTConfig.getMac(deviceId));}};
    }
    
    private static Map<String,Object> getDBRecordingFilter(String deviceId, int recordingId){
        Map<String,Object> filter = getDBMacFilter(deviceId);
        filter.put("RECORDING_ID",recordingId);
        return filter;
    }
    
    private static Map<String,Object> getCSMacFilter(String deviceId){
        return new  HashMap(){{put("DEVICEID",deviceId);}};
    }
    
    private static Map<String,Object> getCSRecordingFilter(String deviceId, int recordingId){
        Map<String,Object> filter = getCSMacFilter(deviceId);
        filter.put("RECORDINGID",recordingId);
        return filter;
    }
    
    public static int getDBRecCount(String deviceId){
        List result = selectDB(DB_RECORDING_TABLE, Arrays.asList("count(*)"), getDBMacFilter(deviceId)); 
        return 0 == result.size() || null == result.get(0) ? 0 : ((Number)result.get(0)).intValue();
    }
    
    public static int getDBRecStatus(String deviceId, int recordingId){
        List result = selectDB(DB_RECORDING_TABLE, Arrays.asList("RECORDING_STATUS"), getDBRecordingFilter(deviceId, recordingId)); 
        return 0 == result.size() || null == result.get(0) ? 0 : ((Number)result.get(0)).intValue();
    }
    
    public static int getCassandraRecCount(String deviceId){
        List result = selectCassandra(CS_RECORDING_TABLE, Arrays.asList("count(*)"), getCSMacFilter(deviceId)); 
        return 0 == result.size() || null == result.get(0) ? 0 : ((Number)result.get(0)).intValue();
    }
    
    public static int getCassandraRecStatus(String deviceId, int recordingId){
        List result = selectCassandra(CS_RECORDING_TABLE, Arrays.asList("STATUS"), getCSRecordingFilter(deviceId, recordingId)); 
        return 0 == result.size() || null == result.get(0) ? 0 : ((Number)result.get(0)).intValue();
    }
    
    
    public static DBCounts getCounts(String deviceId){
//        Object count = DBHelper.selectSingle(String.format(SQL_COUNT_RECORDING, FTConfig.getMac(deviceId)));
//        int dbCount = ((Number)count).intValue();
//        
//        Session session = CassandraHelper.getSession();
//        ResultSet rs = session.execute(String.format(CQL_COUNT_RECORDING, deviceId));
//        int csCount = (int)rs.all().get(0).getLong(0);
//        
        return new DBCounts(getDBRecCount(deviceId), getCassandraRecCount(deviceId));
    }
    
    public static void addRecording(String deviceId, long recordingId){
        insertDB(DB_RECORDING_TABLE,
            Arrays.asList("MAC", "RECORDING_ID"), 
            Arrays.asList(FTConfig.getMac(deviceId), recordingId));
        
        insertCassandra(CS_RECORDING_TABLE, 
            Arrays.asList("DEVICEID", "RECORDINGID"), 
            Arrays.asList(deviceId, recordingId));
    }
    
    public static void addRecording(String deviceId, long recordingId, int type){
        addRecording(deviceId, recordingId, type, true);
    }
    
    public static void addRecording(String deviceId, long recordingId, int type, boolean insertStb){
        insertDB(DB_RECORDING_TABLE,
            Arrays.asList("MAC", "RECORDING_ID", "RECORDING_TYPE"), 
            Arrays.asList(FTConfig.getMac(deviceId), recordingId, type));
        
        insertCassandra(CS_RECORDING_TABLE, 
            Arrays.asList("DEVICEID", "RECORDINGID", "RECTYPE"), 
            Arrays.asList(deviceId, recordingId, type));
        
        if(insertStb){
            insertDB(DB_STB_TABLE,
                Arrays.asList("MAC","MAC_STR"),
                Arrays.asList(FTConfig.getMac(deviceId), deviceId));
            insertCassandra(CS_STB_TABLE,
                Arrays.asList("DEVICEID"),
                Arrays.asList(deviceId));
        }
    }
    
    public static void clearRecording(String deviceId){
        clearRecording(deviceId, true);
    }
            
    public static void clearRecording(String deviceId, boolean deleteSTB){
        deleteCassandra(CS_RECORDING_TABLE, getCSMacFilter(deviceId));
        deleteDB(DB_RECORDING_TABLE, getDBMacFilter(deviceId));
        if (deleteSTB){
            deleteDB(DB_STB_TABLE, getDBMacFilter(deviceId));
            deleteCassandra(CS_STB_TABLE, getCSMacFilter(deviceId));
        }
    }
    
    public static DVRParam getCassandraDVRParam(String deviceId, Long recordingId){
        if (null == recordingId)
            return null;
        List<String> result = new ArrayList<>();
        Session session = CassandraHelper.getSession();
        ResultSet rs = session.execute(String.format(CQL_SELECT_RECORDINGS, deviceId, recordingId));
        DVRParam param = null;
        
        List<Row> rows = rs.all();
        int i;
        if (null != rows && 1 == rows.size()){
            Row row = rows.get(0);
            param = new RecordingParam();
            param.setRecordingId((int) row.getLong(1));
            i = row.getInt(2);
            if (0 != i)
                param.setServiceId(i);
            i = row.getInt(3);
            if (0 != i)
                param.setChannelNumber(i);
            param.setRecordDuplicates(YesNo.fromCode(row.getInt(4)));
            param.setEpisodesDefinition(EpisodesDefinition.fromCode(row.getInt(5)));
            param.setSaveDays(SaveDays.fromCode(row.getInt(6)));
            param.setRecordingFormat(RecordingFormat.fromCode(row.getInt(7)));
            param.setType(Type.fromCode(row.getInt(8)));
            i = row.getInt(9); //startoffset
            if (0 != i)
                param.setStartOffset(i / 1000);
            i = row.getInt(10); //endoffset
            if (0 != i)
                param.setEndOffset(i / 1000);
            i = row.getInt(11); //repeat
            if (0 != i)
                param.setRepeat(i);
        }
        return param;
    }
         
    public static RecordingParam getCassandraRecordingParam(String deviceId, Long recordingId){
        DVRParam param = getCassandraDVRParam(deviceId, recordingId);
        return null == param ? null : new RecordingParam(param);
    }
    
    public static ModifyRequest getCassandraModifyRequest(String deviceId, Long recordingId){
        DVRParam param = getCassandraDVRParam(deviceId, recordingId);
        return null == param ? null : new ModifyRequest(param);
    }
    
    public static RecordingParam getDVRRecordingParam(long mac, Integer recordingId){
        DVRParam param = getDBDVRParam(mac, recordingId);
        return  null == param ? null : new RecordingParam(param);
    }
    
    public static ModifyRequest getDVRModifyRequest(long mac, Integer recordingId){
        DVRParam param = getDBDVRParam(mac, recordingId);
        return  null == param ? null : new ModifyRequest(param);
    }
    
    
    private static List<Integer> getPriorityVectorFromString(String str){
        List<Integer> priority =new ArrayList<>();
        if (null == str || "".equals(str.trim()))
            return priority;
        String [] tokens = str.split(",");
        for (String token: tokens){
            if (!"".equals(token.trim()))
                priority.add(Integer.valueOf(token.trim()));
        }
        return priority;
    }
    
    public static List<Integer> getCassandraPriorityVector(String deviceId){
        List params = FtDbHelper.selectCassandra("STB", 
            new ArrayList(){{
                add("PRIORITYVECTOR");
            }}, 
            new HashMap(){{
                put("DEVICEID", deviceId);
            }}
        );
        return (params.size() > 0) ? getPriorityVectorFromString((String)params.get(0)) : new ArrayList<>();
    }
    
    public static List<Integer> getDBPriorityVector(String deviceId){
        List params = FtDbHelper.selectDB("DVR_STB", 
            new ArrayList(){{
                add("PRIORITY_VECTOR");
            }}, 
            new HashMap(){{
                put("MAC", FTConfig.getMac(deviceId));
            }}
        );
        return (params.size() > 0) ? getPriorityVectorFromString(FtDbHelper.clobToString((Clob)params.get(0))) : new ArrayList<>();
    }
    
    public static void setDBPriorityVector(String deviceId, List<Integer> vector){
        StringBuilder sb = new StringBuilder();
        for (Integer id: vector){
            if (sb.length() > 0)
                sb.append(",");
            sb.append(id);
        }
        FtDbHelper.updateDB("DVR_STB", 
            new HashMap(){{
                put("PRIORITY_VECTOR",sb.toString());
            }}, 
            new HashMap(){{
                put("MAC", FTConfig.getMac(deviceId));
            }});
    }
    
    
    private static SpaceStatus getSpaceStatusFromList(List params){
        SpaceStatus space = new SpaceStatus();
        if (params.size() > 0){
            space.setHddSpaceExtFree(JSONUtils.getNumber(params.get(0)));
            space.setHddSpaceExtTotal(JSONUtils.getNumber(params.get(1)));
            space.setHddSpaceIntFree(JSONUtils.getNumber(params.get(2)));
            space.setHddSpaceIntTotal(JSONUtils.getNumber(params.get(3)));
            space.setHddSpaceFreeIn2Weeks(JSONUtils.getNumber(params.get(4)));
        }
        return space;
    }
    
    public static SpaceStatus getCassandraSpaceStatus(String deviceId){
        List params = FtDbHelper.selectCassandra("STB", 
            new ArrayList(){{
                add("HDDSPACEEXTFREE");
                add("HDDSPACEEXTTOTAL");
                add("HDDSPACEINTERNALFREE");
                add("HDDSPACEINTERNALTOTAL");
                add("HDDSPACEFREEIN2WEEKS");
            }}, 
            new HashMap(){{
                put("DEVICEID", deviceId);
            }}
        );
        return getSpaceStatusFromList(params);
    }
    
    public static SpaceStatus getDBSpaceStatus(String deviceId){
        List params = FtDbHelper.selectDB("DVR_STB", 
            new ArrayList(){{
                add("HDD_SPACE_EXT_FREE");
                add("HDD_SPACE_EXT_TOTAL");
                add("HDD_SPACE_INT_FREE");
                add("HDD_SPACE_INT_TOTAL");
                add("HDD_SPACE_IN_TWO_WEEKS");
            }}, 
            new HashMap(){{
                put("MAC", FTConfig.getMac(deviceId));
            }}
        );
        return getSpaceStatusFromList(params);
    }

    public static DVRParam getDBDVRParam(String deviceId, Integer recordingId){
        return getDBDVRParam(FTConfig.getMac(deviceId), recordingId);
    }
    
    public static DVRParam getDBDVRParam(long mac, Integer recordingId){
        if (null == recordingId)
            return null;
        List<Object[]> list = DBHelper.select(String.format(SQL_SELECT_RECORDING, mac, recordingId));
        DVRParam param = null;
        if (null != list && 1 == list.size()){
            param = new DVRParam();
            
            Object[] row = list.get(0);
            
            Number nm;
            String str;
            
            //RECORDING_ID
            nm = (Number)row[0]; 
            if (null != nm)
                param.setRecordingId(nm.intValue());
            
            //RECORDING_TYPE
            nm = (Number)row[1]; 
            if (null != nm)
                param.setType(nm.intValue());
            
            //SERVCIE_ID
            nm = (Number)row[2]; 
            if (null != nm)
                param.setServiceId(nm.intValue());
            
            //START_TIME
            nm = (Number)row[3]; 
            if (null != nm)
                param.setStartTime((int)(nm.longValue()/1_000));
            
            //DURATION
            nm = (Number)row[4];
            if (null != nm)
                param.setDuration((int)(nm.longValue()/1_000));
            
            //UNITED_FLAGS
            nm = (Number)row[5];
            if (null != nm)
                param.setUnitedFlags(nm.byteValue());
            
            //START_OFFSET
            nm = (Number)row[6]; 
            if (null != nm && 0 != nm.longValue())
                param.setStartOffset((int)(nm.longValue()/1_000));
            
            //END_OFFSET
            nm = (Number)row[7];
            if (null != nm && 0 != nm.longValue())
                param.setEndOffset((int)(nm.longValue()/1_000));
            
            //ACTUAL_START
            //row[8]
            
            //ACTUAL_DURATION
            //row[9]
            
            //UNITING_PARAM
            str = (String)row[10];
            if (null != str)
                param.setUnitingParameter(str);
            
            //BOOKMARK,
            //row[11]
            
            //RECORDING_STATUS
            //row[12]
            
            //STORAGE
            //row[13]
            
            //REPEAT_VAL
            nm = (Number)row[14];
            if (null != nm && 0 != nm.intValue())
                param.setRepeat(nm.intValue());
            
            //CHANNEL_NUMBER
            nm = (Number)row[15];
            if (null != nm && 0 != nm.intValue())
                param.setChannelNumber(nm.intValue());
            
            
            //BOOKMARK_TIME
            //row[16]
            
            //ADD_DATA_CRC
            //row[17]
            
            //RECORDING_CRC
            //row[18];
            
            //UPDATE_TIMESTAMP
            //row[19]
        }
        return param;
    }
}
