package com.zodiac.ams.charter.services.ft.rudp.session;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * Base class for RUDP Sessions
 */
public abstract class AbstractRUDP1Session implements IRUDPSession {
//    private final InetSocketAddress remoteAddress;
//    private final int sessionId;
    protected transient SessionKey sessionKey;
    // this is datasize including 4 bytes of CRC32
    private int dataSize;
    private int segmentSize;
    private int segmentCount;
    private volatile long lastPacketTimestamp;

    public AbstractRUDP1Session(InetSocketAddress remoteAddress, int sessionId, int dataSize, int segmentSize) {
//        this.remoteAddress = remoteAddress;
//        this.sessionId = sessionId;
        sessionKey = new SessionKey(remoteAddress, sessionId);
        this.dataSize = dataSize;
        this.segmentSize = segmentSize;
        this.lastPacketTimestamp = System.currentTimeMillis();
        {
            if (dataSize % segmentSize == 0) {
                this.segmentCount = dataSize / segmentSize;
            } else {
                this.segmentCount = 1 + dataSize / segmentSize;
            }
        }
    }

    protected AbstractRUDP1Session() {

    }

    @Override
    public void setSessionKey(SessionKey sessionKey) {
        this.sessionKey = sessionKey;
    }

    @Override
    public InetAddress getRemoteAddress() {
        return sessionKey.getRemoteAddress();
    }

    @Override
    public int getSessionId() {
        return sessionKey.getFileId();
    }

    /**
     * This is DATA size INCLUDING 4 bytes of CRC32
     * @return
     */
    @Override
    public int getDataSize() {
        return dataSize;
    }

    @Override
    public int getSegmentSize() {
        return segmentSize;
    }

    @Override
    public int getSegmentCount() {
        return segmentCount;
    }

    @Override
    public long getLastPacketTimestamp() {
        return lastPacketTimestamp;
    }

    protected void setLastPacketTimestamp(long lastPacketTimestamp) {
        this.lastPacketTimestamp = lastPacketTimestamp;
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeInt(dataSize);
        out.writeInt(segmentSize);
        out.writeInt(segmentCount);
        out.writeLong(lastPacketTimestamp);
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        dataSize = in.readInt();
        segmentSize = in.readInt();
        segmentCount = in.readInt();
        lastPacketTimestamp = in.readLong();
    }
}
