package com.zodiac.ams.charter.services.ft.rudp.session;

import com.zodiac.ams.charter.services.ft.rudp.packet.AckDataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckStartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacket;

import java.util.Map;
import java.util.Set;

public interface IRUDPSenderSession extends IRUDPSession {
    public void startAckReceived(boolean b);
    boolean startAckReceived();
    void dataAckReceived(AckDataPacket ackDataPacket);
    boolean ackReceived(int segment);
    void clearAckReceived(int segment);
    boolean allAcksReceived();
    boolean continueSendPackets();
    byte[] getDataBytes();
    boolean isLastSegment(int segmentNumber);
    Map<Integer, Integer> getSegmentsToBeAcknowledged();
    Map<Integer, Integer> getSegmentsToBeAcknowledged(int segment);
    int incrStartPacketRetries();
    int getStartPacketRetries();
    void incrDataPacketRetries(Set<Integer> segments);
    void incrDataPacketRetries(int segment);
    SessionKey getSessionKey();
    void setErrorPacket(ErrorPacket errorPacket);
    ErrorPacket getErrorPacket();
    void markStartPacketSendtime();
    long getLastStartPacketTimesent();
    void clearStartAckReceived();
}
