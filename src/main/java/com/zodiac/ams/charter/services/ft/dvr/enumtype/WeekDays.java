package com.zodiac.ams.charter.services.ft.dvr.enumtype;

import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomBoolean;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.util.ArrayList;
import java.util.List;

public enum WeekDays {
    Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday;
    
    public static WeekDays[] fromDescription(String description){
        description = description.toUpperCase();
        WeekDays [] values = WeekDays.values();
        List<WeekDays> result = new ArrayList<>();
        for (WeekDays value: values){
            if (description.contains(" " + value.name().toUpperCase()))
                result.add(value);
        }
        return result.toArray(new WeekDays[]{});
    }
    
    public static WeekDays[] getRandomWeekDays(){
        WeekDays [] values = WeekDays.values();
        List<WeekDays> result = new ArrayList<>();
        for (WeekDays value: values){
            if (getRandomBoolean())
                result.add(value);
        }
        if (0 == result.size()) //must be at least one
            result.add(values[getRandomInt(0, values.length-1)]);
        return result.toArray(new WeekDays[]{});
    }
    
    /**
     * 
     * @param mask Sun Mon .. Sat
     * @return 
     */
    public static WeekDays[] getWeekDays(int mask){
        WeekDays [] values = WeekDays.values();
        List<WeekDays> result = new ArrayList<>();
        for (int i=values.length - 1; i >= 0 ; i--){
            if ( 0 != (1 & (mask >> i)))
                result.add(values[values.length-1-i]);
        }
        return result.toArray(new WeekDays[]{});
    }
    
    public static String toString(WeekDays[] weekDays){
        StringBuilder sb =new StringBuilder();
        for (WeekDays weekDay: weekDays){
            if (0 != sb.length())
                sb.append(",");
            sb.append(weekDay);
        }
        return sb.toString();
    }
    
    /**
     * 
     * @param weekDays
     * @return Sat Fri .... Mon San 
     */
    public static int getWeekDaysMask(WeekDays[] weekDays){
        if (null ==  weekDays)
            return 0;
        int result = 0;
        for (WeekDays weekDay: weekDays)
            result = result | (1 << weekDay.ordinal());
        return result;
    }
    
    public static WeekDays[] fromMask(int mask){
        if (0 == mask)
            return null;
        WeekDays[] days = WeekDays.values();
        List<WeekDays> result = new ArrayList<>();
        for (int i=0; i<days.length; i++){
            if ( 0 != ( 1 & (mask >> i)))
                result.add(days[i]);
        }
        return result.toArray(new WeekDays[]{});
    }
}
