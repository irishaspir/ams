package com.zodiac.ams.charter.services.ft.reminder;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReminderResponseHelper {
    private String deviceId;
    private String status="OK";
    
    private List<Integer> codes=new ArrayList<>();
    
    private static final String ID_PROP="deviceId";
    private static final String STATUS_PROP="status";
    private static final String STATUS_CODE_PROP="statusCode";
    private static final String STATUS_PROCESSING_PROP="processingStatus";
    
    public ReminderResponseHelper(String deviceId,String status){
        this.deviceId = deviceId;
        this.status = status;
    }
    
    public ReminderResponseHelper(String deviceId){
        this.deviceId=deviceId;
    }
    
    public ReminderResponseHelper(String deviceId,int code){
        this.deviceId=deviceId;
        codes.add(code);
    }
    
    public void addCode(int code){
        codes.add(code);
    }
    
    public JSONObject toJSON(){
        JSONObject json=new JSONObject();
        json.put(ID_PROP,deviceId);
        json.put(STATUS_PROP,status);
        JSONArray jarray=new JSONArray();
        for (int code: codes)
            jarray.put(new JSONObject().put(STATUS_CODE_PROP,code));
        json.put(STATUS_PROCESSING_PROP, jarray);
        return json;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }

    
    public static boolean equals(JSONObject json1,JSONObject json2) {
        String str1=json1.getString(ID_PROP);
        String str2=json2.getString(ID_PROP);
        if (!str1.equals(str2))
            return false;
        
        str1=json1.getString(STATUS_PROP);
        str2=json2.getString(STATUS_PROP);
        if (!str1.equals(str2))
            return false;
     
        JSONArray arr1=json1.getJSONArray(STATUS_PROCESSING_PROP);
        JSONArray arr2=json2.getJSONArray(STATUS_PROCESSING_PROP);
        if (arr1.length() != arr2.length())
            return false;
        for (int i=0;i<arr1.length();i++){
            if (arr1.getJSONObject(i).getInt(STATUS_CODE_PROP) != arr2.getJSONObject(i).getInt(STATUS_CODE_PROP))
                return false;
        }
        return true;
    }
    
    public static boolean equals(JSONObject json1,String json2){
        return equals(json1,new JSONObject(json2));
    }
   
}
