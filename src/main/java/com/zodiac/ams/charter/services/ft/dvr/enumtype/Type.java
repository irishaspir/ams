package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum Type {
    Single(1), Series(2), Manual(3);
    
    private final int code;
    
    private Type(int code){
        this.code =  code;
    }
    
    public int getCode(){
        return code;
    }
    
    public static Type fromString(String string){
        Type[] types = Type.values();
        for (Type type: types){
            if (type.toString().equals(string))
                return type;
        }
        return null;
    }
    
    public static Type fromCode(int code){
        Type[] types = Type.values();
        for (Type type: types){
            if (code == type.code)
                return type;
        }
        return null;
    }
}
