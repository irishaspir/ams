package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.zip.CRC32;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import org.json.JSONArray;

public class ModifyResponse implements IDVRObject{
    public static final int NO_VALUE = -1;
    
    private Integer recordingId;
    private Error error = Error.Error0;
    private Error status = Error.Error0;
    private List<Integer> conflicts;
    private Integer conflictsSize;
    private Long crc;
    private SpaceStatus spaceStatus;
    private List<Integer> priorityVector;
    private Integer prioritySize;
    
    public ModifyResponse(){
    }
    
    public ModifyResponse(ModifyResponse response){
        this.recordingId = response.recordingId;
        this.error = response.error;
        this.status = response.status;
        if (null != response.conflicts){
            this.conflicts = new ArrayList<>();
            this.conflicts.addAll(response.conflicts);
        }
        this.crc = response.crc;
        if (null != response.spaceStatus)
            this.spaceStatus = new SpaceStatus(response.spaceStatus);
        if (null != response.priorityVector){
            this.priorityVector = new ArrayList<>();
            this.priorityVector.addAll(response.priorityVector);
        }
    }
    
    public static ModifyResponse fromErrorCode(Error error){
        ModifyResponse mr =new ModifyResponse();
        mr.error = error;
        return mr;
    }
    
    public static ModifyResponse getRandom(Integer recordingId){
        return getRandom(recordingId, Error.Error0, Error.Error0);
    }
    
    public static ModifyResponse getRandom(Integer recordingId, Error error, Error status){
        ModifyResponse mr = new ModifyResponse();
        mr.recordingId = recordingId;
        mr.error = error;
        mr.status = status;
        if (Error.Error4 == status || Error.SCHEDULE_CONFLICT == status || Error.SCHEDULE_CONFLICT_ERROR == status)
            mr.conflicts = getRandomRecordsVector(getRandomInt(1,5));
        CRC32 crc32 = new CRC32();
        crc32.update(String.valueOf(getRandomInt(1, 1_000_000)).getBytes());
        mr.crc = crc32.getValue();
        mr.spaceStatus = SpaceStatus.getRandom();
        mr.priorityVector = getRandomRecordsVector(getRandomInt(0, 5));
        return mr;
    }
    
    public static List<Integer> getRandomRecordsVector(int size){
        List<Integer> result = new ArrayList<>();
        for (int i=0; i<size; i++)
            result.add(getRandomInt(1, 1_000_000));
        return result;
    }
    
    public void setError(Error error){
        this.error = error;
    }
    
    public Error getError(){
        return error;
    }
    
    public void setStatus(Error status){
        this.status = status;
    }
    
    public Error getStatus(){
        return status;
    }
    
    
        
    public void setSpaceStatus(SpaceStatus spaceStatus){
        this.spaceStatus = spaceStatus;
    }
    
    public void setCrc(Long crc){
        this.crc = crc;
    }    
    
    public void setPriority(List<Integer> priorityVector){
        this.priorityVector = priorityVector;
    }
    
    public void setPrioritySize(Integer prioritySize){
        this.prioritySize = prioritySize;
    }
    
    public List<Integer> getPriority(){
        return priorityVector;
    }
    
    public void setConflicts(List<Integer> conflicts){
        this.conflicts = conflicts;
    }
    
    public void setConflictsSize(Integer conflictsSize){
        this.conflictsSize = conflictsSize;
    }
    
    public List<Integer> getConflicts(){
        return conflicts;
    }
    
    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(error.getCodeSTB()));
            if (null != status && NO_VALUE != status.getCodeSTB())
                out.write(DOB7bitUtils.encodeUInt(status.getCodeSTB()));
            if (null != conflicts || null != conflictsSize){
                if (null != conflictsSize){
                    if (NO_VALUE != conflictsSize)
                        out.write(DOB7bitUtils.encodeUInt(conflictsSize));
                }
                else
                    out.write(DOB7bitUtils.encodeUInt(conflicts.size()));
            }
            if (null != conflicts){
                for (Integer id: conflicts)
                    out.write(DOB7bitUtils.encodeUInt(id));
            }
            if (null != crc)
                out.write(DOB7bitUtils.encodeLong(crc));
            if (null != spaceStatus)
                out.write(spaceStatus.getData());
            if (null != priorityVector || null != prioritySize){
                if (null != prioritySize){
                    if (NO_VALUE != prioritySize) 
                        out.write(DOB7bitUtils.encodeUInt(prioritySize));
                }
                else
                    out.write(DOB7bitUtils.encodeUInt(priorityVector.size()));
            }
            if (null != priorityVector){
                for (Integer id: priorityVector)
                    out.write(DOB7bitUtils.encodeUInt(id));
            }
            return out.toByteArray();
        }
    }

    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        JSONObject o = new JSONObject();
        o.put("recordingId", recordingId);
        o.put("statusCode", Error.Error0 == error ? status.getCodeHE() : error.getCodeHE());
        if (null != conflicts && (Error.SCHEDULE_CONFLICT == status || Error.Error4 == status)){
            JSONArray confl = new JSONArray();
            for (Integer id: conflicts)
                confl.put(new JSONObject().put("recordingId", id));
            o.put("conflictRecordings", confl);
        }
        arr.put(o);
        json.put("result", arr);
        return json;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
