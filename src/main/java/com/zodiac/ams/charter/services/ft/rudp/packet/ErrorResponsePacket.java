package com.zodiac.ams.charter.services.ft.rudp.packet;

public interface ErrorResponsePacket extends ResponsePacket {
    ErrorCodeType getErrorCode();
}
