package com.zodiac.ams.charter.services.ft.rudp.session;

import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;

import java.util.Set;

public interface IRUDPReceiverSession extends IRUDPSession {
    boolean add(DataPacket dp);
    byte[] getBytesWoLast4();
    byte[] getLast4Bytes();
    boolean allSegmentsReceived();
    Set<Integer> getReceivedSegments();

    /**
     * @see IRUDPReceiverSession#tryMarkUpstreamSent()
     */
    boolean isUpstreamSent();
    /**
     * @see IRUDPReceiverSession#tryMarkUpstreamSent()
     */
    void markUpstreamSent();
    /**
     * Atomic check isUpstreamSent and markUpstreamSent
     * @return true if markUpstreamSent
     */
    boolean tryMarkUpstreamSent();
}
