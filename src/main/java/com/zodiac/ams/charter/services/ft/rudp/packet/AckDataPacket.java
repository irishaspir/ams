package com.zodiac.ams.charter.services.ft.rudp.packet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.BitSet;
import java.util.Set;
import java.util.TreeSet;

import com.dob.ams.util.DOB7bitUtils;

public class AckDataPacket extends AbstractAckResponsePacket {
    private Set<Integer> receivedSegments;

    public AckDataPacket() {
    }

    public AckDataPacket(int fileId, Set<Integer> receivedSegments) {
        super.setFileId(fileId);
        // IMPORTANT
        // make here a copy of set to avoid ConcurrentModificationException!!!
        // since receivedSegments goes from session, and session can change it if another segment will arrive
        // in method 'toBytes' we do iteration thru this set, there is the point when CME can occur!
        this.receivedSegments = new TreeSet<>(receivedSegments);
    }

    public Set<Integer> getReceivedSegments() {
        return receivedSegments;
    }

    public void setReceivedSegments(Set<Integer> receivedSegments) {
        this.receivedSegments = receivedSegments;
    }

    @Override
    public String toString() {
        return "AckDataPacket{" +
                "fileId=" + getFileId() +
                ", receivedSegments.size=" + receivedSegments.size() +
                '}';
    }

    @Override
    public byte[] toBytes() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);

            baos.write(PacketType.ACK.packetType);
            baos.write(PacketType.DATA.packetType);
            baos.write(DOB7bitUtils.encodeUInt(getFileId()));

            BitSet bitmask = set2BitMask(receivedSegments);
            byte[] buf = new byte[bitmask.length()/7 + (bitmask.length()%7 == 0?0:1)];
            for (int i = 0; i < bitmask.length(); i++) {
                buf[i / 7] |= ((byte) (bitmask.get(i) ? 0x1 : 0x0)) << i % 7;
                buf[i / 7] |= 0x80;
            }
            buf[buf.length - 1] &= 0x7f;
            baos.write(buf);

            return baos.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Should not happen", e);
        }
    }

// Before Incorrect encode/decode of segments bitmask in ACK-DATA packets  [ AMSRUDP-9  # 117876 ]     
//    @Override
//    public byte[] toBytes() {
//        try {
//            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
//
//            baos.write(PacketType.ACK.packetType);
//            baos.write(PacketType.DATA.packetType);
//            baos.write(DOB7bitUtils.encodeUInt(getFileId()));
//
//            long masket = convert(set2BitMask(receivedSegments));
//            byte buf[] = DOB7bitUtils.encodeULong(masket);
//            
//            baos.write(buf);
//
//            return baos.toByteArray();
//        } catch (IOException e) {
//            throw new IllegalStateException("Should not happen", e);
//        }
//    }

    public static BitSet set2BitMask(Set<Integer> set) {
        BitSet bs = new BitSet(1024);
        for (int i : set) {
            bs.set(i);
        }
        //System.out.println("bit mask: " + bs.toString());
        return bs;
    }

    public static long convert(BitSet bits) {
        long value = 0L;
        for (int i = 0; i < bits.length(); ++i) {
            value += bits.get(i) ? (1L << i) : 0L;
        }
        return value;
    }

}
