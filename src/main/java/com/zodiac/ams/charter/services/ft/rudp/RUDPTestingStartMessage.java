package com.zodiac.ams.charter.services.ft.rudp;

public class RUDPTestingStartMessage extends RUDPTestingMessage{
    
    final boolean startOnly;
    
    public RUDPTestingStartMessage(int sessionId,boolean startOnly) {
        super(sessionId);
        this.startOnly = startOnly;
    }
    
}
