package com.zodiac.ams.charter.services.ft.defent;

public class GroupFactory {
    public static IGroup getGroup(String mac, EntError[] errors){
        switch(errors[0].getGroup()){
            case 0:
                return new Group0(mac, errors);
            case 1:
                return new Group1(mac, errors);
            case 2:
                return new Group2(mac, errors);
            case 3:
                return new Group3(mac, errors);
            default:
                return null;
        }
    }
}
