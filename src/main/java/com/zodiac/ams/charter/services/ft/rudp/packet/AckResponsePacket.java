package com.zodiac.ams.charter.services.ft.rudp.packet;

public interface AckResponsePacket extends ResponsePacket {
    int getFileId();
    void setFileId(int fileId);
}
