package com.zodiac.ams.charter.services.dsg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public class STBParametersUpdateOptionCollection {

    private ArrayList<STBParametersUpdateOption> list = new ArrayList<>();

    public STBParametersUpdateOptionCollection addOption(STBParametersUpdateOption option) {
        list.add(new STBParametersUpdateOption(option.getName(), option.getColumnName(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public STBParametersUpdateOptionCollection addOptions(STBParametersUpdateOptions option) {
        list.add(new STBParametersUpdateOption(option.getOption().getName(), option.getOption().getColumnName(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<STBParametersUpdateOption> build() {
        return list;
    }

    public STBParametersUpdateOptionCollection addListOptions(List<STBParametersUpdateOption> list) {
        this.list.addAll(list);
        return this;
    }

}
