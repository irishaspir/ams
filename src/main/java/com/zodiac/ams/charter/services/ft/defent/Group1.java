package com.zodiac.ams.charter.services.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomChannelType;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomReason;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomScreenId;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

public class Group1 extends GroupImpl{
    private static final int INVALID_SCREEN_ID = 7777;
    int lineupId;
    int timeStampBase;
    
    int [] timeStampDelta;
    int [] channelNumber; //453 tmsServiceID
    ChannelType [] channelType;
    Reason [] reason;
    ScreenId [] applicationScreenID;	 
    
    
    public Group1(String mac, EntError error, ScreenId applicationScreenID) {
        this(mac, new EntError[]{error}, new ScreenId [] {applicationScreenID});
    }
    
    public Group1(String mac, EntError error) {
        this(mac, new EntError[]{error}, null);
    }
    
    public Group1(String mac, EntError[] errors) {
        this(mac, errors, null);
    }
            
    public Group1(String mac, EntError[] errors, ScreenId [] applicationScreenID) {
        super(mac, errors);
        lineupId = getRandomInt();
        timeStampBase = getNow();
        
        timeStampDelta = new int[errors.length];
        channelNumber = new int[errors.length];
        channelType = new ChannelType[errors.length];
        reason = new Reason[errors.length];
        this.applicationScreenID = (null ==  applicationScreenID) ? new ScreenId[errors.length] : applicationScreenID;
        
        for (int i=0; i< errors.length; i++){
            timeStampDelta[i] = getRandomInt(); 
            channelNumber[i] = getRandomInt();
            channelType[i] = getRandomChannelType();
            reason[i] = getRandomReason();
            if (null == applicationScreenID)
                this.applicationScreenID[i] = getRandomScreenId();
        }
    }
    
    @Override
    public byte[] getMessage() throws Exception{
        byte [] result = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(DOB7bitUtils.encodeUInt(requestId)); 
            baos.write(getGroupDescription(1,errors.length)); //group,count
            //453
            //baos.write(DOB7bitUtils.encodeUInt(lineupId)); 
            baos.write(DOB7bitUtils.encodeUInt(timeStampBase));
            for (int i=0; i<errors.length; i++){
                baos.write(DOB7bitUtils.encodeUInt(errors[i].getCode())); 
                baos.write(DOB7bitUtils.encodeUInt(timeStampDelta[i])); 
                baos.write(DOB7bitUtils.encodeUInt(channelNumber[i])); 
                //453
                //baos.write(DOB7bitUtils.encodeUInt(channelType[i].ordinal()));  
                baos.write(DOB7bitUtils.encodeUInt(reason[i].getCode()));
                if (null == applicationScreenID[i])
                    baos.write(DOB7bitUtils.encodeUInt(INVALID_SCREEN_ID));
                else
                    baos.write(DOB7bitUtils.encodeUInt(applicationScreenID[i].getCode()));
            }
            baos.flush();
            result = baos.toByteArray();
        }
        return result;
    }
    
    @Override
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        //453
        //obj.put("macAddress", mac);
        JSONArray array = new JSONArray();
        for (int i=0; i<errors.length; i++){
            JSONObject o =new JSONObject();
            //453
            o.put("id", mac);
            o.put("errorCode", errors[i].getError());
            o.put("errorDescription", errors[i].getDescription());
            //453
            //o.put("timestamp", String.valueOf(timeStampBase + timeStampDelta[i]));
            //o.put("lineupId", String.valueOf(lineupId));
            //o.put("channelNumber", String.valueOf(channelNumber[i]));
            //o.put("channelType", channelType[i].toString());
            //o.put("reason", reason[i].toReasonString());
            //o.put("applicationScreenID",(null == applicationScreenID[i]) ? "" : applicationScreenID[i].toScreenIdString());
            //453
            o.put("additionalInfo",String.format("timestamp=%d, tmsServiceID=%d, reason=%s, applicationScreenID=%s", 
                timeStampBase + timeStampDelta[i], channelNumber[i], reason[i].toReasonString(),
                (null == applicationScreenID[i]) ? "" : applicationScreenID[i].toScreenIdString()));
            array.put(o);
        }
        //453
        //obj.put("errors", array);
        //453
        obj.put("stbError", array);
        return obj;
    }
    
}
