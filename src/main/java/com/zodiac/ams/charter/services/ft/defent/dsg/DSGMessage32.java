package com.zodiac.ams.charter.services.ft.defent.dsg;

import com.dob.ams.util.DOB7bitCoder;
import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import static com.zodiac.ams.charter.services.ft.defent.dsg.DSGUtils.string2ip;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;
import static com.zodiac.ams.charter.services.ft.defent.dsg.Constants.*;

public class DSGMessage32 extends DSGMessage{
    
    private static final String [] KEYS_DAC = new String[] {VCTID, SGID, AMSIP, SDVSGID, CMIP, CONNECTIONMODE};
    private static final String [] KEYS_DNCS = new String[] {HUBID, SGID, AMSIP, SDVSGID, CMIP, CONNECTIONMODE};
    
    private Environment env;
    private String [] keys;
    private String disabledKey;
    
    public static DSGMessage32 getNULL(String mac){
        return new DSGMessage32(mac, -1, -1, null, -1, null, null);
    }
    
    public static DSGMessage32 getRandom(String mac){
        ConnectionMode connectionMode = RandomUtils.getRandomObject(ConnectionMode.values());
        int hubId = RandomUtils.getRandomInt(1, 100);
        int serviceGroupId = RandomUtils.getRandomInt(1, 100);
        String loadBalancerIpAddress = RandomUtils.getRandomIp();
        int sdvServiceGroupId = RandomUtils.getRandomInt(1, 100);
        String cmIp = RandomUtils.getRandomIp();
        return new DSGMessage32(mac, hubId, serviceGroupId, loadBalancerIpAddress, sdvServiceGroupId, cmIp, connectionMode);
    }
    
    private DSGMessage32(String mac, int hubId, int serviceGroupId, String loadBalancerIpAddress, 
        int sdvServiceGroupId, String cmIp, ConnectionMode connectionMode){
        super((byte)2, mac);
        this.env = Environment.DAC;
        this.hubId = hubId;
        this.serviceGroupId = serviceGroupId;
        this.loadBalancerIpAddress = loadBalancerIpAddress;
        this.sdvServiceGroupId = sdvServiceGroupId;
        this.cmIp = cmIp;
        this.connectionMode = connectionMode;
        
        this.keys = (Environment.DNCS == env)? KEYS_DNCS : KEYS_DAC;
    }
    
    private int getContentMask(){
        Object[] values = getValues();
        int mask =0;
        for (int i=0; i < values.length; i++){
            if (!isEmpty(values[i]))
                mask = mask | (1 << i);
        }
        return mask;
    }
    
    public void setDisabledKey(String disabledKey){
        this.disabledKey = disabledKey;
    }
    
    private Object[] getValues(){
        return new Object[] {hubId, serviceGroupId, loadBalancerIpAddress, sdvServiceGroupId, cmIp, connectionMode};
    }
    
    @Override
    public byte[] getMessage() throws Exception{
        byte [] result = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(requestId);
            baos.write(DOB7bitUtils.encodeUInt(getContentMask()));
            Object [] values = getValues();
            for (int index=0; index< keys.length; index++){
                Object value = values[index]; 
                String key = keys[index];
                if (isEmpty(value))
                    continue;
                if (key.equals(disabledKey))
                    continue;
                switch (key){
                    case CONNECTIONMODE:
                        {
                            ConnectionMode mode = (ConnectionMode)value;
                            baos.write(DOB7bitUtils.encodeUInt(mode.ordinal()));
                        }
                        break;
                    case AMSIP:
                    case CMIP:
                         baos.write(DOB7bitCoder.encodeBigInt(string2ip((String)value)));
                        break;
                    default:
                        baos.write(DOB7bitUtils.encodeInt((Integer)value));
                        break;
                }
                
            }
            baos.flush();
            result = baos.toByteArray();
        }
        return result;
    }
    
    private boolean isEmpty(Object obj){
        if (null == obj)
            return true;
        if (obj instanceof Integer){
            if (-1 == ((Integer)obj).intValue())
                return true;
        }
        return false;
    }
    
    @Override
    public JSONObject toJSON(){
        JSONObject o;
        
        JSONArray arr = new JSONArray();
        
        o = new JSONObject();
        o.put("name", MAC);
        o.put("value", mac);
        arr.put(o);
        
        Object[] values = getValues();
        for (int index=0; index<values.length; index++ ){
            Object value = values[index];
            String key = keys[index];
            if (isEmpty(value))
                continue;
            JSONObject jo = new JSONObject();
            jo.put("name", key);
            jo.put("value", value.toString());
            arr.put(jo);
            
            if (SGID.equals(key)){
                JSONObject j2 = new JSONObject();
                j2.put("name", TSID);
                j2.put("value", value.toString());
                arr.put(j2);
            }
        }
        
       
       
        JSONObject o1 = new JSONObject();
        o1.put("options", arr);
        
        JSONObject o2 = new JSONObject();
        o2.put("attributes", o1);
        
        JSONObject o3 = new JSONObject();
        o3.put("settings", o2);
        
        return o3;
    }
}
