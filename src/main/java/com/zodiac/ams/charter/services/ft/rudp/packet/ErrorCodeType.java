package com.zodiac.ams.charter.services.ft.rudp.packet;

public enum ErrorCodeType {
    NACK(0x1),
    CRC32(0x2),
    RESET(0x3),
    MEMORY(0x4),
    FILEID(0x5);

    public final byte errorCodeType;

    ErrorCodeType(int errorCodeType) {
        this.errorCodeType = (byte) errorCodeType;
    }

    public static ErrorCodeType fromByte(byte b) {
        int t = 0xFF & b;
        return fromInt(t);
    }

    public static ErrorCodeType fromInt(int b) {
        for (ErrorCodeType p : ErrorCodeType.values()) {
            if (p.errorCodeType == b) {
                return p;
            }
        }
        return null;
    }
}
