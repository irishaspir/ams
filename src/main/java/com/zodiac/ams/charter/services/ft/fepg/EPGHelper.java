package com.zodiac.ams.charter.services.ft.fepg;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EPGHelper {
    protected final static Logger logger = LoggerFactory.getLogger(EPGHelper.class);
    
    public static File createEmptyEPGFile(String source){
        try{
            Path pt=new File(source).toPath();
            Files.deleteIfExists(pt);
            return Files.createFile(pt).toFile();
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
    
    public static File createEPGFile(String source,String target,int period,int limit){
        Date today=new Date();
        Calendar cl=Calendar.getInstance();
        SimpleDateFormat sdf=new SimpleDateFormat("|\"yyyy-MM-dd\"|");
        Random random=new Random();
        int counter=0;
        
        File result=null;
        try ( BufferedReader in= new BufferedReader(new FileReader(source));
            PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(target)),true); ) {
            String line;
            while (null != (line=in.readLine())){
                int day=random.nextInt(period)-period/2;
                cl.setTime(today);
                cl.add(Calendar.DATE, day);
                String transformed=line.replaceFirst("\\|\"\\d{4}-\\d{2}-\\d{2}\"\\|",sdf.format(cl.getTime()));
                out.println(transformed);
                if (limit == ++counter)
                    break;
            }
            result=new File(target);
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
        return result;
    }
}