package com.zodiac.ams.charter.services.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPReceiverSession;


public class AckHandler implements IAckHandler{

    @Override
    public int handleDataAck(IRUDPReceiverSession session, DataPacket packet) {
        return RECEIVE_DATA | SEND_DATA_ACK;
    }

    @Override
    public boolean handleStartAck(StartPacket packet) {
        return true;
    }
    
}
