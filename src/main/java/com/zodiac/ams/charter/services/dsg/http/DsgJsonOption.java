package com.zodiac.ams.charter.services.dsg.http;

public class DsgJsonOption {

    private String nameJSON;
    private String valueJSON;

    public DsgJsonOption(String nameJSON, String valueJSON) {
        this.nameJSON = nameJSON;
        this.valueJSON = valueJSON;
    }

    public String getValueJSON() {
        return valueJSON;
    }

    public String getNameJSON() {
        return nameJSON;
    }

}
