package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.sandt.util.ReportingException;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import java.nio.file.Files;
import com.sandt.bx.data.EightBitLengthString;
import com.sandt.bx.data.EtvApplicationControlCode;
import com.sandt.bx.data.EtvApplicationVersionComponent;
import com.sandt.bx.data.TSApplicationId;
import com.sandt.bx.data.TSApplicationPriority;
import com.sandt.bx.data.TSBitRate;
import com.sandt.bx.data.TSModuleRelativePriority;
import com.sandt.bx.data.TSModuleRepeatPeriod;
import com.sandt.bx.data.TSName;
import com.sandt.bx.data.TSOrganisationId;
import com.sandt.bx.etv.api.EtvApplicationsAPI;
import com.sandt.bx.etv.client.EtvApplicationsClient;
import com.sandt.bx.etv.model.application.SummaryModel;
import com.sandt.bx.etv.model.application.EtvApplicationContentModel;
import com.sandt.bx.etv.model.application.EtvApplicationDescriptionModel;
import com.sandt.bx.etv.model.application.EtvApplicationIdentifierModel;
import com.sandt.bx.etv.model.application.EtvApplicationInformationModel;
import com.sandt.bx.etv.model.application.EtvApplicationSignallingModel;
import com.sandt.util.MessageCode;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class CarouselST_ETV extends CarouselST{

    private EtvApplicationsAPI etvApplicationAPI;
    
    private static final String ORGANIZATION_ID = "1";
    private static final String APPLICATION_ID = "2";
    private static final int MAJOR = 3;
    private static final int MINOR = 4;
    private static final EtvApplicationControlCode AUTOSTART = EtvApplicationControlCode.AUTOSTART;
    private static final int PRIORITY = 1;
    private static final String VERSION = "3.4";
    private static final String OWNER = "zodiac";
    private static final double BIT_RATE =1_000_000.0;
    private static final String AUTHORITY = "zodiac.tv";

    public CarouselST_ETV(CarouselHost host,boolean isZipped) {
        super(host,isZipped);                
        etvApplicationAPI = new EtvApplicationsClient(transportManager);
    }
    
    @Override
    public String getCarouselFile(String remoteName) {
        try {
            byte [] bytes = etvApplicationAPI.retrieveFileContent(new TSName(application), remoteName);
            Files.write(new File(getRemoteTmpName(remoteName)).toPath(),bytes);
            return new String(bytes);
        }
        catch (ReportingException ex){
            logger.error("getCarouselFile error: {}  {} application: {} file: {}", ex.getDetailMessage() ,ex.getMessage(), application, remoteName);
            return null;
        }
        catch (Exception ex){
            logger.error("getCarouselFile error: {} application: {} file: {}", ex.getMessage(), application, remoteName);
            return null;
            
        }
    }
    
    @Override
    public String getZippedCarouselFile(String remoteName,String entryName){
        try {
            byte [] bytes = etvApplicationAPI.retrieveFileContent(new TSName(application), remoteName);
            return getZippedCarouselFile(bytes,remoteName,entryName);
        }
        catch (ReportingException ex){
            logger.error("getZippedCarouselFile error: {}  {} application: {} file: {}", ex.getDetailMessage() ,ex.getMessage(), application, remoteName);
            return null;
        }
        catch (Exception ex){
            logger.error("getZippedCarouselFile error: {} application: {} file: {}", ex.getMessage(), application, remoteName);
            return null;        
        }
    }
    
    //TO DO: ??????
    //Detail: Function not defined Message: Operation failed
    private boolean createApplication(){
        try {
            EtvApplicationIdentifierModel aApplicationIdentifier = new EtvApplicationIdentifierModel(
                new TSOrganisationId(ORGANIZATION_ID),new TSApplicationId(APPLICATION_ID)); 
        
            EtvApplicationVersionComponent aVersionMajor = new EtvApplicationVersionComponent(MAJOR);
            EtvApplicationVersionComponent aVersionMinor = new EtvApplicationVersionComponent(MINOR);
            TSApplicationPriority aPriority = new TSApplicationPriority(PRIORITY);
            EightBitLengthString aInitialAbsPath = new EightBitLengthString("und"); ; //new EightBitLengthString("z_dts.ini");
            EightBitLengthString aArgument = null;
            EtvApplicationInformationModel aApplicationInformation = new EtvApplicationInformationModel(
                AUTOSTART, aVersionMajor, aVersionMinor, aPriority, aInitialAbsPath, aArgument);
        
            EtvApplicationSignallingModel aSignalling = 
                new EtvApplicationSignallingModel(aApplicationIdentifier, aApplicationInformation);
        
            SummaryModel aSummary = new SummaryModel(new TSName(application), VERSION, null); //OWNER) ;
        
            EtvApplicationContentModel aContent = new EtvApplicationContentModel(
                new TSBitRate(BIT_RATE),new EightBitLengthString (AUTHORITY)); 
         
            EtvApplicationDescriptionModel description =  new EtvApplicationDescriptionModel(aSummary, aSignalling, aContent);
        
            etvApplicationAPI.createApplicationDescription(description);
            return true;
        }
        catch(ReportingException ex){
            logger.error("createApplication error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        }
    }
    
    @Override
    public boolean createRemoteCarouselDir() {
        String template = application+"_template"+(host.isRootNode() ? "_root" : "")+(isZipped ? "_zip":"");
        try{
            deleteApplication();
            //createApplication();
            etvApplicationAPI.copyApplication(new TSName(template), new TSName(application), true);
            //etvApplicationAPI.importApplication(new File(getName(application)), true);
            return true;
        }
        catch(ReportingException ex){
            logger.error("createRemoteCarouselDir error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        }
    }
    
    @Override
    public boolean deleteApplication(){
        try {
            etvApplicationAPI.deleteApplication(new TSName(application));        
            return true;
        }
        catch(ReportingException ex){
            logger.error("deleteApplication error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        }  
    }
    
    @Override
    public boolean exportApplication(){
        try (BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(application));){
            etvApplicationAPI.exportApplication(new TSName(application), out);
            return true;
        }
        catch(ReportingException ex){
            logger.error("exportApplication error: {} {} application: {}", ex.getDetailMessage(), ex.getMessage(), application);
            return false;
        }
        catch(Exception ex){
            logger.error("exportApplication error: {} application: {}", ex.getMessage(), application);
            return false;
        }
    }
    
    @Override
    public boolean deleteCarouselFile(String remoteName) {
        try {
            etvApplicationAPI.removeFile(new TSName(application),remoteName);        
            return true;
        }
        catch(ReportingException ex){
            logger.error("deleteCarouselFile error: {} {} application: {} file: {}", ex.getDetailMessage(), ex.getMessage(), application, remoteName);
            return false;
        }
    }
    
     
    
    //TO DO: ??????
    //Detail: Function not defined Message: Operation failed
    @Override
    public boolean putFileOnCarousel(String local, String remoteName) {
        try {
            deleteCarouselFile(remoteName);
            TSModuleRelativePriority priority = new TSModuleRelativePriority(1);
            byte [] bytes = Files.readAllBytes(new File(local).toPath());
            etvApplicationAPI.addFile(new TSName(application), remoteName, (TSModuleRepeatPeriod)null, priority, bytes);
            return true;
        }
        catch(ReportingException ex){
            logger.error("putFileOnCarousel error: {} {} application: {}  file: {}", ex.getDetailMessage(), ex.getMessage(), application, remoteName);
            return false;
        }
        catch(Exception ex){
            logger.error("putFileOnCarousel error: {} application: {}  file: {}", ex.getMessage(), application, remoteName);
            return false;
        }
    }

    
}