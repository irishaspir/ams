package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class RequestRecording implements IDVRObject{
    private static final int MAX_PARAMS = 5;
    
    private String deviceId;
    private Method method;
    List<RecordingParam> params;
    
    public RequestRecording(RequestRecording request){
        deviceId = request.deviceId;
        method = request.method;
        if (null != request.params){
            params = new ArrayList<>();
            for (RecordingParam param: request.params)
                params.add(new RecordingParam(param));
        }
    }
    
    public RequestRecording(String deviceId, Method method){
        this.deviceId = deviceId;
        this.method = method;
    }
    
    private RequestRecording(String deviceId, Method method, Class clazz){
        this(deviceId, method, clazz, getRandomInt(1, MAX_PARAMS));
    }
    
    private RequestRecording(String deviceId, Method method, Class clazz, int size){
        this(deviceId, method);
        addRandomParams(clazz, size);
    }
    
    public static RequestRecording getRandom(String deviceId, Method method){
        return new RequestRecording(deviceId, method, RecordingParam.class);
    }
    
    public void setDeviceId(String deviceId){
        this.deviceId = deviceId;
    }
    
    public void addParam(RecordingParam param){
        if (null == params)
            params = new ArrayList<>();
        params.add(param);
    }
    
    public RecordingParam getParam(int index){
        return params.get(index);
    }
    
    public void addRandomParams(Class clazz, int size){
        for (int i=0; i<size; i++){
            if (clazz.equals(RecordingParam.class))
                addParam(RecordingParam.getRandom());
        }
    }
    
    public int getSize(){
        return null == params ? 0 : params.size();
    }
    
    public boolean[] getRepeat(){
        List<Boolean> bools = new ArrayList<>();
        if (null != params){
            for (RecordingParam param: params)
                bools.add(param.isRepeat());
        }
        boolean[] result = new boolean[bools.size()];
        int index = 0;
        for (Boolean bool: bools)
            result[index++] = bool;
        return result;    
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded){
        JSONObject obj = new JSONObject();
        if (null != deviceId)
            obj.put("deviceId", deviceId);
        if (null != method)
            obj.put("method", method.toString());
        if (null != params){
            JSONArray arr = new JSONArray();
            for (IDVRObject param: params)
                arr.put(param.toJSON(excluded));
            obj.put("params", arr);
        }
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
    @Override
    public byte[] getData() throws IOException{
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write((byte)method.getCode());
            out.write(DOB7bitUtils.encodeUInt(params.size()));
            for (IDVRObject param: params)
                out.write(param.getData());
            return out.toByteArray();
        }
    }
}
