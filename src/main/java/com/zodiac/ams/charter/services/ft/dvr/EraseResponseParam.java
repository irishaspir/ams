package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.IOException;
import java.util.Set;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import java.io.ByteArrayOutputStream;

public class EraseResponseParam implements IDVRObject {
    
    private Object recordingId;
    private Object statusCode;
    
    public EraseResponseParam(){
    }
    
    public EraseResponseParam(EraseResponseParam param){
        if (null != param){
            this.recordingId = param.recordingId;
            this.statusCode = param.statusCode;
        }
    }
    
    public EraseResponseParam(Object recordingId, Object statusCode){
        this.recordingId = recordingId;
        this.statusCode = statusCode;
    }
    
    public void setStatusCode(Object statusCode){
        this.statusCode = statusCode;
    }
    
    public int getStatusCode(){
        return (statusCode instanceof Error) ? ((Error)statusCode).getCodeSTB() : 0;
    }

    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONUtils.setField(json, "recordingId",  recordingId);
        if (statusCode instanceof Error)
            json.put("statusCode", ((Error)statusCode).getCodeHE());
        else
            JSONUtils.setField(json, "statusCode", statusCode);
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            if (null != statusCode){
                if (statusCode instanceof Error)
                    out.write(DOB7bitUtils.encodeUInt(((Error)statusCode).getCodeSTB()));
                else
                    out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(statusCode)));
            }
            return out.toByteArray();
        }
    }
}
