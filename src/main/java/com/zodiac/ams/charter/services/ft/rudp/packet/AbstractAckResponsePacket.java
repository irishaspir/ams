package com.zodiac.ams.charter.services.ft.rudp.packet;

import java.net.InetSocketAddress;

public abstract class AbstractAckResponsePacket implements AckResponsePacket {

    private int fileId;
    private InetSocketAddress remoteAddress;

    @Override
    public int getFileId() {
        return fileId;
    }

    @Override
    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    @Override
    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
}
