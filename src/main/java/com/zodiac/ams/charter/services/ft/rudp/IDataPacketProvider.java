package com.zodiac.ams.charter.services.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;

public interface IDataPacketProvider {
    byte[] get(int sessionId,int segmentNumber,DataPacket dataPacket);
}
