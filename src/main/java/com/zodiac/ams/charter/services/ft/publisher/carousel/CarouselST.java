package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.sandt.bx.common.client.UsersClient;
import com.sandt.bx.data.TSName;
import com.sandt.util.ReportingException;
import com.sandt.util.http.client.TransportManager;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getLocalZippedFile;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getRemoteTmpName;
import java.io.File;
import java.nio.file.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CarouselST extends CarouselImpl {
    
    protected UsersClient usersClient;
    protected TransportManager transportManager;
    protected String application;
    
    protected static final Logger logger = LoggerFactory.getLogger(CarouselST.class);
    
    
    public CarouselST(CarouselHost host,boolean isZipped){
        super(host.getServerType(), isZipped);
        this.host = host;
        application = host.getLocation();           
        String url ="http://"+this.host.getHost()+"/"+UsersClient.SERVLET_NAME;
        transportManager = new TransportManager(url);
        usersClient = new UsersClient(transportManager);
        try {
            usersClient.login(new TSName(this.host.getUser()), this.host.getPwd());
        }
        catch(ReportingException ex){
            logger.error("CarouselST error: {} {}",ex.getDetailMessage(),ex.getMessage());
        }
    }
    
    @Override
    public String getLocation(String name){
        return "oob://Prefetch?cid="+getUid()+"&mn="+name;
    }
    
    public abstract boolean deleteApplication();
    public abstract boolean exportApplication();
}
