package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.json.JSONObject;

public class SpaceStatus implements IDVRSerializable{
    private Integer hddSpaceIntTotal;
    private Integer hddSpaceIntFree;
    private Integer hddSpaceExtTotal;
    private Integer hddSpaceExtFree;
    private Integer hddSpaceFreeIn2Weeks;
    
    public SpaceStatus(){
    }
    
    public SpaceStatus(SpaceStatus status){
        if (null != status){
            this.hddSpaceIntTotal = status.hddSpaceIntTotal;
            this.hddSpaceIntFree =  status.hddSpaceIntFree;
            this.hddSpaceExtTotal = status.hddSpaceExtTotal;
            this.hddSpaceExtFree = status.hddSpaceExtFree;
            this.hddSpaceFreeIn2Weeks = status.hddSpaceFreeIn2Weeks;
        }
    }
    
    public SpaceStatus(Integer hddSpaceIntTotal, Integer hddSpaceIntFree, Integer hddSpaceExtTotal, Integer hddSpaceExtFree, Integer hddSpaceFreeIn2Weeks){
        this.hddSpaceIntTotal = hddSpaceIntTotal;
        this.hddSpaceIntFree = hddSpaceIntFree;
        this.hddSpaceExtTotal = hddSpaceExtTotal;
        this.hddSpaceExtFree = hddSpaceExtFree;
        this.hddSpaceFreeIn2Weeks = hddSpaceFreeIn2Weeks;
    }
    
    public static SpaceStatus get0(){
        return new SpaceStatus(0, 0, 0, 0, 0);
    }
    
    public static SpaceStatus getRandom(){
        return new SpaceStatus(getRandomInt(1, 1_000),
            getRandomInt(1,100), 
            getRandomInt(1, 1_000), 
            getRandomInt(1, 100),
            getRandomInt(1, 100));
    }
    
    public void setHddSpaceIntTotal(int hddSpaceIntTotal){
        this.hddSpaceIntTotal = hddSpaceIntTotal;
    }
    
    public void setHddSpaceIntFree(int hddSpaceIntFree){
        this.hddSpaceIntFree = hddSpaceIntFree;
    }
    public void setHddSpaceExtTotal(int hddSpaceExtTotal){
        this.hddSpaceExtTotal = hddSpaceExtTotal;
    }
    
    public void setHddSpaceExtFree(int hddSpaceExtFree){
        this.hddSpaceExtFree = hddSpaceExtFree;
    }
    
    public void setHddSpaceFreeIn2Weeks(int hddSpaceFreeIn2Weeks){
        this.hddSpaceFreeIn2Weeks = hddSpaceFreeIn2Weeks;
    }
    
    public JSONObject toJSON(){
        JSONObject json = new JSONObject();
        json.put("HDD_SPACE_EXT_FREE", hddSpaceExtFree);
        json.put("HDD_SPACE_EXT_TOTAL", hddSpaceExtTotal);
        json.put("HDD_SPACE_INT_FREE", hddSpaceIntFree);
        json.put("HDD_SPACE_INT_TOTAL", hddSpaceIntTotal);
        json.put("HDD_SPACE_IN_TWO_WEEKS", hddSpaceFreeIn2Weeks);
        return json;
    }
    
    @Override
    public byte[] getData() throws IOException{
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeInt(hddSpaceIntTotal.intValue()));
            out.write(DOB7bitUtils.encodeInt(hddSpaceIntFree.intValue()));
            out.write(DOB7bitUtils.encodeInt(hddSpaceExtTotal.intValue()));
            out.write(DOB7bitUtils.encodeInt(hddSpaceExtFree.intValue()));
            out.write(DOB7bitUtils.encodeInt(hddSpaceFreeIn2Weeks.intValue()));
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
    