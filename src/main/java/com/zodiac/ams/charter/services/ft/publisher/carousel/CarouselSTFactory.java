package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;

public class CarouselSTFactory {
    
    public static CarouselST getCarouselST(CarouselHost host, boolean isZipped, boolean aLocEnabled){
        CarouselSTType type = CarouselSTType.valueOf(host.getType());
        switch (type){
            case ETV:
                return new CarouselST_ETV(host, isZipped);
            case DB:
                return new CarouselST_DB(host, isZipped);
            case OCAP:
                return new CarouselST_OCAP(host, isZipped);
            default:
                return null;
        }
    }
    
}
