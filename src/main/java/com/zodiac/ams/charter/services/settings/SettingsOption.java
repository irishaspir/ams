package com.zodiac.ams.charter.services.settings;

public class SettingsOption {

    private String name;
    private String columnName;
    private int columnNumber;
    private String value;
    private String defaultValue;
    private String[] range;


    public SettingsOption(String name, String columnName, String values, String defaultValue, String[] range) {
        this.name = name;
        this.columnName = columnName;
        this.value = values;
        this.defaultValue = defaultValue;
        this.range = range;
        this.columnNumber = Integer.parseInt(columnName.substring(3));
    }

    public SettingsOption() {
    }

    public void setValue(String value){
        this.value = value;
    }

    public void setDefaultValue(String defaultValue){
        this.defaultValue = defaultValue;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String[] getRange() {
        return range;
    }


    public int getColumnNumber() {
        return columnNumber;
    }

    public static SettingsOptionsCollection create() {
        return new SettingsOptionsCollection();
    }

}
