package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DeltaErase implements IDVRSerializable{
    private final static int DELETE = 0;
    
    private final Integer[] recordingIds;
    
    public DeltaErase(Integer [] recordingIds){
        this.recordingIds = recordingIds;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(recordingIds.length));
            for (int recordingId: recordingIds){
                out.write(DOB7bitUtils.encodeUInt(DELETE));
                out.write(DOB7bitUtils.encodeUInt(recordingId));
            }
            return out.toByteArray();
        }
    }
}
