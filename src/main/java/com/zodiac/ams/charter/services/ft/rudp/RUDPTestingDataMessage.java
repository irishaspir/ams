package com.zodiac.ams.charter.services.ft.rudp;

public class RUDPTestingDataMessage extends RUDPTestingMessage {
    public final static int ALL_SEGMENTS = -1;
    
    final int segment;
    
    public RUDPTestingDataMessage(int sessionId,int segment) {
        super(sessionId);
        this.segment = segment;
    }
    
}
