package com.zodiac.ams.charter.services.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class STBMessage implements ISTBRequest{
    private final static int GET = 0;
    private final static int UPDATE = 2;
    private final static int BLOCK =3;
    
    private final static Logger LOG = LoggerFactory.getLogger(STBMessage.class);
    
    private int requestId = GET;
    private int[] packages;
    
    public STBMessage(){
    }
    
    public STBMessage(DefEntMessage message){
        if (null == message)
            return;
        if (message.isBlockAll())
            requestId = BLOCK;
        else {
            requestId = UPDATE;
            packages = message.getPackages();
        }
    }
    
    public STBMessage(byte[] message){
        if (null == message)
            return;
        try {
            DOBCountingInputStream in = new DOBCountingInputStream(new ByteArrayInputStream(message));
            requestId = DOB7bitUtils.decodeUInt(in)[0];
            if (UPDATE == requestId) {
                int count = DOB7bitUtils.decodeUInt(in)[0];
                packages = new int[count];
                for (int i=0; i< count; i++)
                    packages[i] = DOB7bitUtils.decodeUInt(in)[0];
            }
        }
        catch(IOException ex){
            LOG.error("STBMessage exception: {}", ex.getMessage());
        }
    }
    
    @Override
    public String toString(){
        return "STBMessage: requestId=" + requestId +
                ((null == packages) ? "" : " packages=" + Arrays.toString(packages));
    }
    
    @Override
    public boolean equals(Object o){
        if (!(o instanceof STBMessage))
            return false;
        STBMessage r1 = (STBMessage)o;
        if (requestId != r1.requestId)
            return false;
        
        int [] a1 = null;
        if (null != packages){
            a1 = new int[packages.length];
            System.arraycopy(packages, 0, a1, 0, packages.length);
            Arrays.sort(a1);
        }
        
        int [] a2 = null;
        if (null != r1.packages){
            a2 = new int[r1.packages.length];
            System.arraycopy(packages, 0, a2, 0, r1.packages.length);
            Arrays.sort(a2);
        }
        
        return Arrays.equals(a2, a2);
    }

    @Override
    public byte[] getData() {
        byte [] bytes = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(DOB7bitUtils.encodeUInt(requestId)); 
            if (null != packages){
                int count = packages.length;
                baos.write(DOB7bitUtils.encodeUInt(count));
                for (int i=0;i < count; i++)
                    baos.write(DOB7bitUtils.encodeUInt(packages[i]));
            }
            
            baos.flush();
            bytes = baos.toByteArray();
        }
        catch (IOException ex){
            LOG.error("STBMessage getData() exception: {}", ex.getMessage());   
        }
        return bytes;
    }
}
