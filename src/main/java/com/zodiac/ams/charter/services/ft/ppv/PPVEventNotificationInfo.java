package com.zodiac.ams.charter.services.ft.ppv;

import java.util.List;

public class PPVEventNotificationInfo {
    final PPVEventNotification notification;
    final String eid;
    int eventIndex = -1;
    int paramIndex = -1;
    
    
    PPVEventNotificationInfo(PPVEventNotification notification, String eid, int eventIndex, int paramIndex){
        this.notification = notification;
        this.eid = eid;
        this.eventIndex = eventIndex;
        this.paramIndex = paramIndex;
    }
    
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof PPVEventNotificationInfo))
            return false;
        
        PPVEventNotificationInfo other = (PPVEventNotificationInfo) obj;    
        if (!eid.equals(other.eid))
            return false;
        
        PPVEvent event1 = notification.events.events.get(eventIndex);
        List<PPVParameter> parameters1 = event1.parameters.parameters;
        PPVParameter parameter1 = parameters1.get(paramIndex);
                
        PPVEvent event2 = other.notification.events.events.get(other.eventIndex);
        List<PPVParameter> parameters2 = event2.parameters.parameters;
        PPVParameter parameter2 = parameters2.get(other.paramIndex);
        
        if (!event1.command.equals(event2.command))
            return false;
        if (!parameter1.time.equals(parameter2.time))
            return false;
        return true;
    }
    
    public static PPVEventNotificationInfo getLast(PPVEventNotification notification, String eid){
        return getElement(notification, eid,  new PPVParameter.PPVParameterComparator(false));
    }
    
    public static PPVEventNotificationInfo getFirst(PPVEventNotification notification, String eid){
        return getElement(notification, eid,  new PPVParameter.PPVParameterComparator(true));
    }
    
    private static PPVEventNotificationInfo getElement(PPVEventNotification notification, String eid, 
            PPVParameter.PPVParameterComparator comparator){
        
        PPVEventNotificationInfo result = null;
        PPVParameter parameter = null;
        
        
        if (null == notification.events)
            return null;
        
        List<PPVEvent> events = notification.events.events;
        if (null == events)
            return null;
        
        int indexE = -1;
        for (PPVEvent event: events){
            indexE++;
            if (null == event.parameters)
                continue;
            List<PPVParameter> params = event.parameters.parameters;
            if (null == params)
                continue;
            
            params.sort(comparator);
            
            int indexP = -1;
            for (PPVParameter param: params){
                indexP++;
                if (null == param)
                    continue;
                if (eid.equals(param.eid)){
                    if (null == result){
                        result = new PPVEventNotificationInfo(notification, eid, indexE, indexP);
                        parameter = param; 
                    }
                    else {
                        int i = comparator.compare(param, parameter);
                        if (i<0){
                            result.eventIndex = indexE;
                            result.paramIndex = indexP;
                            parameter = param;
                        }
                    }
                    break;
                }
            }
        }
        return result;
    }
    
}
