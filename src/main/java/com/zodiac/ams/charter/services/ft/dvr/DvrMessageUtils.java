package com.zodiac.ams.charter.services.ft.dvr;

import com.zodiac.ams.charter.services.ft.dvr.enumtype.EpisodesDefinition;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.RecordingFormat;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.SaveDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;

public class DvrMessageUtils {
    
    private static final String UNITING_FORMAT = "SH%1$08d0000";
    
    public static byte getUnitedFlagsFromObj(Object saveDays, Object recordingFormat, Object episodesDefinition,Object recordDuplicates){
        int saveDaysInt = (saveDays instanceof SaveDays) ? ((SaveDays)saveDays).getCode() : 0;
        int recordingFormatInt = (recordingFormat instanceof RecordingFormat) ? ((RecordingFormat)recordingFormat).getCode() : 0;
        int episodesDefinitionInt = (episodesDefinition instanceof EpisodesDefinition) ? ((EpisodesDefinition)episodesDefinition).getCode() : 0;
        int recordDuplicatesInt = (recordDuplicates instanceof YesNo) ? ((YesNo)recordDuplicates).getCode() : 0;
        return (byte) (
            ((0b11 & recordDuplicatesInt) << 6) |    
            ((0b11 & episodesDefinitionInt) << 4) |    
            ((0b11 & recordingFormatInt) << 2) | 
            (0b11 & saveDaysInt));
    }
    
    /**
     * 
     * @param flags
     * @return [SaveDays, RecordingFormat, EpisodesDefinition, YesNo]
     */
    public static Object[] getObjFromUnitedFlags(byte flags){
        Object [] result = new Object[4];
        result[0] = SaveDays.fromCode(0b11 & flags);
        result[1] = RecordingFormat.fromCode(0b11 & (flags >> 2));
        result[2] = EpisodesDefinition.fromCode(0b11 & (flags >> 4));
        result[3] = YesNo.fromCode(0b11 & (flags >> 6));
        return result;
    }
    
    
    public static int buildUnitingParam(String param) {
        try {
            if (null != param)
                return Integer.parseInt(param.substring(2, param.length() - 4));
        }
        catch(Exception ex){
        }
        return 0;
    }
    
    public static String buildUnitingParam(int param){
        return String.format(UNITING_FORMAT, param); 
    }
}
