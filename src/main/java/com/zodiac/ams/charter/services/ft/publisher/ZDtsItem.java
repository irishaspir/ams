package com.zodiac.ams.charter.services.ft.publisher;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import java.io.BufferedReader;
import java.io.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ZDtsItem {
    private static final Logger logger = LoggerFactory.getLogger(ZDtsItem.class);
    
    private final String name;
    private final String location;
    private final long version;
    
    private ZDtsItem(String name,String location,long version){
        this.name = name;
        this.location = location;
        this.version = version;
    }
    
    public static ZDtsItem getItem(String name,String fileName){
        ZDtsItem item = null;
        try (BufferedReader in= new BufferedReader(new FileReader(fileName))){
            in.readLine(); //header
            String line;
            while (null != (line = in.readLine())){
                String [] items=line.split(";");
                if (name.equals(items[0])){
                    Version version = FTConfig.getInstance().getPublihserVersion();
                    switch(version){
                        case VERSION_1:
                            return new ZDtsItem(items[0],items[1],Long.parseLong(items[2]));
                        default:
                            return new ZDtsItem(items[0],items[2],Long.parseLong(items[3]));
                    }
                }
            }
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        return null;
    }
    
    public String getName(){
        return name;
    }
    
    public String getLocation(){
        return location;
    }
    
    public long getVersion(){
        return version;
    }
    
    @Override
    public String toString(){
        return "Name: "+name+" Location: "+location+" Version: "+version;
    }
    
}
