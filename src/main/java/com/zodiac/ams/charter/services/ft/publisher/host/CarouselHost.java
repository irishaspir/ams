package com.zodiac.ams.charter.services.ft.publisher.host;

public class CarouselHost {
    private final String type;
    private final String host;
    private final String user;
    private final String pwd;
    private final String uid;
    private final String alias;
    private final String location;
    private final CarouselType [] serverTypes;
    private final String serverMode;
    private final boolean isZipped;
    private final String rootName;
    private final boolean isRootNode;
    private final String sourcePrefix;
    private final String dir;
    
    
    CarouselHost(CarouselType serverType, String serverMode, String type, String host, String user, String pwd, String uid, String alias, String location, 
        boolean isZipped, String rootName, boolean isRootNode, String sourcePrefix, String dir){
        this(new CarouselType[]{serverType}, serverMode, type, host, user, pwd, uid, alias, location, isZipped, rootName, isRootNode, sourcePrefix, dir);
    }
    
    CarouselHost(CarouselType [] serverTypes, String serverMode, String type, String host, String user, String pwd, String uid, String alias, String location, 
        boolean isZipped, String rootName, boolean isRootNode, String sourcePrefix, String dir){
        this.serverTypes = serverTypes;
        this.serverMode = serverMode;
        this.type = type;
        this.host = host;
        this.user = user;
        this.pwd = pwd;
        this.uid = uid;
        this.alias = alias;
        this.location = location;
        this.isZipped = isZipped;
        this.rootName = rootName;
        this.isRootNode = isRootNode;
        this.sourcePrefix = sourcePrefix;
        this.dir = dir;
    }
    
    @Override
    public String toString(){
        return uid+":"+user+":"+pwd+"@"+host+":"+type;
    }
    
    public String getType(){
        return type;
    }
    
    public String getHost(){
        return host;
    }
    
    public String getUser(){
        return user;
    }
    
    public String getPwd(){
        return pwd;
    }
    
    public String getUid(){
        return uid;
    }
    
    public String getAlias(){
        return alias;
    }
    
    public String getLocation(){
        return location;
    }
    
    public CarouselType[] getServerTypes(){
        return serverTypes;
    }
    
    public CarouselType getServerType(){
        return serverTypes[0];
    }
    
    public String getServerMode(){
        return serverMode;
    }
    
    public boolean isZipped(){
        return isZipped;
    }
    
    public String getRootName(){
        return rootName;
    }
    
    public boolean isRootNode(){
        return isRootNode;
    }
    
    public String getSourcePrefix(){
        return sourcePrefix;
    }
    
    public String getDir(){
        return (null == dir || dir.trim().equals("")) ? "/home/"+user : dir;
    }
}
