package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.KeepPartial;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.RandomUtils;

public class StopRecordingRequestParam implements IDVRObject{
    private Object recordingId;
    private Object keepPartial;
    
    public StopRecordingRequestParam(){
    }
    
    public StopRecordingRequestParam(Integer recordingId, KeepPartial keepPartial){
        this.recordingId = recordingId;
        this.keepPartial = keepPartial;
    }
    
    public StopRecordingRequestParam(StopRecordingRequestParam param){
        this.recordingId = param.recordingId;
        this.keepPartial = param.keepPartial;
    }
    
    public static StopRecordingRequestParam getRandom(){
        StopRecordingRequestParam param = new StopRecordingRequestParam();
        param.recordingId = RandomUtils.getRandomInt(1, 1_000_000);
        param.keepPartial = RandomUtils.getRandomObject(KeepPartial.values());
        return param;
    }
    
    public void setRecordingId(Object recordingId){
        this.recordingId = recordingId;
    }
    
    public int getRecordingId(){
        return JSONUtils.getInt(recordingId);
    }
    
    public void setKeepPartial(Object keepPartial){
        this.keepPartial = keepPartial;
    }
    
    public KeepPartial getKeepPartial(){
        return (keepPartial instanceof KeepPartial) ? (KeepPartial)keepPartial : null;
    }

    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONUtils.setField(json, "recordingId", recordingId);
        JSONUtils.setField(json, "keepPartial", keepPartial);
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            if (null != recordingId)
                out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(recordingId)));
            if (null != keepPartial){
                if (keepPartial instanceof KeepPartial)
                    out.write(DOB7bitUtils.encodeUInt(((KeepPartial)keepPartial).getCode()));
                else
                    out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(keepPartial)));
            }
            return out.toByteArray();
        }
    }
}
