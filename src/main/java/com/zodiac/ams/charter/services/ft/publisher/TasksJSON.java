package com.zodiac.ams.charter.services.ft.publisher;

import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TasksJSON {
    private static final Logger logger = LoggerFactory.getLogger(TasksJSON.class);
    
    private static final String SERVER_TYPE_FIELD = "serverType";
    private static final String TASKS_FIELD =  "tasks";
    private static final String DESCRIPTOR_NAME = "tasks.json";
    public static final String ZIP_NAME = "tasks.zip";
    
    
    private CarouselType serverType;
    private PublishTasks tasks;
    
    public TasksJSON(CarouselType serverType,PublishTasks tasks){
        this.serverType = serverType;
        this.tasks = tasks;
    }
    
    public CarouselType getServerType(){
        return serverType;
    }
    
    void setServerType(CarouselType serverType){
        this.serverType = serverType;
    }
    
    JSONObject toJSON(){
        JSONObject obj  = new JSONObject();
        if (null != serverType)
            obj.put(SERVER_TYPE_FIELD, serverType.toString());
        if (null != tasks)
            obj.put(TASKS_FIELD ,tasks.toJSONArray());
        return obj;
    }
    
    
    void saveDescriptor(String name) {
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name)))){
            out.print(toJSON().toString());
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
    }
    
    
    public File createZIP(){
        return createZIP(DESCRIPTOR_NAME,ZIP_NAME);
    } 
    
    /**
     * 
     * @param out
     * @param source sourceFile property in the tasks.json
     * @param fileToAdd the name of the real file to add
     * @throws IOException 
     */
    void addFileToZip(ZipOutputStream out,String source,String fileToAdd) throws IOException{
        byte [] buffer =new byte[4_096];
        ZipEntry entry = new ZipEntry(source);
        out.putNextEntry(entry);
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(fileToAdd), buffer.length)){
            int read;
            while(-1 != (read=in.read(buffer)))
                out.write(buffer,0, read);
        }       
    }
    
    /**
     * 
     * @param descriptor name e.g. tasks.json
     * @param name zip name e.g. tasks.zip
     * @return 
     */
    File createZIP(String descriptor,String name){
        try (ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(name)))){
            saveDescriptor(descriptor);
            addFileToZip(out,descriptor,descriptor);
            int size = tasks.size();
            for (int i=0;i<size;i++){
                String source = tasks.getTask(i).getSource();
                String fileToAdd = tasks.getTask(i).getFileName();
                if (null != fileToAdd && !fileToAdd.trim().equals(""))
                    addFileToZip(out,source,fileToAdd);
            }
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
        return new File(name);
    }
    
}
