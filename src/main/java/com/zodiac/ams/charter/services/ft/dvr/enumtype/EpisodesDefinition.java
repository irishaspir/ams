package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum EpisodesDefinition {
    ALL("All episodes", 1), NEW("New episodes only", 2);
    
    private final String definition;
    private final int code;
    
    private EpisodesDefinition(String definition, int code){
        this.definition = definition;
        this.code = code;
    }
    
    @Override
    public String toString(){
        return definition;
    }
    
    public String getDefinition(){
        return definition;
    }
    
    public int getCode(){
        return code;
    }
    
    public static EpisodesDefinition fromCode(int code){
        EpisodesDefinition [] defs = EpisodesDefinition.values();
        for (EpisodesDefinition def: defs){
            if (code == def.code)
                return def;
        }
        return null;
    }
}
