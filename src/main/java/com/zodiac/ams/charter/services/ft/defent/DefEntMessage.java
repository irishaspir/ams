package com.zodiac.ams.charter.services.ft.defent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

public class DefEntMessage {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    private String macAddress;
    private boolean blockAll;
    private int[] packages;
    
    private EntError[] errors;
    private int[] transactionIds;
    private String []  dateTimes;
    
    private int transactionId;
    private String dateTime;
    
    
    public DefEntMessage(String macAddress, boolean blockAll, int[] packages){
        this(macAddress, blockAll, packages, null);
    }
    
    public DefEntMessage(String macAddress, boolean blockAll, int[] packages, EntError[] errors){
        this.macAddress = macAddress;
        this.blockAll = blockAll;
        this.packages = packages;
        
        if (null != errors && errors.length>0){
            Random random = new Random();
            this.errors = errors;
            transactionIds = new int[errors.length];
            dateTimes = new String[errors.length];
            for (int i=0; i<errors.length; i++) {
                transactionIds[i] = 1 + random.nextInt(10_000);
                dateTimes[i] = now();
            }
            transactionId = 1 + random.nextInt(10_000);
            dateTime = now();
        }
    }
    
    public JSONObject errorsToJSON(){
        JSONObject obj = new JSONObject();
        //MASTER 453
        //obj.put("macAddress", macAddress);
        if (null != errors){
            JSONArray err =new JSONArray();
            for (int i=0; i< errors.length; i++){
                JSONObject o =new JSONObject();
                o.put("id", macAddress); //
                o.put("errorCode", String.valueOf(errors[i].getCode()));
                o.put("errorDescription", errors[i].getDescription());
                o.put("transactionId", String.valueOf(transactionIds[i]));
                o.put("dateTime", dateTimes[i]);
                err.put(o);
            }
            //obj.put("errors", err);
            obj.put("stbError", err);
        }
        return obj;
    }
    
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        obj.put("macAddress", macAddress);
        obj.put("blockAll", String.valueOf(blockAll));
        JSONArray arr = new JSONArray();
        for (int i=0; i< packages.length; i++)
            arr.put(String.valueOf(packages[i]));
        obj.put("packages", arr);
        if (null != errors){
            JSONArray e = new JSONArray();
            for (int i=0; i<errors.length; i++){
                JSONObject o = new JSONObject();
                o.put("errorCode", String.valueOf(errors[i].getCode()));
                o.put("errorDescription", errors[i].getDescription());
                o.put("transactionId", String.valueOf(transactionIds[i]));
                o.put("dateTime", dateTimes[i]);
                e.put(o);
            }
            obj.put("errors", e);
            obj.put("transactionId", String.valueOf(transactionId));
            obj.put("dateTime", dateTime);
        }
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
    public int[] getPackages(){
        return packages;
    }
    
    public boolean isBlockAll(){
        return blockAll;
    }
    
    public EntError[] getErrors(){
        return errors;
    }
    
    private String now(){
        return DATE_FORMAT.format(new Date());
    }
}
