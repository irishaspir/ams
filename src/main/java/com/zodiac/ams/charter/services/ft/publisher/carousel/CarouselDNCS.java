package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.services.ft.publisher.Constants;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CarouselDNCS extends CarouselSFTP{
    String homeDir;
    
    public CarouselDNCS(CarouselHost host,SSHHelper executor,String location,String rootTemplate,boolean isZipped){
        super(host, executor, location, rootTemplate, isZipped);
        this.homeDir = executor.getHomeDir();
    }
    
    public CarouselDNCS(CarouselHost host,SSHHelper executor,String location,boolean isZipped){
        super(host, executor, location, isZipped);
        this.homeDir = executor.getHomeDir();
    }
    
//    public String generateFileName(String name,int minutesPast){
//        return generateFileName(name, minutesPast, 0);
//    }

    public String generateFileName(String name, long remoteDiff){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, -(Constants.MAX_MINUTES+1));
        long ts = calendar.getTimeInMillis() + remoteDiff;
        
        String prefix = host.getSourcePrefix();
        if (null != prefix && !prefix.trim().equals(""))
            return name+"."+(prefix/*+name*/).hashCode()+"."+ts;
        else
            return name+"."+ts;
    }
    
    //public String getFileMaxTimeStamp(String name){
    public List<String> getFilesByName(String name){
        String remoteDir = homeDir+"/"+host.getLocation();
        String mask = "^"+name.replace(".", "\\.")+"(\\.\\d+|)$";
        int index = name.lastIndexOf("/");
        if (-1 != index){
            String nm = name.substring(index+1);
            String prefix = name.substring(0, index+1);
            mask = "^" + nm.replace(".", "\\.") + "(\\." + /*name.hashCode()*/ prefix.hashCode() + "\\.\\d+|)$";
        }
        List<String> names = getFiles(remoteDir, mask);
        Collections.sort(names);
        return names;
    }
    
    public static List<String> getOldFiles(List<String> names,long oldest){
        List<String> old = new ArrayList<>();
        Pattern pattern = Pattern.compile("^.+\\.(\\d+)$");
        for (String name: names){
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()){
                long ts = Long.parseLong(matcher.group(1));
                if (ts<oldest)
                    old.add(name);
            }
        }
        Collections.sort(old);
        return old;
    }
    
    List<String> getFiles(String remoteDir,String mask){
        List<String> result = new ArrayList<>();
        String output = executor.getExecOutput("ls "+remoteDir+" -l");
        String [] lines = output.split("\\n+");
        for (String line: lines){
            String [] items = line.split("\\s+");
            if (items.length>2){
                String name = items[items.length-1];
                if (Pattern.matches(mask, name))
                    result.add(name);
            }
        }
        return result;
    }
    
    @Override
    public String getLocation(String name){
        String last = name;
        int index =last.lastIndexOf("/");
        if (-1 != index)
            last = last.substring(index+1);
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch(version){
            case VERSION_1:
                return "bfs://"+host.getLocation()+"/"+name;
            case VERSION_2:
            case VERSION_3:
                return "bfs://assets-obj-"+getUid()+"/"+name;
            default:
                return "bfs://assets-obj/"+last;
        }
    }
    
}
