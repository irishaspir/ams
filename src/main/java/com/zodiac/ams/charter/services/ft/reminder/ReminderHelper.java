package com.zodiac.ams.charter.services.ft.reminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReminderHelper {
    
    private static final String DEVICE_ID = "deviceId";
    private static final String OPERATION = "operation";
    private static final String CHANNEL = "reminderChannelNumber";
    private static final String START = "reminderProgramStart";
    private static final String ID = "reminderProgramId";
    private static final String OFFSET = "reminderOffset";
    private static final String REMINDERS = "reminders";
    
    
    private List<Reminder> reminders=null;
    private String deviceId;
    private static SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    
    public static class Reminder{
        private Operations operation;
        private Integer channelNumber;
        private String programStart;
        private String programId;
        private Integer offset;
        
        public Reminder(Operations operation){
            this.operation = operation;
        }
        
        public Reminder(Operations operation,Integer channelNumber,String programStart,String programId,Integer offset){
            this.operation=operation;
            this.channelNumber = channelNumber;
            this.programStart = programStart;
            //this.programId = programId;
            this.offset = offset;
        }
        
        public Reminder(Operations operation,Integer channelNumber,int day,String programId,Integer offset){
            this.operation=operation;
            this.channelNumber = channelNumber;
            
            Calendar cl=Calendar.getInstance();
            cl.setTime(new Date());
            cl.add(Calendar.DATE,day);
            this.programStart=format.format(cl.getTime());
            
            //this.programId = programId;
            this.offset = offset;
        }
        
        public String getProgramStart(){
            return programStart; 
       }
    }
    
    public static enum Operations{
        ADD("Add"),DELETE("Delete"),PURGE("Purge");
        private Operations(String operation){
            this.operation=operation;
        }
        
        private String operation;
        
        @Override
        public String toString(){
            return operation;
        }
    }

    public ReminderHelper(String deviceId,boolean createRemidersList){
        this.deviceId=deviceId;
        reminders=createRemidersList ? new ArrayList<Reminder>() : null;
    }
    
    public ReminderHelper(String deviceId){
        this.deviceId=deviceId;
        reminders=new ArrayList<>();
    }
    
    public void clear(){
        if (null != reminders)
            reminders.clear();
    }
    
    
    
    public void addReminder(Reminder reminder){
        if (null != reminders)
            reminders.add(reminder);
    }
    
    public void addReminders(Reminder[] reminder){
        if (null != reminders)
            reminders.addAll(Arrays.asList(reminder));
    }
    
    @Override
    public String toString(){
        JSONObject json=new JSONObject();
        if (null != deviceId)
            json.put(DEVICE_ID,deviceId);
        if (null != reminders){
            JSONArray jarray=new JSONArray();
            for (Reminder reminder: reminders){
                JSONObject obj=new JSONObject();
                if (null != reminder.operation)
                    obj.put(OPERATION,reminder.operation.toString());
                if (null != reminder.channelNumber)
                    obj.put(CHANNEL,reminder.channelNumber);
                if (null != reminder.programStart)
                    obj.put(START,reminder.programStart);
                //if (null != reminder.programId)
                //    obj.put(ID,reminder.programId);
                if (null != reminder.offset)
                    obj.put(OFFSET,reminder.offset);
                jarray.put(obj);
            }
            json.put(REMINDERS, jarray);
        }
        return json.toString();
    }
    
   
    
}

