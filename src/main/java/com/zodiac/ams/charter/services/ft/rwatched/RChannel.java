package com.zodiac.ams.charter.services.ft.rwatched;

import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.http.FTHttpUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RChannel {
    static final String CHANNEL_PROP = "channelNumber";
    static final String TIME_PROP = "lastWatchedTime";
    
    static DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    
    private final static Logger logger = LoggerFactory.getLogger(RChannel.class);
    
    String channelNumber;
    String lastWatchedTime;
    
    public static void setTimeZone(String id){
        if (null == id || id.trim().equals(""))
            format.setTimeZone(TimeZone.getDefault());
        else
            format.setTimeZone(TimeZone.getTimeZone(id));
    }
    
    public RChannel(String channelNumber,String lastWatchedTime){
        this.channelNumber = channelNumber;
        this.lastWatchedTime = lastWatchedTime;
    }
    
    public RChannel(int channel,Date date){
        this.channelNumber = String.valueOf(channel);
        this.lastWatchedTime = format.format(date);
    } 
    
    
    public JSONObject toJSON(){
        JSONObject json=new JSONObject();
        if (null != channelNumber)
            json.put(CHANNEL_PROP, channelNumber);
        if (null != lastWatchedTime)
            json.put(TIME_PROP, lastWatchedTime);
        return json;
    }
    
    
    public String getChannelNumber(){
        return channelNumber;
    }
    
    public String getLastWatchedTime(){
        return lastWatchedTime;
    }
    
    public int getChannel(){
        return (null == channelNumber) ? 0 : Integer.parseInt(channelNumber);
    }
        
    public int getTimeMins(){
        if (null == lastWatchedTime)
            return 0;
        try{
            Date dt=format.parse(lastWatchedTime);
            return (int)(dt.getTime()/60_000L);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        return 0;
    }
    
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof RChannel))
            return false;
        RChannel ch=(RChannel)obj;
        return FTUtils.stringsEqual(channelNumber,ch.channelNumber) && FTUtils.stringsEqual(lastWatchedTime, ch.lastWatchedTime);
    }
}
