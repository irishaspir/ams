package com.zodiac.ams.charter.services.ft.rudp;

public class RUDPTestingMessage {
    final int sessionId;
    
    RUDPTestingMessage(int sessionId){
        this.sessionId = sessionId;
    }
}
