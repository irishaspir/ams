package com.zodiac.ams.charter.services.ft.cleanup;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CleanupRequest {
    
    private static final String DEVICES = "devices";
    private static final String DEVICE_ID = "deviceId";
            
    private List<String> devices;
    
    public void addDevice(String id){
        if (null == devices)
            devices = new ArrayList<>();
        devices.add(id);
    }
    
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        if (null != devices){
            JSONArray arr = new JSONArray();
            for (String id: devices){
                JSONObject o = new JSONObject();
                o.put(DEVICE_ID, id);
                arr.put(o);
            }
            obj.put(DEVICES, arr);
        }
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
