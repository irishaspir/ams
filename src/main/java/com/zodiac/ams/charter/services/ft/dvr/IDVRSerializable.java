package com.zodiac.ams.charter.services.ft.dvr;

import java.io.IOException;

public interface IDVRSerializable {
    byte[] getData() throws IOException;
}
