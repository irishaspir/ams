package com.zodiac.ams.charter.services.cd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SpiridonovaIM on 31.05.2017.
 */
public enum CDProtocolOptions {

    CHANNEL_NUM("ChannelNum", "1", "918", "", new String[]{}),

    CALLSIGN("Callsign", "1", "MC", "", new String[]{}),

    KEY("Key", "2", "0", "1", new String[]{"1", "52"}),

    DEEPLINK("Deeplink", "5", "0", "", new String[]{}),

    RECORDING_ID("RecordingId", "6", "0", "", new String[]{}),

    PLAYBACK_OFFSET("PlaybackOffset", "6", "0", "", new String[]{}),

    MODE("Mode", "7", "0", "", new String[]{"0","7"}),

    SPEED("Speed", "7", "0", "", new String[]{"0","7"}),

    INTERVAL("Interval", "7", "0", "", new String[]{}),;

    private CDProtocolOption dsgOption;

    CDProtocolOptions(String name, String requestId, String value, String defaultValueInDB, String[] range) {
        dsgOption = new CDProtocolOption(name, requestId, value, defaultValueInDB, range);
    }

    public CDProtocolOption getOption() {
        return dsgOption;
    }

    public static List<CDProtocolOption> allDsgProtocolOptions() {
        ArrayList<CDProtocolOption> options = new ArrayList<>();
        Arrays.asList(CDProtocolOptions.values()).forEach(value -> options.add(value.getOption()));
        return options;
    }

    public static List<CDProtocolOption> dsgProtocolOptions() {
        ArrayList<CDProtocolOption> options = new ArrayList<>();
        Arrays.asList(CDProtocolOptions.values()).forEach(value -> {
            if (!value.getOption().getValue().equals("-1")) options.add(value.getOption());
        });
        return options;
    }


}
