package com.zodiac.ams.charter.services.ft.rudp.util;

public class NetworkOrderBitUtils {

    public static int b2i(byte b0, byte b1) {
        return ((0xFF & b0) << 8) | (0xFF & b1);
    }

    public static byte[] i2b2(int x) {
        byte[] a = new byte[2];
        a[0] = (byte)((x >> 8) & 0xFF);
        a[1] = (byte)((x     ) & 0xFF);
        return a;
    }

    public static byte[] l2b4(long x) {
        byte[] a = new byte[4];
        a[0] = (byte)((x >> 24) & 0xFF);
        a[1] = (byte)((x >> 16) & 0xFF);
        a[2] = (byte)((x >> 8) & 0xFF);
        a[3] = (byte)((x     ) & 0xFF);
        return a;
    }
}
