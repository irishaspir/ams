package com.zodiac.ams.charter.services.dsg;


import java.util.ArrayList;
import java.util.List;


public class DsgOptionsCollection {

    private ArrayList<DsgProtocolOption> list = new ArrayList<>();

    public DsgOptionsCollection addOption(DsgProtocolOption option) {
        list.add(new DsgProtocolOption(option.getName(), option.getColumnName(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public DsgOptionsCollection addOptions(DsgProtocolOptions option) {
        list.add(new DsgProtocolOption(option.getOption().getName(), option.getOption().getColumnName(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<DsgProtocolOption> build() {
        return list;
    }

    public DsgOptionsCollection addListOptions(List<DsgProtocolOption> list) {
        this.list.addAll(list);
        return this;
    }

}
