package com.zodiac.ams.charter.services.ft.rudp.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RUDP1SessionRegistryServiceImpl implements RUDP1SessionRegistryService<SessionKey> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    protected final Map<SessionKey, IRUDPSession> sessions;

    public RUDP1SessionRegistryServiceImpl() {
        // initial capacity, LF, concurrency level
        sessions = new ConcurrentHashMap<>(1024*1024, 0.75f, 1024);
    }

    @Override
    public IRUDPSession getSession(SessionKey key) {
        return sessions.get(key);
    }

    @Override
    public void putSession(SessionKey key, IRUDPSession session) {
        log.debug("Put session by key: {}", key);
        sessions.put(key, session);
    }

    @Override
    public boolean removeSession(SessionKey key) {
        log.debug("Remove session by key: {}", key);
        return sessions.remove(key) != null;
    }

    @Override
    public boolean containsSession(SessionKey key) {
        return sessions.containsKey(key);
    }

    @Override
    public  Collection<SessionKey> keys() {
        return sessions.keySet();
    }

    @Override
    public void close() throws Exception {
        for (IRUDPSession session : sessions.values()) {
            session.close();
        }
        log.debug("RUDP: RUDP1SessionRegistryServiceImpl closed");
    }

    @Override
    public IRUDPSenderSession createSenderSession(InetSocketAddress remoteAddress, int sessionId, byte[] dataBytes, int segmentSize) {
        IRUDPSenderSession session = new RUDP1SenderSession(remoteAddress, sessionId, dataBytes, segmentSize);
        SessionKey key = new SessionKey(remoteAddress, sessionId);
        putSession(key, session);
        return session;
    }

    @Override
    public IRUDPReceiverSession createReceiverSession(InetSocketAddress remoteAddress, int sessionId, int dataSize, int segmentSize) {
        IRUDPReceiverSession session = new RUDP1ReceiverSession(remoteAddress, sessionId, dataSize, segmentSize);
        SessionKey key = new SessionKey(remoteAddress, sessionId);
        putSession(key, session);
        return session;
    }
}
