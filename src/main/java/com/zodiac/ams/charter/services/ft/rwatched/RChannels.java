package com.zodiac.ams.charter.services.ft.rwatched;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class RChannels {
    public static final String CHANNELS_PROP = "channels";
    
    private List<RChannel> channels=new ArrayList<>();
    
    public void addChannel(RChannel channel){
        channels.add(channel);
    }
    
    public JSONArray toJSONArray(){
        JSONArray array=new JSONArray();
        for (RChannel channel: channels)
            array.put(channel.toJSON());
        return array;
    }
    
    public int size(){
        return channels.size();
    }
    
    public RChannel getChannel(int index){
        return channels.get(index);
    }
    
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof RChannels))
            return false;
        RChannels channels=(RChannels)obj;
        if (channels.size() != size())
            return false;
        for (int i=0;i<size();i++){
            if (!getChannel(i).equals(channels.getChannel(i)))
                return false;
        }
        return true;
    }
    
}
