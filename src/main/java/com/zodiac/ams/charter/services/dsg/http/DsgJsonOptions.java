package com.zodiac.ams.charter.services.dsg.http;

import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_1;


public enum DsgJsonOptions {

    STB_MAC("deviceId", DEVICE_ID_NUMBER_1),

    //  LAST_ACTIVITY_TIME_UTC("lastActivityTimeUTC", converterUnixToDateFormat(new STBMetadataUtils().getSTBLastActivityLastTimeToDate(MAC_NUMBER_1))),
    LAST_ACTIVITY_TIME_UTC("lastActivityTimeUTC", "${json-unit.ignore}"),

    NETWORK_TYPE("networkType", "ALOHA");

    private DsgJsonOption dsgJsonOption;

    DsgJsonOptions(String nameJSON, String valueJSON) {
        dsgJsonOption = new DsgJsonOption(nameJSON, valueJSON);
    }

    public DsgJsonOption getOption() {
        return dsgJsonOption;
    }
}
