package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import java.util.ArrayList;

public class EraseRequest implements IDVRObject{
    private final static String METHOD = "ERASE";
    
    private String deviceId;
    private List<EraseRequestParam> params;
    
    public static EraseRequest getRandom(String deviceId, int size){
        int[] ids = RandomUtils.getRandomIntArray(size, size, 1, 1_000_000, true);
        List<EraseRequestParam> params = new ArrayList<>();
        for (int id: ids)
            params.add(new EraseRequestParam(id, RandomUtils.getRandomObject(YesNo.values())));
        return new EraseRequest(deviceId, params);
    }
    
    public EraseRequest(String deviceId){
        this.deviceId = deviceId;
    }
    
    public EraseRequest(String deviceId,List<EraseRequestParam> params){
        this(deviceId);
        this.params = params;
    }
    
    public Integer[] getIds(){
        List<Integer> ids = new ArrayList<>();
        if (null != params){
            for (EraseRequestParam param: params)
                ids.add(param.getRecordingId());
        }
        return ids.toArray(new Integer[]{});
    }
    
    public int getSize(){
        return (null == params) ? 0 : params.size();
    }
    
    public EraseRequestParam getParam(int index){
        return params.get(index);
    }
    
    public List<EraseRequestParam> getParams(){
        return params;
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded){
        JSONObject json = new JSONObject();
        JSONUtils.setField(json, "deviceId", deviceId);
        JSONUtils.setField(json, "method", METHOD);
        if (null != params){
            JSONArray arr = new JSONArray();
            for (EraseRequestParam param: params)
                arr.put(param.toJSON());
            json.put("params", arr);
        }
        return json;
    }
    
    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(Method.ERASE.getCode()));
            if (null != params){
                out.write(DOB7bitUtils.encodeUInt(params.size()));
                for (EraseRequestParam param: params)
                    out.write(param.getData());
            }
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
