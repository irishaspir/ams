package com.zodiac.ams.charter.services.ft.rudp.session;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by anton on 29.06.16.
 */
public class RUDP1SessionRegistryServiceClusterRouting extends RUDP1SessionRegistryServiceWithAutoPurgeImpl implements IClusterRouting {

    private final static Logger log = LoggerFactory.getLogger(RUDP1SessionRegistryServiceWithAutoPurgeImpl.class);

    public static final String MAP_NAME = "AMS.rudp1.session-registry-routing";
    public static final String EXECUTOR_NAME = "AMS.rudp1.routing.executor";

    private final IMap<SessionKey, Member> routing;
    private final IExecutorService executorService;
    private final HazelcastInstance hazelcast;

    public RUDP1SessionRegistryServiceClusterRouting(HazelcastInstance hazelcast, long sessionIdleTimeoutMins) {
        super(sessionIdleTimeoutMins);
        if (hazelcast == null) {
            throw new IllegalArgumentException("Hazelcast arg is null!");
        }
        this.hazelcast = hazelcast;
        this.routing = hazelcast.getMap(MAP_NAME);
        this.executorService = hazelcast.getExecutorService(EXECUTOR_NAME);
    }

    @Override
    public void putSession(SessionKey key, IRUDPSession session) {
        routing.put(key, hazelcast.getCluster().getLocalMember());
        super.putSession(key, session);
    }

    @Override
    public boolean removeSession(SessionKey key) {
        routing.remove(key);
        return super.removeSession(key);
    }

    @Override
    public boolean containsSession(SessionKey key) {
        return routing.containsKey(key);
    }

    @Override
    public boolean executeOnMember(SessionKey key, Runnable task) {
        final Member member = routing.get(key);
        if (member == null) {
            log.debug("node for routing not found {}", key.toString());
            return false;
        }
        if (member.localMember()) {
            log.debug("node for routing is local {}", key.toString());
            return false;
        }
        log.debug("routing {} to node", key.toString(), member.toString());
        executorService.executeOnMember(task, member);
        return true;
    }
}
