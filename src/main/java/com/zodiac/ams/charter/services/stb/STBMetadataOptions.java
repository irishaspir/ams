package com.zodiac.ams.charter.services.stb;

import com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class STBMetadataOptions {

    private List<String> dataFromDB;
    private Object[][] data;
    private Map<String, String> results = new HashMap<>();

    public STBMetadataOptions(List<String> dataFromDB) {
        this.data = new STBMetadataUtils().getTableDescription();
        this.dataFromDB = dataFromDB;
        setDataValue();
    }

    private void setDataValue() {
        if (dataFromDB.size() == data.length) {
            for (int i = 0; i < data.length; i++) {
                results.put(String.valueOf(data[i][0]), dataFromDB.get(i));
            }
        } else throw new RuntimeException("Wrong size STB_METADATA table");
    }

    private String getValue(String key) {
        return String.valueOf(results.get(key));
    }

    public long getMac() {
        return convertUTimeToLong(getValue("MAC"));
    }

    public String getMacStr() {
        return String.valueOf(getValue("MAC_STR"));
    }

    public int getStbMode() {
        return getIntData(getValue("STB_MODE"));
    }

    public long getRegistrTimeStamp() {
        return getUTimeToLongData("REGISTR_TIME_STAMP");
    }

    public long getLasActTimeStamp() {
        return getUTimeToLongData("LAST_ACT_TIME_STAMP");
    }

    public long getModeChangeTimeStamp() {
        return getUTimeToLongData("MODE_CHANGE_TIME_STAMP");
    }

    public int getGroupCDL() {
        return getIntData("GROUP_CDL");
    }

    public int getGroupCDS() {
        return getIntData("GROUP_CDS");
    }

    public int getStbModelId() {
        return getIntData("STB_MODEL_ID");
    }

    public int getVendorId() {
        return getIntData("VENDOR_ID");
    }

    public int getRomId() {
        return getIntData("ROM_ID");
    }

    public long getMsgTypesMap() {
        return getLongData("MSG_TYPES_MAP");
    }

    public String getTimezone() {
        return getValue("TIMEZONE");
    }

    public int getHubId() {
        return getIntData("HUBID");
    }

    public int getStbMetadataFlags() {
        return getIntData("STB_METADATA_FLAGS");
    }
    private int convertToInt(String str) {
        return Integer.parseInt(str);
    }

    private long convertUTimeToLong(String str) {
      long x=  Long.parseLong(str.substring(0, str.length() - 3)) ;
        return x;
    }

    private long convertToLong(String str) {
        return Long.parseLong(str);
    }

    private boolean isDataNull(String str) {
        return str.equals("null");
    }

    private boolean isData0(String str) {
        return str.equals("0");
    }

    private long getUTimeToLongData(String str) {
        return ((!isDataNull(getValue(str)) && !isData0(getValue(str))) ? convertUTimeToLong(getValue(str)) : 0);
    }

    private int getIntData(String str){
        return (!isDataNull(getValue(str)) ? convertToInt(getValue(str)) : 0);
    }

    private long getLongData(String str){
        return (!isDataNull(getValue(str)) ? convertToLong(getValue(str)) : 0);
    }

}
