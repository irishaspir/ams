package com.zodiac.ams.charter.services.ft.publisher.host;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.*;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CarouselHosts {
    private static final String DALINC = "DALINC";
            
    private static final String PREFIX="carousel";
    
    private static final String HOST_PROP = "host";
    private static final String USER_PROP = "user";
    private static final String PWD_PROP = "pwd";
    private static final String UID_PROP = "uid";
    private static final String TYPE_PROP = "type";
    private static final String MODE_PROP = "mode";
    private static final String ALIAS_PROP = "alias";
    private static final String LOCATION_PROP = "location";
    private static final String ROOT_NAME_PROP = "root-name";
    private static final String ROOT_NODE_PROP = "root-node";
    private static final String SOURCE_PREFIX_PROP = "source-prefix";
    private static final String DIR_PROP = "dir";
    
    protected final CarouselType[] carouselTypes;
    protected final boolean dalinc;
    
    protected final List<CarouselHost> hosts = new ArrayList<>();
    
    
    private String getPropName(String name){
        return PREFIX + "-" + carouselTypes[0].toString() + "-" + name;
    }
    
    private String getDirPropName(){
        return getPropName(DIR_PROP);
    }
    
    private String getHostPropName(){
        return getPropName(HOST_PROP);
    }
    
    private String getUserPropName(){
        return getPropName(USER_PROP);
    }
    
    private String getPwdPropName(){
       return getPropName(PWD_PROP);
    }
    
    private String getUidPropName(){
        return getPropName(UID_PROP);
    }
    
    private String getTypePropName(){
        return getPropName(TYPE_PROP);
    }
    
    private String getModePropName(){
        return getPropName(MODE_PROP);
    } 
    
    private String getAliasPropName(){
        return getPropName(ALIAS_PROP);
    }
    
    private String getLocationPropName(){
        return getPropName(LOCATION_PROP);
    }
    
    private String getRootNamePropName(){
        return getPropName(ROOT_NAME_PROP);
    }
    
    private String getRootNodePropName(){
        return getPropName(ROOT_NODE_PROP);
    }
    
    private String getSourcePrefixPropName(){
        return getPropName(SOURCE_PREFIX_PROP);
    }
    
    public CarouselHosts(FTConfig properties, CarouselType carouselType, boolean isZipped, boolean aLocEnabled, boolean dalinc){    
        this(properties, new CarouselType[] {carouselType}, isZipped, aLocEnabled, dalinc);
    } 
    
    public CarouselHosts(FTConfig properties, CarouselType[] carouselTypes, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        this.carouselTypes = carouselTypes;
        this.dalinc = dalinc;
       
        FTConfig props = properties.getPublisher();
        
        String uid = props.getProperty(getUidPropName());
        String mode = props.getProperty(getModePropName());
        String type = props.getProperty(getTypePropName());
        String host = props.getProperty(getHostPropName());
        String user = props.getProperty(getUserPropName());
        String pwd = props.getProperty(getPwdPropName());
        String alias = props.getProperty(getAliasPropName());
        String location = props.getProperty(getLocationPropName());
        String rootName = props.getProperty(getRootNamePropName());
        String prefix = props.getProperty(getSourcePrefixPropName());
        String dir = props.getProperty(getDirPropName());
        
        boolean isRootNode = false;
        if (aLocEnabled)
            isRootNode = Boolean.parseBoolean(props.getProperty(getRootNodePropName()));
        
        if (isValidMode(carouselTypes, mode))
            hosts.add(new CarouselHost(carouselTypes, mode, type,host,user,pwd,uid,alias,location,isZipped,rootName,isRootNode,prefix,dir));
        int index = 1;
        
        while (null != (host = props.getProperty(getHostPropName()+"-"+index))){
            String uid1 = props.getProperty(getUidPropName()+"-"+index);
            String mode1 = props.getProperty(getModePropName()+"-"+index,mode);
            String type1 = props.getProperty(getTypePropName()+"-"+index,type);
            String user1 = props.getProperty(getUserPropName()+"-"+index,user);
            String pwd1 = props.getProperty(getPwdPropName()+"-"+index,pwd);
            String alias1 = props.getProperty(getAliasPropName()+"-"+index,alias);
            String location1 = props.getProperty(getLocationPropName()+"-"+index,location);
            String rootName1 = props.getProperty(getRootNamePropName()+"-"+index);
            String prefix1 = props.getProperty(getSourcePrefixPropName()+"-"+index,prefix);
            String dir1 = props.getProperty(getDirPropName()+"-"+index);
            boolean isRootNode1 = false;
            if (aLocEnabled)
                isRootNode1 = Boolean.parseBoolean(props.getProperty(getRootNodePropName()+"-"+index));
            if (isValidMode(carouselTypes, mode1))
                hosts.add(new CarouselHost(carouselTypes, mode1, type1,host,user1,pwd1,uid1,alias1,location1,isZipped,rootName1,isRootNode1,prefix1,dir1));
            index++;
        }
    }
    
    private boolean isValidMode(CarouselType[] carouselTypes, String mode){
        for (CarouselType type: carouselTypes){
            if (type.toString().equals(mode))
                return true;
        }
        return false;
    }
    
    public int size(){
        return hosts.size();
    }
    
    public CarouselHost get(int index){
        return hosts.get(index);
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts)
            sb.append(sb.length()>0 ? ";" : "").append(host);
        return sb.toString();
    }
     
    public String getAlias(){
        return hosts.get(0).getAlias();
    }
    
    public String getLocation(){
        return hosts.get(0).getLocation();
    }
    
    public String getProperty(String name){
        if (ZDTS_PROP.equals(name))
            return getZdtsIni();
        if (MODES_PROP.equals(name))
            return getServerModes();
        if (TYPES_PROP.equals(name))
            return getServerTypes();
        if (ROOT_ZDTS_PROP.equals(name))
            return getRootZdtsIni();
        return null;
    }
    
    protected String getZdtsIni(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts){
            sb.append(sb.length()>0 ? ";" : "").append(host.getUid()).
                    append(":").
                    append(host.getAlias()).
                    append("/");
            if (host.isZipped())
                sb.append(ZDTS_INI_ZIP).append(":");
            sb.append(ZDTS_INI);
        }
        return sb.toString();
    }
    
    protected String getRootZdtsIni(){
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch (version){
            case VERSION_1:
            case VERSION_2:
            case VERSION_3:
            case VERSION_3_1:
                return getRootZdtsIni_1();
            default:
                return getRootZdtsIni_3_2();
        }
    }
    
    //protected String
    private String getRootZdtsIni_1(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts) {
            if (host.isRootNode()){
                sb.append(sb.length()>0 ? ";" : "").append(host.getServerType()).
                    append(":").
                    append(host.getUid()).
                    append(":").
                    append(host.getAlias()).
                    append("/");
                //if (host.isZipped())
                //    sb.append(ROOT_ZDTS_INI_ZIP).append(":");
                sb.append(ROOT_ZDTS_INI);
            }
        }
        return sb.toString();
    }
    
    private String getRootZdtsIni_3_2(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts) {
            if (host.isRootNode()){
                sb.append(sb.length()>0 ? ";" : "").append(host.getUid()).
                    append(":").
                    append(host.getAlias()).
                    append("/");
                sb.append(ROOT_ZDTS_INI);
            }
        }
        return sb.toString();
    }
    
    
    protected String getServerTypes(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts){
            sb.append(sb.length()>0 ? ";" : "").append(host.getUid()).append(":").append(carouselTypes[0].toString());
            for (int i=1;i<carouselTypes.length;i++)
                sb.append(",").append(carouselTypes[i].toString());
        }
        return sb.toString();
    }
    
    protected String getServerModes(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts)
            sb.append(sb.length()>0 ? ";" : "").append(host.getUid()).append(":").append(dalinc ? DALINC : host.getServerMode());
        return sb.toString();
    }
    
    
    /**
     * only for 3.0 version
     * @return 
     */
    public Map<String,String> getCarouselsUid(){
        Map<String,String> result = new HashMap<>();
        for (CarouselHost host: hosts)
            result.put(String.format("carousel.uid.%s",host.getUid()), host.getAlias());
        return result;
    }
   
}
