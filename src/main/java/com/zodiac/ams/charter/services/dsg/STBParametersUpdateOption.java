package com.zodiac.ams.charter.services.dsg;

/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public class STBParametersUpdateOption {

    private String name;
    private String columnName;
    private String value;
    private String defaultValue;
    private String[] range;


    public STBParametersUpdateOption(String name, String columnName, String values, String defaultValue, String[] range) {
        this.name = name;
        this.columnName = columnName;
        this.value = values;
        this.defaultValue = defaultValue;
        this.range = range;
    }

    public STBParametersUpdateOption() {
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String[] getRange() {
        return range;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static STBParametersUpdateOptionCollection create() {
        return new STBParametersUpdateOptionCollection();
    }

}
