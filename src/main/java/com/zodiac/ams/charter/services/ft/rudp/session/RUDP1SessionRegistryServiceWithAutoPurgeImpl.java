package com.zodiac.ams.charter.services.ft.rudp.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class RUDP1SessionRegistryServiceWithAutoPurgeImpl extends RUDP1SessionRegistryServiceImpl {

    private final static Logger log = LoggerFactory.getLogger(RUDP1SessionRegistryServiceWithAutoPurgeImpl.class);

    private final static long DEFAULT_IDLE_TIMEOUT_MINS = 15;

    private ScheduledExecutorService executorService;
    private boolean isClosed = false;

    public RUDP1SessionRegistryServiceWithAutoPurgeImpl() {
        this(DEFAULT_IDLE_TIMEOUT_MINS);
    }

    public RUDP1SessionRegistryServiceWithAutoPurgeImpl(long sessionIdleTimeoutMins) {
        final long sessionIdleTimeoutMillis = sessionIdleTimeoutMins*60*1000;
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new PurgeSessionsRunnable(this, sessionIdleTimeoutMillis),sessionIdleTimeoutMillis/2, sessionIdleTimeoutMillis, TimeUnit.MILLISECONDS);
        log.info("Init  RUDP1SessionRegistryServiceWithAutoPurgeImpl, sessionIdleTimeoutMillis={}", sessionIdleTimeoutMillis);
    }

    @Override
    public void close() throws Exception {
        if (!isClosed) {
            executorService.shutdownNow();
            super.close();
            isClosed = true;
            log.debug("RUDP: RUDP1SessionRegistryServiceWithAutoPurgeImpl closed...");
        } else {
            log.debug("RUDP: RUDP1SessionRegistryServiceWithAutoPurgeImpl ALREADY closed");
        }
    }

    static class PurgeSessionsRunnable<T> implements Runnable {

        private final RUDP1SessionRegistryService<T> registryService;
        private final long timeout;

        PurgeSessionsRunnable(RUDP1SessionRegistryService<T> registryService, long timeout) {
            this.registryService = registryService;
            this.timeout = timeout;
        }

        @Override
        public void run() {
            Thread.currentThread().setName("RUDP Session AutoPurger");
            log.debug("RUDP: will purge RUDP1 sessions");
            Set<T> preselectedKeys = new HashSet<>();

            final long now = System.currentTimeMillis();
            for (T key : registryService.keys()) {
                IRUDPSession session = registryService.getSession(key);
                if (session != null) {
                    if (now - session.getLastPacketTimestamp() > timeout) {
                        preselectedKeys.add(key);
                    }
                }
            }

            removeSessions(preselectedKeys);
        }

        private void removeSessions(Collection<T> keys) {
            log.trace("Going to remove RUDP1 sessions: {}", keys.size());
            for (T key : keys) {
                removeSession(key);
            }
        }

        private void removeSession(T key) {
            IRUDPSession session = registryService.getSession(key);
            if (session != null) {
                if (System.currentTimeMillis() - session.getLastPacketTimestamp() > timeout) {
                    try {
                        session.close();
                    } catch (Exception e) {
                        log.error("Error while closing session, id={}", session.getSessionId(), e);
                    }
                    registryService.removeSession(key);
                }
            }
        }
    }
}
