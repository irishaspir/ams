package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import java.util.ArrayList;

public class EraseResponse implements IDVRObject{
    private Object errorCode = Error.Error0.getCodeSTB();;
    private SpaceStatus spaceStatus;
    private List<EraseResponseParam> params;
    private Integer count;
    
    public EraseResponse(EraseResponse response){
        this.errorCode = response.errorCode;
        this.spaceStatus = new SpaceStatus(response.spaceStatus);
        this.count = count;
        if (null != response.params){
            this.params = new ArrayList<>();
            for (EraseResponseParam param: response.params)
                this.params.add(new EraseResponseParam(param));
        }
    }
    
    public EraseResponse(EraseRequest request, Error error){
        List<EraseRequestParam> rparams = request.getParams();
        if (null != rparams){
            params = new ArrayList<>();
            for (EraseRequestParam rparam: rparams)
                params.add(new EraseResponseParam(rparam.getRecordingId(), error));
        }
        spaceStatus = SpaceStatus.getRandom();
    }
    
    public SpaceStatus getSpaceStatus(){
        return spaceStatus;
    }
    
    public void setCount(Integer count){
        this.count = count;
    }
    
    public void setSpaceStatus(SpaceStatus spaceStatus){
        this.spaceStatus = spaceStatus;
    }
    
    public List<EraseResponseParam> getParams(){
        return params;
    }
    
    public EraseResponseParam getParam(int index){
        return params.get(index);
    }
    
    public void setErrorCode(Object errorCode){
        this.errorCode = errorCode;
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json =  new JSONObject();
        if (null != params){
            JSONArray arr =new JSONArray();
            for (EraseResponseParam param : params)
                arr.put(param.toJSON());
            json.put("result", arr);
        }
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            if (errorCode instanceof Integer)
                out.write(DOB7bitUtils.encodeUInt((Integer)errorCode));
            if (null != params || (null != count && -1 != count))
                out.write(DOB7bitUtils.encodeUInt(null == count ? params.size() : count));
            if (null != params){
                for (EraseResponseParam param: params)
                    out.write(param.getData());
            }
            if (null != spaceStatus)
                out.write(spaceStatus.getData());
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
