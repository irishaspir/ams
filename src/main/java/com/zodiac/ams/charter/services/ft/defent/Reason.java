package com.zodiac.ams.charter.services.ft.defent;

public enum Reason {
    AVN_SRV_NA(180),
    AVN_APP_NA(181),
    AVN_SETUP_TIME(182),
    AVN_SRVRES_NA(183),
    AVN_SRV_SETUPERR(184),
    AVN_NO_TUNEINFO(185),
    AVN_INV_PAR(186),
    AVN_TUNE_FAIL(187),
    AVN_TSID_MISM(188),
    AVN_STRM_LOST(189),
    AVN_CL_HB(190),
    AVN_SRV_ERR(191),
    AVN_SRV_TIME(192),
    AVN_SRV_HB(193),
    AVN_SRV_QUAR(194),
    AVN_CL_INTL(195),
    AVN_MSG_FAIL(196),
    AVN_NET_BAD(197);

    private final int code;

    private Reason(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
    
    public String toReasonString(){
        return toString().replace("_", "-");
    }
}
