package com.zodiac.ams.charter.services.dsg;

/**
 * Created by SpiridonovaIM on 06.06.2017.
 */
public class MessageTypesMapOption {

    private String name;
    private String columnName;
    private String value;
    private String defaultValue;
    private String[] range;


    public MessageTypesMapOption(String name, String columnName, String values, String defaultValue, String[] range) {
        this.name = name;
        this.columnName = columnName;
        this.value = values;
        this.defaultValue = defaultValue;
        this.range = range;
    }

    public MessageTypesMapOption() {
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String[] getRange() {
        return range;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static MessageTypesMapOption create() {
        return new MessageTypesMapOption();
    }


}
