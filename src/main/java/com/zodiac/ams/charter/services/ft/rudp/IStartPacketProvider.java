package com.zodiac.ams.charter.services.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;

public interface IStartPacketProvider {
    
    StartPacket get(StartPacket packet);
}
