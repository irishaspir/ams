package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum Status {
    MANUALLY_CANCELED(4);
    
    private final int code;
    
    private Status(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
    
    public static Status fromCode(int code){
        Status[] statuses = Status.values();
        for (Status status: statuses){
            if (code == status.getCode())
                return status;
        }
        return null;
    }
}
