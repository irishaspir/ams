package com.zodiac.ams.charter.services.dsg;


import java.util.ArrayList;
import java.util.List;


public class ConnectionModeOptionsCollection {

    private ArrayList<ConnectionModeProtocolOption> list = new ArrayList<>();

    public ConnectionModeOptionsCollection addOption(ConnectionModeProtocolOption option) {
        list.add(new ConnectionModeProtocolOption(option.getName(), option.getColumnName(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public ConnectionModeOptionsCollection addOptions(ConnectionModeProtocolOptions option) {
        list.add(new ConnectionModeProtocolOption(option.getOption().getName(), option.getOption().getColumnName(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<ConnectionModeProtocolOption> build() {
        return list;
    }

    public ConnectionModeOptionsCollection addListOptions(List<ConnectionModeProtocolOption> list) {
        this.list.addAll(list);
        return this;
    }

}
