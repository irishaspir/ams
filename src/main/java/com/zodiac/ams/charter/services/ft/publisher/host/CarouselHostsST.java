package com.zodiac.ams.charter.services.ft.publisher.host;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import java.util.HashSet;
import java.util.Set;

public class CarouselHostsST extends CarouselHosts{
    
    private final static String LIST = "st.srv.list";
    private final static String CAROUSELS = "st.carousels";
  
            
    public CarouselHostsST(FTConfig props, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        super(props, CarouselType.ST, isZipped, aLocEnabled, dalinc);
    }
    
    public CarouselHostsST(FTConfig props, CarouselType type, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        super(props, type, isZipped, aLocEnabled, dalinc);
    }
    
    public CarouselHostsST(FTConfig props, CarouselType[] types, boolean isZipped, boolean aLocEnabled, boolean dalinc){
        super(props, types, isZipped, aLocEnabled, dalinc);
    }
    
    @Override
    public String getProperty(String name){
        String value = super.getProperty(name);
        if (null != value)
            return value;
        if (LIST.equals(name))
            return getList();
        if (CAROUSELS.equals(name))
            return getCarousels();
        return null;
    }
     
    private String getList(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts)
            sb.append(sb.length()>0 ? ";" : "").append(host.getUid()).
                    append(":").
                    append(host.getUser()).
                    append(":").
                    append(host.getPwd()).
                    append("@").
                    append(host.getHost());
        return sb.toString();
    }
    
    private String getCarousels(){
        Version version = FTConfig.getInstance().getPublihserVersion();
        switch (version){
            case VERSION_1:
            case VERSION_2:
                return getCarousels2();
            default:
                return getCarousels3();
        }
    }
    
    private String getCarousels2(){
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts)
            sb.append(sb.length()>0 ? ";" : "").append(host.getAlias()).
                    append(":").
                    append(host.getLocation()).
                    append(":").
                    append(host.getUid()).
                    append(":").
                    append(host.getType()).
                    append(":").
                    append("oob:Prefetch?cid=").
                    append(host.getUid()).
                    append("&mn=");
        return sb.toString();
    }
    
    /**
     * Only unique aliases are allowed
     * dncs.carousels="assets-alias:assets-oob1:bfs:assets-oob1"
     * @return 
     */
    private String getCarousels3(){
        Set<String> aliases =new HashSet<>();
        StringBuilder sb = new StringBuilder();
        for (CarouselHost host: hosts){
            String alias = host.getAlias();
            if (!aliases.add(alias))
                continue;
            sb.append(sb.length()>0 ? ";" : "").append(host.getAlias()).
                    append(":").
                    append(host.getLocation()).
                    append(":").
                    append(host.getType()).
                    append(":").
                    append("oob:Prefetch?cid=").
                    append(host.getUid()).
                    append("&mn=");
        }
        return sb.toString();
    }
    
}
