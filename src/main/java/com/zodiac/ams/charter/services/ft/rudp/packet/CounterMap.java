package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by anton on 18.05.16.
 */
public class CounterMap implements DataSerializable {

    private Map<Integer, Integer> map;
    private int count;

    public CounterMap() {
        map = new TreeMap<>();
        count = 0;
    }

    public CounterMap(int count) {
        this.map = new TreeMap<>();
        this.count = count;
    }

    public Integer get(Integer segment) {
        return map.get(segment);
    }

    public void put(Integer segment, int count) {
        map.put(segment, count);
    }

    public Map<Integer, Integer> asMap() {
        return Collections.unmodifiableMap(map);
    }

    public void removeAll(Set<Integer> segments) {
        map.keySet().removeAll(segments);
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeInt(count);
        for (int i = 0; i < count; i++) {
            final Integer integer = map.get(i);
            out.writeInt(integer != null ? integer : -1);
        }
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        count = in.readInt();
        for (int i = 0; i < count; i++) {
            int val = in.readInt();
            if (val >= 0) {
                map.put(i, val);
            }
        }
    }
}
