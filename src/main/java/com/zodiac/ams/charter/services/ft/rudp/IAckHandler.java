package com.zodiac.ams.charter.services.ft.rudp;

import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPReceiverSession;

public interface IAckHandler {
    static int SEND_DATA_ACK = 0b0001;
    static int RECEIVE_DATA  = 0b0010;
    int handleDataAck(IRUDPReceiverSession session, DataPacket packet);
    boolean handleStartAck(StartPacket packet);
}
