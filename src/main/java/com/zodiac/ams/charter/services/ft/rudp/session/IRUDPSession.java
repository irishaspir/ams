package com.zodiac.ams.charter.services.ft.rudp.session;

import com.hazelcast.nio.serialization.DataSerializable;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public interface IRUDPSession extends DataSerializable, AutoCloseable {
    InetAddress getRemoteAddress();
    int getSessionId();
    int getDataSize();
    int getSegmentSize();
    int getSegmentCount();
    long getLastPacketTimestamp();

    @Deprecated
    void setSessionKey(SessionKey key);
}
