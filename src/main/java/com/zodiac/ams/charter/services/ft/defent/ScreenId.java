package com.zodiac.ams.charter.services.ft.defent;

public enum ScreenId {
    LOST(11),
    OV_NR_OD(18),
    OV_NR_MENU(20),
    OV_NR_GUIDE(22),
    OV_TUNE_FAIL(41),
    OV_NR_INFO(43),
    OV_PPV_NA(46),
    OV_IU_NA(47),
    OV_NR_SET(78),
    OV_NR_YT(89),
    OV_CH_NA(90);

    private final int code;
    
    private ScreenId(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
    
    public String toScreenIdString(){
        return this.toString().replace("_","-");
    }
}
