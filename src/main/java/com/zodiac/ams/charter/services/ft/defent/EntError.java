package com.zodiac.ams.charter.services.ft.defent;

public class EntError {
    private final String error;
    private final int code;
    private final String description;
    private final int group;
    private final String comment;
    
    public EntError(String error, int code, String description, int group, String comment){
        this.error = error;
        this.code = code;
        this.description = description;
        this.group = group;
        this.comment = comment;
    }
    
    public String getError(){
        return error;
    }
    
    public int getCode(){
        return code;
    }
    
    public String getDescription(){
        return description;
    }
    
    public int getGroup(){
        return group;
    }
    
    public String getComment(){
        return comment;
    }
}
