package com.zodiac.ams.charter.services.ft.rudp.session;

import com.zodiac.ams.charter.services.ft.rudp.packet.AckDataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.AckStartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.ErrorPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * RUDP Sender Session - session used when AMS server send messages to STB.
 */
public class RUDP1SenderSession extends AbstractRUDP1Session implements IRUDPSenderSession {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final byte[] dataBytes;
    private final AtomicBoolean ackStartRef = new AtomicBoolean();
    private final ConcurrentMap<Integer, Integer> segmentsToBeAcknowledgedSet = new ConcurrentHashMap<>();
    private final AtomicInteger startPacketRetries = new AtomicInteger(0);
    private final AtomicBoolean continueSendPacketsFlag = new AtomicBoolean(true);
    private volatile long lastStartPacketSendTime = 0;
    private ErrorPacket errorPacket;

    public RUDP1SenderSession(InetSocketAddress remoteAddress, int sessionId, byte[] dataBytes, int segmentSize) {
        super(remoteAddress, sessionId, dataBytes.length+4, segmentSize);
        this.dataBytes = dataBytes;
        fillSegsmentsToBeAcknowledged();
    }

    private void fillSegsmentsToBeAcknowledged() {
        for (int i=0; i<getSegmentCount(); i++) {
            segmentsToBeAcknowledgedSet.put(i, 0);
        }
    }

    @Override
    public synchronized void clearStartAckReceived(){
        ackStartRef.set(false);
        lastStartPacketSendTime =0;
    }
     
    @Override
    public void startAckReceived(boolean b) {
        ackStartRef.set(b);
    }

    @Override
    public boolean startAckReceived() {
        return ackStartRef.get();
    }

    @Override
    public void dataAckReceived(AckDataPacket ackDataPacket) {
        segmentsToBeAcknowledgedSet.keySet().removeAll(ackDataPacket.getReceivedSegments());
    }
    
    @Override
    public boolean ackReceived(int segment){
        return !segmentsToBeAcknowledgedSet.containsKey(segment);
    }
    
    @Override
    public void clearAckReceived(int segment){
        segmentsToBeAcknowledgedSet.put(segment, 0);
        
    }

    @Override
    public boolean allAcksReceived() {
        return segmentsToBeAcknowledgedSet.isEmpty();
    }

    @Override
    public boolean continueSendPackets() {
        return continueSendPacketsFlag.get();
    }

    @Override
    public byte[] getDataBytes() {
        return dataBytes;
    }

    @Override
    public boolean isLastSegment(int segmentNumber) {
        return segmentNumber == getSegmentCount()-1;
    }

    @Override
    public Map<Integer, Integer> getSegmentsToBeAcknowledged() {
        return Collections.unmodifiableMap(segmentsToBeAcknowledgedSet);
    }
    
    @Override
    public Map<Integer,Integer> getSegmentsToBeAcknowledged(int segment) {
        Map<Integer,Integer> map=new HashMap<>();
        Integer retries = segmentsToBeAcknowledgedSet.get(segment);
        if (null != retries)
            map.put(segment,retries);
        return Collections.unmodifiableMap(map);
    }

    @Override
    public int incrStartPacketRetries() {
        return startPacketRetries.incrementAndGet();
    }

    @Override
    public int getStartPacketRetries() {
        return startPacketRetries.get();
    }

    @Override
    public void incrDataPacketRetries(Set<Integer> segments) {
        for (Integer segment : segments) {
            Integer count = segmentsToBeAcknowledgedSet.get(segment);
            if(count != null) {
                segmentsToBeAcknowledgedSet.put(segment, count + 1);
            }
        }
    }
    
    @Override
    public void incrDataPacketRetries(int segment){
        Integer count = segmentsToBeAcknowledgedSet.get(segment);
        if (null != count)
            segmentsToBeAcknowledgedSet.put(segment, count + 1);
    }

    @Override
    public SessionKey getSessionKey() {
        return new SessionKey(getRemoteAddress(), getSessionId());
    }

    @Override
    public synchronized void setErrorPacket(ErrorPacket errorPacket) {
        if (null == this.errorPacket)
            this.errorPacket = errorPacket;
        log.debug("Get Error packet {}, stop sending packets", errorPacket);
        continueSendPacketsFlag.set(false);
    }

    @Override
    public ErrorPacket getErrorPacket(){
        return errorPacket;
    }
    
    @Override
    public void markStartPacketSendtime() {
        lastStartPacketSendTime = System.currentTimeMillis();
    }

    @Override
    public long getLastStartPacketTimesent() {
        return lastStartPacketSendTime;
    }

    @Override
    public void close() throws Exception {
        // do nothing
    }
}
