package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomObject;
import static com.zodiac.ams.charter.services.ft.dvr.DVRParam.random;
import static com.zodiac.ams.charter.services.ft.dvr.JSONUtils.getInt;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.WeekDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class ModifyRequest extends DVRParam implements IDVRObject{
    
    private String deviceId;
    private Method method =  Method.MODIFY;
    private YesNo wholeSetFlag;
    private Integer mask;
    
    private static final int SERVICEID_MASK = 1 << 0;
    private static final int STARTTIME_MASK = 1 << 1; 
    private static final int DURATION_MASK = 1<<2;
    //saveDays, recordingFormat, episodesDefinition, recordDuplicates
    private static final int UNITED_MASK = 1 << 3;
    private static final int STARTOFFSET_MASK =  1 << 4;
    private static final int ENDOFFSET_MASK = 1 << 5;
    private static final int UNITINGPARAMETER_MASK = 1 << 8;
    private static final int REPEAT_MASK = 1 << 12; 
    private static final int CHANNELNUMBER_MASK =  1 << 13; //mandatory!!!
    
    public ModifyRequest(){
    }
    
    public ModifyRequest(Method method){
        this.method = method;
    }
    
    public ModifyRequest(String deviceId, Integer recordingId, Integer channelNuber){
        this.deviceId = deviceId;
        this.recordingId = recordingId;
        this.channelNumber = channelNuber;
    }
    
    public ModifyRequest(DVRParam param){
        super(param);
    }
    
    public ModifyRequest(ModifyRequest request){
        super(request);
        if (null == request)
            return;
        this.deviceId = request.deviceId;
        this.method =  request.method;
        this.wholeSetFlag = request.wholeSetFlag;
        this.mask = request.mask;
    }
    
    public void setDeviceId(String deviceId){
        this.deviceId = deviceId;
    }
    
    public void setRecordingId(Integer recordingId){
        this.recordingId = recordingId;
    }
    
    public void truncateToDvr(){
        this.deviceId =  null;
        this.method = null;
        this.wholeSetFlag = null;
        this.mask = null;
    }
    
    public void truncate(ModifyRequest template){
        if (null == template)
            return;
        super.truncate(template);
        if (null == template.deviceId)
            this.deviceId = null;
        if (null == template.method)
            this.method = null;
        if (null == template.wholeSetFlag)
            this.wholeSetFlag = null;
        if (null == template.mask)
            this.mask = null;
    }
    
    public static ModifyRequest getTruncated(ModifyRequest source, ModifyRequest template){
        ModifyRequest target =  new ModifyRequest(source);
        target.truncate(template);
        return target;
    }
    
    public static ModifyRequest getRandom(){
        ModifyRequest rec = new ModifyRequest();
        rec.random(getRandomObject(Type.values()), true/*modify*/);
        rec.calculateMask();
        return rec;
    }
    
    public static ModifyRequest getRandom(String deviceId, Integer recordingId, Type type){
        ModifyRequest rec = new ModifyRequest();
        rec.random(type, true/*modify*/);
        rec.calculateMask();
        rec.deviceId = deviceId;
        rec.recordingId = recordingId;
        rec.wholeSetFlag = getRandomObject(YesNo.values());
        return rec;
    }
    
    public static ModifyRequest getRandom(String deviceId, Integer recordingId){
        return getRandom(deviceId, recordingId, getRandomObject(Type.values()));
    }
    
    
    public void calculateMask(){
        int mask = 0;
        if (null != serviceId)
            mask =mask | SERVICEID_MASK;
        if (null != startTime)
            mask = mask | STARTTIME_MASK;
        if (null != duration)
            mask = mask | DURATION_MASK;
        if (isUnitedFlag())
            mask = mask | UNITED_MASK;
        if (null != startOffset)
            mask = mask | STARTOFFSET_MASK;
        if (null != endOffset)
            mask = mask | ENDOFFSET_MASK;
        if (null != unitingParameter)
            mask = mask | UNITINGPARAMETER_MASK;
        if (null != repeat)
            mask = mask | REPEAT_MASK;
        if (null != channelNumber)
            mask = mask | CHANNELNUMBER_MASK;
        this.mask = mask;
    }
    
    private boolean isUnitedFlag(){
        return null != saveDays || null != recordingFormat || null != episodesDefinition || null != recordDuplicates;
    }
    
    public int getMask(){
        return mask;
    }
    
    public void setMask(int mask){
        this.mask =  mask;
    }
    
    public void setWholeSetFlag(YesNo wholeSetFlag){
        this.wholeSetFlag = wholeSetFlag;
    }
    
    public JSONObject getJsonDvr(){
        return getJsonDvr(null);
    }
    
    public void setMethod(Method method){
        this.method = method;
    }
    
    public JSONObject getJsonDvr(Set<String> excluded){
        return super.toJSON(excluded);
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONUtils.setField(json, "deviceId", deviceId);
        JSONUtils.setField(json, "method", method);
        JSONArray arr = new JSONArray();
        JSONObject obj = getJsonDvr(excluded);
        arr.put(obj);
        json.put("params", arr);
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(((Method)method).getCode()); 
            out.write(DOB7bitUtils.encodeUInt(null == type ? 0 : type.getCode()));
            out.write(DOB7bitUtils.encodeUInt(null == wholeSetFlag ? 1 : wholeSetFlag.getCode()));
            
            out.write((mask >> 8) & 0xFF);
            out.write(mask & 0xFF);

            out.write(DOB7bitUtils.encodeUInt(getInt(recordingId)));
            
            if (null != serviceId)
                out.write(DOB7bitUtils.encodeUInt((Integer)serviceId));
            if (null != startTime)
                out.write(DOB7bitUtils.encodeBigInt((Integer)startTime));
            if (null != duration)
                out.write(DOB7bitUtils.encodeUInt((Integer)duration));
            if (this.isUnitedFlag())
                out.write(DvrMessageUtils.getUnitedFlagsFromObj(saveDays, recordingFormat, episodesDefinition, recordDuplicates));
            if (null != startOffset)
                out.write(DOB7bitUtils.encodeUInt((Integer)startOffset));
            if (null != endOffset) 
                out.write(DOB7bitUtils.encodeUInt((Integer)endOffset));
            if (null != unitingParameter){
                if (Type.Series == type)
                    out.write(DOB7bitUtils.encodeUInt(DvrMessageUtils.buildUnitingParam(unitingParameter)));
                else
                    out.write(DOB7bitUtils.encodeUInt(0));
            }
            if (null != repeat)
                out.write(DOB7bitUtils.encodeUInt(WeekDays.getWeekDaysMask((WeekDays[])repeat)));
            if (null != channelNumber)
                out.write(DOB7bitUtils.encodeUInt((Integer)channelNumber));
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
}
