package com.zodiac.ams.charter.services.ft.ppv;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class PPVUtils {
    private static final SimpleDateFormat DATE_FORMAT =
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    
    public static String getDate(){
        return DATE_FORMAT.format(new Date());
    }
    
    public static String getDate(long date){
        return DATE_FORMAT.format(new Date(date));
    }
    
}
