package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

public class DataPacketSerializer {

    private static final Logger log = LoggerFactory.getLogger(StartPacketSerializer.class);

    public static DataPacket parse(byte[] bytes) {
        try {
            // byte number 0 is packet type, we aren't interested in it
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes, 1, bytes.length);
            int[] a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
            // 7-bit int session ID
            int fileId = a[0];

            bais = new ByteArrayInputStream(bytes, 1 + a[1], bytes.length);
            int[] b = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
            // 7-bit int segment number
            int segmentNumber = b[0];

            //System.out.println("fileId="+fileId);
            //System.out.println("segmentNumber="+segmentNumber);
            int offset = 1 + a[1] + b[1];
            // this array will also contain 4 bytes of CRC32 if this is the last segment packet
            byte[] data = new byte[bytes.length - offset];
            System.arraycopy(bytes, offset, data, 0, bytes.length - offset);

            return new DataPacket(fileId, segmentNumber, data);
        } catch (Throwable t) {
            log.error("Error when parsing DATA packet, bytes: {}", Arrays.toString(bytes));
            return null;
        }
    }

    public static byte[] toBytes(DataPacket p) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);

            baos.write(PacketType.DATA.packetType);

            {
                byte[] fileIdBytes = DOB7bitUtils.encodeUInt(p.getFileId());
                baos.write(fileIdBytes);
            }

            {
                byte[] segmentNumberBytes = DOB7bitUtils.encodeUInt(p.getSegmentNumber());
                baos.write(segmentNumberBytes);
            }

            baos.write(p.getData());

            return baos.toByteArray();
        } catch (IOException ioe) {
            throw new IllegalStateException("This should not happen");
        }
    }

    public static AckDataPacket parseAck(byte[] bytes) {
        try {
            PacketType pt = PacketType.fromByte(bytes[0]);

            if (pt == PacketType.ACK) {
                PacketType ackPt = PacketType.fromByte(bytes[1]);
                if (ackPt == PacketType.DATA) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(bytes, 2, bytes.length);
                    DOBCountingInputStream is = new DOBCountingInputStream(bais);
                    int[] a = DOB7bitUtils.decodeUInt(is);
                    int fileId = a[0];

                    BitSet bitMask = new BitSet((bytes.length - 2) * 7);
                    int nextDataByte;
                    int i = 0;
                    while ((nextDataByte = is.read()) != -1) {
                        for (int j = 0; j < 7; j++) {
                            bitMask.set(i * 7 + j, (byte) (((byte) nextDataByte >> j) & 0x1) == 0x1);
                        }
                        if (((byte) nextDataByte & 0x80) != 0x80) {
                            break;
                        }
                        i++;
                    }
                    if (nextDataByte == -1) {
                        log.error("Error when parsing ACK-DATA packet, bytes: {}", Arrays.toString(bytes));
                        return null;
                    }
                    return new AckDataPacket(fileId, bitset2set(bitMask));
                } else {
                    log.error("Not ACK-DATA packet, bytes: {}", Arrays.toString(bytes));
                    return null;
                }
            } else {
                log.error("Not ACK packet, bytes: {}", Arrays.toString(bytes));
                return null;
            }
        } catch (Throwable t) {
            log.error("Error when parsing ACK-DATA packet, bytes: {}", Arrays.toString(bytes));
            return null;
        }
    }
    
// Before Incorrect encode/decode of segments bitmask in ACK-DATA packets  [ AMSRUDP-9  # 117876 ]     
//    public static AckDataPacket parseAck(byte[] bytes) {
//        try {
//            PacketType pt = PacketType.fromByte(bytes[0]);
//
//            if (pt == PacketType.ACK) {
//                PacketType ackPt = PacketType.fromByte(bytes[1]);
//                if (ackPt == PacketType.DATA) {
//                    ByteArrayInputStream bais = new ByteArrayInputStream(bytes, 2, bytes.length);
//                    DOBCountingInputStream is = new DOBCountingInputStream(bais);
//                    int[] a = DOB7bitUtils.decodeUInt(is);
//                    int fileId = a[0];
//
//                    long maskBytes = DOB7bitUtils.decodeUInt(is)[0];
//                    BitSet bitMask = BitSet.valueOf(new long[]{maskBytes});
//
//                    return new AckDataPacket(fileId, bitset2set(bitMask));
//                } else {
//                    log.error("Not ACK-DATA packet, bytes: {}", Arrays.toString(bytes));
//                    return null;
//                }
//            } else {
//                log.error("Not ACK packet, bytes: {}", Arrays.toString(bytes));
//                return null;
//            }
//        } catch (Throwable t) {
//            log.error("Error when parsing ACK-DATA packet, bytes: {}", Arrays.toString(bytes));
//            return null;
//        }
//    }

    static Set<Integer> bitset2set(BitSet bs) {
        Set<Integer> set = new HashSet<>(bs.length() * 2);
        for (int i = 0; i < bs.length(); i++) {
            if (bs.get(i)) {
                set.add(i);
            }
        }
        return set;
    }
}
