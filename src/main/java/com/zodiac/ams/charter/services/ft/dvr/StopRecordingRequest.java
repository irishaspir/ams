package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.KeepPartial;
import java.util.ArrayList;


public class StopRecordingRequest implements IDVRObject{
    
    private Object deviceId;
    private Object method =  Method.STOP;
    private List<StopRecordingRequestParam> params;
    
    public StopRecordingRequest(String deviceId){
        this.deviceId = deviceId;
    }
    
    public StopRecordingRequest(String deviceId, Integer recordingId, KeepPartial keepPartial){
        this.deviceId = deviceId;
        this.params = new ArrayList<>();
        this.params.add(new StopRecordingRequestParam(recordingId, keepPartial));
    }
    
    public StopRecordingRequest(StopRecordingRequest request){
        this.deviceId = request.deviceId;
        this.method = request.method;
        if (null != request.params){
            this.params = new ArrayList<>();
            for (StopRecordingRequestParam param: request.params)
                this.params.add(new StopRecordingRequestParam(param));
        }
    }
    
    public static StopRecordingRequest getRandom(String deviceId){
        return getRandom(deviceId, RandomUtils.getRandomInt(1, 5));
    }
    
    public static StopRecordingRequest getRandom(String deviceId, int size){
        StopRecordingRequest request = new StopRecordingRequest(deviceId); 
        request.params = new ArrayList<>();
        for (int i=0; i<size; i++)
            request.params.add(StopRecordingRequestParam.getRandom());
        return request;
    }
    
    public List<StopRecordingRequestParam> getParams(){
        return params;
    }
    
    public int size(){
        return null == params ? 0 : params.size();
    }
    
    public StopRecordingRequestParam getParam(int index){
        return null == params ? null : params.get(index);
    }
    
    public void addParam(StopRecordingRequestParam param){
        if (null == params)
            params = new ArrayList<>();
        params.add(param);
    }

    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        JSONUtils.setField(json, "deviceId", deviceId);
        JSONUtils.setField(json, "method", method);
        if (null != params){
            JSONArray arr = new JSONArray();
            for (StopRecordingRequestParam param: params)
                arr.put(param.toJSON(excluded));
            json.put("params", arr);
        }
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            if (null != method){
                if (method instanceof Method)
                    out.write(DOB7bitUtils.encodeUInt(((Method)method).getCode()));
                else
                    out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(method)));
            }
            if (null != params){
                out.write(DOB7bitUtils.encodeUInt(params.size()));
                for (StopRecordingRequestParam param: params)
                    out.write(param.getData());
            }
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
