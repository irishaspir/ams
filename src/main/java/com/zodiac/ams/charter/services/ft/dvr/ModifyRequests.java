package com.zodiac.ams.charter.services.ft.dvr;

import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ModifyRequests {
    private final List<ModifyRequest> requests = new ArrayList<>();
    
    private String deviceId;
    private Method method =  Method.MODIFY;
    
    public ModifyRequests(String deviceId){
        this.deviceId = deviceId;
    }
    
    public void add(ModifyRequest request){
        requests.add(request);
    }
    
    public JSONObject toJSON(){
        JSONObject json =new JSONObject();
        json.put("deviceId", deviceId);
        json.put("method", method);
        JSONArray arr = new JSONArray();
        for (ModifyRequest request: requests)
            arr.put(request.getJsonDvr());
        json.put("params", arr);
        return json;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
    public List<ModifyRequest> getRequests(){
        return requests;
    }
}
