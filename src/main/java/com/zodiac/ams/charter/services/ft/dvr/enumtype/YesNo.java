package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum YesNo {
    Yes(2), No(1);
    
    private final int code;
    
    private YesNo(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
    
    public static YesNo fromString(String string){
        YesNo[] yns = YesNo.values();
        for (YesNo yn: yns){
            if (yn.toString().equals(string))
                return yn;
        }
        return null;
    }
    
    public static YesNo fromCode(int code){
        YesNo[] yns = YesNo.values();
        for (YesNo yn: yns){
            if (code == yn.code)
                return yn;
        }
        return null;
    }
}
