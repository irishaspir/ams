package com.zodiac.ams.charter.services.ft.defent.dsg;

import com.dob.ams.util.DOB7bitCoder;
import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import static com.zodiac.ams.charter.services.ft.defent.dsg.DSGUtils.string2ip;
import java.io.ByteArrayOutputStream;
import org.json.JSONObject;
import static com.zodiac.ams.charter.services.ft.defent.dsg.Constants.*;
import org.json.JSONArray;

public class DSGMessage30 extends DSGMessage{
    private boolean truncated = false;
    private int fieldsCount = 3;
    
    public static DSGMessage30 getRandom(String mac){
        ConnectionMode connectionMode = RandomUtils.getRandomObject(ConnectionMode.values());
        int boxModel = RandomUtils.getRandomInt(1, 100);
        Vendor vendor = RandomUtils.getRandomObject(Vendor.values());
        long messageTypesMap = 0;
        boolean migrationStart = RandomUtils.getRandomBoolean();
        int hubId = RandomUtils.getRandomInt(1, 100);
        int serviceGroupId = RandomUtils.getRandomInt(1, 100);
        String loadBalancerIpAddress = RandomUtils.getRandomIp();
        int swVersion = RandomUtils.getRandomInt(1, 100);
        int sdvServiceGroupId = RandomUtils.getRandomInt(1, 100);
        long cmMacAddress = RandomUtils.getRandomInt(1_000_000, 2_000_000);
        String serialNumber = String.valueOf(RandomUtils.getRandomInt(1_000_000, 2_000_000));
        String cmIp = RandomUtils.getRandomIp();
        return new DSGMessage30(mac, connectionMode, boxModel, vendor, messageTypesMap, migrationStart, hubId, serviceGroupId,
            loadBalancerIpAddress, swVersion, sdvServiceGroupId, cmMacAddress, serialNumber, cmIp);
    }
    
    public void setTruncated(boolean truncated){
        this.truncated = truncated;
    }
    
    @Override
    public void setVersion(int version){
        this.version = version;
    }
    
    public void setFieldsCount(int fieldsCount){
        this.fieldsCount = fieldsCount;
    }
    
    public int getFiledsCount(){
        return fieldsCount;
    }
    
    private DSGMessage30(String mac, ConnectionMode connectionMode, int boxModel, Vendor vendor, long messageTypesMap,
        boolean migrationStart, int hubId, int serviceGroupId, String loadBalancerIpAddress,
        int swVersion, int sdvServiceGroupId, long cmMacAddress, String serialNumber, String cmIp){
        
        super((byte)0, mac);
        
        this.connectionMode = connectionMode;
        this.boxModel = boxModel;
        this.vendor = vendor;
        this.messageTypesMap = messageTypesMap;
        this.migrationStart = migrationStart;
        this.hubId = hubId;
        this.serviceGroupId = serviceGroupId;
        this.loadBalancerIpAddress = loadBalancerIpAddress;
        this.swVersion = swVersion;
        this.sdvServiceGroupId = sdvServiceGroupId;
        this.cmMacAddress = cmMacAddress;
        this.serialNumber = serialNumber;
        this.cmIp = cmIp;
    }
    
    
    
    
    
    @Override
    public byte[] getMessage() throws Exception {
        byte [] result = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(requestId);
            baos.write(DOB7bitUtils.encodeUInt(null == connectionMode  ? INVALID_CONNECTION : connectionMode.ordinal()));
            if ((0 == version && fieldsCount > 1) || version >= 1)
                baos.write(DOB7bitUtils.encodeUInt(boxModel));
            if ((0 == version && fieldsCount > 2) || version >= 1)
                baos.write(DOB7bitUtils.encodeUInt(vendor.getCode()));
            if (version >= 1)
                baos.write(DOB7bitUtils.encodeLong(messageTypesMap));
            if (version >= 2)
                baos.write(migrationStart ? 1 : 0);
            if (version >= 3){
                baos.write(DOB7bitUtils.encodeInt(hubId));
                baos.write(DOB7bitUtils.encodeInt(serviceGroupId));
                baos.write(DOB7bitCoder.encodeBigInt((null == loadBalancerIpAddress) ? 0 : string2ip(loadBalancerIpAddress)));
                baos.write(DOB7bitUtils.encodeUInt(swVersion));
                baos.write(DOB7bitUtils.encodeInt(sdvServiceGroupId));
                baos.write(encodeMac(cmMacAddress));
                baos.write(DOB7bitUtils.encodeString((null == serialNumber) ? "" : serialNumber));
                if (!truncated){
                    baos.write(DOB7bitCoder.encodeBigInt((null == cmIp) ? 0 : string2ip(cmIp)));
                }
            }
            baos.flush();
            result = baos.toByteArray();
        }
        return result;
    }    
    
    @Override
    public JSONObject toJSON() {
        JSONArray arr =new JSONArray();
        
        JSONObject o1 = new JSONObject();
        o1.put("name", MAC);
        o1.put("value", mac);
        arr.put(o1);
        
        o1 = new JSONObject();
        o1.put("name", Constants.CONNECTIONMODE);
        o1.put("value", connectionMode);
        arr.put(o1);
        
        if (0 == version){
            
            
            if (fieldsCount >= 2){
                o1 = new JSONObject();
                o1.put("name", MODEL);
                o1.put("value", String.valueOf(boxModel));
                arr.put(o1);
            }
            
            if (this.fieldsCount >= 3){
                o1 = new JSONObject();
                if (Vendor.Cisco != vendor) {
                    o1.put("name", VCTID);
                    o1.put("value", String.valueOf(hubId));
                } else {
                    o1.put("name", HUBID);
                    o1.put("value", String.valueOf(hubId));
                }
                arr.put(o1);
            }
        }
        
        if (version >= 3){
            o1 = new JSONObject();
            if (Vendor.Cisco != vendor) {
                o1.put("name", VCTID);
                o1.put("value", String.valueOf(hubId));
            } else {
                o1.put("name", HUBID);
                o1.put("value", String.valueOf(hubId));
            }
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", "SGID");
            o1.put("value", String.valueOf(serviceGroupId));
            arr.put(o1);
            o1 = new JSONObject();
            o1.put("name", "TSID");
            o1.put("value", String.valueOf(serviceGroupId));
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", AMSIP);
            o1.put("value", loadBalancerIpAddress);
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", SW);
            o1.put("value", String.valueOf(swVersion));
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", SDVSGID);
            o1.put("value", String.valueOf(sdvServiceGroupId));
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", CMMAC);
            o1.put("value", String.format("%012X", cmMacAddress));
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", SN);
            o1.put("value", serialNumber);
            arr.put(o1);

            o1 = new JSONObject();
            o1.put("name", CMIP);
            o1.put("value", this.cmIp);
            arr.put(o1);
            o1 = new JSONObject();
            o1.put("name", HOSTRFIP);
            o1.put("value", this.cmIp);
            arr.put(o1);
        }
        
        JSONObject o2 = new JSONObject();
        o2.put("options", arr);
        
        JSONObject o3 = new JSONObject();
        o3.put("attributes", o2);
        
        JSONObject o4 = new JSONObject();
        o4.put("settings", o3);
        
        return o4;
    }
}
