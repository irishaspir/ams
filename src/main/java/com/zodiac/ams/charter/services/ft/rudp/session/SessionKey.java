package com.zodiac.ams.charter.services.ft.rudp.session;

import com.google.common.base.Objects;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

/**
* Created by iryndin on 16.08.14.
*/
public class SessionKey implements DataSerializable{

    private InetAddress remoteAddress;
    private int fileId;

    public SessionKey() {
    }

    public SessionKey(InetSocketAddress remoteAddress, int fileId) {
        this(remoteAddress.getAddress(), fileId);
    }

    public SessionKey(InetAddress remoteAddress, int fileId) {
        this.remoteAddress = remoteAddress;
        this.fileId = fileId;
    }

    public InetAddress getRemoteAddress() {
        return remoteAddress;
    }

    public int getFileId() {
        return fileId;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SessionKey sessionKey = (SessionKey) o;

        return Objects.equal(this.fileId, sessionKey.fileId) &&
                Arrays.equals(this.remoteAddress.getAddress(), sessionKey.remoteAddress.getAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(Arrays.hashCode(remoteAddress.getAddress()), fileId);
    }

    @Override
    public String toString() {
        return "SessionKey{" +
                "remoteAddress=" + remoteAddress +
                ", fileId=" + fileId +
                ", hashCode=" + hashCode() +
                '}';
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeByteArray(remoteAddress.getAddress());
        out.writeInt(fileId);
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        remoteAddress = InetAddress.getByAddress(in.readByteArray());
        fileId = in.readInt();
    }
}
