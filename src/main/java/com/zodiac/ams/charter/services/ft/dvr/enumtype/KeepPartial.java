package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum KeepPartial {
    Keep(0), Delete(1);
    private final int code;
    
    private KeepPartial(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
}
