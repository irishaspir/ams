package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.zodiac.ams.charter.services.ft.rudp.util.NetworkOrderBitUtils;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import com.google.common.base.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Parse Error Packets
 */
public class ErrorPacketSerializer {

    private static final Logger log = LoggerFactory.getLogger(ErrorPacketSerializer.class);

    public static ErrorPacket parseErrorPacket(byte[] bytes) {
        try {
            byte packetType = bytes[0];
            PacketType pt = PacketType.fromByte(packetType);
            if (pt != PacketType.ERROR) {
                final String msg = "RUDP: packet should have ERROR type! bytes: " + Arrays.toString(bytes);
                log.error(msg);
                throw new IllegalStateException(msg);
            }
            final int crc16 = NetworkOrderBitUtils.b2i(bytes[1],bytes[2]);
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes, 3, bytes.length);
            int[] a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
            final ErrorCodeType errorCodeType = ErrorCodeType.fromInt(a[0]);
            switch (errorCodeType) {
                case NACK:
                {
                    bais = new ByteArrayInputStream(bytes, 3+a[1], bytes.length);
                    int[] b = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                    final int fileId = b[0];
                    log.debug("RUDP: get ERROR.NACK packet, fileId={}", fileId);
                    //bais = new ByteArrayInputStream(bytes, 3+a[1]+b[1], bytes.length);
                    //b = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                    //final int segmentNumber = b[0];
                    return new ErrorPacket() {
                        @Override
                        public int getCRC16() {
                            return crc16;
                        }

                        @Override
                        public ErrorCodeType getErrorCode() {
                            return ErrorCodeType.NACK;
                        }

                        @Override
                        public int getFileId() {
                            return fileId;
                        }

                        @Override
                        public String toString() {
                            return Objects.toStringHelper("ErrorPacket")
                                    .add("crc16",getCRC16())
                                    .add("errorCode",getErrorCode())
                                    .add("fileId",getFileId())
                                    .toString();
                        }
                    };
                }
                case CRC32:
                {
                    bais = new ByteArrayInputStream(bytes, 3+4+4+a[1], bytes.length);
                    int[] b = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                    final int fileId = b[0];
                    log.debug("RUDP: get ERROR.CRC32 packet, fileId={}", fileId);
                    return new ErrorPacket() {
                        @Override
                        public int getCRC16() {
                            return crc16;
                        }

                        @Override
                        public ErrorCodeType getErrorCode() {
                            return ErrorCodeType.CRC32;
                        }

                        @Override
                        public int getFileId() {
                            return fileId;
                        }
                        @Override
                        public String toString() {
                            return Objects.toStringHelper("ErrorPacket")
                                    .add("crc16",getCRC16())
                                    .add("errorCode",getErrorCode())
                                    .add("fileId",getFileId())
                                    .toString();
                        }
                    };
                }
                case RESET:
                {
                    bais = new ByteArrayInputStream(bytes, 3+1+a[1], bytes.length);
                    int[] b = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                    final int fileId = b[0];
                    log.debug("RUDP: get ERROR.RESET packet, fileId={}", fileId);
                    return new ErrorPacket() {
                        @Override
                        public int getCRC16() {
                            return crc16;
                        }

                        @Override
                        public ErrorCodeType getErrorCode() {
                            return ErrorCodeType.RESET;
                        }

                        @Override
                        public int getFileId() {
                            return fileId;
                        }
                        @Override
                        public String toString() {
                            return Objects.toStringHelper("ErrorPacket")
                                    .add("crc16",getCRC16())
                                    .add("errorCode",getErrorCode())
                                    .add("fileId",getFileId())
                                    .toString();
                        }
                    };

                }
                case MEMORY:
                case FILEID:
                {
                    bais = new ByteArrayInputStream(bytes, 3+a[1], bytes.length);
                    a = DOB7bitUtils.decodeUInt(new DOBCountingInputStream(bais));
                    final int fileId = a[0];
                    if (errorCodeType == ErrorCodeType.MEMORY) {
                        log.debug("RUDP: get ERROR.MEMORY packet, fileId={}", fileId);
                    } else if (errorCodeType == ErrorCodeType.FILEID) {
                        log.debug("RUDP: get ERROR.FILEID packet, fileId={}", fileId);
                    }
                    return new ErrorPacket() {
                        @Override
                        public int getCRC16() {
                            return crc16;
                        }

                        @Override
                        public ErrorCodeType getErrorCode() {
                            return errorCodeType;
                        }

                        @Override
                        public int getFileId() {
                            return fileId;
                        }
                        @Override
                        public String toString() {
                            return Objects.toStringHelper("ErrorPacket")
                                    .add("crc16",getCRC16())
                                    .add("errorCode",getErrorCode())
                                    .add("fileId",getFileId())
                                    .toString();
                        }
                    };
                }
                default:
                {
                    log.error("RUDP: unhandled type of ERROR packet: {}", errorCodeType);
                    return null;
                }
            }
        } catch (IOException ioe) {
            log.error("RUDP: error when parsing ERROR packet", ioe);
            return null;
        }
    }
}
