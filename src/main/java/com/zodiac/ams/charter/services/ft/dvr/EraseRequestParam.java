package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import com.zodiac.ams.charter.services.ft.RandomUtils;
import java.util.Set;

public class EraseRequestParam implements IDVRObject{
    private Object recordingId;
    private Object wholeSetFlag;
    
    public static EraseRequestParam getRandom(){
        return new EraseRequestParam(RandomUtils.getRandomInt(1, 1_000_000),
            RandomUtils.getRandomObject(YesNo.values()));
    }
    
    public int getRecordingId(){
        return JSONUtils.getInt(recordingId);
    }
    
    public void setRecordingId(Object recordingId){
        this.recordingId = recordingId;
    }
    
    public void setWholeSetFlag(Object wholeSetFlag){
        this.wholeSetFlag = wholeSetFlag;
    }
    
    public EraseRequestParam(Object recordingId, Object wholeSetFlag){
        this.recordingId = recordingId;
        this.wholeSetFlag = wholeSetFlag;
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded){
        JSONObject json = new JSONObject();
        JSONUtils.setField(json, "recordingId", recordingId);
        JSONUtils.setField(json, "wholeSetFlag", wholeSetFlag);
        return json;
    }
    
    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            if (null != recordingId)
                out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(recordingId)));
            if (null != wholeSetFlag){
                if (wholeSetFlag instanceof YesNo)
                    out.write(DOB7bitUtils.encodeUInt(((YesNo)wholeSetFlag).getCode()));
                else
                    out.write(DOB7bitUtils.encodeUInt(0));
            }
            return out.toByteArray();
        }
    }
}
