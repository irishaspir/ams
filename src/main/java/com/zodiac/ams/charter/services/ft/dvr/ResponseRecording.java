package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import org.json.JSONArray;
import org.json.JSONObject;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Error;
import java.util.Arrays;
import java.util.Set;

public class ResponseRecording implements IDVRObject{
    
    private int errorCode;
    private List<ResponseRecordingParam> params;
    private Integer size;
    private SpaceStatus spaceStatus;
    private List<Integer> priorityVector;
    private Integer prioritySize;
    private Long crc;
   
    
    public ResponseRecording(){
    }
    
    public ResponseRecording(ResponseRecording response){
        errorCode = response.errorCode;
        if (null != response.params){
            params = new ArrayList<>();
            for (ResponseRecordingParam param: response.params)
                params.add(new ResponseRecordingParam(param));
        }
        spaceStatus = response.spaceStatus;
        priorityVector = response.priorityVector;
        crc = response.crc;
    }
    
    
    public static ResponseRecording fromErrorCode(int errorCode){
        ResponseRecording rr =new ResponseRecording();
        rr.errorCode = errorCode;
        return rr;
    }
    
    public static ResponseRecording getRandom(boolean[] repeat){
        return getRandom(repeat, 0, repeat.length, 0);
    }
    
    
    public static ResponseRecording getRandom(boolean[] repeat, int errorCode, int[] errors){
        int recordingId = 0;
        ResponseRecording rr =new ResponseRecording();
        rr.errorCode = errorCode;
        for (int i=0; i < errors.length; i++){
            ResponseRecordingParam param = ResponseRecordingParam.getRandom(repeat[i], errors[i]);
            if (0 == i)
                recordingId = param.getRecordingId();
            else
                param.setRecordingId(++recordingId);
            rr.addParam(param);
        }
        rr.spaceStatus = SpaceStatus.getRandom();
        rr.priorityVector = new ArrayList<>();
        int psize = getRandomInt(0, 5);
        for (int i=0; i<psize; i++)
            rr.priorityVector.add(getRandomInt(1, 1_000_000));
        CRC32 crc = new CRC32();
        try {
            crc.update(rr.getData());
        }
        catch(IOException ex){
        }
        rr.crc = crc.getValue();
        return rr;
    }
    
    public static ResponseRecording getRandom(boolean[] repeat, int errorCode, Error [] errors){
        int [] err = new int[errors.length];
        for (int i=0; i< err.length; i++)
            err[i] = errors[i].getCodeSTB();
        return getRandom(repeat, errorCode, err);
    }
    
    public static ResponseRecording getRandom(boolean[] repeat, int errorCode, int size, int statusCode){
        int err[] = new int[size];
        Arrays.fill(err, statusCode);
        return getRandom(repeat, errorCode, err);
    }
    
    public void setSpaceStatus(SpaceStatus spaceStatus){
        this.spaceStatus = spaceStatus;
    }
    
    public void setCrc(Long crc){
        this.crc = crc;
    }
    
    public void addParam(ResponseRecordingParam param){
        if (null == params)
            params = new ArrayList<>();
        params.add(param);
    }
    
    public int getSize(){
        if (null != size)
            return size;
        return (null == params) ? 0 : params.size();
    }
    
    public void setSize(Integer size){
        this.size = size;
    }
    
    public ResponseRecordingParam getParam(int index){
        return params.get(index);
    }
    
    public void setPriority(List<Integer> priorityVector){
        this.priorityVector = priorityVector;
    }
    
    public void setPrioritySize(Integer prioritySize){
        this.prioritySize = prioritySize;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(errorCode));
            if (null != size || null != params){
                if (null != size ){
                    if (size >= 0){
                        out.write(DOB7bitUtils.encodeUInt(size));
                    }
                }
                else
                    out.write(DOB7bitUtils.encodeUInt(params.size()));
                if (null != params){
                    for (ResponseRecordingParam param: params)
                        out.write(param.getData());
                }
            }
            if (null != crc)
                out.write(DOB7bitUtils.encodeLong(crc));
            if (null != spaceStatus)
                out.write(spaceStatus.getData());
            if (null != priorityVector || null != prioritySize){
                if (null != prioritySize){
                    if (prioritySize >= 0){
                        out.write(DOB7bitUtils.encodeUInt(prioritySize));
                    }
                }
                else
                    out.write(DOB7bitUtils.encodeUInt(priorityVector.size()));
                if (null != priorityVector){
                    for (Integer i: priorityVector)
                        out.write(DOB7bitUtils.encodeUInt(i));
                }
            }
            return out.toByteArray();
        }
    }

    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json = new JSONObject();
        if (null != params){
            JSONArray arr = new JSONArray();
            for (ResponseRecordingParam param: params)
                arr.put(param.toJSON(excluded));
            json.put("result",  arr);
        }
        return json;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
