package com.zodiac.ams.charter.services.ft.defent;

import org.json.JSONObject;

public interface IGroup {
    byte [] getMessage() throws Exception;
    JSONObject toJSON();
}
