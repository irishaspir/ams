package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.zodiac.ams.charter.services.ft.rudp.util.NetworkOrderBitUtils;
import com.dob.ams.util.CRC16;
import com.dob.ams.util.DOB7bitUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;

public class WrongSessionIdErrorResponsePacket implements ErrorResponsePacket {

    private InetSocketAddress remoteAddress;
    private int fileId;

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Override
    public ErrorCodeType getErrorCode() {
        return ErrorCodeType.FILEID;
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    @Override
    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    @Override
    public String toString() {
        return "WrongSessionIdErrorResponsePacket{" +
                "remoteAddress=" + remoteAddress +
                ", fileId=" + fileId +
                '}';
    }

    @Override
    public byte[] toBytes() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(128);

            // error code - this should be 7-bit encoded
            baos.write(getErrorCode().errorCodeType);
            // fileId
            baos.write(DOB7bitUtils.encodeUInt(getFileId()));
            byte[] bytes = baos.toByteArray();
            CRC16 crc16 = new CRC16();
            crc16.update(bytes);
            int crc16Bytes = (int) (crc16.getValue() & 0xFFFF);

            baos.reset();

            baos.write(PacketType.ERROR.packetType);
            baos.write(NetworkOrderBitUtils.i2b2(crc16Bytes));
            baos.write(bytes);

            return baos.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Should not happen", e);
        }
    }
}
