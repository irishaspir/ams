package com.zodiac.ams.charter.services.ft.dvr;

import org.json.JSONObject;

public class JSONUtils {
    
    static int getNumber(Object obj){
        return (obj instanceof Number) ? ((Number)obj).intValue() : 0;
    }
    
    static int getInt(Object obj){
        return (obj instanceof Integer) ? (Integer)obj : 0;
    }
    
    static void setField(JSONObject obj, String key, Object value){
        if (value instanceof Number)
            obj.put(key, ((Number)value).intValue());
        else
        if (null != value)
            obj.put(key, value.toString());
    }
    
}
