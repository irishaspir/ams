package com.zodiac.ams.charter.services.ft.ppv;

import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dob.bcfg3.Bcfg;
import com.dob.bcfg3.Bcfg.FieldInfo;
import java.util.Iterator;

public class PPVMetaDataZDB {
    private final static Logger LOG = LoggerFactory.getLogger(PPVMetaDataZDB.class);
    private final static String ROOT_FIELD = "ppv_info_records";
        
    int showing_time;      //date+time as string UTC (sec)
    int advert;             //advert
    int duration;           //length
    int purchase_lower;     //the first XX namuber in parameters: XX..YY
    int purchase_higher;    //the last YY mumber in parameters: XX..YY
    int service_id;         //source_id
    int dcas_eid ;          //????
    int review_lower;       //the first XX namuber in preview: XX..YY
    int review_higher;      //the last YY mumber in parameters: XX..YY
    String title;           //title
    int eid;                //eid
    
    private PPVMetaDataZDB(){
    }
    
    @Override
    public String toString(){
        return service_id + "|" + showing_time +
            "|" + duration +
            "|" + purchase_lower + " " + purchase_higher +  
            "|" + review_lower + " " + review_higher +
            "|" + title + "|" + eid;
    }
    
    static public List<PPVMetaDataZDB> getMetaDataZDB(String fileName){
        List<PPVMetaDataZDB> result = new ArrayList<>();
        try{
            RandomAccessFile file = new RandomAccessFile(fileName, "r"); 
            
            Bcfg bcfg = new Bcfg();
            bcfg.parse(file);
            Bcfg.Table root = bcfg.getRoot();
            
            FieldInfo info = null;
            for (int i=0; i< root.getFieldsCount(); i++){
                FieldInfo fi = root.getField(i);
                if (ROOT_FIELD.equals(fi.getName())){
                    info = fi;
                    break;
                }
            }
            
            Bcfg.Table table = root.getFieldAsTable(0, info);
            List<FieldInfo> fields = new ArrayList<>();
            for (int i=0; i< table.getFieldsCount(); i++)
                fields.add(table.getField(i));
            
            for (int row=0; row<table.getRecordsCount(); row++){
                PPVMetaDataZDB data = new PPVMetaDataZDB(); 
                Iterator<FieldInfo> it = fields.iterator();
                while (it.hasNext()){
                    FieldInfo fi = it.next();
                    switch(fi.getName()){
                        case "showing_time":
                            data.showing_time = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "duration":   
                            data.duration = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "service_id":
                            data.service_id = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "advert":
                            data.advert = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "dcas_eid":
                            data.dcas_eid = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "purchase_higher":
                            data.purchase_higher = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "purchase_lower":
                            data.purchase_lower = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "review_higher":
                            data.review_higher = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "review_lower":
                            data.review_lower = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                        case "title":
                            data.title = table.getFieldAsStr(row, fi);
                            break;
                        case "eid":
                            data.eid = Integer.parseInt(table.getFieldAsStr(row, fi));
                            break;
                    }
                }
                result.add(data);
            }
        }
        catch(Exception ex){
            LOG.error("getMetaDataZDB method exception: {}", ex.getMessage());
            return null;
        }
        return result;
    }
    
}