package com.zodiac.ams.charter.services.ft.ppv;

import java.io.ByteArrayInputStream;
import org.json.JSONObject;
import com.dob.ams.util.DOB7bitUtils;
import com.dob.ams.util.DOBCountingInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PPVEventNotification {
    
    final static Logger LOG = LoggerFactory.getLogger(PPVEventNotification.class);
    
    private static final String DEVICE_ID =  "DeviceId";
    private static final String PPV_EVENTS = "PPVEvents";
    
    String deviceId;
    PPVEvents events;
    
        
    public static boolean equal(PPVEventNotification notification1, PPVEventNotification notification2, boolean ascending) {
            
        TreeSet<String> eids1 = notification1.getEids();
        TreeSet<String> eids2 = notification2.getEids();
        int size1 = eids1.size();
        int size2 = eids2.size();
        if (size1 != size2) 
            return false;

        Iterator<String> it1 = eids1.iterator();
        Iterator<String> it2 = eids2.iterator();

        while (it1.hasNext()) {
            String eid1 = it1.next();
            String eid2 = it2.next();
            if (!eid1.equals(eid2))
                return false;
            
            PPVEventNotificationInfo info1 = ascending ? PPVEventNotificationInfo.getFirst(notification1, eid1)
                : PPVEventNotificationInfo.getLast(notification1, eid1);
            PPVEventNotificationInfo info2 = ascending ? PPVEventNotificationInfo.getFirst(notification2, eid2)
                : PPVEventNotificationInfo.getLast(notification2, eid2);
            
            if (!info1.equals(info2))
                return false;
        }
        
        return true;
    }
   
    
    TreeSet<String> getEids(){
        TreeSet<String> result = new TreeSet<>();
        if (null == events)
            return result;
        List<PPVEvent> evts = events.events;
        if (null == evts)
            return result;
        for (PPVEvent evt: evts){
            if (null == evt)
                continue;
            if (null == evt.parameters)
                continue;
            List<PPVParameter> prms = evt.parameters.parameters;
            if (null == prms)
                continue;
            for (PPVParameter prm: prms){
                if (null != prm && null != prm.eid)
                    result.add(prm.eid);
            }
        }
        return result;
    }
    
    
    public PPVEventNotification(String deviceId, PPVEvents events){
        this.deviceId = deviceId;
        this.events = events;
    }
    
    
    
    public PPVEventNotification(byte[]  bytes){
        DOBCountingInputStream in =new DOBCountingInputStream(new ByteArrayInputStream(bytes));
        try{
            while (in.available() > 0) {
                if (null == events)
                    events = new PPVEvents();
                
                PPVCommand command = PPVCommand.get(DOB7bitUtils.decodeUInt(in)[0]);
                PPVParameters parameters =  null;
 
                int count = DOB7bitUtils.decodeUInt(in)[0];
                
                for (int i = 0; i < count; i++) {
                    if (null == parameters)
                        parameters = new PPVParameters();
                    
                    String eid = String.valueOf(DOB7bitUtils.decodeUInt(in)[0]);
                    long date = DOB7bitUtils.decodeUInt(in)[0];
                    String purchaseTime = PPVUtils.getDate(date * 1000L);
                    date = DOB7bitUtils.decodeUInt(in)[0];
                    String startTime = PPVUtils.getDate(date * 1000L);
                    String tmsServiceId = String.valueOf(DOB7bitUtils.decodeUInt(in)[0]);
                    
                    PPVParameter parameter = new PPVParameter(PPVParameterType.PurchaseTime, eid, purchaseTime,
                        startTime, tmsServiceId);
                    
                    parameters.addParameter(parameter);
                }
                
                PPVEvent event = new PPVEvent(command, parameters);
                events.addEvent(event);
            }
        }
        catch(Exception ex){
            LOG.error("PPVEventNotification(byte[] )error: {}", ex.getMessage());
        }
    }
     
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        if (null != deviceId)
            obj.put(DEVICE_ID, deviceId);
        if (null != events)
            obj.put(PPV_EVENTS, events.toJSONArray());
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
    public boolean contains(String key, String value){
        JSONObject json =new JSONObject();
        json.put(key, value);
        String toFind = json.toString();
        toFind = toFind.substring(1, toFind.length()-1); //remove first "{" and last "}"
        return toJSON().toString().contains(toFind);
    }
}
