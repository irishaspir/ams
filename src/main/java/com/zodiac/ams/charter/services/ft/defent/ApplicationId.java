package com.zodiac.ams.charter.services.ft.defent;

public enum ApplicationId {
    SYS(9),
    VC(0),
    CB(1),
    MG(2),
    IB(3),
    QS(4),
    AVNC(5),
    CID(6),
    OVRL(7),
    DVR(10),
    TPB(11),
    AVNC_YT(14),
    AVNC_NF(12),
    NRDP(13),
    EAS(8);

    private final int code;
    
    private ApplicationId(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
    
    public String toApplicationIdString(){
        return this.toString().replace("_", "-");
    }
}
