package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum RecordingFormat {
    HD(1), SD(2);
    
    private final int code;
    
    private RecordingFormat(int code){
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }
    
    public static RecordingFormat fromCode(int code){
        RecordingFormat[] formats = RecordingFormat.values();
        for (RecordingFormat format : formats){
            if (code == format.code)
                return format;
        }
        return null;
    }
}
