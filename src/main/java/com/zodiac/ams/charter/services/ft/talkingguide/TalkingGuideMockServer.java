package com.zodiac.ams.charter.services.ft.talkingguide;

import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TalkingGuideMockServer extends Thread{
    private final Logger logger = LoggerFactory.getLogger(TalkingGuideMockServer.class);
    
    private static final String DATA_DIR = "dataTalkingGuide";
    
    private static final String KEYSTORE = "certs";
    private static final char[] KEYSTOREPW = "serverkspw".toCharArray();
    private static final char[] KEYPW = "serverpw".toCharArray();
    
    private SSLServerSocket serverSocket;
    
    private final BlockingQueue<TalkingGuideItem> queue=new ArrayBlockingQueue<>(10);
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    
    public TalkingGuideMockServer(int port) throws Exception {
        try{
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            KeyStore keystore = KeyStore.getInstance("JKS");
            keystore.load(new FileInputStream(getName(KEYSTORE)), KEYSTOREPW);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keystore, KEYPW);
            SSLContext sslc = SSLContext.getInstance("SSLv3");
            sslc.init(kmf.getKeyManagers(), null, null);
            ServerSocketFactory ssf = sslc.getServerSocketFactory();
            serverSocket = (SSLServerSocket) ssf.createServerSocket(port);
            serverSocket.setNeedClientAuth(false);
        } 
        catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | KeyManagementException e) {
            logger.error(e.getMessage());
        }
    }
    
    private static String getName(String name){
        return getFileFromJar(DATA_DIR, name);
    }
    
    public boolean addTalkingGuideItem(TalkingGuideItem item, long timeout, TimeUnit unit){
        boolean result = false;
        try {
            queue.put(item);
            result = item.semaphore.tryAcquire(timeout, unit);
        }
        catch(InterruptedException ex){
            logger.error(ex.getMessage());
        }
        return result;
    }

    
    @Override
    public void run() {
        try {
            while (true) {
                SSLSocket socket = (SSLSocket) serverSocket.accept();
                SSLSession session = socket.getSession();
                TalkingGuideItem item = queue.take();
                item.future=executorService.submit(new RequestHandler(socket, session, item));
                item.semaphore.release();
            }
        } 
        catch (IOException|InterruptedException ex) {
            logger.error(ex.getMessage());
        }
    }
    
    
    class RequestHandler implements Runnable {
        private final SSLSocket socket;
        private final SSLSession session;
        private final TalkingGuideItem item;

        public RequestHandler(SSLSocket socket, SSLSession session, TalkingGuideItem item) {
            this.socket = socket;
            this.session = session;
            this.item = item;
        }
        
        public void run(){
            try {
                BufferedReader reader=new BufferedReader(new InputStreamReader(socket.getInputStream()));
                
                String line;
                int length = -1;
                
                StringBuilder header=new StringBuilder();
                while(!(line = reader.readLine()).equals("")) {
                    if (header.length()>0)
                        header.append("\n");
                    header.append(line);
                    if(line.contains("Content-Length")) 
                        length = Integer.valueOf(line.split(":")[1].trim());
                }
                item.setHeader(header.toString());
                 

                int read = 0;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                while(-1 != read && read < length) {
                    baos.write(reader.read());
                    read++;
                }
                
                String content=baos.toString();
                baos.close();
                item.setContent(content);
               
                byte[] sound = item.getSound();
                
                OutputStream os = socket.getOutputStream();
                PrintWriter out = new PrintWriter(os);
                out.write("HTTP/1.0 200 OK\r\n");
                out.write("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0\r\n");
                out.write("Content-Type: audio/x-wav\r\n");
                out.write("Content-Length: " + sound.length + "\r\n");
                out.write("Content-Disposition: filename=\"1447739981573.wav\";\r\n");
                out.write("x-nuance-sessionid: 31e397ea-ef30-46ac-8a8e-f2f8c2b11be8\r\n");
                out.write("\r\n");
                out.flush();
                if (item.error)
                    os.write(sound,0,sound.length/2);
                else
                    os.write(sound);
                os.flush();  
            } 
            catch (IOException ex) {
                logger.error(ex.getMessage());
            } 
            finally{
                try{
                    socket.close();
                }
                catch(IOException e){
                    logger.error(e.getMessage());
                }
            }
        }
    }
    
    
     public void shutdown() {
        try {
            serverSocket.close();
            executorService.shutdown();
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS))
                executorService.shutdownNow();
        } 
        catch (IOException|InterruptedException ex) {
            logger.error(ex.getMessage());
        }
    }
    
}
