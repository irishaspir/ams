package com.zodiac.ams.charter.services.ft.ppv;

import java.util.Comparator;

public class PPVMetaDataZDBComparator implements Comparator<PPVMetaDataZDB>{

    @Override
    public int compare(PPVMetaDataZDB o1, PPVMetaDataZDB o2) {
        int i;
        i = (int)(o1.showing_time - o2.showing_time);
        if (0 != i)
            return i;
        i = o1.service_id - o2.service_id;
        if (0 != i)
            return i;
        i = o1.eid - o2.eid;
        if (0 != i)
            return i;
        i = o1.duration - o2.duration;
        if (0 != i)
            return i;
        i = o1.advert - o2.advert;
        if (0 != i)
            return i;
        i = o1.title.compareToIgnoreCase(o2.title);
        if (0 != i)
            return i;
        
        String str1 = o1.review_lower + ".." + o1.review_higher;
        String str2 = o2.review_lower + ".." + o2.review_higher;
        i = str1.compareToIgnoreCase(str2);
        if (0 != i)
            return i;
        
        str1 = o1.purchase_lower + ".." + o1.purchase_higher;
        str2 = o2.purchase_lower + ".." + o2.purchase_higher;
        i = str1.compareToIgnoreCase(str2);
        if (0 != i)
            return i;
        
        return 0;
    }
    
}
