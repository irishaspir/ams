package com.zodiac.ams.charter.services.ft.rudp;

import com.dob.ams.transport.zodiac.msg.ZodiacMessagePackage;
import static com.zodiac.ams.charter.services.ft.rudp.RUDPTestingSendManager.getTestingSession;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacketSerializer;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacket;
import com.zodiac.ams.charter.services.ft.rudp.packet.StartPacketSerializer;
import com.zodiac.ams.charter.services.ft.rudp.session.IRUDPSenderSession;
import com.zodiac.ams.charter.services.ft.rudp.util.NetworkOrderBitUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelDownstreamHandler;
import org.jboss.netty.util.Timeout;
import org.jboss.netty.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RUDPTestingSenderHandler extends SimpleChannelDownstreamHandler{
    
    private static final int ALL_SEGMENTS = -1;
    private static final Logger log = LoggerFactory.getLogger(RUDPTestingSenderHandler.class);
    
    public final int TIMEOUT_SEND_START_MILLIS;
    public final int DATA_PACKET_TIMEOUT_MILLIS;
    public final int DATA_PACKET_PORTION_TIMEOUT_MILLIS;
    public final int START_PACKET_MAX_RETRIES;
    public final int DATA_PACKET_MAX_RETRIES;
    public final int SEGMENT_BALK_COUNT;

    public final int CHECK_START_ACK_INTERVAL_MILLIS = 5;
    public final int CHECK_ALL_DATA_ACK_INTERVAL_MILLIS = 5;
    public final int SEND_DATA_PACKETS_TASK_INTERVAL_MILLIS = 5;
    public final int SEGMENT_SIZE;
    
    private RUDPTestingSendManager sender;
    
    private final ConcurrentMap<Integer,IStartPacketProvider> startProviders = new ConcurrentHashMap<>();
    private final ConcurrentMap<Integer,IDataPacketProvider> dataProviders = new ConcurrentHashMap<>();
    
    
    private static final Ordering<Map.Entry<Integer, Integer>> BY_MAP_VALUES = new Ordering<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> left, Map.Entry<Integer, Integer> right) {
            return left.getValue().compareTo(right.getValue());
        }
    };
    
    void setStartPacketProvider(int sessionId,IStartPacketProvider provider){
        startProviders.put(sessionId, provider);
    }
    
    void setDataPacketProvider(int sessionId,IDataPacketProvider provider){
        dataProviders.put(sessionId, provider);
    }
    
    void removeStartPacketProvider(int sessionId){
        startProviders.remove(sessionId);    
    }
    
    void removeDataPacketProvider(int sessionId){
        dataProviders.remove(sessionId);
    }
    
    abstract class AbstractTimerTask implements TimerTask {
        protected final IRUDPSenderSession session;
        protected final ChannelHandlerContext ctx;
        protected final MessageEvent messageEvent;
        
        protected AbstractTimerTask(IRUDPSenderSession session, ChannelHandlerContext ctx, MessageEvent messageEvent) {
            this.session = session;
            this.ctx = ctx;
            this.messageEvent = messageEvent;
        }
        
        protected void scheduleSendStartPacketTask(Timeout timeout,boolean startOnly) {
            timeout.getTimer().newTimeout(new SendStartPacketTask(session, ctx, messageEvent, startOnly), 10, TimeUnit.MILLISECONDS);
        }

        protected void scheduleCheckStartAckPacketTask(Timeout timeout,boolean startOnly) {
            timeout.getTimer().newTimeout(new CheckStartAckPacketTask(session, ctx, messageEvent, startOnly), CHECK_START_ACK_INTERVAL_MILLIS, TimeUnit.MILLISECONDS);
        }
        
        protected void scheduleSendDataPacketsTask(Timeout timeout,int segment) {
            scheduleSendDataPacketsTask(timeout, SEND_DATA_PACKETS_TASK_INTERVAL_MILLIS, TimeUnit.MILLISECONDS, segment);
        }

        protected void scheduleSendDataPacketsTask(Timeout timeout, long delay, TimeUnit timeUnit, int segment) {
            timeout.getTimer().newTimeout(new SendDataPacketsTask(session, ctx, messageEvent,segment), delay, timeUnit);
        }
        
        protected void setErrorMsg(String errorMsg){
            RUDPTestingSession ts = getTestingSession(session.getSessionId());
            if (null != ts)
                ts.setErrorMsg(errorMsg);
        }
     }

    
    class SendStartPacketTask extends AbstractTimerTask{
        
        IStartPacketProvider provider;
        final boolean startOnly;
       
        
        SendStartPacketTask(IRUDPSenderSession session, ChannelHandlerContext ctx, MessageEvent messageEvent,boolean startOnly) {
            super(session, ctx, messageEvent);
            this.startOnly = startOnly;
        }
        

        @Override
        public void run(Timeout timeout) throws Exception {
            
            if (messageEvent.getFuture().isDone()) {
                log.debug("[RUDP TESTING {}] SendStartPacketTask: message is done",session.getSessionId());
                return;
            }

            if (session.startAckReceived()) {
                if (startOnly){
                    messageEvent.getFuture().setSuccess();
                    log.debug("[RUDP TESTING {}] SendStartPacketTask: message is done",session.getSessionId());
                    return;
                }
                else
                    scheduleSendDataPacketsTask(timeout,RUDPTestingDataMessage.ALL_SEGMENTS);
            }
            else {
                int retryNumber = session.incrStartPacketRetries();
                if (retryNumber > START_PACKET_MAX_RETRIES) {
                    final String msg = "[RUDP TESTING "+session.getSessionId()+"] max number of START packets exceeded! Max number: " + 
                            START_PACKET_MAX_RETRIES + ", channel: " + ctx.getChannel();
                    setErrorMsg(msg);
                    log.error(msg);
                    messageEvent.getFuture().setFailure(new IOException(msg));
                } 
                else {
                    ChannelFuture channelFuture = Channels.future(ctx.getChannel());
                    final StartPacket startPacket = createStartPacket();
                    channelFuture.addListener(new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture future) throws Exception {
                            log.debug("[RUDP TESTING {}] START is sent: {} RetryNumber={}", 
                                session.getSessionId(),startPacket, session.getStartPacketRetries());
                        }
                    });
                    sendStartPacket(channelFuture, startPacket);
                    session.markStartPacketSendtime();
                    scheduleCheckStartAckPacketTask(timeout, startOnly);
                }
            }
        }
        
        private StartPacket createStartPacket() {
            int sessionId = session.getSessionId();
            StartPacket sp=new StartPacket(session.getSegmentSize(), sessionId, session.getDataSize());
            IStartPacketProvider provider = startProviders.get(sessionId);
            return (null == provider) ? sp : provider.get(sp);
        }

        private void sendStartPacket(ChannelFuture channelFuture, StartPacket startPacket) {
            log.debug("[RUDP TESTING {}] send START: {} RetryNumber={}", session.getSessionId(),startPacket, session.getStartPacketRetries());
            byte[] bytes = StartPacketSerializer.toBytes(startPacket);
            Channels.write(ctx, channelFuture, ChannelBuffers.wrappedBuffer(bytes), messageEvent.getRemoteAddress());
        }
    }
    
    
    class CheckStartAckPacketTask extends AbstractTimerTask {
        
        final boolean startOnly;

        CheckStartAckPacketTask(IRUDPSenderSession session, ChannelHandlerContext ctx, MessageEvent messageEvent,boolean startOnly) {
            super(session, ctx, messageEvent);
            this.startOnly = startOnly;
        }

        @Override
        public void run(Timeout timeout) throws Exception {
            if (messageEvent.getFuture().isDone()) {
                log.debug("[RUDP TESTING {}] CheckStartAckPacketTask: message is done",session.getSessionId());
                return;
            }

            if (session.startAckReceived()) {
                if (startOnly) {
                    messageEvent.getFuture().setSuccess();
                    log.debug("[RUDP TESTING {}] CheckStartAckPacketTask: message is done",session.getSessionId());
                    return;
                }
                else
                    scheduleSendDataPacketsTask(timeout,RUDPTestingDataMessage.ALL_SEGMENTS);   
            }
            else {
                if (System.currentTimeMillis() - session.getLastStartPacketTimesent() > TIMEOUT_SEND_START_MILLIS) 
                    scheduleSendStartPacketTask(timeout, startOnly);
                else
                    scheduleCheckStartAckPacketTask(timeout,startOnly);
            }
        }
    }
    
     class SendDataPacketsTask extends AbstractTimerTask {
        private int segment = ALL_SEGMENTS;
         
        protected SendDataPacketsTask(IRUDPSenderSession session, ChannelHandlerContext ctx, MessageEvent messageEvent, int segment) {
            super(session, ctx, messageEvent);
            this.segment = segment;
        }

        @Override
        public void run(Timeout timeout) throws Exception {
            if (messageEvent.getFuture().isDone()) {
                log.debug("[RUDP TESTING {}] SendDataPacketsTask segment={}: message is done",session.getSessionId(),segment);
                return;
            }

            //
            // remove all checkings from here since they are now performed in CheckAllDataAckPacketTask
            //
            if (ALL_SEGMENTS == segment){
                if (session.allAcksReceived()) {
                    messageEvent.getFuture().setSuccess();
                    log.debug("[RUDP TESTING {}] SendDataPacketsTask : all acks are received",session.getSessionId());
                    return;
                }
            }
            else
            if (session.ackReceived(segment)){
                messageEvent.getFuture().setSuccess();
                log.debug("[RUDP TESTING {}] SendDataPacketsTask : segment={} ask is received",session.getSessionId(),segment);
                return;
            }    
            
            if (!session.continueSendPackets()) {
                final String msg = "[RUDP TESTING "+session.getSessionId()+"] SendDataPacketsTask segment="+segment+": unable to continue sending";
                setErrorMsg(msg);
                log.debug(msg);
                messageEvent.getFuture().setFailure(new IOException(msg));
                return;
            }

            if (!sendDataPackets(timeout)) {
                final String msg = "[RUDP TESTING "+session.getSessionId()+"] max number of DATA packets retries exceeded! Max number: " +DATA_PACKET_MAX_RETRIES;
                setErrorMsg(msg);
                log.error(msg);
                messageEvent.getFuture().setFailure(new IOException(msg));
                //registryService.removeSession(session.getSessionKey());
            }
        }

        private boolean sendDataPackets(final Timeout timeout) {
            
            Map<Integer, Integer> segmentsToBeSent = (ALL_SEGMENTS == segment) ? 
                session.getSegmentsToBeAcknowledged() : session.getSegmentsToBeAcknowledged(segment);
            if (segmentsToBeSent.isEmpty()) 
                    return true;
            
            Map<Integer, Integer> segmentsToBeSentTop = getMapWithTopValues(segmentsToBeSent, SEGMENT_BALK_COUNT);
            if (getMaxValue(segmentsToBeSentTop) > DATA_PACKET_MAX_RETRIES) {
                return false;
            }

            session.incrDataPacketRetries(segmentsToBeSentTop.keySet());
            

            int counter = 1;
            for (Map.Entry<Integer, Integer> entry : segmentsToBeSentTop.entrySet()) {
                int segmentNumber = entry.getKey();
                int retryNumber = entry.getValue();
                int delay = counter * DATA_PACKET_TIMEOUT_MILLIS;
                log.debug("[RUDP TESTING {}] start sending DATA packets. SegmentNumber: {} RetryNumber: {}", 
                    session.getSessionId(), segmentNumber, retryNumber);
                sender.timer.newTimeout(new SendSingleDataPacketTask(session, ctx, messageEvent, segmentNumber, retryNumber), delay, TimeUnit.MILLISECONDS);
                counter++;
            }

            int delay = counter * DATA_PACKET_TIMEOUT_MILLIS + DATA_PACKET_PORTION_TIMEOUT_MILLIS;
            scheduleSendDataPacketsTask(timeout, delay, TimeUnit.MILLISECONDS, segment);
            return true;
        }

        private Integer getMaxValue(Map<Integer, Integer> map) {
            Integer maxValue = -Integer.MAX_VALUE;
            for (Integer value : map.values()) {
                maxValue = Math.max(maxValue, value);
            }
            return maxValue;
        }

        private Map<Integer, Integer> getMapWithTopValues(Map<Integer, Integer> map, int topCount) {
            if (map.size() <= topCount) {
                return map;
            }

            List<Map.Entry<Integer, Integer>> list = Lists.newArrayList(map.entrySet());
            Collections.sort(list, BY_MAP_VALUES.reverse());
            return createTreeMap(list.subList(0, Math.min(list.size(), topCount)));
        }

        private <K, V> Map<K, V> createTreeMap(Collection<Map.Entry<K, V>> collection) {
            Map<K, V> map = new TreeMap<>();
            for (Map.Entry<K, V> entry : collection) {
                map.put(entry.getKey(), entry.getValue());
            }
            return map;
        }
    }
    
    class SendSingleDataPacketTask extends AbstractTimerTask {

        private final int segmentNumber;
        private final int retryNumber;

        SendSingleDataPacketTask(IRUDPSenderSession session, ChannelHandlerContext ctx, MessageEvent messageEvent, int segmentNumber, int retryNumber) {
            super(session, ctx, messageEvent);
            this.segmentNumber = segmentNumber;
            this.retryNumber = retryNumber;
        }

        @Override
        public void run(Timeout timeout) throws Exception {
            if (messageEvent.getFuture().isDone() || session.allAcksReceived() ) {
                log.debug("[RUDP TESTING {}] SendSingleDataPacketTask: message is done",session.getSessionId());
                return;
            }

            if (!session.continueSendPackets()) {
                log.error("[RUDP TESTING {}] SendSingleDataPacketTask: unable to continue sending",session.getSessionId());
                return;
            }
            
            if (!ctx.getChannel().isOpen()) {
                log.error("[RUDP TESTING {}] Channel is closed {}", session.getSessionId(), ctx.getChannel());
                return;
            }
            
            sendDataPacket();
        }

        /**
         * Case 1
         * <pre>
         * Let segmentSize = 128
         * Let real message size is 253
         * Then dataSize = 253+4 = 257
         * Then segmentCount = Math.ceil(257/128) = 3
         * Segment0: data bytes: 128
         * Segment1: data bytes: 125
         *           crc32 bytes: 3
         * Segment2: data bytes: 0
         *           crc32 bytes: 1
         * </pre>
         *
         * Case 2
         * <pre>
         * Let segmentSize = 128
         * Let real message size is 256
         * Then dataSize = 256+4 = 260
         * Then segmentCount = Math.ceil(260/128) = 3
         * Segment0: data bytes: 128
         * Segment1: data bytes: 128
         *           crc32 bytes: 0
         * Segment2: data bytes: 0
         *           crc32 bytes: 4
         * </pre>
         */
        private void sendDataPacket() {
            byte[] dataBytes = session.getDataBytes();
            byte[] segmentBytes = null;
            final int SEGSIZE = session.getSegmentSize();
            int offset = segmentNumber * SEGSIZE;
            if (offset < dataBytes.length) {
                if (offset + SEGSIZE <= dataBytes.length) {
                    //
                    // here we write _only_data_bytes_, no crc32 bytes are written here
                    //
                    segmentBytes = new byte[SEGSIZE];
                    System.arraycopy(dataBytes, offset, segmentBytes, 0, SEGSIZE);
                } else {
                    //
                    // here we write _both_ data bytes and some or all of crc32 bytes
                    //
                    int dataBytesQty = dataBytes.length - offset;
                    byte[] crc32Bytes = getCRC32Bytes(dataBytes);
                    if (dataBytesQty + 4 <= SEGSIZE) {
                        segmentBytes = new byte[dataBytesQty + 4];
                        System.arraycopy(dataBytes, offset, segmentBytes, 0, dataBytesQty);
                        segmentBytes[dataBytesQty + 0] = crc32Bytes[0];
                        segmentBytes[dataBytesQty + 1] = crc32Bytes[1];
                        segmentBytes[dataBytesQty + 2] = crc32Bytes[2];
                        segmentBytes[dataBytesQty + 3] = crc32Bytes[3];
                    } else {
                        segmentBytes = new byte[SEGSIZE];
                        System.arraycopy(dataBytes, offset, segmentBytes, 0, dataBytesQty);
                        int extra = SEGSIZE - dataBytesQty;
                        for (int i = 0; i < extra; i++) {
                            segmentBytes[dataBytesQty + i] = crc32Bytes[i];
                        }
                    }
                }
            } else {
                //
                // here we write _only_crc32_bytes_, since no data bytes remains to write here
                //
                byte[] crc32Bytes = getCRC32Bytes(dataBytes);
                int extra = (dataBytes.length + 4) - offset;
                segmentBytes = new byte[extra];
                for (int i = 0; i < extra; i++) {
                    segmentBytes[i] = crc32Bytes[4 - extra + i];
                }
            }
            sendDataPacket(segmentBytes);
        }

        private byte[] getCRC32Bytes(byte[] bytes) {
            CRC32 crc32 = new CRC32();
            crc32.update(bytes);
            long val = crc32.getValue();
            byte[] crc32Bytes = NetworkOrderBitUtils.l2b4(val);
            return crc32Bytes;
        }

        private void sendDataPacket(byte[] segmentBytes) {
            final DataPacket dataPacket = new DataPacket(session.getSessionId(), segmentNumber, segmentBytes);
            byte[] bytes = DataPacketSerializer.toBytes(dataPacket);
            log.debug("[RUDP TESTING {}] send DATA: {} RetryNumber={}", session.getSessionId(), dataPacket, retryNumber);
            
            IDataPacketProvider provider = dataProviders.get(session.getSessionId());
            if (null != provider){
                bytes = provider.get(session.getSessionId(), segmentNumber, dataPacket);
                log.debug("[RUDP TESTING {}] Original DATA packet is changed", session.getSessionId());
            }
            
            ChannelFuture channelFuture = Channels.future(ctx.getChannel());
            channelFuture.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    log.debug("[RUDP TESTING {}] DATA is sent: {} RetryNumber={}", session.getSessionId(), dataPacket, retryNumber);
                }
            });
            Channels.write(ctx, channelFuture, ChannelBuffers.wrappedBuffer(bytes), messageEvent.getRemoteAddress());
        }
    }
    
     public RUDPTestingSenderHandler(RUDPTestingSendManager sender) {
        this.sender = sender;
        //this.registryService = registryService;
        //this.timer = timer;
        if (null != sender.config){
            this.SEGMENT_SIZE = sender.config.getRudpSegmentSize();
            this.TIMEOUT_SEND_START_MILLIS = sender.config.getRudpStartPacketTimeoutMillis();
            this.DATA_PACKET_TIMEOUT_MILLIS = sender.config.getRudpDataPacketTimeoutMillis();
            this.DATA_PACKET_PORTION_TIMEOUT_MILLIS = sender.config.getRudpDataPacketPortionTimeoutMillis();
            this.START_PACKET_MAX_RETRIES = sender.config.getRudpSendStartPacketMaxRetries();
            this.DATA_PACKET_MAX_RETRIES = sender.config.getRudpSendDataPacketMaxRetries();
            this.SEGMENT_BALK_COUNT = sender.config.getRudpSegmentBalkCount();
        } 
        else {
            this.SEGMENT_SIZE = 400;
            this.TIMEOUT_SEND_START_MILLIS = 5000;
            this.DATA_PACKET_TIMEOUT_MILLIS = 5;
            this.DATA_PACKET_PORTION_TIMEOUT_MILLIS = 2000;
            this.START_PACKET_MAX_RETRIES = 10;
            this.DATA_PACKET_MAX_RETRIES = 10;
            this.SEGMENT_BALK_COUNT = Integer.MAX_VALUE;
        }

        log.info("[RUDP TESTING]: RUDPTestingSenderHandler is created with following settings: SEGMENT_SIZE={}, TIMEOUT_SEND_START_MILLIS={}, DATA_PACKET_TIMEOUT_MILLIS={}, DATA_PACKET_PORTION_TIMEOUT_MILLIS={}, START_PACKET_MAX_RETRIES={}, DATA_PACKET_MAX_RETRIES={}",
                SEGMENT_SIZE, TIMEOUT_SEND_START_MILLIS, DATA_PACKET_TIMEOUT_MILLIS, DATA_PACKET_PORTION_TIMEOUT_MILLIS, START_PACKET_MAX_RETRIES, DATA_PACKET_MAX_RETRIES);
    }
     
     
    @Override
    public void writeRequested(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        Object msg = e.getMessage();
        
        if (msg instanceof RUDPTestingMessage) {
            int id = ((RUDPTestingMessage)msg).sessionId;
            IRUDPSenderSession session = sender.getSenderSession(id);
            try {
                
                InetSocketAddress remoteAddress = (InetSocketAddress) e.getRemoteAddress();
                log.debug("[RUDP TESTING {}]: Start sending RUDPTestingMessage to {}", id, remoteAddress);
                if (msg instanceof RUDPTestingStartMessage){
                    RUDPTestingStartMessage m = (RUDPTestingStartMessage)msg;
                    startSendingStartSession(session, ctx, e, m.startOnly);
                }
                else
                if (msg instanceof RUDPTestingDataMessage){
                    RUDPTestingDataMessage m = (RUDPTestingDataMessage)msg;
                    startSendingDataSession(session, ctx, e, m.segment);
                }
                
            } catch (Throwable t) {
                log.error("[RUDP TESTING] Error when sending message via RUDP", t);
                throw t;
            }
            return;
        } 
        log.trace("[RUDP TESTING]: sender can handle only IRUDPTestingMessage messages, not of class {}, sending to downstream w/o rudp encoding", e.getMessage().getClass().getSimpleName());
        ctx.sendDownstream(e);
    }
    
    
    private void startSendingStartSession(final IRUDPSenderSession session, final ChannelHandlerContext ctx, final MessageEvent messageEvent,final boolean startOnly) {
        sender.timer.newTimeout(new SendStartPacketTask(session, ctx, messageEvent,startOnly), 5, TimeUnit.MILLISECONDS);   
    }
    
    private void startSendingDataSession(final IRUDPSenderSession session, final ChannelHandlerContext ctx, final MessageEvent messageEvent, final int segment) {
        sender.timer.newTimeout(new SendDataPacketsTask(session, ctx, messageEvent,segment), 5, TimeUnit.MILLISECONDS);   
    }
    
}
