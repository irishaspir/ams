package com.zodiac.ams.charter.services.dsg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SpiridonovaIM on 20.06.2017.
 */
public enum STBParametersUpdateOptions {

    REQUEST_ID("msgId", "", "2", "", new String[]{}),

    CONTENT("content", "", "1", "", new String[]{}),
    
    HUB_ID("hubId/VctId", "", "3", "", new String[]{}),

    SERVICE_GROUP_ID("serviceGroupId/tsId", "", "0", "", new String[]{}),

    LOAD_BALANSER_ADDRESS("loadBalancerIpAddress", "", "0", "", new String[]{}),

    SDV_SERVICE_GROUP_ID("sdvServiceGroupId", "", "0", "", new String[]{}),

    CM_IP("cmIp", "", "0", "", new String[]{}),

    CONNECTION_MODE("connectionMode", "", "0", "", new String[]{}),

    ;

    private STBParametersUpdateOption stbParametersUpdateOption;

    STBParametersUpdateOptions(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        stbParametersUpdateOption = new STBParametersUpdateOption(name, columnName, value, defaultValueInDB, range);
    }

    public STBParametersUpdateOption getOption() {
        return stbParametersUpdateOption;
    }

    public static List<STBParametersUpdateOption> allSTBParametersUpdateOptions() {
        ArrayList<STBParametersUpdateOption> options = new ArrayList<>();
        Arrays.asList(STBParametersUpdateOptions.values()).forEach(value -> options.add(value.getOption()));
        return options;
    }

}
