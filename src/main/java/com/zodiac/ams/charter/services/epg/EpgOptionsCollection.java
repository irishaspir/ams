package com.zodiac.ams.charter.services.epg;

import java.util.ArrayList;

/**
 * Created by SpiridonovaIM on 23.12.2016.
 */
public class EpgOptionsCollection {
    private ArrayList<EpgOption> list = new ArrayList<EpgOption>();

    public EpgOptionsCollection addOption(EpgOption option) {
        list.add(new EpgOption(option.getName(), option.getColumnName(), option.getValue(), option.getDefaultValue(), option.getRange()));
        return this;
    }

    public EpgOptionsCollection addOptions(EpgOptions option) {
        list.add(new EpgOption(option.getOption().getName(), option.getOption().getColumnName(), option.getOption().getValue(), option.getOption().getDefaultValue(), option.getOption().getRange()));
        return this;
    }

    public ArrayList<EpgOption> build() {
        return list;
    }

}
