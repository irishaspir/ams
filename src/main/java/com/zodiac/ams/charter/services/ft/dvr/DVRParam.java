package com.zodiac.ams.charter.services.ft.dvr;

import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomBoolean;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomObject;
import static com.zodiac.ams.charter.services.ft.dvr.JSONUtils.getInt;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.EpisodesDefinition;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.RecordingFormat;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.SaveDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Type;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.WeekDays;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.YesNo;
import java.io.IOException;
import java.util.Set;
import org.json.JSONObject;

public class DVRParam implements IDVRObject{
    protected Object recordingId;
    
    protected Object serviceId;
    protected Object channelNumber;
    protected Object startTime;
    protected Object duration;
    protected Object saveDays;
    protected Type type;
    protected Object startOffset;
    protected Object endOffset;
    protected Object recordingFormat;
    protected Object episodesDefinition;  
    protected String unitingParameter;
    protected Object recordDuplicates;
    protected Object/*WeekDays[]*/ repeat;
    
    public DVRParam(){
    }
    
    public DVRParam(DVRParam param){
        if (null == param)
            return;
        
        this.recordingId = param.recordingId;
        
        this.serviceId = param.serviceId;
        this.channelNumber = param.channelNumber;
        this.startTime = param.startTime;
        this.duration = param.duration;
        this.saveDays = param.saveDays;
        this.type = param.type;
        this.startOffset = param.startOffset;
        this.endOffset = param.endOffset;
        this.recordingFormat = param.recordingFormat;
        this.episodesDefinition = param.episodesDefinition;  
        this.unitingParameter = param.unitingParameter;
        this.recordDuplicates = param.recordDuplicates;
        this.repeat = param.repeat;
    }
    
    public void clear(){
        DVRParam template = new DVRParam();
        template.recordingId = recordingId;
        template.type = type;
        truncate(template);
    }
    
    public void truncate(DVRParam template){
        if (null == template)
            return;
        if (null == template.recordingId)
            this.recordingId = null;
        if (null == template.serviceId)
            this.serviceId = null;
        if (null == template.channelNumber)
            this.channelNumber = null;
        if (null == template.startTime)
            this.startTime = null;
        if (null == template.duration)
            this.duration = null;
        if (null == template.saveDays)
            this.saveDays = null;
        if (null == template.type)
            this.type = null;
        if (null == template.startOffset)
            this.startOffset = null;
        if (null == template.endOffset)
            this.endOffset = null;
        if (null == template.recordingFormat)
            this.recordingFormat = null;
        if (null == template.episodesDefinition)
            this.episodesDefinition = null;  
        if (null == template.unitingParameter)
            this.unitingParameter = null;
        if (null == template.recordDuplicates)
            this.recordDuplicates = null;
        if (null == template.repeat)
            this.repeat = null;
    }
    
    public static void truncate(DVRParam target, DVRParam template){
        target.truncate(template);
    }
    
    public static void random(DVRParam target, Type type){
        random(target, type, false);
    }
    
    public static void random(DVRParam target, Type type, boolean modify){
        target.random(type, modify);
    }
    
    public void random(Type type){
        random(type, false);
    }
    
    public void random(Type type, boolean modify){
        if (!modify || getRandomBoolean())
            this.serviceId = getRandomInt(1, 1000);
        this.channelNumber = getRandomInt(1, 100);
        if (!modify || getRandomBoolean())
            this.startTime = (int) System.currentTimeMillis()/1000;
        if (!modify || getRandomBoolean())
            this.duration = getRandomInt(1, 100);
        if (!modify || getRandomBoolean())
            this.saveDays = getRandomObject(SaveDays.values());
        this.type = type;
        if (getRandomBoolean())
            this.startOffset = getRandomInt(1,5) * 60;
        if (getRandomBoolean())
            this.endOffset = getRandomInt(1,5) * 60;
        if (!modify || getRandomBoolean())
            this.recordingFormat = getRandomObject(RecordingFormat.values());
        if (Type.Series == this.type){
            if (!modify || getRandomBoolean())
                this.episodesDefinition = getRandomObject(EpisodesDefinition.values());
        }
        if (Type.Series == this.type){
            if (!modify || getRandomBoolean())
                this.unitingParameter =  DvrMessageUtils.buildUnitingParam(getRandomInt(1, 1_000)); 
        }
        if (Type.Single == this.type || Type.Series == this.type){
            if (!modify || getRandomBoolean())
                this.recordDuplicates = getRandomObject(YesNo.values());
        }
        if (Type.Manual == this.type){
            if (!modify || getRandomBoolean())
                this.repeat = WeekDays.getRandomWeekDays();
        }
    }
    
    public void setServiceId(Object serviceId){
        this.serviceId = serviceId;
    }    
    
    public Integer getServiceId(){
        return getInt(serviceId);
    }
    
     public void setChannelNumber(Object channelNumber){
        this.channelNumber = channelNumber;
    }
     
    public void setStartTime(Object startTime){
        this.startTime = startTime;
    }
    
    public Integer getStartTime(){
        return getInt(startTime);
    }

    public void setDuration(Object duration){
        this.duration = duration;
    }
    
    public void setSaveDays(Object saveDays){
        this.saveDays = saveDays;
    }
    
    public void setType(Type type){
        this.type = type;
    }
    
    public void setType(int code){
        this.type = Type.fromCode(code);
    }
    
    public void setStartOffset(Object startOffset){
        this.startOffset = startOffset;
    }
    
    public void setEndOffset(Object endOffset){
        this.endOffset = endOffset;
    }
    
    public void setRecordingFormat(Object recordingFormat){
        this.recordingFormat = recordingFormat;
    }
    
    public void setEpisodesDefinition(Object episodesDefinition){
        this.episodesDefinition =  episodesDefinition;
    }
    
    public void setUnitingParameter(String unitingParameter){
        this.unitingParameter = unitingParameter;
    }
    
    public void setUnitingParameter(Integer unitingParameter){
        this.unitingParameter = (null == unitingParameter) ? null : DvrMessageUtils.buildUnitingParam(unitingParameter);
    }
    
    public void setRecordDuplicates(Object recordDuplicates){
        this.recordDuplicates = recordDuplicates;
    }
    
    public void setRepeat(Object repeat){
        this.repeat = repeat;
    }
    
    public void setRepeat(int mask){
        this.repeat = WeekDays.fromMask(mask);
    }
    
    public boolean isRepeat(){
        return (repeat instanceof WeekDays[]) ? ((WeekDays[])repeat).length > 0 : false;
    }
    
    public void setUnitedFlags(byte flags){
        Object[] params = DvrMessageUtils.getObjFromUnitedFlags(flags);
        saveDays = params[0];
        recordingFormat = params[1];
        episodesDefinition = params[2];
        recordDuplicates = params[3];
    }
    
    public Type getType(){
        return type;
    }
    
    
    public void setRecordingId(Object recordingId){
        this.recordingId = recordingId;
    }
    
    public Integer getRecordingId(){
        return getInt(recordingId);
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded){
        JSONObject obj = new JSONObject();
        JSONUtils.setField(obj, "recordingId", recordingId);
        JSONUtils.setField(obj, "serviceId", serviceId);
        JSONUtils.setField(obj, "channelNumber", channelNumber);
        if (null == excluded || !excluded.contains("startTime"))
            JSONUtils.setField(obj, "startTime", startTime);
        if (null == excluded || !excluded.contains("duration"))
            JSONUtils.setField(obj, "duration", duration);
        JSONUtils.setField(obj, "saveDays", saveDays);
        JSONUtils.setField(obj, "type", type);
        if (null == excluded || !excluded.contains("startOffset"))
            JSONUtils.setField(obj, "startOffset", startOffset);
        if (null == excluded || !excluded.contains("endOffset"))
            JSONUtils.setField(obj, "endOffset", endOffset);
        JSONUtils.setField(obj, "recordingFormat", recordingFormat);
        JSONUtils.setField(obj, "episodesDefinition", episodesDefinition);
        if (null == excluded || !excluded.contains("unitingParameter"))
            JSONUtils.setField(obj, "unitingParameter", unitingParameter);
        JSONUtils.setField(obj, "recordDuplicates", recordDuplicates);
        if (repeat instanceof WeekDays[])
            obj.put("repeat", WeekDays.toString((WeekDays[])repeat));
        else
            JSONUtils.setField(obj, "repeat", repeat);
        return obj;
    }

    @Override
    public byte[] getData() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
