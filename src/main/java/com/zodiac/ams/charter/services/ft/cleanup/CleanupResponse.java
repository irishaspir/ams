package com.zodiac.ams.charter.services.ft.cleanup;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CleanupResponse {
    private static final String DEVICES = "devices";
    private List<DeviceStatus> devices;
    
    public void addDevice(DeviceStatus status){
        if (null == devices)
            devices = new ArrayList<>();
        devices.add(status);
    }
    
    public void addDevice(String id){
        addDevice(new DeviceStatus(id));
    }
    
    public void addDevice(String id, String status, String error){
        addDevice(new DeviceStatus(id, status, error));
    }
    
    public JSONObject toJSON(){
        JSONObject obj =new JSONObject();
        if (null != devices){
            JSONArray arr = new JSONArray();
            for(DeviceStatus status: devices)
                arr.put(status.toJSON());
            obj.put(DEVICES, arr);
        }
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
}
