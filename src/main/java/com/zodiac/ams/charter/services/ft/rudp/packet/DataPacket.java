package com.zodiac.ams.charter.services.ft.rudp.packet;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;

public class DataPacket implements DataSerializable {
    private int fileId;
    private int segmentNumber;
    // this array will also contain CRC32 bytes is this last segment number packet
    private byte[] data;

    public DataPacket() {
    }

    public DataPacket(int fileId, int segmentNumber, byte[] data) {
        this.fileId = fileId;
        this.segmentNumber = segmentNumber;
        this.data = data;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public int getSegmentNumber() {
        return segmentNumber;
    }

    public void setSegmentNumber(int segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DataPacket{" +
                "fileId=" + fileId +
                ", segmentNumber=" + segmentNumber +
                ", data.length=" + data.length +
                //", data=" + Arrays.toString(data) +
                '}';
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeByteArray(data);
        out.writeInt(fileId);
        out.writeInt(segmentNumber);
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        data = in.readByteArray();
        fileId = in.readInt();
        segmentNumber = in.readInt();
    }
}
