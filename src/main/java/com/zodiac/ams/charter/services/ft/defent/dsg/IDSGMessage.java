package com.zodiac.ams.charter.services.ft.defent.dsg;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IDSGMessage {
    byte[] getMessage() throws Exception;
    JSONObject toJSON();
    JSONArray toJSONArray();
    int getVersion();
}
