package com.zodiac.ams.charter.services.ft.ppv;

import java.util.Comparator;
import org.json.JSONObject;

public class PPVParameter {
    private static final String EID = "EID";
    private static final String START_TIME = "StartTime";
    private static final String SERVICE_ID = "ServiceId";
    
    PPVParameterType type;
    String eid;
    String time;
    String startTime; //STB only
    String serviceId; //STB only
    
    static class PPVParameterComparator implements Comparator<PPVParameter>{
        boolean ascending;
        
        PPVParameterComparator(boolean ascending){
            this.ascending = ascending;
        }
        
        public boolean isAscending(){
            return this.ascending;
        }
    
        @Override
        public int compare(PPVParameter param1, PPVParameter param2) {
            int res = 0;

            if (null != param1.eid) {
                res = param1.eid.compareTo(param2.eid);
            } else if (null != param2.eid) {
                res = -param2.eid.compareTo(param1.eid);
            }
            if (0 != res) {
                return res;
            }

            res = 0;
            if (null != param1.time) {
                res = param1.time.compareTo(param2.time);
            } else if (null != param2.time) {
                res = -param2.time.compareTo(param1.time);
            }
            return (ascending ? 1 : -1) * res;
        }
    }
    
    public PPVParameter(PPVParameterType type, String eid, String time, String startTime, String serviceId){
        this.type = type;
        this.eid = eid;
        this.time = time;
        this.startTime = startTime;
        this.serviceId = serviceId;
    }
        
    public PPVParameter(PPVParameterType type, String eid, String time){
        this(type, eid,  time, null, null);
    }
    
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        if (null != eid)
            obj.put(EID, eid);
        if (null != time)
            obj.put(type.toString(), time);
        if (null != startTime)
            obj.put(START_TIME, startTime);
        if (null != serviceId)
            obj.put(SERVICE_ID, serviceId);
        return obj;
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
    
}
