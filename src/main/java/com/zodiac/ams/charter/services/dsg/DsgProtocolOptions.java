package com.zodiac.ams.charter.services.dsg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SpiridonovaIM on 31.05.2017.
 */
public enum DsgProtocolOptions {

    REQUEST_ID("msgId", "", "-1", "", new String[]{}),

    CONNECTION_MODE("connectionMode", "", "-1", "", new String[]{}),

    BOX_MODEL("boxModel", "", "-1", "", new String[]{}),

    VENDOR("vendor", "", "-1", "", new String[]{}),

    MESSAGE_TYPE_MAP("messageTypesMap", "", "-1", "", new String[]{}),

    MIGRATION_START("migrationStart", "", "-1", "", new String[]{}),

    HUB_ID("hubId", "", "-1", "", new String[]{}),

    SERVICE_GROUP_ID("serviceGroupId", "", "-1", "", new String[]{}),

    LOAD_BALANCER_IP_ADDRESS("loadBalancerIpAddress", "", "-1", "", new String[]{}),

    SW_VERSION("swVersion", "", "-1", "", new String[]{}),

    SDV_SERVICE_GROUP_ID("sdvServiceGroupId", "", "-1", "", new String[]{}),

    CM_MAC_ADDRESS("cmMacAddress", "", "-1", "", new String[]{}),

    SERIAL_NUMBER("serialNumber", "", "-1", "", new String[]{}),

    CM_IP("cmIp", "", "-1", "", new String[]{}),;

    private DsgProtocolOption dsgOption;

    DsgProtocolOptions(String name, String columnName, String value, String defaultValueInDB, String[] range) {
        dsgOption = new DsgProtocolOption(name, columnName, value, defaultValueInDB, range);
    }

    public DsgProtocolOption getOption() {
        return dsgOption;
    }

    public static List<DsgProtocolOption> allDsgProtocolOptions() {
        ArrayList<DsgProtocolOption> options = new ArrayList<>();
        Arrays.asList(DsgProtocolOptions.values()).forEach(value -> options.add(value.getOption()));
        return options;
    }

    public static List<DsgProtocolOption> dsgProtocolOptions() {
        ArrayList<DsgProtocolOption> options = new ArrayList<>();
        Arrays.asList(DsgProtocolOptions.values()).forEach(value -> {
            if (!value.getOption().getValue().equals("-1")) options.add(value.getOption());
        });
        return options;
    }


}
