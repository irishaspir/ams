package com.zodiac.ams.charter.services.ft.rudp.util;

import org.jboss.netty.channel.Channel;

public class RUDPUtils {

    public static String getChannelId(Channel channel) {
        if (channel != null) {
            return getIdString(channel.getId());
        } else {
            return "Channel is null!";
        }
    }

    /**
     * Code copied from @org.jboss.netty.channel.AbstractChannel.getIdString()
     * @param id
     * @return
     */
    public static String getIdString(Integer id) {
        if (id == null) {
            return "null";
        }
        String hex = Integer.toHexString(id.intValue());
        switch (hex.length()) {
            case 0:
                hex = "00000000";
                break;
            case 1:
                hex = "0000000" + hex;
                break;
            case 2:
                hex = "000000" + hex;
                break;
            case 3:
                hex = "00000" + hex;
                break;
            case 4:
                hex = "0000" + hex;
                break;
            case 5:
                hex = "000" + hex;
                break;
            case 6:
                hex = "00" + hex;
                break;
            case 7:
                hex = '0' + hex;
                break;
        }
        return hex;
    }
}
