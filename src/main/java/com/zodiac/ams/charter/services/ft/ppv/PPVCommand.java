package com.zodiac.ams.charter.services.ft.ppv;

public enum PPVCommand {
    PURCHASE,DELETE,MODIFY,UNKNOWN;
    
    static PPVCommand get(int index){
        return PPVCommand.values()[index];
    }
}
