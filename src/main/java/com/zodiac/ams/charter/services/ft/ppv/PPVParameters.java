package com.zodiac.ams.charter.services.ft.ppv;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class PPVParameters {
    List<PPVParameter> parameters;
    
    
    public PPVParameters(){
    }
    
    public PPVParameters(List<PPVParameter> parameters){
       this.parameters = parameters;
    }
    
    public JSONArray toJSONArray(){
        JSONArray array = new JSONArray();
        if (null != parameters){
            for (PPVParameter parameter: parameters)
                array.put(parameter.toJSON());
        }
        return array;
    }
    
    public void addParameter(PPVParameter parameter){
        if (null == parameters)
            parameters = new ArrayList<>();
        parameters.add(parameter);
    }
    
    @Override
    public String toString(){
        return toJSONArray().toString();
    }
    
}
