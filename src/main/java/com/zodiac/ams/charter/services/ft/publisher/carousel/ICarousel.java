package com.zodiac.ams.charter.services.ft.publisher.carousel;

import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselType;

public interface ICarousel {
    boolean putFileOnCarousel(String local,String remoteName);
    String getCarouselFile(String remoteName);
    String getZippedCarouselFile(String remoteName,String entryName);
    boolean deleteCarouselFile(String remoteName);
    boolean createRemoteCarouselDir();
    String getUid();
    String getHost();
    String getAlias();
    String getRootName();
    boolean isZipped();
    CarouselType getType();
    CarouselHost getCarouselHost();
    String getLocation(String name);
}
