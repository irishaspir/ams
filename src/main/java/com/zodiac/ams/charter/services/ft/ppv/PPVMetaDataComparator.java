package com.zodiac.ams.charter.services.ft.ppv;

import java.util.Comparator;

public class PPVMetaDataComparator implements Comparator<PPVMetaData>{
    
    @Override
    public int compare(PPVMetaData o1, PPVMetaData o2) {
        int i;
        i = o1.date.compareToIgnoreCase(o2.date);
        if (0 != i)
            return i;
        i = o1.time.compareToIgnoreCase(o2.time);
        if (0 != i)
            return i;
        i = o1.sourceid - o2.sourceid;
        if (0 != i)
            return i;
        i = o1.eid - o2.eid;
        if (0 != i)
            return i;
        i = o1.length - o2.length;
        if (0 != i)
            return i;
        i = o1.advert - o2.advert;
        if (0 != i)
            return i;
        i = o1.title.compareToIgnoreCase(o2.title);
        if (0 != i)
            return i;
        i = o1.preview.compareToIgnoreCase(o2.preview);
        if (0 != i)
            return i;
        i = o1.purchase.compareToIgnoreCase(o2.purchase);
        if (0 != i)
            return i;
        i = o1.sid - o2.sid;
        if (0 != i)
            return i;
        i = o1.flags.compareToIgnoreCase(o2.flags);
        if (0 != i)
            return i;
        i = o1.costpennies - o2.costpennies;
        if (0 != i)
            return i;
        i = o1.cancel - o2.cancel;
        if (0 != i)
            return i;
        return 0;
    }
}
