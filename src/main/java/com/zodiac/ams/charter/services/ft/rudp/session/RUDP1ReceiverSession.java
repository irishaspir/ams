package com.zodiac.ams.charter.services.ft.rudp.session;

import com.zodiac.ams.charter.services.ft.rudp.packet.DataPacket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * RUDP Receiver Session - session used when AMS server receive messages from STB.
 */
public class RUDP1ReceiverSession extends AbstractRUDP1Session implements IRUDPReceiverSession {
    private final NavigableMap<Integer, DataPacket> dataMap;
    private final Set<Integer> unreceivedSegmentsSet = new HashSet<>();
    private AtomicBoolean upstreamSent = new AtomicBoolean(false);
    private byte[] last4bytes;

    public RUDP1ReceiverSession(InetSocketAddress remoteAddress, int sessionId, int dataSize, int segmentSize) {
        super(remoteAddress, sessionId, dataSize, segmentSize);
        this.dataMap = new TreeMap<>();
        fillUnreceivedSegsmentsSet();
    }

    private void fillUnreceivedSegsmentsSet() {
        for (int i=0; i<getSegmentCount(); i++) {
            unreceivedSegmentsSet.add(i);
        }
    }

    @Override
    public boolean add(DataPacket dp) {
        setLastPacketTimestamp(System.currentTimeMillis());
        int segmentNumber = dp.getSegmentNumber();
        if (dataMap.containsKey(segmentNumber)) {
            return false;
        } else {
            dataMap.put(segmentNumber, dp);
            unreceivedSegmentsSet.remove(segmentNumber);
            return true;
        }
    }

    @Override
    public byte[] getBytesWoLast4() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(getDataSize());
        for (Map.Entry<Integer, DataPacket> e: dataMap.entrySet()) {
            try {
                baos.write(e.getValue().getData());
            } catch (IOException e1) {
                throw new IllegalStateException(e1);
            }
        }
        byte[] totalBytes = baos.toByteArray();
        assert totalBytes.length > 4;
        final int DATALEN = totalBytes.length-4;
        byte[] dataBytes = new byte[DATALEN];
        this.last4bytes = new byte[4];
        System.arraycopy(totalBytes, 0, dataBytes, 0, DATALEN);
        System.arraycopy(totalBytes, DATALEN, this.last4bytes, 0, 4);
        return dataBytes;
    }

    @Override
    public byte[] getLast4Bytes() {
        if (this.last4bytes == null) {
            // this also sets last4bytes
            getBytesWoLast4();
        }
        return this.last4bytes;
    }

    /**
     * Check that all segments are received
     * @return
     */
    @Override
    public boolean allSegmentsReceived() {
        return unreceivedSegmentsSet.isEmpty();
    }

    @Override
    public Set<Integer> getReceivedSegments() {
        return dataMap.keySet();
    }

    @Override
    public void close() throws Exception {
        // do nothing
    }

    @Override
    public boolean isUpstreamSent() {
        return upstreamSent.get();
    }

    @Override
    public void markUpstreamSent() {
        upstreamSent.set(true);
    }

    @Override
    public boolean tryMarkUpstreamSent() {
        return upstreamSent.compareAndSet(false, true);
    }
}
