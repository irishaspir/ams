package com.zodiac.ams.charter.services.ft.publisher.carousel;


import com.zodiac.ams.charter.services.ft.publisher.Constants;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.ROOT_ZDTS_INI;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.ZDTS_INI;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.ZDTS_INI_ZIP;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getLocalTmpName;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getName;
import static com.zodiac.ams.charter.services.ft.publisher.carousel.CarouselImpl.getRemoteTmpName;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.auth.AuthScope;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;



public class CarouselCDN extends CarouselImpl {
    //[{"archiveName":"ipg-test2.zdb","timestamp":1490713228},{"archiveName":"ipg-1.zdb","timestamp":1490288273}]
    
    private static final String FILE_PATH = "filePath";
    private static final String FILE = "file";
    private static final String ARCHIVE_NAME = "archiveName";
    private static final String TIMESTAMP = "timestamp";
    private static final int OK = 200;
    
    
    private static final String LIST = "/list";
    private static final String DOWNLOAD = "/download";
    private static final String UPLOAD = "/upload";
    private static final String DELETE = "/delete";
    
    private String location;
    private String rootTemplate; 
    
    //https://pi-sit-b.timewarnercable.com/td-admin/origin/
    //https://pi-sit-b.timewarnercable.com/tdcs/origin
    //zodiac_lab
    //SfRZqYL2t3BNlQmIS99w_wHd1
    
    private HostnameVerifier verifier;
    private CredentialsProvider credentialsProvider;
    private SSLContext sslContext;
    private String url;
    private CloseableHttpClient httpClient;
    
    public CarouselCDN(CarouselHost host,String location,boolean isZipped){
        super(host.getServerType(), isZipped);
        this.host = host;
        this.location = location;
        this.rootTemplate = null;
        url  = getURL(host.getHost());
        init();
    }
    
    public CarouselCDN(CarouselHost host,String location,String rootTemplate,boolean isZipped){
        super(host.getServerType(), isZipped);
        this.host = host;
        this.location = location;
        this.rootTemplate = rootTemplate; 
        url  = getURL(host.getHost());
        init();
    }
    
    private String getURL(String url){
        return url.endsWith("/") ? url.substring(0,url.length()-1) : url;
    }
    
    
    private CloseableHttpClient getHTTPClient() throws Exception {
        return HttpClientBuilder.create().
            setSSLContext(sslContext).
            setSSLHostnameVerifier(verifier).
            setDefaultCredentialsProvider(credentialsProvider).build();
    }
    
    
    private void init(){
        verifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            
            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            
            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        
         
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(host.getUser(), host.getPwd()));
            httpClient = getHTTPClient();
        }
        catch(Exception ex){
            logger.error("Unable to init CDN carousel: {}", ex.getMessage());
        }
    }
    
    
    @Override
    public boolean putFileOnCarousel(String local, String remoteName) {
        CloseableHttpClient httpClient = null;
        try{
            httpClient = getHTTPClient();
            HttpPost post = new HttpPost(url+UPLOAD);
        
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody(FILE_PATH, location);
        
            builder.addBinaryBody(FILE,Files.readAllBytes(new File(local).toPath()),ContentType.APPLICATION_OCTET_STREAM,remoteName);
        
            HttpEntity multipart = builder.build();
            post.setEntity(multipart);
        
            HttpResponse response = httpClient.execute(post);
            
            int status=response.getStatusLine().getStatusCode();
            if (OK != status)
                throw new Exception("Invalid response status: "+status);
        }
        catch (Exception ex){
            logger.error("Unable to put file on CDN carousel: {}", ex.getMessage());
            return false;
        }
        finally {
            if (null != httpClient){
                try{
                    httpClient.close();
                }
                catch(IOException ex){
                    logger.error(ex.getMessage());
                }
            }
        }
        return true;
    
    }
    
    @Override
    public String getCarouselFile(String remoteName){
        try {
            byte [] bytes = getCarouselFileImpl(remoteName);
            Files.write(new File(getRemoteTmpName(remoteName)).toPath(),bytes);
            return new String(bytes);
        }
        catch (Exception ex){
            logger.error("Unable to get {} from CDN carousel: {}", remoteName, ex.getMessage());
            return null;
        }
    }
    
    @Override
    public String getZippedCarouselFile(String remoteName,String entryName){
        try {
            byte [] bytes = getCarouselFileImpl(remoteName);
            return getZippedCarouselFile(bytes,remoteName,entryName);
        }
        catch (Exception ex){
            logger.error("Unable to get {}:{} from CDN carousel: {}",remoteName, entryName, ex.getMessage());
            return null;        
        }
    }
    
    
    @Override
    public boolean deleteCarouselFile(String remoteName) {
        CloseableHttpClient httpClient = null;
        try {
            httpClient = getHTTPClient();
            HttpPost post = new HttpPost(url+DELETE);
            
            JSONObject json=new JSONObject();
            json.put(FILE_PATH, location);
            json.put(FILE, remoteName);
        
            StringEntity entity = new StringEntity(json.toString());
            entity.setContentType("application/json");
            post.setEntity(entity);
            
            HttpResponse response = httpClient.execute(post);
            int status=response.getStatusLine().getStatusCode();
            return OK == status;
        }
        catch(Exception ex){
            logger.error("Unable to delete {} form CDN carousel: {}", remoteName, ex.getMessage());
            return false;
        }
        finally {
            if (null != httpClient){
                try{
                    httpClient.close();
                }
                catch(IOException ex){
                    logger.error(ex.getMessage());
                }
            }
        }
    }
    
    
    List<String> getFiles(){
        List<String> names=new ArrayList<>();
        CloseableHttpClient httpClient = null;
        try{
            
            httpClient = getHTTPClient();
            HttpPost post = new HttpPost(url+LIST);
            
            JSONObject json=new JSONObject();
            json.put(FILE_PATH, location);
        
            StringEntity entity = new StringEntity(json.toString());
            entity.setContentType("application/json");
            post.setEntity(entity);
            
            HttpResponse response = httpClient.execute(post);
            int status=response.getStatusLine().getStatusCode();
            
            if (OK != status)
                throw new Exception("Invalid response status: "+status);
                
            JSONArray array;
            
            InputStream in = response.getEntity().getContent();
            byte [] buffer=new byte[4_096];
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
                int read;
                while (-1 != (read=in.read(buffer)))
                    baos.write(buffer,0, read);
                baos.flush();
                array = new JSONArray(new String(baos.toByteArray()));
            }
            
            for (int i=0; i<array.length(); i++){
                JSONObject obj = array.getJSONObject(i);
                names.add(obj.getString(ARCHIVE_NAME));
            }
        }
        catch(Exception ex){
             logger.error("Unable to get list of files from CDN carousel: {}", ex.getMessage());
        }
        finally{
            if (null != httpClient){
                try{
                    httpClient.close();
                }
                catch(IOException ex){
                    logger.error(ex.getMessage());
                }
            }
        }
        return names;
    }
    

    @Override
    public boolean createRemoteCarouselDir() {
        try {
            List<String> names = getFiles();
            for (String name: names) {
                if (!deleteCarouselFile(name))
                    return false;
            }
            
            boolean ok = true;  
            if (isZipped){
                String target = getLocalTmpName(ZDTS_INI);
                ok = zipFile(getName(ZDTS_INI), target, ZDTS_INI);
                if (ok)
                    ok=putFileOnCarousel(target,ZDTS_INI_ZIP);
            }
            else 
                ok = putFileOnCarousel(getName(ZDTS_INI),ZDTS_INI);
            
            if (ok && null != rootTemplate)
                ok = putFileOnCarousel(getName(rootTemplate),ROOT_ZDTS_INI);
            return ok;
        }
        catch(Exception ex){
            logger.error("Unable to create remote directory for CDN carousel: {}", ex.getMessage());
            return false;
        }
    }

    @Override
    public String getLocation(String name) {
        return "http://127.0.0.1:8080"+location+name;
    }
    
    
    public byte[] getCarouselFileImpl(String remoteName) throws Exception{
        byte [] bytes = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = getHTTPClient();
            HttpPost post = new HttpPost(url+DOWNLOAD);
            
            JSONObject json=new JSONObject();
            json.put(FILE_PATH, location);
            json.put(FILE, remoteName);
        
            StringEntity entity = new StringEntity(json.toString());
            entity.setContentType("application/json");
            post.setEntity(entity);
            
            HttpResponse response = httpClient.execute(post);
            int status=response.getStatusLine().getStatusCode();
            
            if (OK == status){
                byte [] buffer=new byte[4_096];
                InputStream in = response.getEntity().getContent();
                try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
                    int read;
                    while (-1 != (read=in.read(buffer)))
                        baos.write(buffer,0, read);
                    baos.flush();
                    bytes = baos.toByteArray();
                }
            }     
        }
        finally {
            if (null != httpClient){
                try{
                    httpClient.close();
                }
                catch(IOException ex){
                    logger.error(ex.getMessage());
                }
            }
        }
        return bytes;
    }
    
    
    public String generateFileName(String name, long remoteDiff){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, -(Constants.MAX_MINUTES+1));
        long ts = calendar.getTimeInMillis() + remoteDiff;
        return ts+"_"+name;
    }
    
    public List<String> getFilesByName(String name){
        List<String> names = new ArrayList<>();
        
        String mask = "^(\\d+_|)"+name.replace(".", "\\.")+"$";
        List<String> fnames = getFiles();
        
        for (String fname: fnames){
              if (Pattern.matches(mask, fname))
                    names.add(fname);
        }
        
        Collections.sort(names, new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                Pattern pattern  = Pattern.compile("^((\\d+)_|).+$");
                Matcher m1 = pattern.matcher(o1);
                Matcher m2 = pattern.matcher(o2);
                long i1=0,i2=0;
                try {
                    if (m1.matches())
                        i1 = Long.parseLong(m1.group(2));
                }
                catch(Exception ex){
                }
                try {
                    if (m2.matches())
                        i2 =Long.parseLong(m2.group(2));
                }
                catch(Exception ex){
                }
                if (0 == i1 && 0 == i2)
                    return o1.compareTo(o2);
                return (i1<i2) ? -1 : ((i1 == i2) ? 0 : 1);
            }
        });
        return names;
    }
    
    public static List<String> getOldFiles(List<String> names,long oldest){
        List<String> old = new ArrayList<>();
        Pattern pattern = Pattern.compile("^(\\d+)_.+$");
        for (String name: names){
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()){
                long ts = Long.parseLong(matcher.group(1));
                if (ts<oldest)
                    old.add(name);
            }
        }
        Collections.sort(old);
        return old;
    }
}
