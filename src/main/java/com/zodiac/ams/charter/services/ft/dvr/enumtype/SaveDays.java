package com.zodiac.ams.charter.services.ft.dvr.enumtype;

public enum SaveDays {
    SPACE_IS_NEEDED("Space is needed for new recordings", 1),
    USER_DELETE("User delete", 2);

    private final String duration;
    private final int code;
    
    private SaveDays(String duration,int code){
        this.duration = duration;
        this.code = code;
    }

    @Override
    public String toString(){
        return duration;
    }
    
    public String getDiration(){
        return duration;
    }
    
    public int getCode(){
        return code;
    }
    
    public static SaveDays fromCode(int code){
        SaveDays[] days = SaveDays.values();
        for (SaveDays day: days){
            if (code == day.code)
                return day;
        }
        return null;
    }
}
