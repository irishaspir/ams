package com.zodiac.ams.charter.services.ft.defent;

import com.dob.ams.util.DOB7bitUtils;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomReason;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomScreenId;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

public class Group2 extends GroupImpl {
    private static final int BAD_REASON = 7777;
    
    int timeStampBase;
    
    int [] timeStampDelta;
    Reason [] reason;
    ScreenId [] applicationScreenID;	 
    
    public Group2(String mac, EntError error, Reason reason) {
        this(mac, new EntError[] {error}, new Reason[]{reason});
    }
    
    public Group2(String mac, EntError error) {
        this(mac, new EntError[] {error}, null);
    }
    
    public Group2(String mac, EntError[] errors) {
        this(mac, errors, null);
    }
            
    public Group2(String mac, EntError[] errors, Reason [] reason) {
        super(mac, errors);
        timeStampBase = getNow();
        
        timeStampDelta = new int[errors.length]; 
        this.reason = (null == reason) ? new Reason[errors.length] : reason;
        applicationScreenID = new ScreenId[errors.length];
        
        for (int i=0; i< errors.length; i++){
            timeStampDelta[i] = getRandomInt(); 
            if (null == reason)
                this.reason[i] = getRandomReason();
            applicationScreenID[i] = getRandomScreenId();
        }
    }
    
    @Override
    public byte[] getMessage() throws Exception{
        byte [] result = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            baos.write(DOB7bitUtils.encodeUInt(requestId)); 
            baos.write(getGroupDescription(2,errors.length)); //group,count
            baos.write(DOB7bitUtils.encodeUInt(timeStampBase));
            for (int i=0; i< errors.length; i++){
                baos.write(DOB7bitUtils.encodeUInt(errors[i].getCode())); 
                baos.write(DOB7bitUtils.encodeUInt(timeStampDelta[i])); 
                baos.write(DOB7bitUtils.encodeUInt((null == reason[i]) ? BAD_REASON : reason[i].getCode()));
                baos.write(DOB7bitUtils.encodeUInt(applicationScreenID[i].getCode()));
            }
            baos.flush();
            result = baos.toByteArray();
        }
        return result;
    }
    
    @Override
    public JSONObject toJSON(){
        JSONObject obj = new JSONObject();
        //453
        //obj.put("macAddress", mac);
        JSONArray array = new JSONArray();
        for (int i=0; i< errors.length; i++){
            JSONObject o =new JSONObject();
            //453
            o.put("id", mac);
            o.put("errorCode", errors[i].getError());
            o.put("errorDescription", errors[i].getDescription());
            //453
            //o.put("timestamp", String.valueOf(timeStampBase + timeStampDelta[i]));
            //o.put("reason", (null == reason[i]) ? "" : reason[i].toReasonString());
            //o.put("applicationScreenID", applicationScreenID[i].toScreenIdString());
            //453
            o.put("additionalInfo",String.format("timestamp=%d, reason=%s, applicationScreenID=%s", 
                timeStampBase + timeStampDelta[i], null == reason[i] ? "" : reason[i].toReasonString(), applicationScreenID[i].toScreenIdString()));
            array.put(o);
        }
        //453
        //obj.put("errors", array);
        //453
        obj.put("stbError", array);
        return obj;
    }
}
