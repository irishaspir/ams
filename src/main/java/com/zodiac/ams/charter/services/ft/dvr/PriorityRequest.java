package com.zodiac.ams.charter.services.ft.dvr;

import com.dob.ams.util.DOB7bitUtils;
import com.zodiac.ams.charter.services.ft.dvr.enumtype.Method;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;
import static com.zodiac.ams.charter.services.ft.RandomUtils.getRandomInt;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;

public class PriorityRequest implements IDVRObject{
    private String deviceId;
    private Method method = Method.PRIORITIZE;
    private Object priorities;
    
    public static PriorityRequest getRandom(String deviceId, int size){
        PriorityRequest request = new PriorityRequest();
        request.deviceId = deviceId;
        List pri =new ArrayList();
        for (int i=0; i< size; i++)
            pri.add(getRandomInt(1, 1_000_000));
        request.priorities = pri;
        return request;
    }
    
    public static PriorityRequest getRandom(String deviceId){
        return getRandom(deviceId, getRandomInt(0, 5));
    }
    
    public void setPrioities(Object priorities){
        this.priorities = priorities;
    }
    
    public List getPriorities(){
        return (priorities instanceof List) ? (List)priorities : null;
    }
    
    public int size(){
        return (priorities instanceof List) ?  ((List)priorities).size() : 0;
    }
    
    @Override
    public JSONObject toJSON(Set<String> excluded) {
        JSONObject json =  new JSONObject();
        if (null != deviceId)
            json.put("deviceId", deviceId);
        if (null != method)
            json.put("method", method.toString());
        
        JSONArray arr= new JSONArray();
        
        JSONObject obj1 = new JSONObject();
        
        if (priorities instanceof List){
            JSONArray arr1 = new JSONArray();
            for (Object pri: (List)priorities){
                if (pri instanceof Number)
                    arr1.put(((Number)pri).intValue());
                else
                    arr1.put(pri);
            }
            obj1.put("priority_vector", arr1);
        }
        else
        if (priorities instanceof Number)
            obj1.put("priority_vector", ((Number)priorities).intValue());
        else
        if (null != priorities)
            obj1.put("priority_vector", priorities.toString());
            
        arr.put(obj1);
        
        json.put("params", arr);
        return json;
    }

    @Override
    public byte[] getData() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            out.write(DOB7bitUtils.encodeUInt(method.getCode()));
            if (priorities instanceof List){
                List list = (List)priorities;
                out.write(DOB7bitUtils.encodeUInt(list.size()));
                for (Object obj: list)
                    out.write(DOB7bitUtils.encodeUInt(JSONUtils.getInt(obj)));
            }
            return out.toByteArray();
        }
    }
    
    @Override
    public String toString(){
        return toJSON().toString();
    }
}
