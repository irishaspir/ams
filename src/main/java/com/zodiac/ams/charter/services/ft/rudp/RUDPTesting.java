/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zodiac.ams.charter.services.ft.rudp;

import com.dob.ams.transport.zodiac.msg.IZodiacMessage;
import com.dob.ams.transport.zodiac.msg.ZodiacMessage;
import com.dob.test.charter.CharterStbEmulator;

/**
 *
 * @author alexander.filippov
 */
public class RUDPTesting {
    
    private static final String SERVICE_ID = "RUDP_TESTING";
    
    public  static void send(CharterStbEmulator emulator,String id,byte[] data) throws Exception{
        ZodiacMessage message = new ZodiacMessage();
        message.setBodyBytes();
        message.setMessageId(String.valueOf(0));
        message.setSender(SERVICE_ID);
        message.setAddressee(SERVICE_ID);
        message.setMac(Long.parseLong(id,16));
        
        //message.setFlag(ZodiacMessage.FL_PERSISTENT);
        //message.setFlag(ZodiacMessage.FL_RETAIN_SOURCE);
        //message.setFlag(ZodiacMessage.FL_COMPRESSED);
        
        message.setData(data);
        
        IZodiacMessage response = emulator.getRUDPTransport().send(message);
        //logger.info("AMS response: {}",response.toString());
    }    
    
}
