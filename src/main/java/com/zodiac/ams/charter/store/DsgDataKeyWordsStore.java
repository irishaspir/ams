package com.zodiac.ams.charter.store;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.MigrationStart.standardSTBRunning;
import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_3;
import static com.zodiac.ams.charter.tests.dsg.rudp.cm.ConnectionModeSteps.changeConnectionMode;

public class DsgDataKeyWordsStore {

    public class DsgVersionProtocol {
        public static final int VERSION_0 = 0;
        public static final int VERSION_1 = 1;
        public static final int VERSION_2 = 2;
        public static final int VERSION_3 = 3;
    }

    public class ConnectionMode {
        public static final int DOCSIS = 0;
        public static final int ALOHA = 1;
        public static final int DAVIC = 2;
        public static final String DOCSIS_NAME = "DOCSIS";
        public static final String ALOHA_NAME = "Aloha";
        public static final String DAVIC_NAME = "DAVIC";
    }

    public class MigrationStart {
        public static final int migrationDataWillBeSentSoon = 1;
        public static final int standardSTBRunning = 0;
    }

    public static final int UNKNOWN_ENVIRONMENT = -1;

    public class Vendor {
        public static final int UNKNOWN_VENDOR = 0;
        public static final int CISCO = 10;
        public static final int HUMAX = 20;
        public static final int MOTOROLA = 30;
        public static final int PACE = 40;
        public static final String CISCO_STRING = "Cisco";
        public static final String HUMAX_STRING = "Humax";
        public static final String MOTOROLA_STRING = "Motorola";
        public static final String PACE_STRING = "Pace";
    }

    public class DsgRequests {
        public static final int CONNECTION_MODE_CHANGED_NOTIFICATIONS = 0;
        public static final int STB_PARAMETERS_UPDATE = 2;
    }

    public static class DsgData {
        public static int msgType = 0;
        public static int connectionMode = changeConnectionMode();
        public static int boxModel = 516;
        public static int vendor = 30;
        public static long messageTypesMap = 1;
        public static int hubId = 0;
        public static int serviceGroupId = 0;
        public static int migrationStart = standardSTBRunning;
        public static String cmMacAddress = DEVICE_ID_NUMBER_3;
        public static String serialNumber = "666";
        public static long cmIp = 0;
        public static long loadBalancerIpAddress = 0;
        public static int swVersion = 0;
        public static int sdvServiceGroupId = 0;
        public static String swVersionTemp ="";
    }


}
