package com.zodiac.ams.charter.store;

import com.zodiac.ams.common.common.Config;

import static com.zodiac.ams.charter.helpers.DataUtils.decimalToHex;

public class TestKeyWordStore {

    protected final String ERROR_400 = "Status code is 400";
    protected final String ERROR_404 = "Status code is 404";
    protected final String ERROR_500 = "Status code is 500";
    protected final String SUCCESS_200 = "Status code is 200";
    protected final String ERROR_203 = "Status code is 203";
    protected final String INCORRECT_SETTINGS = "Settings are incorrect";
    protected final String requestToSTBMustBeAbsent = "Should be null request to STB";
    protected final String wrongValueInDB = "Wrong meaning in BD";
    protected final String mfIsIncorrect = "Migration flag is incorrect";
    protected final String urlIsIncorrect = "URI is incorrect";
    protected final String macIsNotInBD = "MAC is not in BD";
    public static final String NO_DATA_IN_DB = "There are no data in DB by the request!";
    protected final String NULL_RESPONSE = "Body is absent!";

    //EPG
    protected final String EPG_TEST_FINISHED = "Event finished";
    protected final String PATTERN_EPG_LOG = "Pattern of EPG log doesn't match to actual AMS's log";
    protected final String PATTERN_EPG_VERIFIER_LOG = "Pattern of EPG Verifier log doesn't match to actual verifier's log";

    //CallerId
    protected final String IS_ABSENT_IN_RESPONSE = " is absent in response!";
    protected final String CALLER_ID_REQUEST_IS_INCORRECT = "CallerId request to STB is incorrect!";
    protected final String PATTERN_CALLER_ID_LOG = "Pattern of CallerID log doesn't match to actual AMS's log";

    //DSG
    protected final String IN_DSG_RESPONSE = "in DSG response ";
    public static final String UNKNOWN = "Unknown ";
    protected final String ARE = " are ";
    protected final String NOT_EQUAL = " aren't equal ";
    protected final String NOT_IN_LOG = " isn't in log ";
    protected final String EQUAL = " are equal ";
    protected final String VENDOR_ID = "Vendor Id ";
    protected final String BOX_MODEL_ID = "Box model Id ";
    protected final String ROM_ID = "Rom Id ";
    protected final String HUB_ID = "Hub Id ";
    protected final String CHANGE_TIME_STAMP = "Change Time stamp ";
    protected final String LAST_ACTIVITY_STAMP = "Last activity stamp ";
    protected final String MAC_IS_NOT_IN_MAC_IP = "MAC isn't in MacIp ";
    protected final String AND = " AND ";
    protected final String MIGRATION_START="Migration start flag ";
    protected final String ERROR = "ERROR: ";

    //Rollback
    protected final String ROLLBACK_RESPONSE_IS_INCORRECT = "Rollback response from AMS is incorrect!";
    protected final String ROLLBACK_REQUEST_IS_INCORRECT = "Rollback request from AMS to STB is incorrect!";
    protected final String PATTERN_ROLLBACK_LOG = "Pattern of Rollback log doesn't match to actual AMS's log";

    //STB
    public static final String MAC_NUMBER_1 = new Config().getValidMacNumber1();
    public static final String MAC_NUMBER_2 = new Config().getValidMacNumber2();
    public static final String MAC_NUMBER_3 = new Config().getValidMacNumber3();
    public static final String MAC_NUMBER_4 = new Config().getValidMacNumber4();
    public static final String DEVICE_ID_NUMBER_1 = decimalToHex(MAC_NUMBER_1);
    public static final String DEVICE_ID_NUMBER_2 = decimalToHex(MAC_NUMBER_2);
    public static final String DEVICE_ID_NUMBER_3 = decimalToHex(MAC_NUMBER_3);
    public static final String DEVICE_ID_NUMBER_4 = decimalToHex(MAC_NUMBER_4);

}
