package com.zodiac.ams.charter.store;


import java.io.File;

public class TomcatKeyWordStore {

    public final static String MAIN_SUIT = "main_suit.xml";
    public final static String CHARTER_LOG = "charter.log";
    public final static String CONTEXT_XML = "context.xml";
    public final static String SERVER_XML = "server.xml";
    public final static String STB_MODELS_TXT = "stb_models.txt";
    public final static String VENDORS_TXT = "vendors.txt";
    public final static String ROM_ID_TXT = "rom_id.txt";
    public final static String AMS_PROPERTIES = "ams.properties";
    public final static String CHE_PROPERTIES = "che.properties";
    public final static String STB_PROPERTIES = "stb.properties";
    public final static String FT_PROPERTIES = "ft.properties";
    public final static String EPG_VERIFIER_PROPERTIES = "epg_verifier.properties";
    public final static String USER_HOME = "user.home";
    public final static String USER_NAME = "user.name";
    public final static String USER_DIR = "user.dir";
    public final static String LINEUP_FILE_NAME_FOR_CHECKER = "CA11";
    public final static String SAMPLE_XML = "sample.xml";
    public final static String TESTNG_XML = "testng.xml";
    public final static String TESTCASE_XML = "testcase.xml";
    public final static String LINEUP_JSON = "lineup.json";
    public final static String SETTINGS_DESCRIPTION_SCHEMA_XSD = "SettingsDescriptionSchema.xsd";
    public final static String HOME = "/home/";
    public final static String ROOT = "/";
    public final static String MANIFEST = "MANIFEST.MF";
    public final static String VERSION_AMS = "(version: AMS_[0-9]{1,}.)([0-9]{1,})(.*)";

    public final static String PATH_TO_SUITS = "suits/";
    public final static String PATH_TO_PROPERTIES = "properties" + File.separator;
    public final static String PATH_TO_ETC = "etc/";
    public final static String PATH_TO_EPG_DATA = "epgData/";
    public final static String PATH_TO_CHE_DATA = "dataCHE/";
    public final static String PATH_TO_DATA = "data/";
    public final static String PATH_TO_DSG_DATA = "dsgData/";

    public final static String SUDO_COPY = "sudo cp ";
    public final static String SUDO_CHOWN = "sudo chown ";
    public final static String SUDO_CLEAR = "sudo rm -r ";
    public final static String SUDO_CHMOD = "sudo chmode ";

    public final static String META_INF_PATTERN = "%s/webapps/ams/META-INF/";
    public final static String CONF_PATTERN = "%s/conf";
    public final static String CONF_CHARTER_PATTERN = "%s/conf/charter";
    public final static String TEMP_PATTERN = "%s/temp";
    public final static String ENCODED_PATTERN = "%s/temp/%s/encoded.wb_hdr";
    public final static String LOG_PATTERN = "%s/logs";

    public final static String TOMCAT_START_PATTERN = "sudo service %s start";
    public final static String TOMCAT_STOP_PATTERN = "sudo service %s stop";


    public final static String CONTEXT_URL = "url";
    public final static String CONTEXT_USER = "username";
    public final static String CONTEXT_PASSWORD = "password";
    public final static String CONTEXT_VALUE = "value";
    public final static String CONTEXT_NAME = "name";
    public final static String CONTEXT_RESOURCE = "Resource";
    public final static String CONTEXT_ENVIRONMENT = "Environment";
    public final static String CONTEXT_JDBC_DEF_DS = "jdbc/DEF_DS";
    public final static String CONTEXT_JDBC_DEF_DS1 = "jdbc/DEF_DS1";
    public final static String CONTEXT_CONFIG_DIR = "configDir";
    public final static String CONTEXT_LOG_DIR = "logDir";
    public final static String CONTEXT_DRIVER_CLASS_NAME = "driverClassName";

    public final static String TOMCAT = "tomcat7";
    public final static String AMS_STARTER = "AMS started";
    public final static String AMS_DB_ERRORS_PATTERN = "^([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}.[0-9]{3}) ERROR localhost-startStop-1/DBChecker - ERROR.+";

}
