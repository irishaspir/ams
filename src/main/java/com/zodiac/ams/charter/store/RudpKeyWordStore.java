package com.zodiac.ams.charter.store;

public class RudpKeyWordStore {

    protected final int ZODIAC_MESSAGE_TYPE = 126;
    protected final int ZODIAC_RESPONSE_TYPE = 122;

    public static final int UNKNOWN_ENVIRONMENT = -1;

    public class Senders {
        public static final String SETTINGS_SENDER = "charterSettings";
        public static final String CD_SENDER = "cd";
        public static final String DSG_SENDER = "dsg";
    }

    public class Addressees {
        public static final String SETTINGS_ADDRESSEE = "charterSettings";
        public static final String CD_ADDRESSEE = "cd/0";
        public static final String DSG_ADDRESSEE = "dsg/";
    }

    public class CompanionDevice {
        public static final int TUNE_STB_REQUEST_ID = 1;
        public static final int KEY_PRESS_REQUEST_ID = 2;
        public static final int DEEPLINK_REQUEST_ID = 5;
        public static final int DVR_PLAYBACK_START_REQUEST_ID = 6;
        public static final int DVR_TRICKMODE_REQUEST_ID = 7;

        public static final String DEEPLINK = "DEEPLINK";
        public static final String DVR_RECORDING_PLAYBACK_START = "DVR_PLATBACK_START";
        public static final String DVR_RECORDING_TRICKMODE = "DVR_PLAYBACK_TRICKMODE";

        public static final String OK_CD = "OK";
        public static final String ERROR_CD = "ERROR";
        public static final String INCORRECT_PARAMETER_CD = "INCORRECT_PARAMETER";
        public static final String STB_IN_STANDBY_MODE_CD = "STB_IN_STANDBY_MODE";
        public static final String TARGET_NOT_EXIST_CD = "TARGET_RECORDING_NOT_EXIST";
        public static final String OUT_OF_RANGE_CD = "PARAMETER_OUT_OF_RANGE";
        public static final String DVR_NOT_READY_CD = "DVR_NOT_READY";
        public static final int OK_CD_ID = 0;
        public static final int ERROR_CD_ID = 1;
        public static final int INCORRECT_PARAMETER_CD_ID = 2;
        public static final int STB_IN_STANDBY_MODE_CD_ID = 14;
        public static final int TARGET_NOT_EXIST_CD_ID = 15;
        public static final int OUT_OF_RANGE_CD_ID = 16;
        public static final int DVR_NOT_READY_CD_ID = 17;
        public static final int PARAMS = 22;
    }

}
