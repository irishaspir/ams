package com.zodiac.ams.charter.http.listeners.settings;

/**
 * Created by SpiridonovaIM on 07.10.2016.
 */
public interface GetParams {

    String DEVICE_ID = "deviceid";

    String USER_ID = "useid";

    String HOME_ID = "homeid";

    String NAME = "name";

}
