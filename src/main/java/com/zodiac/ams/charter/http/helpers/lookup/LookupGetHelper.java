package com.zodiac.ams.charter.http.helpers.lookup;

import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.json.JSONObject;

import static com.zodiac.ams.charter.http.listeners.lookup.LookupGetParams.*;

public class LookupGetHelper extends HttpHelper{

    private static Config config = new Config();
    public static JSONObject responseJson;

    private static String sendGetSTBLookupRequestToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String str = response.checkResponse();
        responseJson = response.getResponseJson();
        return str;
    }

    public static String sendGetStbStatusRequestMac(String mac) {
        return sendGetSTBLookupRequestToUrl(createURL(mac));
    }

    private static String createURL(String mac) {
        return config.getAmsUrl() + ServicesURI.STB_LOOKUP + String.format(STB_LOOKUP_REQUEST_WITH_MAC, mac);
    }

}
