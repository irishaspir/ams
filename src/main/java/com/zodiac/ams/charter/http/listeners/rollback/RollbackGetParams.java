package com.zodiac.ams.charter.http.listeners.rollback;

public class RollbackGetParams {

    private static String ROLLBACK_TO_ROVI_FIRMWARE = "RollbackStbs";

    public static String ROLLBACK_TO_ROVI_FIRMWARE_VALID = "?req="+ ROLLBACK_TO_ROVI_FIRMWARE;

    public static String ROLLBACK_TO_ROVI_FIRMWARE_WITH_MISTAKE_IN_REQ_VALUE = "?req="+ ROLLBACK_TO_ROVI_FIRMWARE + "mistake";

}
