package com.zodiac.ams.charter.http.helpers.dsg;

import com.zodiac.ams.charter.services.dsg.DsgJsonOption;
import com.zodiac.ams.charter.services.dsg.DsgJsonOptions;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.services.dsg.DsgJsonOptions.CONNECTION_MODE;
import static com.zodiac.ams.charter.services.dsg.DsgJsonOptions.STB_MAC_ADDRESS;

/**
 * Created by SpiridonovaIM on 17.08.2017.
 */
public class DsgParamsListBuilder {

    public static List<DsgJsonOption> createParams(DsgJsonOptions... ars) {
        //    List<DsgJsonOption> list = createOptionSet(createOption(STB_MAC_ADDRESS.getOption()), CONNECTION_MODE.getOption());
        List<DsgJsonOption> list = new ArrayList<>();
        for (int i = 0; i < ars.length; i++) {
            list = createOptionSet(list, ars[i].getOption());
        }
        return list;
    }

    public static List<DsgJsonOption> createParams() {
        return createOptionSet(createOption(STB_MAC_ADDRESS.getOption()), CONNECTION_MODE.getOption());
    }

    private static List<DsgJsonOption> createOption(DsgJsonOption option) {
        return DsgJsonOption.create()
                .addOption(option)
                .build();
    }

    private static List<DsgJsonOption> createOptionSet(List<DsgJsonOption> options, DsgJsonOption option) {
        List<DsgJsonOption> list = DsgJsonOption.create()
                .addListOptions(options)
                .addOption(option)
                .build();

        return list;
    }


}
