package com.zodiac.ams.charter.http.helpers.settings;

import com.zodiac.ams.charter.http.listeners.settings.GetParams;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.common.Config;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.zodiac.ams.charter.http.ServicesURI.SETTINGS;

/**
 * Created by SpiridonovaIM on 21.08.2017.
 */
public class UrlHelper {

    private static Config config = new Config();


     static String createURLWithDeviceId(String deviceId) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.DEVICE_ID + "=" + deviceId;
    }

     static String createURLWithIncorrectDeviceId(String deviceId) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.DEVICE_ID + "=" + deviceId;
    }

     static String createURLUserId(String deviceId) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.USER_ID + "=" + deviceId;
    }

     static String createURLHomeId(String deviceId) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.HOME_ID + "=" + deviceId;
    }

     static String createURLDeviceIdName(String deviceId, ArrayList<SettingsOption> options) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.DEVICE_ID + "=" + deviceId + createOptionsString(options);
    }

     static String createURLDeviceIdName(String deviceId, String options) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.DEVICE_ID + "=" + deviceId + createOptionsString(options);
    }

     static String createURLUserIdName(String deviceId, ArrayList<SettingsOption> options) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.USER_ID + "=" + deviceId + createOptionsString(options);
    }

     static String createURLHomeIdName(String deviceId, ArrayList<SettingsOption> options) {
        return config.getAmsUrl() + SETTINGS + "?" + GetParams.HOME_ID + "=" + deviceId + createOptionsString(options);
    }

    private static String createOptionsString(ArrayList<SettingsOption> options) {
        StringBuilder str = new StringBuilder();
        for (SettingsOption option : options) {
            try {
                str.append("&");
                str.append(GetParams.NAME);
                str.append("=");
                str.append(URLEncoder.encode(option.getName(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Couldn't create get uri " + e);
            }
        }
        return str.toString();
    }


    private static String createOptionsString(String setting) {
        StringBuilder str = new StringBuilder();
        try {
            str.append("&");
            str.append(GetParams.NAME);
            str.append("=");
            str.append(URLEncoder.encode(setting, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Couldn't create get uri " + e);
        }

        return str.toString();
    }

    public static String getUriPattern(String deviceId) {
        return getUriPattern() + deviceId;
    }

    public static String getUriPattern() {
        return SETTINGS + "?deviceid=STB";
    }

    public static String setUriPattern() {
        return SETTINGS + "?requestor=AMS";
    }



}
