package com.zodiac.ams.charter.http.helpers.epg.csv;


import com.zodiac.ams.charter.services.epg.EpgOption;
import com.zodiac.ams.charter.services.epg.EpgOptions;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.zodiac.ams.charter.bd.ams.tables.LineupsSvcWbxUtils.getTmsServiceIdFromLineupsSvcWbx;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentDayPlusCountStr;


/**
 * Created by SpiridonovaIM on 05.01.2017.
 */
public class CsvDataUtils {

    private int minutesInDay = 1440;
    private String date;
    private String time;
    private int recentTime;
    private int indexDurationRange = 0;
    private int day = 0;
    private String serviseId;
    private int tmsId = 7;


    public String getServiseId() {
        return serviseId;
    }

    public StringBuilder createDate(String stTime, int amountDay, String duration, String count, String breakTime, String breakPeriod) {
        recentTime = minutesInDay - convertTimeToMinutes(stTime);
        date = getCurrentDayPlusCountStr(amountDay);
        day = amountDay;
        time = convertMinutesInTime(recentTime);
        return takeDurationRange(duration, count, breakTime, breakPeriod);
    }

    private StringBuilder takeDurationRange(String duration, String count, String breakTime, String breakPeriod) {
        serviseId = getServiseIdFromBd();
        StringBuilder str = new StringBuilder();
        if (duration.equals("0")) {
            for (int i = 1; i <= Integer.parseInt(count); i++) {
                String dur = durationRange();
                str.append(checkDate(i, dur, breakTime, breakPeriod, setRandomOptions()));
            }
            return str;
        } else {

            for (int i = 1; i <= Integer.parseInt(count); i++) {
                str.append(checkDate(i, duration, breakTime, breakPeriod, setRandomOptions()));
            }
            return str;
        }
    }

    private StringBuilder checkDate(int i, String duration, String breakTime, String breakPeriod, EpgOptions[] options) {
        int x;
        StringBuilder str = new StringBuilder();
        if (Integer.parseInt(breakPeriod) != 0) {
            x = (i % Integer.parseInt(breakPeriod));
        } else x = 1;

        if (x != 0) {
            if (Integer.parseInt(duration) < recentTime) {
                str.append(createRow(duration, "0", day, options));
            } else {
                day++;
                str.append(createRow(duration, "0", day, options));
            }
        } else {
            if (Integer.parseInt(breakTime) + Integer.parseInt(duration) < recentTime) {
                str.append(createRow(duration, breakTime, day, options));
            } else {
                day++;
                str.append(createRow(duration, breakTime, day, options));
            }
        }
        return str;
    }

    private StringBuilder createRow(String duration, String breakTime, int day, EpgOptions[] options) {
        StringBuilder str = new StringBuilder();
        str.append(createEpgOptionsString(time, date, duration, options));
        date = getCurrentDayPlusCountStr(day);
        time = timePlusDuration(Integer.parseInt(duration) + Integer.parseInt(breakTime), time);
        recentTime = minutesInDay - convertTimeToMinutes(time);
        return str;
    }


    private StringBuilder createEpgOptionsString(String time, String date, String duration, EpgOptions[] options) {
        StringBuilder str = new StringBuilder();
        for (EpgOptions one : Arrays.asList(options)) {
            switch (one.getOption().getName()) {
                case "PROGRAM_ID":
                    str.append("\"" + createProgramId() + "\"" + "|");
                    break;
                case "SEASON":
                case "EPISODE":
                    str.append(one.getOption().getDefaultValue() + "|");
                    break;
                case "DURATION":
                    str.append(duration + "|");
                    break;
                case "TIME":
                    str.append("\"" + time + "\"" + "|");
                    break;
                case "DATE":
                    str.append("\"" + date + "\"" + "|");
                    break;
                case "SERVICE_ID":
                case "AWAYTEAMID":
                case "HOMETEAMID":
                    str.append(one.getOption().getValue() + "|");
                    break;
                default:
                    str.append("\"" + one.getOption().getValue() + "\"" + "|");
            }
        }
        str.deleteCharAt(str.lastIndexOf("|"));
        str.append("\n");
        return str;
    }

    private String timePlusDuration(int duration, String previousTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
        LocalTime time = formatter.parseLocalTime(previousTime);
        return String.valueOf(formatter.print(time.plusMinutes(duration)));
    }

    private String timeMinusDuration(String duration, String previousTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
        LocalTime time = formatter.parseLocalTime(previousTime);
        return String.valueOf(formatter.print(time.minusMinutes(Integer.parseInt(duration))));
    }

    private int convertTimeToMinutes(String time) {
        String[] str = time.split(":");
        return Integer.parseInt(str[0]) * 60 + Integer.parseInt(str[1]);
    }

    private String convertMinutesInTime(int totalMinutesInt) {
        int hours = totalMinutesInt / 60;
        String hoursToDisplay = null;
        if (hours == 24) hoursToDisplay = "00";
        else if (hours < 10) hoursToDisplay = "0" + hoursToDisplay;
        else hoursToDisplay = "" + hoursToDisplay;
        int minutesToDisplay = totalMinutesInt % 60;
        String minToDisplay = null;
        if (minutesToDisplay == 0) minToDisplay = "00";
        else if (minutesToDisplay < 10) minToDisplay = "0" + minutesToDisplay;
        else minToDisplay = "" + minutesToDisplay;
        return "" + hoursToDisplay + ":" + minToDisplay;
    }

    private String[] serviceIdRange() {
        List<String> list = getTmsServiceIdFromLineupsSvcWbx();
        String[] array = new String[list.size()];
        return list.toArray(array);
    }

    private String durationRange() {
        List<String> durationList = new ArrayList<>(Arrays.asList(EpgOptions.DURATION.getOption().getRange()));
        String m = null;
        m = durationList.get(indexDurationRange);
        indexDurationRange++;
        if (indexDurationRange == durationList.size()) {
            this.indexDurationRange = 0;
        }
        return m;
    }


    private String getServiseIdFromBd() {
        EpgOption option = EpgOptions.SERVICE_ID.getOption();
        setValueRange(option, serviceIdRange());
        setValue(option, getRandomValueFromRange(option));
        return option.getValue();
    }

    private EpgOptions[] setRandomOptions() {
        EpgOptions[] options = EpgOptions.values();
        for (EpgOptions one : options) {
            if ((!one.getOption().getName().equals("SERVICE_ID")) && (!one.getOption().getName().equals("DURATION"))
                    && (!one.getOption().getName().equals("SEASON")) && (!one.getOption().getName().equals("EPISODE"))
                    && (one.getOption().getRange().length > 0))
                setValue(one.getOption(), getRandomValueFromRange(one.getOption()));
        }
        return options;
    }


    private void setValueRange(EpgOption option, String[] values) {
        option.setRange(values);
    }


    private void setValue(EpgOption option, String value) {
        option.setValue(value);
    }

    private String getRandomValueFromRange(EpgOption option) {
        int randomIndex = (int) (Math.random() * option.getRange().length);
        return option.getRange()[randomIndex];

    }

    private String createProgramId() {
        long randomIndex = (long) (new Random().nextDouble()*10000000L);
        return "SH0" + randomIndex + "0000";
    }


}
