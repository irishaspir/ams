package com.zodiac.ams.charter.http.helpers.callerId;

public class CallerIdStringDataUtils {

    public static String error203UnknownMAC(String unknownMAC) {
        return "MAC-addresses not found: Fail MAC addresses lists:map={BOX_NOT_FOUND=[" + unknownMAC +"]}}";
    }

    public static String  error203InvalidMAC(String invalidMAC) {
        return "MAC-addresses not found: Fail MAC addresses lists:map={NOT_INTERNAL_MAC_FAIL=[" + invalidMAC +"]}}";
    }

    private static String NULL_OR_EMPTY_PARAMETER = "Parameter '%s' can not be null or empty";;
    public static final String ID_IS_NULL_OR_EMPTY= String.format(NULL_OR_EMPTY_PARAMETER, "ID");
    public static final String CALLER_NAME_IS_NULL_OR_EMPTY =  String.format(NULL_OR_EMPTY_PARAMETER, "CallerName");
    public static final String PHONE_NUMBER_IS_NULL_OR_EMPTY =  String.format(NULL_OR_EMPTY_PARAMETER, "CallerPhoneNumber");
    public static final String CALL_ID_IS_NULL_OR_EMPTY =  String.format(NULL_OR_EMPTY_PARAMETER, "CallID");

    public String patternSubscribeRequestBody(String ... macs){
        StringBuilder listOfMACs = new StringBuilder();
        for (String mac:macs)
            listOfMACs.append(mac).append(",");
        listOfMACs.deleteCharAt(listOfMACs.lastIndexOf(","));
        return listOfMACs.toString();
    }

}
