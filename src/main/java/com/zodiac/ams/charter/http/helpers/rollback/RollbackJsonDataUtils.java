package com.zodiac.ams.charter.http.helpers.rollback;

import com.zodiac.ams.common.common.Config;
import org.json.JSONArray;
import org.json.JSONObject;

public class RollbackJsonDataUtils {

    private Config config = new Config();

    //Operation Statuses
    private String OK = "OK";
    private String NOT_AVAILABLE = "Not available";
    private String FAILED = "FAILED";

    private enum Parameters {devices, deviceId, status, errorMessage}

    public static JSONObject createRequestBoby(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        JSONArray arrayMacs = new JSONArray();
        for (String device_id:device_ids) arrayMacs.put(device_id);
        resultJson.put(Parameters.devices.name(), arrayMacs);
        return resultJson;
    }

    public static JSONObject createRequestBodyWithoutDeviceIds() {
        JSONObject resultJson = new JSONObject();
        resultJson.put(Parameters.devices.name(), new JSONArray());
        return resultJson;
    }

    public static JSONObject createRequestBobyWithMistakeInDevicesParameter(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        JSONArray arrayMacs = new JSONArray();
        for (String device_id:device_ids) arrayMacs.put(device_id);
        resultJson.put(Parameters.devices.name() + "mistake", arrayMacs);
        return resultJson;
    }

    public static JSONObject createRequestBobyWithoutJsonArrayBrackets(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        for (String device_id:device_ids)
        resultJson.put(Parameters.devices.name(), device_id);
        return resultJson;
    }


    public JSONObject getSuccessfulResponse(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        JSONArray arrayMacs = new JSONArray();
        for (String device_id:device_ids) arrayMacs.put(
                new JSONObject().put(Parameters.deviceId.name(), device_id)
                                .put(Parameters.status.name(), OK));
        resultJson.put(Parameters.devices.name(), arrayMacs);
        return resultJson;
    }

    public JSONObject getSuccessfulResponseWithStatusFailed(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        JSONArray arrayMacs = new JSONArray();
        for (String device_id:device_ids) arrayMacs.put(
                new JSONObject().put(Parameters.deviceId.name(), device_id)
                        .put(Parameters.status.name(), FAILED)
                        .put(Parameters.errorMessage.name(),
                        "Resource text is missing for STATUS_STB_RESPONSE_ERROR"));
        resultJson.put(Parameters.devices.name(), arrayMacs);
        return resultJson;
    }

    public JSONObject getResponseIfSTBIsUnavailable(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        JSONArray arrayMacs = new JSONArray();
        for (String device_id:device_ids) arrayMacs.put(
                new JSONObject().put(Parameters.deviceId.name(), device_id)
                                .put(Parameters.status.name(), NOT_AVAILABLE)
                                .put(Parameters.errorMessage.name(),
                                "RB-ST-006 Delivery error: Can not connect to STB with stbId=" +
                                 device_id + " using ip=" + config.getAmsIp() + ". n/a\""));
        resultJson.put(Parameters.devices.name(), arrayMacs);
        return resultJson;
    }

    public JSONObject getResponseIfMacIsUnknown(String ... device_ids) {
        JSONObject resultJson = new JSONObject();
        JSONArray arrayMacs = new JSONArray();
        for (String device_id:device_ids) arrayMacs.put(
                new JSONObject().put(Parameters.deviceId.name(), device_id)
                        .put(Parameters.status.name(), FAILED)
                        .put(Parameters.errorMessage.name(),
                                "RB-ST-006 Delivery error: Can not find STB with stbId=" + device_id));
        resultJson.put(Parameters.devices.name(), arrayMacs);
        return resultJson;
    }

    public JSONObject getResponseIfRequestBodyEmptyJson() {
        JSONObject resultJson = new JSONObject();
        resultJson.put(Parameters.devices.name(), new JSONArray());
        return resultJson;
    }
}
