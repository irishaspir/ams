package com.zodiac.ams.charter.http.helpers.dsg;

import com.zodiac.ams.charter.http.helpers.dsg.json.JsonDataUtils;
import com.zodiac.ams.charter.services.dsg.DsgJsonOption;
import org.json.JSONObject;

import java.util.List;

import static com.zodiac.ams.charter.http.helpers.dsg.DsgParamsListBuilder.createParams;
import static com.zodiac.ams.charter.services.dsg.DsgJsonOptions.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.*;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.Vendor.*;


public class DsgHttpDataUtils extends DsgDataUtils {

    private static JsonDataUtils jsonDataUtils = new JsonDataUtils();
    private static JSONObject jsonObject;

    public static JSONObject getJsonObjectWithoutNullValue() {
        return deleteNullValue(jsonObject);
    }

    public static JSONObject dsgPostPattern(List options) {
        return jsonObject = jsonDataUtils.dsgRequestPattern(options);
    }

    private static JSONObject setDeviseIdAndConnectionMode(List<DsgJsonOption> options, Object deviceId, int connectionMode) {
        JSONObject jsonOptions = dsgPostPattern(options);
        changeDeviseId(jsonOptions, deviceId);
        changeConnectionMode(jsonOptions, connectionMode);
        return jsonOptions;
    }


    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode) {
        return jsonObject = setDeviseIdAndConnectionMode(createParams(), deviceId, connectionMode);
    }


    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE, MODEL);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel, int vendor) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE, VENDOR, MODEL);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID, SGID);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId, Object loadBalancer) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID, SGID, AMSIP);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        changeLoadBalancer(jsonObject, String.valueOf(loadBalancer));
        return jsonObject;
    }
    
    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId, Object loadBalancer, Object swVersion) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID, SGID, AMSIP, SW_VERSION);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        changeLoadBalancer(jsonObject, String.valueOf(loadBalancer));
        changeSwVersion(jsonObject, String.valueOf(swVersion));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId, Object loadBalancer,
                                            Object swVersion, Object sdvServiceGroupId) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID, SGID, AMSIP, SW_VERSION, SDV_SGID);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        changeLoadBalancer(jsonObject, String.valueOf(loadBalancer));
        changeSwVersion(jsonObject, String.valueOf(swVersion));
        changeSdvServiceGroupId(jsonObject, String.valueOf(sdvServiceGroupId));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId, Object loadBalancer,
                                            Object swVersion, Object sdvServiceGroupId, Object cmMacAddress) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID, SGID, AMSIP, SW_VERSION, SDV_SGID, CM_MAC_ADDRESS);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        changeLoadBalancer(jsonObject, String.valueOf(loadBalancer));
        changeSwVersion(jsonObject, String.valueOf(swVersion));
        changeSdvServiceGroupId(jsonObject, String.valueOf(sdvServiceGroupId));
        changeCmMacAddress(jsonObject, String.valueOf(cmMacAddress));
        return jsonObject;
    }

    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId, Object loadBalancer,
                                            Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, Object serialNumber) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, CONNECTION_MODE,VENDOR, MODEL, HUB_ID, SGID, AMSIP, SW_VERSION, SDV_SGID, CM_MAC_ADDRESS, SERIAL_NUMBER);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        changeLoadBalancer(jsonObject, String.valueOf(loadBalancer));
        changeSwVersion(jsonObject, String.valueOf(swVersion));
        changeSdvServiceGroupId(jsonObject, String.valueOf(sdvServiceGroupId));
        changeCmMacAddress(jsonObject, String.valueOf(cmMacAddress));
        changeSerialNumber(jsonObject, String.valueOf(serialNumber));
        return jsonObject;
    }


    public static JSONObject dsgPostPattern(Object deviceId, int connectionMode, Object boxModel,Object vendor, Object hubId, Object serviceGroupId, Object loadBalancer,
                                            Object swVersion, Object sdvServiceGroupId, Object cmMacAddress, Object serialNumber, Object cmIp) {
        List<DsgJsonOption> options = createParams(STB_MAC_ADDRESS, VCT_ID, SGID, AMSIP, SDV_SGID, CM_IP, CONNECTION_MODE, SW_VERSION, TSID,VENDOR, MODEL,HOST_RF_IP, SW_VERSION_TMP, SERIAL_NUMBER, CM_MAC_ADDRESS);
        jsonObject = setDeviseIdAndConnectionMode(options, deviceId, connectionMode);
        changeBoxModel(jsonObject, String.valueOf(boxModel));
        changeVendor(jsonObject, String.valueOf(vendor));
        changeHubId(jsonObject, String.valueOf(hubId));
        changeServiceGroupId(jsonObject, String.valueOf(serviceGroupId));
        changeLoadBalancer(jsonObject, String.valueOf(loadBalancer));
        changeSwVersion(jsonObject, String.valueOf(swVersion));
        changeSdvServiceGroupId(jsonObject, String.valueOf(sdvServiceGroupId));
        changeCmMacAddress(jsonObject, String.valueOf(cmMacAddress));
        changeSerialNumber(jsonObject, String.valueOf(serialNumber));
        changeCmIp(jsonObject, String.valueOf(cmIp));
        return jsonObject;
    }

    private static void changeDeviseId(JSONObject options, Object id) {
        jsonDataUtils.changeOption(options, STB_MAC_ADDRESS.getOption().getName(), String.valueOf(id));
    }

    private static void changeConnectionMode(JSONObject options, Object id) {
        switch ((int) id) {
            case 0:
                jsonDataUtils.changeOption(options, CONNECTION_MODE.getOption().getName(), DOCSIS_NAME);
                break;
            case 1:
                jsonDataUtils.changeOption(options, CONNECTION_MODE.getOption().getName(), ALOHA_NAME);
                break;
            case 2:
                jsonDataUtils.changeOption(options, CONNECTION_MODE.getOption().getName(), DAVIC_NAME);
                break;
        }
    }

    private static void changeBoxModel(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, MODEL.getOption().getName(), id);
    }

    private static void changeHubId(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, HUB_ID.getOption().getName(), id);
        jsonDataUtils.changeOption(options, VCT_ID.getOption().getName(), id);
    }

    private static void changeVendor(JSONObject options, String id) {
        switch (Integer.valueOf(id)) {
            case CISCO:
                jsonDataUtils.changeOption(options, VENDOR.getOption().getName(), CISCO_STRING);
                break;
            case HUMAX:
                jsonDataUtils.changeOption(options, VENDOR.getOption().getName(), HUMAX_STRING);
                break;
            case MOTOROLA:
                jsonDataUtils.changeOption(options, VENDOR.getOption().getName(), MOTOROLA_STRING);
                break;
            case PACE:
                jsonDataUtils.changeOption(options, VENDOR.getOption().getName(), PACE_STRING);
                break;
            case 999:
                jsonDataUtils.changeOption(options, VENDOR.getOption().getName(), "Unknown");
                break;
        }
    }

    private static void changeServiceGroupId(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, SGID.getOption().getName(), id);
        jsonDataUtils.changeOption(options, TSID.getOption().getName(), id);
        //    jsonDataUtils.changeOption(options, "AMSIP", "${json-unit.ignore}");
    }

    private static void changeCmMacAddress(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, CM_MAC_ADDRESS.getOption().getName(), id);
    }

    private static void changeLoadBalancer(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, AMSIP.getOption().getName(), id);
        jsonDataUtils.changeOption(options, HOST_RF_IP.getOption().getName(), id);
    }

    private static void changeSwVersion(JSONObject options, Object id) {
        jsonDataUtils.changeOption(options, SW_VERSION.getOption().getName(), ((id.equals("0")) ? "" : String.valueOf(id)));
    }

    private static void changeSwVersionTMP(JSONObject options, Object id) {
        jsonDataUtils.changeOption(options, SW_VERSION_TMP.getOption().getName(), String.valueOf(id));
    }

    private static void changeSdvServiceGroupId(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, SDV_SGID.getOption().getName(), id);
    }

    private static void changeSerialNumber(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, SERIAL_NUMBER.getOption().getName(), id);
    }

    private static void changeCmIp(JSONObject options, String id) {
        jsonDataUtils.changeOption(options, CM_IP.getOption().getName(), id);
        //  jsonDataUtils.changeOption(options, "Host RF IP", id);
    }

    private static JSONObject deleteNullValue(JSONObject jsonObject) {
        return jsonDataUtils.deleteValue(jsonObject);
    }

}
