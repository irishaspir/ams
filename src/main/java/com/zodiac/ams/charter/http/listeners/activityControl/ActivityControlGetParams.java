package com.zodiac.ams.charter.http.listeners.activityControl;

public class ActivityControlGetParams {

    public static String SPECIAL_SYMBOLS = "!@-=!*()_!-!";

    public static String DIGITS = "12344687";

    private static String STB_STATUS_BY_MAC = "StbStatusByMac";

    public static String STB_STATUS_REQUEST_INVALID_REQ = "?re="+ STB_STATUS_BY_MAC + "&deviceId=%s";

    public static String STB_STATUS_REQUEST_REQ = "?re=%s&deviceId=%s";

    public static String STB_STATUS_REQUEST_DEVICE_ID = "?req="+ STB_STATUS_BY_MAC + "&deviceId=%s";

    public static String STB_STATUS_REQUEST_WITHOUT_REQ = "?deviceId=%s";

    public static String STB_STATUS_REQUEST_WITHOUT_DEVICE_ID = "?req=" + STB_STATUS_BY_MAC;
}
