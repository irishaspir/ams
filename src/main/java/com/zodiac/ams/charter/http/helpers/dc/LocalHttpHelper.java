package com.zodiac.ams.charter.http.helpers.dc;

import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.apache.commons.collections.MapUtils;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.Map;

import static com.zodiac.ams.charter.http.ServicesURI.DC;

/**
 * The local http helper.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 * @author <a href="mailto:tigran.abaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class LocalHttpHelper extends HttpHelper {
    private static final String PATH_FILE_TO_PROCESS = "PATH_FILE_TO_PROCESS";

    private static final Config config = new Config();

    public static LocalHttpResponse<?> sendFileToProcess(Map<String, Object> paramMap) throws URISyntaxException {
        String url = createAmsUrlWithParamMap(PATH_FILE_TO_PROCESS, paramMap);
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        return new LocalHttpResponse<>(response);
    }

    private static String createAmsUrlWithParamMap(String path, Map<String, Object> paramMap) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(config.getAmsUrl() + DC + path);
        if (MapUtils.isNotEmpty(paramMap)) {
            for (Map.Entry<String, ?> paramMapEntry : paramMap.entrySet()) {
                uriBuilder.addParameter(paramMapEntry.getKey(), String.valueOf(paramMapEntry.getValue()));
            }
        }

        return uriBuilder.toString();
    }
}
