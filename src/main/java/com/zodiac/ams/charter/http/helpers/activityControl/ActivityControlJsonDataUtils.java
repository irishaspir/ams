package com.zodiac.ams.charter.http.helpers.activityControl;

import com.zodiac.ams.charter.services.activityControl.ActivityControlOptions;
import org.json.JSONObject;


public class ActivityControlJsonDataUtils {

    public JSONObject createResponseJsonWithNetworkType(String networkType) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ActivityControlOptions.STB_MAC.getOption().getNameJSON(),
                ActivityControlOptions.STB_MAC.getOption().getValueJSON());
        resultJson.put(ActivityControlOptions.REGISTRATION_TIME.getOption().getNameJSON(),
                ActivityControlOptions.REGISTRATION_TIME.getOption().getValueJSON());
        resultJson.put(ActivityControlOptions.LAST_ACTIVITY_TIME.getOption().getNameJSON(),
                ActivityControlOptions.LAST_ACTIVITY_TIME.getOption().getValueJSON());
        resultJson.put(ActivityControlOptions.NETWORK_TYPE.getOption().getNameJSON(),
                networkType);
        resultJson.put(ActivityControlOptions.LAST_NETWORK_CHANGE_TIME.getOption().getNameJSON(),
                ActivityControlOptions.LAST_NETWORK_CHANGE_TIME.getOption().getValueJSON());
        return resultJson;
    }

}
