package com.zodiac.ams.charter.http.helpers.epg;


import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;

/**
 * Created by Administrator on 12.12.2016.
 */
public class EpgGetHelper extends HttpHelper {

    private static Config config = new Config();
    private static String[] responseCsv;

    public static String sendUpdateEpgDataRequest() {
        return sendGetEpgToUrl(createURL());

    }

    private static String sendGetEpgToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String str = response.checkResponse();
        responseCsv = response.getResponseCsv();
        return str;
    }

    private static String createURL() {
        return config.getAmsUrl() + ServicesURI.UPDATE_EPG_DATA;
    }

    private static String sendGetEpgToUrlSegment(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String str = response.checkResponse();
        responseCsv = response.getResponseCsv();
        return str;
    }


}
