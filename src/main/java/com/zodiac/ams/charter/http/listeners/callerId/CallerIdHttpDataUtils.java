package com.zodiac.ams.charter.http.listeners.callerId;

import com.zodiac.ams.charter.http.ServicesURI;

public class CallerIdHttpDataUtils {

    public String nullResponse() {
        return null;
    }

    public String getCallerIdSubscriptionUriPattern() {
        return ServicesURI.CALLER_ID + "/register";
    }

    public String getCallerIdUnsubscriptionUriPattern() {
        return ServicesURI.CALLER_ID + "/unregister";
    }
}
