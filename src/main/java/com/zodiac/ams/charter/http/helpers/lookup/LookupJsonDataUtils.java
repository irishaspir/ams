package com.zodiac.ams.charter.http.helpers.lookup;

import org.json.JSONObject;

import java.util.List;


public class LookupJsonDataUtils {

    public JSONObject createJsonResponse() {
        JSONObject resultJson = new JSONObject();
        resultJson.put("lineupId", "1");
        resultJson.put("TZ","CST6CDT,M3.2.0/2,M11.1.0");
        resultJson.put("groupCdl", "1");
        resultJson.put("groupCds","1");
        return resultJson;
    }

    public static List<String> nullResponse(){
        return null;
    }
}
