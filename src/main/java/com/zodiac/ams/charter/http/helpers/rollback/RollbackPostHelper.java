package com.zodiac.ams.charter.http.helpers.rollback;

import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.json.JSONObject;

import static com.zodiac.ams.charter.http.helpers.rollback.RollbackJsonDataUtils.*;
import static com.zodiac.ams.charter.http.helpers.rollback.RollbackStringDataUtils.createBodyRequestAsString;
import static com.zodiac.ams.charter.http.listeners.rollback.RollbackGetParams.*;

public class RollbackPostHelper extends HttpHelper{

    private static Config config = new Config();
    public static JSONObject responseJson;
    public static String responseIsAbsent;
    public static String responseString;

    private static String sendRollbackToRoviFirmwareToUrl(String url, JSONObject requestBody) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendPost(url, requestBody)));
        return getResponseBody(response);
    }

    private static String sendRollbackToRoviFirmwareToUrl(String url, String requestBody) {
        return getResponseBody(new ResponseListener(new HttpResponseEntity(sendPost(url, requestBody))));
    }

    private static String sendRollbackToRoviFirmwareToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendPost(url)));
        return getResponseBody(response);
    }

    private static String getResponseBody(ResponseListener responseListener){
        String responseCode = responseListener.checkResponse();
        responseJson = responseListener.getResponseJson();
        responseIsAbsent =  responseListener.getResponseIsAbsent();
        responseString  = responseListener.getResponseString();
        return responseCode;
    }

    public static String sendRollbackToRoviFirmware(String ... device_ids) {
        return sendRollbackToRoviFirmwareToUrl(createURL(), createRequestBoby(device_ids));
    }

    public static String sendRollbackToRoviFirmwareWithMistakeInReqValue(String device_ids) {
        return sendRollbackToRoviFirmwareToUrl(createURLWithMistakeInReqValue(), createRequestBoby(device_ids));
    }

    public static String sendRollbackToRoviFirmwareWithInvalidServiceName(String device_ids) {
        return sendRollbackToRoviFirmwareToUrl(createURLWithMistakeInServiceName(), createRequestBoby(device_ids));
    }

    public static String sendRollbackToRoviFirmwareWithEmptyJson() {
        return sendRollbackToRoviFirmwareToUrl(createURL(), new JSONObject());
    }

    public static String sendRollbackToRoviFirmwareWithMistakeInDevicesParameter(String mac) {
        return sendRollbackToRoviFirmwareToUrl(createURL(), createRequestBobyWithMistakeInDevicesParameter(mac));
    }

    public static String sendRollbackToRoviFirmwareStringInsteadOfJsonInBody() {
        return sendRollbackToRoviFirmwareToUrl(createURL(), createBodyRequestAsString());
    }

    public static String sendRollbackToRoviFirmwareWithoutJsonArrayBrackets(String device_id) {
        return sendRollbackToRoviFirmwareToUrl(createURL(), createRequestBobyWithoutJsonArrayBrackets(device_id));
    }

    public static String sendRollbackToRoviFirmwareWithoutDeviceIds() {
        return sendRollbackToRoviFirmwareToUrl(createURL(), createRequestBodyWithoutDeviceIds());
    }

    public static String sendRollbackToRoviFirmwareWithoutBody() {
        return sendRollbackToRoviFirmwareToUrl(createURL());
    }

    private static String createURL() {
        return config.getAmsUrl() + ServicesURI.ROLLBACK + ROLLBACK_TO_ROVI_FIRMWARE_VALID;
    }

    private static String createURLWithMistakeInReqValue() {
        return config.getAmsUrl() + ServicesURI.ROLLBACK + ROLLBACK_TO_ROVI_FIRMWARE_WITH_MISTAKE_IN_REQ_VALUE;
    }

    private static String createURLWithMistakeInServiceName() {
        return config.getAmsUrl() + ServicesURI.ROLLBACK + "mistake" + ROLLBACK_TO_ROVI_FIRMWARE_VALID;
    }
}
