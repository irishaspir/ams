package com.zodiac.ams.charter.http.listeners.dsg;

public class DsgGetParams {

    public static String SPECIAL_SYMBOLS = "!@-=!*()_!-!";

    public static String DIGITS = "123446873433";

    private static String STB_NETWORK_TYPE_BY_MAC = "StbNetworkTypeByMac";

    private static String STB_POPULATION_DISTRIBUTION = "StbPopulationDistribution";

    public static String STB_NETWORK_TYPE_REQUEST_INVALID_REQ = "?re=" + STB_NETWORK_TYPE_BY_MAC + "&deviceId=%s";

    public static String STB_POPULATION_DISTRIBUTION_REQUEST_INVALID_REQ = "?re=" + STB_POPULATION_DISTRIBUTION;

    public static String STB_NETWORK_TYPE_REQUEST_REQ = "?req=%s&deviceId=%s";

    public static String STB_POPULATION_DISTRIBUTION_REQUEST_REQ = "?req=%s";

    public static String STB_POPULATION_DISTRIBUTION_REQUEST = "?req=" + STB_POPULATION_DISTRIBUTION + "&deviceId=%s";

    public static String STB_NETWORK_TYPE_REQUEST_DEVICE_ID = "?req=" + STB_NETWORK_TYPE_BY_MAC + "&deviceId=%s";

    public static String STB_REQUEST_WITHOUT_REQ = "?deviceId=%s";

    public static String STB_NETWORK_TYPE_REQUEST_WITHOUT_DEVICE_ID = "?req=" + STB_NETWORK_TYPE_BY_MAC;
}
