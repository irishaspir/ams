package com.zodiac.ams.charter.http.helpers.dsg;


import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.json.JSONObject;

import static com.zodiac.ams.charter.http.listeners.dsg.DsgGetParams.*;
import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_1;


public class DsgGetHelper extends HttpHelper {

    private static Config config = new Config();
    public static JSONObject responseJson;
    public static String responseString;

    private static String sendGetRequestToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String responseCode = response.checkResponse();
        responseJson = response.getResponseJson();
        responseString = response.getResponseString();
        return responseCode;
    }

    public static String sendGetStbNetworkTypeRequestToUrlRequestWithMistakeInReq() {
        return sendGetRequestToUrl(createSTBNetworkTypeURLWithMistakeInReq());
    }

    public static String sendGetStbNetworkTypeRequestReq(String req) {
        return sendGetRequestToUrl(createSTBNetworkTypeURLWithReq(req));
    }

    public static String sendGetStbPopulationDistributionRequestToUrlWithMistakeInReq() {
        return sendGetRequestToUrl(createSTBPopulationDistributionURLWithMistakeInReq());
    }

    public static String sendGetStbPopulationDistributionRequestReq(String req) {
        return sendGetRequestToUrl(createSTBPopulationDistributionURLWithReq(req));
    }

    public static String sendGetStbPopulationDistributionRequest() {
        return sendGetRequestToUrl(createSTBPopulationDistributionURL());
    }

    public static String sendGetStbNetworkTypeRequestDeviceId(String deviceId) {
        return sendGetRequestToUrl(createSTBNetworkTypeURLWithDeviceId(deviceId));
    }

    public static String sendGetStbNetworkTypeRequestWithoutReq() {
        return sendGetRequestToUrl(createSTBNetworkTypeURLWithoutReq());
    }

    public static String sendGetStbNetworkTypeRequestWithoutDeviceId() {
        return sendGetRequestToUrl(createSTBNetworkTypeURLWithoutDeviceId());
    }

    private static String createSTBNetworkTypeURLWithMistakeInReq() {
        return config.getAmsUrl() + ServicesURI.DSG + String.format(STB_NETWORK_TYPE_REQUEST_INVALID_REQ, DEVICE_ID_NUMBER_1);
    }

    private static String createSTBPopulationDistributionURLWithMistakeInReq() {
        return config.getAmsUrl() + ServicesURI.DSG + STB_POPULATION_DISTRIBUTION_REQUEST_INVALID_REQ;
    }

    private static String createSTBNetworkTypeURLWithReq(String req) {
        return config.getAmsUrl() + ServicesURI.DSG + String.format(STB_NETWORK_TYPE_REQUEST_REQ, req, DEVICE_ID_NUMBER_1);
    }

    private static String createSTBPopulationDistributionURLWithReq(String req) {
        return config.getAmsUrl() + ServicesURI.DSG + String.format(STB_POPULATION_DISTRIBUTION_REQUEST_REQ, req, DEVICE_ID_NUMBER_1);
    }

    private static String createSTBPopulationDistributionURL() {
        return config.getAmsUrl() + ServicesURI.DSG +  String.format(STB_POPULATION_DISTRIBUTION_REQUEST,DEVICE_ID_NUMBER_1);
    }

    private static String createSTBNetworkTypeURLWithoutReq() {
        return config.getAmsUrl() + ServicesURI.DSG + String.format(STB_REQUEST_WITHOUT_REQ, DEVICE_ID_NUMBER_1);
    }

    private static String createSTBNetworkTypeURLWithDeviceId(String deviceId) {
        return config.getAmsUrl() + ServicesURI.DSG + String.format(STB_NETWORK_TYPE_REQUEST_DEVICE_ID, deviceId);
    }

    private static String createSTBNetworkTypeURLWithoutDeviceId() {
        return config.getAmsUrl() + ServicesURI.DSG + STB_NETWORK_TYPE_REQUEST_WITHOUT_DEVICE_ID;
    }

}
