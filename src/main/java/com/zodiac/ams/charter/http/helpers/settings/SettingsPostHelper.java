package com.zodiac.ams.charter.http.helpers.settings;


import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.charter.http.helpers.settings.json.JsonDataUtils;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import com.zodiac.ams.common.logging.Logger;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.http.helpers.settings.json.JsonDataUtils.deleteSettings55And59And60Keys;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;


public class SettingsPostHelper extends HttpHelper {

    private static Config config = new Config();
    private static JsonDataUtils jsonDataUtils = new JsonDataUtils();
    public static JSONObject responseJson;

    private static String createURL() {
        return config.getAmsUrl() + ServicesURI.SETTINGS;
    }

    private static String sendPostSettingsToUrl(String url, JSONObject json) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendPost(url, json)));
        String str = response.checkResponse();
        responseJson = response.getResponseJson();
        return str;
    }

    public static int getNum(JSONObject responseJson) {
        Pattern p = Pattern.compile("(.*, )(.*, )(.*)\\)");
        Matcher m = p.matcher(responseJson.getJSONObject("settings").getJSONObject("error").get("message").toString());
        if(m.find()){
            m.group(2);
        }
        return Integer.parseInt(m.group(3));
    }

    public static String sendPostRangeSettingsValue(String deviceId, ArrayList<SettingsOption> options, String newValue) {
        JSONObject json = jsonDataUtils.deviceIdRequest(options, deviceId);
        jsonDataUtils.setNewValue(json, newValue);
        deleteSettings55And59And60Keys(json);
        Logger.info("Spoiled request: " + json.toString());
        return sendPostSettingsToUrl(createURL(), json);
    }


    public static String sendPostInvalidSettingsValue(String deviceId, ArrayList<SettingsOption> options) {
        JSONObject json = jsonDataUtils.deviceIdRequest(options, deviceId);
        jsonDataUtils.invalidOptionsEachSecondValue(json);
        deleteSettings55And59And60Keys(json);
        Logger.info("Spoiled request: " + json.toString());
        return sendPostSettingsToUrl(createURL(), json);
    }

    public static String sendPostInvalidSettingsName(String deviceId, ArrayList<SettingsOption> options) {
        JSONObject json = jsonDataUtils.deviceIdRequest(options, deviceId);
        jsonDataUtils.invalidEachSecondSettingsName(json);
        deleteSettings55And59And60Keys(json);
        Logger.info("Spoiled request: " + json.toString());
        return sendPostSettingsToUrl(createURL(), json);
    }


    public static String sendPostSettingsAllList(String deviceId) {
        JSONObject json = jsonDataUtils.deviceIdRequest(allSettings(), deviceId);
        Logger.info("Request json: " + json.toString());
        deleteSettings55And59And60Keys(json);
        return sendPostSettingsToUrl(createURL(), json);
    }

    public static String sendPostInvalidDeviceId(String deviceId) {
        JSONObject json = jsonDataUtils.deviceIdRequest(allSettings(), deviceId);
        Logger.info("Request json: " + json.toString());
        deleteSettings55And59And60Keys(json);
        return sendPostSettingsToUrl(createURL(), json);
    }

    public static String sendPostEmptySettings() {
        return sendPostSettingsToUrl(createURL(), jsonDataUtils.emptyResponse());
    }

    public static String sendPostSettingsDeviseIdNames(String deviceId, ArrayList<SettingsOption> options) {
        if (options.size() == 0) throw new RuntimeException("Settings parameters are absent");
        else {
            JSONObject json = jsonDataUtils.deviceIdRequest(options, deviceId);
            Logger.info("Request json: " + json.toString());
            deleteSettings55And59And60Keys(json);
            return sendPostSettingsToUrl(createURL(), json);
        }
    }

    public static String sendPostSettingsCallerIdNotification(String deviceId, ArrayList<SettingsOption> options) {
        JSONObject json = jsonDataUtils.deviceIdRequest(options, deviceId);
        Logger.info("Request json: " + json.toString());
        deleteSettings55And59And60Keys(json);
        return sendPostSettingsToUrl(createURL(), json);
    }


}
