package com.zodiac.ams.charter.http.helpers.callerId;

import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;

import static com.zodiac.ams.charter.http.listeners.callerId.CallerIdGetParams.*;
import static com.zodiac.ams.charter.services.callerId.CallerIdOptions.*;
import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_1;
import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_2;

public class CallerIdGetHelper extends HttpHelper {

    private static Config config = new Config();
    public static String responseString;

    private static String sendGetCallNotification(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String responseMessage = response.checkResponse();
        responseString = response.getResponseString();
        return responseMessage;
    }

    public static String sendGetIncomingCallNotificationRequestTo1STB() {
        return sendGetCallNotification(createURIIncomingCallFor1STB());
    }

    public static String sendGetIncomingCallNotificationRequestToSeveralSTBs() {
        return sendGetCallNotification(createURIIncomingCallForSeveralSTBs());
    }

    public static String sendGetIncomingCallNotificationRequestToSeveralSTBs(String id1, String id2) {
        return sendGetCallNotification(createURIIncomingCallForSeveralSTBs(id1, id2));
    }

    public static String sendGetIncomingCallNotificationRequestID(String id) {
        return sendGetCallNotification(createURIIncomingCallID(id));
    }

    public static String sendGetIncomingCallNotificationRequestCallID(String callID) {
        return sendGetCallNotification(createURIIncomingCallCallID(callID));
    }

    public static String sendGetIncomingCallNotificationRequestCallerName(String callerName) {
        return sendGetCallNotification(createURIIncomingCallCallerName(callerName));
    }

    public static String sendGetIncomingCallNotificationRequestPhoneNumber(String phoneNumber) {
        return sendGetCallNotification(createURIIncomingCallPhoneNumber(phoneNumber));
    }

    public static String sendGetIncomingCallNotificationRequestIncorrectOrderOfParams() {
        return sendGetCallNotification(createURIIncomingCallIncorrectOrderParams());
    }

    public static String sendGetIncomingCallNotificationRequestWithAmpersandInTheEnd() {
        return sendGetCallNotification(createURIIncomingCallWithAmpersandInTheEnd());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutEqualsAfterID() {
        return sendGetCallNotification(createURIIncomingCallWithoutEqualsAfterID());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutEqualsAfterCallID() {
        return sendGetCallNotification(createURIIncomingCallWithoutEqualsAfterCallId());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutEqualsAfterPhoneNumber() {
        return sendGetCallNotification(createURIIncomingCallWithoutEqualsAfterPhoneNumber());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutEqualsAfterCallerName() {
        return sendGetCallNotification(createURIIncomingCallWithoutEqualsAfterCallerName());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutCallerName() {
        return sendGetCallNotification(createURIIncomingCallWithoutCallerName());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutPhoneNumber() {
        return sendGetCallNotification(createURIIncomingCallWithoutPhoneNumber());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutCallId() {
        return sendGetCallNotification(createURIIncomingCallWithoutCallId());
    }

    public static String sendGetIncomingCallNotificationRequestWithoutID() {
        return sendGetCallNotification(createURIIncomingCallWithoutID());
    }

    public static String sendGetIncomingCallNotificationRequestWithMistakeIDParameter() {
        return sendGetCallNotification(createURIIncomingCallWithMistakeInIDParameter());
    }

    public static String sendGetIncomingCallNotificationRequestWithMistakeCallerNameParameter(String request) {
        return sendGetCallNotification(request);
    }

    public static String sendGetIncomingCallNotificationRequestWithMistakePhoneNumberParameter() {
        return sendGetCallNotification(createURIIncomingCallWithMistakeInPhoneNumberParameter());
    }

    public static String sendGetIncomingCallNotificationRequestWithMistakeCallIDParameter() {
        return sendGetCallNotification(createURIIncomingCallWithMistakeInCallIDParameter());
    }

    public static String sendGetCallTerminationNotificationRequestTo1STB() {
        return sendGetCallNotification(createURICallTerminationFor1STB());
    }

    public static String sendGetCallTerminationNotificationRequestToSeveralSTBs() {
        return sendGetCallNotification(createURICallTerminationForSeveralSTBs());
    }

    public static String sendGetCallTerminationNotificationRequestToSeveralSTBs(String id1, String id2) {
        return sendGetCallNotification(createURICallTerminationForSeveralSTBs(id1, id2));
    }

    public static String sendGetCallTerminationNotificationRequestIncorrectOrderOfParams() {
        return sendGetCallNotification(createURICallTerminationIncorrectOrderParams());
    }

    public static String sendGetCallTerminationNotificationRequestWithAmpersandInTheEnd() {
        return sendGetCallNotification(createURICallTerminationWithAmpersandInTheEnd());
    }

    public static String sendGetCallTerminationNotificationRequestWithoutEqualsAfterID() {
        return sendGetCallNotification(createURICallTerminationWithoutEqualsAfterID());
    }

    public static String sendGetCallTerminationNotificationRequestWithoutEqualsAfterCallID() {
        return sendGetCallNotification(createURICallTerminationWithoutEqualsAfterCallID());
    }

    public static String sendGetCallTerminationNotificationRequestWithoutCallID() {
        return sendGetCallNotification(createURICallTerminationWithoutCallID());
    }

    public static String sendGetCallTerminationNotificationRequestWithoutID() {
        return sendGetCallNotification(createURICallTerminationWithoutID());
    }

    public static String sendGetCallTerminationNotificationRequestID(String id) {
        return sendGetCallNotification(createURICallTerminationID(id));
    }

    public static String sendGetCallTerminationNotificationRequestCallID(String callID) {
        return sendGetCallNotification(createURICallTerminationCallID(callID));
    }

    public static String sendGetCallTerminationNotificationRequestWithMistakeCallIDParameter() {
        return sendGetCallNotification(createURICallTerminationWithMistakeInCallIDParameter());
    }

    public static String sendGetCallTerminationNotificationRequestWithMistakeIDParameter() {
        return sendGetCallNotification(createURICallTerminationWithMistakeInIDParameter());
    }

    private static String createURIIncomingCallFor1STB() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallID(String id) {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID, id, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallCallID(String callID) {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, callID);
    }

    private static String createURIIncomingCallCallerName(String callerName) {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, callerName, VALID_CALL_ID);
    }

    private static String createURIIncomingCallPhoneNumber(String phoneNumber) {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID, DEVICE_ID_NUMBER_1, phoneNumber, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallIncorrectOrderParams() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_INCORRECT_ORDER_PARAMS_CALLER_ID, VALID_CALLER_NAME, DEVICE_ID_NUMBER_1, VALID_CALL_ID, VALID_PHONE_NUMBER);
    }

    private static String createURIIncomingCallWithAmpersandInTheEnd() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID  + "&", DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutEqualsAfterID() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_EQUALS_AFTER_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutEqualsAfterCallerName() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_EQUALS_AFTER_CALLER_NAME, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutEqualsAfterPhoneNumber() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_EQUALS_AFTER_PHONE_NUMBER, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutEqualsAfterCallId() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_EQUALS_AFTER_CALL_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutID() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_ID, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutCallerName() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_CALLER_NAME, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutPhoneNumber() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_PHONE_NUMBER, DEVICE_ID_NUMBER_1, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithoutCallId() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_WITHOUT_CALl_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME);
    }

    private static String createURIIncomingCallWithMistakeInIDParameter() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_INVALID_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME,  VALID_CALL_ID);
    }

    public static String createURIIncomingCallWithMistakeInCallerNameParameter() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_INVALID_CALLER_NAME, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME,  VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithMistakeInPhoneNumberParameter() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_INVALID_PHONE_NUMBER, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME,  VALID_CALL_ID);
    }

    private static String createURIIncomingCallWithMistakeInCallIDParameter() {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_INVALID_CALL_ID, DEVICE_ID_NUMBER_1, VALID_PHONE_NUMBER, VALID_CALLER_NAME,  VALID_CALL_ID);
    }

    private static String createURIIncomingCallForSeveralSTBs(String id1, String id2) {
        return config.getAmsUrl() + ServicesURI.INCOMING_CALL_NOTIFICATION_CALLER_ID +
                String.format(INCOMING_CALL_NOTIFICATION_PARAMS_CALLER_ID, id1 + "," + id2, VALID_PHONE_NUMBER, VALID_CALLER_NAME, VALID_CALL_ID);
    }

    private static String createURIIncomingCallForSeveralSTBs() {
        return  createURIIncomingCallForSeveralSTBs(DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2);
    }

    private static String createURICallTerminationFor1STB() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_CALLER_ID, DEVICE_ID_NUMBER_1, VALID_CALL_ID);
    }

    private static String createURICallTerminationForSeveralSTBs() {
        return  createURICallTerminationForSeveralSTBs(DEVICE_ID_NUMBER_1, DEVICE_ID_NUMBER_2);
    }

    private static String createURICallTerminationForSeveralSTBs(String id1, String id2) {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_CALLER_ID, id1 + "," + id2,VALID_CALL_ID);
    }

    private static String createURICallTerminationIncorrectOrderParams() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_INCORRECT_ORDER_PARAMS_CALLER_ID, VALID_CALL_ID, DEVICE_ID_NUMBER_1);
    }

    private static String createURICallTerminationID(String id) {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_CALLER_ID, id, VALID_CALL_ID);
    }

    private static String createURICallTerminationCallID(String callID) {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_CALLER_ID, DEVICE_ID_NUMBER_1, callID);
    }

    private static String createURICallTerminationWithMistakeInCallIDParameter() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_INVALID_CALL_ID,DEVICE_ID_NUMBER_1, VALID_CALL_ID);
    }

    private static String createURICallTerminationWithMistakeInIDParameter() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_INVALID_ID, DEVICE_ID_NUMBER_1, VALID_CALL_ID);
    }

    private static String createURICallTerminationWithAmpersandInTheEnd() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_CALLER_ID  + "&", DEVICE_ID_NUMBER_1, VALID_CALL_ID);
    }

    private static String createURICallTerminationWithoutEqualsAfterID() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_WITHOUT_EQUALS_AFTER_ID,  DEVICE_ID_NUMBER_1, VALID_CALL_ID);
    }

    private static String createURICallTerminationWithoutEqualsAfterCallID() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_WITHOUT_EQUALS_AFTER_CALL_ID, DEVICE_ID_NUMBER_1, VALID_CALL_ID);
    }

    private static String createURICallTerminationWithoutCallID() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_WITHOUT_CALl_ID, DEVICE_ID_NUMBER_1 );
    }

    private static String createURICallTerminationWithoutID() {
        return config.getAmsUrl() + ServicesURI.CALL_TERMINATION_NOTIFICATION_CALLER_ID +
                String.format(CALL_TERMINATION_NOTIFICATION_PARAMS_WITHOUT_ID,  VALID_CALL_ID );
    }
}
