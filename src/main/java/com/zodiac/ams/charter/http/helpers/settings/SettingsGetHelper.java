package com.zodiac.ams.charter.http.helpers.settings;

import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.UrlHelper.*;


public class SettingsGetHelper extends HttpHelper {

    public static JSONObject responseJson;

    private static String sendGetSettingsToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String str = response.checkResponse();
        responseJson = response.getResponseJson();
        return str;
    }

    public static String sendGetSettingsIncorrectDeviceId(String id) {
        return sendGetSettingsToUrl(createURLWithIncorrectDeviceId(id));
    }

    public static String sendGetSettingsDeviceId(String id) {
        return sendGetSettingsToUrl(createURLWithDeviceId(id));
    }

    public static String sendGetSettingsDeviceIdName(String id, ArrayList<SettingsOption> options) {
        return sendGetSettingsToUrl(createURLDeviceIdName(id, options));
    }

    public static String sendGetSettingsDeviceIdName(String id, String setting) {
        return sendGetSettingsToUrl(createURLDeviceIdName(id, setting));
    }
}
