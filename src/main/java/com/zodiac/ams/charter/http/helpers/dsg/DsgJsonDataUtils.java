package com.zodiac.ams.charter.http.helpers.dsg;

import com.zodiac.ams.charter.services.dsg.http.DsgJsonOptions;
import org.json.JSONArray;
import org.json.JSONObject;

public class DsgJsonDataUtils {

    private String NETWORK_TYPE  = "networkType";
    private String DEVICE_COUNT = "deviceCount";

    public JSONObject createStbNetworkTypeResponseJson(String networkType) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(DsgJsonOptions.STB_MAC.getOption().getNameJSON(),
                DsgJsonOptions.STB_MAC.getOption().getValueJSON());
        resultJson.put(DsgJsonOptions.NETWORK_TYPE.getOption().getNameJSON(),
                networkType);
        resultJson.put(DsgJsonOptions.LAST_ACTIVITY_TIME_UTC.getOption().getNameJSON(),
                DsgJsonOptions.LAST_ACTIVITY_TIME_UTC.getOption().getValueJSON());
        return resultJson;
    }

    public JSONObject createStbPopulationDistributionResponseJson(int deviceCountDocsis, int deviceCountDavic, int deviceCountAloha) {
        JSONObject resultJson = new JSONObject();
        JSONArray networkList = new JSONArray();
        networkList.put(new JSONObject().put(DEVICE_COUNT, deviceCountDocsis)
                .put(NETWORK_TYPE, "DOCSIS"));
        networkList.put(new JSONObject().put(DEVICE_COUNT, deviceCountDavic)
                .put(NETWORK_TYPE, "DAVIC"));
        networkList.put(new JSONObject().put(DEVICE_COUNT, deviceCountAloha)
                .put(NETWORK_TYPE, "ALOHA"));
        resultJson.put("networkList", networkList);
        return resultJson;
    }
}