package com.zodiac.ams.charter.http.helpers.dsg.json;


import com.zodiac.ams.charter.services.dsg.DsgJsonOption;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import static com.zodiac.ams.charter.services.dsg.NewDsgJsonOptions.CM_IP;
import static com.zodiac.ams.charter.services.dsg.NewDsgJsonOptions.SW_VERSION;


public class JsonDataUtils {

    public JSONObject dsgRequestPattern(List options) {
        return new JSONObject()
                .put("settings", new JSONObject()
                        .put("attributes", new JSONObject()
                                .put("options", createOptions(options))));
    }

    private JSONArray createOptions(List options) {
        JSONArray array = new JSONArray();
        for (Object option : options) {
            array.put(createOption((DsgJsonOption) option));
        }
        return array;
    }

    private JSONObject createOption(DsgJsonOption option) {
        return new JSONObject().put("name", option.getName()).put("value", option.getValue());
    }

    public JSONObject changeOption(JSONObject jsonObject, String option, String newValue) {
        JSONArray object = jsonObject.getJSONObject("settings").getJSONObject("attributes").getJSONArray("options");
        int j = 0;
        for (int i = 0; i < object.length(); i++) {
            if (object.getJSONObject(i).get("name").equals(option)) {
                    object.getJSONObject(i).put("value", newValue);
            }
        }
        return jsonObject;
    }

    public JSONObject deleteValue(JSONObject jsonObject) {
        JSONArray object = jsonObject.getJSONObject("settings").getJSONObject("attributes").getJSONArray("options");
        for (int i = 0; i < object.length(); i++) {
            if (!object.getJSONObject(i).get("name").equals(SW_VERSION.getOption().getName())) {
                if (object.getJSONObject(i).get("name").equals(CM_IP.getOption().getName())) {
                    if (object.getJSONObject(i).get("value").equals("0.0.0.0")) {
                        object.getJSONObject(i).remove("value");
                    }
                } else if (object.getJSONObject(i).get("value").equals("null")) {
                    object.getJSONObject(i).remove("value");
                }
            }
        }
        return jsonObject;
    }


}
