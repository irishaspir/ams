package com.zodiac.ams.charter.http.listeners.epg;


import com.zodiac.ams.charter.http.ServicesURI;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentDayPlusAmountDays;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentDayPlusCountStr;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getTodayStr;

public class EpgHttpDataUtils {

    public String nullResponse() {
        return null;
    }

    public String getEpgDataUriPattern() {
        return ServicesURI.EPG_DATA + "/?StartTime=" + getTodayStr() + "+00%3A00&EndTime=" + getCurrentDayPlusCountStr(14) + "+00%3A00";
    }

    public List<URI> getEpgDataUriListPattern() {
        int amountSegment = 14;
        List<URI> uriList = new ArrayList<>();
        for (int i = 0; i < amountSegment; i++) {
            uriList.add(URI.create(ServicesURI.EPG_DATA + "/?StartTime=" + getCurrentDayPlusAmountDays(i) + "+00%3A00&EndTime=" + getCurrentDayPlusAmountDays(i+1) + "+00%3A00"));
        }
        return uriList;
    }





}
