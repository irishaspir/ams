package com.zodiac.ams.charter.http.helpers.settings;

import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.json.JSONObject;

import static com.zodiac.ams.charter.http.helpers.settings.UrlHelper.createURLWithDeviceId;

/**
 * Created by SpiridonovaIM on 21.08.2017.
 */
public class SettingsDeleteHelper extends HttpHelper {

    private static Config config = new Config();
    public static JSONObject responseJson;



    private static String sendDeleteSettingsToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendDelete(url)));
        String str = response.checkResponse();
        responseJson = response.getResponseJson();
        return str;
    }

    private static String sendDeleteSettingsToUrl(String url, String body) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendDelete(url, body)));
        String str = response.checkResponse();
        responseJson = response.getResponseJson();
        return str;
    }

    public static String sendDeleteSettingsDeviceId(String id) {
        return sendDeleteSettingsToUrl(createURLWithDeviceId(id));
    }

    public static String sendDeleteSettingDeviceId(String id) {
        return sendDeleteSettingsToUrl(createURLWithDeviceId(id));
    }


}
