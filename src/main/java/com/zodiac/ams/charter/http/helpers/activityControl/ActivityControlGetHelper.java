package com.zodiac.ams.charter.http.helpers.activityControl;


import com.zodiac.ams.charter.http.ServicesURI;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.http.HttpHelper;
import com.zodiac.ams.common.http.HttpResponseEntity;
import com.zodiac.ams.common.http.ResponseListener;
import org.json.JSONObject;

import static com.zodiac.ams.charter.http.listeners.activityControl.ActivityControlGetParams.*;
import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_1;


public class ActivityControlGetHelper extends HttpHelper {

    private static Config config = new Config();
    public static JSONObject responseJson;
    public static String responseString;

    private static String sendGetStbStatusRequestToUrl(String url) {
        ResponseListener response = new ResponseListener(new HttpResponseEntity(sendGet(url)));
        String responseCode = response.checkResponse();
        responseJson = response.getResponseJson();
        responseString = response.getResponseString();
        return responseCode;
    }

    public static String sendGetStbStatusRequestWithMistakeInReq() {
        return sendGetStbStatusRequestToUrl(createURLWithMistakeInReq());
    }

    public static String sendGetStbStatusRequestReq(String req) {
        return sendGetStbStatusRequestToUrl(createURLWithReq(req));
    }

    public static String sendGetStbStatusRequestDeviceId(String deviceId) {
        return sendGetStbStatusRequestToUrl(createURLWithDeviceId(deviceId));
    }

    public static String sendGetStbStatusRequestWithoutReq() {
        return sendGetStbStatusRequestToUrl(createURLWithoutReq());
    }

    public static String sendGetStbStatusRequestWithoutDeviceId() {
        return sendGetStbStatusRequestToUrl(createURLWithoutDeviceId());
    }

    private static String createURLWithMistakeInReq() {
        return config.getAmsUrl() + ServicesURI.ACTIVITY_CONTROL + String.format(STB_STATUS_REQUEST_INVALID_REQ, DEVICE_ID_NUMBER_1);
    }

    private static String createURLWithReq(String req) {
        return config.getAmsUrl() + ServicesURI.ACTIVITY_CONTROL + String.format(STB_STATUS_REQUEST_REQ, req, DEVICE_ID_NUMBER_1);
    }

    private static String createURLWithoutReq() {
        return config.getAmsUrl() + ServicesURI.ACTIVITY_CONTROL + String.format(STB_STATUS_REQUEST_WITHOUT_REQ, DEVICE_ID_NUMBER_1);
    }

    private static String createURLWithDeviceId(String deviceId) {
        return config.getAmsUrl() + ServicesURI.ACTIVITY_CONTROL + String.format(STB_STATUS_REQUEST_DEVICE_ID, deviceId);
    }

    private static String createURLWithoutDeviceId() {
        return config.getAmsUrl() + ServicesURI.ACTIVITY_CONTROL + STB_STATUS_REQUEST_WITHOUT_DEVICE_ID;
    }
}
