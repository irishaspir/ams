package com.zodiac.ams.charter.http.helpers.dsg;


import com.zodiac.ams.charter.services.dsg.DsgJsonOptions;
import com.zodiac.ams.charter.services.dsg.NewDsgJsonOptions;

import java.util.List;

import static com.zodiac.ams.charter.runner.TestRunner.AMS_VERSION;

/**
 * Created by SpiridonovaIM on 12.07.2017.
 */
public class DsgDataUtils {

    protected static List dsgJsonOptions = initJsonOptions();

    public static List initJsonOptions() {
        if (AMS_VERSION < 390)
            return dsgJsonOptions = DsgJsonOptions.allDsgOptions();
        else return dsgJsonOptions = NewDsgJsonOptions.allDsgOptions();
    }


}
