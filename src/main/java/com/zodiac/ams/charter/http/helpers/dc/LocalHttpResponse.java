package com.zodiac.ams.charter.http.helpers.dc;

import com.zodiac.ams.common.http.ResponseListener;

/**
 * The local http response.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 * @author <a href="mailto:tigran.abaghyan@gmail.com">Tigran Abaghyan</a>
 */
public class LocalHttpResponse<D> {
    private final D data;
    private final String statusCode;

    /**
     * The regular constructor.
     *
     * @param responseListener the response listener
     */
    public LocalHttpResponse(ResponseListener responseListener) {
        data = null;
        statusCode = responseListener.checkResponse();
    }

    public D getData() {
        return data;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
