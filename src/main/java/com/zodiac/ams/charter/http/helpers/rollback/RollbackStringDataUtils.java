package com.zodiac.ams.charter.http.helpers.rollback;

public class RollbackStringDataUtils {

    public static String createBodyRequestAsString(){
        return "request";
    }

    public static String error500WithoutBody() {
        return "RB-002 Rollback service error: Not a JSON Object: null";
    }

    public static String error500WithStringInBody() {
        return "RB-002 Rollback service error: Not a JSON Object: \"request\"";
    }

    public static String error500WithoutJsonArrayBracketsInBody() {
        return "RB-002 Rollback service error: com.google.gson.JsonPrimitive cannot be cast to com.google.gson.JsonArray";
    }
}
