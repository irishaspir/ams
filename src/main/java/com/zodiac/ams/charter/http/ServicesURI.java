package com.zodiac.ams.charter.http;


public interface ServicesURI {

    /**
     * com.zodiac.ams.settings service
     */
    String SETTINGS = "/settings";

    /**
     * callerId service
     */
    String CALLER_ID = "/callerid";

    String INCOMING_CALL_NOTIFICATION_CALLER_ID = "/service/callerid/IncomingCallNotification";

    String CALL_TERMINATION_NOTIFICATION_CALLER_ID = "/service/callerid/CallTerminationNotification";

    /**
     * UpdateEPGData service
     */
    String UPDATE_EPG_DATA = "/UpdateEPGData";

    String LINEUP = "";

    String EPG_DATA = "/api/epg/filtering";

    /**
     * ActivityControl service
     */
    String ACTIVITY_CONTROL = "/StbStatus";

    /**
     * STB Lookup service
     */
    String STB_LOOKUP = "/STBLookupService";

    /**
     * <<<<<<< HEAD
     * Rollback service
     */
    String ROLLBACK = "/amsStbRollbackService";

    /**
     * ActivityControl service
     */
    String DSG = "/DsgStatus";
    /*
         * The DC service uri.
         */
    String DC = "/dc";

}
