package com.zodiac.ams.charter.http.helpers.settings.json;

/**
 * Created by SpiridonovaIM on 27.03.2017.
 */
public class ErrorCode {

    public static final String SET_25 = "SET-025 Unsupported data type: null";
    public static final String NOT_FOUND = "STB MAC not found: ";
    public static final String NON_SPECIFIED = "DeviceId is not specified";
    public static final String TIMEOUT_BOX_PATTERN = "Timeout detected by BoxResponseTracker, (%s, %s)";
    public static final String UNKNOWN_SETTING_NAME_PATTERN = "Unknown setting name %s";

}
