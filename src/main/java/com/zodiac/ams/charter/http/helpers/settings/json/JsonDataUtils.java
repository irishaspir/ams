package com.zodiac.ams.charter.http.helpers.settings.json;


import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.zodiac.ams.charter.http.helpers.settings.json.ErrorCode.*;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.ON_DEMAND_PIN;
import static com.zodiac.ams.charter.services.settings.SettingsOptions.allSettings;


public class JsonDataUtils {

    /**
     * Delete in json 55, 59 and 60 keys
     *
     * @param json
     */
    public static void deleteSettings55And59And60Keys(JSONObject json) {
        JSONArray j = json.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options");
        for (int i = 0; i < j.length(); i++) {
            if (j.getJSONObject(i).getString("name").equals("On Demand Lock")) {
                j.remove(i);
            }
            if (j.getJSONObject(i).getString("name").equals("Message Indicator")) {
                j.remove(i);
            }
            if (j.getJSONObject(i).getString("name").equals("Reminder Timer")) {
                j.remove(i);
            }
        }
    }

    /**
     * @return empty response
     */
    public JSONObject emptyResponse() {
        return new JSONObject();
    }

    /**
     * Response for case when DeviceId is not found in BD
     *
     * @param id
     * @return error 404
     */
    public JSONObject error404MacNotFound(String id) {
        return createErrorJson("404", NOT_FOUND + id);
    }


    /**
     * Response for case when DeviceId is not specified
     *
     * @return error 404
     */
    public JSONObject error404NotSpecified() {
        return createErrorJson("404", NON_SPECIFIED);
    }

    /**
     * Response for case when Unsupported data type
     *
     * @return error 500
     */
    public JSONObject error500Set25() {
        return createErrorJson("500", SET_25);
    }


    /**
     * Response for case when  Unknown setting name
     *
     * @param name
     * @return error 500
     */
    public JSONObject error500UnknownSettingsName(String name) {
        return createErrorJson("500", String.format(UNKNOWN_SETTING_NAME_PATTERN, name));
    }

    /**
     * Response for case when TIMEOUT BOX
     *
     * @return error 500
     */
    public JSONObject error500TimeoutBox(String deviceId, int num) {
        return createErrorJson("500", String.format(TIMEOUT_BOX_PATTERN, deviceId, num));
    }

    private JSONObject createErrorJson(String code, String message) {
        return new JSONObject()
                .put("settings", new JSONObject()
                        .put("error", new JSONObject()
                                .put("code", code)
                                .put("message", message)));

    }

    /**
     * Json for POST request to AMS
     *
     * @param options  settings list
     * @param deviceId
     * @return json with settings list
     */
    public JSONObject deviceIdRequest(ArrayList<SettingsOption> options, String deviceId) {
        return new JSONObject()
                .put("settings", new JSONObject()
                        .put("groups", new JSONArray()
                                .put(new JSONObject()
                                        .put("id", deviceId)
                                        .put("type", "device-stb")
                                        .put("options", createOptions(options)))));

    }

    private JSONArray createOptions(ArrayList<SettingsOption> options) {
        JSONArray array = new JSONArray();
        for (SettingsOption option : options) {
            array.put(createOption(option));
        }
        return array;
    }


    private JSONObject createOption(SettingsOption option) {
        JSONObject jsonObject = new JSONObject().put("name", option.getName());
        switch (option.getColumnNumber()) {
            case 55:
                jsonObject.put("value", new JSONArray());
                break;
            case 31:
                jsonObject.put("value", new JSONArray().put(option.getValue()));
                break;
            default:
                if (option.getValue().length() > 0)
                    jsonObject.put("value", new JSONArray().put(option.getValue()));
                else jsonObject.put("value", new JSONArray());
        }
        return jsonObject;
    }

    /**
     * Json for POST request to AMS with invalid settings name
     *
     * @param options settings list
     * @return json with invalid settings name
     */
    public JSONObject responseWithInvalidSettingsName(ArrayList<SettingsOption> options) {
        return buildResponseWithoutSpoiledName(options);
    }

    /**
     * Json for POST request to AMS with invalid settings value
     *
     * @param options settings list
     * @return json with invalid settings value
     */
    public JSONObject responseWithInvalidSettingsValue(ArrayList<SettingsOption> options) {
        return buildResponseWithoutSpoiledValue(options);
    }

    private JSONObject buildResponseWithoutSpoiledName(ArrayList<SettingsOption> options) {
        JSONObject object = new JSONObject();
        for (int i = 1; i < options.size(); i++) {
            object.put(options.get(i).getName(), "success");
            i++;
        }
        object.put("Spoiled name", "unknown key");
        return object;
    }

    private JSONObject buildResponseWithoutSpoiledValue(ArrayList<SettingsOption> options) {
        JSONObject object = new JSONObject();
        for (int i = 0; i < options.size(); i++) {
            if ((i % 2) == 0) object.put(options.get(i).getName(), "incorrect value");
            else object.put(options.get(i).getName(), "success");

        }
        return object;
    }

    /**
     * Pattern for GET settings response
     *
     * @return json with all settings list
     */
    public JSONObject getSettingsResponse() {
        return new JSONObject().put("options", createAllOptions(allSettings()));
    }

    private JSONArray createAllOptions(ArrayList<SettingsOption> options) {
        JSONArray array = new JSONArray();
        for (SettingsOption option : options) {
            array.put(createOption(option));
        }
        return array;
    }

    /**
     * Pattern for GET settings response
     *
     * @param options settings list
     * @return json
     */
    public JSONObject getSettingsDeviceIdNameResponse(ArrayList<SettingsOption> options) {
        JSONObject json = new JSONObject();
        for (SettingsOption option : options) {
            switch (option.getColumnNumber()) {
                case 27:
                case 28:
                case 37:
                case 39:
                case 41:
                case 53:
                case 54:
                case 55: {
                    json.put("value", new String[]{});
                    json.put("name", option.getName());
                    break;
                }
                default: {
                    json.put("value", new String[]{option.getValue()});
                    json.put("name", option.getName());
                }
            }
        }
        Logger.info("Json: " + json.toString());
        return json;
    }


    /**
     * Pattern for POST settings response
     *
     * @param options settings list
     * @return json with success
     */
    public JSONObject patternSuccessResponse(ArrayList<SettingsOption> options) {
        JSONObject json = new JSONObject();
        for (SettingsOption option : options) {

            if (!(option.getColumnNumber() == 60 || option.getColumnNumber() == 59 || option.getColumnNumber() == 55))
                json.put(option.getName(), "success");
        }
        return json;
    }

    /**
     * Json for POST request to AMS with invalid each second settings name
     *
     * @param object settings list
     * @return Json with  invalid each second settings name
     */
    public JSONObject invalidEachSecondSettingsName(JSONObject object) {
        int length = object.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options").length();
        for (int i = 0; i < length; i++) {
            JSONObject option = object.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options").getJSONObject(i);
            option.put("name", "Spoiled name");
            i++;
        }
        return object;
    }

    /**
     * Json for POST request to AMS with invalid each second settings value
     *
     * @param object settings list
     * @return Json with  invalid each second settings value
     */
    public JSONObject invalidOptionsEachSecondValue(JSONObject object) {
        int length = object.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options").length();
        for (int i = 0; i < length; i++) {
            JSONObject option = object.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options").getJSONObject(i);
            option.put("value", "Spoiled_value");
            i++;
        }
        return object;
    }


    public JSONObject setNewValue(JSONObject object, String newValue) {
        int length = object.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options").length();
        for (int i = 0; i < length; i++) {
            JSONObject option = object.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options").getJSONObject(i);
            option.put("value", newValue);
            i++;
        }
        return object;
    }

    /**
     * Json for POST request to AMS
     *
     * @param options  settings list
     * @param deviceId
     * @return json
     */
    public JSONObject amsSetSettingsRequest(ArrayList<SettingsOption> options, String deviceId) {
        JSONObject jsonObject = deviceIdRequest(options, "STB" + deviceId);
        jsonObject.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).put("amsid", new Config().getAmsIp());
        Logger.info("Set settings pattern: " + jsonObject.toString());
        return jsonObject;
    }

    /**
     * Json for POST request to AMS
     *
     * @param options  settings list
     * @param deviceId
     * @return json
     */
    public JSONObject amsSetSettingsRequest(ArrayList<SettingsOption> options, String deviceId, int x) {
        JSONObject jsonObject = deviceIdRequest(options, "STB" + deviceId);
        changeOption(jsonObject, "CC Language", x);
        jsonObject.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).put("amsid", new Config().getAmsIp());
        Logger.info("Set settings pattern: " + jsonObject.toString());
        return jsonObject;
    }

    /**
     * Json for POST request to AMS without ON DEMAND PIN
     *
     * @param options  settings list
     * @param deviceId
     * @return json
     */
    public JSONObject amsSetSettingsRequestWithODP(ArrayList<SettingsOption> options, String deviceId) {
        if (!options.contains(ON_DEMAND_PIN.getOption()))
            options.add(ON_DEMAND_PIN.getOption());
        return amsSetSettingsRequest(options, deviceId);
    }


    /**
     * Json for POST request to AMS with empty ON DEMAND PIN
     *
     * @param options  settings list
     * @param deviceId
     * @return json
     */
    public JSONObject amsSetSettingsRequestWithODPIsEmpty(ArrayList<SettingsOption> options, String deviceId) {
        JSONObject jsonObject = amsSetSettingsRequestWithODP(options, deviceId);
        JSONArray object = jsonObject.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options");
        for (int i = 0; i < object.length(); i++) {
            if (object.getJSONObject(i).get("name").equals("On Demand PIN"))
                object.getJSONObject(i).put("value", new JSONArray().put(""));
        }
        return jsonObject;
    }

    public JSONObject changeOption(JSONObject jsonObject, String name, int value) {
        JSONArray object = jsonObject.getJSONObject("settings").getJSONArray("groups").getJSONObject(0).getJSONArray("options");
        for (int i = 0; i < object.length(); i++) {
            if (object.getJSONObject(i).get("name").equals(name))
                object.getJSONObject(i).put("value", new JSONArray().put("CCTV" + (value + 1)));
        }
        return jsonObject;
    }

}
