package com.zodiac.ams.charter.csv.file.entry;

import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventOperationResult;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventType;

import java.time.LocalDateTime;

/**
 * The DVR CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DvrCsvFileEntry {
    private String macId;
    private String stbSessionId;
    private Integer sequenceNumber;
    private LocalDateTime eventTime;
    private DvrEventType eventType;
    private DvrEventReason reason;
    private DvrEventOperationResult operationResult;
    private String recordingId;
    private String tmsProgramId;
    private String tmsChannelId;
    private Integer channelNumber;
    private LocalDateTime scheduledStartTime;
    private LocalDateTime scheduledEndTime;
    private LocalDateTime actualStartTime;
    private LocalDateTime actualEndTime;
    private Integer internalHddFreeSpacePercent;
    private Integer externalHddFreeSpacePercent;
    private Integer watchedVideoCurrentPosition;

    /*
     * The private constructor.
     */
    private DvrCsvFileEntry() {
        // do nothing here
    }

    public String getMacId() {
        return macId;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public DvrEventType getEventType() {
        return eventType;
    }

    public DvrEventReason getReason() {
        return reason;
    }

    public DvrEventOperationResult getOperationResult() {
        return operationResult;
    }

    public String getRecordingId() {
        return recordingId;
    }

    public String getTmsProgramId() {
        return tmsProgramId;
    }

    public String getTmsChannelId() {
        return tmsChannelId;
    }

    public Integer getChannelNumber() {
        return channelNumber;
    }

    public LocalDateTime getScheduledStartTime() {
        return scheduledStartTime;
    }

    public LocalDateTime getScheduledEndTime() {
        return scheduledEndTime;
    }

    public LocalDateTime getActualStartTime() {
        return actualStartTime;
    }

    public LocalDateTime getActualEndTime() {
        return actualEndTime;
    }

    public Integer getInternalHddFreeSpacePercent() {
        return internalHddFreeSpacePercent;
    }

    public Integer getExternalHddFreeSpacePercent() {
        return externalHddFreeSpacePercent;
    }

    public Integer getWatchedVideoCurrentPosition() {
        return watchedVideoCurrentPosition;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DvrCsvFileEntry that = (DvrCsvFileEntry) obj;

        if (macId != null ? !macId.equals(that.macId) : that.macId != null) {
            return false;
        }
        if (stbSessionId != null ? !stbSessionId.equals(that.stbSessionId) : that.stbSessionId != null) {
            return false;
        }
        if (sequenceNumber != null ? !sequenceNumber.equals(that.sequenceNumber) : that.sequenceNumber != null) {
            return false;
        }
        if (eventTime != null ? !eventTime.equals(that.eventTime) : that.eventTime != null) {
            return false;
        }
        if (eventType != that.eventType) {
            return false;
        }
        if (reason != that.reason) {
            return false;
        }
        if (operationResult != that.operationResult) {
            return false;
        }
        if (recordingId != null ? !recordingId.equals(that.recordingId) : that.recordingId != null) {
            return false;
        }
        if (tmsProgramId != null ? !tmsProgramId.equals(that.tmsProgramId) : that.tmsProgramId != null) {
            return false;
        }
        if (tmsChannelId != null ? !tmsChannelId.equals(that.tmsChannelId) : that.tmsChannelId != null) {
            return false;
        }
        if (channelNumber != null ? !channelNumber.equals(that.channelNumber) : that.channelNumber != null) {
            return false;
        }
        if (scheduledStartTime != null ? !scheduledStartTime.equals(that.scheduledStartTime) : that.scheduledStartTime != null) {
            return false;
        }
        if (scheduledEndTime != null ? !scheduledEndTime.equals(that.scheduledEndTime) : that.scheduledEndTime != null) {
            return false;
        }
        if (actualStartTime != null ? !actualStartTime.equals(that.actualStartTime) : that.actualStartTime != null) {
            return false;
        }
        if (actualEndTime != null ? !actualEndTime.equals(that.actualEndTime) : that.actualEndTime != null) {
            return false;
        }
        if (internalHddFreeSpacePercent != null ? !internalHddFreeSpacePercent.equals(that.internalHddFreeSpacePercent) : that.internalHddFreeSpacePercent != null) {
            return false;
        }
        if (externalHddFreeSpacePercent != null ? !externalHddFreeSpacePercent.equals(that.externalHddFreeSpacePercent) : that.externalHddFreeSpacePercent != null) {
            return false;
        }
        return watchedVideoCurrentPosition != null ? watchedVideoCurrentPosition.equals(that.watchedVideoCurrentPosition) : that.watchedVideoCurrentPosition == null;
    }

    @Override
    public int hashCode() {
        int result = macId != null ? macId.hashCode() : 0;
        result = 31 * result + (stbSessionId != null ? stbSessionId.hashCode() : 0);
        result = 31 * result + (sequenceNumber != null ? sequenceNumber.hashCode() : 0);
        result = 31 * result + (eventTime != null ? eventTime.hashCode() : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (operationResult != null ? operationResult.hashCode() : 0);
        result = 31 * result + (recordingId != null ? recordingId.hashCode() : 0);
        result = 31 * result + (tmsProgramId != null ? tmsProgramId.hashCode() : 0);
        result = 31 * result + (tmsChannelId != null ? tmsChannelId.hashCode() : 0);
        result = 31 * result + (channelNumber != null ? channelNumber.hashCode() : 0);
        result = 31 * result + (scheduledStartTime != null ? scheduledStartTime.hashCode() : 0);
        result = 31 * result + (scheduledEndTime != null ? scheduledEndTime.hashCode() : 0);
        result = 31 * result + (actualStartTime != null ? actualStartTime.hashCode() : 0);
        result = 31 * result + (actualEndTime != null ? actualEndTime.hashCode() : 0);
        result = 31 * result + (internalHddFreeSpacePercent != null ? internalHddFreeSpacePercent.hashCode() : 0);
        result = 31 * result + (externalHddFreeSpacePercent != null ? externalHddFreeSpacePercent.hashCode() : 0);
        result = 31 * result + (watchedVideoCurrentPosition != null ? watchedVideoCurrentPosition.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DvrCsvFileEntry{" +
                "macId='" + macId + '\'' +
                ", stbSessionId='" + stbSessionId + '\'' +
                ", sequenceNumber=" + sequenceNumber +
                ", eventTime=" + eventTime +
                ", eventType=" + eventType +
                ", reason=" + reason +
                ", operationResult=" + operationResult +
                ", recordingId='" + recordingId + '\'' +
                ", tmsProgramId='" + tmsProgramId + '\'' +
                ", tmsChannelId='" + tmsChannelId + '\'' +
                ", channelNumber=" + channelNumber +
                ", scheduledStartTime=" + scheduledStartTime +
                ", scheduledEndTime=" + scheduledEndTime +
                ", actualStartTime=" + actualStartTime +
                ", actualEndTime=" + actualEndTime +
                ", internalHddFreeSpacePercent=" + internalHddFreeSpacePercent +
                ", externalHddFreeSpacePercent=" + externalHddFreeSpacePercent +
                ", watchedVideoCurrentPosition=" + watchedVideoCurrentPosition +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String macId;
        private String stbSessionId;
        private Integer sequenceNumber;
        private LocalDateTime eventTime;
        private DvrEventType eventType;
        private DvrEventReason reason;
        private DvrEventOperationResult operationResult;
        private String recordingId;
        private String tmsProgramId;
        private String tmsChannelId;
        private Integer channelNumber;
        private LocalDateTime scheduledStartTime;
        private LocalDateTime scheduledEndTime;
        private LocalDateTime actualStartTime;
        private LocalDateTime actualEndTime;
        private Integer internalHddFreeSpacePercent;
        private Integer externalHddFreeSpacePercent;
        private Integer watchedVideoCurrentPosition;

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setEventTime(LocalDateTime eventTime) {
            this.eventTime = eventTime;
            return this;
        }

        public Builder setEventType(DvrEventType eventType) {
            this.eventType = eventType;
            return this;
        }

        public Builder setReason(DvrEventReason reason) {
            this.reason = reason;
            return this;
        }

        public Builder setOperationResult(DvrEventOperationResult operationResult) {
            this.operationResult = operationResult;
            return this;
        }

        public Builder setRecordingId(String recordingId) {
            this.recordingId = recordingId;
            return this;
        }

        public Builder setTmsProgramId(String tmsProgramId) {
            this.tmsProgramId = tmsProgramId;
            return this;
        }

        public Builder setTmsChannelId(String tmsChannelId) {
            this.tmsChannelId = tmsChannelId;
            return this;
        }

        public Builder setChannelNumber(Integer channelNumber) {
            this.channelNumber = channelNumber;
            return this;
        }

        public Builder setScheduledStartTime(LocalDateTime scheduledStartTime) {
            this.scheduledStartTime = scheduledStartTime;
            return this;
        }

        public Builder setScheduledEndTime(LocalDateTime scheduledEndTime) {
            this.scheduledEndTime = scheduledEndTime;
            return this;
        }

        public Builder setActualStartTime(LocalDateTime actualStartTime) {
            this.actualStartTime = actualStartTime;
            return this;
        }

        public Builder setActualEndTime(LocalDateTime actualEndTime) {
            this.actualEndTime = actualEndTime;
            return this;
        }

        public Builder setInternalHddFreeSpacePercent(Integer internalHddFreeSpacePercent) {
            this.internalHddFreeSpacePercent = internalHddFreeSpacePercent;
            return this;
        }

        public Builder setExternalHddFreeSpacePercent(Integer externalHddFreeSpacePercent) {
            this.externalHddFreeSpacePercent = externalHddFreeSpacePercent;
            return this;
        }

        public Builder setWatchedVideoCurrentPosition(Integer watchedVideoCurrentPosition) {
            this.watchedVideoCurrentPosition = watchedVideoCurrentPosition;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public DvrCsvFileEntry build() {
            DvrCsvFileEntry target = new DvrCsvFileEntry();
            target.macId = macId;
            target.stbSessionId = stbSessionId;
            target.sequenceNumber = sequenceNumber;
            target.eventTime = eventTime;
            target.eventType = eventType;
            target.reason = reason;
            target.operationResult = operationResult;
            target.recordingId = recordingId;
            target.tmsProgramId = tmsProgramId;
            target.tmsChannelId = tmsChannelId;
            target.channelNumber = channelNumber;
            target.scheduledStartTime = scheduledStartTime;
            target.scheduledEndTime = scheduledEndTime;
            target.actualStartTime = actualStartTime;
            target.actualEndTime = actualEndTime;
            target.internalHddFreeSpacePercent = internalHddFreeSpacePercent;
            target.externalHddFreeSpacePercent = externalHddFreeSpacePercent;
            target.watchedVideoCurrentPosition = watchedVideoCurrentPosition;

            return target;
        }
    }
}
