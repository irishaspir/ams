package com.zodiac.ams.charter.csv.file.entry.enumtype;

public enum YesNo {
    YES (1, "YES"),
    NO (0, "");
    
    private final int intValue;
    private final String value;
    
    private YesNo(int intValue, String value){
        this.intValue = intValue;
        this.value = value;
    }
    
    public int getIntValue(){
        return intValue;
    }
    
    public String getValue(){
        return value;
    }
    
    public static YesNo of(String value){
        for (YesNo item: YesNo.values()){
            if (item.value.equals(value))
                return item;
        }
        return null;        
    }
}
