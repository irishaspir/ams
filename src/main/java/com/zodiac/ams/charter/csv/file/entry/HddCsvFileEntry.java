package com.zodiac.ams.charter.csv.file.entry;

import java.time.LocalDateTime;

/**
 * The HDD CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class HddCsvFileEntry {
    private String macId;
    private String stbSessionId;
    private Integer sequenceNumber;
    private LocalDateTime eventTime;
    private Integer internalHddCapacity;
    private Integer externalHddCapacity;
    private Integer internalHddFreeSpacePercent;
    private Integer externalHddFreeSpacePercent;

    /*
     * The private constructor.
     */
    private HddCsvFileEntry() {
        // do nothing here
    }

    public String getMacId() {
        return macId;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public Integer getInternalHddCapacity() {
        return internalHddCapacity;
    }

    public Integer getExternalHddCapacity() {
        return externalHddCapacity;
    }

    public Integer getInternalHddFreeSpacePercent() {
        return internalHddFreeSpacePercent;
    }

    public Integer getExternalHddFreeSpacePercent() {
        return externalHddFreeSpacePercent;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        HddCsvFileEntry that = (HddCsvFileEntry) obj;

        if (macId != null ? !macId.equals(that.macId) : that.macId != null) {
            return false;
        }
        if (stbSessionId != null ? !stbSessionId.equals(that.stbSessionId) : that.stbSessionId != null) {
            return false;
        }
        if (sequenceNumber != null ? !sequenceNumber.equals(that.sequenceNumber) : that.sequenceNumber != null) {
            return false;
        }
        if (eventTime != null ? !eventTime.equals(that.eventTime) : that.eventTime != null) {
            return false;
        }
        if (internalHddCapacity != null ? !internalHddCapacity.equals(that.internalHddCapacity) : that.internalHddCapacity != null) {
            return false;
        }
        if (externalHddCapacity != null ? !externalHddCapacity.equals(that.externalHddCapacity) : that.externalHddCapacity != null) {
            return false;
        }
        if (internalHddFreeSpacePercent != null ? !internalHddFreeSpacePercent.equals(that.internalHddFreeSpacePercent) : that.internalHddFreeSpacePercent != null) {
            return false;
        }
        return externalHddFreeSpacePercent != null ? externalHddFreeSpacePercent.equals(that.externalHddFreeSpacePercent) : that.externalHddFreeSpacePercent == null;
    }

    @Override
    public int hashCode() {
        int result = macId != null ? macId.hashCode() : 0;
        result = 31 * result + (stbSessionId != null ? stbSessionId.hashCode() : 0);
        result = 31 * result + (sequenceNumber != null ? sequenceNumber.hashCode() : 0);
        result = 31 * result + (eventTime != null ? eventTime.hashCode() : 0);
        result = 31 * result + (internalHddCapacity != null ? internalHddCapacity.hashCode() : 0);
        result = 31 * result + (externalHddCapacity != null ? externalHddCapacity.hashCode() : 0);
        result = 31 * result + (internalHddFreeSpacePercent != null ? internalHddFreeSpacePercent.hashCode() : 0);
        result = 31 * result + (externalHddFreeSpacePercent != null ? externalHddFreeSpacePercent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HddCsvFileEntry{" +
                "macId='" + macId + '\'' +
                ", stbSessionId='" + stbSessionId + '\'' +
                ", sequenceNumber=" + sequenceNumber +
                ", eventTime=" + eventTime +
                ", internalHddCapacity=" + internalHddCapacity +
                ", externalHddCapacity=" + externalHddCapacity +
                ", internalHddFreeSpacePercent=" + internalHddFreeSpacePercent +
                ", externalHddFreeSpacePercent=" + externalHddFreeSpacePercent +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String macId;
        private String stbSessionId;
        private Integer sequenceNumber;
        private LocalDateTime eventTime;
        private Integer internalHddCapacity;
        private Integer externalHddCapacity;
        private Integer internalHddFreeSpacePercent;
        private Integer externalHddFreeSpacePercent;

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setEventTime(LocalDateTime eventTime) {
            this.eventTime = eventTime;
            return this;
        }

        public Builder setInternalHddCapacity(Integer internalHddCapacity) {
            this.internalHddCapacity = internalHddCapacity;
            return this;
        }

        public Builder setExternalHddCapacity(Integer externalHddCapacity) {
            this.externalHddCapacity = externalHddCapacity;
            return this;
        }

        public Builder setInternalHddFreeSpacePercent(Integer internalHddFreeSpacePercent) {
            this.internalHddFreeSpacePercent = internalHddFreeSpacePercent;
            return this;
        }

        public Builder setExternalHddFreeSpacePercent(Integer externalHddFreeSpacePercent) {
            this.externalHddFreeSpacePercent = externalHddFreeSpacePercent;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public HddCsvFileEntry build() {
            HddCsvFileEntry target = new HddCsvFileEntry();
            target.macId = macId;
            target.stbSessionId = stbSessionId;
            target.sequenceNumber = sequenceNumber;
            target.eventTime = eventTime;
            target.internalHddCapacity = internalHddCapacity;
            target.externalHddCapacity = externalHddCapacity;
            target.internalHddFreeSpacePercent = internalHddFreeSpacePercent;
            target.externalHddFreeSpacePercent = externalHddFreeSpacePercent;

            return target;
        }
    }
}
