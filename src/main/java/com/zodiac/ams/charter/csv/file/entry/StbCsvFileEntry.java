package com.zodiac.ams.charter.csv.file.entry;

/**
 * The STB CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class StbCsvFileEntry {
    private String macId;
    private Integer nodeId;
    private Integer hubId;
    private String channelMapId;
    private Integer maxRebootsPerDay;

    /*
     * The private constructor.
     */
    private StbCsvFileEntry() {
        // do nothing here
    }

    public String getMacId() {
        return macId;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public Integer getHubId() {
        return hubId;
    }

    public String getChannelMapId() {
        return channelMapId;
    }

    public Integer getMaxRebootsPerDay() {
        return maxRebootsPerDay;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StbCsvFileEntry that = (StbCsvFileEntry) obj;

        if (macId != null ? !macId.equals(that.macId) : that.macId != null) {
            return false;
        }
        if (nodeId != null ? !nodeId.equals(that.nodeId) : that.nodeId != null) {
            return false;
        }
        if (hubId != null ? !hubId.equals(that.hubId) : that.hubId != null) {
            return false;
        }
        if (channelMapId != null ? !channelMapId.equals(that.channelMapId) : that.channelMapId != null) {
            return false;
        }
        return maxRebootsPerDay != null ? maxRebootsPerDay.equals(that.maxRebootsPerDay) : that.maxRebootsPerDay == null;
    }

    @Override
    public int hashCode() {
        int result = macId != null ? macId.hashCode() : 0;
        result = 31 * result + (nodeId != null ? nodeId.hashCode() : 0);
        result = 31 * result + (hubId != null ? hubId.hashCode() : 0);
        result = 31 * result + (channelMapId != null ? channelMapId.hashCode() : 0);
        result = 31 * result + (maxRebootsPerDay != null ? maxRebootsPerDay.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "StbCsvFileEntry{" +
                "macId='" + macId + '\'' +
                ", nodeId=" + nodeId +
                ", hubId=" + hubId +
                ", channelMapId='" + channelMapId + '\'' +
                ", maxRebootsPerDay=" + maxRebootsPerDay +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String macId;
        private Integer nodeId;
        private Integer hubId;
        private String channelMapId;
        private Integer maxRebootsPerDay;

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setNodeId(Integer nodeId) {
            this.nodeId = nodeId;
            return this;
        }

        public Builder setHubId(Integer hubId) {
            this.hubId = hubId;
            return this;
        }

        public Builder setChannelMapId(String channelMapId) {
            this.channelMapId = channelMapId;
            return this;
        }

        public Builder setMaxRebootsPerDay(Integer maxRebootsPerDay) {
            this.maxRebootsPerDay = maxRebootsPerDay;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public StbCsvFileEntry build() {
            StbCsvFileEntry target = new StbCsvFileEntry();
            target.macId = macId;
            target.nodeId = nodeId;
            target.hubId = hubId;
            target.channelMapId = channelMapId;
            target.maxRebootsPerDay = maxRebootsPerDay;

            return target;
        }
    }
}
