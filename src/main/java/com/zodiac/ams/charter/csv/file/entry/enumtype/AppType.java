package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The app type enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum AppType {
    APP_TYPE_0(0, "VC"),
    APP_TYPE_1(1, "CB"),
    APP_TYPE_2(2, "MG"),
    APP_TYPE_3(3, "IB"),
    APP_TYPE_4(4, "QS"),
    APP_TYPE_5(5, "AVNC"),
    APP_TYPE_6(6, "CID"),
    APP_TYPE_7(7, "OVRL"),
    APP_TYPE_8(8, "EAS"),
    APP_TYPE_9(9, "SYS"),
    APP_TYPE_10(10, "DVR"),
    APP_TYPE_11(11, "TPB"),
    APP_TYPE_12(12, "AVNC-NF"),
    APP_TYPE_13(13, "NRDP"),
    APP_TYPE_14(14, "AVNC-YT"),
    APP_TYPE_15(15, "RF-PAIRING");

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, AppType> intValueMap = new HashMap<>();
    private static final Map<String, AppType> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (AppType enumValue : AppType.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    AppType(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "AppType{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static AppType of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static AppType of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
