package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.TsbCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventType;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The TSB CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class TsbCsvFileParser extends AbstractCsvFileParser<TsbCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public TsbCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public TsbCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected TsbCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 8) {
            throw new IllegalArgumentException("The row size can't be less than 8");
        }

        TsbCsvFileEntry.Builder csvFileEntryBuilder = new TsbCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            csvFileEntryBuilder.setStbSessionId(rowEl1);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            Integer sequenceNumber = Integer.valueOf(rowEl2);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            Integer tmsId = Integer.valueOf(rowEl3);
            csvFileEntryBuilder.setTmsId(tmsId);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            LocalDateTime eventTime = LocalDateTime.parse(rowEl4);
            csvFileEntryBuilder.setEventTime(eventTime);
        }

        String rowEl5 = row.get(5);
        if (StringUtils.isNotBlank(rowEl5)) {
            TsbEventType eventType = TsbEventType.of(rowEl5);
            csvFileEntryBuilder.setEventType(eventType);
        }

        String rowEl6 = row.get(6);
        if (StringUtils.isNotBlank(rowEl6)) {
            TsbEventReason reason = TsbEventReason.of(rowEl6);
            csvFileEntryBuilder.setReason(reason);
        }

        String rowEl7 = row.get(7);
        if (StringUtils.isNotBlank(rowEl7)) {
            TsbEventScreenMode screenMode = TsbEventScreenMode.of(rowEl7);
            csvFileEntryBuilder.setScreenMode(screenMode);
        }

        return csvFileEntryBuilder.build();
    }
}
