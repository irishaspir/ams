package com.zodiac.ams.charter.csv.file.entry;

import com.zodiac.ams.charter.csv.file.entry.enumtype.AppScreen;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionActivity;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;

import java.time.LocalDateTime;

/**
 * The activity CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class ActivityCsvFileEntry {
    private String appSessionId;
    private SessionActivity sessionActivity;
    private String remoteKeys;
    private AppScreen appScreen;
    private String menuOptionSelected;
    private Integer tmsId;
    private LocalDateTime eventStartTime;
    private SessionEventReason reason;
    private String stbSessionId;
    private Integer sequenceNumber;
    private Integer avnClientSessionId;
    private String additionalInfo;

    /*
     * The private constructor.
     */
    private ActivityCsvFileEntry() {
        // do nothing here
    }

    public String getAppSessionId() {
        return appSessionId;
    }

    public SessionActivity getSessionActivity() {
        return sessionActivity;
    }

    public String getRemoteKeys() {
        return remoteKeys;
    }

    public AppScreen getAppScreen() {
        return appScreen;
    }

    public String getMenuOptionSelected() {
        return menuOptionSelected;
    }

    public Integer getTmsId() {
        return tmsId;
    }

    public LocalDateTime getEventStartTime() {
        return eventStartTime;
    }

    public SessionEventReason getReason() {
        return reason;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public Integer getAvnClientSessionId() {
        return avnClientSessionId;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ActivityCsvFileEntry that = (ActivityCsvFileEntry) obj;

        if (appSessionId != null ? !appSessionId.equals(that.appSessionId) : that.appSessionId != null) {
            return false;
        }
        if (sessionActivity != that.sessionActivity) {
            return false;
        }
        if (remoteKeys != null ? !remoteKeys.equals(that.remoteKeys) : that.remoteKeys != null) {
            return false;
        }
        if (appScreen != that.appScreen) {
            return false;
        }
        if (menuOptionSelected != null ? !menuOptionSelected.equals(that.menuOptionSelected) : that.menuOptionSelected != null) {
            return false;
        }
        if (tmsId != null ? !tmsId.equals(that.tmsId) : that.tmsId != null) {
            return false;
        }
        if (eventStartTime != null ? !eventStartTime.equals(that.eventStartTime) : that.eventStartTime != null) {
            return false;
        }
        if (reason != that.reason) {
            return false;
        }
        if (stbSessionId != null ? !stbSessionId.equals(that.stbSessionId) : that.stbSessionId != null) {
            return false;
        }
        if (sequenceNumber != null ? !sequenceNumber.equals(that.sequenceNumber) : that.sequenceNumber != null) {
            return false;
        }
        if (avnClientSessionId != null ? !avnClientSessionId.equals(that.avnClientSessionId) : that.avnClientSessionId != null) {
            return false;
        }
        return additionalInfo != null ? additionalInfo.equals(that.additionalInfo) : that.additionalInfo == null;
    }

    @Override
    public int hashCode() {
        int result = appSessionId != null ? appSessionId.hashCode() : 0;
        result = 31 * result + (sessionActivity != null ? sessionActivity.hashCode() : 0);
        result = 31 * result + (remoteKeys != null ? remoteKeys.hashCode() : 0);
        result = 31 * result + (appScreen != null ? appScreen.hashCode() : 0);
        result = 31 * result + (menuOptionSelected != null ? menuOptionSelected.hashCode() : 0);
        result = 31 * result + (tmsId != null ? tmsId.hashCode() : 0);
        result = 31 * result + (eventStartTime != null ? eventStartTime.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (stbSessionId != null ? stbSessionId.hashCode() : 0);
        result = 31 * result + (sequenceNumber != null ? sequenceNumber.hashCode() : 0);
        result = 31 * result + (avnClientSessionId != null ? avnClientSessionId.hashCode() : 0);
        result = 31 * result + (additionalInfo != null ? additionalInfo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ActivityCsvFileEntry{" +
                "appSessionId='" + appSessionId + '\'' +
                ", sessionActivity=" + sessionActivity +
                ", remoteKeys='" + remoteKeys + '\'' +
                ", appScreen=" + appScreen +
                ", menuOptionSelected='" + menuOptionSelected + '\'' +
                ", tmsId=" + tmsId +
                ", eventStartTime=" + eventStartTime +
                ", reason=" + reason +
                ", stbSessionId='" + stbSessionId + '\'' +
                ", sequenceNumber=" + sequenceNumber +
                ", avnClientSessionId=" + avnClientSessionId +
                ", additionalInfo='" + additionalInfo + '\'' +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String appSessionId;
        private SessionActivity sessionActivity;
        private String remoteKeys;
        private AppScreen appScreen;
        private String menuOptionSelected;
        private Integer tmsId;
        private LocalDateTime eventStartTime;
        private SessionEventReason reason;
        private String stbSessionId;
        private Integer sequenceNumber;
        private Integer avnClientSessionId;
        private String additionalInfo;

        public Builder setAppSessionId(String appSessionId) {
            this.appSessionId = appSessionId;
            return this;
        }

        public Builder setSessionActivity(SessionActivity sessionActivity) {
            this.sessionActivity = sessionActivity;
            return this;
        }

        public Builder setRemoteKeys(String remoteKeys) {
            this.remoteKeys = remoteKeys;
            return this;
        }

        public Builder setAppScreen(AppScreen appScreen) {
            this.appScreen = appScreen;
            return this;
        }

        public Builder setMenuOptionSelected(String menuOptionSelected) {
            this.menuOptionSelected = menuOptionSelected;
            return this;
        }

        public Builder setTmsId(Integer tmsId) {
            this.tmsId = tmsId;
            return this;
        }

        public Builder setEventStartTime(LocalDateTime eventStartTime) {
            this.eventStartTime = eventStartTime;
            return this;
        }

        public Builder setReason(SessionEventReason reason) {
            this.reason = reason;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setAvnClientSessionId(Integer avnClientSessionId) {
            this.avnClientSessionId = avnClientSessionId;
            return this;
        }

        public Builder setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public ActivityCsvFileEntry build() {
            ActivityCsvFileEntry target = new ActivityCsvFileEntry();
            target.appSessionId = appSessionId;
            target.sessionActivity = sessionActivity;
            target.remoteKeys = remoteKeys;
            target.appScreen = appScreen;
            target.menuOptionSelected = menuOptionSelected;
            target.tmsId = tmsId;
            target.eventStartTime = eventStartTime;
            target.reason = reason;
            target.stbSessionId = stbSessionId;
            target.sequenceNumber = sequenceNumber;
            target.avnClientSessionId = avnClientSessionId;
            target.additionalInfo = additionalInfo;

            return target;
        }
    }
}
