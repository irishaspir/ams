package com.zodiac.ams.charter.csv.file.entry.enumtype;

public enum TunerUse {
    RELEASED(0, "RELEASED"), 
    AVN(1, "AVN"), 
    VOD(2, "VOD"), 
    LINEAR(3, "LINEAR"), 
    PPV(4, "PPV"), 
    SDV(5, "SDV"), 
    EAS(6, "EAS"), 
    SGD(7, "SGD"), 
    IB(8, "IB"),
    UNKNOWN(9, "");
    
    private final int intValue;
    private final String value;
    
    private TunerUse(int intValue, String value){
        this.intValue = intValue;
        this.value = value;
    }
    
    public int getIntValue(){
        return intValue;
    }
    
    public String getValue(){
        return value;
    }
    
    public static TunerUse of(String value){
        for (TunerUse item: TunerUse.values()){
            if (item.value.equals(value))
                return item;
        }
        return null;        
    }
}
