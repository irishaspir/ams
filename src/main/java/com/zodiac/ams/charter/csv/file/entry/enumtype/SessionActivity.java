package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The session activity enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum SessionActivity {
    SESSION_ACTIVITY_1(1, "START"),
    SESSION_ACTIVITY_2(2, "APP"),
    SESSION_ACTIVITY_3(3, "HB"),
    SESSION_ACTIVITY_4(4, "WPIN"),
    SESSION_ACTIVITY_5(5, "VOL"),
    SESSION_ACTIVITY_6(6, "NUM-UPD"),
    SESSION_ACTIVITY_7(7, "QS-SYS"),
    SESSION_ACTIVITY_8(8, "MG-FP"),
    SESSION_ACTIVITY_9(9, "MG-DP"),
    SESSION_ACTIVITY_10(10, "AC-TUNE"),
    SESSION_ACTIVITY_11(11, "AC-LOST"),
    SESSION_ACTIVITY_12(12, "TP-SHOW"),
    SESSION_ACTIVITY_13(13, "TP-CLOSE"),
    SESSION_ACTIVITY_14(14, "MG-ERR"),
    SESSION_ACTIVITY_15(15, "MG-NOVOD"),
    SESSION_ACTIVITY_16(16, "QS-PC-CHANGE"),
    SESSION_ACTIVITY_17(17, "QS-NO-PC"),
    SESSION_ACTIVITY_18(18, "QS-OC-CHANGE"),
    SESSION_ACTIVITY_19(19, "RETURN"),
    SESSION_ACTIVITY_20(20, "AC-LAUNCH"),
    SESSION_ACTIVITY_21(21, "AC-REQUEST"),
    SESSION_ACTIVITY_22(22, "AC-RESPONCE"),
    SESSION_ACTIVITY_23(23, "AC-SHOW"),
    SESSION_ACTIVITY_24(24, "MOVE-TO"),
    SESSION_ACTIVITY_25(25, "DVR-DEL-REC"),
    SESSION_ACTIVITY_26(26, "DVR-DEL-FLD"),
    SESSION_ACTIVITY_27(27, "DVR-CNSL-REC"),
    SESSION_ACTIVITY_28(28, "WAIT-PIN"),
    SESSION_ACTIVITY_29(29, "MG-OPTIONS"),
    SESSION_ACTIVITY_30(30, "MG-NOFAVCH"),
    SESSION_ACTIVITY_31(31, "QS-NO-PU"),
    SESSION_ACTIVITY_32(32, "CWW-SHOW"),
    SESSION_ACTIVITY_33(33, "CWW-CLOSE"),
    SESSION_ACTIVITY_34(34, "AC-SETUP-FAIL"),
    SESSION_ACTIVITY_35(35, "AC-UI-RENDERED"),
    SESSION_ACTIVITY_36(36, "MG-TITLE-BLOCK"),
    SESSION_ACTIVITY_37(37, "EXIT"),
    SESSION_ACTIVITY_38(38, "PWRSAVE"),
    SESSION_ACTIVITY_39(39, "CRASH"),
    SESSION_ACTIVITY_40(40, "REBOOT"),
    SESSION_ACTIVITY_41(41, "SGD-FAIL");

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, SessionActivity> intValueMap = new HashMap<>();
    private static final Map<String, SessionActivity> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (SessionActivity enumValue : SessionActivity.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    SessionActivity(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "SessionActivity{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static SessionActivity of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static SessionActivity of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
