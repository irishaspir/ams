package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.StbCsvFileEntry;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * The STB CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class StbCsvFileParser extends AbstractCsvFileParser<StbCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public StbCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public StbCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected StbCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 5) {
            throw new IllegalArgumentException("The row size can't be less than 5");
        }

        StbCsvFileEntry.Builder csvFileEntryBuilder = new StbCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            Integer nodeId = Integer.valueOf(rowEl1);
            csvFileEntryBuilder.setNodeId(nodeId);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            Integer hubId = Integer.valueOf(rowEl2);
            csvFileEntryBuilder.setHubId(hubId);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            csvFileEntryBuilder.setChannelMapId(rowEl3);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            Integer maxRebootsPerDay = Integer.valueOf(rowEl4);
            csvFileEntryBuilder.setMaxRebootsPerDay(maxRebootsPerDay);
        }

        return csvFileEntryBuilder.build();
    }
}
