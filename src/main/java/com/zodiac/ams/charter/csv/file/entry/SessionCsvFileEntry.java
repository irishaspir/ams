package com.zodiac.ams.charter.csv.file.entry;

import com.zodiac.ams.charter.csv.file.entry.enumtype.AppType;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;

import java.time.LocalDateTime;

/**
 * The session CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class SessionCsvFileEntry {
    private String macId;
    private AppType appType;
    private String appSessionId;
    private LocalDateTime appExitTime;
    private SessionEventReason reason;
    private String stbSessionId;
    private Integer sequenceNumber;
    private Integer avnClientSessionId;

    /*
     * The private constructor.
     */
    private SessionCsvFileEntry() {
        // do nothing here
    }

    public String getMacId() {
        return macId;
    }

    public AppType getAppType() {
        return appType;
    }

    public String getAppSessionId() {
        return appSessionId;
    }

    public LocalDateTime getAppExitTime() {
        return appExitTime;
    }

    public SessionEventReason getReason() {
        return reason;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public Integer getAvnClientSessionId() {
        return avnClientSessionId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SessionCsvFileEntry that = (SessionCsvFileEntry) obj;

        if (macId != null ? !macId.equals(that.macId) : that.macId != null) {
            return false;
        }
        if (appType != that.appType) {
            return false;
        }
        if (appSessionId != null ? !appSessionId.equals(that.appSessionId) : that.appSessionId != null) {
            return false;
        }
        if (appExitTime != null ? !appExitTime.equals(that.appExitTime) : that.appExitTime != null) {
            return false;
        }
        if (reason != that.reason) {
            return false;
        }
        if (stbSessionId != null ? !stbSessionId.equals(that.stbSessionId) : that.stbSessionId != null) {
            return false;
        }
        if (sequenceNumber != null ? !sequenceNumber.equals(that.sequenceNumber) : that.sequenceNumber != null) {
            return false;
        }
        return avnClientSessionId != null ? avnClientSessionId.equals(that.avnClientSessionId) : that.avnClientSessionId == null;
    }

    @Override
    public int hashCode() {
        int result = macId != null ? macId.hashCode() : 0;
        result = 31 * result + (appType != null ? appType.hashCode() : 0);
        result = 31 * result + (appSessionId != null ? appSessionId.hashCode() : 0);
        result = 31 * result + (appExitTime != null ? appExitTime.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (stbSessionId != null ? stbSessionId.hashCode() : 0);
        result = 31 * result + (sequenceNumber != null ? sequenceNumber.hashCode() : 0);
        result = 31 * result + (avnClientSessionId != null ? avnClientSessionId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SessionCsvFileEntry{" +
                "macId='" + macId + '\'' +
                ", appType=" + appType +
                ", appSessionId='" + appSessionId + '\'' +
                ", appExitTime=" + appExitTime +
                ", reason=" + reason +
                ", stbSessionId='" + stbSessionId + '\'' +
                ", sequenceNumber=" + sequenceNumber +
                ", avnClientSessionId=" + avnClientSessionId +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String macId;
        private AppType appType;
        private String appSessionId;
        private LocalDateTime appExitTime;
        private SessionEventReason reason;
        private String stbSessionId;
        private Integer sequenceNumber;
        private Integer avnClientSessionId;

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setAppType(AppType appType) {
            this.appType = appType;
            return this;
        }

        public Builder setAppSessionId(String appSessionId) {
            this.appSessionId = appSessionId;
            return this;
        }

        public Builder setAppExitTime(LocalDateTime appExitTime) {
            this.appExitTime = appExitTime;
            return this;
        }

        public Builder setReason(SessionEventReason reason) {
            this.reason = reason;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setAvnClientSessionId(Integer avnClientSessionId) {
            this.avnClientSessionId = avnClientSessionId;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public SessionCsvFileEntry build() {
            SessionCsvFileEntry target = new SessionCsvFileEntry();
            target.macId = macId;
            target.appType = appType;
            target.appSessionId = appSessionId;
            target.appExitTime = appExitTime;
            target.reason = reason;
            target.stbSessionId = stbSessionId;
            target.sequenceNumber = sequenceNumber;
            target.avnClientSessionId = avnClientSessionId;

            return target;
        }
    }
}
