package com.zodiac.ams.charter.csv.file.parser;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.tests.dc.LocalFileUtils.readCsvFile;

/**
 * The abstract CSV file parser.
 *
 * @param <E> the type of parsed entry
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public abstract class AbstractCsvFileParser<E> implements CsvFileParser<E> {
    private static final char DEFAULT_SEPARATOR_CHAR = '|';

    /**
     * The filename.
     */
    private final String filename;
    /**
     * The separator character.
     */
    private final char separatorChar;

    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    protected AbstractCsvFileParser(String filename) {
        this(filename, DEFAULT_SEPARATOR_CHAR);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    protected AbstractCsvFileParser(String filename, char separatorChar) {
        if (StringUtils.isBlank(filename)) {
            throw new IllegalArgumentException("The filename can't be null or empty/blank");
        }

        // WTF?
        if (!Character.isDefined(separatorChar)) {
            throw new IllegalArgumentException("The separator char can't be undefined");
        }

        this.filename = filename;
        this.separatorChar = separatorChar;
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    public List<E> parse() throws IOException {
        List<List<String>> rows = readCsvFile(filename, separatorChar);
        if (CollectionUtils.isEmpty(rows)) {
            throw new IllegalArgumentException("The file content can't be null or empty/blank");
        }

        List<E> result = new ArrayList<>(rows.size());
        for (List<String> row : rows) {
            E entry = parseRow(row);
            result.add(entry);
        }

        return result;
    }

    /**
     * The method to parse a given row.
     *
     * @param row the row to parse
     * @return the parsed entry
     */
    protected abstract E parseRow(List<String> row);
}
