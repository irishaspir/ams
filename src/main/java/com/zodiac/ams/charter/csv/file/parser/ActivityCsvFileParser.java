package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.ActivityCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.AppScreen;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionActivity;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The activity CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class ActivityCsvFileParser extends AbstractCsvFileParser<ActivityCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public ActivityCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public ActivityCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected ActivityCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 11) {
            throw new IllegalArgumentException("The row size can't be less than 11");
        }

        ActivityCsvFileEntry.Builder csvFileEntryBuilder = new ActivityCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setAppSessionId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            SessionActivity sessionActivity = SessionActivity.of(rowEl1);
            csvFileEntryBuilder.setSessionActivity(sessionActivity);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            csvFileEntryBuilder.setRemoteKeys(rowEl2);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            AppScreen appScreen = AppScreen.of(rowEl3);
            csvFileEntryBuilder.setAppScreen(appScreen);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            csvFileEntryBuilder.setMenuOptionSelected(rowEl4);
        }

        String rowEl5 = row.get(5);
        if (StringUtils.isNotBlank(rowEl5)) {
            Integer tmsId = Integer.valueOf(rowEl5);
            csvFileEntryBuilder.setTmsId(tmsId);
        }

        String rowEl6 = row.get(6);
        if (StringUtils.isNotBlank(rowEl6)) {
            LocalDateTime eventStartTime = LocalDateTime.parse(rowEl6);
            csvFileEntryBuilder.setEventStartTime(eventStartTime);
        }

        String rowEl7 = row.get(7);
        if (StringUtils.isNotBlank(rowEl7)) {
            SessionEventReason reason = SessionEventReason.of(rowEl7);
            csvFileEntryBuilder.setReason(reason);
        }

        String rowEl8 = row.get(8);
        if (StringUtils.isNotBlank(rowEl8)) {
            csvFileEntryBuilder.setStbSessionId(rowEl8);
        }

        String rowEl9 = row.get(9);
        if (StringUtils.isNotBlank(rowEl9)) {
            Integer sequenceNumber = Integer.valueOf(rowEl9);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }

        String rowEl10 = row.get(10);
        if (StringUtils.isNotBlank(rowEl10)) {
            Integer avnClientSessionId = Integer.valueOf(rowEl10);
            csvFileEntryBuilder.setAvnClientSessionId(avnClientSessionId);
        }
// TODO fix later
//        String rowEl11 = row.get(11);
//        if (StringUtils.isNotBlank(rowEl11)) {
//            csvFileEntryBuilder.setAdditionalInfo(rowEl11);
//        }

        return csvFileEntryBuilder.build();
    }
}
