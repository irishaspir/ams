package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The tuning event reason enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum TuningEventReason {
    REASON_0(0, "CH+"),
    REASON_1(1, "CH-"),
    REASON_2(2, "FPCH+"),
    REASON_3(3, "FPCH-"),
    REASON_4(4, "NUM"),
    REASON_5(5, "LAST"),
    REASON_6(6, "AVN"),
    REASON_7(7, "APP"),
    REASON_8(8, "SYM"),
    REASON_9(9, "DIAG"),
    REASON_10(10, "FAV"),
    REASON_11(11, "POWER-ON");

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, TuningEventReason> intValueMap = new HashMap<>();
    private static final Map<String, TuningEventReason> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (TuningEventReason enumValue : TuningEventReason.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    TuningEventReason(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "TuningEventReason{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static TuningEventReason of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static TuningEventReason of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
