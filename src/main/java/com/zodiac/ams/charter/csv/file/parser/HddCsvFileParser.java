package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.HddCsvFileEntry;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The HDD CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class HddCsvFileParser extends AbstractCsvFileParser<HddCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public HddCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public HddCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected HddCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 8) {
            throw new IllegalArgumentException("The row size can't be less than 8");
        }

        HddCsvFileEntry.Builder csvFileEntryBuilder = new HddCsvFileEntry.Builder();


        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            csvFileEntryBuilder.setStbSessionId(rowEl1);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            Integer sequenceNumber = Integer.valueOf(rowEl2);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            LocalDateTime eventTime = LocalDateTime.parse(rowEl3);
            csvFileEntryBuilder.setEventTime(eventTime);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            Integer internalHddCapacity = Integer.valueOf(rowEl4);
            csvFileEntryBuilder.setInternalHddCapacity(internalHddCapacity);
        }

        String rowEl5 = row.get(5);
        if (StringUtils.isNotBlank(rowEl5)) {
            Integer externalHddCapacity = Integer.valueOf(rowEl5);
            csvFileEntryBuilder.setExternalHddCapacity(externalHddCapacity);
        }

        String rowEl6 = row.get(6);
        if (StringUtils.isNotBlank(rowEl6)) {
            Integer internalHddFreeSpacePercent = Integer.valueOf(rowEl6);
            csvFileEntryBuilder.setInternalHddFreeSpacePercent(internalHddFreeSpacePercent);
        }

        String rowEl7 = row.get(7);
        if (StringUtils.isNotBlank(rowEl7)) {
            Integer externalHddFreeSpacePercent = Integer.valueOf(rowEl7);
            csvFileEntryBuilder.setExternalHddFreeSpacePercent(externalHddFreeSpacePercent);
        }

        return csvFileEntryBuilder.build();
    }
}
