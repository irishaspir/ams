package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The app screen enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum AppScreen {
    // TODO fix later
    APP_SCREEN_1(1, "MAIN"),
    APP_SCREEN_2(2, "GRID"),
    APP_SCREEN_3(3, "FILTER"),
    APP_SCREEN_4(4, "DAY"),
    APP_SCREEN_5(5, "IB"),
    APP_SCREEN_6(6, "QS"),
    APP_SCREEN_7(7, "SYSINF"),
    APP_SCREEN_8(8, "VOL"),
    APP_SCREEN_9(9, "<app id>"),
    APP_SCREEN_10(10, "TUNE"),
    APP_SCREEN_11(11, "LOST"),
    APP_SCREEN_12(12, "OV-PC-PIN"),
    APP_SCREEN_13(13, "OV-PC-PIN2"),
    APP_SCREEN_14(14, "OV-PC-NO"),
    APP_SCREEN_15(15, "OV-FT"),
    APP_SCREEN_16(16, "OV-AUTO-OFF"),
    APP_SCREEN_17(17, "OV-ERR"),
    APP_SCREEN_18(18, "OV-NR-OD"),
    APP_SCREEN_19(19, "OD-CD"),
    APP_SCREEN_20(20, "OV-NR-MENU"),
    APP_SCREEN_21(21, "EPG-ERR"),
    APP_SCREEN_22(22, "OV-NR-GUIDE"),
    APP_SCREEN_23(23, "OV-NAUT"),
    APP_SCREEN_24(24, "OV-PU-PIN"),
    APP_SCREEN_25(25, "OV-PU-PIN2"),
    APP_SCREEN_26(26, "OV-SAFE"),
    APP_SCREEN_27(27, "NO-VOD"),
    APP_SCREEN_28(28, "OV-PPV-BGN"),
    APP_SCREEN_29(29, "OV-HIA"),
    APP_SCREEN_30(30, "DIAG<N>"),
    APP_SCREEN_31(31, "AVCTV"),
    APP_SCREEN_32(32, "TPB"),
    APP_SCREEN_33(33, "CLID"),
    APP_SCREEN_34(34, "SET-DIS"),
    APP_SCREEN_35(35, "STB-NOT-AUTH"),
    APP_SCREEN_36(36, "COPY-PROT"),
    APP_SCREEN_37(37, "OV-FIRST-RUN"),
    APP_SCREEN_38(38, "OV-WAIT"),
    APP_SCREEN_39(39, "OV-STB-START"),
    APP_SCREEN_40(40, "OV-PPV-ORDER"),
    APP_SCREEN_41(41, "OV-TUNE-FAIL"),
    APP_SCREEN_42(42, "OV-PU-NO"),
    APP_SCREEN_43(43, "OV-NR-INFO"),
    APP_SCREEN_44(44, "OV-HD"),
    APP_SCREEN_45(45, "OV-PPV-ON"),
    APP_SCREEN_46(46, "OV-PPV-NA"),
    APP_SCREEN_47(47, "OV-IU-NA"),
    APP_SCREEN_48(48, "OV-EDIT-REC"),
    APP_SCREEN_49(49, "OV-EDIT-SER"),
    APP_SCREEN_50(50, "OV-RECPRS"),
    APP_SCREEN_51(51, "OV-CNLREC-C"),
    APP_SCREEN_52(52, "OV-RECS-C"),
    APP_SCREEN_53(53, "OV-RL-REC"),
    APP_SCREEN_54(54, "OV-RL-TV"),
    APP_SCREEN_55(55, "OV-CNLREC"),
    APP_SCREEN_56(56, "OV-SCHREC"),
    APP_SCREEN_57(57, "OV-TVPAUSE"),
    APP_SCREEN_58(58, "OV-REC-FAIL"),
    APP_SCREEN_59(59, "OV-REC-NA"),
    APP_SCREEN_60(60, "OV-CURREC-PRS"),
    APP_SCREEN_61(61, "OV-ACT-NA"),
    APP_SCREEN_62(62, "OV-PLBCPL"),
    APP_SCREEN_63(63, "OV-DLTREC"),
    APP_SCREEN_64(64, "DVR-MREC"),
    APP_SCREEN_65(65, "DVR-MREC-SO"),
    APP_SCREEN_66(66, "DVR-DLT-PRG"),
    APP_SCREEN_67(67, "DVR-DLD-FLD"),
    APP_SCREEN_68(68, "OV-REC-BGN"),
    APP_SCREEN_69(69, "OV-TUNE-TSB"),
    APP_SCREEN_70(70, "TPB-TRPL"),
    APP_SCREEN_71(71, "AVC-CURWIN"),
    APP_SCREEN_72(72, "OV-RM-WCH"),
    APP_SCREEN_73(73, "OV-PC-WAIT"),
    APP_SCREEN_74(74, "OV-PU-WAIT"),
    APP_SCREEN_75(75, "MG-ADD-FAVCH"),
    APP_SCREEN_76(76, "MG-OPTIONS"),
    APP_SCREEN_77(77, "OV-WOPTIONS"),
    APP_SCREEN_78(78, "OV-NR-SET"),
    APP_SCREEN_79(79, "MG-NO-FAVCH"),
    APP_SCREEN_80(80, "OV-UPG-SUBS"),
    APP_SCREEN_81(81, "OV-CANCEL-NA"),
    APP_SCREEN_82(82, "OV-SCH-NA"),
    APP_SCREEN_83(83, "OV-SAVE-NA"),
    APP_SCREEN_84(84, "OV-DEL-NA"),
    APP_SCREEN_85(85, "AVCNF"),
    APP_SCREEN_86(86, "NRDP"),
    APP_SCREEN_87(87, "OV-JUMP-TO"),
    APP_SCREEN_88(88, "AVCYT"),
    APP_SCREEN_89(89, "OV-NR-YT"),
    APP_SCREEN_90(90, "OV-CH-NA"),
    APP_SCREEN_91(91, "OV-SET-STAT"),
    APP_SCREEN_92(92, "OV-TITLE-BLOCK"),
    APP_SCREEN_93(93, "RF-PAIRING-START"),
    APP_SCREEN_94(94, "RF-PAIRED"),
    APP_SCREEN_95(95, "RF-PAIRING-FAIL"),
    APP_SCREEN_96(96, "TV-DETECTED"),
    APP_SCREEN_97(97, "TV-SEL-BRAND"),
    APP_SCREEN_98(98, "TV-SEL-BRAND-ALL"),
    APP_SCREEN_99(99, "TV-TEST1"),
    APP_SCREEN_100(100, "TV-TEST2"),
    APP_SCREEN_101(101, "TV-TEST3"),
    APP_SCREEN_102(102, "TV-TEST4"),
    APP_SCREEN_103(103, "TV- BRAND-CODE"),
    APP_SCREEN_104(104, "TV- BRAND-CODE2"),
    APP_SCREEN_105(105, "TV-TEST-ALL"),
    APP_SCREEN_106(106, "TV-CONNECTED"),
    APP_SCREEN_107(107, "TV-CONNECTED-COMPL"),
    APP_SCREEN_108(108, "TV-TEST-FAIL"),
    APP_SCREEN_109(109, "AUD-DETECTED"),
    APP_SCREEN_110(110, "AUD-SEL-BRAND"),
    APP_SCREEN_111(111, "AUD-SEL-BRAND-ALL"),
    APP_SCREEN_112(112, "AUD-TEST1"),
    APP_SCREEN_113(113, "AUD-TEST2"),
    APP_SCREEN_114(114, "AUD-TEST3"),
    APP_SCREEN_115(115, "AUD-TEST4"),
    APP_SCREEN_116(116, "AUD-BRAND-CODE"),
    APP_SCREEN_117(117, "AUD-BRAND-CODE2"),
    APP_SCREEN_118(118, "AUD-TEST-ALL"),
    APP_SCREEN_119(119, "AUD-CONNECTED"),
    APP_SCREEN_120(120, "AUD-CONNECTED-COMPL"),
    APP_SCREEN_121(121, "AUD-TEST-FAIL"),
    APP_SCREEN_122(122, "OV-PPV-WAIT"),
    APP_SCREEN_123(123, "OV-SDV-WAIT");

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, AppScreen> intValueMap = new HashMap<>();
    private static final Map<String, AppScreen> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (AppScreen enumValue : AppScreen.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    AppScreen(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "AppScreen{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static AppScreen of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static AppScreen of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
