package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.TunerUseCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.ScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TunerUse;
import com.zodiac.ams.charter.csv.file.entry.enumtype.YesNo;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class TunerUseCsvFileParser extends AbstractCsvFileParser<TunerUseCsvFileEntry> {
    
    public TunerUseCsvFileParser(String filename) {
        super(filename);
    }

    public TunerUseCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    @Override
    protected TunerUseCsvFileEntry parseRow(List<String> row) {
        
        if (row.size() < 10) {
            throw new IllegalArgumentException("The row size can't be less than 10");
        }

        TunerUseCsvFileEntry.Builder csvFileEntryBuilder = new TunerUseCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            csvFileEntryBuilder.setStbSessionId(rowEl1);
        }
        
        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            Integer sequenceNumber = Integer.valueOf(rowEl2);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }
        
        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            LocalDateTime eventStartTime = LocalDateTime.parse(rowEl3);
            csvFileEntryBuilder.setEventStartTime(eventStartTime);
        }
        
        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            Integer tunerId = Integer.valueOf(rowEl4);
            csvFileEntryBuilder.setTunerId(tunerId);
        }
        
        String rowEl5 = row.get(5);
        csvFileEntryBuilder.setTunerUse(TunerUse.of(rowEl5));
        
        String rowEl6 = row.get(6);
        csvFileEntryBuilder.setScreenMode(ScreenMode.of(rowEl6));
        
        String rowEl7 = row.get(7);
        csvFileEntryBuilder.setTsbRecording(YesNo.of(rowEl7));
        
        String rowEl8 = row.get(8);
        csvFileEntryBuilder.setDvrRecording(YesNo.of(rowEl8));
        
        String rowEl9 = row.get(9);
        csvFileEntryBuilder.setDlnaOutput(YesNo.of(rowEl9));

        return csvFileEntryBuilder.build();
    }
}
