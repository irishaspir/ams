package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The session event reason enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum SessionEventReason {
    // TODO fix later
    REASON_1(1, "DIAG"),
    REASON_2(2, "TIME"),
    REASON_3(3, "PWROFF"),
    REASON_4(4, "REBOOT"),
    REASON_6(6, "QS-AU-DEF"),
    REASON_7(7, "QS-AU-ENG"),
    REASON_8(8, "QS-AU-ESP"),
    REASON_9(9, "QS-AU-FRA"),
    REASON_10(10, "QS-DVS-ON"),
    REASON_11(11, "QS-DVS-OFF"),
    REASON_12(12, "QS-OF-1080P"),
    REASON_13(13, "QS-OF-1080I"),
    REASON_14(14, "QS-OF-720P"),
    REASON_15(15, "QS-OF-720I"),
    REASON_16(16, "QS-OF-480I"),
    REASON_17(17, "QS-PC-ON"),
    REASON_18(18, "QS-PC-OFF"),
    REASON_19(19, "QS-OC-ON"),
    REASON_20(20, "QS-OC-OFF"),
    REASON_21(21, "MG-TUNE"),
    REASON_22(22, "PPV-BGN-YES"),
    REASON_23(23, "PPV-BGN-NO"),
    REASON_24(24, "VOD-TUNE"),
    REASON_25(25, "B-OK"),
    REASON_26(26, "B-CANCEL"),
    REASON_27(27, "B-CLOSE"),
    REASON_28(28, "ERROR"),
    REASON_29(29, "SUSP"),
    REASON_30(30, "B-QS"),
    REASON_31(31, "B-MG"),
    REASON_32(32, "B-WN"),
    REASON_33(33, "C-EXIT"),
    REASON_34(34, "C-TUNE"),
    REASON_35(35, "FREXIT"),
    REASON_36(36, "QS-SI-VIEW"),
    REASON_37(37, "QS-CC-ON"),
    REASON_38(38, "QS-CC-OFF"),
    REASON_39(39, "APP"),
    REASON_40(40, "QS-SI-BACK"),
    REASON_41(41, "SD-STB-SET"),
    REASON_42(42, "SD-CUR-SET"),
    REASON_43(43, "SD-AVN-SET"),
    REASON_44(44, "B-REC-OPTIONS"),
    REASON_45(45, "B-MORE-INFO"),
    REASON_46(46, "B-UPGRADE"),
    REASON_47(47, "B-KEEP"),
    REASON_48(48, "B-CANCEL-REC"),
    REASON_49(49, "B-CANCEL-SER"),
    REASON_150(150, "B-EDIT-REC"),
    REASON_151(151, "B-EDIT-SER"),
    REASON_152(152, "B-REC-SER"),
    REASON_153(153, "B-SAVE"),
    REASON_154(154, "B-BROWSE-REC"),
    REASON_155(155, "B-OPTIONS"),
    REASON_156(156, "B-RETRY"),
    REASON_157(157, "B-DELETE"),
    REASON_158(158, "B-WATCH-NXT"),
    REASON_159(159, "B-CONNECT-TV"),
    REASON_160(160, "B-CONNECT-AUD"),
    REASON_161(161, "B-VIEW-ALL"),
    REASON_162(162, "B-BACK"),
    REASON_163(163, "B-CLEAR"),
    REASON_164(164, "B-COMPLETE"),
    REASON_165(165, "B-ENTER-MANUAL"),
    REASON_166(166, "B-PLAY"),
    REASON_167(167, "B-RESTART"),
    REASON_168(168, "B-RESUME"),
    REASON_169(169, "RF-TEST-OK"),
    REASON_170(170, "RF-TEST-FAIL"),
    REASON_171(171, "MG-OPT-DARK"),
    REASON_172(172, "MG-OPT-LIGHT"),
    REASON_173(173, "MG-OPT-FILTER"),
    REASON_174(174, "MG-OPT-DAY"),
    REASON_175(175, "MG-OPT-AT-OFF"),
    REASON_176(176, "MG-OPT-AT-ON"),
    REASON_177(177, "MG-OPT-AHEAD24"),
    REASON_178(178, "MG-OPT-BACK24"),
    REASON_179(179, "COMPL"),
    REASON_180(180, "AVN-SRV-NA"),
    REASON_181(181, "AVN-APP-NA"),
    REASON_182(182, "AVN-SETUP-TIME"),
    REASON_183(183, "AVN-SRVRES-NA"),
    REASON_184(184, "AVN-SRV-SETUPERR"),
    REASON_185(185, "AVN-NO-TUNEINFO"),
    REASON_186(186, "AVN-INV-PAR"),
    REASON_187(187, "AVN-TUNE-FAIL"),
    REASON_188(188, "AVN-TSID-MISM"),
    REASON_189(189, "AVN-STRM-LOST"),
    REASON_190(190, "AVN-CL-HB"),
    REASON_191(191, "AVN-SRV-ERR"),
    REASON_192(192, "AVN-SRV-TIME"),
    REASON_193(193, "AVN-SRV-HB"),
    REASON_194(194, "AVN-SRV-QUAR"),
    REASON_195(195, "AVN-CL-INTL"),
    REASON_196(196, "AVN-MSG-FAIL"),
    REASON_197(197, "AVN-NET-BAD"),
    REASON_198(198, "N-EXIT"),
    REASON_199(199, "CD"),
    REASON_200(200, "NUM"),
    REASON_201(201, "QS-NARR-ON"),
    REASON_202(202, "QS-NARR-OFF"),
    REASON_203(203, "BOOT"),
    REASON_204(204, "CONFIG"),
    REASON_205(205, "PWRON"),
    REASON_206(206, "CRASH"),
    REASON_207(207, "DAC"),
    REASON_208(208, "AVNAN-ERR"),
    REASON_209(209, "AVNAN-NO-TUNEINFO"),
    REASON_210(210, "AVNAN-TUNE-FAIL"),
    REASON_211(211, "AVNAN-CL-INTR"),
    REASON_212(212, "AVNAN-ERR-NET"),
    REASON_213(213, "AVNAN-SETUP-TIME"),
    REASON_214(214, "AVNAN-ERR-CONNREFUSED"),
    REASON_215(215, "AVNAN-TSID-MISM"),
    REASON_216(216, ""),
    REASON_217(217, "AVNAN-ERR-SERVERCFG"),
    REASON_218(218, "AVNAN-ERR-TIMEOUT"),
    REASON_219(219, "AVNAN-ERR-REDIRLIM"),
    REASON_220(220, "AVNAN-ERR-APPNOTFOUND"),
    REASON_221(221, "AVNAN-ERR-BANDWIDTH"),
    REASON_222(222, "AVNAN-ERR-SERVERRES"),
    REASON_223(223, "AVNAN-ERR-LATENCY"),
    REASON_225(225, "AVNAN-ERR-UNKNOWN"),
    REASON_226(226, "AVNAN-ERR-HEARTBEAT"),
    REASON_227(227, "AVNAN-ERR-SERVEREXEC"),
    REASON_228(228, "AVNAN-ERR-STREAM"),
    REASON_229(229, "AVNAN-ERR-INVAL"),
    REASON_230(230, ""),
    REASON_231(231, "RF-BRAND-SELECTED"),
    REASON_232(232, "INV-PAR"),
    REASON_233(233, "ATT-EXC"),
    REASON_234(234, "AVN-SRV-SG");
// 50..149 - code of key - see  Note 1 below
// >=1000 - WB reboot reason code - see  Note 3 below

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, SessionEventReason> intValueMap = new HashMap<>();
    private static final Map<String, SessionEventReason> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (SessionEventReason enumValue : SessionEventReason.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    SessionEventReason(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "SessionEventReason{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static SessionEventReason of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static SessionEventReason of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
