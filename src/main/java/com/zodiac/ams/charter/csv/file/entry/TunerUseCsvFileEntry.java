package com.zodiac.ams.charter.csv.file.entry;

import com.zodiac.ams.charter.csv.file.entry.enumtype.ScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TunerUse;
import com.zodiac.ams.charter.csv.file.entry.enumtype.YesNo;
import java.time.LocalDateTime;

public class TunerUseCsvFileEntry {
    
    private String macId;
    private String stbSessionId;
    private Integer sequenceNumber;
    private LocalDateTime eventStartTime;
    private Integer tunerId;
    private TunerUse tunerUse;
    private ScreenMode screenMode;
    private YesNo tsbRecording;
    private YesNo dvrRecording;
    private YesNo dlnaOutput;
    
    private TunerUseCsvFileEntry() {
    }

    public String getMacId() {
        return macId;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public LocalDateTime getEventStartTime() {
        return eventStartTime;
    }
    
    public Integer getTunerId(){
        return tunerId;
    }
    
    public TunerUse getTunerUse(){
        return tunerUse;
    }
    
    public ScreenMode getScreenMode(){
        return screenMode;
    }
    
    public YesNo getTsbRecording(){
        return tsbRecording;
    }
    
    public YesNo getDvrRecording(){
        return dvrRecording;
    }
    
    public YesNo getDlnaOutput(){
        return dlnaOutput;
    }
    
    private boolean compare(Object obj1, Object obj2){
        if (null == obj1 && null == obj2)
            return true;
        return (null == obj1) ? false : obj1.equals(obj2);
        
    }
    

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TunerUseCsvFileEntry))
            return false;

        TunerUseCsvFileEntry that = (TunerUseCsvFileEntry) obj;
        if (!compare(macId, that.macId))
            return false;
        
        if (!compare(stbSessionId, that.stbSessionId))
            return false;
        
        if (!compare(sequenceNumber, that.sequenceNumber))
            return false;
        
        if (!compare(eventStartTime, that.eventStartTime))
            return false;
        
        if (!compare(tunerId, that.tunerId))
            return false;
        
        if (!compare(tunerUse, that.tunerUse))
            return false;
        
        if (!compare(screenMode, that.screenMode))
            return false;
        
        if (!compare(tsbRecording, that.tsbRecording))
            return false;
        
        if (!compare(dvrRecording, that.dvrRecording))
            return false;
        
        if (!compare(dlnaOutput, that.dlnaOutput))
            return false;
        
        return true;
    
    }

    
    @Override
    public String toString() {
        return "TunerUseCsvFileEntry{" +
                "macId=" + macId + 
                ", stbSessionId=" + stbSessionId + 
                ", sequenceNumber=" + sequenceNumber +
                ", eventStartTime=" + eventStartTime +
                ", tunerId=" + tunerId +
                ", tunerUse=" + (null == tunerUse ? "null" : tunerUse.getValue())+
                ", screenMode=" + (null == screenMode ? "null" : screenMode.getValue()) +
                ", tsbRecording=" + (null == tsbRecording ? "null" : tsbRecording.getValue()) +
                ", dvrRecording=" + (null == dvrRecording ? "null" : dvrRecording.getValue()) +
                ", dlnaOutput=" + (null == dlnaOutput ? "null" : dlnaOutput.getValue()) +
                '}';
    }

    public static class Builder {
        
        private String macId;
        private String stbSessionId;
        private Integer sequenceNumber;
        private LocalDateTime eventStartTime;
        private Integer tunerId;
        private TunerUse tunerUse;
        private ScreenMode screenMode;
        private YesNo tsbRecording;
        private YesNo dvrRecording;
        private YesNo dlnaOutput;
    

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setEventStartTime(LocalDateTime eventStartTime) {
            this.eventStartTime = eventStartTime;
            return this;
        }
        
        public Builder setTunerId(Integer tunerId){
            this.tunerId = tunerId;
            return this;
        }
        
        public Builder setTunerUse(TunerUse tunerUse){
            this.tunerUse = tunerUse;
            return this;
        }
        
        public Builder setScreenMode(ScreenMode screenMode){
            this.screenMode = screenMode;
            return this;
        }
        
        public Builder setTsbRecording(YesNo tsbRecording){
            this.tsbRecording = tsbRecording;
            return this;
        }
        
        public Builder setDvrRecording(YesNo dvrRecording){
            this.dvrRecording = dvrRecording;
            return this;
        }
        
        public Builder setDlnaOutput(YesNo dlnaOutput){
            this.dlnaOutput = dlnaOutput;
            return this;
        }


        public TunerUseCsvFileEntry build() {
            TunerUseCsvFileEntry target = new TunerUseCsvFileEntry();
            target.macId = macId;
            target.stbSessionId = stbSessionId;
            target.sequenceNumber = sequenceNumber;
            target.eventStartTime = eventStartTime;
            target.tunerId = tunerId;
            target.tunerUse = tunerUse;
            target.screenMode = screenMode;
            target.tsbRecording = tsbRecording;
            target.dvrRecording = dvrRecording;
            target.dlnaOutput = dlnaOutput;
            return target;
        }
    }
    
}
