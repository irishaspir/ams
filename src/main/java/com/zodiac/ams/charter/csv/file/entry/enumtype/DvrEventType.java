package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The DVR event type enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum DvrEventType {
    TYPE_1(1, "TS"),
    TYPE_2(2, "TUNE-AWAY"),
    TYPE_3(3, "RESUME-LAST"),
    TYPE_4(4, "RESUME-START"),
    TYPE_5(5, "REC"),
    TYPE_6(6, "LIVE"),
    TYPE_7(7, "TSB-LOST");

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, DvrEventType> intValueMap = new HashMap<>();
    private static final Map<String, DvrEventType> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (DvrEventType enumValue : DvrEventType.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    DvrEventType(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "DvrEventType{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static DvrEventType of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static DvrEventType of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
