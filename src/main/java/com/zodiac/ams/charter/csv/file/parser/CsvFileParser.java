package com.zodiac.ams.charter.csv.file.parser;

import java.io.IOException;
import java.util.List;

/**
 * The CSV file parser interface.
 *
 * @param <E> the type of parsed entry
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public interface CsvFileParser<E> {
    /**
     * The method to parse a given file.
     *
     * @return the list of parsed entry
     * @throws IOException in case of an I/O error while reading the file
     */
    List<E> parse() throws IOException;
}
