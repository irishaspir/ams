package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.DvrCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventOperationResult;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.DvrEventType;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The DVR CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class DvrCsvFileParser extends AbstractCsvFileParser<DvrCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public DvrCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public DvrCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected DvrCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 18) {
            throw new IllegalArgumentException("The row size can't be less than 18");
        }

        DvrCsvFileEntry.Builder csvFileEntryBuilder = new DvrCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            csvFileEntryBuilder.setStbSessionId(rowEl1);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            Integer sequenceNumber = Integer.valueOf(rowEl2);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            LocalDateTime eventTime = LocalDateTime.parse(rowEl3);
            csvFileEntryBuilder.setEventTime(eventTime);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            DvrEventType eventType = DvrEventType.of(rowEl4);
            csvFileEntryBuilder.setEventType(eventType);
        }

        String rowEl5 = row.get(5);
        if (StringUtils.isNotBlank(rowEl5)) {
            DvrEventReason reason = DvrEventReason.of(rowEl5);
            csvFileEntryBuilder.setReason(reason);
        }

        String rowEl6 = row.get(6);
        if (StringUtils.isNotBlank(rowEl6)) {
            DvrEventOperationResult operationResult = DvrEventOperationResult.of(rowEl6);
            csvFileEntryBuilder.setOperationResult(operationResult);
        }

        String rowEl7 = row.get(7);
        if (StringUtils.isNotBlank(rowEl7)) {
            csvFileEntryBuilder.setRecordingId(rowEl7);
        }

        String rowEl8 = row.get(8);
        if (StringUtils.isNotBlank(rowEl8)) {
            csvFileEntryBuilder.setTmsProgramId(rowEl8);
        }

        String rowEl9 = row.get(9);
        if (StringUtils.isNotBlank(rowEl9)) {
            csvFileEntryBuilder.setTmsChannelId(rowEl9);
        }

        String rowEl10 = row.get(10);
        if (StringUtils.isNotBlank(rowEl10)) {
            Integer channelNumber = Integer.valueOf(rowEl10);
            csvFileEntryBuilder.setChannelNumber(channelNumber);
        }

        String rowEl11 = row.get(11);
        if (StringUtils.isNotBlank(rowEl11)) {
            LocalDateTime scheduledStartTime = LocalDateTime.parse(rowEl11);
            csvFileEntryBuilder.setScheduledStartTime(scheduledStartTime);
        }

        String rowEl12 = row.get(12);
        if (StringUtils.isNotBlank(rowEl12)) {
            LocalDateTime scheduledEndTime = LocalDateTime.parse(rowEl12);
            csvFileEntryBuilder.setScheduledEndTime(scheduledEndTime);
        }

        String rowEl13 = row.get(13);
        if (StringUtils.isNotBlank(rowEl13)) {
            LocalDateTime actualStartTime = LocalDateTime.parse(rowEl13);
            csvFileEntryBuilder.setActualStartTime(actualStartTime);
        }

        String rowEl14 = row.get(14);
        if (StringUtils.isNotBlank(rowEl14)) {
            LocalDateTime actualEndTime = LocalDateTime.parse(rowEl14);
            csvFileEntryBuilder.setActualEndTime(actualEndTime);
        }

        String rowEl15 = row.get(15);
        if (StringUtils.isNotBlank(rowEl15)) {
            Integer internalHddFreeSpacePercent = Integer.valueOf(rowEl15);
            csvFileEntryBuilder.setInternalHddFreeSpacePercent(internalHddFreeSpacePercent);
        }

        String rowEl16 = row.get(16);
        if (StringUtils.isNotBlank(rowEl16)) {
            Integer externalHddFreeSpacePercent = Integer.valueOf(rowEl16);
            csvFileEntryBuilder.setExternalHddFreeSpacePercent(externalHddFreeSpacePercent);
        }

        String rowEl17 = row.get(17);
        if (StringUtils.isNotBlank(rowEl17)) {
            Integer watchedVideoCurrentPosition = Integer.valueOf(rowEl17);
            csvFileEntryBuilder.setWatchedVideoCurrentPosition(watchedVideoCurrentPosition);
        }

        return csvFileEntryBuilder.build();
    }
}
