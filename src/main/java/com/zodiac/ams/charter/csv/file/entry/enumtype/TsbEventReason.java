package com.zodiac.ams.charter.csv.file.entry.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * The TSB event reason enum type.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public enum TsbEventReason {
    REASON_1(1, "TUNE"),
    REASON_2(2, "AVN"),
    REASON_3(3, "REC-PLBK"),
    REASON_4(4, "AVN-CMD"),
    REASON_5(5, "TUN-CONFL"),
    REASON_6(6, "ERROR"),
    REASON_7(7, "LOW-SPACE"),
    REASON_8(8, "APP");
// 50..149 - code of key - see  Note 1
// otherwise -> empty

    /**
     * The int value.
     */
    private final int intValue;

    /**
     * The value.
     */
    private final String value;

    private static final Map<Integer, TsbEventReason> intValueMap = new HashMap<>();
    private static final Map<String, TsbEventReason> valueMap = new HashMap<>();

    /*
     * The static initialization block.
     */
    static {
        for (TsbEventReason enumValue : TsbEventReason.values()) {
            intValueMap.put(enumValue.intValue, enumValue);
            valueMap.put(enumValue.value, enumValue);
        }
    }

    /**
     * The regular constructor.
     *
     * @param intValue the int value
     * @param value    the value
     */
    TsbEventReason(int intValue, String value) {
        this.intValue = intValue;
        this.value = value;
    }

    public int intValue() {
        return intValue;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "TsbEventReason{" +
                "intValue=" + intValue +
                ", value='" + value + '\'' +
                '}';
    }

    /**
     * The factory method to get an enum constant by int value.
     *
     * @param intValue the int value
     * @return the enum constant
     */
    public static TsbEventReason of(int intValue) {
        return intValueMap.get(intValue);
    }

    /**
     * The factory method to get an enum constant by value.
     *
     * @param value the value
     * @return the enum constant
     */
    public static TsbEventReason of(String value) {
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("The value can't be null or empty/blank");
        }
        return valueMap.get(value);
    }
}
