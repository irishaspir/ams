package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.SessionCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.AppType;
import com.zodiac.ams.charter.csv.file.entry.enumtype.SessionEventReason;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The session CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class SessionCsvFileParser extends AbstractCsvFileParser<SessionCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public SessionCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public SessionCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected SessionCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 8) {
            throw new IllegalArgumentException("The row size can't be less than 8");
        }

        SessionCsvFileEntry.Builder csvFileEntryBuilder = new SessionCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            AppType appType = AppType.of(rowEl1);
            csvFileEntryBuilder.setAppType(appType);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            csvFileEntryBuilder.setAppSessionId(rowEl2);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            LocalDateTime appExitTime = LocalDateTime.parse(rowEl3);
            csvFileEntryBuilder.setAppExitTime(appExitTime);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            SessionEventReason reason = SessionEventReason.of(rowEl4);
            csvFileEntryBuilder.setReason(reason);
        }

        String rowEl5 = row.get(5);
        if (StringUtils.isNotBlank(rowEl5)) {
            csvFileEntryBuilder.setStbSessionId(rowEl5);
        }

        String rowEl6 = row.get(6);
        if (StringUtils.isNotBlank(rowEl6)) {
            Integer sequenceNumber = Integer.valueOf(rowEl6);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }

        String rowEl7 = row.get(7);
        if (StringUtils.isNotBlank(rowEl7)) {
            Integer avnClientSessionId = Integer.valueOf(rowEl7);
            csvFileEntryBuilder.setAvnClientSessionId(avnClientSessionId);
        }

        return csvFileEntryBuilder.build();
    }
}
