package com.zodiac.ams.charter.csv.file.parser;

import com.zodiac.ams.charter.csv.file.entry.TuningCsvFileEntry;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TuningEventReason;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The tuning CSV file parser.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class TuningCsvFileParser extends AbstractCsvFileParser<TuningCsvFileEntry> {
    /**
     * The regular constructor.
     *
     * @param filename the filename
     */
    public TuningCsvFileParser(String filename) {
        super(filename);
    }

    /**
     * The regular constructor.
     *
     * @param filename      the filename
     * @param separatorChar the separator character
     */
    public TuningCsvFileParser(String filename, char separatorChar) {
        super(filename, separatorChar);
    }

    /*
     * (non-Javadoc)
     *
     * @see CsvFileParser#parse()
     */
    @Override
    protected TuningCsvFileEntry parseRow(List<String> row) {
        if (row.size() < 8) {
            throw new IllegalArgumentException("The row size can't be less than 8");
        }

        TuningCsvFileEntry.Builder csvFileEntryBuilder = new TuningCsvFileEntry.Builder();

        String rowEl0 = row.get(0);
        if (StringUtils.isNotBlank(rowEl0)) {
            csvFileEntryBuilder.setMacId(rowEl0);
        }

        String rowEl1 = row.get(1);
        if (StringUtils.isNotBlank(rowEl1)) {
            csvFileEntryBuilder.setStbSessionId(rowEl1);
        }

        String rowEl2 = row.get(2);
        if (StringUtils.isNotBlank(rowEl2)) {
            Integer sourceTmsId = Integer.valueOf(rowEl2);
            csvFileEntryBuilder.setSourceTmsId(sourceTmsId);
        }

        String rowEl3 = row.get(3);
        if (StringUtils.isNotBlank(rowEl3)) {
            Integer destinationTmsId = Integer.valueOf(rowEl3);
            csvFileEntryBuilder.setDestinationTmsId(destinationTmsId);
        }

        String rowEl4 = row.get(4);
        if (StringUtils.isNotBlank(rowEl4)) {
            LocalDateTime eventStartTime = LocalDateTime.parse(rowEl4);
            csvFileEntryBuilder.setEventStartTime(eventStartTime);
        }

        String rowEl5 = row.get(5);
        if (StringUtils.isNotBlank(rowEl5)) {
            TuningEventReason reason = TuningEventReason.of(rowEl5);
            csvFileEntryBuilder.setReason(reason);
        }

        String rowEl6 = row.get(6);
        if (StringUtils.isNotBlank(rowEl6)) {
            Integer signalLevel = Integer.valueOf(rowEl6);
            csvFileEntryBuilder.setSignalLevel(signalLevel);
        }

        String rowEl7 = row.get(7);
        if (StringUtils.isNotBlank(rowEl7)) {
            Integer sequenceNumber = Integer.valueOf(rowEl7);
            csvFileEntryBuilder.setSequenceNumber(sequenceNumber);
        }

        return csvFileEntryBuilder.build();
    }
}
