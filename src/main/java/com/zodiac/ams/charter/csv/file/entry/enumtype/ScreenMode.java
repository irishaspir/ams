package com.zodiac.ams.charter.csv.file.entry.enumtype;

public enum ScreenMode {
    FULL(1, "FULL"),
    SCALED(2, "SCALED"),
    UNKNWON(3, "");
    
    private final int intValue;
    private final String value;
    
    private ScreenMode(int intValue, String value){
        this.intValue = intValue;
        this.value = value;
    }
    
    public int getIntValue(){
        return intValue;
    }
    
    public String getValue(){
        return value;
    }
    
    public static ScreenMode of(String value){
        for (ScreenMode item: ScreenMode.values()){
            if (item.value.equals(value))
                return item;
        }
        return null;        
    }
}
