package com.zodiac.ams.charter.csv.file.entry;

import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventReason;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventScreenMode;
import com.zodiac.ams.charter.csv.file.entry.enumtype.TsbEventType;

import java.time.LocalDateTime;

/**
 * The TSB CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class TsbCsvFileEntry {
    private String macId;
    private String stbSessionId;
    private Integer sequenceNumber;
    private Integer tmsId;
    private LocalDateTime eventTime;
    private TsbEventType eventType;
    private TsbEventReason reason;
    private TsbEventScreenMode screenMode;

    /*
     * The private constructor.
     */
    private TsbCsvFileEntry() {
        // do nothing here
    }

    public String getMacId() {
        return macId;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public Integer getTmsId() {
        return tmsId;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public TsbEventType getEventType() {
        return eventType;
    }

    public TsbEventReason getReason() {
        return reason;
    }

    public TsbEventScreenMode getScreenMode() {
        return screenMode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TsbCsvFileEntry that = (TsbCsvFileEntry) obj;

        if (macId != null ? !macId.equals(that.macId) : that.macId != null) {
            return false;
        }
        if (stbSessionId != null ? !stbSessionId.equals(that.stbSessionId) : that.stbSessionId != null) {
            return false;
        }
        if (sequenceNumber != null ? !sequenceNumber.equals(that.sequenceNumber) : that.sequenceNumber != null) {
            return false;
        }
        if (tmsId != null ? !tmsId.equals(that.tmsId) : that.tmsId != null) {
            return false;
        }
        if (eventTime != null ? !eventTime.equals(that.eventTime) : that.eventTime != null) {
            return false;
        }
        if (eventType != that.eventType) {
            return false;
        }
        if (reason != that.reason) {
            return false;
        }
        return screenMode == that.screenMode;
    }

    @Override
    public int hashCode() {
        int result = macId != null ? macId.hashCode() : 0;
        result = 31 * result + (stbSessionId != null ? stbSessionId.hashCode() : 0);
        result = 31 * result + (sequenceNumber != null ? sequenceNumber.hashCode() : 0);
        result = 31 * result + (tmsId != null ? tmsId.hashCode() : 0);
        result = 31 * result + (eventTime != null ? eventTime.hashCode() : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (screenMode != null ? screenMode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TsbCsvFileEntry{" +
                "macId='" + macId + '\'' +
                ", stbSessionId='" + stbSessionId + '\'' +
                ", sequenceNumber=" + sequenceNumber +
                ", tmsId=" + tmsId +
                ", eventTime=" + eventTime +
                ", eventType=" + eventType +
                ", reason=" + reason +
                ", screenMode=" + screenMode +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String macId;
        private String stbSessionId;
        private Integer sequenceNumber;
        private Integer tmsId;
        private LocalDateTime eventTime;
        private TsbEventType eventType;
        private TsbEventReason reason;
        private TsbEventScreenMode screenMode;

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setTmsId(Integer tmsId) {
            this.tmsId = tmsId;
            return this;
        }

        public Builder setEventTime(LocalDateTime eventTime) {
            this.eventTime = eventTime;
            return this;
        }

        public Builder setEventType(TsbEventType eventType) {
            this.eventType = eventType;
            return this;
        }

        public Builder setReason(TsbEventReason reason) {
            this.reason = reason;
            return this;
        }

        public Builder setScreenMode(TsbEventScreenMode screenMode) {
            this.screenMode = screenMode;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public TsbCsvFileEntry build() {
            TsbCsvFileEntry target = new TsbCsvFileEntry();
            target.macId = macId;
            target.stbSessionId = stbSessionId;
            target.sequenceNumber = sequenceNumber;
            target.tmsId = tmsId;
            target.eventTime = eventTime;
            target.eventType = eventType;
            target.reason = reason;
            target.screenMode = screenMode;

            return target;
        }
    }
}
