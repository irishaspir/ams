package com.zodiac.ams.charter.csv.file.entry;

import com.zodiac.ams.charter.csv.file.entry.enumtype.TuningEventReason;

import java.time.LocalDateTime;

/**
 * The tuning CSV file entry.
 *
 * @author <a href="mailto:gennady.abakyan@gmail.com">Gennady Abakyan</a>
 */
public class TuningCsvFileEntry {
    private String macId;
    private String stbSessionId;
    private Integer sourceTmsId;
    private Integer destinationTmsId;
    private LocalDateTime eventStartTime;
    private TuningEventReason reason;
    private Integer signalLevel;
    private Integer sequenceNumber;

    /*
     * The private constructor.
     */
    private TuningCsvFileEntry() {
        // do nothing here
    }

    public String getMacId() {
        return macId;
    }

    public String getStbSessionId() {
        return stbSessionId;
    }

    public Integer getSourceTmsId() {
        return sourceTmsId;
    }

    public Integer getDestinationTmsId() {
        return destinationTmsId;
    }

    public LocalDateTime getEventStartTime() {
        return eventStartTime;
    }

    public TuningEventReason getReason() {
        return reason;
    }

    public Integer getSignalLevel() {
        return signalLevel;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TuningCsvFileEntry that = (TuningCsvFileEntry) obj;

        if (macId != null ? !macId.equals(that.macId) : that.macId != null) {
            return false;
        }
        if (stbSessionId != null ? !stbSessionId.equals(that.stbSessionId) : that.stbSessionId != null) {
            return false;
        }
        if (sourceTmsId != null ? !sourceTmsId.equals(that.sourceTmsId) : that.sourceTmsId != null) {
            return false;
        }
        if (destinationTmsId != null ? !destinationTmsId.equals(that.destinationTmsId) : that.destinationTmsId != null) {
            return false;
        }
        if (eventStartTime != null ? !eventStartTime.equals(that.eventStartTime) : that.eventStartTime != null) {
            return false;
        }
        if (reason != that.reason) {
            return false;
        }
        if (signalLevel != null ? !signalLevel.equals(that.signalLevel) : that.signalLevel != null) {
            return false;
        }
        return sequenceNumber != null ? sequenceNumber.equals(that.sequenceNumber) : that.sequenceNumber == null;
    }

    @Override
    public int hashCode() {
        int result = macId != null ? macId.hashCode() : 0;
        result = 31 * result + (stbSessionId != null ? stbSessionId.hashCode() : 0);
        result = 31 * result + (sourceTmsId != null ? sourceTmsId.hashCode() : 0);
        result = 31 * result + (destinationTmsId != null ? destinationTmsId.hashCode() : 0);
        result = 31 * result + (eventStartTime != null ? eventStartTime.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (signalLevel != null ? signalLevel.hashCode() : 0);
        result = 31 * result + (sequenceNumber != null ? sequenceNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TuningCsvFileEntry{" +
                "macId='" + macId + '\'' +
                ", stbSessionId='" + stbSessionId + '\'' +
                ", sourceTmsId=" + sourceTmsId +
                ", destinationTmsId=" + destinationTmsId +
                ", eventStartTime=" + eventStartTime +
                ", reason=" + reason +
                ", signalLevel=" + signalLevel +
                ", sequenceNumber=" + sequenceNumber +
                '}';
    }

    /**
     * The nested builder class.
     */
    public static class Builder {
        private String macId;
        private String stbSessionId;
        private Integer sourceTmsId;
        private Integer destinationTmsId;
        private LocalDateTime eventStartTime;
        private TuningEventReason reason;
        private Integer signalLevel;
        private Integer sequenceNumber;

        public Builder setMacId(String macId) {
            this.macId = macId;
            return this;
        }

        public Builder setStbSessionId(String stbSessionId) {
            this.stbSessionId = stbSessionId;
            return this;
        }

        public Builder setSourceTmsId(Integer sourceTmsId) {
            this.sourceTmsId = sourceTmsId;
            return this;
        }

        public Builder setDestinationTmsId(Integer destinationTmsId) {
            this.destinationTmsId = destinationTmsId;
            return this;
        }

        public Builder setEventStartTime(LocalDateTime eventStartTime) {
            this.eventStartTime = eventStartTime;
            return this;
        }

        public Builder setReason(TuningEventReason reason) {
            this.reason = reason;
            return this;
        }

        public Builder setSignalLevel(Integer signalLevel) {
            this.signalLevel = signalLevel;
            return this;
        }

        public Builder setSequenceNumber(Integer sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        /**
         * The method to build the target.
         *
         * @return the built target
         */
        public TuningCsvFileEntry build() {
            TuningCsvFileEntry target = new TuningCsvFileEntry();
            target.macId = macId;
            target.stbSessionId = stbSessionId;
            target.sourceTmsId = sourceTmsId;
            target.destinationTmsId = destinationTmsId;
            target.eventStartTime = eventStartTime;
            target.reason = reason;
            target.signalLevel = signalLevel;
            target.sequenceNumber = sequenceNumber;

            return target;
        }
    }
}
