package com.zodiac.ams.charter.tcp;

import com.zodiac.ams.common.common.Config;

import java.net.Socket;

import static java.lang.Thread.sleep;

/**
 * Created by SpiridonovaIM on 29.08.2016.
 */
public class TCPClient {

    private String host = new Config().getAmsIp();
    private int port = 7900;
    private Socket socket = null;
    private byte[] msg;

    public TCPClient(byte[] msg) {
        this.msg = msg;
    }

    public void send() {
        try {
            socket = new Socket(host, port);
            socket.getOutputStream().write(msg);

            sleep(2000);
            socket.close();
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

}
