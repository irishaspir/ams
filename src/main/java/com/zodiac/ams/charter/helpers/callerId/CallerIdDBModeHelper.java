package com.zodiac.ams.charter.helpers.callerId;

public class CallerIdDBModeHelper {

    private String SUBSCRIBE = "SUBSCRIBE\n";
    private String UNSUBSCRIBE = "UNSUBSCRIBE\n";

    public String createCharterSubscriptions(String ... ids){
        StringBuilder charterSubscriptions = new StringBuilder();
        for (String id : ids)
            charterSubscriptions.append(id + "|" + SUBSCRIBE);
        return charterSubscriptions.toString();
    }

    public String createCharterUnsubscriptions(String ... ids){
        StringBuilder charterUnsubscriptions = new StringBuilder();
        for (String id : ids)
            charterUnsubscriptions.append(id + "|" + UNSUBSCRIBE);
        return charterUnsubscriptions.toString();
    }
}
