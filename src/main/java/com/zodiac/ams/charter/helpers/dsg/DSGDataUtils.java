package com.zodiac.ams.charter.helpers.dsg;

import com.zodiac.ams.charter.bd.ams.tables.*;
import com.zodiac.ams.charter.services.stb.BoxModels;
import com.zodiac.ams.charter.services.stb.STBMetadataOptions;
import com.zodiac.ams.charter.services.stb.Vendors;

import static com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils.getModeChangeTimeStamp;
import static com.zodiac.ams.charter.bd.ams.tables.STBModelsUtils.getDataFromStbModels;
import static com.zodiac.ams.charter.bd.ams.tables.STBVendorsUtils.getAllDataFromStbVendors;
import static com.zodiac.ams.charter.helpers.DataUtils.converterUnixToDate;

public class DSGDataUtils {

    public static String converterUnixToString(long date) {
        return converterUnixToDate(date).toString();
    }

    public static long convertUDateToLong(String data) {
        return Long.parseLong(data.substring(0, data.length() - 3));
    }

    public static boolean isDataNull(String data) {
        return data.equals("null");
    }

    public static String getModeChangeTimeStampInDateFormat(String mac) {
        return converterUnixToString(getModeChangeTimeStamp(mac));
    }

    public static int getRomIdFromSTBMetadata(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getRomId();
    }

    public static int getStbModelIdFromSTBMetadata(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getStbModelId();
    }

    public static int getVendorIdFromSTBMetadata(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getVendorId();
    }

    public static int getHubIdFromSTBMetadata(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getHubId();
    }

    public static int getServiceGroupIdFromSTBMetadata(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getGroupCDL();
    }

    public static int getVendorId(String name) {
        Vendors vendors = new Vendors(getAllDataFromStbVendors());
        return vendors.getVendorId(name.trim());
    }

    public static String getVendorName(int id) {
        Vendors vendors = new Vendors(getAllDataFromStbVendors());
        return vendors.getVendorName(id);
    }

    public static int getModelId(String name) {
        BoxModels boxModels = new BoxModels(getDataFromStbModels());
        return boxModels.getBoxModelId(name.trim());
    }
    public static long getMsgTypesMap(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getMsgTypesMap();
    }

    public static long getStbMetadataFlags(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getStbMetadataFlags();
    }

    public static boolean isMacInMacIp(String mac) {
        return new MacIpUtils().isMacInMacIp(mac);
    }

}
