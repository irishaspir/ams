package com.zodiac.ams.charter.helpers.ft;

import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHosts;
import static com.zodiac.ams.charter.helpers.ft.FTConfig.getFileFromJar;
import com.zodiac.ams.charter.services.ft.publisher.Constants;
import static com.zodiac.ams.charter.services.ft.publisher.Constants.*;
import com.zodiac.ams.charter.services.ft.publisher.Version;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHost;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsCDN;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsDNCS;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsHTTP;
import com.zodiac.ams.charter.services.ft.publisher.host.CarouselHostsST;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerXMLHelper {
    
    public static final int TALKING_GUIDE_MASK = 0b0001;
    public static final int PUBLISHER_MASK = 0b0010;
    
    private final static Logger logger = LoggerFactory.getLogger(ServerXMLHelper.class);
    
    private Document document;
    private Element root;
    private FTConfig properties;
    private Map<String,String> params;
    
    private final static String RWATCHED_ID = "R";
    private final static String RWATCHED_SERVICE = "com.dob.charter.recently_watched.RecWatchedServiceImpl";
    
    private final static String REMIND_ID = "remind";
    private final static String REMIND_SERVICE = "com.dob.charter.reminders.RemindersServiceImpl";
    
    private final static String EPG_ID = "charterEPG";
    private final static String EPG_SERVICE = "com.dob.charter.epg.CharterEpgServiceImpl";
    
    private final static String RUDP_TESTING_ID = "RUDP_TESTING";
    private final static String RUDP_TESTING_SERVICE = "com.dob.charter.rudp_testing.RUDPTestingServiceImpl";
    
    private final static String TEMPLATE_DIR  = "dataFT";
    private final static String TEMPLATE_NAME = "server.xml.template";
    
    private CarouselHostsHTTP hostsHTTP;
    private CarouselHostsST hostsST;
    private CarouselHostsDNCS hostDNCS;
    private CarouselHostsCDN hostCDN;
    
    private void init (FTConfig properties,int mask){
        try {
            this.properties = properties;
            String template=getFileFromJar(TEMPLATE_DIR,TEMPLATE_NAME);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(template);
            root = document.getDocumentElement();
            if (0 != (mask & TALKING_GUIDE_MASK))
                addTalkingGuideService(properties.getLocalHost(),properties.getTTSPort(),"xxx","xxx");
            addDvrService();
            Version version = this.properties.getPublihserVersion();
            switch(version){
                case VERSION_1:
                    addPublisherInitializer();
                    addPublisherService1(properties);
                    break;
                case VERSION_2:
                    addPublisherInitializer();
                    addPublisherService2(properties);
                    break;
                default:
                    addPublisherService3(properties);
                    break;
            }
        } catch (Exception ex) {
            logger.error("XMLHelper() exception: {}", ex.getMessage());
        }
    }
    
    
    
    public ServerXMLHelper(FTConfig properties,int mask){
        init(properties,mask);
    }
    
    public ServerXMLHelper(FTConfig properties){
        init(properties,PUBLISHER_MASK|TALKING_GUIDE_MASK);
    }
    
    public ServerXMLHelper(FTConfig properties, Map<String,String> params){
        this.params = params;
        init(properties,PUBLISHER_MASK|TALKING_GUIDE_MASK);
    }
    
    private void initWithCarouselHosts(FTConfig properties, Map<String,String> params, CarouselHostsHTTP hostsHTTP, CarouselHostsST hostsST,
            CarouselHostsDNCS hostsDNCS, CarouselHostsCDN hostsCDN){
        this.params = params;
        this.hostsHTTP = hostsHTTP;
        this.hostsST = hostsST;
        this.hostDNCS = hostsDNCS;
        this.hostCDN = hostsCDN;
        init(properties,PUBLISHER_MASK|TALKING_GUIDE_MASK);
    }
    
    public ServerXMLHelper(FTConfig properties, Map<String,String> params, CarouselHostsHTTP hostsHTTP, CarouselHostsST hostsST,
            CarouselHostsDNCS hostsDNCS, CarouselHostsCDN hostsCDN){
        initWithCarouselHosts(properties, params, hostsHTTP, hostsST, hostsDNCS, hostsCDN);
    }
    
    public ServerXMLHelper(FTConfig properties, Map<String,String> params, CarouselHostsHTTP hostsHTTP){
        initWithCarouselHosts(properties, params, hostsHTTP, null, null, null);
    }
    
    public ServerXMLHelper(FTConfig properties, Map<String,String> params, CarouselHostsST hostsST){
        initWithCarouselHosts(properties, params, null, hostsST, null, null);
    }
    
    public ServerXMLHelper(FTConfig properties, Map<String,String> params, CarouselHostsDNCS hostsDNCS){
        initWithCarouselHosts(properties, params, null, null, hostsDNCS, null);
    }
    
    public ServerXMLHelper(FTConfig properties, Map<String,String> params, CarouselHostsCDN hostsCDN){
        initWithCarouselHosts(properties, params, null, null, null, hostsCDN);
    }
    
    
    
    public void save(String name) {
        try {
            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "1");
            StreamResult result = new StreamResult(name);
            transformer.transform(source, result);
        } catch (Exception ex) {
            logger.error("XMLHelper save exception: {}", ex.getMessage());
        }
    }
        
   
    /*
        <Service service_id="R"
            service="com.dob.charter.recently_watched.RecWatchedServiceImpl"
            com.dob.charter.datasource-name="java:/comp/env/jdbc/DEF_DS"
            com.dob.charter.headend-url="http://192.168.17.137:8020/rwatched"
        />
    */
    public void addRWatchedService(String uri){
        Element service = document.createElement("Service");
        service.setAttribute("service_id",RWATCHED_ID);
        service.setAttribute("service", RWATCHED_SERVICE);
        service.setAttribute("com.dob.charter.datasource-name","java:/comp/env/jdbc/DEF_DS");
        service.setAttribute("com.dob.charter.headend-url",uri);
        root.appendChild(service);   
    }
    
    public void addRUDPTestingService(String uri){
        Element service = document.createElement("Service");
        service.setAttribute("service_id",RUDP_TESTING_ID);
        service.setAttribute("service", RUDP_TESTING_SERVICE);
        service.setAttribute("com.dob.charter.datasource-name","java:/comp/env/jdbc/DEF_DS");
        service.setAttribute("com.dob.charter.headend-url",uri);
        root.appendChild(service);   
    }
    
    
    public void addPPVMotorolaService(String uri){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "ppvMotorola");
        service.setAttribute("service", "com.dob.charter.ppv.CharterPpvServiceMotorolaImpl");
        service.setAttribute("sending-transport-id","778");
        service.setAttribute("com.dob.charter.datasource-name","java:/comp/env/jdbc/DEF_DS");
//        service.setAttribute("com.dob.charter.data-request-url","http://che.dev-charter.net/STLMOTOPPVMetadata");
        service.setAttribute("com.dob.charter.data-request-url",uri);
        //service.setAttribute("com.dob.charter.distributor.task-list","ib,oob");
        service.setAttribute("com.dob.charter.distributor.task-list","ib");
        service.setAttribute("com.dob.charter.distributor.carousel-type.ib","IB");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.ib","ppv/ib/");
        //service.setAttribute("com.dob.charter.distributor.carousel-default-name.ib","prefetch");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.ib","ppv");
        service.setAttribute("com.dob.charter.distributor.file-list.ib","ppv.*.zdb");
        service.setAttribute("com.dob.charter.distributor.carousel-type.oob","OOB");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.oob","ppv/oob/");
        //service.setAttribute("com.dob.charter.distributor.carousel-default-name.oob","assets-oob4");
        service.setAttribute("com.dob.charter.distributor.file-list.oob","ppv.*.zdb");
        service.setAttribute("com.dob.charter.error-repeat-interval","1");
        service.setAttribute("com.dob.charter.error-repeat-count","2");
        service.setAttribute("com.dob.charter.request-days","7");
        root.appendChild(service);   
    }
    
    public void addPPVCiscoService(String uri){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "ppvCisco");
        service.setAttribute("service", "com.dob.charter.ppv.CharterPpvServiceCiscoImpl");
        service.setAttribute("sending-transport-id", "778");
        service.setAttribute("com.dob.charter.datasource-name", "java:/comp/env/jdbc/DEF_DS");
        //service.setAttribute("com.dob.charter.data-request-url", "http://che.dev-charter.net/PPVMetadata");
        service.setAttribute("com.dob.charter.data-request-url", uri);
        //service.setAttribute("com.dob.charter.distributor.task-list", "ib,oob");
        service.setAttribute("com.dob.charter.distributor.task-list", "ib");
        service.setAttribute("com.dob.charter.distributor.carousel-type.ib", "IB");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.ib", "ppv/ib/");
        //service.setAttribute("com.dob.charter.distributor.carousel-default-name.ib", "prefetch");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.ib", "ppv");
        service.setAttribute("com.dob.charter.distributor.file-list.ib", "ppv.*.zdb");
        service.setAttribute("com.dob.charter.distributor.carousel-type.oob", "OOB");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.oob", "ppv/oob/");
        //service.setAttribute("com.dob.charter.distributor.carousel-default-name.oob", "assets-oob4");
        service.setAttribute("com.dob.charter.distributor.file-list.oob", "ppv.*.zdb");
        service.setAttribute("com.dob.charter.error-repeat-interval", "1");
        service.setAttribute("com.dob.charter.error-repeat-count", "2");
        service.setAttribute("com.dob.charter.request-days","2");
        root.appendChild(service);   
    }
    
    public void addErrorReportingService(String uri){
        Element service = document.createElement("Service");
        //service.setAttribute("service_id", "errorReporting");
        service.setAttribute("service_id", "E");
	service.setAttribute("service", "com.dob.charter.errorreporting.ErrorReportingServiceImpl");
	service.setAttribute("com.dob.charter.errorreporting.errormiddle_url", uri);
	service.setAttribute("com.dob.charter.errorreporting.thread_count", "1000");
	service.setAttribute("com.dob.charter.errorreporting.queue_size", "2000");
        root.appendChild(service);   
    }
    
    public void addDefEntService(String uri){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "DENT");
        service.setAttribute("service", "com.dob.charter.def_ent.DefEntServiceImpl");
        service.setAttribute("com.dob.charter.data-request-url", uri);
	service.setAttribute("stb-service-id", "DENT");
	service.setAttribute("com.dob.charter.delivery-timeout-sec", "6");
	service.setAttribute("com.dob.charter.error-repeat-interval", "1");
	service.setAttribute("com.dob.charter.error-repeat-count", "1");
        service.setAttribute("com.dob.charter.connection-timeout", "30"); //60
        root.appendChild(service);   
    }
    
    public void addDSGService(String heUri, String reminderUri){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "dsg");
        service.setAttribute("service", "com.dob.charter.dsg.service.DsgServiceImpl");
        service.setAttribute("com.dob.charter.datasource-name", "java:/comp/env/jdbc/DEF_DS");
	service.setAttribute("com.dob.charter.headend-url", heUri);
	service.setAttribute("com.dob.charter.reminder-notification-url", reminderUri);
	service.setAttribute("com.dob.charter.error-repeat-count", "1");
	service.setAttribute("com.dob.charter.error-repeat-interval", "1");
	service.setAttribute("com.dob.pool-threads-count", "1");
        root.appendChild(service);
    }
    
    /*
        <Service service_id="remind" 
            service="com.dob.charter.reminders.RemindersServiceImpl" />
    */
    public void addReminderService(){
        Element service = document.createElement("Service");
        service.setAttribute("service_id",REMIND_ID);
        service.setAttribute("service", REMIND_SERVICE);
        int maxReminders = properties.getMaxReminders();
        if (0 != maxReminders)
            service.setAttribute(FTConfig.MAX_REMINDERS_PROP, String.valueOf(maxReminders));
        root.appendChild(service);   
    }

    public void addEPGService(String uri,String controller){
        Element service = document.createElement("Service");
        service.setAttribute("service_id",EPG_ID);
        service.setAttribute("service", EPG_SERVICE);
        if (null != controller &&  !controller.trim().equals(""))
            service.setAttribute("com.dob.charter.controller-name",controller);
        service.setAttribute("com.dob.charter.datasource-name","java:/comp/env/jdbc/DEF_DS");
        service.setAttribute("com.dob.charter.data-request-url",uri);
        service.setAttribute("com.dob.charter.epg.request-days","10");    
        
        
/*        
        service.setAttribute("com.dob.charter.distributor.task-list","ib_moto,oob_moto");

        service.setAttribute("com.dob.charter.distributor.carousel-type.oob_moto","OOB");
        service.setAttribute("com.dob.charter.epg.hours-per-segment.oob_moto","24");
        service.setAttribute("com.dob.charter.distributor.carousel-box-type.oob_moto","MOTOROLA");
        service.setAttribute("com.dob.charter.distributor.publisher-server-type.oob_moto","ST");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.oob_moto","assets-oob4");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.oob_moto","ipg/oob/");
        service.setAttribute("com.dob.charter.distributor.file-list.oob_moto",".*");

        service.setAttribute("com.dob.charter.distributor.carousel-type.ib_moto","IB");
        service.setAttribute("com.dob.charter.epg.hours-per-segment.ib_moto","24");
        service.setAttribute("com.dob.charter.distributor.carousel-box-type.ib_moto","MOTOROLA");
        service.setAttribute("com.dob.charter.distributor.publisher-server-type.ib_moto","ST");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.ib_moto","assets-ib3");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.ib_moto","ipg/ib/");
        service.setAttribute("com.dob.charter.distributor.file-list.ib_moto",".*");

        service.setAttribute("com.dob.charter.distributor.carousel-type.moto2k","IB");
        service.setAttribute("com.dob.charter.epg.hours-per-segment.moto2k","24");
        service.setAttribute("com.dob.charter.distributor.carousel-box-type.moto2k","MOTOROLA");
        service.setAttribute("com.dob.charter.distributor.publisher-server-type.moto2k","ST_NATIVE");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.moto2k","assets-ib1");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.moto2k","ipg/ib/");
        service.setAttribute("com.dob.charter.distributor.file-list.moto2k",".*");
        service.setAttribute("com.dob.charter.zdb-encoder-class.moto2k","com.dob.charter.epg.encoders.EpgZdbEncoderM2000");

        service.setAttribute("com.dob.charter.distributor.carousel-type.moto2k_oob","OOB");
        service.setAttribute("com.dob.charter.epg.hours-per-segment.moto2k_oob","24");
        service.setAttribute("com.dob.charter.distributor.carousel-box-type.moto2k_oob","MOTOROLA");
        service.setAttribute("com.dob.charter.distributor.publisher-server-type.moto2k_oob","ST_NATIVE");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.moto2k_oob","assets-oob5");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.moto2k_oob","ipg/oob/");
        service.setAttribute("com.dob.charter.distributor.file-list.moto2k_oob",".*");
        service.setAttribute("com.dob.charter.zdb-encoder-class.moto2k_oob","com.dob.charter.epg.encoders.EpgZdbEncoderM2000");

        service.setAttribute("com.dob.charter.distributor.carousel-type.wb_ib","IB");
        service.setAttribute("com.dob.charter.distributor.carousel-box-type.wb_ib","WORLDBOX");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.wb_ib","ipg/ib/");
        service.setAttribute("com.dob.charter.distributor.publisher-server-type.wb_ib","ST_OCAP");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.wb_ib","DVBS");
        service.setAttribute("com.dob.charter.epg.hours-per-segment.wb_ib","24");
        
        service.setAttribute("com.dob.charter.distributor.carousel-type.wb_oob","OOB");
        service.setAttribute("com.dob.charter.distributor.carousel-box-type.wb_oob","WORLDBOX");
        service.setAttribute("com.dob.charter.distributor.zdts-name-prefix.wb_oob","ipg/oob/");
        service.setAttribute("com.dob.charter.distributor.publisher-server-type.wb_oob","ST_OCAP");
        service.setAttribute("com.dob.charter.distributor.carousel-default-name.wb_oob","OOBApp");
        service.setAttribute("com.dob.charter.epg.hours-per-segment.wb_oob","24");

        service.setAttribute("com.dob.charter.delete.temp.files","true");
        service.setAttribute("com.dob.charter.file-distributor-target-dir","/srv/tomcat7/temp/EpgFileDistributed");
        service.setAttribute("com.dob.charter.distributor-class","com.dob.charter.epg.distributors.EpgZdbDistributorFile");
        service.setAttribute("com.dob.charter.zdts-file-location","/srv/tomcat7/temp");
  */           
        root.appendChild(service);   
    }
    
    
    public void addTalkingGuideService(String host,String port,String expressionsFileLocation,String cacheRoot){
        Element service = document.createElement("ConfigurationUnit");
              
        service.setAttribute("id","talkingGuide");
        service.setAttribute("nuance.appId","NMDPTRIAL_alexander_ivanov_dev_zodiac_tv20151213121321");
        service.setAttribute("nuance.appKey","ed97e0a62f1fc8474c5f0b0488df5f20cbb2ec4bc7ca62f41a6498488d228e7ab96e21e88ee92fda483bded01a3df1e5efdd30b6a792aac1f64209836598fd1e");
        service.setAttribute("nuance.host","tts.nuancemobility.net");
        service.setAttribute("nuance.port","443");
        service.setAttribute("nuance.ttsServlet","/NMDPTTSCmdServlet/tts");
        service.setAttribute("charterProxy.host",host);
        //service.setAttribute("charterProxy.host","spec.partnerapi.engprod-charter.net");
        service.setAttribute("charterProxy.port",port);
        //service.setAttribute("charterProxy.port","443");
        service.setAttribute("charterProxy.ttsServlet","/api/pub/ttsmiddle/api/service/tts");
        service.setAttribute("ttsClientConnectionTimeoutMillis","30000");
        service.setAttribute("expressionsFileLocation",expressionsFileLocation);
        //service.setAttribute("expressionsFileLocation","/path/to/properties/file");
        service.setAttribute("cacheRoot",cacheRoot);
        //service.setAttribute("cacheRoot","/path/to/cache/root");
        service.setAttribute("cacheExpireTimeMinutes","");
        service.setAttribute("cacheDisabled","false");
        //service.setAttribute("cacheDisabled","true");
        
        
        root.appendChild(service); 
    }
    
    public void addDCService(String outDir, String host, String login, String password, String remoteDir){
        Element rule1 = document.createElement("MessageRule");
        rule1.setAttribute("message_type", "1");
        rule1.setAttribute("service_id", "am");
        rule1.setAttribute("adapter", "com.dob.charter.dc.adapter.DCAdapter");
        root.appendChild(rule1);
        
        Element rule2 = document.createElement("MessageRule");
        rule2.setAttribute("message_type", "2");
        rule2.setAttribute("service_id", "am");
        rule2.setAttribute("adapter", "com.dob.charter.dc.adapter.DCAdapter2");
        root.appendChild(rule2);
        
        Element rule4 = document.createElement("MessageRule");
        rule4.setAttribute("message_type", "4");
        rule4.setAttribute("service_id", "am");
        rule4.setAttribute("adapter", "com.dob.charter.dc.adapter.DCAdapter4");
        root.appendChild(rule4);
        
        if (FTConfig.getInstance().getDCTunerUse()){
            Element rule5 = document.createElement("MessageRule");
            rule5.setAttribute("message_type", "5");
            rule5.setAttribute("service_id", "am");
            rule5.setAttribute("adapter", "com.dob.charter.dc.adapter.DCAdapter5");
            root.appendChild(rule5);
        }
        
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "am");
        service.setAttribute("service", "com.dob.charter.dc.DCService");
        service.setAttribute("transport-id", "778");
        service.setAttribute("charter.dc.outdir", outDir);
        service.setAttribute("charter.dc.upload.timeout", "0.001"); // "0.017"); //~5sec
        service.setAttribute("charter.dc.delete.timeout", "7");
        service.setAttribute("charter.dc.events.bufferLimit", "50000");
        service.setAttribute("charter.dc.sftp", host);
        service.setAttribute("charter.dc.sftp.login", login);
        service.setAttribute("charter.dc.sftp.password", password);
        service.setAttribute("charter.dc.sftp.remoteDir", remoteDir);
        root.appendChild(service);
    }
    
    
    public void addCleanupService(){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "rollback");
        service.setAttribute("service", "com.dob.charter.rollback.RollbackServiceImpl");
        service.setAttribute("com.dob.charter.datasource-name", "java:/comp/env/jdbc/DEF_DS");
        root.appendChild(service);
    }
    
                     
    private String getHostsProp(List<CarouselHosts> list,String name){
        StringBuilder sb=new StringBuilder();
        for (CarouselHosts hosts: list)
            sb.append(hosts.getProperty(name)).append(";");
        return sb.toString();
    } 
      
    

    public void addPublisherService1(FTConfig props){
//        List<CarouselHosts> hosts=new ArrayList<>();
        
//        CarouselHostsHTTP hostsHTTP = this.hostsHTTP;
//        if (null == hostsHTTP)
//            hostsHTTP = new CarouselHostsHTTP(props, false/*useZippedZdtsIni*/, false/*useAlternativeLocations*/, false/*useDalincMode*/);
//        hosts.add(hostsHTTP);
        
        CarouselHostsST hostsST = this.hostsST;
        if (null == hostsST)
            hostsST = new CarouselHostsST(props, false, false, false);
//        hosts.add(hostsST);
        
        CarouselHostsDNCS hostsDNCS  = this.hostDNCS;
        if (null == hostsDNCS)
            hostsDNCS = new CarouselHostsDNCS(props, false, false, false);
//        hosts.add(hostsDNCS);
        
//        CarouselHostsCDN hostsCDN = this.hostCDN;
//        if (null == hostsCDN)
//            hostsCDN = new CarouselHostsCDN(props, false, false, false);
//        hosts.add(hostsCDN);
        
        Element service = document.createElement("ConfigurationUnit");
        service.setAttribute("id","charterPublisher");
        
        CarouselHost dncsHost = hostsDNCS.get(0);
        StringBuilder sb = new StringBuilder();
        sb.append(dncsHost.getUid()).
            append(":").
            append(dncsHost.getUser()).
            append(":").
            append(dncsHost.getPwd()).
            append("@").
            append(dncsHost.getHost()).
            append(dncsHost.getDir()).
            append("/;");
            //append("/:").append(dncsHost.getType()).append(";");
            
        //service.setAttribute("dncs.ftp.list","1:user:password@server1/export/home/dncs/apps/Zodiac/ptv/:DNCS;2:user:password@server2/export/home/dncs/apps/Zodiac/ptv/:DNCS;");
        service.setAttribute("dncs.ftp.list",sb.toString());
        service.setAttribute("dncs.storage","MEMORY");
        //service.setAttribute("dncs.zdts.path","assets-oob1");
        service.setAttribute("dncs.zdts.path", dncsHost.getLocation());
        service.setAttribute("dncs.storage", dncsHost.getType());
                     
        
        
        service.setAttribute("zc.srv.list","55:rest:rest@http://localhost:8080/zcarousel/api");
        service.setAttribute("zc.zdts.location","assets-oob1");
        service.setAttribute("zc.carousels","assets-oob1:oob:Prefetch?cid=1041&mn=;");
        
        CarouselHost stHost = hostsST.get(0);
        sb.setLength(0);
        sb.append(stHost.getUid()).
            append(":").
            append(stHost.getUser()).
            append(":").
            append(stHost.getPwd()).
            append("@").
            append(stHost.getHost());
        //service.setAttribute("st.srv.list","3:demo:demo@192.168.21.19:8080/tsbroadcaster");
        service.setAttribute("st.srv.list",sb.toString());
        //service.setAttribute("st.zdts.location","assets-oob1");
        service.setAttribute("st.zdts.location", stHost.getLocation());
        
        sb.setLength(0);
        sb.append(stHost.getAlias()).
            append(":").
            append(stHost.getLocation()).
            append(":").
            append(stHost.getType()).
            append(":oob:Prefetch?cid=").append(stHost.getUid()).append("&mn=").
            append(";");
        //service.setAttribute("st.carousels","prefetch:prefetch:ETV:oob:Prefetch?cid=1041&mn=;assets-ib1:assets-ib1:ETV:oob:Z_IB1?cid=1042&mn=;assets-ib2:assets-ib2:ETV:oob:Z_IB2?cid=1043&mn=;assets-ib3:assets-ib3:ETV:oob:Z_IB3?cid=1044&mn=;assets-oob1:assets-oob1:ETV:oob:Z_OOB1?cid=785&mn=;assets-oob2:assets-oob2:ETV:oob:Z_OOB2?cid=786&mn=;assets-oob3:assets-oob3:ETV:oob:Z_OOB3?cid=787&mn=;assets-oob4:assets-oob4:ETV:oob:Z_OOB4?cid=788&mn=;OCAPExplorer:OCAPExplorer:OCAP:oc:oobfdc/OCAPExplorer/;");
        service.setAttribute("st.carousels",sb.toString());
        //service.setAttribute("st.carousels.tsb.modules","OCAPExplorer:Module");
        service.setAttribute("st.carousels.tsb.modules","zodiac_ft_ocap:Module");
        service.setAttribute("st.presets","3:FREQUENCY:141000000:PID:0x1c02;");
        service.setAttribute("st.group.cfg","3:OOBApp:group.cfg");
        service.setAttribute("st.supervisor.cfg","3:OOBApp:supervisor.cfg");
        service.setAttribute("st.package.publisher.server.types","ST_OCAP");
        
        service.setAttribute("http.srv.list","5:ft:ft@192.168.21.28:SFTP");
        service.setAttribute("http.zdts.location","assetsoob1");
        service.setAttribute("http.carousels","prefetch:http:prefetch;assetsoob1:http:assetsoob1;");
        
        sb.setLength(0);
        sb.append(dncsHost.getUid()).append(":").append(dncsHost.getServerMode()).append(";");
        sb.append(stHost.getUid()).append(":").append(stHost.getServerMode()).append(";");
        //service.setAttribute("server.modes","1:DNCS;2:DALINC;3:DALINC;");
        service.setAttribute("server.modes",sb.toString());
                
        //service.setAttribute("server.types","1:DNCS;2:DNCS;3:ST,ST_NATIVE,ST_OCAP;4:ST_DATA;5:HTTP");
        service.setAttribute("server.types",sb.toString());
        
        sb.setLength(0);
        sb.append(dncsHost.getUid()).append(":").append("z_dts.ini").append(";");
        sb.append(stHost.getUid()).append(":").append(stHost.getLocation()).append("/z_dts.ini").append(";");
        //service.setAttribute("zdts.name","1:z_dts.ini;3:assets-oob1/z_dts.ini;3:assets-oob1/zdts2k.ini;3:OCAPExplorer/z_dts.ini");
        service.setAttribute("zdts.name",sb.toString());
        
        
        service.setAttribute("bfs.rpc.local.path","1:/export/home/dncs/apps/Zodiac/ptv/;");
        service.setAttribute("services.interval","10");
        service.setAttribute("max.upload.size","15000000");
        service.setAttribute("publishing.allowed.hosts","");
        service.setAttribute("debug.emulate.bfs","true");
        root.appendChild(service); 
    }
    
    public void addPublisherService2(FTConfig props){
        boolean useAlternativeLocations = null != params && Boolean.parseBoolean(params.get("useAlternativeLocations"));
        boolean useZippedZdtsIni = null != params && Boolean.parseBoolean(params.get("useZippedZdtsIni"));
        boolean useDalincMode = null != params && Boolean.parseBoolean(params.get("useDalincMode"));
        
        
        List<CarouselHosts> hosts=new ArrayList<>();
        
        CarouselHostsHTTP hostsHTTP = this.hostsHTTP;
        if (null == hostsHTTP)
            hostsHTTP = new CarouselHostsHTTP(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsHTTP);
        CarouselHostsST hostsST = this.hostsST;
        if (null == hostsST)
            hostsST = new CarouselHostsST(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsST);
        CarouselHostsDNCS hostsDNCS  = this.hostDNCS;
        if (null == hostsDNCS)
            hostsDNCS = new CarouselHostsDNCS(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsDNCS);
        CarouselHostsCDN hostsCDN = this.hostCDN;
        if (null == hostsCDN)
            hostsCDN = new CarouselHostsCDN(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsCDN);
        
        
        
        String alias = hostsHTTP.getAlias();
        //String location = hostsHTTP.getLocation();
        
        Element service = document.createElement("ConfigurationUnit");
        service.setAttribute("id","charterPublisher");
        
        //DNCS
        //service.setAttribute("dncs.ftp.list","1:user:password@server1/export/home/dncs/apps/Zodiac/ptv/:SFTPJSC;2:user:password@server2/export/home/dncs/apps/Zodiac/ptv/:SFTPJSC;");
        service.setAttribute("dncs.ftp.list", hostsDNCS.getProperty("dncs.ftp.list"));
        //not used ???
        //service.setAttribute("dncs.storage","MEMORY");
        //not used ???
        service.setAttribute("dncs.zdts.path","assets-oob1");
        //Carousels
        //dncs.carousels="assets-oob1:assets-oob1:1:bfs:assets-oob1"
        service.setAttribute("dncs.carousels",hostsDNCS.getProperty("dncs.carousels"));
        service.setAttribute("debug.emulate.bfs","true");
        
        
        //CDN
        service.setAttribute("cdn.srv.list", hostsCDN.getProperty("cdn.srv.list"));
        service.setAttribute("cdn.carousels",hostsCDN.getProperty("cdn.carousels"));
        
        
        service.setAttribute("zc.srv.list","555:rest:rest@http://localhost:8080/zcarousel/api");
        service.setAttribute("zc.zdts.location","assets-oob1");
        service.setAttribute("zc.carousels","assets-oob1:oob:Prefetch?cid=1041&mn=;");
        
        //ST 
        //st.srv.list="311:demo:demo@192.168.26.103:8080/tsbroadcaster"
        service.setAttribute("st.srv.list",hostsST.getProperty("st.srv.list"));
        //not used?
        service.setAttribute("st.zdts.location","assets-oob1");
        //Carousels
        //st.carousels="batch-alias:zodiac_ft_etv:311:ETV:oob:Prefetch?cid=311&mn=" 
        service.setAttribute("st.carousels",hostsST.getProperty("st.carousels"));
        //service.setAttribute("st.carousels.tsb.modules","OCAPExplorer:Module");
        service.setAttribute("st.carousels.tsb.modules","zodiac_ft_ocap:Module");
                
        
        service.setAttribute("st.presets","3:FREQUENCY:141000000:PID:0x1c02;");
        service.setAttribute("st.group.cfg","3:OOBApp:group.cfg");
        service.setAttribute("st.supervisor.cfg","3:OOBApp:supervisor.cfg");
        service.setAttribute("st.package.publisher.server.types","ST_OCAP");
        
        //SFTP servers
        //http.srv.list="51:ft:ft@192.168.21.28:SFTPJSC"
        service.setAttribute("http.srv.list",hostsHTTP.toString());
        //Location Alias: not used?
	//service.setAttribute("http.zdts.location","assets-oob1");
        service.setAttribute("http.zdts.location",alias);
        //carousels
        //http.carousels="batch-alias:assetsoob51:51:http:192.168.21.28:8080:/prefetch/51" 
        service.setAttribute("http.carousels",hostsHTTP.getProperty("http.carousels"));
        
        
        //service.setAttribute("server.modes","1:DNCS;2:DALINC;3:DALINC;5:HTTP");
        service.setAttribute("server.modes","2:DALINC;"+getHostsProp(hosts,MODES_PROP));
                
        //service.setAttribute("server.types","1:DNCS;2:DNCS;3:ST,ST_NATIVE,ST_OCAP;4:ST_DATA;5:HTTP");
        service.setAttribute("server.types","2:DNCS;4:ST_DATA;"+ getHostsProp(hosts,TYPES_PROP));
        
        //service.setAttribute("zdts.name","1:z_dts.ini;3:assets-oob1/z_dts.ini;3:assets-oob1/zdts2k.ini;3:OCAPExplorer/z_dts.ini");
        service.setAttribute("zdts.name", getHostsProp(hosts,ZDTS_PROP));
        
        
        service.setAttribute("bfs.rpc.local.path","1:/export/home/dncs/apps/Zodiac/ptv/;");
        service.setAttribute("services.interval","10");
        service.setAttribute("max.upload.size","15000000");
        service.setAttribute("publishing.allowed.hosts","");
        
        
        service.setAttribute("max.timestamped.file.age.minutes", String.valueOf(Constants.MAX_MINUTES));
        
        
        //Alternative locations
        if (useAlternativeLocations)
            service.setAttribute("root.zdts.name", getHostsProp(hosts,ROOT_ZDTS_PROP));
        
        root.appendChild(service); 
    }
    
    public void addPublisherInitializer(){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "charterPublisherInitializer");
        service.setAttribute("service", "com.dob.publisher.PublisherInitializerService");
        root.appendChild(service);
    }
                     
    public void addPublisherService3(FTConfig props){
        boolean useAlternativeLocations = null != params && Boolean.parseBoolean(params.get("useAlternativeLocations"));
        boolean useZippedZdtsIni = null != params && Boolean.parseBoolean(params.get("useZippedZdtsIni"));
        boolean useDalincMode = null != params && Boolean.parseBoolean(params.get("useDalincMode"));
        
        
        List<CarouselHosts> hosts=new ArrayList<>();
        
        CarouselHostsHTTP hostsHTTP = this.hostsHTTP;
        if (null == hostsHTTP)
            hostsHTTP = new CarouselHostsHTTP(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsHTTP);
        CarouselHostsST hostsST = this.hostsST;
        if (null == hostsST)
            hostsST = new CarouselHostsST(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsST);
        CarouselHostsDNCS hostsDNCS  = this.hostDNCS;
        if (null == hostsDNCS)
            hostsDNCS = new CarouselHostsDNCS(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsDNCS);
        CarouselHostsCDN hostsCDN = this.hostCDN;
        if (null == hostsCDN)
            hostsCDN = new CarouselHostsCDN(props, useZippedZdtsIni, useAlternativeLocations, useDalincMode);
        hosts.add(hostsCDN);
        
        
        
        String alias = hostsHTTP.getAlias();
        //String location = hostsHTTP.getLocation();
        
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "charterPublisher");
        service.setAttribute("service", "com.dob.publisher.PublisherInitializerService");
        
        //DNCS
        //service.setAttribute("dncs.ftp.list","1:user:password@server1/export/home/dncs/apps/Zodiac/ptv/:SFTPJSC;2:user:password@server2/export/home/dncs/apps/Zodiac/ptv/:SFTPJSC;");
        service.setAttribute("dncs.ftp.list", hostsDNCS.getProperty("dncs.ftp.list"));
        //not used ???
        //service.setAttribute("dncs.storage","MEMORY");
        //not used ???
        service.setAttribute("dncs.zdts.path","assets-oob1");
        //Carousels
        //dncs.carousels="assets-oob1:assets-oob1:1:bfs:assets-oob1"
        service.setAttribute("dncs.carousels",hostsDNCS.getProperty("dncs.carousels"));
        //uids
        Map<String,String> uids = hostsDNCS.getCarouselsUid();
        uids.forEach((key,value) -> service.setAttribute(key, value));
        service.setAttribute("debug.emulate.bfs","true");
        
        
        
        //CDN
        service.setAttribute("cdn.srv.list", hostsCDN.getProperty("cdn.srv.list"));
        service.setAttribute("cdn.carousels",hostsCDN.getProperty("cdn.carousels"));
        //uids
        uids = hostsCDN.getCarouselsUid();
        uids.forEach((key,value) -> service.setAttribute(key, value));
        
        
        service.setAttribute("zc.srv.list","555:rest:rest@http://localhost:8080/zcarousel/api");
        service.setAttribute("zc.zdts.location","assets-oob1");
        service.setAttribute("zc.carousels","assets-oob1:oob:Prefetch?cid=1041&mn=;");
        
        //ST 
        //st.srv.list="311:demo:demo@192.168.26.103:8080/tsbroadcaster"
        service.setAttribute("st.srv.list",hostsST.getProperty("st.srv.list"));
        //not used?
        service.setAttribute("st.zdts.location","assets-oob1");
        //Carousels
        //st.carousels="batch-alias:zodiac_ft_etv:311:ETV:oob:Prefetch?cid=311&mn=" 
        service.setAttribute("st.carousels",hostsST.getProperty("st.carousels"));
        //uids
        uids = hostsST.getCarouselsUid();
        uids.forEach((key,value) -> service.setAttribute(key, value));
        //service.setAttribute("st.carousels.tsb.modules","OCAPExplorer:Module");
        service.setAttribute("st.carousels.tsb.modules","zodiac_ft_ocap:Module");
                
        
        service.setAttribute("st.presets","3:FREQUENCY:141000000:PID:0x1c02;");
        service.setAttribute("st.group.cfg","3:OOBApp:group.cfg");
        service.setAttribute("st.supervisor.cfg","3:OOBApp:supervisor.cfg");
        service.setAttribute("st.package.publisher.server.types","ST_OCAP");
        
        //SFTP servers
        //http.srv.list="51:ft:ft@192.168.21.28:SFTPJSC"
        service.setAttribute("http.srv.list",hostsHTTP.toString());
        //Location Alias: not used?
	//service.setAttribute("http.zdts.location","assets-oob1");
        service.setAttribute("http.zdts.location",alias);
        //carousels
        //http.carousels="alias:location:http:192.168.21.28:8080:/prefetch/uid" 
        service.setAttribute("http.carousels", hostsHTTP.getProperty("http.carousels"));
        //uids
        uids = hostsHTTP.getCarouselsUid();
        uids.forEach((key,value) -> service.setAttribute(key, value));
        
        //service.setAttribute("server.modes","1:DNCS;2:DALINC;3:DALINC;5:HTTP");
        service.setAttribute("server.modes","2:DALINC;"+getHostsProp(hosts,MODES_PROP));
                
        //service.setAttribute("server.types","1:DNCS;2:DNCS;3:ST,ST_NATIVE,ST_OCAP;4:ST_DATA;5:HTTP");
        service.setAttribute("server.types","2:DNCS;4:ST_DATA;"+ getHostsProp(hosts,TYPES_PROP));
        
        //service.setAttribute("zdts.name","1:z_dts.ini;3:assets-oob1/z_dts.ini;3:assets-oob1/zdts2k.ini;3:OCAPExplorer/z_dts.ini");
        service.setAttribute("zdts.name", getHostsProp(hosts,ZDTS_PROP));
        
        
        service.setAttribute("bfs.rpc.local.path","1:/export/home/dncs/apps/Zodiac/ptv/;");
        service.setAttribute("services.interval","10");
        service.setAttribute("max.upload.size","15000000");
        service.setAttribute("publishing.allowed.hosts","");
        
        
        service.setAttribute("max.timestamped.file.age.minutes", String.valueOf(Constants.MAX_MINUTES));
        
        
        //Alternative locations
        if (useAlternativeLocations)
            service.setAttribute("root.zdts.name", getHostsProp(hosts,ROOT_ZDTS_PROP));
        
        root.appendChild(service); 
    }
    
    private void addDvrService(){
        Element service = document.createElement("Service");
        service.setAttribute("service_id", "dvr");
        service.setAttribute("service", "com.dob.charter.services.dvr.DvrServiceImpl");
        service.setAttribute("sending-transport-id", "778");
        service.setAttribute("service-id", "dvr");
        service.setAttribute("stb-service-id", "dvr/0");
        service.setAttribute("com.dob.charter.datasource-name", "java:/comp/env/jdbc/DEF_DS");
        service.setAttribute("com.dob.charter.dvr.cassandra.localdc","datacenter1");
        service.setAttribute("com.dob.charter.dvr.cassandra.class", "net.charter.dvrlib.impl.DvrMigrationMetadataManagerImpl");
        service.setAttribute("com.dob.charter.dvr.cassandra.host", FTConfig.getInstance().getCassandraHost());
        service.setAttribute("com.dob.charter.dvr.cassandra.host--use_ips", "regionaldvrcassandra-pri.us-east-1c.engprod-charter.net");
        service.setAttribute("com.dob.charter.dvr.cassandra.port", String.valueOf(FTConfig.getInstance().getCassandraPort()));
        service.setAttribute("com.dob.charter.dvr.cassandra.keyspace", FTConfig.getInstance().getCassandraKeySpace());
        service.setAttribute("com.dob.charter.dvr.cassandra.max-connections-per-host", "5000");
        service.setAttribute("com.dob.charter.dvr.cassandra.protocol-version", "2");
        service.setAttribute("com.dob.charter.dvr.stb-listener-max-pool-size", "5000");
        root.appendChild(service);
    }
}
