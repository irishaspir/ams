package com.zodiac.ams.charter.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DataUtils {

    public static String decimalToHex(String decimal) {
        return Long.toHexString(Long.parseLong(decimal)).toUpperCase();
    }

    public static String decimalToHex(Long decimal) {
        return Long.toHexString(decimal).toUpperCase();
    }

    public static Date converterUnixToDate(long date) {
        return new Date(date * 1000);
    }

    public static String converterUnixToDateFormat(Date date) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatGmt.format(date);
    }


    public static String hex2decimal(String s) {
        return String.valueOf(Long.parseLong(s, 16));
    }

}
