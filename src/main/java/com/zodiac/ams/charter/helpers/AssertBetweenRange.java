package com.zodiac.ams.charter.helpers;

import com.zodiac.ams.common.logging.Logger;
import org.testng.Assert;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by SpiridonovaIM on 09.06.2017.
 */
public class AssertBetweenRange {
    
    public static <T> void assertBetweenAllowableRange(Collection<T> actual, Collection<T> expected, String message) {
        if ((expected == null) && (actual == null)) {
            throw new RuntimeException("Actual and expected are null");
        } else if (equals(actual, expected)) {
            Assert.assertEquals(actual, expected, message);
        } else {
            if (assertBetween(actual, expected)) Logger.info("Actual and expected are in the allowable range");
        }
    }


    public static <T> boolean assertBetween(Collection<T> actual, Collection<T> expected) {
        if ((expected == null) && (actual == null)) {
            return false;
        } else if (equals(actual, expected)) {
            return true;
        } else {
            if (assertTrueTimeStamp(actual, expected) == 4) {
                return true;
            } else return false;
        }
    }

    public static <T> boolean equals(Collection<T> lhs, Collection<T> rhs) {
        return lhs.size() == rhs.size() && lhs.containsAll(rhs) && rhs.containsAll(lhs);
    }

    private static <T> int assertTrueTimeStamp(Collection<T> actual, Collection<T> expected) {
        int j = 0;
        List<String> response = (List<String>) actual;
        List<String> pattern = (List<String>) expected;
        for (int i = 0; i < actual.size(); i++) {
            if (response.get(i).equals(pattern.get(i)))
                j++;
            else {
                if (response.get(i).startsWith("\ttimeStamp = ")) {
                    if (compareColl(pattern.get(i).replaceAll("\ttimeStamp = ", ""), response.get(i).replaceAll("\ttimeStamp = ", "")))
                        j++;
                } else Assert.assertEquals(response.get(i), pattern.get(i), "Response and pattern are not equal");
            }
        }
        return j;
    }

    private static boolean compareColl(String pattern, String response) {
        boolean x = true;
        List<String> patternList = Arrays.asList(pattern.split(" "));
        List<String> responseList = Arrays.asList(response.split(" "));
        for (int i = 0; i < patternList.size(); i++) {
            if (patternList.get(i).equals(responseList.get(i))) {
                if (x) x = true;
            } else {
                if (i == 3 && x) {
                    x = compareBetweenDate(patternList.get(i), responseList.get(i));
                } else x = false;
            }
        }
        return x;
    }

    private static boolean compareDate(LocalTime responseDate, LocalTime patternDate) {
        boolean x = responseDate.equals(patternDate.minusSeconds(1))||responseDate.isAfter(patternDate.minusSeconds(1));
        boolean y = responseDate.equals(patternDate.plusSeconds(1))||responseDate.isBefore(patternDate.plusSeconds(1));
        return (x && y);
    }

    private static boolean compareBetweenDate(String pattern, String response) {
        LocalTime pD = LocalTime.parse(pattern);
        LocalTime rD = LocalTime.parse(response);
        return compareDate(rD, pD);
    }
}
