/*
 * Copyright 2016 alexander.filippov.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zodiac.ams.charter.helpers.ft;

import com.zodiac.ams.charter.helpers.ft.FTConfig;
import static com.zodiac.ams.charter.helpers.ft.CHEmulatorHelper.logger;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import com.dob.test.charter.CharterStbEmulator;
import com.zodiac.ams.common.helpers.FTUtils;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author alexander.filippov
 */
public class CharterSTBEmulatorHelper {
    private final static String RUDP_AMS_HOST_PROP = "rudp-ams-host";
    private final static String RUDP_AMS_PORT_PROP = "rudp-ams-port";
    private final static String RUDP_LOCAL_PORT_PROP = "rudp-local-port";
    private final static String RUDP_LOCAL_HOST_PROP = "host";
    
    private final static String DVR_TESTCASE_PROP  = "dvr.testcase.file"; 
    private final static String DEFAULT_DVR_TESTCASE = "./data/testcase.xml";
            
    private static final String GLOBAL_ERROR_CODE_PROP = "reminder.global-error-code";
    private static final String ERROR_CODE_FOR_DELETE_PROP = "reminder.delete-error-code";
    private static final String ERROR_CODE_FOR_ADD_PROP = "reminder.add-error-code";
    private static final String ERROR_CODE_FOR_PURGE_PROP = "reminder.purge-error-code";
    private static final String REMINDERS_LIMIT_PROP = "reminder.reminders-limit";
    
    private final static String FILE_NAME="charter_stb_emulator.properties";
     
    private final static String RWATCHED_JSON_PROP = "r-watched-json";
    
    private final static int DEFAULT_REMINDERS_LIMIT = 16;
    
    private Properties props=new Properties();
    
    public CharterSTBEmulatorHelper(){
        setDefaultSettings();
    }  
    
    public void setRWatchedJSON(String name){
        props.setProperty(RWATCHED_JSON_PROP, name);
    }
    
    public void setGlobalErrorCode(int code){
        //props.setProperty(GLOBAL_ERROR_CODE_PROP,String.valueOf(code));
        props.setProperty(ERROR_CODE_FOR_DELETE_PROP,String.valueOf(code));
        props.setProperty(ERROR_CODE_FOR_ADD_PROP,String.valueOf(code));
        props.setProperty(ERROR_CODE_FOR_PURGE_PROP,String.valueOf(code));
        
    }
    
    public void setRemindersLimit(int limit){
        props.setProperty(REMINDERS_LIMIT_PROP,String.valueOf(limit));
    }
     
    public void setRudpAmsHost(String host){
        props.setProperty(RUDP_AMS_HOST_PROP, host);
    }
    
    public void setRudpAmsPort(String port){
        props.setProperty(RUDP_AMS_PORT_PROP, port);
    }
    
    public void setRudpLocalPort(String port){
        props.setProperty(RUDP_LOCAL_PORT_PROP, port);
    }
    
    public void setRudpLocalHost(String host){
        props.setProperty(RUDP_LOCAL_HOST_PROP, host);
    }
    
    public void setDvrTestCase(String testCase){
        props.setProperty(DVR_TESTCASE_PROP, testCase);
    }
    
    private void setDefaultSettings(){
        props.put("port",String.valueOf(7801)); //???
        
        props.put("callerid.responses","0 22");
        props.put("cd.responses","0 22");
        props.put("settings.retcodes",String.valueOf(0));
        //props.put("id-list","stblist.txt");
        props.put("use-all-ip", String.valueOf(false));
        
        props.put("rudp-ams-port",String.valueOf(FTConfig.DEFAULT_RUDP_AMS_PORT));
        props.put("rudp-local-port",String.valueOf(FTConfig.DEFAULT_RUDP_LOCAL_PORT));
        
        //props.put("dvr.testcase.file","./data/testcase.xml");
        props.put(DVR_TESTCASE_PROP,DEFAULT_DVR_TESTCASE);
        
        props.put("dvr-command-listener-enabled",String.valueOf(false));
        props.put("dvr-command-listener-port",String.valueOf(8070));
        props.put("epg-command-listener-enabled",String.valueOf(false));
        props.put("epg-command-listener-port",String.valueOf(8071));

        props.put("tcp-server-enabled",String.valueOf(false));
        props.put("udp-server-anbled",String.valueOf(false));
        props.put("rudp-server-enabled", String.valueOf(true));
        props.put("use-fragmented-decoder",String.valueOf(true));
        
        props.put(REMINDERS_LIMIT_PROP,String.valueOf(DEFAULT_REMINDERS_LIMIT));
        
        props.put(RUDP_LOCAL_HOST_PROP,FTUtils.getLocalHost());
    }
    
     
    public void saveProperties(){
        try (FileWriter writer=new FileWriter(FILE_NAME)){
            props.store(writer,"");
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
    }
    
    public CharterStbEmulator createRWatchedEmulator(String name){
        setRWatchedJSON(name);
        saveProperties();
        try {
            return new CharterStbEmulator(FILE_NAME);
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
    
    public CharterStbEmulator createEmulator(Map<String,String> params){
        if (null != params){
            Iterator<String> it =  params.keySet().iterator();
            while (it.hasNext()){
                String key = it.next();
                props.put(key, params.get(key));
            }
        }
        
        saveProperties();
        
        try {
            return new CharterStbEmulator(FILE_NAME);
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        } 
    }
    
    public CharterStbEmulator createEmulator(){
        return createEmulator(null);
    }
    
    public CharterStbEmulator createEmulator(int error){
        setGlobalErrorCode(error);
        saveProperties();
        try {
            return new CharterStbEmulator(FILE_NAME);
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        } 
    }
    
}
