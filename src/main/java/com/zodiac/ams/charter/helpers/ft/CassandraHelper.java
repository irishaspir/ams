package com.zodiac.ams.charter.helpers.ft;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.policies.ConstantReconnectionPolicy;
import com.datastax.driver.core.policies.ReconnectionPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CassandraHelper {
    private final static Logger LOG = LoggerFactory.getLogger(CassandraHelper.class);
    
    private final static ProtocolVersion DEFAULT_PROTOCOL_VERSION = ProtocolVersion.V2;
    private final static ConsistencyLevel DEFAULT_CONS_LEVEL = ConsistencyLevel.LOCAL_QUORUM;
    private final static ReconnectionPolicy DEFAULT_REC_POLICY = new ConstantReconnectionPolicy(100L);
    
    
    private static Cluster cluster;
    private static Session session;
    
    
    public static void closeSession(){
        if (null != session){
            session.close();
            session = null;
        }
        if (null != cluster){
            cluster.close();
            cluster = null;
        }
    }
    
    public static Session getSession(){
        return session;
    }
    
    
    public static Session openSession(){
        FTConfig config = FTConfig.getInstance();
        return openSession(config.getCassandraHost(), config.getCassandraPort(), config.getCassandraKeySpace());
    }
    

    public static Session openSession(String host, int port, String keySpace){
        try {
            if (null != session)
                throw new Exception("Cassandra session is already open");
            
            SocketOptions so = new SocketOptions();
            so.setKeepAlive(true);
        
            PoolingOptions poolingOptions = new PoolingOptions();
        
            Cluster.Builder builder = Cluster.builder().addContactPoint(host)
                .withProtocolVersion(DEFAULT_PROTOCOL_VERSION).withReconnectionPolicy(DEFAULT_REC_POLICY)
                .withSocketOptions(so).withPoolingOptions(poolingOptions)
                .withQueryOptions(new QueryOptions().setConsistencyLevel(DEFAULT_CONS_LEVEL))
                .withPort(port);
        
            cluster = builder.build();
            //Metadata metadata = cluster.getMetadata();
            session = cluster.connect(keySpace);
            return session;
        }
        catch(Exception ex){
            LOG.error("getSession exception: {}", ex.getMessage());
            return null;
        }
    }
            
}
