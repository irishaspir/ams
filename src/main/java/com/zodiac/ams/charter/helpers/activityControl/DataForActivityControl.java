package com.zodiac.ams.charter.helpers.activityControl;

import org.testng.annotations.DataProvider;

import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.ALOHA;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.DAVIC;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.DOCSIS;


public class DataForActivityControl {

    @DataProvider(name="networkType")
    public static Object[][] dataForNetworkType(){

        return new Object[][] {
                {DOCSIS, "DOCSIS"},
                {ALOHA, "ALOHA"},
                {DAVIC, "DAVIC"},
        };
    }

}
