package com.zodiac.ams.charter.helpers.ft;

import java.util.Properties;
import com.dob.test.charter.LighweightCHEemulator;
import java.io.FileWriter;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alexander.filippov
 */
public class CHEmulatorHelper {
    
    protected final static Logger logger = LoggerFactory.getLogger(CHEmulatorHelper.class);
    
    private final static String DEFAULT_PORT="8020";
    private final static String PORT_PROP="port";
    
    public  static final String DEFAULT_EPG_FILTERING_CONTEXT="/api/epg/filtering";
    private static final String EPG_FILTERING_CONTEXT_PROP="epg-filtering-context";
    
    private static final String R_WATCHED_CONTEXT_PROP = "recently-watched-context";
    private static final String R_WATCHED_RESPONSE_PROP = "recently-watched-response";
    public static final String DEFAULT_R_WATCHED_CONTEXT = "/rwatched";
    
    private static final String RUDP_TESTING_CONTEXT_PROP = "rudp-testing-context";
    public static final String DEFAULT_RUDP_TESTING_CONTEXT = "/rudptesting";
    
    private static final String PPV_CONTEXT_PROP = "ppv-context";
    public static final String DEFAULT_PPV_CONTEXT = "/ppv";
    
    private static final String ERROR_REPORTING_CONTEXT_PROP = "error-reporting-context"; 
    public static final String DEFAULT_ERROR_REPORTING_CONTEXT = "/errorreporting"; 
    
//    public static final String DENT_CONTEXT_PROP = "default-entitlements-context";
//    public static final String DEFAULT_DENT_CONTEXT = "/entitlements";
//    
    public static final String DENT_NEW_CONTEXT_PROP = "default-entitlements-new-context";
    public static final String DEFAULT_DENT_NEW_CONTEXT = "/dentnew";    
   
    private final static String DEFAULT_EPG_FILE="epg.txt";
    private final static String EPG_FILE_PROP="epg-file";
    public static final String DEFAULT_EPG_F_CONTEXT="/api/epg/filtering";
    
    
    private final static String EPG_ZIPPED_PROP="epg-zipped";
    
    private final static String FILE_NAME="emulator.properties";
    
    private Properties props=new Properties();
    
    public CHEmulatorHelper(){
        setDefaultSettings();
    }        
    
    private void setDefaultSettings(){
        props.put(PORT_PROP, DEFAULT_PORT);
        props.put(EPG_ZIPPED_PROP,String.valueOf(false));
        props.put(EPG_FILTERING_CONTEXT_PROP,DEFAULT_EPG_FILTERING_CONTEXT);
        props.put(EPG_FILE_PROP,DEFAULT_EPG_FILE);
        props.put(R_WATCHED_CONTEXT_PROP,DEFAULT_R_WATCHED_CONTEXT);
        props.put(RUDP_TESTING_CONTEXT_PROP, DEFAULT_RUDP_TESTING_CONTEXT);
        props.put(PPV_CONTEXT_PROP, DEFAULT_PPV_CONTEXT);
        props.put(ERROR_REPORTING_CONTEXT_PROP, DEFAULT_ERROR_REPORTING_CONTEXT);
        props.put(DENT_NEW_CONTEXT_PROP, DEFAULT_DENT_NEW_CONTEXT);
    }
    
    public void setProperty(String key,String value){
        props.put(key, value);
    }
    
    public void setPort(String port){
        props.put(PORT_PROP, port);
    }
    
    public void setZipped(boolean zipped){
        props.put(EPG_ZIPPED_PROP,String.valueOf(zipped));
    }
    
    public void setEPGFile(String file){
        props.put(EPG_FILE_PROP,file);
    }
    
    
    public void saveProperties(){
        try (FileWriter writer=new FileWriter(FILE_NAME)){
            props.store(writer,"");
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
        }
    }
    
    public LighweightCHEemulator createEmulator(){
        saveProperties();
        try {
            return new LighweightCHEemulator(FILE_NAME);
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
}
