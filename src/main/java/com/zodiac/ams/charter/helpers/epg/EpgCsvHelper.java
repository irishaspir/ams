package com.zodiac.ams.charter.helpers.epg;


import com.zodiac.ams.charter.emuls.CheEmulatorConfig;
import com.zodiac.ams.charter.emuls.che.epg.EpgSampleBuilder;
import com.zodiac.ams.charter.services.epg.EpgOptions;
import com.zodiac.ams.common.helpers.DateTimeHelper;

import java.io.*;
import java.util.ArrayList;

public class EpgCsvHelper {

    private CheEmulatorConfig cheEmulatorConfig=new CheEmulatorConfig();
    private String filename = cheEmulatorConfig.getEpgFile();
    private ArrayList<String[]> validCsvFile = new ArrayList<>();
    private String[] serviceId;
    private int countRows;
    private int countServices;
    private String validCsv;
    private int row =2;
    private String invalid = "invalid";
    private String N = "\"N\""; // false value in BD
    private String Y = "\"Y\""; // true value in BD
    private String empty = "";
    private String year = "\"1900\"";//default value YEAR if value is invalid
    private String advisory = "\"AC,AL,GL,MV,V,GV,BN,N,SSC,RP\"";//default value ADVISORY if value is invalid
    private String tvRating = "\"TVMA\"";//default value TV_RATING if value is invalid
    private String mpaaRating = "\"NC17\"";//default value MPAA_RATING if value is invalid
    private String noData = "\"No Data\"";
    private String programIdentifier = "016686220000"; //example for program identifier
    private String slash = "\"";
    private String ecranizedSlash = "\"\"";
    private String separator = "|";
    private int countRowsInCsv = 24;
    private int amountServiceId = 1;
    private String invalidColumn = "SHORT_TITLE";
    private String defaultDuration = "60";
    private String overlapDuration = "90";
    private String skipColumnn = "skip";
    private final int countSegments = 14;  //will be read from server.xml
    private EpgSampleBuilder epgSampleBuilder = new EpgSampleBuilder();

    public String[] getServiceIdList() {
        return serviceId;
    }

    public int getCountRows() {
        return countRows;
    }
    public int getCountServices() {
        return countServices;
    }


    private ArrayList<String[]> readCsvFile()  {
        ArrayList<String[]> csv = new ArrayList<>();
        File file = new File(filename);

        try {
            try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
                String s;
                while ((s = in.readLine()) != null) {
                    csv.add(s.split("\\|"));
                }
            }
            csv.remove(0);
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
        return csv;
    }

    /**
     *
     * @param field - number of column
     * @param invalidName - invalid value
     * @return string of head
     */
    private String createCsvOnlyHeadInvalid(int field, String invalidName) {
        if (invalidName.equals(empty)) invalidName = skipColumnn;
        StringBuilder str = new StringBuilder();
        EpgOptions[] options = EpgOptions.values();
        for (int i = 0; i < options.length; i++) {
            if (i == field) {
                if (!invalidName.equals(skipColumnn))
                    str.append(invalidName + separator);
            }
            else str.append(options[i].getOption().getName() + separator);
        }
        str.deleteCharAt(str.lastIndexOf(separator));
        str.append("\n");

        return epgSampleBuilder.writeDataInCsvFile(str, false);
    }

    public String createCsvWithSegmentsNotOrdered(){
        return createCsvWithSegments(validCsvFile);
    }

    public String expectedCsvWithSegmentsDummy(){
        ArrayList<String[]> csvFile = readCsvFile();
        csvFile.remove(row);
        return createCsvWithSegments(csvFile, countRowsInCsv-1, amountServiceId);
    }

    public String expectedValidCsvWithSegmentsDuplicateTime(){
        return createCsvWithSegments(validCsvFile);
    }

    public String expectedValidCsvWithSegments(){
        return expectedValidCsvWithSegments( countRowsInCsv, amountServiceId);
    }

    public String expectedValidCsvWithSegmentsWithoutOptional(){
        return createCsvWithSegments(getExpectedCsvWithoutOptionalFields());
    }

    public String expectedValidCsvWithSegmentsRepeatData(){
        ArrayList<String[]> csvFile = readCsvFile();
        return createCsvWithSegmentsRepeatData(csvFile, countRowsInCsv, amountServiceId);
    }

    public String expectedValidCsvWithSegments(int countRows, int amountServiceId){
        return createCsvWithSegments(readCsvFile(), countRows, amountServiceId);
    }

    public String expectedCsvWithSegmentsInvalidShortTitle(){
        return createCsvWithSegments(getExpectedCsvInvalidShortTitle());
    }

    public String expectedCsvWithSegmentsInvalidLongTitle(){
        return createCsvWithSegments(getExpectedCsvInvalidLongTitle());
    }

    public String expectedCsvWithSegmentsEmptyLongAndShortTitle(){
        return createCsvWithSegments(getExpectedCsvInvalidLongAndShortTitle());
    }

    public String expectedCsvWithSegmentsInvalidDescription(){
        return createCsvWithSegments( getExpectedCsvInvalidDescription());
    }

    public String expectedCsvWithSegmentsGenres(String genres){
        return createCsvWithSegments(getExpectedCsvGenres(genres));
    }

    public String expectedCsvWithSegmentsTVrating(String tvrating){
        return createCsvWithSegments(getExpectedCsvTVRating(tvrating));
    }

    public String expectedCsvWithSegmentsMPAArating(String tvrating){
        return createCsvWithSegments(getExpectedCsvMPAArating(tvrating));
    }

    public String expectedCsvWithSegmentsAdvisory(String advisory){
        return createCsvWithSegments(getExpectedCsvAdvisory(advisory));
    }

    public String expectedCsvWithSegmentsProgramId(String programid){
        return createCsvWithSegments( getExpectedCsvProgramId(programid));
    }

    public String expectedCsvWithSegmentsInvalidProgramId(){
        return createCsvWithSegments(getExpectedCsvInvalidProgramId());
    }

    public String expectedCsvWithSegmentsSeriesIdentifier(String seriesid){
        return createCsvWithSegments( getExpectedCsvSeriesIdentifier(seriesid));
    }

    public String expectedCsvWithSegmentsInvalidTVRating(){
        return createCsvWithSegments(getExpectedCsvInvalidTVRating());
    }

    public String expectedCsvWithSegmentsInvalidMPAARating(){
        return createCsvWithSegments(getExpectedCsvInvalidMPAARating());
    }

    public String expectedCsvWithSegmentsInvalidAdvisory(){
        return createCsvWithSegments(getExpectedCsvInvalidAdvisory());
    }

    public String expectedCsvWithSegmentsInvalidYear(){
        return createCsvWithSegments(getExpectedCsvInvalidYear());
    }

    public String expectedCsvWithSegmentsInvalidNew(String valueRerun, String valueNew){
        return createCsvWithSegments(getExpectedCsvNew(valueRerun, valueNew));
    }

    public String expectedCsvWithSegmentsMPAARatingGenreAdvisory(String[] values){
        return createCsvWithSegments(getExpectedCsvMPAARating(values));
    }

    public String expectedCsvWithSegmentsInvalidNewInvalidRerun(){
        return createCsvWithSegments(getExpectedCsvNew(invalid, Y));
    }

    public String expectedCsvWithSegmentsInvalidDvs(){
        return createCsvWithSegments(getExpectedCsvInvalidDvs());
    }

    public String expectedCsvWithSegmentsInvalidCc(){
        return createCsvWithSegments(getExpectedCsvInvalidCc());
    }

    public String expectedCsvWithSegmentsInvalidSap(){
        return createCsvWithSegments(getExpectedCsvInvalidSap());
    }

    public String expectedCsvWithSegmentsInvalidSurround(){
        return createCsvWithSegments(getExpectedCsvInvalidSurround());
    }

    public String expectedCsvWithSegmentsInvalidStereo(){
        return createCsvWithSegments(getExpectedCsvInvalidStereo());
    }

    public String expectedCsvWithSegmentsInvalidLive(){
        return createCsvWithSegments(getExpectedCsvInvalidLive());
    }

    public String expectedCsvWithSegmentsInvalidRerun(){
        return createCsvWithSegments(getExpectedCsvInvalidRerun());
    }

    public String expectedCsvWithSegmentsInvalidHd(){
        return createCsvWithSegments(getExpectedCsvInvalidHd());
    }

    public String expectedCsvWithSegmentsInvalidIsSeries(){
        return createCsvWithSegments(getExpectedCsvInvalidIsSeries());
    }

    public String expectedCsvWithSegmentsInvalidSeason(){
        return createCsvWithSegments(getExpectedCsvInvalidSeason());
    }

    public String expectedCsvWithSegmentsInvalidEpisode(){
        return createCsvWithSegments( getExpectedCsvInvalidEpisode());
    }

    public String expectedCsvWithSegmentsInvalidHometeamid(){
        return createCsvWithSegments(getExpectedCsvInvalidHometeamid());
    }

    public String expectedCsvWithSegmentsInvalidAwayteamid(){
        return createCsvWithSegments(getExpectedCsvInvalidAwayteamid());
    }

    private String createCsvWithSegments(ArrayList<String[]> csvFile, int countRows, int amountServiceId){
        ArrayList<String[]> dayliCsv = new ArrayList<>();
        StringBuilder dataForCsv = new StringBuilder();
        for (int k = 0; k < amountServiceId; k++) {
            for (int j=0;j< countSegments; j++) {
                for (int i = 0; i < countRows; i++)
                    dayliCsv = updateDataInCsv(new ArrayList<>(csvFile.subList(csvFile.size()/amountServiceId*k, csvFile.size()/amountServiceId*(k+1))), i, 1, "\"" + DateTimeHelper.getCurrentDayPlusAmountDays(j) + "\"");
                dataForCsv.append(createDataForCsv(dayliCsv));
            }
        }
        return dataForCsv.toString();
    }

    private String createCsvWithSegments(ArrayList<String[]> csvFile){
        return createCsvWithSegments(csvFile, countRowsInCsv, amountServiceId);
    }

    private String createCsvWithSegmentsRepeatData(ArrayList<String[]> csvFile, int countRows, int amountServiceId){
        ArrayList<String[]> dayliCsv;
        StringBuilder dataForCsv = new StringBuilder();
        for (int k = 0; k < amountServiceId; k++) {
            for (int j=0;j< countSegments; j++) {
                for (int i = 0; i < countRows; i++) {
                    for (int l=0;l<2;l++) {
                        dayliCsv = updateDataInCsv(new ArrayList<>(csvFile.subList(csvFile.size() / amountServiceId * k, csvFile.size() / amountServiceId * (k + 1))), i, 1, "\"" + DateTimeHelper.getCurrentDayPlusAmountDays(j) + "\"");
                        dataForCsv.append(createDataForCsv(dayliCsv));
                    }
                }
            }
        }
        return dataForCsv.toString();
    }

    /**
     *
     * @param csv - list of arrays, each row of list is row that consists of values of fields
     * @return string for writing to csv file
     */
    private StringBuilder createDataForCsv(ArrayList<String[]> csv){
        StringBuilder str = new StringBuilder();
        for (String[] row : csv) {
            for (int j = 0; j < row.length; j++) {
                str.append(row[j] + separator);
            }
            str.deleteCharAt(str.lastIndexOf(separator));
            str.append("\n");
        }
        return str;
    }

    /**
     *
     * @param csv - list of arrays, each row of list is row that consists of values of fields
     * @param row - row in csv file
     * @return string for writing to csv file
     */
    private StringBuilder skipPermanentFieldsInCsv(ArrayList<String[]> csv, int row) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < csv.size(); i++) {
            for (int j = 0; j < csv.get(i).length; j++) {
                if (i == row) {
                    if (j == 0 || j==1 || j==2)  str.append(empty + separator);
                    else str.append(csv.get(i)[j] + separator);
                }
                else str.append(csv.get(i)[j] + separator);;
            }
            str.deleteCharAt(str.lastIndexOf(separator));
            str.append("\n");
        }    return str;
    }


    /**
     *
     * @param csv - list of arrays, each row of list is row that consists of values of fields
     * @param row - row in csv file
     * @return string for writing to csv file
     */
    private StringBuilder skipOptionalFieldsInCsv(ArrayList<String[]> csv, int row) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < csv.size(); i++) {
            for (int j = 0; j < csv.get(i).length; j++) {
                if (i == row) {
                    if (j == 0 || j==1 || j==2 || j==3 )  str.append(csv.get(i)[j]  + separator);
                    else str.append(ecranizedSlash + separator);
                   //    else str.append(empty + separator);
                }
                else str.append(csv.get(i)[j] + separator);;
            }
            str.deleteCharAt(str.lastIndexOf(separator));
            str.append("\n");
        }    return str;
    }

    /**
     *
     * @param csv - list of arrays, each row of list is row that consists of values of fields
     * @param row - row in csv file
     * @return string for writing to csv file
     */
    private StringBuilder skipRowInCsv(ArrayList<String[]> csv, int row) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < csv.size(); i++) {
            for (int j = 0; j < csv.get(i).length; j++) {
                if (i != row)
                    str.append(csv.get(i)[j] + separator);
            }
            if (i != row) {
                str.deleteCharAt(str.lastIndexOf(separator));
                str.append("\n");
            }
        }
        return str;
    }

    /**
     *
     * @param str - string for writing to csv file
     * @return csv file
     */
    private String updateCsvFile(StringBuilder str) {
        epgSampleBuilder.createCsvOnlyHead();
        return epgSampleBuilder.writeDataInCsvFile(str, true);
    }

    /**
     *
     * @param str - string for writing to csv file
     * @param field - number of column, name of his is changed to invalid
     * @param invalidValue - invalid name of field
     * @return csv file
     */
    private String updateCsvFileWithInvalidHead(StringBuilder str, int field, String invalidValue) {
        createCsvOnlyHeadInvalid(field, invalidValue);
        return epgSampleBuilder.writeDataInCsvFile(str, true);
    }

    /**
     *
     * @param dataInCsv - list of arrays, each row of list is row that consists of values of fields
     * @param numberRow - number of row from csv
     * @param numberColumn - number od column from csv
     * @param value - new value that will be set
     * @return updated list of arrays
     */
    private ArrayList<String[]> updateDataInCsv( ArrayList<String[]> dataInCsv, int numberRow, int numberColumn, String value){
        dataInCsv.get(numberRow)[numberColumn] = value;
        return dataInCsv;
    }

    /**
     *
     * @param numbersRows - array of rows to be updated
     * @param numbersColumns - array of rows to be updated
     * @param values -  new value that will be set
     * @return csv file
     */
    private String createUpdatedCsv(int numbersRows, int  numbersColumns, String values){
        ArrayList<String[]> csvFile = getValidCsv();
        csvFile = updateDataInCsv(csvFile, numbersRows, numbersColumns, values);
        StringBuilder dataForCsv = createDataForCsv(csvFile);
        return updateCsvFile(dataForCsv);
    }

    private  ArrayList<String[]> getValidCsv(){
        validCsv = epgSampleBuilder.createCsv( Integer.toString(countRowsInCsv), defaultDuration);
        serviceId = epgSampleBuilder.getServiceIdList();
        countRows = epgSampleBuilder.getCountRows();
        countServices = epgSampleBuilder.getCountServices();
        return readCsvFile();
    }

    /**
     ** @return string of csv
     */
    public String createInvalidCsvWithoutPermanent() {
        ArrayList<String[]> csvFile = getValidCsv();
        StringBuilder dataForCsv = skipPermanentFieldsInCsv(csvFile, row);
        return updateCsvFile(dataForCsv);
    }

    /**
     ** @return string of csv
     */
    public String createInvalidCsvWithoutOptional() {
        ArrayList<String[]> csvFile = getValidCsv();
        StringBuilder dataForCsv = skipOptionalFieldsInCsv(csvFile, row);
        return updateCsvFile(dataForCsv);
    }

    public String createInvalidCsvDuplicateTime(){
        ArrayList<String[]> csvFile = getValidCsv();
        validCsvFile.addAll(csvFile);
        csvFile.add(2,  csvFile.get(1));
        return updateCsvFile(createDataForCsv(csvFile));
    }

    public String createInvalidCsvWithDuplicateColumns(){
        String csv ="";
        ArrayList<String[]> csvFile = getValidCsv();

        int[] numbersRows = new int[countRowsInCsv];
        for (int i=0;i< countRowsInCsv;i++)
            numbersRows[i] =i;

        for (int i=0; i< countRowsInCsv; i++) {
            csvFile = updateDataInCsv(csvFile, numbersRows[i], 3, csvFile.get(i)[4]);
            StringBuilder dataForCsv = createDataForCsv(csvFile);
            csv = updateCsvFileWithInvalidHead(dataForCsv,3,invalidColumn);
        }
        return csv;
    }

    public String createInvalidCsvNotOrderedByTime(){
        ArrayList<String[]> csvFile = getValidCsv();
        validCsvFile.addAll(csvFile);
        String[] first = csvFile.get(0);
        String[] temp = first;
        csvFile.remove(0);
        csvFile.add(0, csvFile.get(csvFile.size()-1));
        csvFile.add(csvFile.size()-1, temp);
        csvFile.remove(csvFile.size()-1);
        return updateCsvFile(createDataForCsv(csvFile));
    }


    public String createInvalidCsvEmptyLongAndShortTitle(){
        ArrayList<String[]> csvFile = getValidCsv();
        csvFile = updateDataInCsv(csvFile, row, 4, empty);
        csvFile = updateDataInCsv(csvFile, row, 5, empty);
        return updateCsvFile(createDataForCsv(csvFile));
    }

    public String createInvalidCsvProgramOverlap(){
        return createUpdatedCsv(5, 3, overlapDuration);
    }

    public String createCsvInvalidNew(String valueRerun){
        ArrayList<String[]> csvFile = getValidCsv();
        csvFile = updateDataInCsv(csvFile,row,20,valueRerun);
        csvFile = updateDataInCsv(csvFile,row,12, invalid);
        return updateCsvFile(createDataForCsv(csvFile));
    }

    public String createCsvInvalidNewInvalidRerun(){
        ArrayList<String[]> csvFile = getValidCsv();
        csvFile = updateDataInCsv(csvFile,row,20,invalid);
        csvFile = updateDataInCsv(csvFile,row,12, invalid);
        return updateCsvFile(createDataForCsv(csvFile));
    }

    public String createInvalidCsvIncorrectServiceId(String invalidServiceId){
        return createCsvWithDifferentData(0, invalidServiceId );
    }

    public String createInvalidCsvIncorrectDate(String invalidDate){
        return createCsvWithDifferentData(1, invalidDate);
    }

    public String createInvalidCsvIncorrectTime(String invalidTime){
        return createCsvWithDifferentData(2,invalidTime);
    }

    public String createInvalidCsvIncorrectDuration(String invalidDuration){
        return createCsvWithDifferentData(3, invalidDuration);
    }

    public String createInvalidCsvEmptyShortTitle(){
        return createCsvWithDifferentData(4, empty);
    }

    public String createInvalidCsvEmptyLongTitle(){
        return createCsvWithDifferentData(5, empty);
    }

    public String createInvalidCsvEmptyDescription(){
        return createCsvWithDifferentData(9, empty);
    }

    public String createInvalidCsvInvalidYear(){ return createCsvWithDifferentData(28,invalid);}

    public String createInvalidCsvInvalidSeason(){return createCsvWithDifferentData(7, invalid);}

    public String createInvalidCsvInvalidEpisode(){ return createCsvWithDifferentData(8, invalid);}

    public String createInvalidCsvInvalidHometeamid(){ return createCsvWithDifferentData(31, empty);}

    public String createInvalidCsvInvalidAwayteamid(){ return createCsvWithDifferentData(33, empty);}

    public String createCsvWithDifferentGenres(String genre){
        return createCsvWithDifferentData(10, genre);
    }

    public String createCsvWithTVRating(String tvrating){
        return createCsvWithDifferentData(18, tvrating);
    }

    public String createCsvWithInvalidTVRating(){
        return createCsvWithTVRating(invalid);
    }

    public String createCsvWithInvalidMPAARating(){
        return createCsvWithMPAARating(invalid);
    }

    public String createCsvWithMPAARating(String mpaarating){
        return createCsvWithDifferentData(19, mpaarating);
    }

    public String createCsvWithMPAARatingGenreAdvisory(String[] values){
        return createCsvWithDifferentData(values);
    }

    public String createCsvWithAdvisory(String advisory){ return createCsvWithDifferentData(23, advisory);}

    public String createCsvWithInvalidAdvisory(){ return createCsvWithAdvisory(invalid);}

    public String createCsvWithInvalidProgramId(){return createCsvWithDifferentData(29, invalid);
    }

    public String createCsvWithProgramId(String program){
        return createCsvWithDifferentData(29, program + programIdentifier);
    }

    public String createCsvWithSeriesIdentifier(String seriesIdent){
        return createCsvWithDifferentData(30, seriesIdent + programIdentifier);
    }

    public String createCsvWithInvalidDvs(){ return createCsvWithDifferentDvs(invalid);}

    public String createCsvWithDifferentDvs(String dvs){ return createCsvWithDifferentData(13, dvs);}

    public String createCsvWithInvalidCc(){ return createCsvWithDifferentCc(invalid);}

    public String createCsvWithDifferentCc(String cc){ return createCsvWithDifferentData(14, cc);}

    public String createCsvWithInvalidSap(){ return createCsvWithDifferentSap(invalid);}

    public String createCsvWithDifferentSap(String sap){ return createCsvWithDifferentData(15, sap);}

    public String createCsvWithInvalidSurround(){ return createCsvWithDifferentSurround(invalid);}

    public String createCsvWithDifferentSurround(String surround){ return createCsvWithDifferentData(16, surround);}

    public String createCsvWithInvalidStereo(){ return createCsvWithDifferentStereo(invalid);}

    public String createCsvWithDifferentStereo(String stereo){ return createCsvWithDifferentData(17, stereo);}

    public String createCsvWithDifferentNew(String _new){ return createCsvWithDifferentData(12, _new);}

    public String createCsvWithInvalidRerun(){ return createCsvWithDifferentRerun(invalid);}

    public String createCsvWithDifferentRerun(String rerun){ return createCsvWithDifferentData(20, rerun);}

    public String createCsvWithInvalidLive(){ return createCsvWithDifferentLive(invalid);}

    public String createCsvWithDifferentLive(String live){ return createCsvWithDifferentData(21, live);}

    public String createCsvWithInvalidHd(){ return createCsvWithDifferentHd(invalid);}

    public String createCsvWithDifferentHd(String hd){ return createCsvWithDifferentData(22, hd);}

    public String createCsvWithInvalidIsSeries(){ return createCsvWithDifferentIsSeries(invalid);}

    private String createCsvWithDifferentIsSeries(String isseries){ return createCsvWithDifferentData(35, isseries);}

    /**
     *
     * @param field - column number with invalid value
     * @param invalidValue - invalid value
     * @return invalid csv
     */
    private String createCsvWithDifferentData(int field, String invalidValue){
        return createUpdatedCsv(row, field, invalidValue );
    }

    private String createCsvWithDifferentData(String[] values){
        ArrayList<String[]> csvFile = getValidCsv();
        csvFile = updateDataInCsv(csvFile,row,23,values[2]);
        csvFile = updateDataInCsv(csvFile,row,10, values[1]);
        csvFile = updateDataInCsv(csvFile,row,19, values[0]);
        return updateCsvFile(createDataForCsv(csvFile));
    }

    private ArrayList<String[]> getExpectedCsvInvalidShortTitle(){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, 4, csv.get(2)[5]);
        return csv;
    }

    private ArrayList<String[]> getExpectedCsvInvalidLongTitle(){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, 5, csv.get(2)[4]);
        return csv;
    }

    private ArrayList<String[]> getExpectedCsvInvalidLongAndShortTitle(){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, 4, noData);
        csv = updateDataInCsv(csv, row, 5, noData);
        return csv;
    }

    private ArrayList<String[]> getExpectedCsvWithoutOptionalFields(){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, 3,defaultDuration);
        csv = updateDataInCsv(csv, row, 4, noData);
        csv = updateDataInCsv(csv, row, 5, noData);
        csv = updateDataInCsv(csv, row, 9, noData);
        csv = updateDataInCsv(csv, row, 28, year);
        csv = updateDataInCsv(csv, row, 7, empty);
        csv = updateDataInCsv(csv, row, 8, empty);
        for (int i= 12; i< 18; i++)
            csv = updateDataInCsv(csv, row, i, N);
        for (int i= 20; i< 23; i++)
            csv = updateDataInCsv(csv, row, i, N);
        for (int i= 24; i< 27; i++)
            csv = updateDataInCsv(csv, row, i, N);
        csv = updateDataInCsv(csv, row, 31, "0");
        csv = updateDataInCsv(csv, row, 33, "0");
        csv = updateDataInCsv(csv, row, 35, N);
        return csv;
    }

    private ArrayList<String[]> getExpectedCsvNew(String valueRerun, String valueNew){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, 12, valueNew);
        if (valueRerun.equals(invalid))
            csv = updateDataInCsv(csv, row, 20, N);
        return csv;
    }

    private ArrayList<String[]> getExpectedCsvMPAARating(String[] values){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, 19, values[0]);
        csv = updateDataInCsv(csv, row, 10, values[1]);
        csv = updateDataInCsv(csv, row, 23, values[2]);
        return csv;
    }

    private ArrayList<String[]> getExpectedValueCsv(int numberColumn, String expectedValue){
        ArrayList<String[]> csv = readCsvFile();
        csv = updateDataInCsv(csv, row, numberColumn, expectedValue);
        return csv;
    }

    private ArrayList<String[]> getExpectedCsvInvalidDescription(){return getExpectedValueCsv(9, noData);}

    private ArrayList<String[]> getExpectedCsvGenres(String genres){ return getExpectedValueCsv(10, genres);}

    private ArrayList<String[]> getExpectedCsvInvalidDvs(){ return getExpectedValueCsv(13, N);}

    private ArrayList<String[]> getExpectedCsvInvalidCc(){ return getExpectedValueCsv(14, N);}

    private ArrayList<String[]> getExpectedCsvInvalidSap(){ return getExpectedValueCsv(15, N);}

    private ArrayList<String[]> getExpectedCsvInvalidSurround(){ return getExpectedValueCsv(16, N);}

    private ArrayList<String[]> getExpectedCsvInvalidStereo(){ return getExpectedValueCsv(17, N);}

    private ArrayList<String[]> getExpectedCsvInvalidRerun(){ return getExpectedValueCsv(20, N);}

    private ArrayList<String[]> getExpectedCsvInvalidLive(){ return getExpectedValueCsv(21, N);}

    private ArrayList<String[]> getExpectedCsvInvalidHd(){ return getExpectedValueCsv(22, N);}

    private ArrayList<String[]> getExpectedCsvInvalidIsSeries(){ return getExpectedValueCsv(35, N);}

    private ArrayList<String[]> getExpectedCsvTVRating(String tvrating){return getExpectedValueCsv(18, tvrating);}

    private ArrayList<String[]> getExpectedCsvInvalidTVRating(){return getExpectedValueCsv(18, tvRating);}

    private ArrayList<String[]> getExpectedCsvInvalidMPAARating(){return getExpectedValueCsv(19, mpaaRating);}

    private ArrayList<String[]> getExpectedCsvMPAArating(String mpaarating){return getExpectedValueCsv(19, mpaarating);}

    private ArrayList<String[]> getExpectedCsvInvalidAdvisory(){ return getExpectedValueCsv(23, advisory);}

    private ArrayList<String[]> getExpectedCsvAdvisory(String advisory){ return getExpectedValueCsv(23,advisory);}

    private ArrayList<String[]> getExpectedCsvInvalidProgramId(){
        return  getExpectedCsvProgramId(invalid);
    }

    private ArrayList<String[]> getExpectedCsvProgramId(String programid){
        if (programid.equals(invalid))
            return getExpectedValueCsv(29,ecranizedSlash);
        else return getExpectedValueCsv(29,  slash + programid + programIdentifier + slash);
    }

    private ArrayList<String[]> getExpectedCsvSeriesIdentifier(String seriesid){
        if (seriesid.equals(invalid))
            return getExpectedValueCsv(30,ecranizedSlash);
        else return getExpectedValueCsv(30, slash + seriesid + programIdentifier + slash);
    }

    private ArrayList<String[]> getExpectedCsvInvalidYear(){ return getExpectedValueCsv(28, year);}

    private ArrayList<String[]> getExpectedCsvInvalidSeason(){ return getExpectedValueCsv(7,  empty);}

    private ArrayList<String[]> getExpectedCsvInvalidEpisode(){ return getExpectedValueCsv(8,  empty);}

    private ArrayList<String[]> getExpectedCsvInvalidHometeamid(){ return getExpectedValueCsv(31, empty);}

    private ArrayList<String[]> getExpectedCsvInvalidAwayteamid(){ return getExpectedValueCsv(33, empty);}

    public String getExpectedSkipRecord(){ return skipRowInCsv( readCsvFile(), row).toString();

    }
}
