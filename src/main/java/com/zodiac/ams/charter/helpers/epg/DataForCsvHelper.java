package com.zodiac.ams.charter.helpers.epg;

import org.testng.annotations.DataProvider;

public class DataForCsvHelper {

    @DataProvider(name="invalidStringProperty")

    public static Object[][] setInvalidValue(){

        return new Object[][] {
                {""},
                {"invalid"},
        };
    }

    @DataProvider(name="dataForGenres")
    public static Object[][] dataForGenres(){

        return new Object[][] {
                {"Adventure,Drama,Drama,Fantasy", "\"Adventure,Drama,Drama\""},
                {"Adventure,Drama,Fantasy, Religious", "\"Adventure,Drama,Fantasy\""},
        };
    }

    @DataProvider(name="dataForInvalidNew")
    public static Object[][] dataForInvalidNew(){

        return new Object[][] {
                {"\"N\"", "\"Y\""},
                {"\"Y\"", "\"N\""},
        };
    }

    @DataProvider(name="dataForBooleanValues")
    public static Object[][] dataForBooleanValues(){

        return new Object[][] {
                {"\"N\""},
                {"\"Y\""}
        };
    }

    @DataProvider(name="dataForTVRating")
    public static Object[][] dataForTVRating(){

        return new Object[][] {
                {"\"TV-Y\"", "\"TV-Y\""},
                {"\"TV-Y7\"", "\"TV-Y7\""},
                {"\"TV-G\"", "\"TV-G\""},
                {"\"TV-PG\"", "\"TV-PG\""},
                {"\"TV-14\"", "\"TV-14\""},
                {"\"TV-MA\"", "\"TV-MA\""},
                {"\"\"", "\"\""},
        };
    }

    @DataProvider(name="dataForMPAARating")
    public static Object[][] dataForMPAARating(){

        return new Object[][] {
                {"\"G\"", "\"G\""},
                {"\"PG\"", "\"PG\""},
                {"\"PG-13\"", "\"PG-13\""},
                {"\"NC-17\"", "\"NC-17\""},
                {"\"R\"", "\"R\""},
                {"\"NR\"", "\"NR\""},
                {"\"NR-ADULT\"", "\"NR-ADULT\""},
                {"\"\"", "\"\""},
        };
    }

    @DataProvider(name="dataForAdvisory")
    public static Object[][] dataForAdvisory(){

        return new Object[][] {
                {"\"AL\"", "\"AL\""},
                {"\"GL\"", "\"GL\""},
                {"\"MV\"", "\"MV\""},
                {"\"V\"", "\"V\""},
                {"\"GV\"", "\"GV\""},
                {"\"BN\"", "\"BN\""},
                {"\"N\"", "\"N\""},
                {"\"RP\"", "\"RP\""},
                {"\"SSC\"", "\"SSC\""},
                {"\"\"", "\"\""},
        };
    }

    @DataProvider(name="dataForProgramId")
    public static Object[][] dataForProgramId(){

        return new Object[][] {
                {"SH"},
                {"MV"},
                {"EP"},
                {"SP"},
        };
    }
}

