package com.zodiac.ams.charter.helpers.ft;

import com.zodiac.ams.charter.tests.ft.FuncTest;
import com.zodiac.ams.common.bd.DBHelper;
import com.zodiac.ams.common.common.AbstractPropertyHelper;
import com.zodiac.ams.common.helpers.FTUtils;
import com.zodiac.ams.common.ssh.SSHHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.zodiac.ams.charter.services.ft.publisher.Version;


public class FTConfig extends AbstractPropertyHelper{
    
    public final static int DEFAULT_RUDP_AMS_PORT = 7750;
    public final static int DEFAULT_RUDP_LOCAL_PORT = 7760;
    
    private static final String AMS_DIR_PROP = "ams-dir";
    private static final String AMS_CONFIG_PROP = "ams-config";
    private static final String AMS_CONTEXT_PROP = "ams-context";
    private static final String AMS_LOG_PROP = "ams-log";
    private static final String AMS_START_PROP="ams-start";
    private static final String AMS_STOP_PROP="ams-stop";
    private static final String AMS_CLEAR_LOGS_PROP="ams-clear-logs";
    private static final String AMS_REMOVE_PROP = "ams-remove";
    private static final String AMS_SERVER_PROP ="ams-server";
    private static final String AMS_USER_PROP = "ams-user";
    private static final String AMS_PASSWORD_PROP = "ams-password";
    
    private static final String AMS_EPG_CONTEXT_PROP = "ams-epg-context"; 
    private static final String DEFAULT_EPG_CONTEXT = "ams/UpdateEPGData";
    
    private static final String AMS_EPG_FILE_PROP = "ams-epg-file";
    
    private static final String AMS_PORT_PROP = "ams-port";
    private static final String DEFAULT_AMS_PORT = "8080";
    
    private static final String AMS_TIME_ZONE_PROP = "ams-time-zone";
    
    private static final String EMULATOR_PORT_PROP = "emulator-port";
    private static final String EPG_FILE_PROP = "epg-file-template";
    private static final String EPG_CONTROLLER_PROP = "epg-controller";
    private static final String EPG_PERIOD_PROP = "epg-period-days";
    private static final int DEFAULT_EPG_PERIOD = 10;
    private static final String EPG_MAX_RECORDS_PROP = "epg-max-records";
    
    private static final String TTS_PORT_PROP = "tts-port";
        
    
    private static final String DEVICE_ID_PROP = "device-id";
    
    private static final String AMS_REMIND_CONTEXT_PROP = "ams-remind-context";
    private static final String DEFAULT_REMIND_CONTEXT = "ams/Reminders";
    
    private static final String AMS_RWATCH_CONTEXT_PROP = "ams-rwatch-context";
    private static final String DEFAULT_RWATCH_CONTEXT = "ams/RecentlyWatched";
    
    private static final String AMS_TALKINGGUIDE_CONTEXT_PROP = "ams-talkingguide-context";
    private static final String DEFAULT_TALKINGGUIDE_CONTEXT = "ams/TalkingGuideTTS";
    
    private static final String AMS_RUDP_TESTING_CONTEXT_PROP = "ams-rudp-testing-context";
    private static final String DEFAULT_RUDP_TESTING_CONTEXT= "ams/RUDPTesting";
            
    
    private static final String AMS_PUBLISH_CONTEXT_PROP = "ams-publish-context";
    private static final String DEFAULT_PUBLISH_CONTEXT = "ams/publish";
    
    private static final String AMS_DVR_CONTEXT_PROP = "ams-dvr-context";
    private static final String DEFAULT_DVR_CONTEXT = "ams/DVR";
    
    private static final String AMS_PPV_NOTIFICATION_CONTEXT_PROP  = "ams-ppv-notification-context";
    private static final String DEFAULT_PPV_NOTIFICATION_CONTEXT = "ams/PPVEventNotification";               
    
    private static final String AMS_PPV_UPDATE_CONTEXT_PROP  = "ams-ppv-update-context";
    private static final String DEFAULT_PPV_UPDATE_CONTEXT = "ams/UpdatePPVData";       
    
    private static final String AMS_DENT_CONTEXT_PROP = "ams-dent-context";
    private static final String DEFAULT_DENT_CONTEXT = "ams/amsDefEntitlementsService";
    
    private static final String AMS_CLEANUP_CONTEXT_PROP = "ams-cleanup-context";
    private static final String DEFAULT_CLEANUP_CONTEXT = "ams/StbCleanup";
    
    private static final String CASSANDRA_HOST_PROP = "cassandra-host";
    private static final String CASSANDRA_DEFAULT_HOST = "127.0.0.1";
    private static final String CASSANDRA_PORT_PROP = "cassandra-port";
    private static final int CASSANDRA_DEFAULT_PORT = 9042;
    private static final String CASSANDRA_KEYSPACE_PROP = "cassandra-keyspace";
    private static final String CASSANDRA_DEFAULT_KEYSPACE = "dvr";
            
    private static final String LOCAL_HOST_PROP = "local-host";
    
    private static final String AMS_DB_CONN_PROP = "ams.db.connection";
    private static final String AMS_DB_USER_PROP = "ams.db.username";
    private static final String AMS_DB_PWD_PROP = "ams.db.password";            
    
    private static final String AMS_PUBLISHER_VERSION_PROP = "ams.publihser.version";
    
    private static final String NAME="func.properties";
    
    static final String MAX_REMINDERS_PROP = "max-stb-receiving-reminders";
    
    private static FTConfig instance;
    private FTConfig publisher;
    
    protected final static Logger logger = LoggerFactory.getLogger(FTConfig.class);
    
    
    private static final String configPropertyName = "ft";
    private static final String localPropertiesFileName = "properties/ft.properties";

    private static final String JACOCO_DATA_FILE_PROP = "jacoco.datafile";
    
    
    private static final String DC_OUT_PROP = "dc-out-dir";
    private static final String DC_SFTP_HOST_PROP = "dc-sftp-host";
    private static final String DC_SFTP_LOGIN_PROP = "dc-sftp-login";
    private static final String DC_SFTP_PWD_PROP = "dc-sftp-pwd";
    private static final String DC_SFTP_REMOTE_DIR_PROP = "dc-sftp-remote-dir";
    private static final String DC_DOCKER_PROP = "dc-docker";
    private static final String DC_TUNER_USE_PROP = "dc-tuner-use";

    static {
            System.setProperty(configPropertyName, localPropertiesFileName);
            instance=new FTConfig();
    }
    
    private FTConfig(String name){
        super(getFileFromJar("properties",name));
    }

    private FTConfig() {
        this("ft.properties");
    }
    
    public synchronized static String getFileFromJar(String dir,String name){
        String path = dir+"/"+name;
        if (new File(path).exists())
            return path;
        
        try {
            new File(dir).mkdirs();
            byte [] buffer = new byte[4_096];
        
            try (BufferedInputStream in=new BufferedInputStream(FuncTest.class.getClassLoader().getResourceAsStream(path));
                BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(path));){
                int read;
                while (-1 != (read=(in.read(buffer))))
                    out.write(buffer,0, read);
                out.flush();
            }
        
        }
        catch(IOException ex){
            logger.error(ex.getMessage());
            return null;
        }
        return path;
    }
    
    public static FTConfig getInstance(){
        return instance;
    }
    
    public FTConfig getPublisher(){
        if (null == publisher){
            Version configVersion = getPublihserVersion();
            if (Version.VERSION_3_1 == configVersion || Version.VERSION_3_2 == configVersion)
                configVersion = Version.VERSION_3;
            publisher = new FTConfig(String.format("publisher_%s.properties", configVersion.getVersion()));
        }
        return publisher;
    }
    
    @Override
    public String getProperty(String key) {
        String value = System.getProperty(key);
        if (null != value)
            return value;
        return properties.getString(key);
    }
    
    public Version getPublihserVersion(){
        return Version.fromVersion(getProperty(AMS_PUBLISHER_VERSION_PROP));
    }
    
    public int getMaxReminders(){
        return getIntProperty(MAX_REMINDERS_PROP,0);
    }
    
    public String getTTSPort(){
        return getProperty(TTS_PORT_PROP);
    }
    
    public String getAMSTimeZone(){
        return getProperty(AMS_TIME_ZONE_PROP);
    }
    
    public String getAMSDbConn(){
        return getProperty(AMS_DB_CONN_PROP);
    }
    
    public String getAMSDbUser(){
        return getProperty(AMS_DB_USER_PROP);
    }
    
    public String getAMSDbPwd(){
        return getProperty(AMS_DB_PWD_PROP);
    }
    
    public String getAMSStartCommand(){
        return getProperty(AMS_START_PROP);
    }
    
    public String getAMSStopCommand(){
        return getProperty(AMS_STOP_PROP);
    }
    
    public String getAMSClearLogsCommand(){
        return getProperty(AMS_CLEAR_LOGS_PROP);
    }
    
    public String getAMSRemoveCommand(){
        return getProperty(AMS_REMOVE_PROP);
    }
    
    public String getAMSDir(){
        return getProperty(AMS_DIR_PROP);
    }
    
    public String getAMSConfig(){
        return getProperty(AMS_CONFIG_PROP);
    }
    
    public String getAMSContext(){
        return getProperty(AMS_CONTEXT_PROP);
    }
    
    public String getAMSLog(){
        return getProperty(AMS_LOG_PROP);
    }
    
    public String getAMSEpgFile(){
        return getProperty(AMS_EPG_FILE_PROP);
    }
    
    public SSHHelper getSSHExecutor(){
        return new SSHHelper(
            getProperty(AMS_SERVER_PROP),getProperty(AMS_USER_PROP),getProperty(AMS_PASSWORD_PROP));
    }
    
    public String getEmulatorPort(){
        return getProperty(EMULATOR_PORT_PROP);
    }
    
    public String getEPGFile(){
        return getProperty(EPG_FILE_PROP);
    }
    
    public String getEPGController(){
        return getProperty(EPG_CONTROLLER_PROP);
    }
    
    public int getEPGPeriod(){
        return getIntProperty(EPG_PERIOD_PROP, DEFAULT_EPG_PERIOD);
    }
    
    public int getEPGMaxRecords(){
        return getIntProperty(EPG_MAX_RECORDS_PROP,-1);
    }
    
    public String getJaCoCoDataFile(){
        return getProperty(JACOCO_DATA_FILE_PROP);
    }

    public String getAMSUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        return "http://"+host+":"+port;
    }
    
    public String getEPGUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_EPG_CONTEXT_PROP, DEFAULT_EPG_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getRemindersUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_REMIND_CONTEXT_PROP, DEFAULT_REMIND_CONTEXT);
        return "http://"+host+":"+port+"/"+context+"?req=ChangeReminders";
    }
    
    public String getRWatchUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_RWATCH_CONTEXT_PROP, DEFAULT_RWATCH_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getTalkingGuideUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_TALKINGGUIDE_CONTEXT_PROP, DEFAULT_TALKINGGUIDE_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getRUDPTestingUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_RUDP_TESTING_CONTEXT_PROP, DEFAULT_RUDP_TESTING_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getPublisherUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_PUBLISH_CONTEXT_PROP, DEFAULT_PUBLISH_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getDVRUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_DVR_CONTEXT_PROP, DEFAULT_DVR_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getPPVNotificationUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_PPV_NOTIFICATION_CONTEXT_PROP, DEFAULT_PPV_NOTIFICATION_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getPPVUpdateUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_PPV_UPDATE_CONTEXT_PROP, DEFAULT_PPV_UPDATE_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
    
    public String getDENTUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_DENT_CONTEXT_PROP, DEFAULT_DENT_CONTEXT);
        return "http://"+host+":"+port+"/"+context+"?req=SetEntitlements";
    }
    
    public String getCleanupUri(){
        String host=getProperty(AMS_SERVER_PROP);
        String port=getProperty(AMS_PORT_PROP,DEFAULT_AMS_PORT);
        String context=getProperty(AMS_CLEANUP_CONTEXT_PROP, DEFAULT_CLEANUP_CONTEXT);
        return "http://"+host+":"+port+"/"+context;
    }
                
    public String getAMSHost(){
        return getProperty(AMS_SERVER_PROP);
    }
    
    public String getLocalHost(){
        return getProperty(LOCAL_HOST_PROP,FTUtils.getLocalHost());
    }
    
    public String getMacAddress(String ipS){
        DBHelper db = new DBHelper();
        Connection connection = null;
        try {
            connection = db.getConnection(getAMSDbConn(),getAMSDbUser(),getAMSDbPwd());
        
            long l = 0;
        
            int ip = FTUtils.ipToInt(ipS);
        
            try{
                l = ((Number)db.selectSingle("select max(MAC) from MAC_IP where IP=?", ip)).longValue();
                return String.format("%012x",l).toUpperCase();
            }
            catch(Exception ex){
            }
        
            try{
                l=((Number)db.selectSingle("select max(MAC) from MAC_IP")).longValue();
            }
            catch(Exception ex){
            }
        
            l++;
            String mac = String.format("%012x",l); 
            db.updateSingle("insert into MAC_IP(MAC,IP,MAC_STR,IP_STR) values (?,?,?,?)",l,ip,mac,ipS);
            return mac.toUpperCase();  
        }
        finally {
            if (null != connection){
                try {
                    connection.commit();
                    connection.close();
                }
                catch(SQLException ex){
                    logger.error(ex.getMessage());
                }
            }
        }
     } 
    
    public void setDeviceId(String id){
        properties.setProperty(DEVICE_ID_PROP, id);
    }
    
    public static long getMac(String deviceId){
        return Long.parseLong(deviceId, 16);
    }
    
    public String getDeviceId(){
        String id=getProperty(DEVICE_ID_PROP);
        if (null == id || id.trim().equals("")){
            id = getMacAddress(getLocalHost());
            properties.setProperty(DEVICE_ID_PROP, id);
        }
        return id;
    }
    
    public String getDCOutDir(){
        return getProperty(DC_OUT_PROP);
    }
    
    public String getDCSftpHost(){
        return getProperty(DC_SFTP_HOST_PROP);
    }
    
    public String getDCSftpLogin(){
        return getProperty(DC_SFTP_LOGIN_PROP);
    }
    
    public String getDCSftpPwd(){
        return getProperty(DC_SFTP_PWD_PROP);
    }
    
    public String getDCSftpRemoteDir(){
        return getProperty(DC_SFTP_REMOTE_DIR_PROP);
    }
    
    public boolean isDCDocker(){
        return Boolean.parseBoolean(getProperty(DC_DOCKER_PROP, "true"));
    }
    
    public boolean getDCTunerUse(){
        return Boolean.parseBoolean(getProperty(DC_TUNER_USE_PROP, "false"));
    }
    
    public String getCassandraHost(){
        return getProperty(CASSANDRA_HOST_PROP, CASSANDRA_DEFAULT_HOST);
    }
    
    public int getCassandraPort(){
        try{
            return Integer.parseInt(getProperty(CASSANDRA_PORT_PROP));
        }
        catch(Exception ex){
            return CASSANDRA_DEFAULT_PORT;
        }
    }
    
    public String getCassandraKeySpace(){
        return getProperty(CASSANDRA_KEYSPACE_PROP, CASSANDRA_DEFAULT_KEYSPACE);
    }
}
