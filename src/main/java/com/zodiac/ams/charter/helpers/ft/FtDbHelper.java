package com.zodiac.ams.charter.helpers.ft;

import com.datastax.driver.core.ColumnDefinitions.Definition;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.zodiac.ams.common.bd.DBHelper;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.testng.Assert;
import java.sql.Clob;

public class FtDbHelper {
    
    
    private static String getItems(Map<String,Object> params, String head, String delimiter){
        StringBuilder sb = new StringBuilder();
        params.forEach((key,value) -> {
            sb.append(" ");
            sb.append(0 == sb.toString().trim().length() ? head : delimiter);
            sb.append(" ");
            sb.append(key).append("=");
            if (value instanceof String)
                sb.append("'");
            sb.append(value.toString());
            if (value instanceof String)
                sb.append("'");
        });
        return sb.toString();
    }
    
    private static String getWhereItems(Map<String,Object> params){
        if (null == params || 0 == params.size())
            return "";
        return getItems(params, "WHERE", "AND");
    }
    
    private static String getUpdateItems(Map<String,Object> params){
        return getItems(params, "SET", ",");
    }
    
    
    private static String getSelectItem(List<String> fields){
        if (null == fields || 0 == fields.size())
            return "SELECT *";
        StringBuilder sb = new StringBuilder();
        fields.forEach(field -> sb.append(0 == sb.length() ? "SELECT " : " , ").append(field));
        return sb.toString();
    }
    
    private static String getSelect(String table, List<String> fields, Map<String,Object> params){
        StringBuilder sb = new StringBuilder();
        sb.append(getSelectItem(fields)).append(" FROM ").append(table).append(" ").append(getWhereItems(params));
        return sb.toString();
    }
    
    private static String getDelete(String table, Map<String,Object> params){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ").append(table).append(" ").append(getWhereItems(params));
        return sb.toString();
    }
    
    private static String getUpdate(String table, Map<String,Object> updateItems, Map<String,Object> whereItems){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(table).append(" ").
            append(getUpdateItems(updateItems)).append(" ").append(getWhereItems(whereItems));
        return sb.toString();
    }
    
    private static String getInsert(String table, List<String> fields, List<Object> values){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(table).append("(");
        int index = 0;
        for (String field: fields){
            if (index > 0)
                sb.append(",");
            sb.append(field);
            index++;
        }
        sb.append(") VALUES (");
        index = 0;
        for (Object value: values){
            if (index > 0)
                sb.append(",");
            if (value instanceof String)
                sb.append("'");
            sb.append(value.toString());
            if (value instanceof String)
                sb.append("'");
            index++;
        }
        sb.append(")");
        return sb.toString();
    }
    
    public static List selectCassandra(String table, List<String> fields, Map<String,Object> params){
        List result = new ArrayList();
        Session session = CassandraHelper.getSession();
        ResultSet rs = session.execute(getSelect(table, fields, params));
        Row row = rs.one();
        if (null != row){
            Iterator<Definition> it = row.getColumnDefinitions().iterator();
            int index = 0;
            while (it.hasNext()){
                result.add(row.getObject(index++));
                it.next();
            }
            Assert.assertTrue(null == rs.one(), "Only one row in Cassandra result set is allowed");
        }
        return result;
    }
    
    public static List selectDB(String table, List<String> fields, Map<String,Object> params){
        List result = new ArrayList();
        List<Object[]> list = DBHelper.select(getSelect(table, fields, params));
        Assert.assertTrue(list.size() < 2, "Only one row in DB result set is allowed");
        if (1 == list.size())
            result.addAll(Arrays.asList(list.get(0)));
        return result;
    }
    
    public static void deleteDB(String table, Map<String,Object> params){
        DBHelper.update(getDelete(table, params));
    }
    
    public static void deleteCassandra(String table, Map<String,Object> params){
        CassandraHelper.getSession().execute(getDelete(table, params));
    }
    
    public static void insertDB(String table, List<String> fields, List values){
        DBHelper.update(getInsert(table, fields, values));
    }
    
    public static void insertCassandra(String table, List<String> fields, List values){
        CassandraHelper.getSession().execute(getInsert(table, fields, values));
    }
    
    public static void updateDB(String table, Map<String,Object> updateItems, Map<String,Object> whereItems){
        DBHelper.update(getUpdate(table, updateItems, whereItems));
    }
    
    public static String clobToString(Clob data){
        StringBuilder sb = new StringBuilder();
        try(BufferedReader br = new BufferedReader(data.getCharacterStream())){
            int read;
            while(-1 != (read = br.read()))
                sb.append((char)read);
        }
        catch (Exception ex){
        }
        return sb.toString();
    }
    
}
