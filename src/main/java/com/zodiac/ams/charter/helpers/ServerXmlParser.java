package com.zodiac.ams.charter.helpers;

/**
 * Created by SpiridonovaIM on 16.02.2017.
 */
public class ServerXmlParser {

    public static int ipToInt(String ip) {
        String[] values = ip.split("\\.");
        return (Integer.valueOf(values[0]) << 24)
                + (Integer.valueOf(values[1]) << 16)
                + (Integer.valueOf(values[2]) << 8)
                + (Integer.valueOf(values[3]));
    }



}
