package com.zodiac.ams.charter.tomcat.command;

import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import com.zodiac.ams.common.tomcat.ParserLog;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.*;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentTimeTest;
import static java.lang.Thread.sleep;

public class TomcatHelper {

    private static Config config = new Config();
    private static final String TOMCAT_USER = config.getTomcatUser();
    private static final String SEPARATOR = File.separator;
    private static final String USER_FOLDER = System.getProperty(USER_DIR);
    private static final String EPG_DATA_FOLDER = USER_FOLDER + SEPARATOR + config.getEpgData();
    private static final String HOME_FOLDER = USER_FOLDER.replaceAll(System.getProperty(USER_NAME), "") + SEPARATOR;
    private static final String REMOTE_CLIENT_FILES_PATTERN = HOME + config.getDncsClientName() + "/dncs/assets-oob1/%s ";
    private static final String PATH_TO_TOMCAT = config.getTomcatPath();
    private static ParserLog parserLog = new ParserLog();
    private static final String PATH_TO_LOG = String.format(LOG_PATTERN, config.getTomcatPath()) + File.separator + CHARTER_LOG;
    protected static List<String> log = new ArrayList<>();
    private static Date startTomcatTime = getCurrentTimeTest();

    private static void runCommand(String command) {
        Logger.info("Command: " + command);
        //    ProcessBuilder run = new ProcessBuilder(command);
        Runtime run = Runtime.getRuntime();
        Process p = null;
        try {
            //   p = run.startDSGService();
            p = run.exec(command);
            p.getErrorStream();
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.debug("ERROR_VERSION3_PROTOCOL.RUNNING.CMD");
        } finally {
            p.destroy();
        }
    }

    public static void copyZdbFromDncsClientToEpgDataFolder(List<String> names) {
        for (String name : names) {
            runCommand(String.format(SUDO_COPY + REMOTE_CLIENT_FILES_PATTERN + EPG_DATA_FOLDER, name));
        }
    }


    public static Date getStartTomcatTime() {
        return startTomcatTime;
    }

    public static void tomcatStart() {
        Logger.info("Tomcat start ");
        runCommand(String.format(TOMCAT_START_PATTERN, TOMCAT));
    }

    public static void parseAMSLog() {
        Logger.info("Parse log path to log: " + PATH_TO_LOG + " start tomcat Time " + startTomcatTime + " key word " + AMS_STARTER);
        while (log.size() == 0) {
            log = parserLog.readLog(PATH_TO_LOG, startTomcatTime, AMS_STARTER);
            Logger.info("Number of log lines is " + log.size());
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int i = 0;
        List<String> exceptions = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        while (i != log.size()) {
            if (StringUtils.containsIgnoreCase(log.get(i), "exception")) {
                exceptions.add(log.get(i));
            }
            if (Pattern.compile(AMS_DB_ERRORS_PATTERN).matcher(log.get(i)).find()) {
                errors.add(log.get(i));
            }
            i++;
        }
        if (exceptions.size() != 0) {
            Logger.info("--------------------------------");
            Logger.info("AMS started with exceptions: ");
            exceptions.forEach(exception -> Logger.info("Exception: " + exception));
            Logger.info("--------------------------------");
        }
        if (errors.size() != 0) {
            Logger.info("--------------------------------");
            Logger.info("AMS started with errors in DB: ");
            errors.forEach(error -> Logger.info("DB error: " + error));
            Logger.info("--------------------------------");
        }
    }

    public static void tomcatStop() {
        Logger.info("Tomcat stop ");
        runCommand(String.format(TOMCAT_STOP_PATTERN, TOMCAT));
    }

    public static void copyContextXML() {
        copyFileToPathWithTomcatChowner(PATH_TO_ETC + CONTEXT_XML, String.format(CONF_PATTERN, PATH_TO_TOMCAT));
    }

    public static void copyServerXML() {
        copyFileToPathWithTomcatChowner(PATH_TO_ETC + SERVER_XML, String.format(CONF_CHARTER_PATTERN, PATH_TO_TOMCAT));
    }

    public static void copyTestNG() {
        copyFileToPathWithTomcatChowner(PATH_TO_SUITS + TESTNG_XML, USER_FOLDER);
    }

    private static void copyFileToPathWithTomcatChowner(String file, String path) {
        Logger.info("" + SUDO_COPY + USER_FOLDER + SEPARATOR + file + " " + path);
        runCommand(SUDO_CHOWN + " " + TOMCAT_USER + ":" + TOMCAT_USER + " " + path + SEPARATOR + file);
        runCommand(SUDO_COPY + USER_FOLDER + SEPARATOR + file + " " + path);
        runCommand(SUDO_CHOWN + " " + TOMCAT_USER + ":" + TOMCAT_USER + " " + path + SEPARATOR + file);
    }

    public static void clearTemp() {
        Logger.debug("Clear temp ");
        runCommand(String.format(SUDO_CLEAR + TEMP_PATTERN + SEPARATOR + "*", PATH_TO_TOMCAT));
    }

    public static void clearTemp(List<String> zdbFolders) {
        Logger.debug("Clear temp ");
        for (String zdbFolder : zdbFolders)
            runCommand(String.format(SUDO_CLEAR + TEMP_PATTERN + SEPARATOR + zdbFolder, PATH_TO_TOMCAT));
    }

    public static void clearLog() {
        Logger.debug("Clear temp ");
        runCommand(String.format(SUDO_CLEAR + LOG_PATTERN + SEPARATOR + "*", PATH_TO_TOMCAT));
    }
}
