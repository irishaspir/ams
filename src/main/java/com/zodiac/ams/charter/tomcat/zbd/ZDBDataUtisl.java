package com.zodiac.ams.charter.tomcat.zbd;


import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import com.zodiac.ams.common.ssh.SSHHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
;
import static com.zodiac.ams.charter.runner.Utils.isLocalIp;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.*;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getTodayStrForZDB;

public class ZDBDataUtisl {

    private final String AMS_DOWNLOAD = "ams.downloads";
    private final String AMS_FOLDER = "ams.";

    private Config config = new Config();
    private SSHHelper sshHelper = new SSHHelper(config.getAmsIp(), config.getServerUsername(), config.getServerPassword());

    private final String EPG_WORKING_DIR = String.format(TEMP_PATTERN, config.getTomcatPath());


    public String epgLastFolder() {
        String lastFolder = null;
        if (isLocalIp()) {
            lastFolder = findEPGLastFolder(sshHelper.getFolders(EPG_WORKING_DIR)) +"/encoded.wb_hdr";
            Logger.debug(lastFolder);
        } else {
            lastFolder = String.format(ENCODED_PATTERN, config.getTomcatPath(), findEPGLastFolder(getTempZdbFolders(EPG_WORKING_DIR)));
            Logger.debug(lastFolder);
        }
        return lastFolder;
    }

    private List<String> getTempZdbFolders(String dir) {
        List<String> tempZdbFolders = getTempFolder(dir);
        tempZdbFolders.remove(AMS_DOWNLOAD);
        return tempZdbFolders;
    }

    public List<String> getTempFolder() {
        return getTempFolder(EPG_WORKING_DIR);
    }

    private List<String> getTempFolder(String dir) {
        List<String> tempZdbFolders = new ArrayList<>();
        File folderZDB = new File(dir);
        File[] filesList = folderZDB.listFiles();
        for (File folder : filesList) {
            if (folder.isDirectory()) {
                tempZdbFolders.add(folder.getName());
                Logger.info("ZDB folders " + folder.getName());
            }
        }
        return tempZdbFolders;
    }

    private String findEPGLastFolder(List<String> list) {
        Pattern p = Pattern.compile(AMS_FOLDER + getTodayStrForZDB() + "-");
        String last = list.get(0).substring(13, 19);
        String lastFolder = list.get(0);
        for (String item : list) {
            Matcher m = p.matcher(item);
            if (m.find()) {
                if (Integer.parseInt(last) < Integer.parseInt(item.substring(13, 19))) {
                    last = item.substring(13, 19);
                    lastFolder = item;
                }
            }
        }
        Logger.info("Last folder " + lastFolder);
        return lastFolder;
    }

    public void cleanTempAms() {
        sshHelper.exec(String.format(SUDO_CLEAR + TEMP_PATTERN + File.separator + "*", config.getTomcatPath()));
    }
}



