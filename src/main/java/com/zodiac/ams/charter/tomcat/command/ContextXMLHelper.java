package com.zodiac.ams.charter.tomcat.command;

import com.zodiac.ams.charter.store.TomcatKeyWordStore;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.*;

public class ContextXMLHelper extends XMLHelper {

    private static Config config = new Config();
    private static final String CONTEXT_XML = PATH_TO_ETC + TomcatKeyWordStore.CONTEXT_XML;

    public static void setContext() {
        changeUrlUsernamePasswordInContext();
        changeEnvironmentLogDirValue();
        changeEnvironmentConfigDirValue();
    }

    private static void changeUrlUsernamePasswordInContext() {
        init(CONTEXT_XML);
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        keys.add(CONTEXT_DRIVER_CLASS_NAME);
        values.add(config.getAmsDbDriverClassName());
        keys.add(CONTEXT_URL);
        values.add(config.getAmsDbConnection());
        keys.add(CONTEXT_USER);
        values.add(config.getAmsDbUsername());
        keys.add(CONTEXT_PASSWORD);
        values.add(config.getAmsDbPassword());
        changeAttributesInTag(CONTEXT_RESOURCE, CONTEXT_JDBC_DEF_DS, keys, values);
        changeAttributesInTag(CONTEXT_RESOURCE, CONTEXT_JDBC_DEF_DS1, keys, values);
        save(CONTEXT_XML);
    }

    private static void changeEnvironmentLogDirValue() {
        init(CONTEXT_XML);
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        keys.add(CONTEXT_VALUE);
        values.add(String.format(LOG_PATTERN, config.getTomcatPath()));
        changeAttributesInTag(CONTEXT_ENVIRONMENT, CONTEXT_LOG_DIR, keys, values);
        save(CONTEXT_XML);
    }

    private static void changeEnvironmentConfigDirValue() {
        init(CONTEXT_XML);
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        keys.add(CONTEXT_VALUE);
        values.add(String.format(CONF_CHARTER_PATTERN, config.getTomcatPath()));
        changeAttributesInTag(CONTEXT_ENVIRONMENT, CONTEXT_CONFIG_DIR, keys, values);
        save(CONTEXT_XML);
    }

    private static void changeAttributesInTag(String tag, String name, List<String> keys, List<String> values) {
        NodeList nodeList = root.getElementsByTagName(tag);
        Logger.debug("" + nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element el = (Element) nodeList.item(i);
            if (el.getAttribute(CONTEXT_NAME).equals(name))
                for (int j = 0; j < keys.size(); j++)
                    el.setAttribute(keys.get(j), values.get(j));
        }

    }


}
