package com.zodiac.ams.charter.tomcat.logger.callerId;

import com.zodiac.ams.common.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CallerIDLogParser {

    private String iD = CallerIDLogKeyWords.iD;
    private String callerName = CallerIDLogKeyWords.callerName;
    private String phoneNumber = CallerIDLogKeyWords.phoneNumber;
    private String callId = CallerIDLogKeyWords.callId;

    public List<String> getLogAMSForValidDBModeIsActive(List<String> log) {
        List<String> keyExpressions = new ArrayList<>();
        for (String lineLog : log) {
            if (Pattern.compile(CallerIDLogKeyWords.dbModeIsActive).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return  keyExpressions;
    }

    public List<String> getLogAMSForValid(List<String> log) {
        List<String> keyExpressions = new ArrayList<>();
        for (String lineLog : log) {
            if (Pattern.compile(CallerIDLogKeyWords.callTermination).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(CallerIDLogKeyWords.incomingCall).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(CallerIDLogKeyWords.moveToDBMode).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(CallerIDLogKeyWords.moveToSubscriptionMode).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return  keyExpressions;
    }

    private List<Pattern> createPatternsListExitFromDBToSubscriptionMode(){
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(CallerIDLogKeyWords.moveToDBMode));
        patterns.add(Pattern.compile(CallerIDLogKeyWords.moveToSubscriptionMode));
        return patterns;
    }

    private List<Pattern> createPatternsListDBModeIsActive(){
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(CallerIDLogKeyWords.dbModeIsActive));
        return patterns;
    }

    private List<Pattern> createPatternsListIncoming(String id, List<String> parameters){
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(CallerIDLogKeyWords.incomingCallPattern
                .replace(iD, id)
                .replace(phoneNumber, parameters.get(1))
                .replace(callerName,parameters.get(0))
                .replace(callId,parameters.get(2))));
        return patterns;
    }

    private List<Pattern> createPatternsListTermination(String id, String callID){
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(CallerIDLogKeyWords.callTerminationPattern
                .replace(iD, id)
                .replace(callId,callID)));
        return patterns;
    }

    public boolean compareLogAMSWithPatternExitFromDBToSubscriptionMode(List<String> log){
        List<Pattern> patterns = createPatternsListExitFromDBToSubscriptionMode();
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternDBModeIsActive(List<String> log){
        List<Pattern> patterns = createPatternsListDBModeIsActive();
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternIncoming(List<String> log, String id, List<String> parameters){
        List<Pattern> patterns = createPatternsListIncoming(id, parameters);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternTermination(List<String> log, String id, String callId){
        List<Pattern> patterns = createPatternsListTermination(id, callId);
        return compareLogAMSWithPattern(log, patterns);
    }

    private boolean compareLogAMSWithPattern(List<String> log, List<Pattern> patterns){
        for (String logLine: log)
            Logger.info("LOG CALLER_ID: " + logLine);
        if (log.size()!=patterns.size()) {
            try {
                throw new Exception("Pattern log's size doesn't equals AMS log's size");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        boolean verified = false;
        List<String> errorLog = new ArrayList<>();
        List<Pattern> errorPattern = new ArrayList<>();
        for (int i=0;i< log.size();i++) {
            if (!patterns.get(i).matcher(log.get(i)).find()) {
                errorLog.add(log.get(i));
                errorPattern.add(patterns.get(i));
            }
        }
        if (errorLog.size()!=0) {
            for (int i=0;i< errorLog.size();i++) {
                Logger.info("Log from AMS: " + errorLog.get(i) + "\n");
                Logger.info("Pattern log: " + errorPattern.get(i) + "\n");
            }
        }
        else verified = true;
        return verified;
    }
}
