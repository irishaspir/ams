package com.zodiac.ams.charter.tomcat.command;

import com.zodiac.ams.common.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

abstract class XMLHelper {

    protected static Document document;
    protected static Element root;

    protected static void init (String name){
        try {
            File file=new File(name);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
            root = document.getDocumentElement();
        } catch (Exception ex) {
            Logger.error("XMLHelper() exception: {}", ex.getMessage());
        }
    }

    protected static void save(String name) {
        try {
            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(name);
            transformer.transform(source, result);
        } catch (Exception ex) {
            Logger.error("XMLHelper save exception: {}", ex.getMessage());
        }
    }



}
