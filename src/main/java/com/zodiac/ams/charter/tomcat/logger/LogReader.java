package com.zodiac.ams.charter.tomcat.logger;

import com.zodiac.ams.common.common.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.CHARTER_LOG;
import static com.zodiac.ams.charter.store.TomcatKeyWordStore.LOG_PATTERN;

/**
 * Created by SpiridonovaIM on 09.02.2017.
 */
public class LogReader {
    private static Config config = new Config();

    public static void main(String[] args) {
        File file = new File(String.format(LOG_PATTERN, config.getTomcatPath()) + CHARTER_LOG);
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            //First time read
            String str = null;
            while ((str = randomAccessFile.readLine()) != null) {
                System.out.println(str);
            }
            randomAccessFile.seek(randomAccessFile.getFilePointer());
            startTimer(randomAccessFile);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static void startTimer(final RandomAccessFile randomAccessFile) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                String str = null;
                try {
                    while ((str = randomAccessFile.readLine()) != null) {
                        System.out.println(str);
                    }
                    randomAccessFile.seek(randomAccessFile.getFilePointer());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000);
    }

}
