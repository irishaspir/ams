package com.zodiac.ams.charter.tomcat.logger.epg;

import com.zodiac.ams.common.common.Config;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.TEMP_PATTERN;

public interface EpgLogKeyWords {

    Config config = new Config();
    String amountSegments = "{amountSegments}";
    String allPrograms = "{allPrograms}";
    String acceptedPrograms = "{acceptedPrograms}";
    String declinedPrograms = "{declinedPrograms}";
    String duplicatePrograms = "{duplicatePrograms}";
    String verifiedCsvLines = "{verifiedCsvLines}";
    String invalidParameters = "{invalidParameters}";
    String ratio = "{ratio}";
    String value = "{value}";
    String fieldName = "{fieldName}";
    String tempDirZdb = "{tempDirZdb}";
    String startTime = "{startTime}";
    String endTime = "{endTime}";
    String date = "{date}";
    String url = "{url}";
    String path = String.format(TEMP_PATTERN, new Config().getTomcatPath());

    String EPG_SERVICE_PATTERN = "^([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}.[0-9]{3}) {type} Thread-[0-9]{1,}/";
    String EPG_PARSER_PATTERN = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "EpgCSVParser - ";

    /**
     * patterns for start/finish EpgService Event
     */
    String epgServiceStarted = EPG_SERVICE_PATTERN.replace("{type}", "INFO ") + "SourceProviderImpl - \\[EPG-Service\\] ------------ Event started \\(#[0-9]+\\) ------------";
    String epgServiceFinished = EPG_SERVICE_PATTERN.replace("{type}", "INFO ") + "SourceProviderImpl - \\[EPG-Service\\] ------------ Event finished ------------";

    /**
     * patterns for EpgCSVParser
     */
    String startParsing = EPG_PARSER_PATTERN + "\\[parse\\] Starting epg source parsing...";
    String processedLines = EPG_PARSER_PATTERN + "Processed CSV lines: [0-9]+, accepted programs: [0-9]+; declined by unknown service programs: [0-9]+; duplicate programs: [0-9]+";
    String verifiedLines = EPG_PARSER_PATTERN + "\\[parse\\] Verified lines: [0-9]+; invalid parameters found: [0-9]+; ratio: [0-9]+";

    String startParsingPattern = EPG_PARSER_PATTERN + "\\[parse\\] Starting epg source parsing...";
    String processedLinesPattern = EPG_PARSER_PATTERN + "Processed CSV lines: " + allPrograms + ", accepted programs: " + acceptedPrograms + "; declined by unknown service programs: " + declinedPrograms + "; duplicate programs: " + duplicatePrograms;
    String verifiedLinesPattern = EPG_PARSER_PATTERN + "\\[parse\\] Verified lines: " + verifiedCsvLines + "; invalid parameters found: " + invalidParameters + "; ratio: " + ratio;

    String epgMissingData = EPG_SERVICE_PATTERN.replace("{type}", "ERROR") + "UrlSegmentProviderImpl - CORE\\-[0-9]{1,} Data file parsing error Data is missing from the EPG input file.";

    String verifierMissed = EPG_SERVICE_PATTERN.replace("{type}", "WARN ") + "EpgDataVerifier - EPG-0[0-9]{2} Missing description for program:";
    String verifierInvalid = EPG_SERVICE_PATTERN.replace("{type}", "WARN ") + "EpgDataVerifier - EPG-0[0-9]{2} Invalid .* field value: .*";

    String verifierInvalidPattern = EPG_SERVICE_PATTERN.replace("{type}", "WARN ") + "EpgDataVerifier - EPG-0[0-9]{2} Invalid " + fieldName + " field value: " + value;
    String verifierMissedPattern = EPG_SERVICE_PATTERN.replace("{type}", "WARN ") + "EpgDataVerifier - EPG-0[0-9]{2} Missing " + fieldName + " for program: \\[.*\\]";

    String duplicateData = EPG_PARSER_PATTERN + "\\[parse\\] duplicate data: EpgData";
    String duplicateDataPattern = EPG_PARSER_PATTERN + "\\[parse\\] duplicate data: EpgData .+";

    String verifierDateField = EPG_SERVICE_PATTERN.replace("{type}", "WARN ") + "EpgDataVerifier - EPG-0[0-9]{2} EPG data date is out of range:";
    String verifierDateFieldPattern = EPG_SERVICE_PATTERN.replace("{type}", "WARN ") + "EpgDataVerifier - EPG-0[0-9]{2} EPG data date is out of range: " + value;

    /**
     * patterns for temporary directory ZDB files
     */
    String tempZDBDirectory = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "SourceProviderImpl - Created temporary directory";
    //  String tempZDBDirectoryPattern = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "SourceProviderImpl - Created temporary directory " + path+ "/"+ tempDirZdb;
    String tempZDBDirectoryPattern = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "SourceProviderImpl - Created temporary directory " + tempDirZdb;

    /**
     * patterns for published directory ZDB files
     */
    String zdbDirectory = EPG_SERVICE_PATTERN.replace("{type}", "INFO ") + "PublisherUtils - Original file: ipg-[0-9]{10}.zdb.[0-9]{10}, Latest file: ipg-[0-9]{10}.zdb.[0-9]{10}.[0-9]{13}";
    String zdbDirectoryPattern = EPG_SERVICE_PATTERN.replace("{type}", "INFO ") + "PublisherUtils - Original file: ipg-" + date + "00.zdb.[0-9]{10}, Latest file: ipg-" + date + "00.zdb.[0-9]{10}.[0-9]{13}";
    String zdbHdrDirectory = EPG_SERVICE_PATTERN.replace("{type}", "INFO ") + "PublisherUtils - Original file: ipg-hdr.zdb.[0-9]{10}, Latest file: ipg-hdr.zdb.[0-9]{10}.[0-9]{13}";
    String zdbFileBuilder = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "ZdbSourceFileListBuilder - \\[buildSourceZdbFileList\\] accepted: ipg-[0-9]{10}.zdb, mask\\: .*";
    String zdbFileBuilderPattern = "ipg-" + date + "00.zdb, mask: .*";

    /**
     * patterns for DownloadParamsBuilderSegmentEpg
     */
    String segmentEpg = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "DownloadParamsBuilderSegmentEpg - \\[getRrequestUrlList\\] segmentUrl: " + "http://" + config.getEpgHeadendUrl() + ":8020/api/epg/filtering/\\?StartTime=([0-9]{4}-[0-9]{2}-[0-9]{2})%2000:00&EndTime=([0-9]{4}-[0-9]{2}-[0-9]{2})%2000:00";
    String segmentEpgPattern = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "DownloadParamsBuilderSegmentEpg - \\[getRrequestUrlList\\] segmentUrl: " + "http://" + url + "/api/epg/filtering/\\?StartTime=" + startTime + "%2000:00&EndTime=" + endTime + "%2000:00";
    String countRequests = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "UrlSegmentProviderImpl - \\[execute\\] requesting [0-9]+ segments";
    String countRequestsPattern = EPG_SERVICE_PATTERN.replace("{type}", "DEBUG") + "UrlSegmentProviderImpl - \\[execute\\] requesting " + amountSegments + " segments";

    /**
     * patterns for Connection refused case
     */
    String connectionRefused = EPG_SERVICE_PATTERN.replace("{type}", "ERROR") + "UrlSegmentProviderImpl - CORE\\-[0-9]{1,} Internal error\\: Connection refused \\(Connection refused\\)";


}