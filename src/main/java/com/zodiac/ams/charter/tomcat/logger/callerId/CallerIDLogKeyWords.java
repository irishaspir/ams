package com.zodiac.ams.charter.tomcat.logger.callerId;

public interface CallerIDLogKeyWords {

    String iD = "id";
    String callerName = "callerName";
    String phoneNumber = "phoneNumber";
    String callId = "callId";

    String CALLER_ID_SERVICE_PATTERN = "^([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}.[0-9]{3}) {type} ";
    String incomingCall = CALLER_ID_SERVICE_PATTERN.replace("{type}", "DEBUG") + "http-bio-8080-exec-[0-9]+/CallerIDRESTService - Incoming request to CallerId service. ID: .*, PhoneNumber: .*, CallerName: .*, CallId: .*.";
    String incomingCallPattern = CALLER_ID_SERVICE_PATTERN.replace("{type}", "DEBUG") + "http-bio-8080-exec-[0-9]+/CallerIDRESTService - Incoming request to CallerId service. ID: " + iD +", PhoneNumber: " + phoneNumber + ", CallerName: " + callerName +", CallId: "  + callId +".";
    String callTermination = CALLER_ID_SERVICE_PATTERN.replace("{type}", "DEBUG") + "http-bio-8080-exec-[0-9]+/CallerIDRESTService - Incoming request to dismiss call  ID: .*, CallId: .*.";
    String callTerminationPattern = CALLER_ID_SERVICE_PATTERN.replace("{type}", "DEBUG") + "http-bio-8080-exec-[0-9]+/CallerIDRESTService - Incoming request to dismiss call  ID: " + iD + ", CallId: " + callId +".";
    String moveToDBMode = CALLER_ID_SERVICE_PATTERN.replace("{type}", "INFO ")  + "Thread-[0-9]+/SubscribeManager - CID-[0-9]+. Subscription trouble. Move to DB mode.";
    String moveToSubscriptionMode = CALLER_ID_SERVICE_PATTERN.replace("{type}", "INFO ")  + "Thread-[0-9]+/SubscribeManager - CID-[0-9]+. Subscription success. Move to SUBSCRIBE mode.";
    String dbModeIsActive = CALLER_ID_SERVICE_PATTERN.replace("{type}", "DEBUG")  + "Thread-[0-9]+/DBDAOSubscription - Save subscription in DB. Count: [0-9]+";
  //  String dbModeIsActive = CALLER_ID_SERVICE_PATTERN.replace("{type}", "DEBUG")  + "Thread-[0-9]+/DBDAOSubscription - Save subscription in DB.+";

}