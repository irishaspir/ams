package com.zodiac.ams.charter.tomcat.command;


import com.zodiac.ams.charter.store.TomcatKeyWordStore;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.PATH_TO_ETC;

public class ServerXMLHelper extends XMLHelper {

    private static Config config = new Config();
    private static final String SERVER_XML = PATH_TO_ETC + TomcatKeyWordStore.SERVER_XML;


    public static void changeServiceInServerXML(String tag, String key, String value) {
        init(SERVER_XML);
        NodeList nodeList = root.getElementsByTagName(tag);
        Logger.debug("" + nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element el = (Element) nodeList.item(i);
            el.setAttribute(key, value);
        }
        save(SERVER_XML);
    }


}
