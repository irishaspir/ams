package com.zodiac.ams.charter.tomcat.logger.dsg;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.getVendorName;
import static com.zodiac.ams.charter.runner.TestRunner.AMS_VERSION;

import static com.zodiac.ams.charter.runner.TestRunner.AMS_VERSION;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.ALOHA_NAME;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.DAVIC_NAME;
import static com.zodiac.ams.charter.store.DsgDataKeyWordsStore.ConnectionMode.DOCSIS_NAME;


public class DsgLogParser extends DsgLogKeyWords {

    public boolean isPWUPWBInAMSLog(List<String> log, String id, String error) {
        List<String> rowList = getPWUPWBInAMSLog(log, id, error);
        return rowList.size() > 0;
    }

    public boolean isMigrationStartRowInAMSLog(List<String> log, String id, int flag) {
        return getMigrationStartRowInAMSLog(log, id, flag).size() != 0;
    }

    public boolean isConnectionModeIntRowInAMSLog(List<String> log, String id, Object connectionMode) {
        return getConnectionModeIntRowInAMSLog(log, id, connectionMode).size() != 0;
    }

    public boolean isMessageTypeRowInAMSLog(List<String> log, String id, int msgType) {
        return getMessageTypeRowInAMSLog(log, id, msgType).size() != 0;
    }

    public boolean isBoxModelRomIdRowInAMSLog(List<String> log, String id, String boxModelRomId) {
        return isInLog(log, id, boxModelRomId, BOX_MODEL_ROM_ID_PATTERN);
    }

    public boolean isVendorIdRowInAMSLog(List<String> log, String id, int value) {
        return isInLog(log, id, getVendorName(value).toUpperCase(), VENDOR_ID_PATTERN);
    }

    public boolean isVendorIdRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, VENDOR_ID_PATTERN);
    }

    public boolean isMessageTypeMapRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, MSG_TYPES_MAP_PATTERN);
    }

    public boolean isHubIDRowInAMSLog(List<String> log, String id, Object value) {
        return isInLog(log, id, value, HUB_ID_VCT_ID_PATTERN);
    }

    public boolean isCmMacAddressRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, CM_MAC_ADDRESS_PATTERN);
    }

    public boolean isSwVersionTmpInAMSLog(List<String> log, String id, String value) {
        return getRowByCondition(log, getTimeTreadRowLB(id) + String.format(SW_VERSION_TMP_PATTERN, value)).size() != 0;
    }

    public boolean isCmIpRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, CM_IP_PATTERN);
    }

    public boolean isSerialNumberRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, SERIAL_NUMBER_PATTERN);
    }

    public boolean isSwVersionRowInAMSLog(List<String> log, String id, String value) {
        String pattern = (AMS_VERSION < 390) ? SW_VERSION_PATTERN : NEW_SW_VERSION_PATTERN;
        return getRowByCondition(log, getTimeTreadRowLB(id) + String.format(pattern, value)).size() != 0;
    }

    public boolean isSdvServiceGroupIdRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, SDV_SERVICE_GROUP_ID_PATTERN);
    }

    public boolean isServiceGroupIDRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, SERVICE_GROUP_ID_PATTERN);
    }

    public boolean isTSIDRowInAMSLog(List<String> log, String id, String value) {
        return isInLog(log, id, value, TS_ID_PATTERN);
    }

    public boolean isLoadBalancerIpDRowInAMSLog(List<String> log, String id, Object value) {
        return isInLog(log, id, value, LOAD_BALANCER_IP_PATTERN);
    }

    public boolean isLoadBalancerIpDRowInAMSLogLB(List<String> log, String id, Object value) {
        return getRowByCondition(log, getTimeTreadRowLB(id) + String.format(LOAD_BALANCER_IP_PATTERN, value)).size() != 0;

    }

    public boolean isInvalidCacheLoaderExceptionInLog(List<String> log, String value) {
        return getInvalidCacheRowInAMSLog(log, value).size() != 0;
    }

    private boolean isInLog(List<String> log, String id, Object value, String pattern) {
        return getRowInAMSLog(log, id, value, pattern).size() != 0;
    }


    private List<String> getMigrationStartRowInAMSLog(List<String> log, String id, int flag) {
        String row;
        if (flag == 0) {
            row = String.format(MIGRATION_START_PATTERN, "false");
        } else {
            row = String.format(MIGRATION_START_PATTERN, "true");
        }
        return getRowByCondition(log, getTimeTreadRow(id) + row);
    }

    private List<String> getLoadBalancerRowAMSLog(List<String> log, String id, String error) {
        String row = error;
        return getRowByCondition(log, getTimeTreadRow(id) + row);
    }

    private List<String> getPWUPWBInAMSLog(List<String> log, String id, String error) {
        return getRowByCondition(log, getErrorRow(id) + error);
    }

    private List<String> getMessageTypeRowInAMSLog(List<String> log, String id, int messageId) {
        String row;
        if (messageId == 0)
            row = String.format(MSG_TYPE_PATTERN, "MODE_CHANGE");
        else
            row = String.format(MSG_TYPE_PATTERN, "MODE_CHANGE1");
        return getRowByCondition(log, getTimeTreadRow(id) + row);
    }

    private List<String> getConnectionModeIntRowInAMSLog(List<String> log, String id, Object connectionMode) {
        String row = "";
        int cM = (connectionMode instanceof Integer) ? (int) connectionMode : 0;
        if (cM == 0)
            row = String.format(CONNECTION_MODE_INT_PATTERN, DOCSIS_NAME.toUpperCase());
        else if (cM == 1)
            row = String.format(CONNECTION_MODE_INT_PATTERN, ALOHA_NAME.toUpperCase());
        else if (cM == 2)
            row = String.format(CONNECTION_MODE_INT_PATTERN, DAVIC_NAME.toUpperCase());
        return getRowByCondition(log, getTimeTreadRow(id) + row);
    }


    private List<String> getRowInAMSLog(List<String> log, String id, Object value, String pattern) {
        return getRowByCondition(log, getTimeTreadRow(id) + String.format(pattern, value));
    }

    private List<String> getInvalidCacheRowInAMSLog(List<String> log, String value) {
        return getRowByCondition(log, NEW_IO_WORKER_STRING + String.format(INVALID_CACHE_PATTERN, String.format(ERROR_CACHE_LOADER_PATTERN, value)));
    }


    private List<String> getRowByCondition(List<String> log, String row) {
        List<String> keyExpressions = new ArrayList<>();
        for (String lineLog : log) {
            if (Pattern.compile(row).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    private String getTimeTreadRow(String id) {
        return TIME_AND_TREAD_PATTERN + String.format(DSG_TREAD_PATTERN, id);
    }

    private String getTimeTreadRowLB(String id) {
        return TIME_AND_TREAD_PATTERN + String.format(DSG_TREAD_PATTERN2, id);
    }

    private String getErrorRow(String id) {
        return TIME_AND_TREAD_PATTERN + String.format(DSG_TREAD_PATTERN1, id);
    }


}
