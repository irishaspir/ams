package com.zodiac.ams.charter.tomcat.logger.cd;

/**
 * Created by SpiridonovaIM on 07.06.2017.
 */
public class CDLogKeyWords {

    protected static String TIME_PATTERN = "^([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}.[0-9]{3}) {type}  ";

    protected static String CD_REQUEST_PROCESSOR_PATTERN = "/CDRequestProcessor - ";

    public static String SOCKET_CLOSE = "Socket closed by client";
    public static String STB_ERROR = "STB returned error \\[%s\\]";
    public static String CD_RESPONSE_PATTERN =  "CD response content\\: CDResponse\\{id=%s, stbMacAddress=\\'%s\\', requestType=%s, errorCode=%s, params=\\[%s\\]\\}" ;
  //  public static String CONTENT_PATTERN = "CD request content: CDRequest\\{id=%s, stbMacAddress='%s', requestType=DVR_PLAYBACK_TRICKMODE\\}";
    public static String CONTENT_PATTERN = "CD request content: CDRequest";


    protected static String TREAD_PATTERN =  "pool-[0-9]{1,}-thread-[0-9]{1,}" ;

    //   2017-07-12 18:50:15.241 DEBUG pool-5-thread-44-FC0000000014/DsgMessageDecoder - [decodeMessage] SwVersion (int): 0


}
