package com.zodiac.ams.charter.tomcat.logger.rollback;

abstract class RollbackLogKeyWords {

    private static String getRollbackService(){
        return "^([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}.[0-9]{3}) {type} ";
    }

    public static String getKeywordStatus(){
        return "{status}";
    }

    public static String getKeywordMAC(){
        return "{MAC}";
    }

    public static String getKeywordResponseReceived(){
        return getRollbackService().replace("{type}", "DEBUG") + "pool-[0-9]+-thread-[0-9]+/SimpleSenderToStbImpl - " +
                "\\[responseReceived\\]\\[pool-[0-9]+-thread-[0-9]+-AppChanAuth-[a-zA-Z_0-9]{12}\\] " +
                "DOBTransportMessage\\{deliveryInfo=DOBDeliveryInfo\\{stbId='[a-zA-Z_0-9]{12}', .+ data=\\[0.+\\]\\}" ;
    }

    public static String getKeywordResponseReceivedPattern(){
        return getRollbackService().replace("{type}", "DEBUG") + "pool-[0-9]+-thread-[0-9]+/SimpleSenderToStbImpl - " +
                "\\[responseReceived\\]\\[pool-[0-9]+-thread-[0-9]+-AppChanAuth-" + getKeywordMAC() + "\\] "  +
                "DOBTransportMessage\\{deliveryInfo=DOBDeliveryInfo\\{stbId='" + getKeywordMAC() + "', .+ data=\\[0" + getKeywordStatus() + "\\]\\}" ;
    }

    public static String getKeywordEmptyBodyRequest() {
        return getRollbackService().replace("{type}", "WARN ") +
                "http-bio-8080-exec-[0-9]+-Rollback/RollbackJsonConverter - RB-007 JSON parsing error: empty STB list";
    }

    public static String getKeywordConnectionToCassandraPattern(){
        return getRollbackService().replace("{type}", "INFO ") + "pool-[0-9]+-thread-[0-9]+-" + getKeywordMAC() + "/DvrCassandraConnection - Connected to cluster: DVR";
    }

    public static String getKeywordConnectionToCassandra(){
        return getRollbackService().replace("{type}", "INFO ") + "pool-[0-9]+-thread-[0-9]+-[a-zA-Z_0-9]{12}/DvrCassandraConnection - Connected to cluster: DVR";
    }
}//2017-06-16 13:32:16.149 INFO  pool-5-thread-1-FC0000000013/Cluster - New Cassandra host /192.168.21.78:9042 added
   //     2017-06-16 13:32:16.149 INFO  pool-5-thread-1-FC0000000013/DvrCassandraConnection - Connected to cluster: DVR