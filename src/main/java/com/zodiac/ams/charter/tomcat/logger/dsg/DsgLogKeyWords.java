package com.zodiac.ams.charter.tomcat.logger.dsg;

/**
 * Created by SpiridonovaIM on 07.06.2017.
 */
public class DsgLogKeyWords {

    private static String DSG_TIME_PATTERN = "^([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}.[0-9]{3}) {type} ";
    private static String ROW_PATTERN = "{type}: %s";
    private static String ROW_PATTERN1 = "{type} %s";

    protected static String DSG_TREAD_PATTERN = "%s/DsgDecoderMsgType[0-3]{1} - \\[decode\\] ";
    protected static String DSG_TREAD_PATTERN2 = "%s/DsgDecoderMsgType[0-3]{1} - \\[decodeMessage\\] ";
    protected static String DSG_TREAD_PATTERN1 = "%s/DsgMessageDecoder - \\[decodeModeChangeNotification\\] ";

    public static String MSG_TYPE_PATTERN = ROW_PATTERN.replace("{type}", "MsgType");
    public static String CONNECTION_MODE_INT_PATTERN = ROW_PATTERN.replace("{type}", "connectionModeInt");
    public static String BOX_MODEL_ROM_ID_PATTERN = ROW_PATTERN.replace("{type}", "BoxModelRomId");
    public static String VENDOR_ID_PATTERN = ROW_PATTERN.replace("{type}", "VendorId");
    public static String MSG_TYPES_MAP_PATTERN = ROW_PATTERN.replace("{type}", "MsgTypesMap");
    public static String MIGRATION_START_PATTERN = ROW_PATTERN.replace("{type}", "MigrationStart");
    public static String HUB_ID_VCT_ID_PATTERN = ROW_PATTERN.replace("{type}", "HubIdVctId");
    public static String SERVICE_GROUP_ID_PATTERN = ROW_PATTERN.replace("{type}", "ServiceGroupId");
    public static String TS_ID_PATTERN = ROW_PATTERN.replace("{type}", "TsId");
    public static String LOAD_BALANCER_IP_PATTERN = ROW_PATTERN.replace("{type}", "LoadBalancerIp");
    public static String SW_VERSION_PATTERN = ROW_PATTERN.replace("{type}", "SwVersion");
    public static String NEW_SW_VERSION_PATTERN = ROW_PATTERN.replace("{type}", "SwVersion \\(int\\)");
    public static String SDV_SERVICE_GROUP_ID_PATTERN = ROW_PATTERN.replace("{type}", "SdvServiceGroupId");
    public static String CM_MAC_ADDRESS_PATTERN = ROW_PATTERN.replace("{type}", "CmMacAddress");
    public static String SERIAL_NUMBER_PATTERN = ROW_PATTERN.replace("{type}", "SerialNumber");
    public static String CM_IP_PATTERN = ROW_PATTERN.replace("{type}", "CmIp");
    public static String SW_VERSION_TMP_PATTERN = ROW_PATTERN1.replace("{type}", "SwVersionTmp");
    public static String INVALID_CACHE_PATTERN = ROW_PATTERN.replace("{type}", "InvalidCacheLoadException");
    public static String ERROR_VERSION3_PROTOCOL = "DSG-003 Decoding STB message error: null";
    public static String ERROR_CACHE_LOADER_PATTERN = "CacheLoader returned null for key %s.";

    protected static String TIME_AND_TREAD_PATTERN = DSG_TIME_PATTERN.replace("{type}", "DEBUG") + "pool-[0-9]{1,}-thread-[0-9]{1,}-";
    protected static String NEW_IO_WORKER_STRING = DSG_TIME_PATTERN.replace("{type}", "DEBUG") + "New I/O worker #[0-9]{1,}/LocalCachedStorage - \\[findMetadata\\] ";

 //   2017-07-12 18:50:15.241 DEBUG pool-5-thread-44-FC0000000014/DsgMessageDecoder - [decodeMessage] SwVersion (int): 0



}
