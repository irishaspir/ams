package com.zodiac.ams.charter.tomcat.logger.cd;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class CdLogParser extends CDLogKeyWords {


    public boolean isContentInLog(List<String> log, int messageId, String deviceId) {
        return isInLog("INFO", log, String.format(CONTENT_PATTERN, messageId, deviceId));
    }

    public boolean isResponseContentInLog(List<String> log, int messageId, String deviceId, String message, String code, String params) {
        String str=String.format(CD_RESPONSE_PATTERN, messageId, deviceId, message, code, params);

        return isInLog("INFO", log, String.format(CD_RESPONSE_PATTERN, messageId, deviceId, message, code, params));
    }

    public boolean isSocketClose(List<String> log) {
        return isInLog("INFO", log, SOCKET_CLOSE);
    }

    public boolean isSTBError(List<String> log, String error) {
        return isInLog("INFO", log, String.format(STB_ERROR, error));
    }

    private boolean isInLog(String mode, List<String> log, String pattern) {
        return getRowInAMSLog(mode, log, pattern).size() != 0;
    }


    private List<String> getRowInAMSLog(String mode, List<String> log, String pattern) {
        return getRowByCondition(log, getTimeTreadRow(mode) + pattern);
    }


    private List<String> getRowByCondition(List<String> log, String row) {
        List<String> keyExpressions = new ArrayList<>();
        for (String lineLog : log) {
            if (Pattern.compile(row).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }


    private String getTimeTreadRow(String mode) {
        return TIME_PATTERN.replace("{type}", mode) + TREAD_PATTERN + CD_REQUEST_PROCESSOR_PATTERN;
    }


}
