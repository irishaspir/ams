package com.zodiac.ams.charter.tomcat.logger.epg;

import com.zodiac.ams.charter.services.epg.EpgOptions;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.helpers.DateTimeHelper;
import com.zodiac.ams.common.logging.Logger;
import org.testng.Assert;

import java.util.*;
import java.util.regex.Pattern;

public class EpgLogParser {

    private Config config = new Config();
    private String url = config.getEpgHeadendUrl() + ":8020";
    private String encoded = "/encoded.wb_hdr";
    private String invalid = "invalid";
    private String missedField = "description";
    private String zeroAllPrograms = "0";
    private String zeroAcceptedPrograms = "0";
    private String zeroDeclainedPrograms = "0";
    private String zeroDuplicatePrograms = "0";
    private String tempDirZdb = EpgLogKeyWords.tempDirZdb;
    private String fieldName = EpgLogKeyWords.fieldName;
    private String fieldValue = EpgLogKeyWords.value;
    private String startTime = EpgLogKeyWords.startTime;
    private String endTime = EpgLogKeyWords.endTime;
    private String date = EpgLogKeyWords.date;
    private String urlAms = EpgLogKeyWords.url;
    private String zeroInvalidParameters = "0";
    private String zeroRatio = "0";
    private int ratioEqualsFour = 4;
    private String amountSegments = EpgLogKeyWords.amountSegments;

    private int countSegmentsFromServer = 14;//will be read from server.xml

    public List<String> getPublishedZdbFiles(List<String> logs) {
        List<String> zdbFiles = new ArrayList<>();
        for (int i = logs.size() - (countSegmentsFromServer + 1); i < logs.size() - 1; i++)
            zdbFiles.add(logs.get(i).substring(logs.get(i).length() - 43));
        zdbFiles.add(logs.get(logs.size() - 1).substring(logs.get(logs.size() - 1).length() - 36));
        zdbFiles.forEach(value -> Logger.info("ZDB: " + value));
        return zdbFiles;
    }

    public List<String> getLogAMSForValidData(List<String> log) {
        List<String> keyExpressions = new ArrayList<>();
        SortedSet<String> listZdb = new TreeSet<>();
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.epgServiceStarted).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.startParsing).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.processedLines).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.verifiedLines).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.epgServiceFinished).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.tempZDBDirectory).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.segmentEpg).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.countRequests).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.zdbFileBuilder).matcher(lineLog).find())
                listZdb.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.zdbDirectory).matcher(lineLog).find())
                listZdb.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.zdbHdrDirectory).matcher(lineLog).find())
                listZdb.add(lineLog);
        }
        for (String zdb : listZdb) {
            keyExpressions.add(zdb);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForConnectionRefused(List<String> log) {
        List<String> keyExpressions = new ArrayList<>();
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.epgServiceStarted).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.epgServiceFinished).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.segmentEpg).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(EpgLogKeyWords.countRequests).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            String str = EpgLogKeyWords.connectionRefused;
            if (Pattern.compile(EpgLogKeyWords.connectionRefused).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForMissedFiled(List<String> log) {
        List<String> keyExpressions = getLogAMSForValidData(log);
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.verifierMissed).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForMissedData(List<String> log) {
        List<String> keyExpressions = getLogAMSForValidData(log);
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.epgMissingData).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForInvalidFiled(List<String> log) {
        List<String> keyExpressions = getLogAMSForValidData(log);
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.verifierInvalid).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForInvalidDate(List<String> log) {
        List<String> keyExpressions = getLogAMSForValidData(log);
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.verifierDateField).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForDuplicateData(List<String> log) {
        List<String> keyExpressions = getLogAMSForValidData(log);
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.duplicateData).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    public List<String> getLogAMSForDuplicateFields(List<String> log) {
        List<String> keyExpressions = getLogAMSForValidData(log);
        for (String lineLog : log) {
            if (Pattern.compile(EpgLogKeyWords.epgMissingData).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return keyExpressions;
    }

    private Pattern createProcessedLinesPattern(List<String> parametersForParsingCsv) {
        return Pattern.compile(EpgLogKeyWords.processedLinesPattern
                .replace(EpgLogKeyWords.allPrograms, parametersForParsingCsv.get(0))
                .replace(EpgLogKeyWords.acceptedPrograms, parametersForParsingCsv.get(1))
                .replace(EpgLogKeyWords.declinedPrograms, parametersForParsingCsv.get(2))
                .replace(EpgLogKeyWords.duplicatePrograms, parametersForParsingCsv.get(3)));
    }

    private List<String> createPatternValidProcessedLinesParameters(int countRows, int countServices) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        return parametersForParsingCsv;
    }

    private List<Pattern> createPatternsListDuplicateFields(int countRows, int countServices, String tempZdbDir) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(zeroAcceptedPrograms);
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceStarted));
        patterns.add(Pattern.compile(EpgLogKeyWords.tempZDBDirectoryPattern.replace(tempDirZdb, tempZdbDir.substring(0, tempZdbDir.indexOf(encoded)))));
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.segmentEpgPattern
                    .replace(urlAms, url)
                    .replace(startTime, DateTimeHelper.getCurrentDayPlusAmountDays(i))
                    .replace(endTime, DateTimeHelper.getCurrentDayPlusAmountDays(i + 1))));
        patterns.add(Pattern.compile(EpgLogKeyWords.countRequestsPattern.replace(amountSegments, Integer.toString(countSegmentsFromServer))));
        patterns.add(Pattern.compile(EpgLogKeyWords.startParsingPattern));
        patterns.add(createProcessedLinesPattern(parametersForParsingCsv));
        patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceFinished));
        patterns.add(Pattern.compile(EpgLogKeyWords.epgMissingData));
        return patterns;
    }

    private List<Pattern> createPatternsListMissingData(String tempZdbDir) {
        ArrayList<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(zeroAllPrograms);
        parametersForParsingCsv.add(zeroAcceptedPrograms);
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        ArrayList<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceStarted));
        patterns.add(Pattern.compile(EpgLogKeyWords.tempZDBDirectoryPattern.replace(tempDirZdb, tempZdbDir.substring(0, tempZdbDir.indexOf(encoded)))));
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.segmentEpgPattern
                    .replace(urlAms, url)
                    .replace(startTime, DateTimeHelper.getCurrentDayPlusAmountDays(i))
                    .replace(endTime, DateTimeHelper.getCurrentDayPlusAmountDays(i + 1))));
        patterns.add(Pattern.compile(EpgLogKeyWords.countRequestsPattern.replace(amountSegments, Integer.toString(countSegmentsFromServer))));
        patterns.add(Pattern.compile(EpgLogKeyWords.startParsingPattern));
        patterns.add(createProcessedLinesPattern(parametersForParsingCsv));
        patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceFinished));
        patterns.add(Pattern.compile(EpgLogKeyWords.epgMissingData));
        return patterns;
    }

    private List<Pattern> createPatternsListValid(int countRows, int countServices, String tempZdbDir) {
        List<String> parametersForParsingCsv = createPatternValidProcessedLinesParameters(countRows, countServices);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(zeroInvalidParameters);
        parametersForParsingCsv.add(zeroRatio);
        return createPatternsList(tempZdbDir, parametersForParsingCsv);
    }

    private List<Pattern> createPatternsListMissedField(int countRows, int countServices, String tempZdbDir, String field) {
        List<String> parametersForParsingCsv = createPatternValidProcessedLinesParameters(countRows, countServices);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(ratioEqualsFour));
        List<Pattern> patterns = createPatternsList(tempZdbDir, parametersForParsingCsv);
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.verifierMissedPattern.replace(fieldName, field)));
        return patterns;
    }

    private List<Pattern> createPatternsListWithoutOptional(int countRows, int countServices, String tempZdbDir, String field) {
        List<String> parametersForParsingCsv = createPatternValidProcessedLinesParameters(countRows, countServices);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer * 2));
        parametersForParsingCsv.add(Integer.toString(8));
        List<Pattern> patterns = createPatternsList(tempZdbDir, parametersForParsingCsv);
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.verifierMissedPattern.replace(fieldName, field)));
        return patterns;
    }

    private List<Pattern> createPatternsListInvalidField(int countRows, int countServices, String tempZdbDir, String field, String value) {
        List<String> parametersForParsingCsv = createPatternValidProcessedLinesParameters(countRows, countServices);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(ratioEqualsFour));
        List<Pattern> patterns = createPatternsList(tempZdbDir, parametersForParsingCsv);
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.verifierInvalidPattern.replace(fieldName, field).replace(fieldValue, value)));
        return patterns;
    }

    private List<Pattern> createPatternsListInvalidDuration(int countRows, int countServices, String tempZdbDir, String field, String value) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(ratioEqualsFour));
        List<Pattern> patterns = createPatternsList(tempZdbDir, parametersForParsingCsv);
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.verifierInvalidPattern.replace(fieldName, field).replace(fieldValue, value)));
        return patterns;
    }

    private List<Pattern> createPatternsListInvalidFields(int countRows, int countServices, String tempZdbDir, String field[], String value[]) {
        List<String> parametersForParsingCsv = createPatternValidProcessedLinesParameters(countRows, countServices);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer * 2));
        parametersForParsingCsv.add(Integer.toString(ratioEqualsFour * 2));
        List<Pattern> patterns = createPatternsList(tempZdbDir, parametersForParsingCsv);
        for (int i = 0; i < countSegmentsFromServer; i++)
            for (int j = 0; j < field.length; j++)
                patterns.add(Pattern.compile(EpgLogKeyWords.verifierInvalidPattern.replace(fieldName, field[j]).replace(fieldValue, value[j])));
        return patterns;
    }

    private List<Pattern> createPatternsListInvalidTime(int countRows, int countServices, String tempZdbDir, String field, String value) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroInvalidParameters);
        parametersForParsingCsv.add(zeroRatio);
        return createPatternsList(tempZdbDir, parametersForParsingCsv);
    }

    private List<Pattern> createPatternsListInvalidDate(int countRows, int countServices, String tempZdbDir, String value) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroInvalidParameters);
        parametersForParsingCsv.add(zeroRatio);
        return createPatternsList(tempZdbDir, parametersForParsingCsv);
    }

    private List<Pattern> createPatternsListInvalidServiceId(int countRows, int countServices, String tempZdbDir, String field, String value) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer));
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroInvalidParameters);
        parametersForParsingCsv.add(zeroRatio);
        return createPatternsList(tempZdbDir, parametersForParsingCsv);
    }

    private List<Pattern> createPatternsListDummy(int countRows, int countServices, String tempZdbDir) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(zeroDuplicatePrograms);
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices - countSegmentsFromServer));
        parametersForParsingCsv.add(zeroInvalidParameters);
        parametersForParsingCsv.add(zeroRatio);
        return createPatternsList(tempZdbDir, parametersForParsingCsv);
    }

    private List<Pattern> createPatternsListDuplicateTime(int countRows, int countServices, String tempZdbDir) {
        List<String> parametersForParsingCsv = new ArrayList<>();
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices + countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices));
        parametersForParsingCsv.add(zeroDeclainedPrograms);
        parametersForParsingCsv.add(Integer.toString(countSegmentsFromServer));
        parametersForParsingCsv.add(Integer.toString(countRows * countSegmentsFromServer * countServices + countSegmentsFromServer));
        parametersForParsingCsv.add(zeroInvalidParameters);
        parametersForParsingCsv.add(zeroRatio);
        List<Pattern> patterns = createPatternsList(tempZdbDir, parametersForParsingCsv);
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.duplicateDataPattern));
        return patterns;
    }

    private List<Pattern> createPatternsListConnectionRefused() {
        ArrayList<Pattern> patterns = new ArrayList<>();
        for (int j = 0; j < 2; j++) {
            patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceStarted));
            for (int i = 0; i < countSegmentsFromServer; i++)
                patterns.add(Pattern.compile(EpgLogKeyWords.segmentEpgPattern
                        .replace(urlAms, url)
                        .replace(startTime, DateTimeHelper.getCurrentDayPlusAmountDays(i))
                        .replace(endTime, DateTimeHelper.getCurrentDayPlusAmountDays(i + 1))));
            patterns.add(Pattern.compile(EpgLogKeyWords.countRequestsPattern.replace(amountSegments, Integer.toString(countSegmentsFromServer))));
            patterns.add(Pattern.compile(EpgLogKeyWords.connectionRefused));
            patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceFinished));
        }
        return patterns;
    }


    private List<Pattern> createPatternsList(String tempZdbDir, List<String> parametersForParsingCsv) {
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceStarted));
        patterns.add(Pattern.compile(EpgLogKeyWords.tempZDBDirectoryPattern.replace(tempDirZdb, tempZdbDir.substring(0, tempZdbDir.indexOf(encoded)))));
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.segmentEpgPattern
                    .replace(urlAms, url)
                    .replace(startTime, DateTimeHelper.getCurrentDayPlusAmountDays(i))
                    .replace(endTime, DateTimeHelper.getCurrentDayPlusAmountDays(i + 1))));
        patterns.add(Pattern.compile(EpgLogKeyWords.countRequestsPattern.replace(amountSegments, Integer.toString(countSegmentsFromServer))));
        patterns.add(Pattern.compile(EpgLogKeyWords.startParsingPattern));
        patterns.add(createProcessedLinesPattern(parametersForParsingCsv));
        patterns.add(Pattern.compile(EpgLogKeyWords.verifiedLinesPattern
                .replace(EpgLogKeyWords.verifiedCsvLines, parametersForParsingCsv.get(4))
                .replace(EpgLogKeyWords.invalidParameters, parametersForParsingCsv.get(5))
                .replace(EpgLogKeyWords.ratio, parametersForParsingCsv.get(6))));
        patterns.add(Pattern.compile(EpgLogKeyWords.epgServiceFinished));
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.zdbFileBuilderPattern.replace(date, DateTimeHelper.getCurrentDayPlusCountStrForZDB(i))));
        for (int i = 0; i < countSegmentsFromServer; i++)
            patterns.add(Pattern.compile(EpgLogKeyWords.zdbDirectoryPattern
                    .replace(date, DateTimeHelper.getCurrentDayPlusCountStrForZDB(i))
                    .replace(date, DateTimeHelper.getCurrentDayPlusCountStrForZDB(i))));
        patterns.add(Pattern.compile(EpgLogKeyWords.zdbHdrDirectory));
        return patterns;
    }

    private boolean compareLogAMSWithPatternConnectionRefused(List<String> log, List<Pattern> patterns) {
        for (String logLine : log)
            Logger.info("LOG EPG: " + logLine);
        if (log.size() != patterns.size()) {
            Assert.assertEquals(log, patterns);
            try {
                throw new Exception("Pattern log's size doesn't equals AMS log's size");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        boolean verified = false;
        for (int i = 0; i < log.size(); i++) {
            if (patterns.get(i).matcher(log.get(i)).find())
                verified = true;
            else {
                verified = false;
                Logger.info("Log from AMS: " + log.get(i) + "\n");
                Logger.info("Pattern log: " + patterns.get(i) + "\n");
                return verified;
            }
        }
        return verified;
    }

    private boolean compareLogAMSWithPattern(List<String> log, List<Pattern> patterns) {
        for (String logLine : log)
            Logger.info("LOG EPG: " + logLine);
        if (log.size() != patterns.size()) {
            Assert.assertEquals(log, patterns, "Different between log and pattern: ");
            try {
                throw new Exception("Pattern log's size doesn't equals AMS log's size");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        boolean verified = false;
        ArrayList<String> aceeptedZdbList = new ArrayList<>();
        ArrayList<String> aceeptedZdbListPattern = new ArrayList<>();
        for (int i = 0; i < 7 + countSegmentsFromServer; i++) {
            if (patterns.get(i).matcher(log.get(i)).find())
                verified = true;
            else {
                verified = false;
                Logger.info("Log from AMS: " + log.get(i) + "\n");
                Logger.info("Pattern log: " + patterns.get(i) + "\n");
                return verified;
            }
        }
        if (patterns.size() != 7 + countSegmentsFromServer) {
            for (int i = 0; i < countSegmentsFromServer; i++)
                aceeptedZdbListPattern.add(EpgLogKeyWords.zdbFileBuilderPattern.replace(date, DateTimeHelper.getCurrentDayPlusCountStrForZDB(i)));
            for (int i = 7 + countSegmentsFromServer; i < 7 + countSegmentsFromServer + countSegmentsFromServer; i++)
                aceeptedZdbList.add(log.get(i).substring(log.get(i).indexOf("ipg")));
            if (aceeptedZdbList.containsAll(aceeptedZdbListPattern))
                verified = true;
            else {
                verified = false;
                return verified;
            }
        }
        if (patterns.size() != 7 + countSegmentsFromServer + countSegmentsFromServer)
            for (int i = 7 + countSegmentsFromServer + countSegmentsFromServer; i < patterns.size(); i++) {
                if (patterns.get(i).matcher(log.get(i)).find())
                    verified = true;
                else {
                    verified = false;
                    Logger.info("Log from AMS: " + log.get(i) + "\n");
                    Logger.info("Pattern log: " + patterns.get(i) + "\n");
                    break;
                }
            }

        return verified;
    }

    public boolean compareLogAMSWithPatternConnectionRefused(List<String> log) {
        List<Pattern> patterns = createPatternsListConnectionRefused();
        return compareLogAMSWithPatternConnectionRefused(log, patterns);
    }

    public boolean compareLogAMSWithPatternMissingData(List<String> log, String tempZdbDir) {
        List<Pattern> patterns = createPatternsListMissingData(tempZdbDir);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternDuplicateFields(List<String> log, int countRows, int countServices, String tempZdbDir) {
        List<Pattern> patterns = createPatternsListDuplicateFields(countRows, countServices, tempZdbDir);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternDuplicateTime(List<String> log, int countRows, int countServices, String tempZdbDir) {
        List<Pattern> patterns = createPatternsListDuplicateTime(countRows, countServices, tempZdbDir);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternDummy(List<String> log, int countRows, int countServices, String tempZdbDir) {
        List<Pattern> patterns = createPatternsListDummy(countRows, countServices, tempZdbDir);
        return compareLogAMSWithPattern(log, patterns);
    }

    public List<Pattern> createPattern(int countRows, int countServices, String tempZdbDir) {
        return createPatternsListValid(countRows, countServices, tempZdbDir);
    }

    public boolean compareLogAMSWithPatternValid(List<String> log, List<Pattern> patterns) {
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternValid(List<String> log, int countRows, int countServices, String tempZdbDir) {
        List<Pattern> patterns = createPatternsListValid(countRows, countServices, tempZdbDir);
        return compareLogAMSWithPattern(log, patterns);
    }

    private boolean compareLogAMSWithPatternMissedValue(List<String> log, int countRows, int countServices, String tempZdbDir, String field) {
        List<Pattern> patterns = createPatternsListMissedField(countRows, countServices, tempZdbDir, field);
        return compareLogAMSWithPattern(log, patterns);
    }

    private boolean compareLogAMSWithPatternMissedOptional(List<String> log, int countRows, int countServices, String tempZdbDir, String field) {
        List<Pattern> patterns = createPatternsListWithoutOptional(countRows, countServices, tempZdbDir, field);
        return compareLogAMSWithPattern(log, patterns);
    }

    private boolean compareLogAMSWithPatternInvalidValue(List<String> log, int countRows, int countServices, String tempZdbDir, String field, String value) {
        List<Pattern> patterns = createPatternsListInvalidField(countRows, countServices, tempZdbDir, field, value);
        return compareLogAMSWithPattern(log, patterns);
    }

    private boolean compareLogAMSWithPatternInvalidValues(List<String> log, int countRows, int countServices, String tempZdbDir, String field[], String value[]) {
        List<Pattern> patterns = createPatternsListInvalidFields(countRows, countServices, tempZdbDir, field, value);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternInvalidServiceId(List<String> log, int countRows, int countServices, String tempZdbDir, String value) {
        List<Pattern> patterns = createPatternsListInvalidServiceId(countRows, countServices, tempZdbDir, EpgOptions.SERVICE_ID.getOption().getColumnName(), value);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternInvalidTime(List<String> log, int countRows, int countServices, String tempZdbDir, String value) {
        List<Pattern> patterns = createPatternsListInvalidTime(countRows, countServices, tempZdbDir, EpgOptions.TIME.getOption().getColumnName(), value);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternInvalidDate(List<String> log, int countRows, int countServices, String tempZdbDir, String value) {
        List<Pattern> patterns = createPatternsListInvalidDate(countRows, countServices, tempZdbDir, value);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternInvalidTVRating(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.TV_RATING.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidMPAARating(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.MPAA_RATING.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidAdvisory(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.ADVISORY.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidProgramId(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.PROGRAM_ID.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidYear(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.YEAR.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidHometeamid(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.HOMETEAMID.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidAwayteamid(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.AWAYTEAMID.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidDuration(List<String> log, int countRows, int countServices, String tempZdbDir, String value) {
        List<Pattern> patterns = createPatternsListInvalidDuration(countRows, countServices, tempZdbDir, EpgOptions.DURATION.getOption().getColumnName(), value);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternInvalidSeason(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.SEASON.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidEpisode(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.EPISODE.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidDvs(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.DVS.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidCc(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.CC.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidSap(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.SAP.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidSurround(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.SURROUND.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidStereo(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.STEREO.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidLive(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.LIVE.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidHd(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.HD.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidIsSeries(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.IS_SERIES.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidRerun(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.RERUN.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidNew(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValue(log, countRows, countServices, tempZdbDir, EpgOptions.NEW.getOption().getColumnName(), invalid);
    }

    public boolean compareLogAMSWithPatternInvalidNewInvalidRerun(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternInvalidValues(log, countRows, countServices, tempZdbDir,
                new String[]{EpgOptions.RERUN.getOption().getColumnName(), EpgOptions.NEW.getOption().getColumnName()},
                new String[]{invalid, invalid});
    }

    public boolean compareLogAMSWithPatternMissedDescription(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternMissedValue(log, countRows, countServices, tempZdbDir, missedField);
    }

    public boolean compareLogAMSWithPatternWithouOptional(List<String> log, int countRows, int countServices, String tempZdbDir) {
        return compareLogAMSWithPatternMissedOptional(log, countRows, countServices, tempZdbDir, missedField);
    }
}
