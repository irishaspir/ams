package com.zodiac.ams.charter.tomcat.logger.rollback;

import com.zodiac.ams.common.logging.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.store.TestKeyWordStore.DEVICE_ID_NUMBER_2;

public class RollbackLogParser {

    private String SUCCESS = "0";
    private String ERROR = "1";

    public List<String> getLogAMSForValid(List<String> log) {
        List<String> keyExpressions = new ArrayList<>();
        for (String lineLog : log) {
            if (Pattern.compile(RollbackLogKeyWords.getKeywordResponseReceived()).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(RollbackLogKeyWords.getKeywordEmptyBodyRequest()).matcher(lineLog).find())
                keyExpressions.add(lineLog);
            if (Pattern.compile(RollbackLogKeyWords.getKeywordConnectionToCassandra()).matcher(lineLog).find())
                keyExpressions.add(lineLog);
        }
        return  keyExpressions;
    }

    private List<Pattern> createPatternsList( String status, String ... macs){
        List<Pattern> patterns = new ArrayList<>();
        for (String mac:macs)
        patterns.add(Pattern.compile(RollbackLogKeyWords.getKeywordResponseReceivedPattern()
                .replace(RollbackLogKeyWords.getKeywordMAC(), mac)
                .replace(RollbackLogKeyWords.getKeywordStatus(), status)));
        return patterns;
    }

    private List<Pattern> createPatternsListDSGIntegration(String ... macs){
        List<Pattern> patterns = new ArrayList<>();
        for (String mac:macs)
            patterns.add(Pattern.compile(RollbackLogKeyWords.getKeywordConnectionToCassandraPattern()
                    .replace(RollbackLogKeyWords.getKeywordMAC(), mac)));
        return patterns;
    }

    private List<Pattern> createPatternsListEmptyBody(){
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(RollbackLogKeyWords.getKeywordEmptyBodyRequest()));
        return patterns;
    }

    public boolean compareLogAMSWithPatternStatusSuccess(List<String> log, String ... macs){
       if (log.size()==2 & log.get(0).contains(DEVICE_ID_NUMBER_2))
           Collections.swap(log, 0, 1);
        List<Pattern> patterns = createPatternsList(SUCCESS, macs);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternStatusError(List<String> log, String ... macs){
        if (log.size()==2 & log.get(0).contains(DEVICE_ID_NUMBER_2))
            Collections.swap(log, 0, 1);
        List<Pattern> patterns = createPatternsList(ERROR, macs);
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternEmptyJsonBody(List<String> log){
        List<Pattern> patterns = createPatternsListEmptyBody();
        return compareLogAMSWithPattern(log, patterns);
    }

    public boolean compareLogAMSWithPatternDSGIntegration(List<String> log, String mac){
        List<Pattern> patterns = createPatternsListDSGIntegration(mac);
        return compareLogAMSWithPattern(log, patterns);
    }

    private boolean compareLogAMSWithPattern(List<String> log, List<Pattern> patterns){
        for (String logLine: log)
            Logger.info("LOG ROLLBACK: " + logLine);
        if (log.size()!=patterns.size()) {
            try {
                throw new Exception("Pattern log's size doesn't equals AMS log's size");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        boolean verified = false;
        List<String> errorLog = new ArrayList<>();
        List<Pattern> errorPattern = new ArrayList<>();
        for (int i=0;i< log.size();i++) {
            if (!patterns.get(i).matcher(log.get(i)).find()) {
                errorLog.add(log.get(i));
                errorPattern.add(patterns.get(i));
            }
        }
        if (errorLog.size()!=0) {
            for (int i=0;i< errorLog.size();i++) {
                Logger.info("Log from AMS: " + errorLog.get(i) + "\n");
                Logger.info("Pattern log: " + errorPattern.get(i) + "\n");
            }
        }
        else verified = true;
        return verified;
    }
}
