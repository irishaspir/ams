package com.zodiac.ams.charter.runner;

import com.zodiac.ams.charter.bd.ams.tables.STBLastActivityUtils;
import com.zodiac.ams.charter.bd.ams.tables.STBMetadataUtils;
import com.zodiac.ams.charter.bd.ams.tables.SettingsSyncV2Utils;
import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;

import static com.zodiac.ams.charter.bd.ams.tables.MacIpUtils.deleteMacInMacIp;
import static com.zodiac.ams.charter.bd.ams.tables.MacIpUtils.insertRowToMacIp;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBDataV2Utils.*;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.deleteRowFromSettingsSTB;
import static com.zodiac.ams.charter.bd.ams.tables.SettingsSTBUtils.insertRowToSettingsSTB;
import static com.zodiac.ams.charter.runner.Utils.isLocalIp;
import static com.zodiac.ams.charter.store.TestKeyWordStore.*;
import static com.zodiac.ams.common.helpers.Utils.ipToInt;


public class DBDataBuilder {

    private final String MIGRATION_FLAG_ONE = "1";
    private final String MODEL_ID_NULL = "0";

    public void fillDb() {
        Logger.info("Insert STB data in BD");
        String ip;
        if (isLocalIp()) {
            ip = "192.168.25.133";
        } else
            ip = new Config().getAmsIp();
        String ipInIntForm = String.valueOf(ipToInt(ip));
        Logger.info("Insert row in DB with ip = " + ip);

        cleanDb();
        insertToMacIp(ipInIntForm, ip);
        insertToSettingsSTB();
        insertToSettingsSTBDataV2();
    }

    private void cleanDb() {
        deleteKeysFromSettingsSTBDataV2ByMAC(MAC_NUMBER_1);
        deleteKeysFromSettingsSTBDataV2ByMAC(MAC_NUMBER_2);
        deleteKeysFromSettingsSTBDataV2ByMAC(MAC_NUMBER_3);
        deleteKeysFromSettingsSTBDataV2ByMAC(MAC_NUMBER_4);

        deleteRowFromSettingsSTB(MAC_NUMBER_1);
        deleteRowFromSettingsSTB(MAC_NUMBER_2);
        deleteRowFromSettingsSTB(MAC_NUMBER_3);
        deleteRowFromSettingsSTB(MAC_NUMBER_4);

        deleteMacInMacIp(MAC_NUMBER_1);
        deleteMacInMacIp(MAC_NUMBER_2);
        deleteMacInMacIp(MAC_NUMBER_3);
        deleteMacInMacIp(MAC_NUMBER_4);

        new SettingsSyncV2Utils().deleteAll();
        new STBMetadataUtils().deleteAll();
        new STBLastActivityUtils().deleteAll();
    }

    private void insertToMacIp(String ipInIntForm, String ip) {
        insertRowToMacIp(MAC_NUMBER_1, ipInIntForm, DEVICE_ID_NUMBER_1, ip);
        insertRowToMacIp(MAC_NUMBER_2, ipInIntForm, DEVICE_ID_NUMBER_2, ip);
        insertRowToMacIp(MAC_NUMBER_3, ipInIntForm, DEVICE_ID_NUMBER_3, ip);
        insertRowToMacIp(MAC_NUMBER_4, ipInIntForm, DEVICE_ID_NUMBER_4, ip);
    }

    private void insertToSettingsSTB() {
        insertRowToSettingsSTB(MAC_NUMBER_1, DEVICE_ID_NUMBER_1, MODEL_ID_NULL, MIGRATION_FLAG_ONE);
        insertRowToSettingsSTB(MAC_NUMBER_2, DEVICE_ID_NUMBER_2, MODEL_ID_NULL, MIGRATION_FLAG_ONE);
        insertRowToSettingsSTB(MAC_NUMBER_3, DEVICE_ID_NUMBER_3, MODEL_ID_NULL, MIGRATION_FLAG_ONE);
        insertRowToSettingsSTB(MAC_NUMBER_4, DEVICE_ID_NUMBER_4, MODEL_ID_NULL, MIGRATION_FLAG_ONE);
    }

    private void insertToSettingsSTBDataV2() {
        insertMACToSettingsSTBDataV2(MAC_NUMBER_1);
        setDataTypeIdInSettingsSTBDataV2(MAC_NUMBER_1);
        insertMACToSettingsSTBDataV2(MAC_NUMBER_2);
        setDataTypeIdInSettingsSTBDataV2(MAC_NUMBER_2);
        insertMACToSettingsSTBDataV2(MAC_NUMBER_3);
        setDataTypeIdInSettingsSTBDataV2(MAC_NUMBER_3);
        insertMACToSettingsSTBDataV2(MAC_NUMBER_4);
        setDataTypeIdInSettingsSTBDataV2(MAC_NUMBER_4);
    }
}
