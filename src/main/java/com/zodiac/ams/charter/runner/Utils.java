package com.zodiac.ams.charter.runner;

import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by SpiridonovaIM on 27.06.2017.
 */
public class Utils {

    private static int i = 0;
    private static List<String> macList = getMacRange();
    protected static String mac;
    private static boolean isLocalIp;


    private static List<String> getMacRange() {
        List<String> macList = new ArrayList<>();
        String[] firstAndLastMac = new Config().getValidMacRange().split("-");
        for (long i = Long.parseLong(firstAndLastMac[0]); i < Long.parseLong(firstAndLastMac[1]); i++) {
            macList.add(String.valueOf(i));
        }
        return macList;
    }

    public static String getMacFromRange() {
        return macList.get(i++);
    }

    public static boolean isLocalIp() {
        return isLocalIp;
    }

    public static void defineIp() {
        isLocal(getIp());
    }

    public static List getIp() {
        Logger.info("Get IP!!! ");
        List<String> ip = new ArrayList<>();
        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                ip.add(i.getHostAddress());
            }
        }
        return ip;
    }

    private static void isLocal(List<String> ip) {
        boolean x = false;

        for (int i = 0; i < ip.size(); i++) {
            if (ip.get(i).contains("192.168.25.")) {
                x = true;
                i = ip.size();
            }
        }
        isLocalIp = x;
        Logger.info("isLocalIp!!! " + isLocalIp);
    }

}
