package com.zodiac.ams.charter.runner;

import com.zodiac.ams.common.logging.Logger;
import org.testng.IExecutionListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;

import org.testng.ITestResult;
import org.testng.internal.IResultListener;

public class AllureTestListener implements IResultListener, ISuiteListener, IExecutionListener {
    
    @Override
    public void onConfigurationSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onConfigurationFailure(ITestResult iTestResult) {

    }

    @Override
    public void onConfigurationSkip(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ISuite iSuite) {
        Logger.info("AllureTestListener: onStart Suite={}",iSuite.getName());
    }

    @Override
    public void onFinish(ISuite iSuite) {
        Logger.info("AllureTestListener: onFinish Suite={}",iSuite.getName());
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        Logger.info("AllureTestListener: onTestStart Test={}",iTestResult.getName());
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Logger.info("AllureTestListener: onTestSuccess Test={}",iTestResult.getName());

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Logger.info("AllureTestListener: onTestFailure Test={}",iTestResult.getName());

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Logger.info("AllureTestListener: onTestSkipped Test={}",iTestResult.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        Logger.info("AllureTestListener: onStart TestContext={}",iTestContext.getName());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        Logger.info("AllureTestListener: onFinish TestContext={}",iTestContext.getName());
    }

    @Override
    public void onExecutionStart() {
        Logger.info("AllureTestListener: onExecutionStart");
    }

    @Override
    public void onExecutionFinish() {
        Logger.info("AllureTestListener: onExecutionFinish");
        new Thread(){
            @Override
            public void run(){
                try{
                    Thread.sleep(10_000);
                }
                catch(Exception ex){
                }
                finally{
                    System.exit(0);
                }
            }
        }.start();
    }
}
