package com.zodiac.ams.charter.runner;

import com.zodiac.ams.common.logging.Logger;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.zodiac.ams.charter.tomcat.command.TomcatHelper.*;
import static com.zodiac.ams.common.bd.DBHelper.disconnect;
import static java.lang.Thread.sleep;

public class TestRunner {

    private static final String START_SUIT_XML = "start_suit.xml";
  //  private static final String SUITS_MAIN_SUIT_XML = "suits/main_suit.xml";
    private static final String LOCAL_FOLDER = "suits/";
    private static final String TESTNG_SUIT = "testng.xml";
    public static int AMS_VERSION = 400;
    private static GlobalConfig globalConfig = new GlobalConfig();


    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, InterruptedException {
        globalConfig.setEnvironment(TestRunner.class.getClassLoader().getResource(LOCAL_FOLDER), LOCAL_FOLDER);
        tomcatStart();
        AMS_VERSION = (globalConfig.getAMSVersion() != 0) ? globalConfig.getAMSVersion() : 390;
        Logger.debug("!!!!! BUILD NUMBER: " + AMS_VERSION);
        parseAMSLog();
        new TestRunner().runTests(args);
        tomcatStop();
        stopProcess();
        disconnect();
    }


    private void runTests(String[] args) throws IOException, SAXException, ParserConfigurationException {
        Collection<XmlSuite> allSuits = new ArrayList<>();
        Collection<XmlSuite> suitList;
        TestNG testNG = new TestNG();
        testNG.addListener((Object) new AllureTestListener());
        if (null == args || 0 == args.length) {
            Logger.info("Default suit: " + TESTNG_SUIT);
            testNG.setTestSuites(getTestOrder());
        } else {
            Collection<XmlSuite> suits = new ArrayList<>();
            List<String> suitsListFromCmd = new ArrayList<>();
            suitsListFromCmd.add(START_SUIT_XML);
            if (args[0].trim().contains(","))
                suitsListFromCmd.addAll(Arrays.asList(args[0].trim().split(",")));
            else suitsListFromCmd.addAll(Arrays.asList(args[0].trim()));
            for (String aSuitsListFromCmd : suitsListFromCmd) {
                Logger.info("Suit: " + aSuitsListFromCmd);
                suitList = new Parser(TestRunner.class.getClassLoader().getResourceAsStream(LOCAL_FOLDER + aSuitsListFromCmd)).parse();
                suits.addAll(suitList);
            }
            allSuits.addAll(sortAllSuits(getTestOrder(), (List<XmlSuite>) suits));
            testNG.setXmlSuites((List<XmlSuite>) allSuits);
        }
        testNG.run();
    }

    //this methods will be deleted when we can stop the CHE emulator thread
    private static void stopProcess() throws InterruptedException {
        Logger.debug("Tests finished!!!");
        sleep(30000);
        System.exit(0);
    }

    private static List<String> getTestOrder() throws IOException, SAXException, ParserConfigurationException {
        List<XmlSuite> main = (List<XmlSuite>) new Parser(TestRunner.class.getClassLoader().getResourceAsStream(TESTNG_SUIT)).parse();
        return main.get(0).getSuiteFiles();
    }

    private static List<XmlSuite> sortAllSuits(List<String> suits, List<XmlSuite> allSuits) {
        List<XmlSuite> resultSuits = new ArrayList<>();
        for (int i = 0; i < suits.size(); i++) {
            for (int j = 0; j < allSuits.size(); j++) {
                if (suits.get(i).contains(allSuits.get(j).getName())) {
                    resultSuits.add(allSuits.get(j));
                }
            }
        }
        return resultSuits;
    }
}
