package com.zodiac.ams.charter.runner;

import com.zodiac.ams.common.common.Config;
import com.zodiac.ams.common.logging.Logger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zodiac.ams.charter.store.TomcatKeyWordStore.*;
import static com.zodiac.ams.charter.tomcat.command.ContextXMLHelper.setContext;
import static com.zodiac.ams.charter.tomcat.command.TomcatHelper.copyContextXML;
import static com.zodiac.ams.charter.tomcat.command.TomcatHelper.copyServerXML;
import static com.zodiac.ams.charter.tomcat.command.TomcatHelper.copyTestNG;
import static java.nio.file.Files.readAllLines;

class GlobalConfig {

    private static String JAR_NAME_PATTERN = "(charter.{0,}\\-[0-9]{1,}\\.[0-9]{1,}.{0,}\\.jar)";

    void setEnvironment(URL jarName, String localFolder) throws IOException {
        Logger.info("USER HOME " + System.getProperty(USER_HOME));
        Logger.info("USER NAME " + System.getProperty(USER_NAME));
        Logger.info("USER FOLDER " + System.getProperty(USER_DIR));
        for (String one : getSuitList(getJarName(jarName), localFolder)) {
            copyFile(one);
        }
        copyFile(PATH_TO_EPG_DATA + LINEUP_FILE_NAME_FOR_CHECKER, PATH_TO_ETC);
        copyFile(PATH_TO_DATA + SAMPLE_XML, PATH_TO_ETC);
        copyFile(PATH_TO_DATA + TESTCASE_XML, PATH_TO_ETC);
        copyFile(PATH_TO_CHE_DATA + LINEUP_JSON, PATH_TO_ETC);
        copyFile(PATH_TO_CHE_DATA + SETTINGS_DESCRIPTION_SCHEMA_XSD, PATH_TO_ETC);
        copyFile(PATH_TO_ETC + CONTEXT_XML);
        copyFile(PATH_TO_ETC + SERVER_XML);
        copyFile(PATH_TO_DSG_DATA + VENDORS_TXT, PATH_TO_ETC);
        copyFile(PATH_TO_DSG_DATA + STB_MODELS_TXT, PATH_TO_ETC);
        copyFile(PATH_TO_DSG_DATA + ROM_ID_TXT, PATH_TO_ETC);
        copyFile(PATH_TO_PROPERTIES + AMS_PROPERTIES);
        updateProperties(PATH_TO_PROPERTIES + AMS_PROPERTIES);
        copyFile(PATH_TO_PROPERTIES + CHE_PROPERTIES);
        updateProperties(PATH_TO_PROPERTIES + CHE_PROPERTIES);
        copyFile(PATH_TO_PROPERTIES + STB_PROPERTIES);
        updateProperties(PATH_TO_PROPERTIES + STB_PROPERTIES);
//        copyFile(EPG_VERIFIER_PROPERTIES, PATH_TO_PROPERTIES);
//        updateProperties(EPG_VERIFIER_PROPERTIES);
        copyFile(PATH_TO_PROPERTIES + FT_PROPERTIES);
        updateProperties(PATH_TO_PROPERTIES + FT_PROPERTIES);
        setContext();
        copyContextXML();
        copyServerXML();
        copyTestNG();
    }

    private void updateProperties(String properties) throws IOException {
        Properties config = new Properties();
        config.load(new FileInputStream(properties));
        Set<String> names = config.stringPropertyNames();
        for (String one : names) {
            if (System.getProperty(one) != null) {
                String str = System.getProperty(one);
                config.put(one, str);
                Logger.debug("New property: " + str);
            }
        }
        config.store(new FileOutputStream(properties), null);
    }

    private static void copyFile(String fileName, String path) {
        Logger.info("Copy " + fileName);
        File file = new File(fileName);
        URL fileInJar = TestRunner.class.getClassLoader().getResource(path + fileName);
        try {
            FileUtils.copyURLToFile(fileInJar, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Copy file with path
     *
     * @param fileName
     */
    private static void copyFile(String fileName) {
        Logger.info("Copy " + fileName);
        File file = new File(fileName);
        URL fileInJar = TestRunner.class.getClassLoader().getResource(fileName);
        try {
            FileUtils.copyURLToFile(fileInJar, file);
        } catch (IOException e) {
            Logger.info("File isn't copy: " + e);
        }
    }

    int getAMSVersion() {
        return getAMSVersionFromManifest();
    }

    private int getAMSVersionFromManifest() {
        int version = 0;
        String pattern = VERSION_AMS;
        Pattern p = Pattern.compile(pattern);
        Logger.info("Get AMS version:  ");
        List<String> dataFromFile = readFromFile(String.format(META_INF_PATTERN, new Config().getTomcatPath()) + MANIFEST);
        for (String str : dataFromFile) {
            Matcher m = p.matcher(str);
            if (m.find()) {
                version = Integer.valueOf(m.group(2));
            }
        }
        return version;
    }

    private List<String> readFromFile(String path) {
        List<String> dataFromFile = new ArrayList<>();
        Logger.info("Read " + path);
        try {
            dataFromFile = readAllLines(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logger.info("TEXT FROM " + path + " : \n");
        dataFromFile.forEach(value ->
                Logger.info("" + value + "\n"));
        return dataFromFile;
    }

    private String getJarName(URL list) throws IOException {
        Logger.info(list.getFile());
        Pattern p = Pattern.compile(JAR_NAME_PATTERN);
        Matcher m = p.matcher(list.getFile());
        if (m.find()) {
            Logger.info("JAR name: " + m.group(0));
            return m.group(0);
        }
        return null;
    }

    private List<String> getSuitList(String name, String localFolder) throws IOException {
        JarFile jar = new JarFile(name);
        Enumeration<JarEntry> entries = jar.entries();
        Pattern p = Pattern.compile(localFolder + ".{1,}");
        List<String> suitsList = new ArrayList<>();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            Matcher m = p.matcher(entry.getName());
            if (m.find()) {
                Logger.info("FILES in suits: " + entry.getName());
                suitsList.add(entry.getName());
            }
        }
        return suitsList;
    }
}
