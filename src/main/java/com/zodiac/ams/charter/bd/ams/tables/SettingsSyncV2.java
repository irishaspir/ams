package com.zodiac.ams.charter.bd.ams.tables;


import com.zodiac.ams.common.bd.AMSDBHelper;

 class SettingsSyncV2 extends AMSDBHelper {

    SettingsSyncV2(String id) {
        super("SETTINGS_SYNC_V2", id);
    }

    SettingsSyncV2() {
        super("SETTINGS_SYNC_V2");
    }

    boolean isSettingsSyncV2Empty() {
        return isSelectNull("*");
    }

    int clean() {
        return deleteAll();
    }

    boolean isIdInSettingsSyncV2() {
        return !isSelectNull("STB_ID", "STB_ID", getId());
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
