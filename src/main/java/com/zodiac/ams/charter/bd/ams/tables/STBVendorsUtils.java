package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;

public class STBVendorsUtils extends DBUtils {

    public static List<List<String>> getAllDataFromStbVendors() {
        return new STBVendors().getAllColumns();
    }

    public static void insertToSTBVendors(int id, String vendorName) {
        new STBVendors().insertVendors(id, "'" + vendorName + "'");
    }

    @Override
    public void deleteAll() {
        new STBVendors().deleteAllFromSTBVendors();
    }

    @Override
    public Object[][] getTableDescription() {
        return new STBVendors().getDesc();
    }

}
