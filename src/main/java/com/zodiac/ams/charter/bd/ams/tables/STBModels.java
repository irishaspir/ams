package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;


 class STBModels extends AMSDBHelper {

     STBModels() {
        super("STB_MODELS");
    }

     List<List<String>> getAllColumns() {
        return getStringValues();
    }

     void insertModels(int id, String modelName, int vendorId) {
        insertRow(id, modelName, vendorId);
    }

     void deleteAllFromSTBModels() {
        deleteAll();
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }


}
