package com.zodiac.ams.charter.bd.ams.tables;


import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class SettingsSTBDataV2 extends AMSDBHelper {

    SettingsSTBDataV2(String id) {
        super("SETTINGS_STBDATA_V2", id);
    }

    SettingsSTBDataV2() {
        super("SETTINGS_STBDATA_V2");
    }

    String getKey(String key) {
        return getStringValueByUniqueField(key, "STB_ID");
    }

    List<List<String>> getAllFromSettingsSTBDataV2ByMac() {
        return getStringValuesByUniqueField("*", "STB_ID", getId());
    }

    List<String> getAllKeys() {
        return getStringValuesByUniqueField("*", "STB_ID");
    }

    int setKey(String key, String value) {
        return setValue(key, "'" + value + "'", "STB_ID", getId());
    }

    int setKeys(List keys, List values) {
        return setValues(keys, values, "STB_ID", getId());
    }

    int deleteKeys() {
        return delete("STB_ID", getId());
    }

    Object insertMAC(String value) {
        return insertRowByValue("STB_ID", value);
    }

    int setDataTypeId() {
        return setValue("DATA_TYPE_ID", "3", "STB_ID", getId());
    }

    int setCallerIdNotificationZero() {
        return setValue("KEY42", "'0'", "STB_ID", getId());
    }

    int setCallerIdNotificationOne() {
        return setValue("KEY42", "'1'", "STB_ID", getId());
    }

    void deleteAllFromSettingsSTBDataV2() {
        deleteAll();
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
