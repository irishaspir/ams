package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

public class DvrMigrCHEMtdReqUtils extends DBUtils {

    public static String getAllFromDvrMigrCHEMtdReqByMac(String mac) {
        return getAll(new DvrMigrCHEMtdReq(mac).getAllFromDvrMigrCHEMtdReqBySTBId());
    }

    public static void insertToDvrMigrCHEMtdReq(String mac) {
        new DvrMigrCHEMtdReq().insertDvrMigrCHEMtdReq(mac);
    }

    @Override
    public void deleteAll() {
        new DvrMigrCHEMtdReq().deleteAllFromDvrMigrCHEMtdReq();
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrMigrCHEMtdReq().getDesc();
    }

}
