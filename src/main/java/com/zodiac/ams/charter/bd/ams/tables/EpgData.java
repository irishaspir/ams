package com.zodiac.ams.charter.bd.ams.tables;



import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

import static com.zodiac.ams.common.helpers.DateTimeHelper.getCurrentDayPlusCountStrForBd;
import static com.zodiac.ams.common.helpers.DateTimeHelper.getTodayStrForBD;


 class EpgData extends AMSDBHelper {
    
     EpgData() {
        super("EPG_DATA");
    }

     List<List<String>> getValuesByField(String requestedFieldName, String fieldName, Object fieldValue) {
        return getStringValuesByUniqueField(requestedFieldName, fieldName, fieldValue);
    }

     List<List<String>> getRowByDate(Object fieldValue) {
        return getStringValuesByUniqueField("*", "DATE_VAL", fieldValue);
    }

     List<List<String>> getRowByDateOrderByTime(Object fieldValue) {
        return getStringValuesBuUniqueFieldOrderBy("*", "DATE_VAL", fieldValue, "TIME_VAL");
    }

     List<List<String>> getRowByTime(Object fieldValue) {
        return getStringValuesByUniqueField("*", "TIME_VAL", fieldValue);
    }

     List<List<String>> getRowByServiceId(Object fieldValue) {
        return getStringValuesByUniqueField("*", "SERVICE_ID", fieldValue, getTodayStrForBD(), getCurrentDayPlusCountStrForBd(14));
    }

     int cleanDb(int amountDays) {
        return delete(getTodayStrForBD(), getCurrentDayPlusCountStrForBd(amountDays));
    }

     void deleteAllFromEpgData(){deleteAll();}

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
