package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;


class CharterSubscription extends AMSDBHelper {

    CharterSubscription() {
        super("CHARTER_SUBSCRIPTION");
    }

    CharterSubscription(String id) {
        super("CHARTER_SUBSCRIPTION", id);
    }

    List<String> getMac() {
        return getStringValues("MAC");
    }

    List<String> getSubscribed() {
        return getStringValues("SUBSCRIBED");
    }

    List<List<String>> getAllSubscriptions() {
        return getStringValues();
    }

    List<List<String>> getAllSubscriptionsByMac() {
        return getStringValuesByUniqueField("*", "MAC", getId());
    }

    int deleteRow(int mac) {
        return delete("MAC", mac);
    }

    void deleteAllFromCharterSubscription() {
        deleteAll();
    }

    Object insertRowToSubscriptionRow(String mac) {
        return insertRow("'" + mac + "'", "'SUBSCRIBE'");
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
