package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;

public class LineupsSvcWbxUtils extends DBUtils {

    public static void delTmServiceIdInLineupSvcWbx() {
        LineupsSvcWbx lineups = new LineupsSvcWbx();
        for (int id = 700; id < 720; id++)
            lineups.deleteRow(id);
    }

    public static void insertTmsServiceIdLineupSvcWbx() {
        LineupsSvcWbx lineups = new LineupsSvcWbx();
        for (int id = 700; id < 720; id++)
            lineups.insertTmsServiceId(id, "20" + id);
    }

    public static List<String> getTmsServiceIdFromLineupsSvcWbx() {return new LineupsSvcWbx().getTmsServiceId();}

    @Override
    public void deleteAll() {
      new LineupsSvcWbx().deleteAllFromLineupsSvcWbx();
    }

    @Override
    public Object[][] getTableDescription() {
        return new LineupsSvcWbx().getDesc();
    }
}
