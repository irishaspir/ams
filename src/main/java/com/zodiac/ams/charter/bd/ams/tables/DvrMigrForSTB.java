package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class DvrMigrForSTB extends AMSDBHelper {

    DvrMigrForSTB() {
        super("DVR_MIGR_FOR_STB");
    }

    DvrMigrForSTB(String id) {
        super("DVR_MIGR_FOR_STB", id);
    }

    void deleteAllFromDvrMigrForSTB() {
        deleteAll();
    }

    List<List<String>> getAllFromDvrMigrForSTBBySTBId() {
        return getStringValuesByUniqueField("*", "STBID", getId());
    }

    Object insertDvrMigrForSTB(String mac) {
        return insertRow("'" + mac + "'", 0, "'" + "" + "'", 0);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
