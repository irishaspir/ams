package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.ArrayList;
import java.util.List;

public class EpgDataUtils extends DBUtils{

    public static void cleanEpgData(int amountDays) {
        new EpgData().cleanDb(amountDays);
    }

    public static String getStringRowByDate(String[] fieldValue) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < fieldValue.length; i++) {
            StringBuilder row = getStringRow(getListStringRow(new EpgData().getRowByDateOrderByTime(fieldValue[i])));
            str.append(row);
            if (row.toString().equals("There are no data in DB by the request!"))
                break;
        }
        return str.toString();
    }

    public static String getStringRowByServiceId(String[] fieldValue) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < fieldValue.length; i++) {
            str.append(getStringRow(getListStringRow(new EpgData().getRowByServiceId(fieldValue[i]))));
        }
        return str.toString();
    }

    private static List<String> getListStringRow(List<List<String>> list) {
        if (list == null)
            return null;
        List<String> stringList = new ArrayList<>();
        for (List<String> aList : list) {
            StringBuilder str = new StringBuilder();

            for (int j = 1; j < aList.size(); j++) {
                switch (j) {
                    case 31:
                    case 38:
                        break;
                    case 1:
                    case 4:
                    case 8:
                    case 9:
                        if (aList.get(j).equals("0")) str.append("|");
                        else str.append(aList.get(j)).append("|");
                        break;
                    case 33:
                    case 35:
                        str.append(aList.get(j)).append("|");
                        break;
                    case 2:
                        str.append("\"").append(aList.get(j).replace('/', '-')).append("\"").append("|");
                        break;
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 21:
                    case 22:
                    case 23:
                    case 25:
                    case 26:
                    case 27:
                    case 37:
                        if (aList.get(j).equals("0"))
                            str.append("\"").append("N").append("\"").append("|");
                        else str.append("\"").append("Y").append("\"").append("|");
                        break;
                    default:
                        if (aList.get(j).equals("null"))
                            str.append("\"").append("\"").append("|");
                        else str.append("\"").append(aList.get(j)).append("\"").append("|");
                }
            }
            str.deleteCharAt(str.lastIndexOf("|"));
            str.append("\n");
            stringList.add(str.toString());
        }
        return stringList;
    }

    private static StringBuilder getStringRow(List<String> list) {
        StringBuilder str = new StringBuilder();
        if (list == null)
            return str.append("There are no data in DB by the request!");
        for (String aList : list) {
            str.append(aList);
        }
        return str;
    }

    @Override
    public void deleteAll() {
        new EpgData().deleteAllFromEpgData();
    }

    @Override
    public Object[][] getTableDescription() {
        return new EpgData().getDesc();
    }
}
