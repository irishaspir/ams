package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;
import java.util.Map;

public class DvrUnsyncSTBUtils extends DBUtils {

    public static String getAllFromDvrUnsyncSTBByMac(String mac) {
        return getAll(new DvrUnsyncSTB(mac).getAllFromDvrUnsyncSTBByMac());
    }

    public static void insertToDvrUnsyncSTB(String mac) {
        new DvrUnsyncSTB().insertDvrUnsyncSTB(mac);
    }

    @Override
    public void deleteAll() {
        new DvrUnsyncSTB().deleteAllFromDvrUnsyncSTB();
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrUnsyncSTB().getDesc();
    }

}
