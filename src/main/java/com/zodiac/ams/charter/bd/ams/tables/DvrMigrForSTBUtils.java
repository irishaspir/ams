package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;
import java.util.Map;

public class DvrMigrForSTBUtils extends DBUtils {

    public static String getAllFromDvrMigrForSTBByMac(String mac) {
        return getAll(new DvrMigrForSTB(mac).getAllFromDvrMigrForSTBBySTBId());
    }

    public static void insertToDvrMigrForSTB(String mac) {
        new DvrMigrForSTB().insertDvrMigrForSTB(mac);
    }

    @Override
    public void deleteAll() {
        new DvrMigrForSTB().deleteAllFromDvrMigrForSTB();
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrMigrForSTB().getDesc();
    }

}
