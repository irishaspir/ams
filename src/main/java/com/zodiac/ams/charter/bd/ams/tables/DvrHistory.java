package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

 class DvrHistory extends AMSDBHelper {

     DvrHistory(String id) {
        super("DVR_HISTORY", id);
    }

     DvrHistory() {
        super("DVR_HISTORY");
    }

     void deleteAllFromDvrHistory(){
        deleteAll();
    }

     List<List<String>> getAllFromDvrHistoryBySTBId() {
        return getStringValuesByUniqueField("*", "STB_ID", getId());
    }

     Object insertDvrHistory(String mac) {
        return insertRow("'"+ mac + "'", 0, "'" + "" + "'",
               0,0,0,0,0,0,0,0,0);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
