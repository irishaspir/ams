package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;
import com.zodiac.ams.charter.services.stb.STBMetadataOptions;

import java.util.*;

import static com.zodiac.ams.charter.helpers.DataUtils.converterUnixToDate;
import static com.zodiac.ams.charter.helpers.DataUtils.decimalToHex;
import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.convertUDateToLong;
import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.isDataNull;

public class STBMetadataUtils extends DBUtils {

    public List<String> getSTBMetadata(String mac) {
        return new STBMetadata(mac).getSTBMetadata();
    }

    public String getAllSTBMetadataByMac(String mac) {
        return getAll(new STBMetadata(mac).getAllFromSTBMetadataByMac());
    }

    public static void insertToSTBMetadata(long mac, String id) {
        new STBMetadata().insertInSTBMetadata(mac, id);
    }

    public static void insertToSTBMetadata(String mac) {
        new STBMetadata().insertInSTBMetadata(createRowsToInsetsInSTBMetadata(mac));
    }

    private static Object[] createRowsToInsetsInSTBMetadata(String mac) {
        Object[][] data = new STBMetadataUtils().getTableDescription();
        Object[] args = new Object[data.length];
        for (int i = 0; i < data.length; i++) {
            String key = String.valueOf(data[i][0]);
            String value = String.valueOf(data[i][1]);

            switch (key.toUpperCase()) {
                case "MAC":
                    args[i] = mac;
                    break;
                case "STB_MODE":
                    args[i] = 3;
                    break;
                case "HUBID":
                    args[i] = -1;
                    break;
                case "STB_METADATA_FLAGS":
                    args[i] = null;
                    break;
                case "MAC_STR":
                    args[i] = decimalToHex(mac);
                    break;
                case "REGISTR_TIME_STAMP":
                case "LAST_ACT_TIME_STAMP":
                case "MODE_CHANGE_TIME_STAMP":
                    args[i] = 1470642894467L;
                    break;
                default:
                    switch (value) {
                        case "NUMBER":
                            args[i] = 0;
                            break;
                        case "VARCHAR2":
                            args[i] = "";
                            break;
                        default:
                            args[i] = null;
                    }
            }

        }

        return args;

    }

    public void insertToSTBMetadataByValue(long mac) {
        new STBMetadata().insertSTBByValue(mac);
    }

    public void deleteFromSTBMetadata(long mac) {
        new STBMetadata().deleteMacInSTBMetadata(mac);
    }

    @Override
    public void deleteAll() {
        new STBMetadata().deleteAllFromSTBMetadata();
    }

    public static void setSTBConnectionMode(int stbMode, String mac) {
        new STBMetadata(mac).setSTBModeByMac(stbMode);
    }

    public static long getRegisterTimeStamp(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getRegistrTimeStamp();
    }

    public static long getModeChangeTimeStamp(String mac) {
        return new STBMetadataOptions(new STBMetadataUtils().getSTBMetadata(mac)).getModeChangeTimeStamp();
    }

    public static List<Date> getSTBLastActivityDateList(String mac) {
        STBMetadata stbMetadata = new STBMetadata(mac);
        List<Date> listDate = new ArrayList<>();
        List<List<String>> listUnixDate = stbMetadata.getSTBLastActivityByMac();
        for (List<String> date : listUnixDate)
            listDate.add(converterUnixToDate(!isDataNull(date.get(0)) ? convertUDateToLong(date.get(0)) : 0));
        Collections.reverse(listDate);
        return listDate;
    }

    public Date getSTBLastActivityLastTimeToDate(String mac) {
        return getSTBLastActivityDateList(mac).get(0);
    }

    public static void setSTBModeInSTBMetadata(int stbMode, String mac) {
        new STBMetadata(mac).setSTBModeByMac(stbMode);
    }

    public static void setLastActivityTime(String mac) {
        new STBMetadata(mac).setLastActivityTimeByMac();
    }

    public static boolean isMacInSTBMetadata(String mac) {
        return (new STBMetadata(mac).getSTBMetadata()) != null;
    }

    @Override
    public Object[][] getTableDescription() {
        return new STBMetadata().getDesc();
    }

}
