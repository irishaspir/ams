package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

public class LineupLogosUtils extends DBUtils {

    //LineupLogos
    public static void delTmServiceIdInLineupLogo() {
        LineupLogos lineupLogos = new LineupLogos();
        for (int id = 138; id < 140; id++)
            lineupLogos.deleteRow(id);
    }


    public static void insertTmsServiceIdInLineupLogo() {
        LineupLogos lineupLogos = new LineupLogos();
        for (int id = 138; id < 140; id++)
            lineupLogos.insertTmsServiceId(id, "10" + id);
    }


    @Override
    public void deleteAll() {
        new LineupLogos().deleteAllFromLineupLogos();
    }

    @Override
    public Object[][] getTableDescription() {
        return new LineupLogos().getDesc();
    }
}
