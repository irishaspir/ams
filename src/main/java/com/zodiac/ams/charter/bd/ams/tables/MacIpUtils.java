package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;

/**
 * Created by SpiridonovaIM on 19.05.2017.
 */
public class MacIpUtils extends DBUtils {

    public static Object insertRowToMacIp(String mac, String int_ip, String deviceId, String ip) {
        return new MacIp().insertRowToMacIp(mac, int_ip, deviceId, ip);
    }

    public static int deleteMacInMacIp(String mac) {
        return new MacIp(mac).deleteMac();
    }

    public static List<String> getRow(String mac) {
        return new MacIp(mac).getRowFromMacIp();
    }

    @Override
    public void deleteAll() {
        new MacIp().deleteAllFromMacIp();
    }

    @Override
    public Object[][] getTableDescription() {
        return new MacIp().getDesc();
    }

    public boolean isMacInMacIp(String mac) {
        return new MacIp(mac).isMacInTable();
    }
}
