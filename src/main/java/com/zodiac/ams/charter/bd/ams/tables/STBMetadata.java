package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class STBMetadata extends AMSDBHelper {

    STBMetadata(String id) {
        super("STB_METADATA", id);
    }

    STBMetadata() {
        super("STB_METADATA");
    }

    List<String> getSTBMetadata() {
        return getStringValuesByUniqueField("*", "MAC");
    }

    List<List<String>> getAllFromSTBMetadataByMac() {
        return getStringValuesByUniqueField("*", "MAC", getId());
    }

    List<List<String>> getSTBLastActivityByMac() {
        return getStringValuesBuUniqueFieldOrderBy("LAST_ACT_TIME_STAMP", "MAC", "LAST_ACT_TIME_STAMP");
    }

    Object insertInSTBMetadata(long mac, String id) {
        return insertRow(mac, id, 3, 1470642894467L, 0, 1495112237883L, 0, 0, 0, 30, 51, 0, "", (-1), null,
                0, "", 0, 0, 0, "", 0, 0, 0, 0);
    }

    Object insertInSTBMetadata(Object... args) {
        return insertRow(args);
    }

    Object insertSTBByValue(long mac) {
        return insertRowByValue("MAC", String.valueOf(mac));
    }

    void deleteMacInSTBMetadata(Object mac) {
        delete("MAC", mac);
    }

    void deleteAllFromSTBMetadata() {
        deleteAll();
    }

    void setSTBModeByMac(int stbMode) {
        setValue("STB_MODE", stbMode, "MAC");
    }

    void setLastActivityTimeByMac() {
        setValue("LAST_ACT_TIME_STAMP", 1495112237883L, "MAC");
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }


}
