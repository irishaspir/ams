package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

/**
 * Created by SpiridonovaIM on 19.05.2017.
 */
public class SettingsSyncV2Utils extends DBUtils{

    public boolean isMacInSettingsSyncV2(String mac) {
        return new SettingsSyncV2(mac).isIdInSettingsSyncV2();
    }

    public static boolean isSettingsSyncV2Empty() {
        return new SettingsSyncV2().isSettingsSyncV2Empty();
    }

    @Override
    public void deleteAll() {
        new SettingsSyncV2().clean();
    }

    @Override
    public Object[][] getTableDescription() {
        return new SettingsSyncV2().getDesc();
    }

}
