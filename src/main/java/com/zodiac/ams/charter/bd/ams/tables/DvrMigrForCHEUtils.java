package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;
import java.util.Map;

public class DvrMigrForCHEUtils extends DBUtils {

    public static String getAllFromDvrMigrForCHEByMac(String mac) {
        return getAll(new DvrMigrForCHE(mac).getAllFromDvrMigrForCHEBySTBId());
    }

    public static void insertToDvrMigrForCHE(String mac) {
        new DvrMigrForCHE().insertDvrMigrForCHE(mac);
    }

    @Override
    public void deleteAll() {
        new DvrMigrForCHE().deleteAllFromDvrMigrForCHE();
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrMigrForCHE().getDesc();
    }

}
