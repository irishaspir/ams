package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

public class CharterSubscriptionUtils extends DBUtils {

    public static String getAllFromSubscriptions() {
        return getAll(new CharterSubscription().getAllSubscriptions());
    }

    public static String getAllFromSubscriptionsByMac(String mac) {
        return getAll(new CharterSubscription(mac).getAllSubscriptionsByMac());
    }

    public static Object insertRowToCharterSubscription(String mac) {
        return new CharterSubscription().insertRowToSubscriptionRow(mac);
    }

    @Override
    public void deleteAll() {
        new CharterSubscription().deleteAllFromCharterSubscription();
    }

    @Override
    public Object[][] getTableDescription() {
        return new CharterSubscription().getDesc();
    }
}
