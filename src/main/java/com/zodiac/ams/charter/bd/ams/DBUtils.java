package com.zodiac.ams.charter.bd.ams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.zodiac.ams.charter.store.TestKeyWordStore.NO_DATA_IN_DB;

public abstract class DBUtils implements TablesUtils {

    protected static String getAll(List<List<String>> dataDB) {
        if (dataDB == null) return NO_DATA_IN_DB;
        else {
            List<String> listRowsDB = new ArrayList<>();
            StringBuilder stringDBData = new StringBuilder();
            for (List<String> rowTable : dataDB) {
                StringBuilder stringRow = new StringBuilder();
                rowTable.forEach(value -> stringRow.append(value + "|"));
                stringRow.deleteCharAt(stringRow.lastIndexOf("|"));
                stringRow.append("\n");
                listRowsDB.add(stringRow.toString());
            }
            Collections.sort(listRowsDB);
            listRowsDB.forEach(value -> stringDBData.append(value));
            return stringDBData.toString();
        }
    }




}
