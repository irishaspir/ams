package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

 class DvrMigrForCHE extends AMSDBHelper {

     DvrMigrForCHE() {
        super("DVR_MIGR_FOR_CHE");
    }

     DvrMigrForCHE(String id) {
        super("DVR_MIGR_FOR_CHE", id);
    }

     void deleteAllFromDvrMigrForCHE(){
        deleteAll();
    }

     List<List<String>> getAllFromDvrMigrForCHEBySTBId() {
         return getStringValuesByUniqueField("*", "STBID", getId());
    }

     Object insertDvrMigrForCHE(String mac) {
        return insertRow("'" + "e5ff9752-a353-4bdd-9a53-0a1d44111111" + "'", "'" + mac + "'", null,0);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
