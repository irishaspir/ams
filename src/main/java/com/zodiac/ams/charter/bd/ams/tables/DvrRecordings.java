package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class DvrRecordings extends AMSDBHelper {

     DvrRecordings() {
        super("DVR_RECORDINGS");
    }

     DvrRecordings(String id) {
        super("DVR_RECORDINGS", id);
    }

     void deleteAllFromDvrRecordings(){
        deleteAll();
    }

     Object insertDvrRecordings(String mac) {
        return insertRow("'" + mac + "'", 0,0,0,0,0,0,0,0,0,0, "'" + "" + "'",0,0,0,0,0,0,0,0,0);
    }

     List<List<String>> getAllFromDvrRecordingsByMac() {
        return getStringValuesByUniqueField("*", "MAC", getId());
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
