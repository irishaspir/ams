package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.*;

import static com.zodiac.ams.charter.helpers.DataUtils.converterUnixToDate;
import static com.zodiac.ams.charter.helpers.dsg.DSGDataUtils.*;


public class STBLastActivityUtils extends DBUtils {

    public static List<String> getSTBLastActivityList(String mac) {
        STBLastActivity stbLastActivity = new STBLastActivity(mac);
        List<String> listDate = new ArrayList<>();
        List<List<String>> listUnixDate = stbLastActivity.getSTBLastActivityByMac();
        for (List<String> date : listUnixDate)
            listDate.add(converterUnixToString(!isDataNull(date.get(0)) ? convertUDateToLong(date.get(0)) : 0));
        Collections.reverse(listDate);
        return listDate;
    }

    public static List<String> getSTBLastActivityStringList(String mac) {
        List<Date> list = getSTBLastActivityDateList(mac);
        List<String> listDate = new ArrayList<>();
        list.forEach(value -> listDate.add(value.toString()));
        return listDate;
    }

    private static List<Date> getSTBLastActivityDateList(String mac) {
        STBLastActivity stbLastActivity = new STBLastActivity(mac);
        List<Date> listDate = new ArrayList<>();
        List<List<String>> listUnixDate = stbLastActivity.getSTBLastActivityByMac();
        for (List<String> date : listUnixDate)
            listDate.add(converterUnixToDate(!isDataNull(date.get(0)) ? convertUDateToLong(date.get(0)) : 0));
        Collections.reverse(listDate);
        return listDate;
    }

    public static String getSTBLastActivityLastTime(String mac) {
        return getSTBLastActivityStringList(mac).get(0);
    }

    public static boolean isMacInSTBLastActivity(String mac) {
        return new STBLastActivity(mac).isMACInSTBLastActivity();
    }

    public static Date getSTBLastActivityLastTimeToDate(String mac) {
        return getSTBLastActivityDateList(mac).get(0);
    }

    public static void insertToSTBLastActivity(String mac, long time) {
        new STBLastActivity().insertLastActivityTime(mac,  time);
    }

    @Override
    public void deleteAll() {
        new STBLastActivity().deleteAllFromSTBLastActivity();

    }

    @Override
    public Object[][] getTableDescription() {
        return new STBLastActivity().getDesc();
    }
}
