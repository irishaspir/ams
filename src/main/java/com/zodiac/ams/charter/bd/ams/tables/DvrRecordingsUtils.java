package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;
import java.util.Map;

public class DvrRecordingsUtils extends DBUtils {

    public static String getAllFromDvrRecordingsByMac(String mac) {
        return getAll(new DvrRecordings(mac).getAllFromDvrRecordingsByMac());
    }

    public static void insertToDvrRecordings(String mac) {
        new DvrRecordings().insertDvrRecordings(mac);
    }

    @Override
    public void deleteAll() {
        new DvrRecordings().deleteAllFromDvrRecordings();
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrRecordings().getDesc();
    }

}
