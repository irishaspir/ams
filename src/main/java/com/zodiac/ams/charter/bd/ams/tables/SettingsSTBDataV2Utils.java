package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;
import com.zodiac.ams.charter.services.settings.SettingsOption;
import com.zodiac.ams.common.logging.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SpiridonovaIM on 19.05.2017.
 */
public class SettingsSTBDataV2Utils extends DBUtils {

    public static boolean isMacInSettingSTBDataV2(String mac){
        return new SettingsSTBDataV2(mac).getAllFromSettingsSTBDataV2ByMac().size()>0;
    }

    // SettingsSTBDataV2
    public  static int setDataTypeIdInSettingsSTBDataV2(String mac) {
        return new SettingsSTBDataV2(mac).setDataTypeId();
    }

    public static Object insertMACToSettingsSTBDataV2(String value) {
        return new SettingsSTBDataV2().insertMAC(value);
    }

    public static int deleteKeysFromSettingsSTBDataV2ByMAC(String mac) {
        return new SettingsSTBDataV2(mac).deleteKeys();
    }

    public static List<String> getAllKeysFromSettingsSTBDataV2(String mac) {
        List<String> list = new SettingsSTBDataV2(mac).getAllKeys();
        return list;
    }

    public static List<String> getAllKeysWithout65And62KeysFromSettingsSTBDataV2(String mac) {
        List<String> list = getAllKeysFromSettingsSTBDataV2(mac);
        list.remove(0);
        list.remove(0);
        list.remove(0);
        list.remove(61);
        list.remove(63);
        return list;
    }

    public static List<String> getAllKeysWithout65KeyFromSettingsSTBDataV2(String mac) {
        List<String> list = getAllKeysFromSettingsSTBDataV2(mac);
        list.remove(0);
        list.remove(0);
        list.remove(0);
        list.remove(64);
        return list;
    }

    public static List<String> getKeysFromSettingsSTBDataV2(ArrayList<SettingsOption> options, String mac) {
        SettingsSTBDataV2 table = new SettingsSTBDataV2(mac);
        List<String> list = new ArrayList<>();
        for (SettingsOption one : options) {
            list.add(table.getKey(one.getColumnName()));
        }
        Logger.info(list.toString());
        return list;
    }

    public static int setKeysInSettingsSTBDataV2(String mac, List keys, List values) {
        return new SettingsSTBDataV2(mac).setKeys(keys, values);
    }

    public static void setCallerIdNotificationZeroInSettingsSTBDataV2(String mac) {
        new SettingsSTBDataV2(mac).setCallerIdNotificationZero();
    }

    public static void setCallerIdNotificationOneInSettingsSTBDataV2(String mac) {
        new SettingsSTBDataV2(mac).setCallerIdNotificationOne();
    }

    public static String getAllSettingsSTBDataV2ByMac(String mac) {
        return getAll(new SettingsSTBDataV2(mac).getAllFromSettingsSTBDataV2ByMac());
    }

        @Override
    public void deleteAll() {
        new SettingsSTBDataV2().deleteAllFromSettingsSTBDataV2();
    }

    @Override
    public Object[][] getTableDescription() {
        return new SettingsSTBDataV2().getDesc();
    }

}
