package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class STBLastActivity extends AMSDBHelper {

    STBLastActivity(String id) {
        super("STB_LAST_ACTIVITY", id);
    }

    STBLastActivity() {
        super("STB_LAST_ACTIVITY");
    }

    List<List<String>> getSTBLastActivityByMac() {
        return getStringValuesBuUniqueFieldOrderBy("LAST_ACT_TIME_STAMP", "MAC", "LAST_ACT_TIME_STAMP");
    }

    boolean isMACInSTBLastActivity() {
        return getStringValuesBuUniqueFieldOrderBy("LAST_ACT_TIME_STAMP", "MAC", "LAST_ACT_TIME_STAMP") != null;
    }

    void insertLastActivityTime(String mac, long time) {
        insertRow(mac, time);
    }

    void deleteAllFromSTBLastActivity() {
        deleteAll();
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
