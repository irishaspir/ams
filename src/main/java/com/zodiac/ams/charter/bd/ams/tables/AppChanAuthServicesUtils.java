package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

public class AppChanAuthServicesUtils extends DBUtils {

    public static boolean isMacInAppChanAuthServices(String mac) {
        return new AppChanAuthServices(mac).getAllFromAppChanAuthServicesByMac().size() > 0;
    }

    public static String getAllFromAppChanAuthServicesByMac(String mac) {
        return getAll(new AppChanAuthServices(mac).getAllFromAppChanAuthServicesByMac());
    }

    public static Object insertRowToAppChanAuthServices(String mac, String id) {
        return new AppChanAuthServices().insertRowToAppChanAuthServices(mac, id);
    }

    @Override
    public void deleteAll() {
        new AppChanAuthServices().deleteAllFromAppChanAuthServices();
    }

    @Override
    public Object[][] getTableDescription() {
        return new AppChanAuthServices().getDesc();
    }

}
