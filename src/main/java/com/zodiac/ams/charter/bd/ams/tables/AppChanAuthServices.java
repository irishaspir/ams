package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

 class AppChanAuthServices extends AMSDBHelper {

     AppChanAuthServices() {
        super("APP_CHAN_AUTH_SERVICES");
    }

     AppChanAuthServices(String id) {
        super("APP_CHAN_AUTH_SERVICES", id);
    }

     void deleteAllFromAppChanAuthServices(){
        deleteAll();
    }

     List<List<String>> getAllFromAppChanAuthServicesByMac() {
        return  getStringValuesByUniqueField("*", "MAC", getId());
    }

     Object insertRowToAppChanAuthServices(String mac, String id){
        return insertRow( "'" + mac + "'", "'" + id + "'", "'" + "" + "'");
    }

     Object[][] getDesc() {
        return getTableMetadata();
    }

}
