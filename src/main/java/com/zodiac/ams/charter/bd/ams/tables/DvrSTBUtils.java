package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

public class DvrSTBUtils extends DBUtils {

    public static String getAllFromDvrSTBByMac(String mac) {
        return getAll(new DvrSTB(mac).getAllFromDvrSTBByMac());
    }

    public static void insertToDvrSTB(String mac, String id) {
        new DvrSTB().insertDvrSTB(mac, id);
    }

    @Override
    public void deleteAll() {
        new DvrSTB().deleteAllFromDvrSTB();
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrSTB().getDesc();
    }

}
