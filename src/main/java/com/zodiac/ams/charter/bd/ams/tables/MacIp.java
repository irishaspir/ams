package com.zodiac.ams.charter.bd.ams.tables;


import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

 class MacIp extends AMSDBHelper {

    MacIp() {
        super("MAC_IP");
    }

    MacIp(String mac) {
        super("MAC_IP", mac);
    }

    String getMacConformToDeviceId(Object param) {
        return getValueByUniqueField("MAC", "MAC_STR", param).toString();
    }

    String getDeviceIdConformToMac(String mac) {
        return getValueByUniqueField("MAC_STR", "MAC", Long.parseLong(mac)).toString();
    }

    Object insertRowToMacIp(String mac, String int_ip, String deviceId, String ip) {
        return insertRow(mac, int_ip, deviceId, ip);
    }

    int deleteMac() {
        return delete("MAC", getId());
    }

    void deleteAllFromMacIp() {
        deleteAll();
    }

    List<String> getRowFromMacIp() {
        return getStringValuesByUniqueField("*", "MAC");
    }

    boolean isMacInTable() {
        return getStringValuesByUniqueField("*", "MAC") != null;
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
