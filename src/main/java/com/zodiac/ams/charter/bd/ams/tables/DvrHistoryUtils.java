package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

public class DvrHistoryUtils extends DBUtils {

    public static String getAllFromDvrHistoryByMac(String mac) {
        return getAll(new DvrHistory(mac).getAllFromDvrHistoryBySTBId());
    }

    @Override
    public void deleteAll() {
        new DvrHistory().deleteAllFromDvrHistory();
    }

    public static void insertToDvrHistory(String mac) {
        new DvrHistory().insertDvrHistory(mac);
    }

    @Override
    public Object[][] getTableDescription() {
        return new DvrHistory().getDesc();
    }

}
