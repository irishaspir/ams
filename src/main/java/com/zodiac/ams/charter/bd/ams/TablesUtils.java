package com.zodiac.ams.charter.bd.ams;

/**
 * Created by SpiridonovaIM on 22.05.2017.
 */
public interface TablesUtils {

    void deleteAll();

    Object[][] getTableDescription();

}
