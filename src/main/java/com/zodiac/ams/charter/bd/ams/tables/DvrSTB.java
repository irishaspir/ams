package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class DvrSTB extends AMSDBHelper {

    DvrSTB() {
        super("DVR_STB");
    }

    DvrSTB(String id) {
        super("DVR_STB", id);
    }

    void deleteAllFromDvrSTB() {
        deleteAll();
    }

    List<List<String>> getAllFromDvrSTBByMac() {
        return getStringValuesByUniqueField("*", "MAC", getId());
    }

    Object insertDvrSTB(String mac, String id) {
        return insertRow(mac, "'" + id + "'", 0, 0, 0, 0, 0, 0, 0, "'" + "" + "'", 0);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
