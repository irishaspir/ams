package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

 class STBVendors extends AMSDBHelper {

     STBVendors(String id) {
        super("STB_VENDORS", id);
    }

    STBVendors() {
        super("STB_VENDORS");
    }

    List<List<String>> getAllColumns() {
        return getStringValues();
    }

    void insertVendors(int id, String vendorName) {
        insertRow(id, vendorName);
    }

    void deleteAllFromSTBVendors() {
        deleteAll();
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
