package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.TablesUtils;

/**
 * Created by SpiridonovaIM on 19.06.2017.
 */
public class SettingsKeysUtils implements TablesUtils {

    public static String getSupportedSettingsValues(String name) {
        return new SettingsKeys().getSupportValues(name);
    }

    public static String[] getSupportedSettingsValuesArray(String name) {
        return new SettingsKeys().getSupportValues(name).split("\\|");
    }

    public static String getDefaultSettingsValues(String name) {
        return new SettingsKeys().getDefaultValues(name);
    }


    @Override
    public void deleteAll() {

    }

    @Override
    public Object[][] getTableDescription() {
        return new SettingsKeys().getDesc();
    }
}
