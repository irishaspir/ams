package com.zodiac.ams.charter.bd.ams.tables;


import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;


 class LineupLogos extends AMSDBHelper {

     LineupLogos() {
        super("LINEUP_LOGOS");
    }

     List<String> getTmsServiceId() {
        return getStringValues("TMSSERVICEID");
    }


     Object insertTmsServiceId(int id, String tmsServiceId) {
        return insertRow(id, "'" + tmsServiceId + "'", "44", "24", "'" + "" + "'", "'" + "" + "'");
    }

     int deleteRow(int id) {
        return delete("ID", id);
    }

     void deleteAllFromLineupLogos() {deleteAll();}

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
