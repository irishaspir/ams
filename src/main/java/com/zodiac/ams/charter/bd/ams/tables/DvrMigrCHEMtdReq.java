package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

 class DvrMigrCHEMtdReq extends AMSDBHelper {

     DvrMigrCHEMtdReq() {
        super("DVR_MIGR_CHE_MTD_REQ");
    }

     DvrMigrCHEMtdReq(String id) {
        super("DVR_MIGR_CHE_MTD_REQ", id);
    }

     void deleteAllFromDvrMigrCHEMtdReq(){
        deleteAll();
    }

     List<List<String>> getAllFromDvrMigrCHEMtdReqBySTBId() {
        return getStringValuesByUniqueField("*", "STBID", getId());
    }

     Object insertDvrMigrCHEMtdReq(String mac) {
        return insertRow("'" + mac + "'", 0,0,0);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
