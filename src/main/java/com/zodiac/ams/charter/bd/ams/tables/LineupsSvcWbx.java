package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;


 class LineupsSvcWbx extends AMSDBHelper {


    LineupsSvcWbx() {
        super("LINEUPS_SVC_WBX");
    }

    List<String> getTmsServiceId() {
        return getStringValues("TMSSERVICEID");
    }


    Object insertTmsServiceId(int id, String tmsServiceId) {
        return insertRow(id, tmsServiceId, "MC", tmsServiceId, 0, "", "", 0, "", "", "", "", "", 1, 0, 10018);
    }

    int deleteRow(int id) {
        return delete("ID", id);
    }

    void deleteAllFromLineupsSvcWbx() {
        deleteAll();
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
