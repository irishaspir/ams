package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;


public class SettingsSTBUtils extends DBUtils {

    public static boolean isMacInSettingSTB(String mac){
        return new SettingsSTB(mac).getAllFromSettingsSTBByMac().size()>0;
    }

    public static Object insertRowToSettingsSTB(String id, String deviceId, String modelId, String migrationFlag) {
        return new SettingsSTB().insertRowToSettingsSTB(id, deviceId, modelId, migrationFlag);
    }

    public static String getAllSettingsByMacFromSettingsSTB(String mac) {
        return getAll(new SettingsSTB(mac).getAllFromSettingsSTBByMac());
    }

    public static String getMigrationFlagFromSettingsSTB(String mac) {
        return new SettingsSTB(mac).getDataMigration();
    }

    public static int deleteRowFromSettingsSTB(String mac) {
        return new SettingsSTB(mac).deleteRow();
    }

    public static void setMigrationFlagNullInSettingsSTB(String mac) {
        new SettingsSTB(mac).setDataMigrationNull();
    }

    public static void setMigrationFlagOneInSettingSTB(String mac) {
        new SettingsSTB(mac).setDataMigrationOne();
    }


    @Override
    public void deleteAll() {
      new SettingsSTB().deleteAllFromSettingsSTB();
    }

    @Override
    public Object[][] getTableDescription() {
        return new SettingsSTB().getDesc();
    }

}
