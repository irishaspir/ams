package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class DvrUnsyncSTB extends AMSDBHelper {

    DvrUnsyncSTB() {
        super("DVR_UNSYNC_STB");
    }

    DvrUnsyncSTB(String id) {
        super("DVR_UNSYNC_STB", id);
    }

    void deleteAllFromDvrUnsyncSTB() {
        deleteAll();
    }

    List<List<String>> getAllFromDvrUnsyncSTBByMac() {
        return getStringValuesByUniqueField("*", "MAC", getId());
    }

    Object insertDvrUnsyncSTB(String mac) {
        return insertRow(0, "'" + mac + "'", 0, 0);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
