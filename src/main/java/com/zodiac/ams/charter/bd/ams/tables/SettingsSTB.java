package com.zodiac.ams.charter.bd.ams.tables;


import com.zodiac.ams.common.bd.AMSDBHelper;

import java.util.List;

class SettingsSTB extends AMSDBHelper {

    SettingsSTB(String id) {
        super("SETTINGS_STB", id);
    }

    SettingsSTB() {
        super("SETTINGS_STB");
    }

    List<List<String>> getAllFromSettingsSTBByMac() {
        return getStringValuesByUniqueField("*", "ID", getId());
    }

    String getSTBID() {
        return getStringFieldValue("STBID");
    }

    String getModelId() {
        return getStringFieldValue("MODELID");
    }

    String getDataMigration() {
        return getStringFieldValue("DATA_MIGRATION");
    }

    int setDataMigrationNull() {
        return setValue("DATA_MIGRATION", "'0'", "ID", getId());
    }

    int setDataMigrationOne() {
        return setValue("DATA_MIGRATION", "'1'", "ID", getId());
    }

    int deleteRow() {
        return delete("ID", getId());
    }

    List<String> getAllColumns() {
        return getStringValuesByUniqueField("*", "ID");
    }

    Object insertRowToSettingsSTB(String id, String deviceId, String modelId, String migrationFlag) {
        return insertRow(id, deviceId, modelId, migrationFlag);
    }

    void deleteAllFromSettingsSTB() {
        deleteAll();
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }

}
