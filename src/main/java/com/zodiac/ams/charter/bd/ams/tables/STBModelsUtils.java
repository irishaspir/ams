package com.zodiac.ams.charter.bd.ams.tables;

import com.zodiac.ams.charter.bd.ams.DBUtils;

import java.util.List;

public class STBModelsUtils extends DBUtils {

    public static List<List<String>> getDataFromStbModels() {
        return new STBModels().getAllColumns();
    }

    public static void insertToSTBModels(int id, String modelName, int vendorId) {
        new STBModels().insertModels(id, "'" + modelName + "'", vendorId);
    }

    @Override
    public void deleteAll() {
        new STBModels().deleteAllFromSTBModels();
    }

    @Override
    public Object[][] getTableDescription() {
        return new STBModels().getDesc();
    }
}
