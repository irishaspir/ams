package com.zodiac.ams.charter.bd.ams.tables;


import com.zodiac.ams.common.bd.AMSDBHelper;

class SettingsKeys extends AMSDBHelper {

    SettingsKeys(String id) {
        super("SETTINGS_KEYS", id);
    }

    SettingsKeys() {
        super("SETTINGS_KEYS");
    }

    String getSupportValues(String optionName) {
        return getStringValueByUniqueField("SUP_VAL", "KEY_NAME", optionName);
    }

    String getDefaultValues(String optionName) {
        return getStringValueByUniqueField("KEY_DEFAULT", "KEY_NAME", optionName);
    }

    Object[][] getDesc() {
        return getTableMetadata();
    }
}
