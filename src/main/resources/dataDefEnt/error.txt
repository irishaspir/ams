#|group|error|description|error code|tarrantula test

1|0|CAS1|Viewing Problem. Invalid CA packet|0|12.2.17	
2|0|CAS2|Viewing Problem. Card not authorized|1|12.2.28	
3|0|CAS3|Viewing Problem. Blackout active|2|12.2.39	
4|0|CAS4|Viewing Problem. Minimum Price or Special reason|3|12.2.49	
5|0|CAS5|Viewing Problem. Consummation warning|4|12.2.50	
6|0|CAS6|Viewing Problem. Not viewable|5|12.2.51	
7|0|CAS7|Viewing Problem. Purchase Window Closed|6|12.2.52	
8|0|CAS8|Viewing Problem. Individual Free Preview Slots Full|7|12.2.53	
9|0|CAS9|Viewing Problem. Service not authorized|8|12.2.54	
10|0|CAS10|Viewing Problem. No viewing of this IPPV allowed|9|12.2.18	
11|0|CAS11|Viewing Problem. Bad Pricing Table index|10|12.2.19	
12|0|CAS12|Viewing Problem. No matching VECM found|11|12.2.20	
13|0|CAS13|Viewing Problem. No matching VEMM found|12|12.2.21	
14|0|CAS14|Viewing Problem. No heartbeat received|13|12.2.22	
15|0|CAS15|Viewing Problem. Service provide blackout|14|12.2.23	
16|0|CAS16|Viewing Problem. No VGS data|15|12.2.24	
17|0|CAS17|CardStatus_UNKNOWN|16|12.2.25	
18|0|CAS18|CardStatus_CARD_FAILURE|17|12.2.26	
19|0|CAS19|CardStatus_CARD_INVALID|18|12.2.27	
20|0|CAS20|CardStatus_VGS_DNS_ERROR|19|12.2.29	
21|0|CAS21|CardStatus_VGS_DHCP_ERROR|20|12.2.30	
22|0|CAS22|CardStatus_VGS_HW_ERROR|21|12.2.31	
23|0|CAS23|CardStatus_VGS_FIRST_REG_ERROR|22|12.2.32	
24|0|CAS24|CardStatus_VGS_SECOND_REG_ERROR|23|12.2.33	
25|0|CAS25|CardStatus_VGS_SENDING_DATA_ERROR|24|12.2.34	
26|0|CAS26|CardStatus_VGS_UNKNOWN_STB_ERROR|25|12.2.35	
27|0|CAS27|CardStatus_VGS_PUBID_NOT_EXIST_IN_EMMG|26|12.2.36	
28|0|CAS28|CardStatus_VGS_PUBID_NOT_ACTIVE_IN_EMMG|27|12.2.37	
29|0|CAS29|CardStatus_VGS_EMMG_COMM_ERROR|28|12.2.38	
30|0|CAS30|CardStatus_VGS_INVALID_DNONCE|29|12.2.40	
31|0|CAS31|CardStatus_VGS_NO_VGS_SERVER|30|12.2.41	
32|0|CAS32|CardStatus_VGS_ERROR_IN_VGS_SERVER|31|12.2.42	
33|0|CAS33|CardStatus_VGS_INVALID_REQUEST_FORMAT|32|12.2.43	
34|0|CAS34|CardStatus_VGS_BAD_SIGNATURE|33|12.2.44	
35|0|CAS35|CardStatus_VGS_PUBID_NOT_EXIST_IN_VGS|34|12.2.45	
366|0|CAS36|CardStatus_CARD_IDL|35|12.2.46	
37|0|CAS37|CardStatus_VGS_SERVER_BUSY|36|12.2.47	
38|0|CAS38|CardStatus_VGS_PUBID_DELETED|37|12.2.48

39|0|TUNE7|Attempt to tune to channel that is not entitled through LRM|38|12.2.57	
40|0|TUNE8|Attempt to tune to channel that is not entitled through LRM but entitled through CAS|39|	
41|0|TUNE9|Channel is entitled through LRM but tune to stream failed|40|12.2.59	
42|0|TUNE10|While switched to default packages, attempt to tune to non-default channel but the channel is entitled through CAS|57|


43|1|CSE-36|Channel Unavailable|41|12.2.11

44|0|CSE-201|Unable to tune to the EAN channel|42|12.2.3 


45|2|CSE-9|AVN session setup failure. The full screen Guide is not accessible.|43|12.2.16	
46|2|CSE-10|AVN session setup failure. The Home Screen is not accessible.|44|12.2.1	
47|2|CSE-11|AVN session setup failure. On Demand is not accessible.|45|12.2.2	
48|2|CSE-24|AVN session establishing initiated by AVN channel tune has failed.|46|12.2.9	
49|2|CSE-26|AVN session setup failure. PPV Event Unavailable.|47|12.2.10	
50|2|CSE-22-5|AVN session setup failure. The Details Page is not accessible.|48|	
51|2|CSE-38|AVN session setup failure. Instant Upgrade Unavailable|49|12.2.12	
52|2|CSE-71|AVN session setup failure. The Network screen is not accessible.|50|12.2.13	
53|2|CSE-73|AVN session setup failure. The Global Settings is not accessible.|51|12.2.14	
54|2|CSE-80|Application Not Available|52|12.2.15

55|3|CSE-22-1|Server error|53|12.2.4	
56|3|CSE-22-2|Inactivity Timeout Reached|54|12.2.5	
57|3|CSE-22-3|Heartbeat lost|55|12.2.6	
58|3|CSE-22-4|The server has the session resources in quarantine|56|
