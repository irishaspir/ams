7.1.03 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with empty body
7.1.04 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with bad format of body, one comma was deleted
7.1.05 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in title command PURCHASE
7.1.06 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with empty title command
7.1.07 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in title command
7.1.08 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with numbers in title command
7.1.09 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with empty PURCHASE EID
7.1.10 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in PURCHASE EID
7.1.11 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in PURCHASE EID
7.1.12 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in PURCHASE PurchaseTime, empty PurchaseTime
7.1.13 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in PURCHASE PurchaseTime, bad format
7.1.14 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in PURCHASE PurchaseTime, nonexistent time
7.1.15 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in PURCHASE PurchaseTime
7.1.16 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in title command MODIFY"
7.1.17 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with empty MODIFY EID
7.1.18 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in MODIFY EID
7.1.19 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in MODIFY EID
7.1.20 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in MODIFY PurchaseTime,  empty PurchaseTime
7.1.21 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in MODIFY PurchaseTime, bad format
7.1.22 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in MODIFY PurchaseTime, nonexistent time
7.1.23 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in MODIFY PurchaseTime
7.1.24 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in title command DELETE
7.1.25 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with empty DELETE EID
7.1.26 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in DELETE EID
7.1.27 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in DELETE EID
7.1.28 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in DELETE PurchaseTime,  empty PurchaseTime
7.1.29 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in DELETE PurchaseTime, bad format
7.1.30 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with error in DELETE PurchaseTime, nonexistent time
7.1.31 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in DELETE PurchaseTime
7.1.32 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in title command PURCHASE
7.1.33 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in title command MODIFY
7.1.34 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in title command DELETE
7.1.35 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in key DeviceId
7.1.36 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in key PPVEvents
7.1.37 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in key Command
7.1.38 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in key Parameters
7.1.39 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in key EID
7.1.40 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with wrong case in key PurchaseTime
#7.1.41 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent EID for command PURCHASE
#7.1.42 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent EID for command MODIFY
#7.1.43 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent EID for command DELETE
7.1.44 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with empty  STB MAC address
7.1.45 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with bad format of STB MAC address
7.1.46 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with special characters in STB MAC address
7.1.47 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with nonexistent STB MAC address
7.1.48 [PPV][Validation] PPV EVENT NOTIFICATION REQUEST with powerOFF STB. The attempt to reach STB has failed