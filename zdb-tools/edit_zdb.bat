java -jar "%~dp0zdbDecoder.jar" %1 "%~dpn1-temp.xml"
@:loop
@if not exist "%~dpn1-temp.xml" goto end
@for %%X in ("%~dpn1-temp.xml") do @if %%~zX == 0 goto end
@"notepad.exe" "%~dpn1-temp.xml"
java -jar "%~dp0zdbEncoder.jar" "%~dpn1-temp.xml" "%~dpn1-out.zdb"
@for %%X in ("%~dpn1-out.zdb") do @if %%~zX == 0 goto loop
@del %1
@ren "%~dpn1-out.zdb" "%~n1.zdb"
@:end
@del "%~dpn1-temp.xml"
@if exist "%~dpn1-out.zdb" del "%~dpn1-out.zdb"
